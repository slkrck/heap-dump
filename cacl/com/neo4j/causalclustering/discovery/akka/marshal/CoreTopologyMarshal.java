package com.neo4j.causalclustering.discovery.akka.marshal;

import com.neo4j.causalclustering.core.state.storage.SafeChannelMarshal;
import com.neo4j.causalclustering.discovery.CoreServerInfo;
import com.neo4j.causalclustering.discovery.DatabaseCoreTopology;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.identity.RaftId;
import com.neo4j.causalclustering.messaging.EndOfStreamException;
import com.neo4j.causalclustering.messaging.marshalling.ChannelMarshal;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;
import org.neo4j.kernel.database.DatabaseId;

public class CoreTopologyMarshal extends SafeChannelMarshal<DatabaseCoreTopology>
{
    private final ChannelMarshal<MemberId> memberIdMarshal = new MemberId.Marshal();
    private final ChannelMarshal<CoreServerInfo> coreServerInfoChannelMarshal = new CoreServerInfoMarshal();
    private final ChannelMarshal<RaftId> raftIdMarshal = new RaftId.Marshal();

    protected DatabaseCoreTopology unmarshal0( ReadableChannel channel ) throws IOException, EndOfStreamException
    {
        DatabaseId databaseId = (DatabaseId) DatabaseIdWithoutNameMarshal.INSTANCE.unmarshal( channel );
        RaftId raftId = (RaftId) this.raftIdMarshal.unmarshal( channel );
        int memberCount = channel.getInt();
        HashMap<MemberId,CoreServerInfo> members = new HashMap( memberCount );

        for ( int i = 0; i < memberCount; ++i )
        {
            MemberId memberId = (MemberId) this.memberIdMarshal.unmarshal( channel );
            CoreServerInfo coreServerInfo = (CoreServerInfo) this.coreServerInfoChannelMarshal.unmarshal( channel );
            members.put( memberId, coreServerInfo );
        }

        return new DatabaseCoreTopology( databaseId, raftId, members );
    }

    public void marshal( DatabaseCoreTopology coreTopology, WritableChannel channel ) throws IOException
    {
        DatabaseIdWithoutNameMarshal.INSTANCE.marshal( coreTopology.databaseId(), channel );
        this.raftIdMarshal.marshal( coreTopology.raftId(), channel );
        channel.putInt( coreTopology.members().size() );
        Iterator var3 = coreTopology.members().entrySet().iterator();

        while ( var3.hasNext() )
        {
            Entry<MemberId,CoreServerInfo> entry = (Entry) var3.next();
            MemberId memberId = (MemberId) entry.getKey();
            CoreServerInfo coreServerInfo = (CoreServerInfo) entry.getValue();
            this.memberIdMarshal.marshal( memberId, channel );
            this.coreServerInfoChannelMarshal.marshal( coreServerInfo, channel );
        }
    }
}
