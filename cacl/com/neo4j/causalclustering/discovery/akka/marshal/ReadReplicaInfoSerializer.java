package com.neo4j.causalclustering.discovery.akka.marshal;

import com.neo4j.causalclustering.discovery.ReadReplicaInfo;

public class ReadReplicaInfoSerializer extends BaseAkkaSerializer<ReadReplicaInfo>
{
    protected ReadReplicaInfoSerializer()
    {
        super( new ReadReplicaInfoMarshal(), 1006, 256 );
    }
}
