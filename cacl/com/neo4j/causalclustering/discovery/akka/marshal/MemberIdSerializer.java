package com.neo4j.causalclustering.discovery.akka.marshal;

import com.neo4j.causalclustering.identity.MemberId;

public class MemberIdSerializer extends BaseAkkaSerializer<MemberId>
{
    protected MemberIdSerializer()
    {
        super( new MemberId.Marshal(), 1005, 17 );
    }
}
