package com.neo4j.causalclustering.discovery.akka.marshal;

import akka.cluster.UniqueAddress;

public class UniqueAddressSerializer extends BaseAkkaSerializer<UniqueAddress>
{
    private static final int SIZE_HINT = 256;

    public UniqueAddressSerializer()
    {
        super( new UniqueAddressMarshal(), 1002, 256 );
    }
}
