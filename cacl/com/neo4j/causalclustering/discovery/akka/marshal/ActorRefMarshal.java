package com.neo4j.causalclustering.discovery.akka.marshal;

import akka.actor.ActorRef;
import akka.actor.ExtendedActorSystem;
import akka.serialization.Serialization;
import com.neo4j.causalclustering.core.state.storage.SafeChannelMarshal;
import com.neo4j.causalclustering.messaging.marshalling.StringMarshal;

import java.io.IOException;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class ActorRefMarshal extends SafeChannelMarshal<ActorRef>
{
    private final ExtendedActorSystem actorSystem;

    public ActorRefMarshal( ExtendedActorSystem actorSystem )
    {
        this.actorSystem = actorSystem;
    }

    protected ActorRef unmarshal0( ReadableChannel channel ) throws IOException
    {
        String actorRefPath = StringMarshal.unmarshal( channel );
        return this.actorSystem.provider().resolveActorRef( actorRefPath );
    }

    public void marshal( ActorRef actorRef, WritableChannel channel ) throws IOException
    {
        String actorPath = Serialization.serializedActorPath( actorRef );
        StringMarshal.marshal( channel, actorPath );
    }
}
