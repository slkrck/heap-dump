package com.neo4j.causalclustering.discovery.akka.marshal;

import com.neo4j.causalclustering.discovery.akka.coretopology.CoreServerInfoForMemberId;

public class CoreServerInfoForMemberIdSerializer extends BaseAkkaSerializer<CoreServerInfoForMemberId>
{
    private static final int SIZE_HINT = 512;

    public CoreServerInfoForMemberIdSerializer()
    {
        super( new CoreServerInfoForMemberIdMarshal(), 1003, 512 );
    }
}
