package com.neo4j.causalclustering.discovery.akka.marshal;

import akka.actor.ActorRef;
import akka.actor.ExtendedActorSystem;
import com.neo4j.causalclustering.core.state.storage.SafeChannelMarshal;
import com.neo4j.causalclustering.discovery.ReadReplicaInfo;
import com.neo4j.causalclustering.discovery.akka.database.state.DiscoveryDatabaseState;
import com.neo4j.causalclustering.discovery.akka.readreplicatopology.ReadReplicaRefreshMessage;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.messaging.EndOfStreamException;
import com.neo4j.causalclustering.messaging.marshalling.ChannelMarshal;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;
import org.neo4j.kernel.database.DatabaseId;

public class ReadReplicaRefreshMessageMarshal extends SafeChannelMarshal<ReadReplicaRefreshMessage>
{
    private final ChannelMarshal<ReadReplicaInfo> readReplicaInfoMarshal;
    private final ChannelMarshal<MemberId> memberIdMarshal = new MemberId.Marshal();
    private final ChannelMarshal<ActorRef> actorRefMarshal;

    public ReadReplicaRefreshMessageMarshal( ExtendedActorSystem system )
    {
        this.actorRefMarshal = new ActorRefMarshal( system );
        this.readReplicaInfoMarshal = new ReadReplicaInfoMarshal();
    }

    protected ReadReplicaRefreshMessage unmarshal0( ReadableChannel channel ) throws IOException, EndOfStreamException
    {
        ReadReplicaInfo rrInfo = (ReadReplicaInfo) this.readReplicaInfoMarshal.unmarshal( channel );
        MemberId memberId = (MemberId) this.memberIdMarshal.unmarshal( channel );
        ActorRef clusterClient = (ActorRef) this.actorRefMarshal.unmarshal( channel );
        ActorRef topologyClient = (ActorRef) this.actorRefMarshal.unmarshal( channel );
        HashMap<DatabaseId,DiscoveryDatabaseState> databaseStates = new HashMap();
        int size = channel.getInt();

        for ( int i = 0; i < size; ++i )
        {
            DatabaseId id = (DatabaseId) DatabaseIdWithoutNameMarshal.INSTANCE.unmarshal( channel );
            DiscoveryDatabaseState state = (DiscoveryDatabaseState) DiscoveryDatabaseStateMarshal.INSTANCE.unmarshal( channel );
            databaseStates.put( id, state );
        }

        return new ReadReplicaRefreshMessage( rrInfo, memberId, clusterClient, topologyClient, databaseStates );
    }

    public void marshal( ReadReplicaRefreshMessage readReplicaRefreshMessage, WritableChannel channel ) throws IOException
    {
        this.readReplicaInfoMarshal.marshal( readReplicaRefreshMessage.readReplicaInfo(), channel );
        this.memberIdMarshal.marshal( readReplicaRefreshMessage.memberId(), channel );
        this.actorRefMarshal.marshal( readReplicaRefreshMessage.clusterClient(), channel );
        this.actorRefMarshal.marshal( readReplicaRefreshMessage.topologyClientActorRef(), channel );
        Map<DatabaseId,DiscoveryDatabaseState> databaseStates = readReplicaRefreshMessage.databaseStates();
        channel.putInt( databaseStates.size() );
        Iterator var4 = databaseStates.entrySet().iterator();

        while ( var4.hasNext() )
        {
            Entry<DatabaseId,DiscoveryDatabaseState> entry = (Entry) var4.next();
            DatabaseIdWithoutNameMarshal.INSTANCE.marshal( (DatabaseId) entry.getKey(), channel );
            DiscoveryDatabaseStateMarshal.INSTANCE.marshal( (DiscoveryDatabaseState) entry.getValue(), channel );
        }
    }
}
