package com.neo4j.causalclustering.discovery.akka.marshal;

import com.neo4j.causalclustering.core.state.storage.SafeChannelMarshal;
import com.neo4j.causalclustering.discovery.ReplicatedDatabaseState;
import com.neo4j.causalclustering.discovery.akka.database.state.DiscoveryDatabaseState;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.messaging.EndOfStreamException;
import com.neo4j.causalclustering.messaging.marshalling.BooleanMarshal;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;
import org.neo4j.kernel.database.DatabaseId;

public class ReplicatedDatabaseStateMarshal extends SafeChannelMarshal<ReplicatedDatabaseState>
{
    public static ReplicatedDatabaseStateMarshal INSTANCE = new ReplicatedDatabaseStateMarshal();

    private ReplicatedDatabaseStateMarshal()
    {
    }

    protected ReplicatedDatabaseState unmarshal0( ReadableChannel channel ) throws IOException, EndOfStreamException
    {
        DatabaseId databaseId = (DatabaseId) DatabaseIdWithoutNameMarshal.INSTANCE.unmarshal( channel );
        boolean containsCoreMembers = BooleanMarshal.unmarshal( channel );
        int memberCount = channel.getInt();
        HashMap<MemberId,DiscoveryDatabaseState> memberStates = new HashMap();

        for ( int i = 0; i < memberCount; ++i )
        {
            MemberId memberId = (MemberId) MemberId.Marshal.INSTANCE.unmarshal( channel );
            DiscoveryDatabaseState databaseState = (DiscoveryDatabaseState) DiscoveryDatabaseStateMarshal.INSTANCE.unmarshal( channel );
            memberStates.put( memberId, databaseState );
        }

        return containsCoreMembers ? ReplicatedDatabaseState.ofCores( databaseId, memberStates )
                                   : ReplicatedDatabaseState.ofReadReplicas( databaseId, memberStates );
    }

    public void marshal( ReplicatedDatabaseState clusteredDatabaseState, WritableChannel channel ) throws IOException
    {
        DatabaseIdWithoutNameMarshal.INSTANCE.marshal( clusteredDatabaseState.databaseId(), channel );
        BooleanMarshal.marshal( channel, clusteredDatabaseState.containsCoreStates() );
        Map<MemberId,DiscoveryDatabaseState> memberStates = Map.copyOf( clusteredDatabaseState.memberStates() );
        channel.putInt( memberStates.size() );
        Iterator var4 = memberStates.entrySet().iterator();

        while ( var4.hasNext() )
        {
            Entry<MemberId,DiscoveryDatabaseState> entry = (Entry) var4.next();
            MemberId memberId = (MemberId) entry.getKey();
            DiscoveryDatabaseState databaseState = (DiscoveryDatabaseState) entry.getValue();
            MemberId.Marshal.INSTANCE.marshal( memberId, channel );
            DiscoveryDatabaseStateMarshal.INSTANCE.marshal( databaseState, channel );
        }
    }
}
