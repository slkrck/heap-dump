package com.neo4j.causalclustering.discovery.akka.marshal;

import com.neo4j.causalclustering.core.state.storage.SafeChannelMarshal;
import com.neo4j.causalclustering.messaging.EndOfStreamException;
import com.neo4j.causalclustering.messaging.marshalling.UUIDMarshal;

import java.io.IOException;
import java.util.UUID;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;
import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.kernel.database.DatabaseIdFactory;

public class DatabaseIdWithoutNameMarshal extends SafeChannelMarshal<DatabaseId>
{
    public static DatabaseIdWithoutNameMarshal INSTANCE = new DatabaseIdWithoutNameMarshal();

    protected DatabaseId unmarshal0( ReadableChannel channel ) throws IOException, EndOfStreamException
    {
        return DatabaseIdFactory.from( (UUID) UUIDMarshal.INSTANCE.unmarshal( channel ) );
    }

    public void marshal( DatabaseId databaseIdRaw, WritableChannel channel ) throws IOException
    {
        UUIDMarshal.INSTANCE.marshal( databaseIdRaw.uuid(), channel );
    }
}
