package com.neo4j.causalclustering.discovery.akka.marshal;

import com.neo4j.causalclustering.core.state.storage.SafeChannelMarshal;
import com.neo4j.causalclustering.discovery.DiscoveryServerInfo;
import com.neo4j.causalclustering.messaging.EndOfStreamException;
import com.neo4j.causalclustering.messaging.marshalling.StringMarshal;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;
import org.neo4j.kernel.database.DatabaseId;

abstract class DiscoveryServerInfoMarshal<T extends DiscoveryServerInfo> extends SafeChannelMarshal<T>
{
    static Set<String> unmarshalGroups( ReadableChannel channel ) throws IOException
    {
        int size = channel.getInt();
        HashSet<String> groups = new HashSet( size );

        for ( int i = 0; i < size; ++i )
        {
            groups.add( StringMarshal.unmarshal( channel ) );
        }

        return groups;
    }

    static void marshalGroups( DiscoveryServerInfo info, WritableChannel channel ) throws IOException
    {
        Set<String> groups = info.groups();
        channel.putInt( groups.size() );
        Iterator var3 = groups.iterator();

        while ( var3.hasNext() )
        {
            String group = (String) var3.next();
            StringMarshal.marshal( channel, group );
        }
    }

    static Set<DatabaseId> unmarshalDatabaseIds( ReadableChannel channel ) throws IOException, EndOfStreamException
    {
        int size = channel.getInt();
        HashSet<DatabaseId> databaseIds = new HashSet( size );

        for ( int i = 0; i < size; ++i )
        {
            databaseIds.add( (DatabaseId) DatabaseIdWithoutNameMarshal.INSTANCE.unmarshal( channel ) );
        }

        return databaseIds;
    }

    static void marshalDatabaseIds( DiscoveryServerInfo info, WritableChannel channel ) throws IOException
    {
        Set<DatabaseId> databaseIds = info.startedDatabaseIds();
        channel.putInt( databaseIds.size() );
        Iterator var3 = databaseIds.iterator();

        while ( var3.hasNext() )
        {
            DatabaseId databaseId = (DatabaseId) var3.next();
            DatabaseIdWithoutNameMarshal.INSTANCE.marshal( databaseId, channel );
        }
    }
}
