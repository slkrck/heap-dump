package com.neo4j.causalclustering.discovery.akka.marshal;

import com.neo4j.causalclustering.discovery.ReplicatedDatabaseState;

public class ReplicatedDatabaseStateSerializer extends BaseAkkaSerializer<ReplicatedDatabaseState>
{
    private static final int SIZE_HINT = 1024;

    public ReplicatedDatabaseStateSerializer()
    {
        super( ReplicatedDatabaseStateMarshal.INSTANCE, 1013, 1024 );
    }
}
