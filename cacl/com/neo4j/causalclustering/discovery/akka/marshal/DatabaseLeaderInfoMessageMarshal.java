package com.neo4j.causalclustering.discovery.akka.marshal;

import com.neo4j.causalclustering.core.consensus.LeaderInfo;
import com.neo4j.causalclustering.core.state.storage.SafeChannelMarshal;
import com.neo4j.causalclustering.discovery.akka.directory.LeaderInfoDirectoryMessage;
import com.neo4j.causalclustering.messaging.EndOfStreamException;
import com.neo4j.causalclustering.messaging.marshalling.ChannelMarshal;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;
import org.neo4j.kernel.database.DatabaseId;

public class DatabaseLeaderInfoMessageMarshal extends SafeChannelMarshal<LeaderInfoDirectoryMessage>
{
    private final ChannelMarshal<LeaderInfo> leaderInfoMarshal = new LeaderInfoMarshal();

    protected LeaderInfoDirectoryMessage unmarshal0( ReadableChannel channel ) throws IOException, EndOfStreamException
    {
        int size = channel.getInt();
        HashMap<DatabaseId,LeaderInfo> leaders = new HashMap( size );

        for ( int i = 0; i < size; ++i )
        {
            DatabaseId databaseId = (DatabaseId) DatabaseIdWithoutNameMarshal.INSTANCE.unmarshal( channel );
            LeaderInfo leaderInfo = (LeaderInfo) this.leaderInfoMarshal.unmarshal( channel );
            leaders.put( databaseId, leaderInfo );
        }

        return new LeaderInfoDirectoryMessage( leaders );
    }

    public void marshal( LeaderInfoDirectoryMessage leaderInfoDirectoryMessage, WritableChannel channel ) throws IOException
    {
        channel.putInt( leaderInfoDirectoryMessage.leaders().size() );
        Iterator var3 = leaderInfoDirectoryMessage.leaders().entrySet().iterator();

        while ( var3.hasNext() )
        {
            Entry<DatabaseId,LeaderInfo> entry = (Entry) var3.next();
            DatabaseId databaseId = (DatabaseId) entry.getKey();
            LeaderInfo leaderInfo = (LeaderInfo) entry.getValue();
            DatabaseIdWithoutNameMarshal.INSTANCE.marshal( databaseId, channel );
            this.leaderInfoMarshal.marshal( leaderInfo, channel );
        }
    }
}
