package com.neo4j.causalclustering.discovery.akka.marshal;

import com.neo4j.causalclustering.discovery.akka.database.state.DatabaseToMember;

public class DatabaseToMemberSerializer extends BaseAkkaSerializer<DatabaseToMember>
{
    private static final int SIZE_HINT = 96;

    public DatabaseToMemberSerializer()
    {
        super( DatabaseToMemberMarshal.INSTANCE, 1014, 96 );
    }
}
