package com.neo4j.causalclustering.discovery.akka.marshal;

import akka.actor.ActorRef;
import akka.actor.ExtendedActorSystem;
import com.neo4j.causalclustering.core.state.storage.SafeChannelMarshal;
import com.neo4j.causalclustering.discovery.akka.readreplicatopology.ReadReplicaRemovalMessage;
import com.neo4j.causalclustering.messaging.EndOfStreamException;
import com.neo4j.causalclustering.messaging.marshalling.ChannelMarshal;

import java.io.IOException;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class ReadReplicaRemovalMessageMarshal extends SafeChannelMarshal<ReadReplicaRemovalMessage>
{
    private final ChannelMarshal<ActorRef> actorRefMarshal;

    public ReadReplicaRemovalMessageMarshal( ExtendedActorSystem system )
    {
        this.actorRefMarshal = new ActorRefMarshal( system );
    }

    protected ReadReplicaRemovalMessage unmarshal0( ReadableChannel channel ) throws IOException, EndOfStreamException
    {
        return new ReadReplicaRemovalMessage( (ActorRef) this.actorRefMarshal.unmarshal( channel ) );
    }

    public void marshal( ReadReplicaRemovalMessage readReplicaRemovalMessage, WritableChannel channel ) throws IOException
    {
        this.actorRefMarshal.marshal( readReplicaRemovalMessage.clusterClient(), channel );
    }
}
