package com.neo4j.causalclustering.discovery.akka.marshal;

import org.neo4j.kernel.database.DatabaseId;

public class DatabaseIdWithoutNameSerializer extends BaseAkkaSerializer<DatabaseId>
{
    private static final int sizeHint = 16;

    protected DatabaseIdWithoutNameSerializer()
    {
        super( DatabaseIdWithoutNameMarshal.INSTANCE, 1012, 16 );
    }
}
