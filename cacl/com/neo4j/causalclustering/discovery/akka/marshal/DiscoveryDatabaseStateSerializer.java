package com.neo4j.causalclustering.discovery.akka.marshal;

import com.neo4j.causalclustering.discovery.akka.database.state.DiscoveryDatabaseState;

public class DiscoveryDatabaseStateSerializer extends BaseAkkaSerializer<DiscoveryDatabaseState>
{
    public static int SIZE_HINT = 128;

    public DiscoveryDatabaseStateSerializer()
    {
        super( DiscoveryDatabaseStateMarshal.INSTANCE, 1015, SIZE_HINT );
    }
}
