package com.neo4j.causalclustering.discovery.akka.marshal;

import com.neo4j.causalclustering.discovery.akka.directory.ReplicatedLeaderInfo;

public class ReplicatedLeaderInfoSerializer extends BaseAkkaSerializer<ReplicatedLeaderInfo>
{
    private static final int SIZE_HINT = 25;

    public ReplicatedLeaderInfoSerializer()
    {
        super( new ReplicatedLeaderInfoMarshal(), 1011, 25 );
    }
}
