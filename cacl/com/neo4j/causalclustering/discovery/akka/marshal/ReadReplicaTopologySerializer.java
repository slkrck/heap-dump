package com.neo4j.causalclustering.discovery.akka.marshal;

import com.neo4j.causalclustering.discovery.DatabaseReadReplicaTopology;

public class ReadReplicaTopologySerializer extends BaseAkkaSerializer<DatabaseReadReplicaTopology>
{
    protected ReadReplicaTopologySerializer()
    {
        super( new ReadReplicaTopologyMarshal(), 1009, 1024 );
    }
}
