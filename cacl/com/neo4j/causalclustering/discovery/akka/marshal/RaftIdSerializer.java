package com.neo4j.causalclustering.discovery.akka.marshal;

import com.neo4j.causalclustering.identity.RaftId;

public class RaftIdSerializer extends BaseAkkaSerializer<RaftId>
{
    static final int SIZE_HINT = 16;

    public RaftIdSerializer()
    {
        super( new RaftId.Marshal(), 1001, 16 );
    }
}
