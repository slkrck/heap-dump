package com.neo4j.causalclustering.discovery.akka.marshal;

import com.neo4j.causalclustering.core.state.storage.SafeChannelMarshal;
import com.neo4j.causalclustering.messaging.marshalling.StringMarshal;

import java.io.IOException;

import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class AdvertisedSocketAddressMarshal extends SafeChannelMarshal<SocketAddress>
{
    protected SocketAddress unmarshal0( ReadableChannel channel ) throws IOException
    {
        String host = StringMarshal.unmarshal( channel );
        int port = channel.getInt();
        return new SocketAddress( host, port );
    }

    public void marshal( SocketAddress advertisedSocketAddress, WritableChannel channel ) throws IOException
    {
        StringMarshal.marshal( channel, advertisedSocketAddress.getHostname() );
        channel.putInt( advertisedSocketAddress.getPort() );
    }
}
