package com.neo4j.causalclustering.discovery.akka.marshal;

import com.neo4j.causalclustering.discovery.akka.directory.LeaderInfoDirectoryMessage;

public class DatabaseLeaderInfoMessageSerializer extends BaseAkkaSerializer<LeaderInfoDirectoryMessage>
{
    protected DatabaseLeaderInfoMessageSerializer()
    {
        super( new DatabaseLeaderInfoMessageMarshal(), 1010, 384 );
    }
}
