package com.neo4j.causalclustering.discovery.akka.marshal;

import com.neo4j.causalclustering.core.state.storage.SafeChannelMarshal;
import com.neo4j.causalclustering.discovery.CoreServerInfo;
import com.neo4j.causalclustering.discovery.akka.coretopology.CoreServerInfoForMemberId;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.messaging.EndOfStreamException;
import com.neo4j.causalclustering.messaging.marshalling.ChannelMarshal;

import java.io.IOException;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class CoreServerInfoForMemberIdMarshal extends SafeChannelMarshal<CoreServerInfoForMemberId>
{
    private final ChannelMarshal<MemberId> memberIdMarshal = new MemberId.Marshal();
    private final ChannelMarshal<CoreServerInfo> coreServerInfoMarshal = new CoreServerInfoMarshal();

    protected CoreServerInfoForMemberId unmarshal0( ReadableChannel channel ) throws IOException, EndOfStreamException
    {
        MemberId memberId = (MemberId) this.memberIdMarshal.unmarshal( channel );
        CoreServerInfo coreServerInfo = (CoreServerInfo) this.coreServerInfoMarshal.unmarshal( channel );
        return new CoreServerInfoForMemberId( memberId, coreServerInfo );
    }

    public void marshal( CoreServerInfoForMemberId coreServerInfoForMemberId, WritableChannel channel ) throws IOException
    {
        this.memberIdMarshal.marshal( coreServerInfoForMemberId.memberId(), channel );
        this.coreServerInfoMarshal.marshal( coreServerInfoForMemberId.coreServerInfo(), channel );
    }
}
