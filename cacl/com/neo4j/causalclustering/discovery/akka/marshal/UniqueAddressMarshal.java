package com.neo4j.causalclustering.discovery.akka.marshal;

import akka.actor.Address;
import akka.cluster.UniqueAddress;
import com.neo4j.causalclustering.core.state.storage.SafeChannelMarshal;
import com.neo4j.causalclustering.messaging.EndOfStreamException;
import com.neo4j.causalclustering.messaging.marshalling.StringMarshal;

import java.io.IOException;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class UniqueAddressMarshal extends SafeChannelMarshal<UniqueAddress>
{
    protected UniqueAddress unmarshal0( ReadableChannel channel ) throws IOException, EndOfStreamException
    {
        String protocol = StringMarshal.unmarshal( channel );
        String system = StringMarshal.unmarshal( channel );
        String host = StringMarshal.unmarshal( channel );
        int port = channel.getInt();
        long uid = channel.getLong();
        Address address;
        if ( host != null )
        {
            address = new Address( protocol, system, host, port );
        }
        else
        {
            address = new Address( protocol, system );
        }

        return new UniqueAddress( address, uid );
    }

    public void marshal( UniqueAddress uniqueAddress, WritableChannel channel ) throws IOException
    {
        Address address = uniqueAddress.address();
        StringMarshal.marshal( channel, address.protocol() );
        StringMarshal.marshal( channel, address.system() );
        if ( address.host().isDefined() )
        {
            StringMarshal.marshal( channel, (String) address.host().get() );
        }
        else
        {
            StringMarshal.marshal( (WritableChannel) channel, (String) null );
        }

        if ( address.port().isDefined() )
        {
            channel.putInt( (Integer) address.port().get() );
        }
        else
        {
            channel.putInt( -1 );
        }

        channel.putLong( uniqueAddress.longUid() );
    }
}
