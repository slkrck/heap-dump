package com.neo4j.causalclustering.discovery.akka.marshal;

import com.neo4j.causalclustering.core.state.storage.SafeChannelMarshal;
import com.neo4j.causalclustering.discovery.akka.database.state.DatabaseToMember;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.messaging.EndOfStreamException;

import java.io.IOException;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;
import org.neo4j.kernel.database.DatabaseId;

public class DatabaseToMemberMarshal extends SafeChannelMarshal<DatabaseToMember>
{
    public static final DatabaseToMemberMarshal INSTANCE = new DatabaseToMemberMarshal();

    private DatabaseToMemberMarshal()
    {
    }

    protected DatabaseToMember unmarshal0( ReadableChannel channel ) throws IOException, EndOfStreamException
    {
        DatabaseId databaseId = (DatabaseId) DatabaseIdWithoutNameMarshal.INSTANCE.unmarshal( channel );
        MemberId memberId = (MemberId) MemberId.Marshal.INSTANCE.unmarshal( channel );
        return new DatabaseToMember( databaseId, memberId );
    }

    public void marshal( DatabaseToMember databaseToMember, WritableChannel channel ) throws IOException
    {
        DatabaseIdWithoutNameMarshal.INSTANCE.marshal( databaseToMember.databaseId(), channel );
        MemberId.Marshal.INSTANCE.marshal( databaseToMember.memberId(), channel );
    }
}
