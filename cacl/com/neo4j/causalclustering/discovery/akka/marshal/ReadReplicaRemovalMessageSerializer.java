package com.neo4j.causalclustering.discovery.akka.marshal;

import akka.actor.ExtendedActorSystem;
import com.neo4j.causalclustering.discovery.akka.readreplicatopology.ReadReplicaRemovalMessage;

public class ReadReplicaRemovalMessageSerializer extends BaseAkkaSerializer<ReadReplicaRemovalMessage>
{
    protected ReadReplicaRemovalMessageSerializer( ExtendedActorSystem system )
    {
        super( new ReadReplicaRemovalMessageMarshal( system ), 1008, 64 );
    }
}
