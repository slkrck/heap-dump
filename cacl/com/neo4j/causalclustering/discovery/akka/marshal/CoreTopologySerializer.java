package com.neo4j.causalclustering.discovery.akka.marshal;

import com.neo4j.causalclustering.discovery.DatabaseCoreTopology;

public class CoreTopologySerializer extends BaseAkkaSerializer<DatabaseCoreTopology>
{
    public static final int SIZE_HINT = 1536;

    public CoreTopologySerializer()
    {
        super( new CoreTopologyMarshal(), 1007, 1536 );
    }
}
