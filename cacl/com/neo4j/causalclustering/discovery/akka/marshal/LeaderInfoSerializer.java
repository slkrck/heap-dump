package com.neo4j.causalclustering.discovery.akka.marshal;

import com.neo4j.causalclustering.core.consensus.LeaderInfo;

public class LeaderInfoSerializer extends BaseAkkaSerializer<LeaderInfo>
{
    private static final int SIZE_HINT = 25;

    public LeaderInfoSerializer()
    {
        super( new LeaderInfoMarshal(), 1000, 25 );
    }
}
