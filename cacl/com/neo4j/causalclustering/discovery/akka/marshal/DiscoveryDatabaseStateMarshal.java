package com.neo4j.causalclustering.discovery.akka.marshal;

import com.neo4j.causalclustering.core.state.storage.SafeChannelMarshal;
import com.neo4j.causalclustering.discovery.akka.database.state.DiscoveryDatabaseState;
import com.neo4j.causalclustering.messaging.EndOfStreamException;
import com.neo4j.causalclustering.messaging.marshalling.BooleanMarshal;
import com.neo4j.causalclustering.messaging.marshalling.StringMarshal;
import com.neo4j.dbms.EnterpriseOperatorState;
import com.neo4j.dbms.RemoteDatabaseManagementException;

import java.io.IOException;
import java.util.Optional;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;
import org.neo4j.kernel.database.DatabaseId;

public class DiscoveryDatabaseStateMarshal extends SafeChannelMarshal<DiscoveryDatabaseState>
{
    public static DiscoveryDatabaseStateMarshal INSTANCE = new DiscoveryDatabaseStateMarshal();

    private DiscoveryDatabaseStateMarshal()
    {
    }

    protected DiscoveryDatabaseState unmarshal0( ReadableChannel channel ) throws IOException, EndOfStreamException
    {
        DatabaseId databaseId = (DatabaseId) DatabaseIdWithoutNameMarshal.INSTANCE.unmarshal( channel );
        EnterpriseOperatorState operatorState = this.getOperatorState( channel.getInt() );
        boolean hasFailed = BooleanMarshal.unmarshal( channel );
        RemoteDatabaseManagementException failure;
        if ( hasFailed )
        {
            String failureMessage = StringMarshal.unmarshal( channel );
            failure = new RemoteDatabaseManagementException( failureMessage );
        }
        else
        {
            failure = null;
        }

        return new DiscoveryDatabaseState( databaseId, operatorState, failure );
    }

    public void marshal( DiscoveryDatabaseState databaseState, WritableChannel channel ) throws IOException
    {
        DatabaseIdWithoutNameMarshal.INSTANCE.marshal( databaseState.databaseId(), channel );
        channel.putInt( databaseState.operatorState().ordinal() );
        BooleanMarshal.marshal( channel, databaseState.hasFailed() );
        Optional<Throwable> failure = databaseState.failure();
        if ( failure.isPresent() )
        {
            StringMarshal.marshal( channel, ((Throwable) failure.get()).getMessage() );
        }
    }

    private EnterpriseOperatorState getOperatorState( int ordinal )
    {
        EnterpriseOperatorState[] operatorStates = EnterpriseOperatorState.values();
        return ordinal >= 0 && ordinal < operatorStates.length ? operatorStates[ordinal] : EnterpriseOperatorState.UNKNOWN;
    }
}
