package com.neo4j.causalclustering.discovery.akka;

import akka.actor.ActorRef;
import akka.actor.AbstractActor.Receive;
import akka.cluster.Cluster;
import akka.cluster.ddata.Key;
import akka.cluster.ddata.ReplicatedData;
import akka.cluster.ddata.Replicator.Changed;
import akka.cluster.ddata.Replicator.ReplicatorMessage;
import akka.cluster.ddata.Replicator.Subscribe;
import akka.cluster.ddata.Replicator.Unsubscribe;
import akka.cluster.ddata.Replicator.Update;
import akka.cluster.ddata.Replicator.UpdateResponse;
import akka.cluster.ddata.Replicator.WriteAll;
import akka.cluster.ddata.Replicator.WriteConsistency;
import akka.japi.pf.ReceiveBuilder;
import com.neo4j.causalclustering.discovery.akka.monitoring.ReplicatedDataIdentifier;
import com.neo4j.causalclustering.discovery.akka.monitoring.ReplicatedDataMonitor;

import java.time.Duration;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Supplier;

import scala.concurrent.duration.FiniteDuration;

public abstract class BaseReplicatedDataActor<T extends ReplicatedData> extends AbstractActorWithTimersAndLogging
{
    private static final WriteConsistency WRITE_CONSISTENCY;
    private static final String METRIC_TIMER_KEY = "refresh metric";

    static
    {
        WRITE_CONSISTENCY = new WriteAll( new FiniteDuration( 10L, TimeUnit.SECONDS ) );
    }

    protected final Cluster cluster;
    protected final ActorRef replicator;
    protected final Key<T> key;
    private final ReplicatedDataIdentifier identifier;
    private final Supplier<T> emptyData;
    private final ReplicatedDataMonitor monitor;
    protected T data;

    protected BaseReplicatedDataActor( Cluster cluster, ActorRef replicator, Function<String,Key<T>> keyFunction, Supplier<T> emptyData,
            ReplicatedDataIdentifier identifier, ReplicatedDataMonitor monitor )
    {
        this.cluster = cluster;
        this.replicator = replicator;
        this.key = (Key) keyFunction.apply( identifier.keyName() );
        this.emptyData = emptyData;
        this.data = (ReplicatedData) emptyData.get();
        this.identifier = identifier;
        this.monitor = monitor;
    }

    public final void preStart()
    {
        this.sendInitialDataToReplicator();
        this.subscribeToReplicatorEvents( new Subscribe( this.key, this.getSelf() ) );
        this.getTimers().startPeriodicTimer( "refresh metric", BaseReplicatedDataActor.MetricsRefresh.getInstance(), Duration.ofMinutes( 1L ) );
    }

    protected abstract void sendInitialDataToReplicator();

    public final void postStop()
    {
        this.subscribeToReplicatorEvents( new Unsubscribe( this.key, this.getSelf() ) );
    }

    public final Receive createReceive()
    {
        ReceiveBuilder receiveBuilder = new ReceiveBuilder();
        this.handleCustomEvents( receiveBuilder );
        this.handleReplicationEvents( receiveBuilder );
        return receiveBuilder.build();
    }

    private void handleReplicationEvents( ReceiveBuilder builder )
    {
        builder.match( Changed.class, ( c ) -> {
            return c.key().equals( this.key );
        }, ( message ) -> {
            T newData = message.dataValue();
            this.handleIncomingData( newData );
            this.logDataMetric();
        } ).match( UpdateResponse.class, ( updated ) -> {
            this.log().debug( "Update: {}", updated );
        } ).match( BaseReplicatedDataActor.MetricsRefresh.class, ( ignored ) -> {
            this.logDataMetric();
        } );
    }

    protected abstract void handleCustomEvents( ReceiveBuilder var1 );

    protected abstract void handleIncomingData( T var1 );

    private void logDataMetric()
    {
        this.monitor.setVisibleDataSize( this.identifier, this.dataMetricVisible() );
        this.monitor.setInvisibleDataSize( this.identifier, this.dataMetricInvisible() );
    }

    protected abstract int dataMetricVisible();

    protected abstract int dataMetricInvisible();

    private void subscribeToReplicatorEvents( ReplicatorMessage message )
    {
        this.replicator.tell( message, this.getSelf() );
    }

    protected void modifyReplicatedData( Key<T> key, Function<T,T> modify )
    {
        this.modifyReplicatedData( key, modify, (Object) null );
    }

    protected <M> void modifyReplicatedData( Key<T> key, Function<T,T> modify, M message )
    {
        Update<T> update = new Update( key, (ReplicatedData) this.emptyData.get(), WRITE_CONSISTENCY, Optional.ofNullable( message ), modify );
        this.replicator.tell( update, this.self() );
    }

    static class MetricsRefresh
    {
        private static BaseReplicatedDataActor.MetricsRefresh instance = new BaseReplicatedDataActor.MetricsRefresh();

        private MetricsRefresh()
        {
        }

        public static BaseReplicatedDataActor.MetricsRefresh getInstance()
        {
            return instance;
        }
    }
}
