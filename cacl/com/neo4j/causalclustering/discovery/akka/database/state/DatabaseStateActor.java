package com.neo4j.causalclustering.discovery.akka.database.state;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.cluster.Cluster;
import akka.cluster.ddata.LWWMap;
import akka.cluster.ddata.LWWMapKey;
import akka.japi.pf.ReceiveBuilder;
import akka.stream.javadsl.SourceQueueWithComplete;
import com.neo4j.causalclustering.discovery.ReplicatedDatabaseState;
import com.neo4j.causalclustering.discovery.akka.BaseReplicatedDataActor;
import com.neo4j.causalclustering.discovery.akka.monitoring.ReplicatedDataIdentifier;
import com.neo4j.causalclustering.discovery.akka.monitoring.ReplicatedDataMonitor;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.dbms.EnterpriseOperatorState;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Map.Entry;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.neo4j.kernel.database.DatabaseId;

public class DatabaseStateActor extends BaseReplicatedDataActor<LWWMap<DatabaseToMember,DiscoveryDatabaseState>>
{
    public static final String NAME = "cc-database-status-actor";
    private final SourceQueueWithComplete<ReplicatedDatabaseState> stateUpdateSink;
    private final ActorRef rrTopologyActor;
    private final MemberId memberId;

    private DatabaseStateActor( Cluster cluster, ActorRef replicator, SourceQueueWithComplete<ReplicatedDatabaseState> stateUpdateSink,
            ActorRef rrTopologyActor, ReplicatedDataMonitor monitor, MemberId memberId )
    {
        super( cluster, replicator, LWWMapKey::create, LWWMap::create, ReplicatedDataIdentifier.DATABASE_STATE, monitor );
        this.stateUpdateSink = stateUpdateSink;
        this.rrTopologyActor = rrTopologyActor;
        this.memberId = memberId;
    }

    public static Props props( Cluster cluster, ActorRef replicator, SourceQueueWithComplete<ReplicatedDatabaseState> discoveryUpdateSink,
            ActorRef rrTopologyActor, ReplicatedDataMonitor monitor, MemberId memberId )
    {
        return Props.create( DatabaseStateActor.class, () -> {
            return new DatabaseStateActor( cluster, replicator, discoveryUpdateSink, rrTopologyActor, monitor, memberId );
        } );
    }

    private static <K, V> Collector<Entry<K,V>,?,Map<K,V>> entriesToMap()
    {
        return Collectors.toMap( Entry::getKey, Entry::getValue );
    }

    protected int dataMetricVisible()
    {
        return ((LWWMap) this.data).size();
    }

    protected int dataMetricInvisible()
    {
        return ((LWWMap) this.data).underlying().keys().vvector().size();
    }

    protected void sendInitialDataToReplicator()
    {
    }

    protected void handleCustomEvents( ReceiveBuilder builder )
    {
        builder.match( DiscoveryDatabaseState.class, this::handleDatabaseState );
    }

    private void handleDatabaseState( DiscoveryDatabaseState update )
    {
        if ( update.operatorState() == EnterpriseOperatorState.DROPPED )
        {
            this.modifyReplicatedData( this.key, ( map ) -> {
                return map.remove( this.cluster, new DatabaseToMember( update.databaseId(), this.memberId ) );
            } );
        }
        else
        {
            this.modifyReplicatedData( this.key, ( map ) -> {
                return map.put( this.cluster, new DatabaseToMember( update.databaseId(), this.memberId ), update );
            } );
        }
    }

    protected void handleIncomingData( LWWMap<DatabaseToMember,DiscoveryDatabaseState> newData )
    {
        this.data = newData;
        Map<DatabaseId,Map<MemberId,DiscoveryDatabaseState>> statesGroupedByDatabase =
                (Map) ((LWWMap) this.data).getEntries().entrySet().stream().map( ( e ) -> {
                    return Map.entry( ((DatabaseToMember) e.getKey()).memberId(), (DiscoveryDatabaseState) e.getValue() );
                } ).collect( Collectors.groupingBy( ( e ) -> {
                    return ((DiscoveryDatabaseState) e.getValue()).databaseId();
                }, entriesToMap() ) );
        List<ReplicatedDatabaseState> allReplicatedStates = (List) statesGroupedByDatabase.entrySet().stream().map( ( e ) -> {
            return ReplicatedDatabaseState.ofCores( (DatabaseId) e.getKey(), (Map) e.getValue() );
        } ).collect( Collectors.toList() );
        SourceQueueWithComplete var10001 = this.stateUpdateSink;
        Objects.requireNonNull( var10001 );
        allReplicatedStates.forEach( var10001::offer );
        this.rrTopologyActor.tell( new AllReplicatedDatabaseStates( allReplicatedStates ), this.getSelf() );
    }
}
