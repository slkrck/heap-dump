package com.neo4j.causalclustering.discovery.akka.database.state;

import com.neo4j.causalclustering.discovery.ReplicatedDatabaseState;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.neo4j.kernel.database.DatabaseId;

public class AllReplicatedDatabaseStates
{
    private final Map<DatabaseId,ReplicatedDatabaseState> databaseStates;

    AllReplicatedDatabaseStates( Collection<ReplicatedDatabaseState> databaseStates )
    {
        this.databaseStates = (Map) databaseStates.stream().collect( Collectors.toMap( ReplicatedDatabaseState::databaseId, Function.identity() ) );
    }

    public Map<DatabaseId,ReplicatedDatabaseState> databaseStates()
    {
        return this.databaseStates;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            AllReplicatedDatabaseStates that = (AllReplicatedDatabaseStates) o;
            return Objects.equals( this.databaseStates, that.databaseStates );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.databaseStates} );
    }
}
