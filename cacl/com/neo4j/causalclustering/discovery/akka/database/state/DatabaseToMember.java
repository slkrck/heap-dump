package com.neo4j.causalclustering.discovery.akka.database.state;

import com.neo4j.causalclustering.identity.MemberId;

import java.util.Objects;

import org.neo4j.kernel.database.DatabaseId;

public class DatabaseToMember
{
    private final DatabaseId databaseId;
    private final MemberId memberId;

    public DatabaseToMember( DatabaseId databaseId, MemberId memberId )
    {
        this.databaseId = databaseId;
        this.memberId = memberId;
    }

    public DatabaseId databaseId()
    {
        return this.databaseId;
    }

    public MemberId memberId()
    {
        return this.memberId;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            DatabaseToMember that = (DatabaseToMember) o;
            return Objects.equals( this.databaseId, that.databaseId ) && Objects.equals( this.memberId, that.memberId );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.databaseId, this.memberId} );
    }

    public String toString()
    {
        return "DatabaseToMember{databaseId=" + this.databaseId + ", memberId=" + this.memberId + "}";
    }
}
