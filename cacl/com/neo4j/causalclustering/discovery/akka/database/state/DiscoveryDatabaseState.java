package com.neo4j.causalclustering.discovery.akka.database.state;

import com.neo4j.dbms.EnterpriseOperatorState;

import java.util.Objects;
import java.util.Optional;

import org.neo4j.dbms.DatabaseState;
import org.neo4j.dbms.OperatorState;
import org.neo4j.kernel.database.DatabaseId;

public class DiscoveryDatabaseState
{
    private final DatabaseId databaseId;
    private final OperatorState operatorState;
    private final Throwable failure;

    public DiscoveryDatabaseState( DatabaseId databaseId, OperatorState operatorState )
    {
        this( databaseId, operatorState, (Throwable) null );
    }

    public DiscoveryDatabaseState( DatabaseId databaseId, OperatorState operatorState, Throwable failure )
    {
        this.databaseId = databaseId;
        this.operatorState = operatorState;
        this.failure = failure;
    }

    public static DiscoveryDatabaseState from( DatabaseState databaseState )
    {
        return new DiscoveryDatabaseState( databaseState.databaseId().databaseId(), databaseState.operatorState(),
                (Throwable) databaseState.failure().orElse( (Object) null ) );
    }

    public static DiscoveryDatabaseState unknown( DatabaseId id )
    {
        return new DiscoveryDatabaseState( id, EnterpriseOperatorState.UNKNOWN, (Throwable) null );
    }

    private static String printResult( Throwable failure )
    {
        return failure == null ? "SUCCESS" : "FAIL [" + failure + "]";
    }

    public DatabaseId databaseId()
    {
        return this.databaseId;
    }

    public OperatorState operatorState()
    {
        return this.operatorState;
    }

    public boolean hasFailed()
    {
        return this.failure != null;
    }

    public Optional<Throwable> failure()
    {
        return Optional.ofNullable( this.failure );
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            DiscoveryDatabaseState that = (DiscoveryDatabaseState) o;
            return Objects.equals( this.databaseId, that.databaseId ) && Objects.equals( this.operatorState, that.operatorState ) &&
                    Objects.equals( this.failure, that.failure );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.databaseId, this.operatorState, this.failure} );
    }

    public String toString()
    {
        DatabaseId var10000 = this.databaseId;
        return "DiscoveryDatabaseState{" + var10000 + ", operatorState=" + this.operatorState + ", result=" + printResult( this.failure ) + "}";
    }
}
