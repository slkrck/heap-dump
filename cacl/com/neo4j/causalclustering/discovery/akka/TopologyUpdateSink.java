package com.neo4j.causalclustering.discovery.akka;

import com.neo4j.causalclustering.discovery.DatabaseCoreTopology;
import com.neo4j.causalclustering.discovery.DatabaseReadReplicaTopology;

public interface TopologyUpdateSink
{
    void onTopologyUpdate( DatabaseCoreTopology var1 );

    void onTopologyUpdate( DatabaseReadReplicaTopology var1 );
}
