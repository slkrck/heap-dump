package com.neo4j.causalclustering.discovery.akka;

import com.neo4j.causalclustering.discovery.akka.coretopology.BootstrapState;

public interface BootstrapStateUpdateSink
{
    void onBootstrapStateUpdate( BootstrapState var1 );
}
