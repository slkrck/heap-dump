package com.neo4j.causalclustering.discovery.akka.monitoring;

public interface ClusterSizeMonitor
{
    void setMembers( int var1 );

    void setUnreachable( int var1 );

    void setConverged( boolean var1 );
}
