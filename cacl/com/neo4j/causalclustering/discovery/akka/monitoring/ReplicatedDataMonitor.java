package com.neo4j.causalclustering.discovery.akka.monitoring;

public interface ReplicatedDataMonitor
{
    void setVisibleDataSize( ReplicatedDataIdentifier var1, int var2 );

    void setInvisibleDataSize( ReplicatedDataIdentifier var1, int var2 );
}
