package com.neo4j.causalclustering.discovery.akka.monitoring;

public enum ReplicatedDataIdentifier
{
    METADATA( "member-data" ),
    RAFT_ID( "raft-id-published-by-member" ),
    DIRECTORY( "per-db-leader-name" ),
    DATABASE_STATE( "member-db-state" );

    private final String keyName;

    private ReplicatedDataIdentifier( String keyName )
    {
        this.keyName = keyName;
    }

    public String keyName()
    {
        return this.keyName;
    }
}
