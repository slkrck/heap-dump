package com.neo4j.causalclustering.discovery.akka.system;

import akka.actor.Address;
import akka.actor.Props;
import akka.actor.AbstractActor.Receive;
import akka.cluster.Cluster;
import akka.japi.pf.ReceiveBuilder;
import com.neo4j.causalclustering.core.CausalClusteringSettings;
import com.neo4j.causalclustering.discovery.RemoteMembersResolver;
import com.neo4j.causalclustering.discovery.akka.AbstractActorWithTimersAndLogging;

import java.time.Duration;
import java.util.ArrayList;

import org.neo4j.configuration.Config;
import org.neo4j.configuration.helpers.SocketAddress;

public class ClusterJoiningActor extends AbstractActorWithTimersAndLogging
{
    public static final String NAME = "joiner";
    static final String AKKA_SCHEME = "akka";
    private static final String TIMER = "join timer";
    private final Cluster cluster;
    private final RemoteMembersResolver remoteMembersResolver;
    private final Duration retry;

    private ClusterJoiningActor( Cluster cluster, RemoteMembersResolver remoteMembersResolver, Config config )
    {
        this.cluster = cluster;
        this.remoteMembersResolver = remoteMembersResolver;
        this.retry = (Duration) config.get( CausalClusteringSettings.cluster_binding_retry_timeout );
    }

    public static Props props( Cluster cluster, RemoteMembersResolver resolver, Config config )
    {
        return Props.create( ClusterJoiningActor.class, () -> {
            return new ClusterJoiningActor( cluster, resolver, config );
        } );
    }

    public void preStart()
    {
        this.cluster.registerOnMemberUp( () -> {
            this.log().debug( "Join successful, exiting" );
            this.getContext().stop( this.getSelf() );
        } );
    }

    public Receive createReceive()
    {
        return ReceiveBuilder.create().match( JoinMessage.class, this::join ).build();
    }

    private void join( JoinMessage message )
    {
        this.log().debug( "Processing: {}", message );
        ArrayList seedNodes;
        if ( !message.isReJoin() )
        {
            seedNodes = this.resolve();
            this.log().info( "Joining seed nodes: {}", seedNodes );
            this.cluster.joinSeedNodes( seedNodes );
            this.startTimer( message );
        }
        else if ( !message.hasAddress() )
        {
            seedNodes = this.resolve();
            this.getSelf().tell( JoinMessage.initial( message.isReJoin(), seedNodes ), this.getSelf() );
        }
        else if ( message.head().equals( this.cluster.selfAddress() ) )
        {
            this.log().info( "Not joining to self. Retrying next." );
            this.getSelf().tell( message.tailMsg(), this.getSelf() );
        }
        else
        {
            Address address = message.head();
            this.log().info( "Attempting to join: {}", address );
            this.cluster.join( address );
            this.startTimer( message.tailMsg() );
        }
    }

    private ArrayList<Address> resolve()
    {
        return (ArrayList) this.remoteMembersResolver.resolve( this::toAkkaAddress, ArrayList::new );
    }

    private void startTimer( JoinMessage message )
    {
        this.getTimers().startSingleTimer( "join timer", message, this.retry );
    }

    private Address toAkkaAddress( SocketAddress resolvedAddress )
    {
        return new Address( "akka", this.getContext().getSystem().name(), TypesafeConfigService.hostname( resolvedAddress ), resolvedAddress.getPort() );
    }
}
