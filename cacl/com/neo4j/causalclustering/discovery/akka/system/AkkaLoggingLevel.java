package com.neo4j.causalclustering.discovery.akka.system;

import org.neo4j.logging.Level;

enum AkkaLoggingLevel
{
    DEBUG( Level.DEBUG ),
    INFO( Level.INFO ),
    WARNING( Level.WARN ),
    ERROR( Level.ERROR ),
    OFF( Level.NONE );

    private final Level neo4jLevel;

    private AkkaLoggingLevel( Level neo4jLevel )
    {
        this.neo4jLevel = neo4jLevel;
    }

    static AkkaLoggingLevel fromNeo4jLevel( Level neo4jLevel )
    {
        AkkaLoggingLevel[] var1 = values();
        int var2 = var1.length;

        for ( int var3 = 0; var3 < var2; ++var3 )
        {
            AkkaLoggingLevel akkaLevel = var1[var3];
            if ( akkaLevel.neo4jLevel == neo4jLevel )
            {
                return akkaLevel;
            }
        }

        throw new IllegalArgumentException( "Unknown Neo4j logging level: " + neo4jLevel );
    }

    public Level neo4jLevel()
    {
        return this.neo4jLevel;
    }
}
