package com.neo4j.causalclustering.discovery.akka.system;

import akka.actor.Address;
import com.neo4j.causalclustering.discovery.RemoteMembersResolver;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class JoinMessageFactory
{
    private final RemoteMembersResolver resolver;
    private final Set<Address> allSeenAddresses = new HashSet();
    private boolean isReJoin;

    public JoinMessageFactory( RemoteMembersResolver resolver )
    {
        this.resolver = resolver;
    }

    JoinMessage message()
    {
        JoinMessage message = JoinMessage.initial( this.isReJoin, this.allSeenAddresses );
        this.allSeenAddresses.clear();
        return message;
    }

    void addSeenAddresses( Collection<Address> addresses )
    {
        this.isReJoin = true;
        if ( this.resolver.useOverrides() )
        {
            this.allSeenAddresses.addAll( addresses );
        }
    }
}
