package com.neo4j.causalclustering.discovery.akka.system;

import akka.actor.ActorSystem;
import akka.actor.BootstrapSetup;
import akka.actor.ProviderSelection;
import akka.actor.setup.ActorSystemSetup;
import akka.actor.setup.Setup;
import akka.dispatch.ExecutionContexts;
import akka.remote.artery.tcp.SSLEngineProvider;
import akka.remote.artery.tcp.SSLEngineProviderSetup;
import com.typesafe.config.ConfigObject;
import com.typesafe.config.ConfigRenderOptions;

import java.util.Optional;
import java.util.concurrent.Executor;

import org.neo4j.configuration.Config;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import scala.concurrent.ExecutionContextExecutor;

public class ActorSystemFactory
{
    public static final String ACTOR_SYSTEM_NAME = "cc-discovery-actor-system";
    private final LogProvider logProvider;
    private final Optional<SSLEngineProvider> sslEngineProvider;
    private final TypesafeConfigService configService;
    private final Executor executor;
    private final Log log;

    public ActorSystemFactory( Optional<SSLEngineProvider> sslEngineProvider, Executor executor, Config config, LogProvider logProvider )
    {
        this.executor = executor;
        this.logProvider = logProvider;
        this.sslEngineProvider = sslEngineProvider;
        TypesafeConfigService.ArteryTransport arteryTransport =
                sslEngineProvider.isPresent() ? TypesafeConfigService.ArteryTransport.TLS_TCP : TypesafeConfigService.ArteryTransport.TCP;
        this.configService = new TypesafeConfigService( arteryTransport, config );
        this.log = logProvider.getLog( this.getClass() );
    }

    ActorSystem createActorSystem( ProviderSelection providerSelection )
    {
        com.typesafe.config.Config tsConfig = this.configService.generate();
        Log var10000 = this.log;
        ConfigObject var10001 = tsConfig.root();
        var10000.debug( "Akka config: " + var10001.render( ConfigRenderOptions.concise() ) );
        ExecutionContextExecutor ec = ExecutionContexts.fromExecutor( this.executor );
        BootstrapSetup bootstrapSetup = BootstrapSetup.create( tsConfig ).withActorRefProvider( providerSelection ).withDefaultExecutionContext( ec );
        ActorSystemSetup actorSystemSetup = ActorSystemSetup.create( new Setup[]{bootstrapSetup} );
        if ( this.sslEngineProvider.isPresent() )
        {
            actorSystemSetup = actorSystemSetup.withSetup( SSLEngineProviderSetup.create( ( system ) -> {
                return (SSLEngineProvider) this.sslEngineProvider.get();
            } ) );
        }

        LoggingFilter.enable( this.logProvider );
        LoggingActor.enable( this.logProvider );
        ActorSystem actorSystem = ActorSystem.create( "cc-discovery-actor-system", actorSystemSetup );
        LoggingActor.enable( actorSystem, this.logProvider );
        return actorSystem;
    }
}
