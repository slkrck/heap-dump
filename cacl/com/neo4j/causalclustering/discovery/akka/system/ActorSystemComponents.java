package com.neo4j.causalclustering.discovery.akka.system;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.CoordinatedShutdown;
import akka.actor.ProviderSelection;
import akka.cluster.Cluster;
import akka.cluster.client.ClusterClientReceptionist;
import akka.cluster.ddata.DistributedData;
import akka.stream.ActorMaterializer;
import akka.stream.ActorMaterializerSettings;

public class ActorSystemComponents
{
    private final ActorSystem actorSystem;
    private Cluster cluster;
    private ActorRef replicator;
    private ActorMaterializer materializer;
    private ClusterClientReceptionist clusterClientReceptionist;
    private CoordinatedShutdown coordinatedShutdown;

    public ActorSystemComponents( ActorSystemFactory actorSystemFactory, ProviderSelection providerSelection )
    {
        this.actorSystem = actorSystemFactory.createActorSystem( providerSelection );
        this.coordinatedShutdown = CoordinatedShutdown.get( this.actorSystem );
    }

    public ActorSystem actorSystem()
    {
        return this.actorSystem;
    }

    public Cluster cluster()
    {
        if ( this.cluster == null )
        {
            this.cluster = Cluster.get( this.actorSystem );
        }

        return this.cluster;
    }

    public ActorRef replicator()
    {
        if ( this.replicator == null )
        {
            this.replicator = DistributedData.get( this.actorSystem ).replicator();
        }

        return this.replicator;
    }

    ClusterClientReceptionist clusterClientReceptionist()
    {
        if ( this.clusterClientReceptionist == null )
        {
            this.clusterClientReceptionist = ClusterClientReceptionist.get( this.actorSystem );
        }

        return this.clusterClientReceptionist;
    }

    ActorMaterializer materializer()
    {
        if ( this.materializer == null )
        {
            ActorMaterializerSettings settings = ActorMaterializerSettings.create( this.actorSystem ).withDispatcher( "discovery-dispatcher" );
            this.materializer = ActorMaterializer.create( settings, this.actorSystem );
        }

        return this.materializer;
    }

    public CoordinatedShutdown coordinatedShutdown()
    {
        return this.coordinatedShutdown;
    }
}
