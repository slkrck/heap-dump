package com.neo4j.causalclustering.discovery.akka.system;

import akka.actor.Address;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

class JoinMessage
{
    private final boolean isReJoin;
    private final List<Address> addresses;

    private JoinMessage( boolean isReJoin, List<Address> addresses )
    {
        this.isReJoin = isReJoin;
        this.addresses = Collections.unmodifiableList( addresses );
    }

    static JoinMessage initial( boolean isReJoin, Collection<Address> addresses )
    {
        return new JoinMessage( isReJoin, new ArrayList( addresses ) );
    }

    JoinMessage tailMsg()
    {
        return new JoinMessage( this.isReJoin, this.addresses.subList( 1, this.addresses.size() ) );
    }

    boolean hasAddress()
    {
        return !this.addresses.isEmpty();
    }

    Address head()
    {
        return (Address) this.addresses.get( 0 );
    }

    boolean isReJoin()
    {
        return this.isReJoin;
    }

    public String toString()
    {
        return "JoinMessage{isReJoin=" + this.isReJoin + ", addresses=" + this.addresses + "}";
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            JoinMessage that = (JoinMessage) o;
            return this.isReJoin == that.isReJoin && Objects.equals( this.addresses, that.addresses );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.isReJoin, this.addresses} );
    }
}
