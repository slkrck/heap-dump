package com.neo4j.causalclustering.discovery.akka.system;

import akka.actor.AbstractActor;
import akka.actor.ActorSystem;
import akka.actor.AbstractActor.Receive;
import akka.dispatch.RequiresMessageQueue;
import akka.event.LoggerMessageQueueSemantics;
import akka.event.Logging;
import akka.event.Logging.Debug;
import akka.event.Logging.Error;
import akka.event.Logging.Info;
import akka.event.Logging.InitializeLogger;
import akka.event.Logging.LogEvent;
import akka.event.Logging.Warning;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.logging.NullLogProvider;

public class LoggingActor extends AbstractActor implements RequiresMessageQueue<LoggerMessageQueueSemantics>
{
    private static final Map<ActorSystem,LogProvider> LOG_PROVIDERS = new HashMap();
    private static LogProvider defaultLogProvider = NullLogProvider.getInstance();

    static void enable( ActorSystem system, LogProvider logProvider )
    {
        LOG_PROVIDERS.put( system, logProvider );
        logProvider.getLog( LoggingActor.class ).debug( "Added logProvider for %s. %d LogProviders and ActorSystems remaining.",
                new Object[]{system.name(), LOG_PROVIDERS.size()} );
    }

    static void enable( LogProvider logProvider )
    {
        defaultLogProvider = logProvider;
    }

    static void disable( ActorSystem system )
    {
        Optional<LogProvider> removed = Optional.ofNullable( (LogProvider) LOG_PROVIDERS.remove( system ) );
        removed.ifPresent( ( log ) -> {
            log.getLog( LoggingActor.class ).debug( "Removed logProvider for %s. %d LogProviders and ActorSystems remaining.",
                    new Object[]{system.name(), LOG_PROVIDERS.size()} );
        } );
    }

    public Receive createReceive()
    {
        return this.receiveBuilder().match( Error.class, ( error ) -> {
            this.getLog( error.logClass() ).error( this.getMessage( error ), error.cause() );
        } ).match( Warning.class, ( warning ) -> {
            this.getLog( warning.logClass() ).warn( this.getMessage( warning ) );
        } ).match( Info.class, ( info ) -> {
            this.getLog( info.logClass() ).info( this.getMessage( info ) );
        } ).match( Debug.class, ( debug ) -> {
            this.getLog( debug.logClass() ).debug( this.getMessage( debug ) );
        } ).match( InitializeLogger.class, ( ignored ) -> {
            this.sender().tell( Logging.loggerInitialized(), this.self() );
        } ).build();
    }

    private Log getLog( Class loggingClass )
    {
        LogProvider configuredLogProvider = (LogProvider) LOG_PROVIDERS.get( this.getContext().system() );
        LogProvider logProvider = configuredLogProvider == null ? defaultLogProvider : configuredLogProvider;
        return logProvider.getLog( loggingClass );
    }

    private String getMessage( LogEvent error )
    {
        return error.message() == null ? "null" : error.message().toString();
    }
}
