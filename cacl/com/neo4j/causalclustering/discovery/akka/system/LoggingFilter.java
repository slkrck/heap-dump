package com.neo4j.causalclustering.discovery.akka.system;

import akka.actor.ActorSystem.Settings;
import akka.event.DefaultLoggingFilter;
import akka.event.EventStream;
import org.neo4j.logging.LogProvider;
import org.neo4j.logging.NullLogProvider;

public class LoggingFilter implements akka.event.LoggingFilter
{
    private static LogProvider logProvider = NullLogProvider.getInstance();
    private final DefaultLoggingFilter defaultLoggingFilter;

    LoggingFilter( Settings settings, EventStream eventStream )
    {
        this.defaultLoggingFilter = new DefaultLoggingFilter( settings, eventStream );
    }

    static void enable( LogProvider logProvider )
    {
        LoggingFilter.logProvider = logProvider;
    }

    public boolean isErrorEnabled( Class<?> logClass, String logSource )
    {
        return this.defaultLoggingFilter.isErrorEnabled( logClass, logSource );
    }

    public boolean isWarningEnabled( Class<?> logClass, String logSource )
    {
        return this.defaultLoggingFilter.isWarningEnabled( logClass, logSource );
    }

    public boolean isInfoEnabled( Class<?> logClass, String logSource )
    {
        return this.defaultLoggingFilter.isInfoEnabled( logClass, logSource );
    }

    public boolean isDebugEnabled( Class<?> logClass, String logSource )
    {
        return this.defaultLoggingFilter.isDebugEnabled( logClass, logSource ) && logProvider.getLog( logClass ).isDebugEnabled();
    }
}
