package com.neo4j.causalclustering.discovery.akka.coretopology;

import akka.cluster.UniqueAddress;
import akka.cluster.ddata.LWWMap;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import org.neo4j.util.VisibleForTesting;

public class MetadataMessage
{
    public static final MetadataMessage EMPTY = new MetadataMessage( Collections.emptyMap() );
    private final Map<UniqueAddress,CoreServerInfoForMemberId> metadata;

    public MetadataMessage( LWWMap<UniqueAddress,CoreServerInfoForMemberId> metadata )
    {
        this( metadata.getEntries() );
    }

    @VisibleForTesting
    public MetadataMessage( Map<UniqueAddress,CoreServerInfoForMemberId> metadata )
    {
        this.metadata = Collections.unmodifiableMap( metadata );
    }

    public Optional<CoreServerInfoForMemberId> getOpt( UniqueAddress address )
    {
        return Optional.ofNullable( (CoreServerInfoForMemberId) this.metadata.get( address ) );
    }

    public Stream<CoreServerInfoForMemberId> getStream()
    {
        return this.metadata.values().stream();
    }

    public Stream<CoreServerInfoForMemberId> getStream( UniqueAddress address )
    {
        return Stream.ofNullable( (CoreServerInfoForMemberId) this.metadata.get( address ) );
    }

    public String toString()
    {
        return "MetadataMessage{metadata=" + this.metadata + "}";
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            MetadataMessage that = (MetadataMessage) o;
            return Objects.equals( this.metadata, that.metadata );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.metadata} );
    }
}
