package com.neo4j.causalclustering.discovery.akka.coretopology;

import com.neo4j.causalclustering.identity.RaftId;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;

public class BootstrappedRaftsMessage
{
    public static final BootstrappedRaftsMessage EMPTY = new BootstrappedRaftsMessage( Collections.emptySet() );
    private final Set<RaftId> bootstrappedRafts;

    public BootstrappedRaftsMessage( Set<RaftId> bootstrappedRafts )
    {
        this.bootstrappedRafts = Set.copyOf( bootstrappedRafts );
    }

    public Set<RaftId> bootstrappedRafts()
    {
        return this.bootstrappedRafts;
    }

    public String toString()
    {
        return "BootstrappedRaftsMessage{bootstrappedRafts=" + this.bootstrappedRafts + "}";
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            BootstrappedRaftsMessage that = (BootstrappedRaftsMessage) o;
            return Objects.equals( this.bootstrappedRafts, that.bootstrappedRafts );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.bootstrappedRafts} );
    }
}
