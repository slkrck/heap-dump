package com.neo4j.causalclustering.discovery.akka.coretopology;

import com.neo4j.causalclustering.discovery.CoreServerInfo;
import com.neo4j.causalclustering.discovery.DatabaseCoreTopology;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.identity.RaftId;

import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Nullable;

import org.neo4j.kernel.database.DatabaseId;

public class TopologyBuilder
{
    DatabaseCoreTopology buildCoreTopology( DatabaseId databaseId, @Nullable RaftId raftId, ClusterViewMessage cluster, MetadataMessage memberData )
    {
        Stream var10000 = cluster.availableMembers();
        Objects.requireNonNull( memberData );
        Map<MemberId,CoreServerInfo> coreMembers = (Map) var10000.flatMap( memberData::getStream ).filter( ( member ) -> {
            return member.coreServerInfo().startedDatabaseIds().contains( databaseId );
        } ).collect( Collectors.toMap( CoreServerInfoForMemberId::memberId, CoreServerInfoForMemberId::coreServerInfo ) );
        return new DatabaseCoreTopology( databaseId, raftId, coreMembers );
    }
}
