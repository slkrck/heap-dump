package com.neo4j.causalclustering.discovery.akka.coretopology;

import com.neo4j.causalclustering.discovery.CoreServerInfo;
import com.neo4j.causalclustering.identity.MemberId;

import java.util.Objects;

public class CoreServerInfoForMemberId
{
    private final MemberId memberId;
    private final CoreServerInfo coreServerInfo;

    public CoreServerInfoForMemberId( MemberId memberId, CoreServerInfo coreServerInfo )
    {
        this.memberId = memberId;
        this.coreServerInfo = coreServerInfo;
    }

    public MemberId memberId()
    {
        return this.memberId;
    }

    public CoreServerInfo coreServerInfo()
    {
        return this.coreServerInfo;
    }

    public String toString()
    {
        return "CoreServerInfoForMemberId{memberId=" + this.memberId + ", coreServerInfo=" + this.coreServerInfo + "}";
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            CoreServerInfoForMemberId that = (CoreServerInfoForMemberId) o;
            return Objects.equals( this.memberId, that.memberId ) && Objects.equals( this.coreServerInfo, that.coreServerInfo );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.memberId, this.coreServerInfo} );
    }
}
