package com.neo4j.causalclustering.discovery.akka.coretopology;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor.Receive;
import akka.cluster.Cluster;
import akka.cluster.ClusterEvent;
import akka.cluster.Member;
import akka.cluster.ClusterEvent.ClusterDomainEvent;
import akka.cluster.ClusterEvent.CurrentClusterState;
import akka.cluster.ClusterEvent.LeaderChanged;
import akka.cluster.ClusterEvent.MemberRemoved;
import akka.cluster.ClusterEvent.MemberUp;
import akka.cluster.ClusterEvent.MemberWeaklyUp;
import akka.cluster.ClusterEvent.ReachableMember;
import akka.cluster.ClusterEvent.UnreachableMember;
import akka.japi.pf.ReceiveBuilder;
import com.neo4j.causalclustering.core.CausalClusteringSettings;
import com.neo4j.causalclustering.discovery.akka.AbstractActorWithTimersAndLogging;
import com.neo4j.causalclustering.discovery.akka.monitoring.ClusterSizeMonitor;

import java.time.Duration;

import org.neo4j.configuration.Config;

public class ClusterStateActor extends AbstractActorWithTimersAndLogging
{
    private static final String DOWNING_TIMER_KEY = "DOWNING_TIMER_KEY key";
    private static final String MONITOR_TICK_KEY = "MONITOR_TICK_KEY tick";
    private final Cluster cluster;
    private final ActorRef topologyActor;
    private final ActorRef downingActor;
    private final ActorRef metadataActor;
    private final Duration clusterStabilityWait;
    private final ClusterSizeMonitor monitor;
    private ClusterViewMessage clusterView;

    public ClusterStateActor( Cluster cluster, ActorRef topologyActor, ActorRef downingActor, ActorRef metadataActor, Config config,
            ClusterSizeMonitor monitor )
    {
        this.clusterView = ClusterViewMessage.EMPTY;
        this.cluster = cluster;
        this.topologyActor = topologyActor;
        this.downingActor = downingActor;
        this.metadataActor = metadataActor;
        this.monitor = monitor;
        this.clusterStabilityWait = ((Duration) config.get( CausalClusteringSettings.akka_failure_detector_heartbeat_interval )).plus(
                (Duration) config.get( CausalClusteringSettings.akka_failure_detector_acceptable_heartbeat_pause ) );
    }

    static Props props( Cluster cluster, ActorRef topologyActor, ActorRef downingActor, ActorRef metadataActor, Config config, ClusterSizeMonitor monitor )
    {
        return Props.create( ClusterStateActor.class, () -> {
            return new ClusterStateActor( cluster, topologyActor, downingActor, metadataActor, config, monitor );
        } );
    }

    public void preStart()
    {
        this.cluster.subscribe( this.getSelf(), ClusterEvent.initialStateAsSnapshot(), new Class[]{ClusterDomainEvent.class, UnreachableMember.class} );
        this.getTimers().startPeriodicTimer( "MONITOR_TICK_KEY tick", ClusterStateActor.ClusterMonitorRefresh.INSTANCE, Duration.ofMinutes( 1L ) );
    }

    public void postStop()
    {
        this.cluster.unsubscribe( this.getSelf() );
    }

    public Receive createReceive()
    {
        return ReceiveBuilder.create().match( CurrentClusterState.class, this::handleCurrentClusterState ).match( ReachableMember.class,
                this::handleReachableMember ).match( UnreachableMember.class, this::handleUnreachableMember ).match( MemberUp.class,
                this::handleMemberUp ).match( MemberWeaklyUp.class, this::handleMemberWeaklyUp ).match( MemberRemoved.class, this::handleMemberRemoved ).match(
                LeaderChanged.class, this::handleLeaderChanged ).match( ClusterDomainEvent.class, this::handleOtherClusterEvent ).match(
                ClusterStateActor.StabilityMessage.class, this::notifyDowningActor ).match( ClusterStateActor.ClusterMonitorRefresh.class, ( ignored ) -> {
            this.updateMonitor();
        } ).build();
    }

    private void handleCurrentClusterState( CurrentClusterState event )
    {
        this.clusterView = new ClusterViewMessage( event );
        this.log().debug( "Akka initial cluster state {}", event );
        this.sendClusterView();
    }

    private void handleReachableMember( ReachableMember event )
    {
        this.log().debug( "Akka cluster event {}", event );
        this.clusterView = this.clusterView.withoutUnreachable( event.member() );
        this.sendClusterView();
    }

    private void handleUnreachableMember( UnreachableMember event )
    {
        this.log().debug( "Akka cluster event {}", event );
        this.clusterView = this.clusterView.withUnreachable( event.member() );
        this.sendClusterView();
    }

    private void handleMemberUp( MemberUp event )
    {
        this.log().debug( "Akka cluster event {}", event );
        this.clusterView = this.clusterView.withMember( event.member() );
        this.sendClusterView();
    }

    private void handleMemberWeaklyUp( MemberWeaklyUp event )
    {
        this.log().debug( "Akka cluster event {}", event );
        this.clusterView = this.clusterView.withMember( event.member() );
        this.sendClusterView();
    }

    private void handleMemberRemoved( MemberRemoved event )
    {
        this.log().debug( "Akka cluster event {}", event );
        Member member = event.member();
        this.clusterView = this.clusterView.withoutMember( member );
        this.sendClusterView();
        this.metadataActor.tell( new CleanupMessage( member.uniqueAddress() ), this.getSelf() );
    }

    private void handleLeaderChanged( LeaderChanged event )
    {
        this.log().debug( "Akka cluster event {}", event );
        this.clusterView = this.clusterView.withConverged( event.leader().isDefined() );
        this.sendClusterView();
    }

    private void handleOtherClusterEvent( ClusterDomainEvent event )
    {
        this.log().debug( "Ignoring Akka cluster event {}", event );
        this.resetDowningTimer();
    }

    private void notifyDowningActor( ClusterStateActor.StabilityMessage ignored )
    {
        this.log().debug( "Cluster is stable at {}", this.clusterView );
        this.downingActor.tell( this.clusterView, this.getSelf() );
    }

    private void sendClusterView()
    {
        this.topologyActor.tell( this.clusterView, this.getSelf() );
        this.updateMonitor();
        this.resetDowningTimer();
    }

    private void resetDowningTimer()
    {
        this.timers().startSingleTimer( "DOWNING_TIMER_KEY key", ClusterStateActor.StabilityMessage.INSTANCE, this.clusterStabilityWait );
    }

    private void updateMonitor()
    {
        this.monitor.setMembers( this.clusterView.members().size() );
        this.monitor.setUnreachable( this.clusterView.unreachable().size() );
        this.monitor.setConverged( this.clusterView.converged() );
    }

    static class ClusterMonitorRefresh
    {
        static final ClusterStateActor.ClusterMonitorRefresh INSTANCE = new ClusterStateActor.ClusterMonitorRefresh();

        private ClusterMonitorRefresh()
        {
        }
    }

    private static class StabilityMessage
    {
        static final ClusterStateActor.StabilityMessage INSTANCE = new ClusterStateActor.StabilityMessage();
    }
}
