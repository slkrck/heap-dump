package com.neo4j.causalclustering.discovery.akka.coretopology;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.cluster.Cluster;
import akka.cluster.UniqueAddress;
import akka.cluster.ddata.LWWMap;
import akka.cluster.ddata.LWWMapKey;
import akka.japi.pf.ReceiveBuilder;
import com.neo4j.causalclustering.discovery.CoreServerInfo;
import com.neo4j.causalclustering.discovery.akka.BaseReplicatedDataActor;
import com.neo4j.causalclustering.discovery.akka.common.DatabaseStartedMessage;
import com.neo4j.causalclustering.discovery.akka.common.DatabaseStoppedMessage;
import com.neo4j.causalclustering.discovery.akka.monitoring.ReplicatedDataIdentifier;
import com.neo4j.causalclustering.discovery.akka.monitoring.ReplicatedDataMonitor;
import com.neo4j.causalclustering.discovery.member.DiscoveryMember;

import java.util.HashSet;
import java.util.Set;

import org.neo4j.configuration.Config;
import org.neo4j.kernel.database.DatabaseId;

public class MetadataActor extends BaseReplicatedDataActor<LWWMap<UniqueAddress,CoreServerInfoForMemberId>>
{
    private final DiscoveryMember myself;
    private final ActorRef topologyActor;
    private final Config config;
    private final Set<DatabaseId> startedDatabases = new HashSet();

    private MetadataActor( DiscoveryMember myself, Cluster cluster, ActorRef replicator, ActorRef topologyActor, Config config, ReplicatedDataMonitor monitor )
    {
        super( cluster, replicator, LWWMapKey::create, LWWMap::empty, ReplicatedDataIdentifier.METADATA, monitor );
        this.myself = myself;
        this.topologyActor = topologyActor;
        this.config = config;
    }

    static Props props( DiscoveryMember myself, Cluster cluster, ActorRef replicator, ActorRef topologyActor, Config config, ReplicatedDataMonitor monitor )
    {
        return Props.create( MetadataActor.class, () -> {
            return new MetadataActor( myself, cluster, replicator, topologyActor, config, monitor );
        } );
    }

    protected void handleCustomEvents( ReceiveBuilder builder )
    {
        builder.match( CleanupMessage.class, this::removeDataFromReplicator ).match( DatabaseStartedMessage.class, this::handleDatabaseStartedMessage ).match(
                DatabaseStoppedMessage.class, this::handleDatabaseStoppedMessage );
    }

    private void handleDatabaseStartedMessage( DatabaseStartedMessage message )
    {
        if ( this.startedDatabases.add( message.namedDatabaseId().databaseId() ) )
        {
            this.sendCoreServerInfo();
        }
    }

    private void handleDatabaseStoppedMessage( DatabaseStoppedMessage message )
    {
        if ( this.startedDatabases.remove( message.namedDatabaseId().databaseId() ) )
        {
            this.sendCoreServerInfo();
        }
    }

    public void sendInitialDataToReplicator()
    {
        this.startedDatabases.addAll( this.myself.startedDatabases() );
        this.sendCoreServerInfo();
    }

    private void removeDataFromReplicator( CleanupMessage message )
    {
        this.modifyReplicatedData( this.key, ( map ) -> {
            return map.remove( this.cluster, message.uniqueAddress() );
        } );
    }

    protected void handleIncomingData( LWWMap<UniqueAddress,CoreServerInfoForMemberId> newData )
    {
        this.data = newData;
        this.topologyActor.tell( new MetadataMessage( (LWWMap) this.data ), this.getSelf() );
    }

    protected int dataMetricVisible()
    {
        return ((LWWMap) this.data).size();
    }

    protected int dataMetricInvisible()
    {
        return ((LWWMap) this.data).underlying().keys().vvector().size();
    }

    private void sendCoreServerInfo()
    {
        Set<DatabaseId> startedDatabases = Set.copyOf( this.startedDatabases );
        CoreServerInfo info = CoreServerInfo.from( this.config, startedDatabases );
        CoreServerInfoForMemberId metadata = new CoreServerInfoForMemberId( this.myself.id(), info );
        this.modifyReplicatedData( this.key, ( map ) -> {
            return map.put( this.cluster, this.cluster.selfUniqueAddress(), metadata );
        } );
    }
}
