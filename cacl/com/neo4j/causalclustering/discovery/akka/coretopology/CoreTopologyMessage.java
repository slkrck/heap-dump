package com.neo4j.causalclustering.discovery.akka.coretopology;

import akka.actor.Address;
import com.neo4j.causalclustering.discovery.DatabaseCoreTopology;

import java.util.Collection;
import java.util.Objects;

public class CoreTopologyMessage
{
    private final DatabaseCoreTopology coreTopology;
    private final Collection<Address> akkaMembers;

    public CoreTopologyMessage( DatabaseCoreTopology coreTopology, Collection<Address> akkaMembers )
    {
        this.coreTopology = coreTopology;
        this.akkaMembers = akkaMembers;
    }

    public DatabaseCoreTopology coreTopology()
    {
        return this.coreTopology;
    }

    public Collection<Address> akkaMembers()
    {
        return this.akkaMembers;
    }

    public String toString()
    {
        return "CoreTopologyMessage{coreTopology=" + this.coreTopology + ", akkaMembers=" + this.akkaMembers + "}";
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            CoreTopologyMessage that = (CoreTopologyMessage) o;
            return Objects.equals( this.coreTopology, that.coreTopology ) && Objects.equals( this.akkaMembers, that.akkaMembers );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.coreTopology, this.akkaMembers} );
    }
}
