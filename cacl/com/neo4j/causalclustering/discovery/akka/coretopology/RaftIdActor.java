package com.neo4j.causalclustering.discovery.akka.coretopology;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.cluster.Cluster;
import akka.cluster.ddata.LWWMap;
import akka.cluster.ddata.LWWMapKey;
import akka.cluster.ddata.LWWRegister;
import akka.cluster.ddata.LWWRegister.Clock;
import akka.cluster.ddata.Replicator.Get;
import akka.cluster.ddata.Replicator.GetSuccess;
import akka.cluster.ddata.Replicator.ReadConsistency;
import akka.cluster.ddata.Replicator.ReadFrom;
import akka.cluster.ddata.Replicator.UpdateFailure;
import akka.cluster.ddata.Replicator.UpdateSuccess;
import akka.japi.pf.ReceiveBuilder;
import com.neo4j.causalclustering.discovery.PublishRaftIdOutcome;
import com.neo4j.causalclustering.discovery.akka.BaseReplicatedDataActor;
import com.neo4j.causalclustering.discovery.akka.monitoring.ReplicatedDataIdentifier;
import com.neo4j.causalclustering.discovery.akka.monitoring.ReplicatedDataMonitor;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.identity.RaftId;

import java.util.Objects;
import java.util.Optional;

public class RaftIdActor extends BaseReplicatedDataActor<LWWMap<RaftId,MemberId>>
{
    private final ActorRef coreTopologyActor;
    private final Clock<MemberId> clock = LWWRegister.reverseClock();
    private final int minRuntimeQuorumSize;

    RaftIdActor( Cluster cluster, ActorRef replicator, ActorRef coreTopologyActor, ReplicatedDataMonitor monitors, int minRuntimeCores )
    {
        super( cluster, replicator, LWWMapKey::create, LWWMap::create, ReplicatedDataIdentifier.RAFT_ID, monitors );
        this.coreTopologyActor = coreTopologyActor;
        this.minRuntimeQuorumSize = minRuntimeCores / 2 + 1;
    }

    public static Props props( Cluster cluster, ActorRef replicator, ActorRef coreTopologyActor, ReplicatedDataMonitor monitors, int minRuntimeCores )
    {
        return Props.create( RaftIdActor.class, () -> {
            return new RaftIdActor( cluster, replicator, coreTopologyActor, monitors, minRuntimeCores );
        } );
    }

    protected void sendInitialDataToReplicator()
    {
    }

    protected void handleCustomEvents( ReceiveBuilder builder )
    {
        builder.match( RaftIdSetRequest.class, this::setRaftId ).match( UpdateSuccess.class, this::handleUpdateSuccess ).match( GetSuccess.class,
                this::validateRaftIdUpdate ).match( UpdateFailure.class, this::handleUpdateFailure );
    }

    private void setRaftId( RaftIdSetRequest message )
    {
        this.log().debug( "Setting RaftId: {}", message );
        this.modifyReplicatedData( this.key, ( map ) -> {
            return map.put( this.cluster, message.raftId(), message.publisher(), this.clock );
        }, message.withReplyTo( this.getSender() ) );
    }

    private void handleUpdateSuccess( UpdateSuccess<?> updateSuccess )
    {
        updateSuccess.getRequest().filter( ( m ) -> {
            return m instanceof RaftIdSetRequest;
        } ).map( ( m ) -> {
            return (RaftIdSetRequest) m;
        } ).ifPresent( ( m ) -> {
            ReadConsistency readConsistency = new ReadFrom( this.minRuntimeQuorumSize, m.timeout() );
            Get<LWWMap<RaftId,MemberId>> getOp = new Get( this.key, readConsistency, Optional.of( m ) );
            this.replicator.tell( getOp, this.getSelf() );
        } );
    }

    private void validateRaftIdUpdate( GetSuccess<LWWMap<RaftId,MemberId>> getSuccess )
    {
        LWWMap<RaftId,MemberId> current = (LWWMap) getSuccess.get( this.key );
        getSuccess.getRequest().filter( ( m ) -> {
            return m instanceof RaftIdSetRequest;
        } ).map( ( m ) -> {
            return (RaftIdSetRequest) m;
        } ).ifPresent( ( request ) -> {
            MemberId successfulPublisher = (MemberId) current.getEntries().get( request.raftId() );
            PublishRaftIdOutcome outcome;
            if ( successfulPublisher == null )
            {
                outcome = PublishRaftIdOutcome.FAILED_PUBLISH;
            }
            else if ( Objects.equals( successfulPublisher, request.publisher() ) )
            {
                outcome = PublishRaftIdOutcome.SUCCESSFUL_PUBLISH_BY_ME;
            }
            else
            {
                outcome = PublishRaftIdOutcome.SUCCESSFUL_PUBLISH_BY_OTHER;
            }

            request.replyTo().tell( outcome, this.getSelf() );
        } );
    }

    private void handleUpdateFailure( UpdateFailure<?> updateFailure )
    {
        updateFailure.getRequest().filter( ( m ) -> {
            return m instanceof RaftIdSetRequest;
        } ).map( ( m ) -> {
            return (RaftIdSetRequest) m;
        } ).ifPresent( ( request ) -> {
            String message = String.format( "Failed to set RaftId with request: %s", request );
            this.log().warning( message );
            request.replyTo().tell( PublishRaftIdOutcome.FAILED_PUBLISH, this.getSelf() );
        } );
    }

    protected void handleIncomingData( LWWMap<RaftId,MemberId> newData )
    {
        this.data = newData;
        this.coreTopologyActor.tell( new BootstrappedRaftsMessage( ((LWWMap) this.data).getEntries().keySet() ), this.getSelf() );
    }

    protected int dataMetricVisible()
    {
        return ((LWWMap) this.data).size();
    }

    protected int dataMetricInvisible()
    {
        return ((LWWMap) this.data).underlying().keys().vvector().size();
    }
}
