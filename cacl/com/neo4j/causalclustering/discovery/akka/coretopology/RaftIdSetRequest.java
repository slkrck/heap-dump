package com.neo4j.causalclustering.discovery.akka.coretopology;

import akka.actor.ActorRef;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.identity.RaftId;

import java.time.Duration;
import java.util.Objects;

public class RaftIdSetRequest
{
    private final RaftId raftId;
    private final MemberId publisher;
    private final Duration timeout;
    private final ActorRef replyTo;

    public RaftIdSetRequest( RaftId raftId, MemberId publisher, Duration timeout )
    {
        this( raftId, publisher, timeout, ActorRef.noSender() );
    }

    private RaftIdSetRequest( RaftId raftId, MemberId publisher, Duration timeout, ActorRef replyTo )
    {
        this.raftId = raftId;
        this.publisher = publisher;
        this.timeout = timeout;
        this.replyTo = replyTo;
    }

    RaftIdSetRequest withReplyTo( ActorRef replyTo )
    {
        return new RaftIdSetRequest( this.raftId, this.publisher, this.timeout, replyTo );
    }

    ActorRef replyTo()
    {
        return this.replyTo;
    }

    public RaftId raftId()
    {
        return this.raftId;
    }

    public MemberId publisher()
    {
        return this.publisher;
    }

    public Duration timeout()
    {
        return this.timeout;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            RaftIdSetRequest that = (RaftIdSetRequest) o;
            return Objects.equals( this.raftId, that.raftId ) && Objects.equals( this.publisher, that.publisher );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.raftId, this.publisher} );
    }

    public String toString()
    {
        return "RaftIdSetRequest{raftId=" + this.raftId + ", publisher=" + this.publisher + ", timeout=" + this.timeout + ", replyTo=" + this.replyTo + "}";
    }
}
