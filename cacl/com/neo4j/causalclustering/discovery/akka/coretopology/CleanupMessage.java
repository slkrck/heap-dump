package com.neo4j.causalclustering.discovery.akka.coretopology;

import akka.cluster.UniqueAddress;

import java.util.Objects;

public class CleanupMessage
{
    private final UniqueAddress uniqueAddress;

    public CleanupMessage( UniqueAddress uniqueAddress )
    {
        this.uniqueAddress = uniqueAddress;
    }

    public UniqueAddress uniqueAddress()
    {
        return this.uniqueAddress;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            CleanupMessage that = (CleanupMessage) o;
            return Objects.equals( this.uniqueAddress, that.uniqueAddress );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.uniqueAddress} );
    }
}
