package com.neo4j.causalclustering.discovery.akka.coretopology;

import akka.cluster.UniqueAddress;
import com.neo4j.causalclustering.core.CausalClusteringSettings;
import com.neo4j.causalclustering.discovery.CoreServerInfo;

import java.util.Objects;
import java.util.Optional;

import org.neo4j.configuration.Config;
import org.neo4j.kernel.database.NamedDatabaseId;

public class BootstrapState
{
    public static final BootstrapState EMPTY;

    static
    {
        EMPTY = new BootstrapState( ClusterViewMessage.EMPTY, MetadataMessage.EMPTY, (UniqueAddress) null, (Config) null );
    }

    private final ClusterViewMessage clusterView;
    private final MetadataMessage memberData;
    private final UniqueAddress selfAddress;
    private final Config config;

    BootstrapState( ClusterViewMessage clusterView, MetadataMessage memberData, UniqueAddress selfAddress, Config config )
    {
        this.clusterView = (ClusterViewMessage) Objects.requireNonNull( clusterView );
        this.memberData = (MetadataMessage) Objects.requireNonNull( memberData );
        this.selfAddress = selfAddress;
        this.config = config;
    }

    private static boolean isPotentialLeader( CoreServerInfoForMemberId infoForMember, NamedDatabaseId namedDatabaseId )
    {
        CoreServerInfo info = infoForMember.coreServerInfo();
        return !info.refusesToBeLeader() && info.startedDatabaseIds().contains( namedDatabaseId.databaseId() );
    }

    public boolean canBootstrapRaft( NamedDatabaseId namedDatabaseId )
    {
        boolean iDoNotRefuseToBeLeader = this.config != null && !(Boolean) this.config.get( CausalClusteringSettings.refuse_to_be_leader );
        boolean clusterHasConverged = this.clusterView.converged();
        boolean iAmFirstPotentialLeader = this.iAmFirstPotentialLeader( namedDatabaseId );
        return iDoNotRefuseToBeLeader && clusterHasConverged && iAmFirstPotentialLeader;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            BootstrapState that = (BootstrapState) o;
            return Objects.equals( this.clusterView, that.clusterView ) && Objects.equals( this.memberData, that.memberData ) &&
                    Objects.equals( this.selfAddress, that.selfAddress ) && Objects.equals( this.config, that.config );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.clusterView, this.memberData, this.selfAddress, this.config} );
    }

    private boolean iAmFirstPotentialLeader( NamedDatabaseId namedDatabaseId )
    {
        Optional<UniqueAddress> firstPotentialLeader = this.clusterView.availableMembers().filter( ( member ) -> {
            return this.isPotentialLeader( member, namedDatabaseId );
        } ).findFirst();
        return (Boolean) firstPotentialLeader.map( ( address ) -> {
            return Objects.equals( address, this.selfAddress );
        } ).orElse( false );
    }

    private boolean isPotentialLeader( UniqueAddress member, NamedDatabaseId namedDatabaseId )
    {
        return (Boolean) this.memberData.getOpt( member ).map( ( metadata ) -> {
            return isPotentialLeader( metadata, namedDatabaseId );
        } ).orElse( false );
    }
}
