package com.neo4j.causalclustering.discovery.akka.coretopology;

import akka.actor.AbstractLoggingActor;
import akka.actor.Props;
import akka.actor.AbstractActor.Receive;
import akka.cluster.Cluster;
import akka.cluster.Member;
import akka.japi.pf.ReceiveBuilder;

import java.util.Objects;
import java.util.stream.Stream;

public class ClusterDowningActor extends AbstractLoggingActor
{
    private final Cluster cluster;

    public ClusterDowningActor( Cluster cluster )
    {
        this.cluster = cluster;
    }

    public static Props props( Cluster cluster )
    {
        return Props.create( ClusterDowningActor.class, () -> {
            return new ClusterDowningActor( cluster );
        } );
    }

    public Receive createReceive()
    {
        return ReceiveBuilder.create().match( ClusterViewMessage.class, this::handle ).build();
    }

    private void handle( ClusterViewMessage clusterView )
    {
        if ( clusterView.mostAreReachable() )
        {
            this.log().info( "Downing members: {}", clusterView.unreachable() );
            Stream var10000 = clusterView.unreachable().stream().map( Member::address );
            Cluster var10001 = this.cluster;
            Objects.requireNonNull( var10001 );
            var10000.forEach( var10001::down );
        }
        else
        {
            this.log().info( "In minority side of network partition? {}", clusterView );
        }
    }
}
