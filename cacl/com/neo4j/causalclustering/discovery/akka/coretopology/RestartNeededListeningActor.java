package com.neo4j.causalclustering.discovery.akka.coretopology;

import akka.actor.AbstractLoggingActor;
import akka.actor.Props;
import akka.actor.AbstractActor.Receive;
import akka.cluster.Cluster;
import akka.cluster.ClusterEvent.CurrentClusterState;
import akka.cluster.ClusterEvent.ClusterShuttingDown.;
import akka.event.EventStream;
import akka.japi.pf.ReceiveBuilder;
import akka.remote.ThisActorSystemQuarantinedEvent;

public class RestartNeededListeningActor extends AbstractLoggingActor
{
    public static final String NAME = "cc-core-restart-needed-listener";
    private final Runnable restart;
    private final EventStream eventStream;
    private final Cluster cluster;

    private RestartNeededListeningActor( Runnable restart, EventStream eventStream, Cluster cluster )
    {
        this.restart = restart;
        this.eventStream = eventStream;
        this.cluster = cluster;
    }

    public static Props props( Runnable restart, EventStream eventStream, Cluster cluster )
    {
        return Props.create( RestartNeededListeningActor.class, () -> {
            return new RestartNeededListeningActor( restart, eventStream, cluster );
        } );
    }

    public void preStart()
    {
        this.eventStream.subscribe( this.getSelf(), ThisActorSystemQuarantinedEvent.class );
        this.cluster.subscribe( this.getSelf(), new Class[]{. class});
    }

    public void postStop()
    {
        this.unsubscribe();
    }

    private void unsubscribe()
    {
        this.eventStream.unsubscribe( this.getSelf(), ThisActorSystemQuarantinedEvent.class );
        this.cluster.unsubscribe( this.getSelf(),. class);
    }

    public Receive createReceive()
    {
        return ReceiveBuilder.create().match( ThisActorSystemQuarantinedEvent.class, this::doRestart ).match(. class,this::doRestart).
        match( CurrentClusterState.class, ( ignore ) -> {
        } ).build();
    }

    private void doRestart( Object event )
    {
        this.log().info( "Restart triggered by {}", event );
        this.restart.run();
        this.unsubscribe();
        this.getContext().become( this.createShuttingDownReceive() );
    }

    private Receive createShuttingDownReceive()
    {
        return ReceiveBuilder.create().match( ThisActorSystemQuarantinedEvent.class, this::ignore ).match(. class,this::ignore).
        match( CurrentClusterState.class, ( ignore ) -> {
        } ).build();
    }

    private void ignore( Object event )
    {
        this.log().debug( "Ignoring as restart has been triggered: {}", event );
    }
}
