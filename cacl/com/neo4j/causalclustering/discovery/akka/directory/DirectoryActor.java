package com.neo4j.causalclustering.discovery.akka.directory;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.cluster.Cluster;
import akka.cluster.ddata.ORMap;
import akka.cluster.ddata.ORMapKey;
import akka.japi.pf.ReceiveBuilder;
import akka.stream.javadsl.SourceQueueWithComplete;
import com.neo4j.causalclustering.core.consensus.LeaderInfo;
import com.neo4j.causalclustering.discovery.akka.BaseReplicatedDataActor;
import com.neo4j.causalclustering.discovery.akka.monitoring.ReplicatedDataIdentifier;
import com.neo4j.causalclustering.discovery.akka.monitoring.ReplicatedDataMonitor;

import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.neo4j.kernel.database.DatabaseId;

public class DirectoryActor extends BaseReplicatedDataActor<ORMap<DatabaseId,ReplicatedLeaderInfo>>
{
    public static final String NAME = "cc-directory-actor";
    private final SourceQueueWithComplete<Map<DatabaseId,LeaderInfo>> discoveryUpdateSink;
    private final ActorRef rrTopologyActor;

    private DirectoryActor( Cluster cluster, ActorRef replicator, SourceQueueWithComplete<Map<DatabaseId,LeaderInfo>> discoveryUpdateSink,
            ActorRef rrTopologyActor, ReplicatedDataMonitor monitor )
    {
        super( cluster, replicator, ORMapKey::create, ORMap::create, ReplicatedDataIdentifier.DIRECTORY, monitor );
        this.discoveryUpdateSink = discoveryUpdateSink;
        this.rrTopologyActor = rrTopologyActor;
    }

    public static Props props( Cluster cluster, ActorRef replicator, SourceQueueWithComplete<Map<DatabaseId,LeaderInfo>> discoveryUpdateSink,
            ActorRef rrTopologyActor, ReplicatedDataMonitor monitor )
    {
        return Props.create( DirectoryActor.class, () -> {
            return new DirectoryActor( cluster, replicator, discoveryUpdateSink, rrTopologyActor, monitor );
        } );
    }

    protected void sendInitialDataToReplicator()
    {
    }

    protected void handleCustomEvents( ReceiveBuilder builder )
    {
        builder.match( LeaderInfoSettingMessage.class, ( message ) -> {
            this.modifyReplicatedData( this.key, ( map ) -> {
                return map.put( this.cluster, message.database(), new ReplicatedLeaderInfo( message.leaderInfo() ) );
            } );
        } );
    }

    protected void handleIncomingData( ORMap<DatabaseId,ReplicatedLeaderInfo> newData )
    {
        this.data = newData;
        Map<DatabaseId,LeaderInfo> leaderInfos = (Map) ((ORMap) this.data).getEntries().entrySet().stream().collect( Collectors.toMap( Entry::getKey, ( e ) -> {
            return ((ReplicatedLeaderInfo) e.getValue()).leaderInfo();
        } ) );
        this.discoveryUpdateSink.offer( leaderInfos );
        this.rrTopologyActor.tell( new LeaderInfoDirectoryMessage( leaderInfos ), this.getSelf() );
    }

    protected int dataMetricVisible()
    {
        return ((ORMap) this.data).size();
    }

    protected int dataMetricInvisible()
    {
        return ((ORMap) this.data).keys().vvector().size();
    }
}
