package com.neo4j.causalclustering.discovery.akka.directory;

import com.neo4j.causalclustering.core.consensus.LeaderInfo;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

import org.neo4j.kernel.database.DatabaseId;

public class LeaderInfoDirectoryMessage
{
    public static final LeaderInfoDirectoryMessage EMPTY = new LeaderInfoDirectoryMessage( Collections.emptyMap() );
    private final Map<DatabaseId,LeaderInfo> leaders;

    public LeaderInfoDirectoryMessage( Map<DatabaseId,LeaderInfo> leaders )
    {
        this.leaders = Collections.unmodifiableMap( leaders );
    }

    public Map<DatabaseId,LeaderInfo> leaders()
    {
        return this.leaders;
    }

    public String toString()
    {
        return "LeaderInfoDirectoryMessage{leaders=" + this.leaders + "}";
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            LeaderInfoDirectoryMessage that = (LeaderInfoDirectoryMessage) o;
            return Objects.equals( this.leaders, that.leaders );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.leaders} );
    }
}
