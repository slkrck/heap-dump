package com.neo4j.causalclustering.discovery.akka.directory;

import akka.cluster.ddata.AbstractReplicatedData;
import com.neo4j.causalclustering.core.consensus.LeaderInfo;

import java.util.Objects;

public class ReplicatedLeaderInfo extends AbstractReplicatedData<ReplicatedLeaderInfo>
{
    private final LeaderInfo leaderInfo;

    public ReplicatedLeaderInfo( LeaderInfo leaderInfo )
    {
        this.leaderInfo = leaderInfo;
    }

    public LeaderInfo leaderInfo()
    {
        return this.leaderInfo;
    }

    public ReplicatedLeaderInfo mergeData( ReplicatedLeaderInfo that )
    {
        if ( that.leaderInfo.term() > this.leaderInfo.term() )
        {
            return that;
        }
        else
        {
            return that.leaderInfo.term() >= this.leaderInfo.term() && !this.leaderInfo.isSteppingDown() ? that : this;
        }
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            ReplicatedLeaderInfo that = (ReplicatedLeaderInfo) o;
            return Objects.equals( this.leaderInfo, that.leaderInfo );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.leaderInfo} );
    }
}
