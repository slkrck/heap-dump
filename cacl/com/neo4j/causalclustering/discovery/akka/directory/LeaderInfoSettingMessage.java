package com.neo4j.causalclustering.discovery.akka.directory;

import com.neo4j.causalclustering.core.consensus.LeaderInfo;
import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.kernel.database.NamedDatabaseId;

public class LeaderInfoSettingMessage
{
    private final LeaderInfo leaderInfo;
    private final DatabaseId databaseId;

    public LeaderInfoSettingMessage( LeaderInfo leaderInfo, NamedDatabaseId namedDatabaseId )
    {
        this.leaderInfo = leaderInfo;
        this.databaseId = namedDatabaseId.databaseId();
    }

    public LeaderInfo leaderInfo()
    {
        return this.leaderInfo;
    }

    public DatabaseId database()
    {
        return this.databaseId;
    }
}
