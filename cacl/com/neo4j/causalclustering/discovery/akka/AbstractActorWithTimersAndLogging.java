package com.neo4j.causalclustering.discovery.akka;

import akka.actor.AbstractActor;
import akka.actor.ActorLogging;
import akka.actor.TimerScheduler;
import akka.actor.TimerSchedulerImpl;
import akka.actor.Timers;
import akka.event.LoggingAdapter;
import scala.Option;
import scala.PartialFunction;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public abstract class AbstractActorWithTimersAndLogging extends AbstractActor implements Timers, ActorLogging
{
    private final TimerSchedulerImpl akka$actor$Timers$$_timers;
    private LoggingAdapter akka$actor$ActorLogging$$_log;

    public AbstractActorWithTimersAndLogging()
    {
        Timers.$init$( this );
        ActorLogging.$init$( this );
    }

    public LoggingAdapter log()
    {
        return ActorLogging.log$( this );
    }

    public final TimerScheduler timers()
    {
        return Timers.timers$( this );
    }

    public void aroundPreRestart( final Throwable reason, final Option<Object> message )
    {
        Timers.aroundPreRestart$( this, reason, message );
    }

    public void aroundPostStop()
    {
        Timers.aroundPostStop$( this );
    }

    public void aroundReceive( final PartialFunction<Object,BoxedUnit> receive, final Object msg )
    {
        Timers.aroundReceive$( this, receive, msg );
    }

    public LoggingAdapter akka$actor$ActorLogging$$_log()
    {
        return this.akka$actor$ActorLogging$$_log;
    }

    public void akka$actor$ActorLogging$$_log_$eq( final LoggingAdapter x$1 )
    {
        this.akka$actor$ActorLogging$$_log = x$1;
    }

    public TimerSchedulerImpl akka$actor$Timers$$_timers()
    {
        return this.akka$actor$Timers$$_timers;
    }

    public final void akka$actor$Timers$_setter_$akka$actor$Timers$$_timers_$eq( final TimerSchedulerImpl x$1 )
    {
        this.akka$actor$Timers$$_timers = x$1;
    }

    public final TimerScheduler getTimers()
    {
        return this.timers();
    }
}
