package com.neo4j.causalclustering.discovery.akka.readreplicatopology;

import akka.stream.javadsl.SourceQueueWithComplete;
import com.neo4j.causalclustering.discovery.DatabaseCoreTopology;
import com.neo4j.causalclustering.discovery.DatabaseReadReplicaTopology;
import com.neo4j.causalclustering.discovery.ReplicatedDatabaseState;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

class PruningStateSink<T>
{
    private final String stateType;
    private final SourceQueueWithComplete<T> stateSink;
    private final Function<DatabaseId,T> emptyStateFactory;
    private final Function<T,DatabaseId> getDatabaseId;
    private final Duration maxStateLifetime;
    private final Clock clock;
    private final Log log;
    private final Map<DatabaseId,Instant> lastUpdated = new HashMap();

    private PruningStateSink( String stateType, SourceQueueWithComplete<T> underlying, Function<DatabaseId,T> emptyStateFactory,
            Function<T,DatabaseId> getDatabaseId, Duration maxStateLifetime, Clock clock, LogProvider logProvider )
    {
        this.stateType = stateType;
        this.stateSink = underlying;
        this.emptyStateFactory = emptyStateFactory;
        this.maxStateLifetime = maxStateLifetime;
        this.clock = clock;
        this.log = logProvider.getLog( this.getClass() );
        this.getDatabaseId = getDatabaseId;
    }

    static PruningStateSink<DatabaseCoreTopology> forCoreTopologies( SourceQueueWithComplete<DatabaseCoreTopology> topologySink, Duration maxTopologyLifetime,
            Clock clock, LogProvider logProvider )
    {
        return new PruningStateSink( "Core topology", topologySink, DatabaseCoreTopology::empty, DatabaseCoreTopology::databaseId, maxTopologyLifetime, clock,
                logProvider );
    }

    static PruningStateSink<DatabaseReadReplicaTopology> forReadReplicaTopologies( SourceQueueWithComplete<DatabaseReadReplicaTopology> topologySink,
            Duration maxTopologyLifetime, Clock clock, LogProvider logProvider )
    {
        return new PruningStateSink( "Read replica topology", topologySink, DatabaseReadReplicaTopology::empty, DatabaseReadReplicaTopology::databaseId,
                maxTopologyLifetime, clock, logProvider );
    }

    static PruningStateSink<ReplicatedDatabaseState> forCoreDatabaseStates( SourceQueueWithComplete<ReplicatedDatabaseState> databaseStateSink,
            Duration maxDatabaseStateLifetime, Clock clock, LogProvider logProvider )
    {
        return new PruningStateSink( "Core member database states", databaseStateSink, ( id ) -> {
            return ReplicatedDatabaseState.ofCores( id, Map.of() );
        }, ReplicatedDatabaseState::databaseId, maxDatabaseStateLifetime, clock, logProvider );
    }

    static PruningStateSink<ReplicatedDatabaseState> forReadReplicaDatabaseStates( SourceQueueWithComplete<ReplicatedDatabaseState> databaseStateSink,
            Duration maxDatabaseStateLifetime, Clock clock, LogProvider logProvider )
    {
        return new PruningStateSink( "Read replica member database states", databaseStateSink, ( id ) -> {
            return ReplicatedDatabaseState.ofReadReplicas( id, Map.of() );
        }, ReplicatedDatabaseState::databaseId, maxDatabaseStateLifetime, clock, logProvider );
    }

    void offer( T state )
    {
        this.stateSink.offer( state );
        this.lastUpdated.put( (DatabaseId) this.getDatabaseId.apply( state ), this.clock.instant() );
    }

    void pruneStaleState()
    {
        Set<DatabaseId> databaseIdsForStaleState = this.findDatabaseIdsForStaleState();
        Iterator var2 = databaseIdsForStaleState.iterator();

        while ( var2.hasNext() )
        {
            DatabaseId databaseId = (DatabaseId) var2.next();
            this.log.info( "%s for database %s hasn't been updated for more than %s and will be removed",
                    new Object[]{this.stateType, databaseId, this.maxStateLifetime} );
            T empty = this.emptyStateFactory.apply( databaseId );
            this.stateSink.offer( empty );
            this.lastUpdated.remove( databaseId );
        }
    }

    private Set<DatabaseId> findDatabaseIdsForStaleState()
    {
        Instant oldestAllowed = this.clock.instant().minus( this.maxStateLifetime );
        return (Set) this.lastUpdated.entrySet().stream().filter( ( entry ) -> {
            return ((Instant) entry.getValue()).isBefore( oldestAllowed );
        } ).map( Entry::getKey ).collect( Collectors.toSet() );
    }
}
