package com.neo4j.causalclustering.discovery.akka.readreplicatopology;

import akka.actor.ActorRef;
import com.neo4j.causalclustering.discovery.ReadReplicaInfo;
import com.neo4j.causalclustering.discovery.akka.database.state.DiscoveryDatabaseState;
import com.neo4j.causalclustering.identity.MemberId;

import java.time.Clock;
import java.time.Instant;
import java.util.Map;
import java.util.Objects;

import org.neo4j.kernel.database.DatabaseId;

public class ReadReplicaViewRecord
{
    private final ReadReplicaInfo readReplicaInfo;
    private final Instant timestamp;
    private final ActorRef topologyClientActorRef;
    private final MemberId memberId;
    private final Map<DatabaseId,DiscoveryDatabaseState> databaseStates;

    ReadReplicaViewRecord( ReadReplicaInfo readReplicaInfo, ActorRef topologyClientActorRef, MemberId memberId, Instant timestamp,
            Map<DatabaseId,DiscoveryDatabaseState> databaseStates )
    {
        this.readReplicaInfo = readReplicaInfo;
        this.timestamp = timestamp;
        this.topologyClientActorRef = topologyClientActorRef;
        this.memberId = memberId;
        this.databaseStates = databaseStates;
    }

    ReadReplicaViewRecord( ReadReplicaRefreshMessage message, Clock clock )
    {
        this( message.readReplicaInfo(), message.topologyClientActorRef(), message.memberId(), Instant.now( clock ), message.databaseStates() );
    }

    public ReadReplicaInfo readReplicaInfo()
    {
        return this.readReplicaInfo;
    }

    public Instant timestamp()
    {
        return this.timestamp;
    }

    public ActorRef topologyClientActorRef()
    {
        return this.topologyClientActorRef;
    }

    public MemberId memberId()
    {
        return this.memberId;
    }

    Map<DatabaseId,DiscoveryDatabaseState> databaseStates()
    {
        return this.databaseStates;
    }

    public String toString()
    {
        return "ReadReplicaViewRecord{readReplicaInfo=" + this.readReplicaInfo + ", timestamp=" + this.timestamp + ", topologyClientActorRef=" +
                this.topologyClientActorRef + ", memberId=" + this.memberId + ", databaseState=" + this.databaseStates + "}";
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            ReadReplicaViewRecord that = (ReadReplicaViewRecord) o;
            return Objects.equals( this.readReplicaInfo, that.readReplicaInfo ) && Objects.equals( this.timestamp, that.timestamp ) &&
                    Objects.equals( this.topologyClientActorRef, that.topologyClientActorRef ) && Objects.equals( this.memberId, that.memberId ) &&
                    Objects.equals( this.databaseStates, that.databaseStates );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.readReplicaInfo, this.timestamp, this.topologyClientActorRef, this.memberId, this.databaseStates} );
    }
}
