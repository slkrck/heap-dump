package com.neo4j.causalclustering.discovery.akka.readreplicatopology;

import akka.actor.ActorRef;
import com.neo4j.causalclustering.discovery.ReadReplicaInfo;
import com.neo4j.causalclustering.discovery.akka.database.state.DiscoveryDatabaseState;
import com.neo4j.causalclustering.identity.MemberId;

import java.util.Map;
import java.util.Objects;

import org.neo4j.kernel.database.DatabaseId;

public class ReadReplicaRefreshMessage
{
    private final ReadReplicaInfo readReplicaInfo;
    private final MemberId memberId;
    private final ActorRef clusterClient;
    private final ActorRef topologyClient;
    private final Map<DatabaseId,DiscoveryDatabaseState> databaseStates;

    public ReadReplicaRefreshMessage( ReadReplicaInfo readReplicaInfo, MemberId memberId, ActorRef clusterClient, ActorRef topologyClient,
            Map<DatabaseId,DiscoveryDatabaseState> databaseStates )
    {
        this.readReplicaInfo = readReplicaInfo;
        this.memberId = memberId;
        this.clusterClient = clusterClient;
        this.topologyClient = topologyClient;
        this.databaseStates = databaseStates;
    }

    public ReadReplicaInfo readReplicaInfo()
    {
        return this.readReplicaInfo;
    }

    public MemberId memberId()
    {
        return this.memberId;
    }

    public Map<DatabaseId,DiscoveryDatabaseState> databaseStates()
    {
        return this.databaseStates;
    }

    public ActorRef clusterClient()
    {
        return this.clusterClient;
    }

    public ActorRef topologyClientActorRef()
    {
        return this.topologyClient;
    }

    public String toString()
    {
        return "ReadReplicaRefreshMessage{readReplicaInfo=" + this.readReplicaInfo + ", memberId=" + this.memberId + ", clusterClient=" + this.clusterClient +
                ", topologyClient=" + this.topologyClient + ", databaseStates=" + this.databaseStates + "}";
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            ReadReplicaRefreshMessage that = (ReadReplicaRefreshMessage) o;
            return Objects.equals( this.readReplicaInfo, that.readReplicaInfo ) && Objects.equals( this.memberId, that.memberId ) &&
                    Objects.equals( this.clusterClient, that.clusterClient ) && Objects.equals( this.topologyClient, that.topologyClient ) &&
                    Objects.equals( this.databaseStates, that.databaseStates );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.readReplicaInfo, this.memberId, this.clusterClient, this.topologyClient, this.databaseStates} );
    }
}
