package com.neo4j.causalclustering.discovery.akka.readreplicatopology;

import akka.actor.AbstractActorWithTimers;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor.Receive;
import akka.cluster.client.ClusterClient.Publish;
import akka.japi.pf.ReceiveBuilder;
import akka.stream.javadsl.SourceQueueWithComplete;
import com.neo4j.causalclustering.core.CausalClusteringSettings;
import com.neo4j.causalclustering.core.consensus.LeaderInfo;
import com.neo4j.causalclustering.discovery.DatabaseCoreTopology;
import com.neo4j.causalclustering.discovery.DatabaseReadReplicaTopology;
import com.neo4j.causalclustering.discovery.ReadReplicaInfo;
import com.neo4j.causalclustering.discovery.ReplicatedDatabaseState;
import com.neo4j.causalclustering.discovery.akka.common.DatabaseStartedMessage;
import com.neo4j.causalclustering.discovery.akka.common.DatabaseStoppedMessage;
import com.neo4j.causalclustering.discovery.akka.database.state.DiscoveryDatabaseState;
import com.neo4j.causalclustering.discovery.akka.directory.LeaderInfoDirectoryMessage;
import com.neo4j.causalclustering.discovery.member.DiscoveryMember;
import com.neo4j.dbms.EnterpriseOperatorState;

import java.time.Clock;
import java.time.Duration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.neo4j.configuration.Config;
import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class ClientTopologyActor extends AbstractActorWithTimers
{
    public static final String NAME = "cc-client-topology-actor";
    private static final String REFRESH = "topology refresh";
    private static final int REFRESHES_BEFORE_REMOVE_TOPOLOGY = 3;
    private final Duration refreshDuration;
    private final DiscoveryMember myself;
    private final PruningStateSink<DatabaseCoreTopology> coreTopologySink;
    private final PruningStateSink<DatabaseReadReplicaTopology> readreplicaTopologySink;
    private final PruningStateSink<ReplicatedDatabaseState> coresDbStateSink;
    private final PruningStateSink<ReplicatedDatabaseState> readReplicasDbStateSink;
    private final SourceQueueWithComplete<Map<DatabaseId,LeaderInfo>> discoverySink;
    private final Map<DatabaseId,DiscoveryDatabaseState> localDatabaseStates;
    private final ActorRef clusterClient;
    private final Config config;
    private final Log log;
    private final Set<DatabaseId> startedDatabases = new HashSet();

    private ClientTopologyActor( DiscoveryMember myself, SourceQueueWithComplete<DatabaseCoreTopology> coreTopologySink,
            SourceQueueWithComplete<DatabaseReadReplicaTopology> rrTopologySink, SourceQueueWithComplete<Map<DatabaseId,LeaderInfo>> leaderInfoSink,
            SourceQueueWithComplete<ReplicatedDatabaseState> stateSink, Config config, LogProvider logProvider, Clock clock, ActorRef clusterClient )
    {
        this.myself = myself;
        this.refreshDuration = (Duration) config.get( CausalClusteringSettings.cluster_topology_refresh );
        Duration maxTopologyLifetime = this.refreshDuration.multipliedBy( 3L );
        this.coreTopologySink = PruningStateSink.forCoreTopologies( coreTopologySink, maxTopologyLifetime, clock, logProvider );
        this.readreplicaTopologySink = PruningStateSink.forReadReplicaTopologies( rrTopologySink, maxTopologyLifetime, clock, logProvider );
        this.coresDbStateSink = PruningStateSink.forCoreDatabaseStates( stateSink, maxTopologyLifetime, clock, logProvider );
        this.readReplicasDbStateSink = PruningStateSink.forReadReplicaDatabaseStates( stateSink, maxTopologyLifetime, clock, logProvider );
        this.discoverySink = leaderInfoSink;
        this.clusterClient = clusterClient;
        this.localDatabaseStates = new HashMap();
        this.config = config;
        this.log = logProvider.getLog( this.getClass() );
    }

    public static Props props( DiscoveryMember myself, SourceQueueWithComplete<DatabaseCoreTopology> coreTopologySink,
            SourceQueueWithComplete<DatabaseReadReplicaTopology> rrTopologySink, SourceQueueWithComplete<Map<DatabaseId,LeaderInfo>> discoverySink,
            SourceQueueWithComplete<ReplicatedDatabaseState> stateSink, ActorRef clusterClient, Config config, LogProvider logProvider, Clock clock )
    {
        return Props.create( ClientTopologyActor.class, () -> {
            return new ClientTopologyActor( myself, coreTopologySink, rrTopologySink, discoverySink, stateSink, config, logProvider, clock, clusterClient );
        } );
    }

    public Receive createReceive()
    {
        ReceiveBuilder var10000 = ReceiveBuilder.create();
        PruningStateSink var10002 = this.coreTopologySink;
        Objects.requireNonNull( var10002 );
        var10000 = var10000.match( DatabaseCoreTopology.class, var10002::offer );
        var10002 = this.readreplicaTopologySink;
        Objects.requireNonNull( var10002 );
        return var10000.match( DatabaseReadReplicaTopology.class, var10002::offer ).match( LeaderInfoDirectoryMessage.class, ( msg ) -> {
            this.discoverySink.offer( msg.leaders() );
        } ).match( ReplicatedDatabaseState.class, this::handleRemoteDatabaseStateUpdate ).match( ClientTopologyActor.TopologiesRefresh.class, ( ignored ) -> {
            this.handleRefresh();
        } ).match( DatabaseStartedMessage.class, this::handleDatabaseStartedMessage ).match( DatabaseStoppedMessage.class,
                this::handleDatabaseStoppedMessage ).match( DiscoveryDatabaseState.class, this::handleLocalDatabaseStateUpdate ).build();
    }

    public void preStart()
    {
        this.getTimers().startPeriodicTimer( "topology refresh", ClientTopologyActor.TopologiesRefresh.getInstance(), this.refreshDuration );
        this.startedDatabases.addAll( this.myself.startedDatabases() );
        this.sendReadReplicaInfo();
    }

    private void handleDatabaseStartedMessage( DatabaseStartedMessage message )
    {
        if ( this.startedDatabases.add( message.namedDatabaseId().databaseId() ) )
        {
            this.sendReadReplicaInfo();
        }
    }

    private void handleDatabaseStoppedMessage( DatabaseStoppedMessage message )
    {
        if ( this.startedDatabases.remove( message.namedDatabaseId().databaseId() ) )
        {
            this.sendReadReplicaInfo();
        }
    }

    private void handleRemoteDatabaseStateUpdate( ReplicatedDatabaseState update )
    {
        if ( update.containsCoreStates() )
        {
            this.coresDbStateSink.offer( update );
        }
        else
        {
            this.readReplicasDbStateSink.offer( update );
        }
    }

    private void handleLocalDatabaseStateUpdate( DiscoveryDatabaseState update )
    {
        if ( update.operatorState() == EnterpriseOperatorState.DROPPED )
        {
            this.localDatabaseStates.remove( update.databaseId() );
        }
        else
        {
            this.localDatabaseStates.put( update.databaseId(), update );
        }
    }

    private void handleRefresh()
    {
        this.coreTopologySink.pruneStaleState();
        this.readreplicaTopologySink.pruneStaleState();
        this.sendReadReplicaInfo();
    }

    private void sendReadReplicaInfo()
    {
        Set<DatabaseId> databaseIds = Set.copyOf( this.startedDatabases );
        ReadReplicaInfo readReplicaInfo = ReadReplicaInfo.from( this.config, databaseIds );
        ReadReplicaRefreshMessage refreshMsg =
                new ReadReplicaRefreshMessage( readReplicaInfo, this.myself.id(), this.clusterClient, this.getSelf(), this.localDatabaseStates );
        this.sendToCore( refreshMsg );
    }

    public void postStop()
    {
        ReadReplicaRemovalMessage msg = new ReadReplicaRemovalMessage( this.clusterClient );
        this.log.debug( "Shutting down and sending removal message: %s", new Object[]{msg} );
        this.sendToCore( msg );
    }

    private void sendToCore( Object msg )
    {
        this.clusterClient.tell( new Publish( "rr-topic", msg ), this.getSelf() );
    }

    private static class TopologiesRefresh
    {
        private static ClientTopologyActor.TopologiesRefresh instance = new ClientTopologyActor.TopologiesRefresh();

        public static ClientTopologyActor.TopologiesRefresh getInstance()
        {
            return instance;
        }
    }
}
