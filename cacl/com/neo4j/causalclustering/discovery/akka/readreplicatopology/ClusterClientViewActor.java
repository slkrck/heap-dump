package com.neo4j.causalclustering.discovery.akka.readreplicatopology;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor.Receive;
import akka.cluster.client.ClusterClientUnreachable;
import akka.cluster.client.ClusterClientUp;
import akka.cluster.client.ClusterClients;
import akka.cluster.client.SubscribeClusterClients;
import akka.cluster.client.UnsubscribeClusterClients;
import akka.japi.pf.ReceiveBuilder;

import java.util.HashSet;
import java.util.Set;

class ClusterClientViewActor extends AbstractLoggingActor
{
    private final ActorRef parent;
    private final ActorRef receptionist;
    private Set<ActorRef> clusterClients = new HashSet();

    private ClusterClientViewActor( ActorRef parent, ActorRef receptionist )
    {
        this.parent = parent;
        this.receptionist = receptionist;
    }

    static Props props( ActorRef parent, ActorRef receptionist )
    {
        return Props.create( ClusterClientViewActor.class, () -> {
            return new ClusterClientViewActor( parent, receptionist );
        } );
    }

    public void preStart()
    {
        this.receptionist.tell( SubscribeClusterClients.getInstance(), this.getSelf() );
    }

    public void postStop()
    {
        this.receptionist.tell( UnsubscribeClusterClients.getInstance(), this.getSelf() );
    }

    public Receive createReceive()
    {
        return ReceiveBuilder.create().match( ClusterClients.class, this::handleClusterClients ).match( ClusterClientUp.class,
                this::handleClusterClientUp ).match( ClusterClientUnreachable.class, this::handleClusterClientUnreachable ).build();
    }

    private void handleClusterClients( ClusterClients msg )
    {
        this.log().debug( "All cluster clients: {}", msg );
        this.clusterClients.addAll( msg.getClusterClients() );
        this.sendToParent();
    }

    private void handleClusterClientUp( ClusterClientUp msg )
    {
        this.log().debug( "Cluster client up: {}", msg );
        this.clusterClients.add( msg.clusterClient() );
        this.sendToParent();
    }

    private void handleClusterClientUnreachable( ClusterClientUnreachable msg )
    {
        this.log().debug( "Cluster client down: {}", msg );
        this.clusterClients.remove( msg.clusterClient() );
        this.sendToParent();
    }

    private void sendToParent()
    {
        this.parent.tell( new ClusterClientViewMessage( this.clusterClients ), this.getSelf() );
    }
}
