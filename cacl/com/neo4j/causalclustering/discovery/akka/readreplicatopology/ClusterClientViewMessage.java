package com.neo4j.causalclustering.discovery.akka.readreplicatopology;

import akka.actor.ActorRef;

import java.util.Objects;
import java.util.Set;

public class ClusterClientViewMessage
{
    private final Set<ActorRef> clusterClients;

    public ClusterClientViewMessage( Set<ActorRef> clusterClients )
    {
        this.clusterClients = Set.copyOf( clusterClients );
    }

    public Set<ActorRef> clusterClients()
    {
        return this.clusterClients;
    }

    public String toString()
    {
        return "ClusterClientViewMessage{clusterClients=" + this.clusterClients + "}";
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            ClusterClientViewMessage that = (ClusterClientViewMessage) o;
            return Objects.equals( this.clusterClients, that.clusterClients );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.clusterClients} );
    }
}
