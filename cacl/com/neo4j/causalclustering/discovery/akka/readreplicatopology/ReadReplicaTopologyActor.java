package com.neo4j.causalclustering.discovery.akka.readreplicatopology;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor.Receive;
import akka.cluster.client.ClusterClientReceptionist;
import akka.japi.pf.ReceiveBuilder;
import akka.stream.javadsl.SourceQueueWithComplete;
import com.neo4j.causalclustering.core.CausalClusteringSettings;
import com.neo4j.causalclustering.discovery.DatabaseCoreTopology;
import com.neo4j.causalclustering.discovery.DatabaseReadReplicaTopology;
import com.neo4j.causalclustering.discovery.ReplicatedDatabaseState;
import com.neo4j.causalclustering.discovery.akka.database.state.AllReplicatedDatabaseStates;
import com.neo4j.causalclustering.discovery.akka.directory.LeaderInfoDirectoryMessage;

import java.time.Clock;
import java.time.Duration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.neo4j.configuration.Config;
import org.neo4j.kernel.database.DatabaseId;

public class ReadReplicaTopologyActor extends AbstractLoggingActor
{
    public static final String NAME = "cc-rr-topology-actor";
    private final SourceQueueWithComplete<DatabaseReadReplicaTopology> topologySink;
    private final SourceQueueWithComplete<ReplicatedDatabaseState> databaseStateSink;
    private final Map<DatabaseId,DatabaseCoreTopology> coreTopologies = new HashMap();
    private final Map<DatabaseId,DatabaseReadReplicaTopology> readReplicaTopologies = new HashMap();
    private Map<DatabaseId,ReplicatedDatabaseState> coreMemberDbStates = new HashMap();
    private Map<DatabaseId,ReplicatedDatabaseState> rrMemberDbStates = new HashMap();
    private LeaderInfoDirectoryMessage databaseLeaderInfo;
    private Set<ActorRef> myClusterClients;
    private ReadReplicaViewMessage readReplicaViewMessage;

    ReadReplicaTopologyActor( SourceQueueWithComplete<DatabaseReadReplicaTopology> topologySink,
            SourceQueueWithComplete<ReplicatedDatabaseState> databaseStateSink, ClusterClientReceptionist receptionist, Config config, Clock clock )
    {
        this.databaseLeaderInfo = LeaderInfoDirectoryMessage.EMPTY;
        this.myClusterClients = new HashSet();
        this.readReplicaViewMessage = ReadReplicaViewMessage.EMPTY;
        this.topologySink = topologySink;
        this.databaseStateSink = databaseStateSink;
        Duration refresh = (Duration) config.get( CausalClusteringSettings.cluster_topology_refresh );
        Props readReplicaViewProps = ReadReplicaViewActor.props( this.getSelf(), receptionist, clock, refresh );
        this.getContext().actorOf( readReplicaViewProps );
        Props clusterClientViewProps = ClusterClientViewActor.props( this.getSelf(), receptionist.underlying() );
        this.getContext().actorOf( clusterClientViewProps );
    }

    public static Props props( SourceQueueWithComplete<DatabaseReadReplicaTopology> topologySink,
            SourceQueueWithComplete<ReplicatedDatabaseState> databaseStateSink, ClusterClientReceptionist receptionist, Config config, Clock clock )
    {
        return Props.create( ReadReplicaTopologyActor.class, () -> {
            return new ReadReplicaTopologyActor( topologySink, databaseStateSink, receptionist, config, clock );
        } );
    }

    public Receive createReceive()
    {
        return ReceiveBuilder.create().match( AllReplicatedDatabaseStates.class, this::updateCoreDatabaseStates ).match( ClusterClientViewMessage.class,
                this::handleClusterClientView ).match( ReadReplicaViewMessage.class, this::handleReadReplicaView ).match( ReadReplicaViewActor.Tick.class,
                this::sendTopologiesToClients ).match( DatabaseCoreTopology.class, this::addCoreTopology ).match( LeaderInfoDirectoryMessage.class,
                this::setDatabaseLeaderInfo ).build();
    }

    private void updateCoreDatabaseStates( AllReplicatedDatabaseStates msg )
    {
        this.coreMemberDbStates = msg.databaseStates();
    }

    private void handleReadReplicaView( ReadReplicaViewMessage msg )
    {
        this.readReplicaViewMessage = msg;
        this.rrMemberDbStates = msg.allReplicatedDatabaseStates();
        this.rrMemberDbStates.forEach( ( id, state ) -> {
            this.databaseStateSink.offer( state );
        } );
        this.buildTopologies();
    }

    private void handleClusterClientView( ClusterClientViewMessage msg )
    {
        this.myClusterClients = msg.clusterClients();
        this.buildTopologies();
    }

    private Stream<ActorRef> myTopologyClients()
    {
        Stream var10000 = this.myClusterClients.stream();
        ReadReplicaViewMessage var10001 = this.readReplicaViewMessage;
        Objects.requireNonNull( var10001 );
        return var10000.flatMap( var10001::topologyClient );
    }

    private void sendTopologiesToClients( ReadReplicaViewActor.Tick ignored )
    {
        this.log().debug( "Sending core topologies to clients: {}", this.coreTopologies );
        this.log().debug( "Sending read replica topologies to clients: {}", this.readReplicaTopologies );
        this.log().debug( "Sending database leader info to clients: {}", this.databaseLeaderInfo );
        this.log().debug( "Sending cores' database states to clients: {}", this.coreMemberDbStates );
        this.log().debug( "Sending read replicas' database states to clients: {}", this.rrMemberDbStates );
        this.myTopologyClients().forEach( ( client ) -> {
            this.sendReadReplicaTopologiesTo( client );
            this.sendCoreTopologiesTo( client );
            client.tell( this.databaseLeaderInfo, this.getSelf() );
            this.sendDatabaseStatesTo( client );
        } );
    }

    private void sendReadReplicaTopologiesTo( ActorRef client )
    {
        Iterator var2 = this.readReplicaTopologies.values().iterator();

        while ( var2.hasNext() )
        {
            DatabaseReadReplicaTopology readReplicaTopology = (DatabaseReadReplicaTopology) var2.next();
            client.tell( readReplicaTopology, this.getSelf() );
        }
    }

    private void sendCoreTopologiesTo( ActorRef client )
    {
        Iterator var2 = this.coreTopologies.values().iterator();

        while ( var2.hasNext() )
        {
            DatabaseCoreTopology coreTopology = (DatabaseCoreTopology) var2.next();
            client.tell( coreTopology, this.getSelf() );
        }
    }

    private void sendDatabaseStatesTo( ActorRef client )
    {
        Iterator var2 = this.coreMemberDbStates.values().iterator();

        ReplicatedDatabaseState replicaState;
        while ( var2.hasNext() )
        {
            replicaState = (ReplicatedDatabaseState) var2.next();
            client.tell( replicaState, this.getSelf() );
        }

        var2 = this.rrMemberDbStates.values().iterator();

        while ( var2.hasNext() )
        {
            replicaState = (ReplicatedDatabaseState) var2.next();
            client.tell( replicaState, this.getSelf() );
        }
    }

    private void addCoreTopology( DatabaseCoreTopology coreTopology )
    {
        this.coreTopologies.put( coreTopology.databaseId(), coreTopology );
    }

    private void setDatabaseLeaderInfo( LeaderInfoDirectoryMessage leaderInfo )
    {
        this.databaseLeaderInfo = leaderInfo;
    }

    private void buildTopologies()
    {
        Set<DatabaseId> receivedDatabaseIds = this.readReplicaViewMessage.databaseIds();
        Set<DatabaseId> absentDatabaseIds = (Set) this.readReplicaTopologies.keySet().stream().filter( ( id ) -> {
            return !receivedDatabaseIds.contains( id );
        } ).collect( Collectors.toSet() );
        absentDatabaseIds.forEach( this::buildTopology );
        receivedDatabaseIds.forEach( this::buildTopology );
    }

    private void buildTopology( DatabaseId databaseId )
    {
        this.log().debug( "Building read replica topology for database {} with read replicas: {}", databaseId, this.readReplicaViewMessage );
        DatabaseReadReplicaTopology readReplicaTopology = this.readReplicaViewMessage.toReadReplicaTopology( databaseId );
        this.log().debug( "Built read replica topology for database {}: {}", databaseId, readReplicaTopology );
        this.topologySink.offer( readReplicaTopology );
        if ( readReplicaTopology.members().isEmpty() )
        {
            this.readReplicaTopologies.remove( databaseId );
        }
        else
        {
            this.readReplicaTopologies.put( databaseId, readReplicaTopology );
        }
    }
}
