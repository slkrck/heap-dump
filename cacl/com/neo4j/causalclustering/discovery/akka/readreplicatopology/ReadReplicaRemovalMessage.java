package com.neo4j.causalclustering.discovery.akka.readreplicatopology;

import akka.actor.ActorRef;

import java.util.Objects;

public class ReadReplicaRemovalMessage
{
    private final ActorRef clusterClient;

    public ReadReplicaRemovalMessage( ActorRef clusterClient )
    {
        this.clusterClient = clusterClient;
    }

    public ActorRef clusterClient()
    {
        return this.clusterClient;
    }

    public String toString()
    {
        return "ReadReplicaRemovalMessage{clusterClient=" + this.clusterClient + "}";
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            ReadReplicaRemovalMessage that = (ReadReplicaRemovalMessage) o;
            return Objects.equals( this.clusterClient, that.clusterClient );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.clusterClient} );
    }
}
