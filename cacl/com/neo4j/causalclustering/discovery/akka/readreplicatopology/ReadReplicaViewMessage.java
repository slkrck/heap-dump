package com.neo4j.causalclustering.discovery.akka.readreplicatopology;

import akka.actor.ActorRef;
import com.neo4j.causalclustering.discovery.DatabaseReadReplicaTopology;
import com.neo4j.causalclustering.discovery.ReadReplicaInfo;
import com.neo4j.causalclustering.discovery.ReplicatedDatabaseState;
import com.neo4j.causalclustering.discovery.akka.database.state.DiscoveryDatabaseState;
import com.neo4j.causalclustering.identity.MemberId;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.neo4j.internal.helpers.collection.Pair;
import org.neo4j.kernel.database.DatabaseId;

class ReadReplicaViewMessage
{
    static final ReadReplicaViewMessage EMPTY = new ReadReplicaViewMessage( Collections.emptyMap() );
    private final Map<ActorRef,ReadReplicaViewRecord> clusterClientReadReplicas;

    ReadReplicaViewMessage( Map<ActorRef,ReadReplicaViewRecord> clusterClientReadReplicas )
    {
        this.clusterClientReadReplicas = Map.copyOf( clusterClientReadReplicas );
    }

    Stream<ActorRef> topologyClient( ActorRef clusterClient )
    {
        return Stream.ofNullable( (ReadReplicaViewRecord) this.clusterClientReadReplicas.get( clusterClient ) ).map(
                ReadReplicaViewRecord::topologyClientActorRef );
    }

    DatabaseReadReplicaTopology toReadReplicaTopology( DatabaseId databaseId )
    {
        Map<MemberId,ReadReplicaInfo> knownReadReplicas = (Map) this.clusterClientReadReplicas.values().stream().filter( ( info ) -> {
            return info.readReplicaInfo().startedDatabaseIds().contains( databaseId );
        } ).map( ( info ) -> {
            return Pair.of( info.memberId(), info.readReplicaInfo() );
        } ).collect( Collectors.toMap( Pair::first, Pair::other ) );
        return new DatabaseReadReplicaTopology( databaseId, knownReadReplicas );
    }

    Map<DatabaseId,ReplicatedDatabaseState> allReplicatedDatabaseStates()
    {
        Map<DatabaseId,Map<MemberId,DiscoveryDatabaseState>> allMemberStatesPerDbMultiMap =
                (Map) this.clusterClientReadReplicas.values().stream().flatMap( this::getAllStatesFromMember ).collect( Collectors.groupingBy( ( p ) -> {
                    return ((DiscoveryDatabaseState) p.other()).databaseId();
                }, Collectors.toMap( Pair::first, Pair::other ) ) );
        return (Map) allMemberStatesPerDbMultiMap.entrySet().stream().collect( Collectors.toMap( Entry::getKey, ( e ) -> {
            return ReplicatedDatabaseState.ofReadReplicas( (DatabaseId) e.getKey(), (Map) e.getValue() );
        } ) );
    }

    private Stream<Pair<MemberId,DiscoveryDatabaseState>> getAllStatesFromMember( ReadReplicaViewRecord record )
    {
        return record.databaseStates().values().stream().map( ( state ) -> {
            return Pair.of( record.memberId(), state );
        } );
    }

    Set<DatabaseId> databaseIds()
    {
        return (Set) this.clusterClientReadReplicas.values().stream().map( ReadReplicaViewRecord::readReplicaInfo ).flatMap( ( info ) -> {
            return info.startedDatabaseIds().stream();
        } ).collect( Collectors.toSet() );
    }

    public String toString()
    {
        return "ReadReplicaViewMessage{clusterClientReadReplicas=" + this.clusterClientReadReplicas + "}";
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            ReadReplicaViewMessage that = (ReadReplicaViewMessage) o;
            return Objects.equals( this.clusterClientReadReplicas, that.clusterClientReadReplicas );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.clusterClientReadReplicas} );
    }
}
