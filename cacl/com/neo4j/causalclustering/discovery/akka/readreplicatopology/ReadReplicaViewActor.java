package com.neo4j.causalclustering.discovery.akka.readreplicatopology;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor.Receive;
import akka.cluster.client.ClusterClientReceptionist;
import akka.japi.pf.ReceiveBuilder;
import com.neo4j.causalclustering.discovery.akka.AbstractActorWithTimersAndLogging;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Map.Entry;
import java.util.stream.Collectors;

class ReadReplicaViewActor extends AbstractActorWithTimersAndLogging
{
    static final String READ_REPLICA_TOPIC = "rr-topic";
    static final int TICKS_BEFORE_REMOVE_READ_REPLICA = 3;
    private static final String TICK_KEY = "Tick key";
    private final ActorRef parent;
    private final ClusterClientReceptionist receptionist;
    private final Clock clock;
    private final Duration refresh;
    private Map<ActorRef,ReadReplicaViewRecord> clusterClientReadReplicas = new HashMap();

    private ReadReplicaViewActor( ActorRef parent, ClusterClientReceptionist receptionist, Clock clock, Duration refresh )
    {
        this.parent = parent;
        this.receptionist = receptionist;
        this.clock = clock;
        this.refresh = refresh;
    }

    static Props props( ActorRef parent, ClusterClientReceptionist receptionist, Clock clock, Duration refresh )
    {
        return Props.create( ReadReplicaViewActor.class, () -> {
            return new ReadReplicaViewActor( parent, receptionist, clock, refresh );
        } );
    }

    public void preStart()
    {
        this.receptionist.registerSubscriber( "rr-topic", this.getSelf() );
        this.getTimers().startPeriodicTimer( "Tick key", ReadReplicaViewActor.Tick.getInstance(), this.refresh );
    }

    public void postStop()
    {
        this.receptionist.unregisterSubscriber( "rr-topic", this.getSelf() );
    }

    public Receive createReceive()
    {
        return ReceiveBuilder.create().match( ReadReplicaRefreshMessage.class, this::handleRefreshMessage ).match( ReadReplicaRemovalMessage.class,
                this::handleRemovalMessage ).match( ReadReplicaViewActor.Tick.class, this::handleTick ).build();
    }

    private void handleRemovalMessage( ReadReplicaRemovalMessage msg )
    {
        ReadReplicaViewRecord removed = (ReadReplicaViewRecord) this.clusterClientReadReplicas.remove( msg.clusterClient() );
        this.log().debug( "Removed shut down read replica {} -> {}", msg.clusterClient(), removed );
        this.sendClusterView();
    }

    private void handleRefreshMessage( ReadReplicaRefreshMessage msg )
    {
        this.log().debug( "Received {}", msg );
        this.clusterClientReadReplicas.put( msg.clusterClient(), new ReadReplicaViewRecord( msg, this.clock ) );
        this.sendClusterView();
    }

    private void handleTick( ReadReplicaViewActor.Tick tick )
    {
        Instant nTicksAgo = Instant.now( this.clock ).minus( this.refresh.multipliedBy( 3L ) );
        List<ActorRef> remove = (List) this.clusterClientReadReplicas.entrySet().stream().filter( ( entry ) -> {
            return ((ReadReplicaViewRecord) entry.getValue()).timestamp().isBefore( nTicksAgo );
        } ).peek( ( entry ) -> {
            this.log().debug( "Removing {} after inactivity", entry );
        } ).map( Entry::getKey ).collect( Collectors.toList() );
        if ( !remove.isEmpty() )
        {
            Map var10001 = this.clusterClientReadReplicas;
            Objects.requireNonNull( var10001 );
            remove.forEach( var10001::remove );
            this.sendClusterView();
        }

        this.parent.tell( tick, this.getSelf() );
    }

    private void sendClusterView()
    {
        this.parent.tell( new ReadReplicaViewMessage( this.clusterClientReadReplicas ), this.getSelf() );
    }

    static class Tick
    {
        private static ReadReplicaViewActor.Tick instance = new ReadReplicaViewActor.Tick();

        private Tick()
        {
        }

        public static ReadReplicaViewActor.Tick getInstance()
        {
            return instance;
        }
    }
}
