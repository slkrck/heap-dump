package com.neo4j.causalclustering.discovery.akka;

import java.util.function.Supplier;

import org.neo4j.internal.helpers.TimeoutStrategy;
import org.neo4j.internal.helpers.TimeoutStrategy.Timeout;

public class Restarter
{
    private final TimeoutStrategy timeoutStrategy;
    private final int acceptableFailures;
    private volatile boolean healthy = true;

    public Restarter( TimeoutStrategy timeoutStrategy, int acceptableFailures )
    {
        this.timeoutStrategy = timeoutStrategy;
        this.acceptableFailures = acceptableFailures;
    }

    public boolean isHealthy()
    {
        return this.healthy;
    }

    public void restart( Supplier<Boolean> retry )
    {
        int count = 0;
        Timeout timeout = this.timeoutStrategy.newTimeout();

        while ( !(Boolean) retry.get() )
        {
            if ( count > this.acceptableFailures )
            {
                this.healthy = false;
            }

            ++count;

            try
            {
                Thread.sleep( timeout.getAndIncrement() );
            }
            catch ( InterruptedException var5 )
            {
                Thread.currentThread().interrupt();
                throw new RuntimeException( var5 );
            }
        }

        this.healthy = true;
    }
}
