package com.neo4j.causalclustering.discovery;

import com.neo4j.causalclustering.core.CausalClusteringSettings;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.neo4j.configuration.Config;
import org.neo4j.configuration.helpers.SocketAddress;

public class InitialDiscoveryMembersResolver implements RemoteMembersResolver
{
    public static final Comparator<SocketAddress> advertisedSockedAddressComparator =
            Comparator.comparing( SocketAddress::getHostname ).thenComparingInt( SocketAddress::getPort );
    private final HostnameResolver hostnameResolver;
    private final List<SocketAddress> advertisedSocketAddresses;

    public InitialDiscoveryMembersResolver( HostnameResolver hostnameResolver, Config config )
    {
        this.hostnameResolver = hostnameResolver;
        this.advertisedSocketAddresses = (List) config.get( CausalClusteringSettings.initial_discovery_members );
    }

    public static Comparator<SocketAddress> advertisedSocketAddressComparator()
    {
        return advertisedSockedAddressComparator;
    }

    public <C extends Collection<T>, T> C resolve( Function<SocketAddress,T> transform, Supplier<C> collectionFactory )
    {
        return (Collection) this.advertisedSocketAddresses.stream().flatMap( ( raw ) -> {
            return this.hostnameResolver.resolve( raw ).stream();
        } ).sorted( advertisedSockedAddressComparator ).distinct().map( transform ).collect( Collectors.toCollection( collectionFactory ) );
    }

    public boolean useOverrides()
    {
        return this.hostnameResolver.useOverrides();
    }
}
