package com.neo4j.causalclustering.discovery;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;
import javax.naming.NamingException;

import org.neo4j.configuration.Config;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.logging.Log;
import org.neo4j.logging.internal.LogService;

public class SrvHostnameResolver extends RetryingHostnameResolver
{
    private final Log userLog;
    private final Log log;
    private final SrvRecordResolver srvRecordResolver;

    SrvHostnameResolver( LogService logService, SrvRecordResolver srvRecordResolver, Config config, RetryStrategy retryStrategy )
    {
        super( config, retryStrategy );
        this.log = logService.getInternalLog( this.getClass() );
        this.userLog = logService.getUserLog( this.getClass() );
        this.srvRecordResolver = srvRecordResolver;
    }

    public static RemoteMembersResolver resolver( LogService logService, SrvRecordResolver srvHostnameResolver, Config config )
    {
        SrvHostnameResolver hostnameResolver = new SrvHostnameResolver( logService, srvHostnameResolver, config, defaultRetryStrategy( config ) );
        return new InitialDiscoveryMembersResolver( hostnameResolver, config );
    }

    public Collection<SocketAddress> resolveOnce( SocketAddress initialAddress )
    {
        try
        {
            Set<SocketAddress> addresses = (Set) this.srvRecordResolver.resolveSrvRecord( initialAddress.getHostname() ).map( ( srvRecord ) -> {
                return new SocketAddress( srvRecord.host, srvRecord.port );
            } ).collect( Collectors.toSet() );
            this.userLog.info( "Resolved initial host '%s' to %s", new Object[]{initialAddress, addresses} );
            if ( addresses.isEmpty() )
            {
                this.log.error( "Failed to resolve srv records for '%s'", new Object[]{initialAddress.getHostname()} );
            }

            return addresses;
        }
        catch ( NamingException var3 )
        {
            this.log.error( String.format( "Failed to resolve srv records for '%s'", initialAddress.getHostname() ), var3 );
            return Collections.emptySet();
        }
    }
}
