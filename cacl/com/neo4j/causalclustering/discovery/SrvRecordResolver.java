package com.neo4j.causalclustering.discovery;

import java.util.stream.Stream;
import javax.naming.NamingException;

public abstract class SrvRecordResolver
{
    public abstract Stream<SrvRecordResolver.SrvRecord> resolveSrvRecord( String var1 ) throws NamingException;

    public Stream<SrvRecordResolver.SrvRecord> resolveSrvRecord( String service, String protocol, String hostname ) throws NamingException
    {
        String url = String.format( "_%s._%s.%s", service, protocol, hostname );
        return this.resolveSrvRecord( url );
    }

    public static class SrvRecord
    {
        public final int priority;
        public final int weight;
        public final int port;
        public final String host;

        private SrvRecord( int priority, int weight, int port, String host )
        {
            this.priority = priority;
            this.weight = weight;
            this.port = port;
            this.host = host.charAt( host.length() - 1 ) == '.' ? host.substring( 0, host.length() - 1 ) : host;
        }

        public static SrvRecordResolver.SrvRecord parse( String input )
        {
            String[] parts = input.split( " " );
            return new SrvRecordResolver.SrvRecord( Integer.parseInt( parts[0] ), Integer.parseInt( parts[1] ), Integer.parseInt( parts[2] ), parts[3] );
        }
    }
}
