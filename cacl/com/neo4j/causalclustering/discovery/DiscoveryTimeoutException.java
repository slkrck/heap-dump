package com.neo4j.causalclustering.discovery;

public class DiscoveryTimeoutException extends Exception
{
    public DiscoveryTimeoutException( Throwable cause )
    {
        super( cause );
    }
}
