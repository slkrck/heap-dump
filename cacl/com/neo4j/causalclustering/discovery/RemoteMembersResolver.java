package com.neo4j.causalclustering.discovery;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Function;
import java.util.function.Supplier;

import org.neo4j.configuration.helpers.SocketAddress;

public interface RemoteMembersResolver
{
    default <REMOTE> Collection<REMOTE> resolve( Function<SocketAddress,REMOTE> transform )
    {
        return this.resolve( transform, ArrayList::new );
    }

    <COLL extends Collection<REMOTE>, REMOTE> COLL resolve( Function<SocketAddress,REMOTE> var1, Supplier<COLL> var2 );

    boolean useOverrides();
}
