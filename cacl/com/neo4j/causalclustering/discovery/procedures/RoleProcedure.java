package com.neo4j.causalclustering.discovery.procedures;

import com.neo4j.causalclustering.discovery.RoleInfo;

import java.util.Arrays;

import org.neo4j.collection.RawIterator;
import org.neo4j.dbms.database.DatabaseContext;
import org.neo4j.dbms.database.DatabaseManager;
import org.neo4j.internal.kernel.api.exceptions.ProcedureException;
import org.neo4j.internal.kernel.api.procs.Neo4jTypes;
import org.neo4j.internal.kernel.api.procs.ProcedureSignature;
import org.neo4j.internal.kernel.api.procs.QualifiedName;
import org.neo4j.kernel.api.ResourceTracker;
import org.neo4j.kernel.api.exceptions.Status.Database;
import org.neo4j.kernel.api.procedure.Context;
import org.neo4j.kernel.api.procedure.CallableProcedure.BasicProcedure;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.TextValue;
import org.neo4j.values.storable.Values;

abstract class RoleProcedure extends BasicProcedure
{
    private static final String PROCEDURE_NAME = "role";
    private static final String[] PROCEDURE_NAMESPACE = new String[]{"dbms", "cluster"};
    private static final String OUTPUT_NAME = "role";
    private static final String PARAMETER_NAME = "database";
    protected final DatabaseManager<?> databaseManager;

    RoleProcedure( DatabaseManager<?> databaseManager )
    {
        super( ProcedureSignature.procedureSignature( new QualifiedName( PROCEDURE_NAMESPACE, "role" ) ).in( "database", Neo4jTypes.NTString ).out( "role",
                Neo4jTypes.NTString ).description( "The role of this instance in the cluster for the specified database." ).systemProcedure().build() );
        this.databaseManager = databaseManager;
    }

    private static ProcedureException databaseNotFoundException( String databaseName )
    {
        return new ProcedureException( Database.DatabaseNotFound,
                "Unable to get a cluster role for database '" + databaseName + "' because this database does not exist", new Object[0] );
    }

    public RawIterator<AnyValue[],ProcedureException> apply( Context ctx, AnyValue[] input, ResourceTracker resourceTracker ) throws ProcedureException
    {
        DatabaseContext databaseContext = this.extractDatabaseContext( input );
        this.checkAvailable( databaseContext );
        RoleInfo role = this.role( databaseContext );
        return RawIterator.of( new AnyValue[][]{{Values.stringValue( role.toString() )}} );
    }

    private void checkAvailable( DatabaseContext databaseContext ) throws ProcedureException
    {
        if ( !databaseContext.database().getDatabaseAvailabilityGuard().isAvailable() )
        {
            throw new ProcedureException( Database.DatabaseUnavailable,
                    "Unable to get a cluster role for database '" + databaseContext.database().getNamedDatabaseId() + " because the database is not available",
                    new Object[0] );
        }
    }

    abstract RoleInfo role( DatabaseContext var1 ) throws ProcedureException;

    private DatabaseContext extractDatabaseContext( AnyValue[] input ) throws ProcedureException
    {
        if ( input.length != 1 )
        {
            throw new IllegalArgumentException( "Illegal input: " + Arrays.toString( input ) );
        }
        else
        {
            AnyValue value = input[0];
            if ( value instanceof TextValue )
            {
                String databaseName = ((TextValue) value).stringValue();
                return (DatabaseContext) this.databaseManager.getDatabaseContext( databaseName ).orElseThrow( () -> {
                    return databaseNotFoundException( databaseName );
                } );
            }
            else
            {
                throw new IllegalArgumentException( "Parameter 'database' value should be a string: " + value );
            }
        }
    }
}
