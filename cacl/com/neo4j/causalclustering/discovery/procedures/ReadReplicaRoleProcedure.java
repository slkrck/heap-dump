package com.neo4j.causalclustering.discovery.procedures;

import com.neo4j.causalclustering.discovery.RoleInfo;
import org.neo4j.dbms.database.DatabaseContext;
import org.neo4j.dbms.database.DatabaseManager;

public class ReadReplicaRoleProcedure extends RoleProcedure
{
    public ReadReplicaRoleProcedure( DatabaseManager<?> databaseManager )
    {
        super( databaseManager );
    }

    RoleInfo role( DatabaseContext namedDatabaseId )
    {
        return RoleInfo.READ_REPLICA;
    }
}
