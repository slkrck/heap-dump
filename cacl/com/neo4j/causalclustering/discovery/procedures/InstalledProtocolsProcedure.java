package com.neo4j.causalclustering.discovery.procedures;

import com.neo4j.causalclustering.protocol.Protocol;
import com.neo4j.causalclustering.protocol.application.ApplicationProtocolVersion;
import com.neo4j.causalclustering.protocol.handshake.ProtocolStack;

import java.util.Comparator;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.neo4j.collection.RawIterator;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.internal.helpers.collection.Iterators;
import org.neo4j.internal.helpers.collection.Pair;
import org.neo4j.internal.kernel.api.exceptions.ProcedureException;
import org.neo4j.internal.kernel.api.procs.Neo4jTypes;
import org.neo4j.internal.kernel.api.procs.ProcedureSignature;
import org.neo4j.internal.kernel.api.procs.QualifiedName;
import org.neo4j.kernel.api.ResourceTracker;
import org.neo4j.kernel.api.procedure.Context;
import org.neo4j.kernel.api.procedure.CallableProcedure.BasicProcedure;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Values;

public class InstalledProtocolsProcedure extends BasicProcedure
{
    public static final String PROCEDURE_NAME = "protocols";
    private static final String[] PROCEDURE_NAMESPACE = new String[]{"dbms", "cluster"};
    private final Supplier<Stream<Pair<SocketAddress,ProtocolStack>>> clientInstalledProtocols;
    private final Supplier<Stream<Pair<SocketAddress,ProtocolStack>>> serverInstalledProtocols;

    public InstalledProtocolsProcedure( Supplier<Stream<Pair<SocketAddress,ProtocolStack>>> clientInstalledProtocols,
            Supplier<Stream<Pair<SocketAddress,ProtocolStack>>> serverInstalledProtocols )
    {
        super( ProcedureSignature.procedureSignature( new QualifiedName( PROCEDURE_NAMESPACE, "protocols" ) ).out( "orientation", Neo4jTypes.NTString ).out(
                "remoteAddress", Neo4jTypes.NTString ).out( "applicationProtocol", Neo4jTypes.NTString ).out( "applicationProtocolVersion",
                Neo4jTypes.NTInteger ).out( "modifierProtocols", Neo4jTypes.NTString ).description(
                "Overview of installed protocols" ).systemProcedure().build() );
        this.clientInstalledProtocols = clientInstalledProtocols;
        this.serverInstalledProtocols = serverInstalledProtocols;
    }

    public RawIterator<AnyValue[],ProcedureException> apply( Context ctx, AnyValue[] input, ResourceTracker resourceTracker )
    {
        Stream<AnyValue[]> outbound = this.toOutputRows( this.clientInstalledProtocols, "outbound" );
        Stream<AnyValue[]> inbound = this.toOutputRows( this.serverInstalledProtocols, "inbound" );
        return Iterators.asRawIterator( Stream.concat( outbound, inbound ) );
    }

    private <T extends SocketAddress> Stream<AnyValue[]> toOutputRows( Supplier<Stream<Pair<T,ProtocolStack>>> installedProtocols, String orientation )
    {
        Comparator<Pair<T,ProtocolStack>> connectionInfoComparator = Comparator.comparing( ( entry ) -> {
            return ((SocketAddress) entry.first()).getHostname();
        } ).thenComparing( ( entry ) -> {
            return ((SocketAddress) entry.first()).getPort();
        } );
        return ((Stream) installedProtocols.get()).sorted( connectionInfoComparator ).map( ( entry ) -> {
            return this.buildRow( entry, orientation );
        } );
    }

    private <T extends SocketAddress> AnyValue[] buildRow( Pair<T,ProtocolStack> connectionInfo, String orientation )
    {
        T socketAddress = (SocketAddress) connectionInfo.first();
        ProtocolStack protocolStack = (ProtocolStack) connectionInfo.other();
        return new AnyValue[]{Values.stringValue( orientation ), Values.stringValue( socketAddress.toString() ),
                Values.stringValue( protocolStack.applicationProtocol().category() ),
                Values.stringValue( ((ApplicationProtocolVersion) protocolStack.applicationProtocol().implementation()).toString() ),
                Values.stringValue( this.modifierString( protocolStack ) )};
    }

    private String modifierString( ProtocolStack protocolStack )
    {
        return (String) protocolStack.modifierProtocols().stream().map( Protocol::implementation ).collect( Collectors.joining( ",", "[", "]" ) );
    }
}
