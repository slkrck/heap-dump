package com.neo4j.causalclustering.discovery.procedures;

import com.neo4j.causalclustering.core.consensus.RaftMachine;
import com.neo4j.causalclustering.discovery.RoleInfo;
import org.neo4j.dbms.database.DatabaseContext;
import org.neo4j.dbms.database.DatabaseManager;
import org.neo4j.internal.kernel.api.exceptions.ProcedureException;
import org.neo4j.kernel.api.exceptions.Status.General;

public class CoreRoleProcedure extends RoleProcedure
{
    public CoreRoleProcedure( DatabaseManager<?> databaseManager )
    {
        super( databaseManager );
    }

    RoleInfo role( DatabaseContext databaseContext ) throws ProcedureException
    {
        RaftMachine raftMachine = (RaftMachine) databaseContext.dependencies().resolveDependency( RaftMachine.class );
        if ( raftMachine == null )
        {
            throw new ProcedureException( General.UnknownError, "Unable to resolve role for database. This may be because the database is stopping.",
                    new Object[0] );
        }
        else
        {
            return raftMachine.isLeader() ? RoleInfo.LEADER : RoleInfo.FOLLOWER;
        }
    }
}
