package com.neo4j.causalclustering.discovery.procedures;

import com.neo4j.causalclustering.discovery.ClientConnectorAddresses;
import com.neo4j.causalclustering.discovery.CoreServerInfo;
import com.neo4j.causalclustering.discovery.DiscoveryServerInfo;
import com.neo4j.causalclustering.discovery.ReadReplicaInfo;
import com.neo4j.causalclustering.discovery.RoleInfo;
import com.neo4j.causalclustering.discovery.TopologyService;
import com.neo4j.causalclustering.identity.MemberId;

import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.neo4j.collection.RawIterator;
import org.neo4j.internal.helpers.collection.Iterators;
import org.neo4j.internal.kernel.api.exceptions.ProcedureException;
import org.neo4j.internal.kernel.api.procs.Neo4jTypes;
import org.neo4j.internal.kernel.api.procs.ProcedureSignature;
import org.neo4j.internal.kernel.api.procs.QualifiedName;
import org.neo4j.kernel.api.ResourceTracker;
import org.neo4j.kernel.api.procedure.Context;
import org.neo4j.kernel.api.procedure.CallableProcedure.BasicProcedure;
import org.neo4j.kernel.database.DatabaseIdRepository;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Values;
import org.neo4j.values.virtual.MapValueBuilder;
import org.neo4j.values.virtual.VirtualValues;

public class ClusterOverviewProcedure extends BasicProcedure
{
    public static final String PROCEDURE_NAME = "overview";
    private static final String[] PROCEDURE_NAMESPACE = new String[]{"dbms", "cluster"};
    private final TopologyService topologyService;
    private final DatabaseIdRepository databaseIdRepository;

    public ClusterOverviewProcedure( TopologyService topologyService, DatabaseIdRepository databaseIdRepository )
    {
        super( ProcedureSignature.procedureSignature( new QualifiedName( PROCEDURE_NAMESPACE, "overview" ) ).out( "id", Neo4jTypes.NTString ).out( "addresses",
                Neo4jTypes.NTList( Neo4jTypes.NTString ) ).out( "databases", Neo4jTypes.NTMap ).out( "groups",
                Neo4jTypes.NTList( Neo4jTypes.NTString ) ).description(
                "Overview of all currently accessible cluster members, their databases and roles." ).systemProcedure().build() );
        this.topologyService = topologyService;
        this.databaseIdRepository = databaseIdRepository;
    }

    private static AnyValue[] formatResultRow( ClusterOverviewProcedure.ResultRow row )
    {
        return new AnyValue[]{Values.stringValue( row.memberId.toString() ), formatAddresses( row ), formatDatabases( row ), formatGroups( row )};
    }

    private static AnyValue formatAddresses( ClusterOverviewProcedure.ResultRow row )
    {
        List<AnyValue> stringValues = (List) row.addresses.uriList().stream().map( URI::toString ).map( Values::stringValue ).collect( Collectors.toList() );
        return VirtualValues.fromList( stringValues );
    }

    private static AnyValue formatDatabases( ClusterOverviewProcedure.ResultRow row )
    {
        MapValueBuilder builder = new MapValueBuilder();
        Iterator var2 = row.databases.entrySet().iterator();

        while ( var2.hasNext() )
        {
            Entry<NamedDatabaseId,RoleInfo> entry = (Entry) var2.next();
            NamedDatabaseId databaseId = (NamedDatabaseId) entry.getKey();
            String roleString = ((RoleInfo) entry.getValue()).toString();
            builder.add( databaseId.name(), Values.stringValue( roleString ) );
        }

        return builder.build();
    }

    private static AnyValue formatGroups( ClusterOverviewProcedure.ResultRow row )
    {
        List<AnyValue> stringValues = (List) row.groups.stream().sorted().map( Values::stringValue ).collect( Collectors.toList() );
        return VirtualValues.fromList( stringValues );
    }

    public RawIterator<AnyValue[],ProcedureException> apply( Context ctx, AnyValue[] input, ResourceTracker resourceTracker )
    {
        ArrayList<ClusterOverviewProcedure.ResultRow> resultRows = new ArrayList();
        Iterator var5 = this.topologyService.allCoreServers().entrySet().iterator();

        Entry entry;
        ClusterOverviewProcedure.ResultRow row;
        while ( var5.hasNext() )
        {
            entry = (Entry) var5.next();
            row = this.buildResultRowForCore( (MemberId) entry.getKey(), (CoreServerInfo) entry.getValue() );
            resultRows.add( row );
        }

        var5 = this.topologyService.allReadReplicas().entrySet().iterator();

        while ( var5.hasNext() )
        {
            entry = (Entry) var5.next();
            row = this.buildResultRowForReadReplica( (MemberId) entry.getKey(), (ReadReplicaInfo) entry.getValue() );
            resultRows.add( row );
        }

        Stream<AnyValue[]> resultStream = resultRows.stream().sorted().map( ClusterOverviewProcedure::formatResultRow );
        return Iterators.asRawIterator( resultStream );
    }

    private ClusterOverviewProcedure.ResultRow buildResultRowForCore( MemberId memberId, CoreServerInfo coreInfo )
    {
        return this.buildResultRow( memberId, coreInfo, ( databaseId ) -> {
            return this.topologyService.lookupRole( databaseId, memberId );
        } );
    }

    private ClusterOverviewProcedure.ResultRow buildResultRowForReadReplica( MemberId memberId, ReadReplicaInfo readReplicaInfo )
    {
        return this.buildResultRow( memberId, readReplicaInfo, ( ignore ) -> {
            return RoleInfo.READ_REPLICA;
        } );
    }

    private ClusterOverviewProcedure.ResultRow buildResultRow( MemberId memberId, DiscoveryServerInfo serverInfo, Function<NamedDatabaseId,RoleInfo> result )
    {
        Map<NamedDatabaseId,RoleInfo> databases = (Map) serverInfo.startedDatabaseIds().stream().flatMap( ( databaseId ) -> {
            return this.databaseIdRepository.getById( databaseId ).stream();
        } ).collect( Collectors.toMap( Function.identity(), result ) );
        return new ClusterOverviewProcedure.ResultRow( memberId.getUuid(), serverInfo.connectors(), databases, serverInfo.groups() );
    }

    static class ResultRow implements Comparable<ClusterOverviewProcedure.ResultRow>
    {
        final UUID memberId;
        final ClientConnectorAddresses addresses;
        final Map<NamedDatabaseId,RoleInfo> databases;
        final Set<String> groups;

        ResultRow( UUID memberId, ClientConnectorAddresses addresses, Map<NamedDatabaseId,RoleInfo> databases, Set<String> groups )
        {
            this.memberId = memberId;
            this.addresses = addresses;
            this.databases = databases;
            this.groups = groups;
        }

        public int compareTo( ClusterOverviewProcedure.ResultRow other )
        {
            return this.memberId.compareTo( other.memberId );
        }
    }
}
