package com.neo4j.causalclustering.discovery;

import com.neo4j.causalclustering.core.CausalClusteringSettings;

import java.util.Collection;
import java.util.Objects;
import java.util.Set;

import org.neo4j.configuration.Config;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.kernel.database.DatabaseId;

public class ReadReplicaInfo implements DiscoveryServerInfo
{
    private final SocketAddress catchupServerAddress;
    private final ClientConnectorAddresses clientConnectorAddresses;
    private final Set<String> groups;
    private final Set<DatabaseId> databaseIds;

    public ReadReplicaInfo( ClientConnectorAddresses clientConnectorAddresses, SocketAddress catchupServerAddress, Set<String> groups,
            Set<DatabaseId> databaseIds )
    {
        this.clientConnectorAddresses = clientConnectorAddresses;
        this.catchupServerAddress = catchupServerAddress;
        this.groups = groups;
        this.databaseIds = databaseIds;
    }

    public static ReadReplicaInfo from( Config config, Set<DatabaseId> databaseIds )
    {
        ClientConnectorAddresses connectorUris = ClientConnectorAddresses.extractFromConfig( config );
        SocketAddress catchupAddress = (SocketAddress) config.get( CausalClusteringSettings.transaction_advertised_address );
        Set<String> groups = Set.copyOf( (Collection) config.get( CausalClusteringSettings.server_groups ) );
        return new ReadReplicaInfo( connectorUris, catchupAddress, groups, databaseIds );
    }

    public Set<DatabaseId> startedDatabaseIds()
    {
        return this.databaseIds;
    }

    public ClientConnectorAddresses connectors()
    {
        return this.clientConnectorAddresses;
    }

    public SocketAddress catchupServer()
    {
        return this.catchupServerAddress;
    }

    public Set<String> groups()
    {
        return this.groups;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            ReadReplicaInfo that = (ReadReplicaInfo) o;
            return Objects.equals( this.catchupServerAddress, that.catchupServerAddress ) &&
                    Objects.equals( this.clientConnectorAddresses, that.clientConnectorAddresses ) && Objects.equals( this.groups, that.groups ) &&
                    Objects.equals( this.databaseIds, that.databaseIds );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.catchupServerAddress, this.clientConnectorAddresses, this.groups, this.databaseIds} );
    }

    public String toString()
    {
        return "ReadReplicaInfo{catchupServerAddress=" + this.catchupServerAddress + ", clientConnectorAddresses=" + this.clientConnectorAddresses +
                ", groups=" + this.groups + ", startedDatabaseIds=" + this.databaseIds + "}";
    }
}
