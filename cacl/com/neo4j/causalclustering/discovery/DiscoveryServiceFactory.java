package com.neo4j.causalclustering.discovery;

import com.neo4j.causalclustering.discovery.member.DiscoveryMemberFactory;
import com.neo4j.causalclustering.identity.MemberId;

import java.time.Clock;

import org.neo4j.configuration.Config;
import org.neo4j.logging.LogProvider;
import org.neo4j.monitoring.Monitors;
import org.neo4j.scheduler.JobScheduler;
import org.neo4j.ssl.config.SslPolicyLoader;

public interface DiscoveryServiceFactory
{
    CoreTopologyService coreTopologyService( Config var1, MemberId var2, JobScheduler var3, LogProvider var4, LogProvider var5, RemoteMembersResolver var6,
            RetryStrategy var7, SslPolicyLoader var8, DiscoveryMemberFactory var9, Monitors var10, Clock var11 );

    TopologyService readReplicaTopologyService( Config var1, LogProvider var2, JobScheduler var3, MemberId var4, RemoteMembersResolver var5,
            SslPolicyLoader var6, DiscoveryMemberFactory var7, Clock var8 );
}
