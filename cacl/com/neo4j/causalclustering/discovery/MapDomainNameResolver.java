package com.neo4j.causalclustering.discovery;

import java.net.InetAddress;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class MapDomainNameResolver implements DomainNameResolver
{
    private final Map<String,InetAddress[]> domainNameMapping;

    public MapDomainNameResolver( Map<String,InetAddress[]> mapping )
    {
        this.domainNameMapping = mapping;
    }

    private static InetAddress inetAddress( String address )
    {
        try
        {
            return InetAddress.getByName( address );
        }
        catch ( java.net.UnknownHostException var2 )
        {
            throw new UnknownHostException( var2 );
        }
    }

    public InetAddress[] resolveDomainName( String hostname ) throws UnknownHostException
    {
        return (InetAddress[]) Optional.ofNullable( (InetAddress[]) this.domainNameMapping.get( hostname ) ).orElse( new InetAddress[0] );
    }

    public void setHostnameAddresses( String hostname, Collection<String> addresses )
    {
        InetAddress[] processedAddresses = new InetAddress[addresses.size()];
        ((List) addresses.stream().map( MapDomainNameResolver::inetAddress ).collect( Collectors.toList() )).toArray( processedAddresses );
        this.domainNameMapping.put( hostname, processedAddresses );
    }
}
