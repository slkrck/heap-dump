package com.neo4j.causalclustering.discovery.member;

import com.neo4j.causalclustering.identity.MemberId;

import java.util.Set;

import org.neo4j.kernel.database.DatabaseId;

class DefaultDiscoveryMember implements DiscoveryMember
{
    private final MemberId id;
    private final Set<DatabaseId> startedDatabases;

    DefaultDiscoveryMember( MemberId id, Set<DatabaseId> startedDatabases )
    {
        this.id = id;
        this.startedDatabases = startedDatabases;
    }

    public MemberId id()
    {
        return this.id;
    }

    public Set<DatabaseId> startedDatabases()
    {
        return this.startedDatabases;
    }

    public String toString()
    {
        return "DefaultDiscoveryMember{id=" + this.id + ", startedDatabases=" + this.startedDatabases + "}";
    }
}
