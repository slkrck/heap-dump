package com.neo4j.causalclustering.discovery.member;

import com.neo4j.causalclustering.identity.MemberId;

@FunctionalInterface
public interface DiscoveryMemberFactory
{
    DiscoveryMember create( MemberId var1 );
}
