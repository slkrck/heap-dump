package com.neo4j.causalclustering.discovery.member;

import com.neo4j.causalclustering.identity.MemberId;

import java.util.Set;

import org.neo4j.kernel.database.DatabaseId;

public interface DiscoveryMember
{
    MemberId id();

    Set<DatabaseId> startedDatabases();
}
