package com.neo4j.causalclustering.discovery.member;

import com.neo4j.causalclustering.identity.MemberId;

import java.util.Set;
import java.util.stream.Collectors;

import org.neo4j.dbms.DatabaseStateService;
import org.neo4j.dbms.database.DatabaseContext;
import org.neo4j.dbms.database.DatabaseManager;
import org.neo4j.kernel.database.Database;
import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.kernel.database.NamedDatabaseId;

public class DefaultDiscoveryMemberFactory implements DiscoveryMemberFactory
{
    private final DatabaseManager<?> databaseManager;
    private final DatabaseStateService databaseStateService;

    public DefaultDiscoveryMemberFactory( DatabaseManager<?> databaseManager, DatabaseStateService databaseStateService )
    {
        this.databaseManager = databaseManager;
        this.databaseStateService = databaseStateService;
    }

    public DiscoveryMember create( MemberId id )
    {
        Set<DatabaseId> startedDatabases = this.startedDatabases();
        return new DefaultDiscoveryMember( id, startedDatabases );
    }

    private Set<DatabaseId> startedDatabases()
    {
        return (Set) this.databaseManager.registeredDatabases().values().stream().map( DatabaseContext::database ).filter( ( db ) -> {
            return !this.hasFailed( db.getNamedDatabaseId() ) && db.isStarted();
        } ).map( Database::getNamedDatabaseId ).map( NamedDatabaseId::databaseId ).collect( Collectors.toUnmodifiableSet() );
    }

    private boolean hasFailed( NamedDatabaseId namedDatabaseId )
    {
        return this.databaseStateService.causeOfFailure( namedDatabaseId ).isPresent();
    }
}
