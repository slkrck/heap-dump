package com.neo4j.causalclustering.discovery;

import com.neo4j.causalclustering.core.CausalClusteringSettings;

import java.time.Duration;
import java.util.Collection;
import java.util.concurrent.TimeoutException;

import org.neo4j.configuration.Config;
import org.neo4j.configuration.helpers.SocketAddress;

public abstract class RetryingHostnameResolver implements HostnameResolver
{
    private final int minResolvedAddresses;
    private final RetryStrategy retryStrategy;

    RetryingHostnameResolver( Config config, RetryStrategy retryStrategy )
    {
        this.minResolvedAddresses = (Integer) config.get( CausalClusteringSettings.minimum_core_cluster_size_at_formation );
        this.retryStrategy = retryStrategy;
    }

    static RetryStrategy defaultRetryStrategy( Config config )
    {
        long retryIntervalMillis = ((Duration) config.get( CausalClusteringSettings.discovery_resolution_retry_interval )).toMillis();
        long clusterBindingTimeout = ((Duration) config.get( CausalClusteringSettings.discovery_resolution_timeout )).toMillis();
        long numRetries = clusterBindingTimeout / retryIntervalMillis + 1L;
        return new RetryStrategy( retryIntervalMillis, numRetries );
    }

    public final Collection<SocketAddress> resolve( SocketAddress advertisedSocketAddress )
    {
        try
        {
            return (Collection) this.retryStrategy.apply( () -> {
                return this.resolveOnce( advertisedSocketAddress );
            }, ( addrs ) -> {
                return addrs.size() >= this.minResolvedAddresses;
            } );
        }
        catch ( TimeoutException var3 )
        {
            return this.resolveOnce( advertisedSocketAddress );
        }
    }

    protected abstract Collection<SocketAddress> resolveOnce( SocketAddress var1 );
}
