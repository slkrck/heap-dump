package com.neo4j.causalclustering.discovery;

public enum PublishRaftIdOutcome
{
    FAILED_PUBLISH,
    SUCCESSFUL_PUBLISH_BY_ME,
    SUCCESSFUL_PUBLISH_BY_OTHER;
}
