package com.neo4j.causalclustering.discovery;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.neo4j.causalclustering.core.CausalClusteringSettings;
import com.neo4j.causalclustering.discovery.kubernetes.KubernetesType;
import com.neo4j.causalclustering.discovery.kubernetes.ServiceList;
import com.neo4j.causalclustering.discovery.kubernetes.Status;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.client.api.ContentResponse;
import org.eclipse.jetty.http.HttpHeader;
import org.eclipse.jetty.http.HttpMethod;
import org.eclipse.jetty.http.MimeTypes.Type;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.internal.helpers.collection.Pair;
import org.neo4j.logging.Log;
import org.neo4j.logging.internal.LogService;
import org.neo4j.util.Preconditions;

public class KubernetesResolver implements RemoteMembersResolver
{
    private final KubernetesResolver.KubernetesClient kubernetesClient;
    private final HttpClient httpClient;
    private final Log log;

    private KubernetesResolver( LogService logService, Config config )
    {
        this.log = logService.getInternalLog( this.getClass() );
        SslContextFactory sslContextFactory = this.createSslContextFactory( config );
        this.httpClient = new HttpClient( sslContextFactory );
        String token = this.read( ((Path) config.get( CausalClusteringSettings.kubernetes_token )).toFile() );
        String namespace = this.read( ((Path) config.get( CausalClusteringSettings.kubernetes_namespace )).toFile() );
        this.kubernetesClient = new KubernetesResolver.KubernetesClient( logService, this.httpClient, token, namespace, config,
                RetryingHostnameResolver.defaultRetryStrategy( config ) );
    }

    public static RemoteMembersResolver resolver( LogService logService, Config config )
    {
        return new KubernetesResolver( logService, config );
    }

    private SslContextFactory createSslContextFactory( Config config )
    {
        File caCert = ((Path) config.get( CausalClusteringSettings.kubernetes_ca_crt )).toFile();

        try
        {
            SecurePassword password = new SecurePassword( 16, new SecureRandom() );

            SslContextFactory var7;
            try
            {
                InputStream caCertStream = Files.newInputStream( caCert.toPath(), StandardOpenOption.READ );

                try
                {
                    KeyStore keyStore = this.loadKeyStore( password, caCertStream );
                    SslContextFactory sslContextFactory = new SslContextFactory();
                    sslContextFactory.setTrustStore( keyStore );
                    sslContextFactory.setTrustStorePassword( String.valueOf( password.password() ) );
                    var7 = sslContextFactory;
                }
                catch ( Throwable var10 )
                {
                    if ( caCertStream != null )
                    {
                        try
                        {
                            caCertStream.close();
                        }
                        catch ( Throwable var9 )
                        {
                            var10.addSuppressed( var9 );
                        }
                    }

                    throw var10;
                }

                if ( caCertStream != null )
                {
                    caCertStream.close();
                }
            }
            catch ( Throwable var11 )
            {
                try
                {
                    password.close();
                }
                catch ( Throwable var8 )
                {
                    var11.addSuppressed( var8 );
                }

                throw var11;
            }

            password.close();
            return var7;
        }
        catch ( Exception var12 )
        {
            throw new IllegalStateException( "Unable to load CA certificate for Kubernetes", var12 );
        }
    }

    private KeyStore loadKeyStore( SecurePassword password, InputStream caCertStream )
            throws CertificateException, KeyStoreException, IOException, NoSuchAlgorithmException
    {
        CertificateFactory certificateFactory = CertificateFactory.getInstance( "X.509" );
        Collection<? extends Certificate> certificates = certificateFactory.generateCertificates( caCertStream );
        Preconditions.checkState( !certificates.isEmpty(), "Expected non empty Kubernetes CA certificates" );
        KeyStore keyStore = KeyStore.getInstance( KeyStore.getDefaultType() );
        keyStore.load( (InputStream) null, password.password() );
        int idx = 0;
        Iterator var7 = certificates.iterator();

        while ( var7.hasNext() )
        {
            Certificate certificate = (Certificate) var7.next();
            int var10001 = idx++;
            keyStore.setCertificateEntry( "ca" + var10001, certificate );
        }

        return keyStore;
    }

    private String read( File file )
    {
        try
        {
            Optional<String> line = Files.lines( file.toPath() ).findFirst();
            if ( line.isPresent() )
            {
                return (String) line.get();
            }
            else
            {
                throw new IllegalStateException( String.format( "Expected file at %s to have at least 1 line", file ) );
            }
        }
        catch ( IOException var3 )
        {
            throw new IllegalArgumentException( "Unable to read file " + file, var3 );
        }
    }

    public <C extends Collection<T>, T> C resolve( Function<SocketAddress,T> transform, Supplier<C> collectionFactory )
    {
        Collection var3;
        try
        {
            this.httpClient.start();
            var3 = (Collection) this.kubernetesClient.resolve( (SocketAddress) null ).stream().map( transform ).collect(
                    Collectors.toCollection( collectionFactory ) );
        }
        catch ( Exception var12 )
        {
            throw new IllegalStateException( "Unable to query Kubernetes API", var12 );
        }
        finally
        {
            try
            {
                this.httpClient.stop();
            }
            catch ( Exception var11 )
            {
                this.log.warn( "Unable to shut down HTTP client", var11 );
            }
        }

        return var3;
    }

    public boolean useOverrides()
    {
        return false;
    }

    private static class Parser implements KubernetesType.Visitor<Collection<SocketAddress>>
    {
        private final String portName;
        private final String namespace;

        private Parser( String portName, String namespace )
        {
            this.portName = portName;
            this.namespace = namespace;
        }

        public Collection<SocketAddress> visit( Status status )
        {
            String message = String.format( "Unable to contact Kubernetes API. Status: %s", status );
            throw new IllegalStateException( message );
        }

        public Collection<SocketAddress> visit( ServiceList serviceList )
        {
            Stream<Pair<String,ServiceList.Service.ServiceSpec.ServicePort>> serviceNamePortStream =
                    serviceList.items().stream().filter( this::notDeleted ).flatMap( this::extractServicePort );
            return (Collection) serviceNamePortStream.map( ( serviceNamePort ) -> {
                return new SocketAddress( String.format( "%s.%s.svc.cluster.local", serviceNamePort.first(), this.namespace ),
                        ((ServiceList.Service.ServiceSpec.ServicePort) serviceNamePort.other()).port() );
            } ).collect( Collectors.toSet() );
        }

        private boolean notDeleted( ServiceList.Service service )
        {
            return service.metadata().deletionTimestamp() == null;
        }

        private Stream<Pair<String,ServiceList.Service.ServiceSpec.ServicePort>> extractServicePort( ServiceList.Service service )
        {
            return service.spec().ports().stream().filter( ( port ) -> {
                return this.portName.equals( port.name() );
            } ).map( ( port ) -> {
                return Pair.of( service.metadata().name(), port );
            } );
        }
    }

    static class KubernetesClient extends RetryingHostnameResolver
    {
        static final String path = "/api/v1/namespaces/%s/services";
        private final Log log;
        private final Log userLog;
        private final HttpClient httpClient;
        private final String token;
        private final String namespace;
        private final String labelSelector;
        private final ObjectMapper objectMapper;
        private final String portName;
        private final SocketAddress kubernetesAddress;

        KubernetesClient( LogService logService, HttpClient httpClient, String token, String namespace, Config config, RetryStrategy retryStrategy )
        {
            super( config, retryStrategy );
            this.log = logService.getInternalLog( this.getClass() );
            this.userLog = logService.getUserLog( this.getClass() );
            this.token = token;
            this.namespace = namespace;
            this.kubernetesAddress = (SocketAddress) config.get( CausalClusteringSettings.kubernetes_address );
            this.labelSelector = (String) config.get( CausalClusteringSettings.kubernetes_label_selector );
            this.portName = (String) config.get( CausalClusteringSettings.kubernetes_service_port_name );
            this.httpClient = httpClient;
            this.objectMapper = (new ObjectMapper()).configure( DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false );
        }

        protected Collection<SocketAddress> resolveOnce( SocketAddress ignored )
        {
            try
            {
                ContentResponse response =
                        this.httpClient.newRequest( this.kubernetesAddress.getHostname(), this.kubernetesAddress.getPort() ).method( HttpMethod.GET ).scheme(
                                "https" ).path( String.format( "/api/v1/namespaces/%s/services", this.namespace ) ).param( "labelSelector",
                                this.labelSelector ).header( HttpHeader.AUTHORIZATION, "Bearer " + this.token ).accept(
                                new String[]{Type.APPLICATION_JSON.asString()} ).send();
                this.log.info( "Received from k8s api: " + response.getContentAsString() );
                KubernetesType serviceList = (KubernetesType) this.objectMapper.readValue( response.getContent(), KubernetesType.class );
                Collection<SocketAddress> addresses = (Collection) serviceList.handle( new KubernetesResolver.Parser( this.portName, this.namespace ) );
                this.userLog.info( "Resolved %s from Kubernetes API at %s namespace %s labelSelector %s",
                        new Object[]{addresses, this.kubernetesAddress, this.namespace, this.labelSelector} );
                if ( addresses.isEmpty() )
                {
                    this.log.error( "Resolved empty hosts from Kubernetes API at %s namespace %s labelSelector %s",
                            new Object[]{this.kubernetesAddress, this.namespace, this.labelSelector} );
                }

                return addresses;
            }
            catch ( IOException var5 )
            {
                this.log.error( "Failed to parse result from Kubernetes API", var5 );
                return Collections.emptySet();
            }
            catch ( ExecutionException | TimeoutException | InterruptedException var6 )
            {
                this.log.error( String.format( "Failed to resolve hosts from Kubernetes API at %s namespace %s labelSelector %s", this.kubernetesAddress,
                        this.namespace, this.labelSelector ), var6 );
                return Collections.emptySet();
            }
        }
    }
}
