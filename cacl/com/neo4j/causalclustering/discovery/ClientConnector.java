package com.neo4j.causalclustering.discovery;

import org.neo4j.configuration.helpers.SocketAddress;

public interface ClientConnector
{
    ClientConnectorAddresses connectors();

    default SocketAddress boltAddress()
    {
        return this.connectors().boltAddress();
    }
}
