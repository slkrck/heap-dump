package com.neo4j.causalclustering.discovery;

public enum RoleInfo
{
    LEADER,
    FOLLOWER,
    READ_REPLICA,
    UNKNOWN;
}
