package com.neo4j.causalclustering.discovery;

import java.util.Set;

public interface GroupedServer
{
    Set<String> groups();
}
