package com.neo4j.causalclustering.discovery;

public class UnknownHostException extends RuntimeException
{
    UnknownHostException( java.net.UnknownHostException unknownHostException )
    {
        super( unknownHostException );
    }
}
