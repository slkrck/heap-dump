package com.neo4j.causalclustering.discovery;

import java.net.InetAddress;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.neo4j.configuration.Config;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.logging.Log;
import org.neo4j.logging.internal.LogService;

public class DnsHostnameResolver extends RetryingHostnameResolver
{
    private final Log userLog;
    private final Log log;
    private final DomainNameResolver domainNameResolver;

    DnsHostnameResolver( LogService logService, DomainNameResolver domainNameResolver, Config config, RetryStrategy retryStrategy )
    {
        super( config, retryStrategy );
        this.log = logService.getInternalLog( this.getClass() );
        this.userLog = logService.getUserLog( this.getClass() );
        this.domainNameResolver = domainNameResolver;
    }

    public static RemoteMembersResolver resolver( LogService logService, DomainNameResolver domainNameResolver, Config config )
    {
        DnsHostnameResolver hostnameResolver = new DnsHostnameResolver( logService, domainNameResolver, config, defaultRetryStrategy( config ) );
        return new InitialDiscoveryMembersResolver( hostnameResolver, config );
    }

    protected Collection<SocketAddress> resolveOnce( SocketAddress initialAddress )
    {
        Set<SocketAddress> addresses = new HashSet();
        InetAddress[] ipAddresses = this.domainNameResolver.resolveDomainName( initialAddress.getHostname() );
        if ( ipAddresses.length == 0 )
        {
            this.log.error( "Failed to resolve host '%s'", new Object[]{initialAddress.getHostname()} );
        }

        InetAddress[] var4 = ipAddresses;
        int var5 = ipAddresses.length;

        for ( int var6 = 0; var6 < var5; ++var6 )
        {
            InetAddress ipAddress = var4[var6];
            addresses.add( new SocketAddress( ipAddress.getHostAddress(), initialAddress.getPort() ) );
        }

        this.userLog.info( "Resolved initial host '%s' to %s", new Object[]{initialAddress, addresses} );
        return addresses;
    }
}
