package com.neo4j.causalclustering.discovery;

import com.neo4j.causalclustering.core.consensus.LeaderInfo;
import com.neo4j.causalclustering.identity.RaftId;

import java.util.concurrent.TimeoutException;

import org.neo4j.kernel.database.NamedDatabaseId;

public interface CoreTopologyService extends TopologyService
{
    void addLocalCoreTopologyListener( CoreTopologyService.Listener var1 );

    void removeLocalCoreTopologyListener( CoreTopologyService.Listener var1 );

    PublishRaftIdOutcome publishRaftId( RaftId var1 ) throws TimeoutException;

    void setLeader( LeaderInfo var1, NamedDatabaseId var2 );

    void handleStepDown( long var1, NamedDatabaseId var3 );

    boolean canBootstrapRaftGroup( NamedDatabaseId var1 );

    public interface Listener
    {
        void onCoreTopologyChange( DatabaseCoreTopology var1 );

        NamedDatabaseId namedDatabaseId();
    }
}
