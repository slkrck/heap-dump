package com.neo4j.causalclustering.discovery;

import com.neo4j.causalclustering.catchup.CatchupAddressResolutionException;
import com.neo4j.causalclustering.core.consensus.LeaderInfo;
import com.neo4j.causalclustering.discovery.akka.database.state.DiscoveryDatabaseState;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.dbms.DatabaseStateChangedListener;

import java.util.Map;

import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.lifecycle.Lifecycle;

public interface TopologyService extends Lifecycle, DatabaseStateChangedListener
{
    void onDatabaseStart( NamedDatabaseId var1 );

    void onDatabaseStop( NamedDatabaseId var1 );

    MemberId memberId();

    Map<MemberId,CoreServerInfo> allCoreServers();

    Map<MemberId,ReadReplicaInfo> allReadReplicas();

    DatabaseCoreTopology coreTopologyForDatabase( NamedDatabaseId var1 );

    DatabaseReadReplicaTopology readReplicaTopologyForDatabase( NamedDatabaseId var1 );

    SocketAddress lookupCatchupAddress( MemberId var1 ) throws CatchupAddressResolutionException;

    LeaderInfo getLeader( NamedDatabaseId var1 );

    RoleInfo lookupRole( NamedDatabaseId var1, MemberId var2 );

    DiscoveryDatabaseState lookupDatabaseState( NamedDatabaseId var1, MemberId var2 );

    Map<MemberId,DiscoveryDatabaseState> allCoreStatesForDatabase( NamedDatabaseId var1 );

    Map<MemberId,DiscoveryDatabaseState> allReadReplicaStatesForDatabase( NamedDatabaseId var1 );

    boolean isHealthy();
}
