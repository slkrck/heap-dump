package com.neo4j.causalclustering.discovery;

import com.neo4j.causalclustering.core.CausalClusteringSettings;

import java.util.Collection;
import java.util.Objects;
import java.util.Set;

import org.neo4j.configuration.Config;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.kernel.database.DatabaseId;

public class CoreServerInfo implements DiscoveryServerInfo
{
    private final SocketAddress raftServer;
    private final SocketAddress catchupServer;
    private final ClientConnectorAddresses clientConnectorAddresses;
    private final Set<String> groups;
    private final Set<DatabaseId> startedDatabaseIds;
    private final boolean refuseToBeLeader;

    public CoreServerInfo( SocketAddress raftServer, SocketAddress catchupServer, ClientConnectorAddresses clientConnectorAddresses, Set<String> groups,
            Set<DatabaseId> startedDatabaseIds, boolean refuseToBeLeader )
    {
        this.raftServer = raftServer;
        this.catchupServer = catchupServer;
        this.clientConnectorAddresses = clientConnectorAddresses;
        this.groups = groups;
        this.startedDatabaseIds = startedDatabaseIds;
        this.refuseToBeLeader = refuseToBeLeader;
    }

    public static CoreServerInfo fromRaw( Config config, Set<DatabaseId> databaseIds )
    {
        SocketAddress raftAddress = (SocketAddress) config.get( CausalClusteringSettings.raft_advertised_address );
        SocketAddress catchupAddress = (SocketAddress) config.get( CausalClusteringSettings.transaction_advertised_address );
        ClientConnectorAddresses connectorUris = ClientConnectorAddresses.extractFromConfig( config );
        Set<String> groups = Set.copyOf( (Collection) config.get( CausalClusteringSettings.server_groups ) );
        Boolean refuseToBeLeader = (Boolean) config.get( CausalClusteringSettings.refuse_to_be_leader );
        return new CoreServerInfo( raftAddress, catchupAddress, connectorUris, groups, databaseIds, refuseToBeLeader );
    }

    public static CoreServerInfo from( Config config, Set<DatabaseId> databaseIds )
    {
        return fromRaw( config, databaseIds );
    }

    public Set<DatabaseId> startedDatabaseIds()
    {
        return this.startedDatabaseIds;
    }

    public SocketAddress getRaftServer()
    {
        return this.raftServer;
    }

    public SocketAddress catchupServer()
    {
        return this.catchupServer;
    }

    public ClientConnectorAddresses connectors()
    {
        return this.clientConnectorAddresses;
    }

    public Set<String> groups()
    {
        return this.groups;
    }

    public boolean refusesToBeLeader()
    {
        return this.refuseToBeLeader;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            CoreServerInfo that = (CoreServerInfo) o;
            return this.refuseToBeLeader == that.refuseToBeLeader && Objects.equals( this.raftServer, that.raftServer ) &&
                    Objects.equals( this.catchupServer, that.catchupServer ) &&
                    Objects.equals( this.clientConnectorAddresses, that.clientConnectorAddresses ) && Objects.equals( this.groups, that.groups ) &&
                    Objects.equals( this.startedDatabaseIds, that.startedDatabaseIds );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash(
                new Object[]{this.raftServer, this.catchupServer, this.clientConnectorAddresses, this.groups, this.startedDatabaseIds, this.refuseToBeLeader} );
    }

    public String toString()
    {
        return "CoreServerInfo{raftServer=" + this.raftServer + ", catchupServer=" + this.catchupServer + ", clientConnectorAddresses=" +
                this.clientConnectorAddresses + ", groups=" + this.groups + ", startedDatabaseIds=" + this.startedDatabaseIds + ", refuseToBeLeader=" +
                this.refuseToBeLeader + "}";
    }
}
