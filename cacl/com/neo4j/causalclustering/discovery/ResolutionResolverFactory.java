package com.neo4j.causalclustering.discovery;

import com.neo4j.causalclustering.core.CausalClusteringSettings;
import com.neo4j.causalclustering.core.DiscoveryType;
import org.neo4j.configuration.Config;
import org.neo4j.logging.internal.LogService;

public class ResolutionResolverFactory
{
    public static RemoteMembersResolver chooseResolver( Config config, LogService logService )
    {
        DiscoveryType discoveryType = (DiscoveryType) config.get( CausalClusteringSettings.discovery_type );
        return discoveryType.getHostnameResolver( logService, config );
    }
}
