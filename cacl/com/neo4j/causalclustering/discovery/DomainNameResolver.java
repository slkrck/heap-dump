package com.neo4j.causalclustering.discovery;

import java.net.InetAddress;

public interface DomainNameResolver
{
    InetAddress[] resolveDomainName( String var1 ) throws UnknownHostException;
}
