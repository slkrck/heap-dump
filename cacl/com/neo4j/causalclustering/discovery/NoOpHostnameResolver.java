package com.neo4j.causalclustering.discovery;

import java.util.Collection;
import java.util.Collections;

import org.neo4j.configuration.Config;
import org.neo4j.configuration.helpers.SocketAddress;

public class NoOpHostnameResolver implements HostnameResolver
{
    public static RemoteMembersResolver resolver( Config config )
    {
        return new InitialDiscoveryMembersResolver( new NoOpHostnameResolver(), config );
    }

    public Collection<SocketAddress> resolve( SocketAddress advertisedSocketAddresses )
    {
        return Collections.singleton( advertisedSocketAddresses );
    }

    public boolean useOverrides()
    {
        return true;
    }
}
