package com.neo4j.causalclustering.discovery;

import com.neo4j.causalclustering.core.state.storage.SafeChannelMarshal;
import com.neo4j.causalclustering.messaging.marshalling.StringMarshal;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.neo4j.configuration.Config;
import org.neo4j.configuration.connectors.BoltConnector;
import org.neo4j.configuration.connectors.HttpConnector;
import org.neo4j.configuration.connectors.HttpsConnector;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.configuration.helpers.SocketAddressParser;
import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class ClientConnectorAddresses implements Iterable<ClientConnectorAddresses.ConnectorUri>
{
    private final List<ClientConnectorAddresses.ConnectorUri> connectorUris;

    public ClientConnectorAddresses( List<ClientConnectorAddresses.ConnectorUri> connectorUris )
    {
        this.connectorUris = connectorUris;
    }

    public static ClientConnectorAddresses extractFromConfig( Config config )
    {
        List<ClientConnectorAddresses.ConnectorUri> connectorUris = new ArrayList();
        connectorUris.add( new ClientConnectorAddresses.ConnectorUri( ClientConnectorAddresses.Scheme.bolt,
                (SocketAddress) config.get( BoltConnector.advertised_address ) ) );
        if ( (Boolean) config.get( HttpConnector.enabled ) )
        {
            connectorUris.add( new ClientConnectorAddresses.ConnectorUri( ClientConnectorAddresses.Scheme.http,
                    (SocketAddress) config.get( HttpConnector.advertised_address ) ) );
        }

        if ( (Boolean) config.get( HttpsConnector.enabled ) )
        {
            connectorUris.add( new ClientConnectorAddresses.ConnectorUri( ClientConnectorAddresses.Scheme.https,
                    (SocketAddress) config.get( HttpsConnector.advertised_address ) ) );
        }

        return new ClientConnectorAddresses( connectorUris );
    }

    static ClientConnectorAddresses fromString( String value )
    {
        return new ClientConnectorAddresses( (List) Stream.of( value.split( "," ) ).map( ( x$0 ) -> {
            return ClientConnectorAddresses.ConnectorUri.fromString( x$0 );
        } ).collect( Collectors.toList() ) );
    }

    public SocketAddress boltAddress()
    {
        return ((ClientConnectorAddresses.ConnectorUri) this.connectorUris.stream().filter( ( connectorUri ) -> {
            return connectorUri.scheme == ClientConnectorAddresses.Scheme.bolt;
        } ).findFirst().orElseThrow( () -> {
            return new IllegalArgumentException( "A Bolt connector must be configured to run a cluster" );
        } )).socketAddress;
    }

    public List<URI> uriList()
    {
        return (List) this.connectorUris.stream().map( ( rec$ ) -> {
            return ((ClientConnectorAddresses.ConnectorUri) rec$).toUri();
        } ).collect( Collectors.toList() );
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            ClientConnectorAddresses that = (ClientConnectorAddresses) o;
            return Objects.equals( this.connectorUris, that.connectorUris );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.connectorUris} );
    }

    public String toString()
    {
        return (String) this.connectorUris.stream().map( ClientConnectorAddresses.ConnectorUri::toString ).collect( Collectors.joining( "," ) );
    }

    public Iterator<ClientConnectorAddresses.ConnectorUri> iterator()
    {
        return this.connectorUris.iterator();
    }

    public static enum Scheme
    {
        bolt,
        http,
        https;
    }

    public static class Marshal extends SafeChannelMarshal<ClientConnectorAddresses>
    {
        protected ClientConnectorAddresses unmarshal0( ReadableChannel channel ) throws IOException
        {
            int size = channel.getInt();
            List<ClientConnectorAddresses.ConnectorUri> connectorUris = new ArrayList( size );

            for ( int i = 0; i < size; ++i )
            {
                String schemeName = StringMarshal.unmarshal( channel );
                String hostName = StringMarshal.unmarshal( channel );
                int port = channel.getInt();
                connectorUris.add( new ClientConnectorAddresses.ConnectorUri( ClientConnectorAddresses.Scheme.valueOf( schemeName ),
                        new SocketAddress( hostName, port ) ) );
            }

            return new ClientConnectorAddresses( connectorUris );
        }

        public void marshal( ClientConnectorAddresses connectorUris, WritableChannel channel ) throws IOException
        {
            channel.putInt( connectorUris.connectorUris.size() );
            Iterator var3 = connectorUris.iterator();

            while ( var3.hasNext() )
            {
                ClientConnectorAddresses.ConnectorUri uri = (ClientConnectorAddresses.ConnectorUri) var3.next();
                StringMarshal.marshal( channel, uri.scheme.name() );
                StringMarshal.marshal( channel, uri.socketAddress.getHostname() );
                channel.putInt( uri.socketAddress.getPort() );
            }
        }
    }

    public static class ConnectorUri
    {
        private final ClientConnectorAddresses.Scheme scheme;
        private final SocketAddress socketAddress;

        public ConnectorUri( ClientConnectorAddresses.Scheme scheme, SocketAddress socketAddress )
        {
            this.scheme = scheme;
            this.socketAddress = socketAddress;
        }

        private static ClientConnectorAddresses.ConnectorUri fromString( String string )
        {
            URI uri = URI.create( string );
            SocketAddress advertisedSocketAddress = SocketAddressParser.socketAddress( uri.getAuthority(), SocketAddress::new );
            return new ClientConnectorAddresses.ConnectorUri( ClientConnectorAddresses.Scheme.valueOf( uri.getScheme() ), advertisedSocketAddress );
        }

        private URI toUri()
        {
            try
            {
                return new URI( this.scheme.name().toLowerCase(), (String) null, this.socketAddress.getHostname(), this.socketAddress.getPort(), (String) null,
                        (String) null, (String) null );
            }
            catch ( URISyntaxException var2 )
            {
                throw new IllegalArgumentException( var2 );
            }
        }

        public String toString()
        {
            return this.toUri().toString();
        }

        public boolean equals( Object o )
        {
            if ( this == o )
            {
                return true;
            }
            else if ( o != null && this.getClass() == o.getClass() )
            {
                ClientConnectorAddresses.ConnectorUri that = (ClientConnectorAddresses.ConnectorUri) o;
                return this.scheme == that.scheme && Objects.equals( this.socketAddress, that.socketAddress );
            }
            else
            {
                return false;
            }
        }

        public int hashCode()
        {
            return Objects.hash( new Object[]{this.scheme, this.socketAddress} );
        }
    }
}
