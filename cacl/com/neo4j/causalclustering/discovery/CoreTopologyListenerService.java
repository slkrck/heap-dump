package com.neo4j.causalclustering.discovery;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class CoreTopologyListenerService
{
    private final Set<CoreTopologyService.Listener> listeners = ConcurrentHashMap.newKeySet();

    public void addCoreTopologyListener( CoreTopologyService.Listener listener )
    {
        this.listeners.add( listener );
    }

    public void removeCoreTopologyListener( CoreTopologyService.Listener listener )
    {
        this.listeners.remove( listener );
    }

    public void notifyListeners( DatabaseCoreTopology coreTopology )
    {
        Iterator var2 = this.listeners.iterator();

        while ( var2.hasNext() )
        {
            CoreTopologyService.Listener listener = (CoreTopologyService.Listener) var2.next();
            if ( listener.namedDatabaseId().databaseId().equals( coreTopology.databaseId() ) )
            {
                listener.onCoreTopologyChange( coreTopology );
            }
        }
    }
}
