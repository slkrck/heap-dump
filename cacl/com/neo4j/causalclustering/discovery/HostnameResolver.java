package com.neo4j.causalclustering.discovery;

import java.util.Collection;

import org.neo4j.configuration.helpers.SocketAddress;

public interface HostnameResolver
{
    Collection<SocketAddress> resolve( SocketAddress var1 );

    default boolean useOverrides()
    {
        return false;
    }
}
