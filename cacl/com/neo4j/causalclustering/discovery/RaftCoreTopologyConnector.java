package com.neo4j.causalclustering.discovery;

import com.neo4j.causalclustering.core.consensus.LeaderInfo;
import com.neo4j.causalclustering.core.consensus.LeaderListener;
import com.neo4j.causalclustering.core.consensus.RaftMachine;
import com.neo4j.causalclustering.identity.MemberId;

import java.util.Set;

import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;

public class RaftCoreTopologyConnector extends LifecycleAdapter implements CoreTopologyService.Listener, LeaderListener
{
    private final CoreTopologyService coreTopologyService;
    private final RaftMachine raftMachine;
    private final NamedDatabaseId namedDatabaseId;

    public RaftCoreTopologyConnector( CoreTopologyService coreTopologyService, RaftMachine raftMachine, NamedDatabaseId namedDatabaseId )
    {
        this.coreTopologyService = coreTopologyService;
        this.raftMachine = raftMachine;
        this.namedDatabaseId = namedDatabaseId;
    }

    public void start()
    {
        this.coreTopologyService.addLocalCoreTopologyListener( this );
        this.raftMachine.registerListener( this );
    }

    public void stop()
    {
        this.raftMachine.unregisterListener( this );
        this.coreTopologyService.removeLocalCoreTopologyListener( this );
    }

    public synchronized void onCoreTopologyChange( DatabaseCoreTopology coreTopology )
    {
        Set<MemberId> targetMembers = coreTopology.members().keySet();
        this.raftMachine.setTargetMembershipSet( targetMembers );
    }

    public void onLeaderSwitch( LeaderInfo leaderInfo )
    {
        this.coreTopologyService.setLeader( leaderInfo, this.namedDatabaseId );
    }

    public void onLeaderStepDown( long stepDownTerm )
    {
        this.coreTopologyService.handleStepDown( stepDownTerm, this.namedDatabaseId );
    }

    public NamedDatabaseId namedDatabaseId()
    {
        return this.namedDatabaseId;
    }
}
