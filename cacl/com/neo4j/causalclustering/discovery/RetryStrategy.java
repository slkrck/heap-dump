package com.neo4j.causalclustering.discovery;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Predicate;
import java.util.function.Supplier;

import org.neo4j.internal.helpers.ConstantTimeTimeoutStrategy;
import org.neo4j.internal.helpers.TimeoutStrategy;
import org.neo4j.internal.helpers.TimeoutStrategy.Timeout;

public class RetryStrategy
{
    private final TimeoutStrategy timeoutStrategy;
    private final long retries;

    public RetryStrategy( long delayInMillis, long retries )
    {
        this( new ConstantTimeTimeoutStrategy( delayInMillis, TimeUnit.MILLISECONDS ), retries );
    }

    public RetryStrategy( TimeoutStrategy timeoutStrategy, long retries )
    {
        this.timeoutStrategy = timeoutStrategy;
        this.retries = retries;
    }

    public <T> T apply( Supplier<T> action, Predicate<T> validator ) throws TimeoutException
    {
        Timeout timeout = this.timeoutStrategy.newTimeout();
        T result = action.get();

        for ( int var5 = 0; !validator.test( result ); result = action.get() )
        {
            if ( this.retries > 0L && (long) (var5++) == this.retries )
            {
                throw new TimeoutException( "Unable to fulfill predicate within " + this.retries + " retries" );
            }

            try
            {
                Thread.sleep( timeout.getAndIncrement() );
            }
            catch ( InterruptedException var7 )
            {
                Thread.currentThread().interrupt();
                throw new RuntimeException( var7 );
            }
        }

        return result;
    }
}
