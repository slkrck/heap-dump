package com.neo4j.causalclustering.upstream.strategies;

import com.neo4j.causalclustering.discovery.RoleInfo;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.upstream.UpstreamDatabaseSelectionException;
import com.neo4j.causalclustering.upstream.UpstreamDatabaseSelectionStrategy;

import java.util.Iterator;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import org.neo4j.kernel.database.NamedDatabaseId;

public class LeaderOnlyStrategy extends UpstreamDatabaseSelectionStrategy
{
    public static final String IDENTITY = "leader-only";

    public LeaderOnlyStrategy()
    {
        super( "leader-only" );
    }

    public Optional<MemberId> upstreamMemberForDatabase( NamedDatabaseId namedDatabaseId ) throws UpstreamDatabaseSelectionException
    {
        Set<MemberId> coreMemberIds = this.topologyService.coreTopologyForDatabase( namedDatabaseId ).members().keySet();
        if ( coreMemberIds.isEmpty() )
        {
            throw new UpstreamDatabaseSelectionException( "No core servers available" );
        }
        else
        {
            Iterator var3 = coreMemberIds.iterator();

            MemberId memberId;
            RoleInfo role;
            do
            {
                if ( !var3.hasNext() )
                {
                    return Optional.empty();
                }

                memberId = (MemberId) var3.next();
                role = this.topologyService.lookupRole( namedDatabaseId, memberId );
            }
            while ( role != RoleInfo.LEADER || Objects.equals( this.myself, memberId ) );

            return Optional.of( memberId );
        }
    }
}
