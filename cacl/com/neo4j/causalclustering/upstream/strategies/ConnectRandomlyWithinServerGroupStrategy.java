package com.neo4j.causalclustering.upstream.strategies;

import com.neo4j.causalclustering.core.CausalClusteringSettings;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.upstream.UpstreamDatabaseSelectionStrategy;

import java.util.List;
import java.util.Optional;

import org.neo4j.kernel.database.NamedDatabaseId;

public class ConnectRandomlyWithinServerGroupStrategy extends UpstreamDatabaseSelectionStrategy
{
    private static final String IDENTITY = "connect-randomly-within-server-group";
    private ConnectRandomlyToServerGroupImpl strategyImpl;

    public ConnectRandomlyWithinServerGroupStrategy()
    {
        super( "connect-randomly-within-server-group" );
    }

    public void init()
    {
        List<String> groups = (List) this.config.get( CausalClusteringSettings.server_groups );
        this.strategyImpl = new ConnectRandomlyToServerGroupImpl( groups, this.topologyService, this.myself );
        this.log.warn( "Upstream selection strategy " + this.name + " is deprecated. Consider using connect-randomly-within-server-group instead." );
    }

    public Optional<MemberId> upstreamMemberForDatabase( NamedDatabaseId namedDatabaseId )
    {
        return this.strategyImpl.upstreamMemberForDatabase( namedDatabaseId );
    }
}
