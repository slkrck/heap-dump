package com.neo4j.causalclustering.upstream.strategies;

import com.neo4j.causalclustering.core.CausalClusteringSettings;
import com.neo4j.causalclustering.discovery.DatabaseCoreTopology;
import com.neo4j.causalclustering.discovery.DatabaseReadReplicaTopology;
import com.neo4j.causalclustering.discovery.DiscoveryServerInfo;
import com.neo4j.causalclustering.discovery.Topology;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.routing.load_balancing.filters.Filter;
import com.neo4j.causalclustering.routing.load_balancing.plugins.server_policies.FilterConfigParser;
import com.neo4j.causalclustering.routing.load_balancing.plugins.server_policies.InvalidFilterSpecification;
import com.neo4j.causalclustering.routing.load_balancing.plugins.server_policies.ServerInfo;
import com.neo4j.causalclustering.upstream.UpstreamDatabaseSelectionStrategy;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.neo4j.kernel.database.NamedDatabaseId;

public class UserDefinedConfigurationStrategy extends UpstreamDatabaseSelectionStrategy
{
    public static final String IDENTITY = "user-defined";
    private Optional<Filter<ServerInfo>> filters;

    public UserDefinedConfigurationStrategy()
    {
        super( "user-defined" );
    }

    public void init()
    {
        String filterConfig = (String) this.config.get( CausalClusteringSettings.user_defined_upstream_selection_strategy );

        try
        {
            Filter<ServerInfo> parsed = FilterConfigParser.parse( filterConfig );
            this.filters = Optional.of( parsed );
            this.log.info( "Upstream selection strategy " + this.name + " configured with " + filterConfig );
        }
        catch ( InvalidFilterSpecification var3 )
        {
            this.filters = Optional.empty();
            this.log.warn( "Cannot parse configuration '" + filterConfig + "' for upstream selection strategy " + this.name + ". " + var3.getMessage() );
        }
    }

    public Optional<MemberId> upstreamMemberForDatabase( NamedDatabaseId namedDatabaseId )
    {
        return this.filters.flatMap( ( filters ) -> {
            Set<ServerInfo> possibleServers = this.possibleServers( namedDatabaseId );
            return filters.apply( possibleServers ).stream().map( ServerInfo::memberId ).filter( ( memberId ) -> {
                return !Objects.equals( this.myself, memberId );
            } ).findFirst();
        } );
    }

    private Set<ServerInfo> possibleServers( NamedDatabaseId namedDatabaseId )
    {
        DatabaseCoreTopology coreTopology = this.topologyService.coreTopologyForDatabase( namedDatabaseId );
        DatabaseReadReplicaTopology readReplicaTopology = this.topologyService.readReplicaTopologyForDatabase( namedDatabaseId );
        Stream<? extends Entry<MemberId,? extends DiscoveryServerInfo>> infoMap =
                Stream.of( coreTopology, readReplicaTopology ).map( Topology::members ).map( Map::entrySet ).flatMap( Collection::stream );
        return (Set) infoMap.map( this::toServerInfo ).collect( Collectors.toSet() );
    }

    private <T extends DiscoveryServerInfo> ServerInfo toServerInfo( Entry<MemberId,T> entry )
    {
        T server = (DiscoveryServerInfo) entry.getValue();
        MemberId memberId = (MemberId) entry.getKey();
        return new ServerInfo( server.connectors().boltAddress(), memberId, server.groups() );
    }
}
