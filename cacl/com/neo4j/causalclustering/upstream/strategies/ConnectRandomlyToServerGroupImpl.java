package com.neo4j.causalclustering.upstream.strategies;

import com.neo4j.causalclustering.discovery.ReadReplicaInfo;
import com.neo4j.causalclustering.discovery.TopologyService;
import com.neo4j.causalclustering.identity.MemberId;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.Map.Entry;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.neo4j.kernel.database.NamedDatabaseId;

public class ConnectRandomlyToServerGroupImpl
{
    private final List<String> groups;
    private final TopologyService topologyService;
    private final MemberId myself;
    private final Random random = new Random();

    ConnectRandomlyToServerGroupImpl( List<String> groups, TopologyService topologyService, MemberId myself )
    {
        this.groups = groups;
        this.topologyService = topologyService;
        this.myself = myself;
    }

    public Optional<MemberId> upstreamMemberForDatabase( NamedDatabaseId namedDatabaseId )
    {
        Map<MemberId,ReadReplicaInfo> replicas = this.topologyService.readReplicaTopologyForDatabase( namedDatabaseId ).members();
        List<MemberId> choices = (List) this.groups.stream().flatMap( ( group ) -> {
            return replicas.entrySet().stream().filter( this.isMyGroupAndNotMe( group ) );
        } ).map( Entry::getKey ).collect( Collectors.toList() );
        return choices.isEmpty() ? Optional.empty() : Optional.of( (MemberId) choices.get( this.random.nextInt( choices.size() ) ) );
    }

    private Predicate<Entry<MemberId,ReadReplicaInfo>> isMyGroupAndNotMe( String group )
    {
        return ( entry ) -> {
            return ((ReadReplicaInfo) entry.getValue()).groups().contains( group ) && !((MemberId) entry.getKey()).equals( this.myself );
        };
    }
}
