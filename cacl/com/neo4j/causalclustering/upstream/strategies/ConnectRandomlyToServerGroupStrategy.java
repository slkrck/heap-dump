package com.neo4j.causalclustering.upstream.strategies;

import com.neo4j.causalclustering.core.CausalClusteringSettings;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.upstream.UpstreamDatabaseSelectionStrategy;

import java.util.List;
import java.util.Optional;

import org.neo4j.kernel.database.NamedDatabaseId;

public class ConnectRandomlyToServerGroupStrategy extends UpstreamDatabaseSelectionStrategy
{
    static final String IDENTITY = "connect-randomly-to-server-group";
    private ConnectRandomlyToServerGroupImpl strategyImpl;

    public ConnectRandomlyToServerGroupStrategy()
    {
        super( "connect-randomly-to-server-group" );
    }

    public void init()
    {
        List<String> groups = (List) this.config.get( CausalClusteringSettings.connect_randomly_to_server_group_strategy );
        this.strategyImpl = new ConnectRandomlyToServerGroupImpl( groups, this.topologyService, this.myself );
        if ( groups.isEmpty() )
        {
            this.log.warn( "No server groups configured for upstream strategy " + this.name + ". Strategy will not find upstream servers." );
        }
        else
        {
            String readableGroups = String.join( ", ", groups );
            this.log.info( "Upstream selection strategy " + this.name + " configured with server groups " + readableGroups );
        }
    }

    public Optional<MemberId> upstreamMemberForDatabase( NamedDatabaseId namedDatabaseId )
    {
        return this.strategyImpl.upstreamMemberForDatabase( namedDatabaseId );
    }
}
