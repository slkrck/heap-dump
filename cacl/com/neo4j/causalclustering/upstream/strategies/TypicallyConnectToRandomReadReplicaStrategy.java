package com.neo4j.causalclustering.upstream.strategies;

import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.upstream.UpstreamDatabaseSelectionStrategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.neo4j.kernel.database.NamedDatabaseId;

public class TypicallyConnectToRandomReadReplicaStrategy extends UpstreamDatabaseSelectionStrategy
{
    public static final String IDENTITY = "typically-connect-to-random-read-replica";
    private final TypicallyConnectToRandomReadReplicaStrategy.ModuloCounter counter;

    public TypicallyConnectToRandomReadReplicaStrategy()
    {
        this( 10 );
    }

    public TypicallyConnectToRandomReadReplicaStrategy( int connectToCoreInterval )
    {
        super( "typically-connect-to-random-read-replica" );
        this.counter = new TypicallyConnectToRandomReadReplicaStrategy.ModuloCounter( connectToCoreInterval );
    }

    public Optional<MemberId> upstreamMemberForDatabase( NamedDatabaseId namedDatabaseId )
    {
        if ( this.counter.shouldReturnCoreMemberId() )
        {
            return this.randomCoreMember( namedDatabaseId );
        }
        else
        {
            List<MemberId> readReplicaMembers = new ArrayList( this.topologyService.readReplicaTopologyForDatabase( namedDatabaseId ).members().keySet() );
            Collections.shuffle( readReplicaMembers );
            List<MemberId> coreMembers = new ArrayList( this.topologyService.coreTopologyForDatabase( namedDatabaseId ).members().keySet() );
            Collections.shuffle( coreMembers );
            Stream var10000 = Stream.concat( readReplicaMembers.stream(), coreMembers.stream() );
            MemberId var10001 = this.myself;
            Objects.requireNonNull( var10001 );
            return var10000.filter( Predicate.not( var10001::equals ) ).findFirst();
        }
    }

    private Optional<MemberId> randomCoreMember( NamedDatabaseId namedDatabaseId )
    {
        Stream var10000 = this.topologyService.coreTopologyForDatabase( namedDatabaseId ).members().keySet().stream();
        MemberId var10001 = this.myself;
        Objects.requireNonNull( var10001 );
        List<MemberId> coreMembersNotSelf = (List) var10000.filter( Predicate.not( var10001::equals ) ).collect( Collectors.toList() );
        if ( coreMembersNotSelf.isEmpty() )
        {
            return Optional.empty();
        }
        else
        {
            int randomIndex = ThreadLocalRandom.current().nextInt( coreMembersNotSelf.size() );
            return Optional.of( (MemberId) coreMembersNotSelf.get( randomIndex ) );
        }
    }

    private static class ModuloCounter
    {
        private final int modulo;
        private int counter;

        ModuloCounter( int modulo )
        {
            this.modulo = modulo;
        }

        boolean shouldReturnCoreMemberId()
        {
            this.counter = (this.counter + 1) % this.modulo;
            return this.counter == 0;
        }
    }
}
