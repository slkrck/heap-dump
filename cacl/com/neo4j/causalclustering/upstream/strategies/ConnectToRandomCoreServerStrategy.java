package com.neo4j.causalclustering.upstream.strategies;

import com.neo4j.causalclustering.discovery.DatabaseCoreTopology;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.upstream.UpstreamDatabaseSelectionException;
import com.neo4j.causalclustering.upstream.UpstreamDatabaseSelectionStrategy;

import java.util.Optional;
import java.util.Random;

import org.neo4j.kernel.database.NamedDatabaseId;

public class ConnectToRandomCoreServerStrategy extends UpstreamDatabaseSelectionStrategy
{
    static final String IDENTITY = "connect-to-random-core-server";
    private final Random random = new Random();

    public ConnectToRandomCoreServerStrategy()
    {
        super( "connect-to-random-core-server" );
    }

    public Optional<MemberId> upstreamMemberForDatabase( NamedDatabaseId namedDatabaseId ) throws UpstreamDatabaseSelectionException
    {
        DatabaseCoreTopology coreTopology = this.topologyService.coreTopologyForDatabase( namedDatabaseId );
        if ( coreTopology.members().isEmpty() )
        {
            throw new UpstreamDatabaseSelectionException( "No core servers available" );
        }
        else
        {
            int skippedServers = this.random.nextInt( coreTopology.members().size() );
            return coreTopology.members().keySet().stream().skip( (long) skippedServers ).findFirst();
        }
    }
}
