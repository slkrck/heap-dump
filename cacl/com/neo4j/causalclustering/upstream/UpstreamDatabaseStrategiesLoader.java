package com.neo4j.causalclustering.upstream;

import com.neo4j.causalclustering.core.CausalClusteringSettings;
import com.neo4j.causalclustering.discovery.TopologyService;
import com.neo4j.causalclustering.identity.MemberId;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.collections.impl.block.factory.Predicates;
import org.neo4j.configuration.Config;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.service.Services;

public class UpstreamDatabaseStrategiesLoader implements Iterable<UpstreamDatabaseSelectionStrategy>
{
    private final TopologyService topologyService;
    private final Config config;
    private final MemberId myself;
    private final Log log;
    private final LogProvider logProvider;

    public UpstreamDatabaseStrategiesLoader( TopologyService topologyService, Config config, MemberId myself, LogProvider logProvider )
    {
        this.topologyService = topologyService;
        this.config = config;
        this.myself = myself;
        this.log = logProvider.getLog( this.getClass() );
        this.logProvider = logProvider;
    }

    private static String nicelyCommaSeparatedList( Collection<UpstreamDatabaseSelectionStrategy> items )
    {
        return (String) items.stream().map( UpstreamDatabaseSelectionStrategy::toString ).collect( Collectors.joining( ", " ) );
    }

    public Iterator<UpstreamDatabaseSelectionStrategy> iterator()
    {
        List<String> configuredNames = (List) this.config.get( CausalClusteringSettings.upstream_selection_strategy );
        Map<String,UpstreamDatabaseSelectionStrategy> availableStrategies = (Map) Services.loadAll( UpstreamDatabaseSelectionStrategy.class ).stream().collect(
                Collectors.toMap( UpstreamDatabaseSelectionStrategy::getName, Function.identity() ) );
        Stream var10000 = configuredNames.stream().distinct();
        Objects.requireNonNull( availableStrategies );
        List<UpstreamDatabaseSelectionStrategy> strategies =
                (List) var10000.map( availableStrategies::get ).filter( Predicates.notNull() ).collect( Collectors.toList() );
        strategies.forEach( ( strategy ) -> {
            strategy.inject( this.topologyService, this.config, this.logProvider, this.myself );
        } );
        this.log.debug( "Upstream database strategies loaded in order of precedence: " + nicelyCommaSeparatedList( strategies ) );
        return strategies.iterator();
    }
}
