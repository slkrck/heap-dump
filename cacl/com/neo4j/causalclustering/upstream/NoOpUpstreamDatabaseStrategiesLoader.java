package com.neo4j.causalclustering.upstream;

import com.neo4j.causalclustering.discovery.TopologyService;
import com.neo4j.causalclustering.identity.MemberId;

import java.util.Iterator;
import java.util.NoSuchElementException;

import org.neo4j.configuration.Config;
import org.neo4j.logging.NullLogProvider;

public class NoOpUpstreamDatabaseStrategiesLoader extends UpstreamDatabaseStrategiesLoader
{
    public NoOpUpstreamDatabaseStrategiesLoader()
    {
        super( (TopologyService) null, (Config) null, (MemberId) null, NullLogProvider.getInstance() );
    }

    public Iterator<UpstreamDatabaseSelectionStrategy> iterator()
    {
        return new Iterator<UpstreamDatabaseSelectionStrategy>()
        {
            public boolean hasNext()
            {
                return false;
            }

            public UpstreamDatabaseSelectionStrategy next()
            {
                throw new NoSuchElementException();
            }
        };
    }
}
