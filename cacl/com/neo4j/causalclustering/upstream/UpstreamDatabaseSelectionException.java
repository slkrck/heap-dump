package com.neo4j.causalclustering.upstream;

public class UpstreamDatabaseSelectionException extends Exception
{
    public UpstreamDatabaseSelectionException( String message )
    {
        super( message );
    }
}
