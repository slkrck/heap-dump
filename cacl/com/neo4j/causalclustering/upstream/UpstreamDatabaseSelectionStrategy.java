package com.neo4j.causalclustering.upstream;

import com.neo4j.causalclustering.discovery.TopologyService;
import com.neo4j.causalclustering.identity.MemberId;

import java.util.Optional;

import org.neo4j.annotations.service.Service;
import org.neo4j.configuration.Config;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.service.NamedService;

@Service
public abstract class UpstreamDatabaseSelectionStrategy implements NamedService
{
    protected TopologyService topologyService;
    protected Config config;
    protected Log log;
    protected MemberId myself;
    protected String name;

    protected UpstreamDatabaseSelectionStrategy( String name )
    {
        this.name = name;
    }

    public String getName()
    {
        return this.name;
    }

    public void inject( TopologyService topologyService, Config config, LogProvider logProvider, MemberId myself )
    {
        this.topologyService = topologyService;
        this.config = config;
        this.log = logProvider.getLog( this.getClass() );
        this.myself = myself;
        this.log.info( "Using upstream selection strategy " + this.name );
        this.init();
    }

    public void init()
    {
    }

    public abstract Optional<MemberId> upstreamMemberForDatabase( NamedDatabaseId var1 ) throws UpstreamDatabaseSelectionException;

    public String toString()
    {
        return this.name;
    }
}
