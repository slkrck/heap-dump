package com.neo4j.causalclustering.upstream;

import com.neo4j.causalclustering.identity.MemberId;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

import org.neo4j.internal.helpers.collection.Iterables;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.logging.NullLogProvider;

public class UpstreamDatabaseStrategySelector
{
    private final Set<UpstreamDatabaseSelectionStrategy> strategies;
    private final Log log;

    public UpstreamDatabaseStrategySelector( UpstreamDatabaseSelectionStrategy defaultStrategy )
    {
        this( defaultStrategy, Iterables.empty(), NullLogProvider.getInstance() );
    }

    public UpstreamDatabaseStrategySelector( UpstreamDatabaseSelectionStrategy defaultStrategy, Iterable<UpstreamDatabaseSelectionStrategy> otherStrategies,
            LogProvider logProvider )
    {
        this.strategies = new LinkedHashSet();
        this.log = logProvider.getLog( this.getClass() );
        Iterables.addAll( this.strategies, otherStrategies );
        this.strategies.add( defaultStrategy );
    }

    public MemberId bestUpstreamMemberForDatabase( NamedDatabaseId namedDatabaseId ) throws UpstreamDatabaseSelectionException
    {
        Iterator var2 = this.strategies.iterator();

        Optional upstreamMember;
        do
        {
            if ( !var2.hasNext() )
            {
                throw new UpstreamDatabaseSelectionException(
                        "Could not find an upstream member for database " + namedDatabaseId.name() + " with which to connect" );
            }

            UpstreamDatabaseSelectionStrategy strategy = (UpstreamDatabaseSelectionStrategy) var2.next();
            this.log.debug( "Trying selection strategy [%s]", new Object[]{strategy} );
            upstreamMember = strategy.upstreamMemberForDatabase( namedDatabaseId );
        }
        while ( !upstreamMember.isPresent() );

        MemberId memberId = (MemberId) upstreamMember.get();
        this.log.debug( "Selected upstream database [%s]", new Object[]{memberId} );
        return memberId;
    }
}
