package com.neo4j.causalclustering;

import com.neo4j.causalclustering.core.replication.session.GlobalSession;
import com.neo4j.causalclustering.core.replication.session.GlobalSessionTrackerState;
import com.neo4j.causalclustering.core.replication.session.LocalOperationId;
import com.neo4j.causalclustering.core.state.storage.StateStorage;

import java.io.IOException;

public class SessionTracker
{
    private final StateStorage<GlobalSessionTrackerState> sessionTrackerStorage;
    private GlobalSessionTrackerState sessionState;

    public SessionTracker( StateStorage<GlobalSessionTrackerState> sessionTrackerStorage )
    {
        this.sessionTrackerStorage = sessionTrackerStorage;
    }

    public void start()
    {
        if ( this.sessionState == null )
        {
            this.sessionState = (GlobalSessionTrackerState) this.sessionTrackerStorage.getInitialState();
        }
    }

    public long getLastAppliedIndex()
    {
        return this.sessionState.logIndex();
    }

    public void flush() throws IOException
    {
        this.sessionTrackerStorage.writeState( this.sessionState );
    }

    public GlobalSessionTrackerState snapshot()
    {
        return this.sessionState.newInstance();
    }

    public void installSnapshot( GlobalSessionTrackerState sessionState )
    {
        this.sessionState = sessionState;
    }

    public boolean validateOperation( GlobalSession globalSession, LocalOperationId localOperationId )
    {
        return this.sessionState.validateOperation( globalSession, localOperationId );
    }

    public void update( GlobalSession globalSession, LocalOperationId localOperationId, long logIndex )
    {
        this.sessionState.update( globalSession, localOperationId, logIndex );
    }

    public GlobalSessionTrackerState newInstance()
    {
        return this.sessionState.newInstance();
    }
}
