package com.neo4j.causalclustering.error_handling;

import org.neo4j.kernel.availability.DatabaseAvailabilityGuard;
import org.neo4j.kernel.database.Database;

class RaiseAvailabilityGuardHandler implements DatabasePanicEventHandler
{
    private static final String PANIC_REQUIREMENT_MESSAGE = "Clustering components have encountered a critical error: ";
    private final Database db;

    RaiseAvailabilityGuardHandler( Database db )
    {
        this.db = db;
    }

    public void onPanic( Throwable cause )
    {
        DatabaseAvailabilityGuard dbAvailabilityGuard = this.db.getDatabaseAvailabilityGuard();
        if ( dbAvailabilityGuard != null )
        {
            dbAvailabilityGuard.require( () -> {
                return "Clustering components have encountered a critical error: " + cause.getMessage();
            } );
        }
    }
}
