package com.neo4j.causalclustering.error_handling;

import java.util.concurrent.CompletableFuture;

public interface DatabasePanicker
{
    CompletableFuture<Void> panicAsync( Throwable var1 );

    default void panic( Throwable e )
    {
        this.panicAsync( e ).join();
    }
}
