package com.neo4j.causalclustering.error_handling;

import com.neo4j.dbms.ClusterInternalDbmsOperator;
import org.neo4j.kernel.database.Database;

public class StopDatabaseHandler implements DatabasePanicEventHandler
{
    private final Database db;
    private final ClusterInternalDbmsOperator internalOperator;

    StopDatabaseHandler( Database db, ClusterInternalDbmsOperator internalOperator )
    {
        this.db = db;
        this.internalOperator = internalOperator;
    }

    public void onPanic( Throwable cause )
    {
        this.internalOperator.stopOnPanic( this.db.getNamedDatabaseId(), cause );
    }
}
