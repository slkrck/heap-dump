package com.neo4j.causalclustering.error_handling;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;

import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.internal.LogService;
import org.neo4j.scheduler.Group;
import org.neo4j.scheduler.JobScheduler;
import org.neo4j.util.Preconditions;

public class PanicService
{
    private final Map<NamedDatabaseId,PanicService.DatabasePanicEventHandlers> handlersByDatabase = new ConcurrentHashMap();
    private final Executor executor;
    private final Log log;

    public PanicService( JobScheduler jobScheduler, LogService logService )
    {
        this.executor = jobScheduler.executor( Group.PANIC_SERVICE );
        this.log = logService.getUserLog( this.getClass() );
    }

    public void addPanicEventHandlers( NamedDatabaseId namedDatabaseId, List<? extends DatabasePanicEventHandler> handlers )
    {
        PanicService.DatabasePanicEventHandlers newHandlers = new PanicService.DatabasePanicEventHandlers( handlers );
        PanicService.DatabasePanicEventHandlers oldHandlers =
                (PanicService.DatabasePanicEventHandlers) this.handlersByDatabase.putIfAbsent( namedDatabaseId, newHandlers );
        Preconditions.checkState( oldHandlers == null, "Panic handlers for database %s are already installed", new Object[]{namedDatabaseId.name()} );
    }

    public void removePanicEventHandlers( NamedDatabaseId namedDatabaseId )
    {
        this.handlersByDatabase.remove( namedDatabaseId );
    }

    public DatabasePanicker panickerFor( NamedDatabaseId namedDatabaseId )
    {
        return ( error ) -> {
            return CompletableFuture.runAsync( () -> {
                this.panic( namedDatabaseId, error );
            }, this.executor );
        };
    }

    private void panic( NamedDatabaseId namedDatabaseId, Throwable error )
    {
        this.log.error( "Clustering components for database '" + namedDatabaseId.name() + "' have encountered a critical error", error );
        PanicService.DatabasePanicEventHandlers handlers = (PanicService.DatabasePanicEventHandlers) this.handlersByDatabase.get( namedDatabaseId );
        if ( handlers != null )
        {
            handlers.handlePanic( error );
        }
    }

    private class DatabasePanicEventHandlers
    {
        final List<? extends DatabasePanicEventHandler> handlers;
        final AtomicBoolean panicked;

        DatabasePanicEventHandlers( List<? extends DatabasePanicEventHandler> handlers )
        {
            this.handlers = handlers;
            this.panicked = new AtomicBoolean();
        }

        void handlePanic( Throwable cause )
        {
            if ( this.panicked.compareAndSet( false, true ) )
            {
                Iterator var2 = this.handlers.iterator();

                while ( var2.hasNext() )
                {
                    DatabasePanicEventHandler handler = (DatabasePanicEventHandler) var2.next();

                    try
                    {
                        handler.onPanic( cause );
                    }
                    catch ( Throwable var5 )
                    {
                        PanicService.this.log.error( "Failed to handle a panic event", var5 );
                    }
                }
            }
        }
    }
}
