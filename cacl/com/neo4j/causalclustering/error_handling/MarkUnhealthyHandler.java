package com.neo4j.causalclustering.error_handling;

import org.neo4j.kernel.database.Database;
import org.neo4j.monitoring.Health;

class MarkUnhealthyHandler implements DatabasePanicEventHandler
{
    private final Database db;

    MarkUnhealthyHandler( Database db )
    {
        this.db = db;
    }

    public void onPanic( Throwable cause )
    {
        Health dbHealth = this.db.getDatabaseHealth();
        if ( dbHealth != null )
        {
            dbHealth.panic( cause );
        }
    }
}
