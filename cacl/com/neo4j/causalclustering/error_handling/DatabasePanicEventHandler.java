package com.neo4j.causalclustering.error_handling;

import com.neo4j.dbms.ClusterInternalDbmsOperator;
import org.neo4j.kernel.database.Database;

@FunctionalInterface
public interface DatabasePanicEventHandler
{
    static DatabasePanicEventHandler raiseAvailabilityGuard( Database db )
    {
        return new RaiseAvailabilityGuardHandler( db );
    }

    static DatabasePanicEventHandler markUnhealthy( Database db )
    {
        return new MarkUnhealthyHandler( db );
    }

    static DatabasePanicEventHandler stopDatabase( Database db, ClusterInternalDbmsOperator internalOperator )
    {
        return new StopDatabaseHandler( db, internalOperator );
    }

    void onPanic( Throwable var1 );
}
