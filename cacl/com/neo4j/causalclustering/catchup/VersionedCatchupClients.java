package com.neo4j.causalclustering.catchup;

import com.neo4j.causalclustering.catchup.storecopy.PrepareStoreCopyResponse;
import com.neo4j.causalclustering.catchup.storecopy.StoreCopyFinishedResponse;
import com.neo4j.causalclustering.catchup.tx.TxStreamFinishedResponse;
import com.neo4j.causalclustering.core.state.snapshot.CoreSnapshot;

import java.io.File;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.storageengine.api.StoreId;

public interface VersionedCatchupClients extends AutoCloseable
{
    <RESULT> VersionedCatchupClients.NeedsResponseHandler<RESULT> v3(
            Function<VersionedCatchupClients.CatchupClientV3,VersionedCatchupClients.PreparedRequest<RESULT>> var1 );

    @FunctionalInterface
    public interface PreparedRequest<RESULT>
    {
        CompletableFuture<RESULT> execute( CatchupResponseCallback<RESULT> var1 );
    }

    public interface CatchupClientV3
    {
        VersionedCatchupClients.PreparedRequest<NamedDatabaseId> getDatabaseId( String var1 );

        VersionedCatchupClients.PreparedRequest<CoreSnapshot> getCoreSnapshot( NamedDatabaseId var1 );

        VersionedCatchupClients.PreparedRequest<StoreId> getStoreId( NamedDatabaseId var1 );

        VersionedCatchupClients.PreparedRequest<TxStreamFinishedResponse> pullTransactions( StoreId var1, long var2, NamedDatabaseId var4 );

        VersionedCatchupClients.PreparedRequest<PrepareStoreCopyResponse> prepareStoreCopy( StoreId var1, NamedDatabaseId var2 );

        VersionedCatchupClients.PreparedRequest<StoreCopyFinishedResponse> getStoreFile( StoreId var1, File var2, long var3, NamedDatabaseId var5 );
    }

    public interface IsPrepared<RESULT>
    {
        RESULT request() throws Exception;
    }

    public interface NeedsResponseHandler<RESULT>
    {
        VersionedCatchupClients.IsPrepared<RESULT> withResponseHandler( CatchupResponseCallback<RESULT> var1 );
    }

    public interface NeedsV3Handler<RESULT>
    {
        VersionedCatchupClients.NeedsResponseHandler<RESULT> v3(
                Function<VersionedCatchupClients.CatchupClientV3,VersionedCatchupClients.PreparedRequest<RESULT>> var1 );
    }

    public interface CatchupRequestBuilder<RESULT> extends VersionedCatchupClients.NeedsV3Handler<RESULT>, VersionedCatchupClients.NeedsResponseHandler<RESULT>,
            VersionedCatchupClients.IsPrepared<RESULT>
    {
    }
}
