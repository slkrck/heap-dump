package com.neo4j.causalclustering.catchup;

import com.neo4j.dbms.database.ClusteredDatabaseContext;

@FunctionalInterface
public interface CatchupComponentsFactory
{
    CatchupComponentsRepository.CatchupComponents createDatabaseComponents( ClusteredDatabaseContext var1 );
}
