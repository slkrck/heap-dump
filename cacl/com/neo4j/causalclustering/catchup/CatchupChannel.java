package com.neo4j.causalclustering.catchup;

import com.neo4j.causalclustering.messaging.CatchupProtocolMessage;
import com.neo4j.causalclustering.net.PooledChannel;
import com.neo4j.causalclustering.protocol.application.ApplicationProtocol;
import io.netty.channel.Channel;

import java.util.OptionalLong;
import java.util.concurrent.CompletableFuture;

class CatchupChannel
{
    private final PooledChannel pooledChannel;
    private TrackingResponseHandler trackingResponseHandler;

    CatchupChannel( PooledChannel pooledChannel )
    {
        this.pooledChannel = pooledChannel;
    }

    void setResponseHandler( CatchupResponseCallback<?> handler, CompletableFuture<?> requestOutcomeSignal )
    {
        this.getOrCreateResponseHandler().setResponseHandler( handler, requestOutcomeSignal );
    }

    ApplicationProtocol protocol()
    {
        return this.pooledChannel.protocolStack().applicationProtocol();
    }

    void send( CatchupProtocolMessage message )
    {
        Channel channel = this.pooledChannel.channel();
        channel.eventLoop().execute( () -> {
            channel.write( message.messageType(), channel.voidPromise() );
            channel.writeAndFlush( message, channel.voidPromise() );
        } );
    }

    void release()
    {
        this.pooledChannel.release();
    }

    OptionalLong millisSinceLastResponse()
    {
        return this.getOrCreateResponseHandler().millisSinceLastResponse();
    }

    private TrackingResponseHandler getOrCreateResponseHandler()
    {
        if ( this.trackingResponseHandler == null )
        {
            this.trackingResponseHandler = (TrackingResponseHandler) this.pooledChannel.getAttribute( CatchupChannelPoolService.TRACKING_RESPONSE_HANDLER );
        }

        return this.trackingResponseHandler;
    }

    void dispose()
    {
        this.pooledChannel.dispose();
    }
}
