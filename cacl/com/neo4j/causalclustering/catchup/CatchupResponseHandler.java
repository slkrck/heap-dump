package com.neo4j.causalclustering.catchup;

import com.neo4j.causalclustering.catchup.storecopy.FileChunk;
import com.neo4j.causalclustering.catchup.storecopy.FileHeader;
import com.neo4j.causalclustering.catchup.storecopy.GetStoreIdResponse;
import com.neo4j.causalclustering.catchup.storecopy.PrepareStoreCopyResponse;
import com.neo4j.causalclustering.catchup.storecopy.StoreCopyFinishedResponse;
import com.neo4j.causalclustering.catchup.tx.TxPullResponse;
import com.neo4j.causalclustering.catchup.tx.TxStreamFinishedResponse;
import com.neo4j.causalclustering.catchup.v3.databaseid.GetDatabaseIdResponse;
import com.neo4j.causalclustering.core.state.snapshot.CoreSnapshot;

import java.io.IOException;

public interface CatchupResponseHandler
{
    void onFileHeader( FileHeader var1 );

    boolean onFileContent( FileChunk var1 ) throws IOException;

    void onFileStreamingComplete( StoreCopyFinishedResponse var1 );

    void onTxPullResponse( TxPullResponse var1 );

    void onTxStreamFinishedResponse( TxStreamFinishedResponse var1 );

    void onGetStoreIdResponse( GetStoreIdResponse var1 );

    void onGetDatabaseIdResponse( GetDatabaseIdResponse var1 );

    void onCoreSnapshot( CoreSnapshot var1 );

    void onStoreListingResponse( PrepareStoreCopyResponse var1 );

    void onCatchupErrorResponse( CatchupErrorResponse var1 );

    void onClose();
}
