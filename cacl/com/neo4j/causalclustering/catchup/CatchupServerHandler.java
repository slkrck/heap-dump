package com.neo4j.causalclustering.catchup;

import io.netty.channel.ChannelHandler;

public interface CatchupServerHandler
{
    ChannelHandler getDatabaseIdRequestHandler( CatchupServerProtocol var1 );

    ChannelHandler txPullRequestHandler( CatchupServerProtocol var1 );

    ChannelHandler getStoreIdRequestHandler( CatchupServerProtocol var1 );

    ChannelHandler storeListingRequestHandler( CatchupServerProtocol var1 );

    ChannelHandler getStoreFileRequestHandler( CatchupServerProtocol var1 );

    ChannelHandler snapshotHandler( CatchupServerProtocol var1 );
}
