package com.neo4j.causalclustering.catchup;

public class CatchUpProtocolViolationException extends Exception
{
    public CatchUpProtocolViolationException( String message, Object... args )
    {
        super( String.format( message, args ) );
    }
}
