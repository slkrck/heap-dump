package com.neo4j.causalclustering.catchup.tx;

import com.neo4j.causalclustering.catchup.CatchupResult;

import java.util.Objects;

public class TxStreamFinishedResponse
{
    private final CatchupResult status;
    private final long latestTxId;

    public TxStreamFinishedResponse( CatchupResult status, long latestTxId )
    {
        this.status = status;
        this.latestTxId = latestTxId;
    }

    public long lastTxId()
    {
        return this.latestTxId;
    }

    public CatchupResult status()
    {
        return this.status;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            TxStreamFinishedResponse that = (TxStreamFinishedResponse) o;
            return this.latestTxId == that.latestTxId && this.status == that.status;
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.status, this.latestTxId} );
    }

    public String toString()
    {
        return "TxStreamFinishedResponse{status=" + this.status + ", lastTxId=" + this.latestTxId + "}";
    }
}
