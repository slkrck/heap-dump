package com.neo4j.causalclustering.catchup.tx;

public interface PullRequestMonitor
{
    void txPullRequest( long var1 );

    void txPullResponse( long var1 );

    long lastRequestedTxId();

    long lastReceivedTxId();

    long numberOfRequests();
}
