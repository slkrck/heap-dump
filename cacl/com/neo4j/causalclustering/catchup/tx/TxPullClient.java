package com.neo4j.causalclustering.catchup.tx;

import com.neo4j.causalclustering.catchup.CatchupClientFactory;
import com.neo4j.causalclustering.catchup.CatchupResponseAdaptor;

import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.monitoring.Monitors;
import org.neo4j.storageengine.api.StoreId;

public class TxPullClient
{
    private final CatchupClientFactory catchUpClient;
    private final NamedDatabaseId namedDatabaseId;
    private final Supplier<Monitors> monitors;
    private final LogProvider logProvider;
    private TxPullClient.PullRequestMonitor pullRequestMonitor = new TxPullClient.PullRequestMonitor();

    public TxPullClient( CatchupClientFactory catchUpClient, NamedDatabaseId namedDatabaseId, Supplier<Monitors> monitors, LogProvider logProvider )
    {
        this.catchUpClient = catchUpClient;
        this.namedDatabaseId = namedDatabaseId;
        this.monitors = monitors;
        this.logProvider = logProvider;
    }

    public TxStreamFinishedResponse pullTransactions( SocketAddress fromAddress, StoreId storeId, long previousTxId,
            final TxPullResponseListener txPullResponseListener ) throws Exception
    {
        CatchupResponseAdaptor<TxStreamFinishedResponse> responseHandler = new CatchupResponseAdaptor<TxStreamFinishedResponse>()
        {
            public void onTxPullResponse( CompletableFuture<TxStreamFinishedResponse> signal, TxPullResponse response )
            {
                txPullResponseListener.onTxReceived( response );
            }

            public void onTxStreamFinishedResponse( CompletableFuture<TxStreamFinishedResponse> signal, TxStreamFinishedResponse response )
            {
                signal.complete( response );
            }
        };
        this.pullRequestMonitor.get().txPullRequest( previousTxId );
        Log log = this.logProvider.getLog( this.getClass() );
        return (TxStreamFinishedResponse) this.catchUpClient.getClient( fromAddress, log ).v3( ( c ) -> {
            return c.pullTransactions( storeId, previousTxId, this.namedDatabaseId );
        } ).withResponseHandler( responseHandler ).request();
    }

    private class PullRequestMonitor
    {
        private com.neo4j.causalclustering.catchup.tx.PullRequestMonitor pullRequestMonitor;

        private com.neo4j.causalclustering.catchup.tx.PullRequestMonitor get()
        {
            if ( this.pullRequestMonitor == null )
            {
                this.pullRequestMonitor = (com.neo4j.causalclustering.catchup.tx.PullRequestMonitor) ((Monitors) TxPullClient.this.monitors.get()).newMonitor(
                        com.neo4j.causalclustering.catchup.tx.PullRequestMonitor.class, new String[0] );
            }

            return this.pullRequestMonitor;
        }
    }
}
