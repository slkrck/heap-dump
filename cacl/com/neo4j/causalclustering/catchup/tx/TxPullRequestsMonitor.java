package com.neo4j.causalclustering.catchup.tx;

public interface TxPullRequestsMonitor
{
    long txPullRequestsReceived();

    void increment();
}
