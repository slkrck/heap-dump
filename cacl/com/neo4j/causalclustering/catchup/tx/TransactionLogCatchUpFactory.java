package com.neo4j.causalclustering.catchup.tx;

import java.io.IOException;

import org.neo4j.configuration.Config;
import org.neo4j.internal.helpers.collection.LongRange;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.logging.LogProvider;
import org.neo4j.storageengine.api.StorageEngineFactory;

public class TransactionLogCatchUpFactory
{
    public TransactionLogCatchUpWriter create( DatabaseLayout databaseLayout, FileSystemAbstraction fs, PageCache pageCache, Config config,
            LogProvider logProvider, StorageEngineFactory storageEngineFactory, LongRange validInitialTx, boolean asPartOfStoreCopy,
            boolean keepTxLogsInStoreDir, boolean rotateTransactionsManually ) throws IOException
    {
        return new TransactionLogCatchUpWriter( databaseLayout, fs, pageCache, config, logProvider, storageEngineFactory, validInitialTx, asPartOfStoreCopy,
                keepTxLogsInStoreDir, rotateTransactionsManually );
    }
}
