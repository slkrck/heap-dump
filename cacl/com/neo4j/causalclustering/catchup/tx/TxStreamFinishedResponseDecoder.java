package com.neo4j.causalclustering.catchup.tx;

import com.neo4j.causalclustering.catchup.CatchupResult;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

public class TxStreamFinishedResponseDecoder extends ByteToMessageDecoder
{
    protected void decode( ChannelHandlerContext ctx, ByteBuf msg, List<Object> out )
    {
        int ordinal = msg.readInt();
        long latestTxid = msg.readLong();
        CatchupResult status = CatchupResult.values()[ordinal];
        out.add( new TxStreamFinishedResponse( status, latestTxid ) );
    }
}
