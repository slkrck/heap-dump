package com.neo4j.causalclustering.catchup.tx;

import org.neo4j.kernel.impl.transaction.log.TransactionCursor;
import org.neo4j.storageengine.api.StoreId;

class TxPullingContext
{
    private final TransactionCursor transactions;
    private final StoreId localStoreId;
    private final long firstTxId;
    private final long txIdPromise;

    TxPullingContext( TransactionCursor transactions, StoreId localStoreId, long firstTxId, long txIdPromise )
    {
        this.transactions = transactions;
        this.localStoreId = localStoreId;
        this.firstTxId = firstTxId;
        this.txIdPromise = txIdPromise;
    }

    long firstTxId()
    {
        return this.firstTxId;
    }

    long txIdPromise()
    {
        return this.txIdPromise;
    }

    StoreId localStoreId()
    {
        return this.localStoreId;
    }

    TransactionCursor transactions()
    {
        return this.transactions;
    }
}
