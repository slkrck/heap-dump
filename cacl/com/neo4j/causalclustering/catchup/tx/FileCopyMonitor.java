package com.neo4j.causalclustering.catchup.tx;

import java.io.File;

public interface FileCopyMonitor
{
    void copyFile( File var1 );
}
