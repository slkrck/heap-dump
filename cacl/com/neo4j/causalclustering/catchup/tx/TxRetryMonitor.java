package com.neo4j.causalclustering.catchup.tx;

public interface TxRetryMonitor
{
    long transactionsRetries();

    void retry();
}
