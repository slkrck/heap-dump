package com.neo4j.causalclustering.catchup.tx;

import java.util.Objects;

import org.neo4j.kernel.impl.transaction.CommittedTransactionRepresentation;
import org.neo4j.storageengine.api.StoreId;

public class TxPullResponse
{
    public static final TxPullResponse EMPTY = new TxPullResponse( (StoreId) null, (CommittedTransactionRepresentation) null );
    private final StoreId storeId;
    private final CommittedTransactionRepresentation tx;

    public TxPullResponse( StoreId storeId, CommittedTransactionRepresentation tx )
    {
        this.storeId = storeId;
        this.tx = tx;
    }

    public StoreId storeId()
    {
        return this.storeId;
    }

    public CommittedTransactionRepresentation tx()
    {
        return this.tx;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            TxPullResponse that = (TxPullResponse) o;
            return Objects.equals( this.storeId, that.storeId ) && Objects.equals( this.tx, that.tx );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        int result = this.storeId != null ? this.storeId.hashCode() : 0;
        result = 31 * result + (this.tx != null ? this.tx.hashCode() : 0);
        return result;
    }

    public String toString()
    {
        return String.format( "TxPullResponse{storeId=%s, tx=%s}", this.storeId, this.tx );
    }
}
