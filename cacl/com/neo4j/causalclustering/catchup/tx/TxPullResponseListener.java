package com.neo4j.causalclustering.catchup.tx;

public interface TxPullResponseListener
{
    void onTxReceived( TxPullResponse var1 );
}
