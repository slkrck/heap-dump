package com.neo4j.causalclustering.catchup.tx;

import com.neo4j.causalclustering.catchup.CatchupResult;
import com.neo4j.causalclustering.catchup.CatchupServerProtocol;
import com.neo4j.causalclustering.catchup.ResponseMessageType;
import com.neo4j.causalclustering.catchup.v3.tx.TxPullRequest;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.io.IOException;

import org.neo4j.kernel.database.Database;
import org.neo4j.kernel.impl.transaction.log.LogicalTransactionStore;
import org.neo4j.kernel.impl.transaction.log.NoSuchTransactionException;
import org.neo4j.kernel.impl.transaction.log.TransactionCursor;
import org.neo4j.logging.Log;
import org.neo4j.storageengine.api.StoreId;
import org.neo4j.storageengine.api.TransactionIdStore;

public class TxPullRequestHandler extends SimpleChannelInboundHandler<TxPullRequest>
{
    private final CatchupServerProtocol protocol;
    private final Database db;
    private final TransactionIdStore transactionIdStore;
    private final LogicalTransactionStore logicalTransactionStore;
    private final TxPullRequestsMonitor monitor;
    private final Log log;

    public TxPullRequestHandler( CatchupServerProtocol protocol, Database db )
    {
        this.protocol = protocol;
        this.db = db;
        this.transactionIdStore = transactionIdStore( db );
        this.logicalTransactionStore = logicalTransactionStore( db );
        this.monitor = (TxPullRequestsMonitor) db.getMonitors().newMonitor( TxPullRequestsMonitor.class, new String[0] );
        this.log = db.getInternalLogProvider().getLog( this.getClass() );
    }

    private static TransactionIdStore transactionIdStore( Database db )
    {
        return (TransactionIdStore) db.getDependencyResolver().resolveDependency( TransactionIdStore.class );
    }

    private static LogicalTransactionStore logicalTransactionStore( Database db )
    {
        return (LogicalTransactionStore) db.getDependencyResolver().resolveDependency( LogicalTransactionStore.class );
    }

    protected void channelRead0( ChannelHandlerContext ctx, TxPullRequest msg ) throws Exception
    {
        this.monitor.increment();
        TxPullRequestHandler.Prepare prepare = this.prepareRequest( msg );
        if ( prepare.isComplete() )
        {
            prepare.complete( ctx );
            this.protocol.expect( CatchupServerProtocol.State.MESSAGE_TYPE );
        }
        else
        {
            TxPullingContext txPullingContext = prepare.txPullingContext();
            TransactionStream txStream = new TransactionStream( this.log, txPullingContext, this.protocol );
            ctx.writeAndFlush( txStream ).addListener( ( f ) -> {
                if ( this.log.isDebugEnabled() || !f.isSuccess() )
                {
                    String message = String.format( "Streamed transactions [%d--%d] to %s", txPullingContext.firstTxId(), txStream.lastTxId(),
                            ctx.channel().remoteAddress() );
                    if ( f.isSuccess() )
                    {
                        this.log.debug( message );
                    }
                    else
                    {
                        this.log.warn( message, f.cause() );
                    }
                }
            } );
        }
    }

    private TxPullRequestHandler.Prepare prepareRequest( TxPullRequest msg ) throws IOException
    {
        if ( !this.isValid( msg ) )
        {
            return TxPullRequestHandler.Prepare.fail( CatchupResult.E_INVALID_REQUEST );
        }
        else
        {
            long firstTxId = msg.previousTxId() + 1L;
            if ( !this.databaseIsAvailable() )
            {
                this.log.info( "Failed to serve TxPullRequest for tx %d because the local database is unavailable.", new Object[]{firstTxId} );
                return TxPullRequestHandler.Prepare.fail( CatchupResult.E_STORE_UNAVAILABLE );
            }
            else
            {
                StoreId expectedStoreId = msg.expectedStoreId();
                StoreId localStoreId = this.db.getStoreId();
                if ( !localStoreId.equals( expectedStoreId ) )
                {
                    this.log.info( "Failed to serve TxPullRequest for tx %d and storeId %s because that storeId is different from this machine with %s",
                            new Object[]{firstTxId, expectedStoreId, localStoreId} );
                    return TxPullRequestHandler.Prepare.fail( CatchupResult.E_STORE_ID_MISMATCH );
                }
                else
                {
                    long txIdPromise;
                    try
                    {
                        txIdPromise = this.transactionIdStore.getLastCommittedTransactionId();
                    }
                    catch ( Exception var10 )
                    {
                        this.log.info( "Failed to serve TxPullRequest. Reason: %s", new Object[]{var10.getMessage()} );
                        return TxPullRequestHandler.Prepare.fail( CatchupResult.E_STORE_UNAVAILABLE );
                    }

                    if ( txIdPromise < firstTxId )
                    {
                        return TxPullRequestHandler.Prepare.nothingToSend( txIdPromise );
                    }
                    else
                    {
                        try
                        {
                            TransactionCursor transactions = this.logicalTransactionStore.getTransactions( firstTxId );
                            return TxPullRequestHandler.Prepare.readyToSend( new TxPullingContext( transactions, localStoreId, firstTxId, txIdPromise ) );
                        }
                        catch ( NoSuchTransactionException var9 )
                        {
                            this.log.info( "Failed to serve TxPullRequest for tx %d because the transaction does not exist. Last committed tx %d",
                                    new Object[]{firstTxId, txIdPromise} );
                            return TxPullRequestHandler.Prepare.fail( CatchupResult.E_TRANSACTION_PRUNED );
                        }
                    }
                }
            }
        }
    }

    private boolean isValid( TxPullRequest msg )
    {
        long previousTxId = msg.previousTxId();
        if ( previousTxId < 1L )
        {
            this.log.error( "Illegal tx pull request. Tx id must be greater or equal to %s but was %s", new Object[]{1L, previousTxId} );
            return false;
        }
        else
        {
            return true;
        }
    }

    private boolean databaseIsAvailable()
    {
        return this.db.getDatabaseAvailabilityGuard().isAvailable();
    }

    private static class Prepare
    {
        private final CatchupResult catchupResult;
        private final long txId;
        private final TxPullingContext txPullingContext;

        private Prepare( CatchupResult catchupResult, long txId, TxPullingContext txPullingContext )
        {
            this.catchupResult = catchupResult;
            this.txId = txId;
            this.txPullingContext = txPullingContext;
        }

        static TxPullRequestHandler.Prepare fail( CatchupResult catchupResult )
        {
            return new TxPullRequestHandler.Prepare( catchupResult, -1L, (TxPullingContext) null );
        }

        static TxPullRequestHandler.Prepare readyToSend( TxPullingContext txPullingContext )
        {
            return new TxPullRequestHandler.Prepare( (CatchupResult) null, txPullingContext.txIdPromise(), txPullingContext );
        }

        static TxPullRequestHandler.Prepare nothingToSend( long txIdPromise )
        {
            return new TxPullRequestHandler.Prepare( CatchupResult.SUCCESS_END_OF_STREAM, txIdPromise, (TxPullingContext) null );
        }

        public boolean isComplete()
        {
            return this.catchupResult != null;
        }

        private void complete( ChannelHandlerContext ctx )
        {
            if ( this.catchupResult == null )
            {
                throw new IllegalStateException( "Cannot complete catchup request." );
            }
            else
            {
                ctx.write( ResponseMessageType.TX_STREAM_FINISHED );
                ctx.writeAndFlush( new TxStreamFinishedResponse( this.catchupResult, this.txId ) );
            }
        }

        TxPullingContext txPullingContext()
        {
            return this.txPullingContext;
        }
    }
}
