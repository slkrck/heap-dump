package com.neo4j.causalclustering.catchup.error;

import com.neo4j.causalclustering.catchup.CatchupErrorResponse;
import com.neo4j.causalclustering.catchup.CatchupServerProtocol;
import com.neo4j.causalclustering.catchup.ResponseMessageType;
import com.neo4j.causalclustering.messaging.CatchupProtocolMessage;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

abstract class ErrorReportingHandler<T extends CatchupProtocolMessage> extends SimpleChannelInboundHandler<T>
{
    private final CatchupServerProtocol protocol;
    private final Log log;

    ErrorReportingHandler( Class<T> messageType, CatchupServerProtocol protocol, LogProvider logProvider )
    {
        super( messageType );
        this.protocol = protocol;
        this.log = logProvider.getLog( this.getClass() );
    }

    protected final void channelRead0( ChannelHandlerContext ctx, T request )
    {
        CatchupErrorResponse errorResponse = this.newErrorResponse( request );
        this.log.warn( errorResponse.message() );
        ctx.write( ResponseMessageType.ERROR );
        ctx.writeAndFlush( errorResponse );
        this.protocol.expect( CatchupServerProtocol.State.MESSAGE_TYPE );
    }

    abstract CatchupErrorResponse newErrorResponse( T var1 );
}
