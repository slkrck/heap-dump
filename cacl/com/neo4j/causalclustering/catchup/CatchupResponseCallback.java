package com.neo4j.causalclustering.catchup;

import com.neo4j.causalclustering.catchup.storecopy.FileChunk;
import com.neo4j.causalclustering.catchup.storecopy.FileHeader;
import com.neo4j.causalclustering.catchup.storecopy.GetStoreIdResponse;
import com.neo4j.causalclustering.catchup.storecopy.PrepareStoreCopyResponse;
import com.neo4j.causalclustering.catchup.storecopy.StoreCopyFinishedResponse;
import com.neo4j.causalclustering.catchup.tx.TxPullResponse;
import com.neo4j.causalclustering.catchup.tx.TxStreamFinishedResponse;
import com.neo4j.causalclustering.catchup.v3.databaseid.GetDatabaseIdResponse;
import com.neo4j.causalclustering.core.state.snapshot.CoreSnapshot;

import java.util.concurrent.CompletableFuture;

public interface CatchupResponseCallback<T>
{
    void onGetDatabaseIdResponse( CompletableFuture<T> var1, GetDatabaseIdResponse var2 );

    void onFileHeader( CompletableFuture<T> var1, FileHeader var2 );

    boolean onFileContent( CompletableFuture<T> var1, FileChunk var2 );

    void onFileStreamingComplete( CompletableFuture<T> var1, StoreCopyFinishedResponse var2 );

    void onTxPullResponse( CompletableFuture<T> var1, TxPullResponse var2 );

    void onTxStreamFinishedResponse( CompletableFuture<T> var1, TxStreamFinishedResponse var2 );

    void onGetStoreIdResponse( CompletableFuture<T> var1, GetStoreIdResponse var2 );

    void onCoreSnapshot( CompletableFuture<T> var1, CoreSnapshot var2 );

    void onStoreListingResponse( CompletableFuture<T> var1, PrepareStoreCopyResponse var2 );

    void onCatchupErrorResponse( CompletableFuture<T> var1, CatchupErrorResponse var2 );
}
