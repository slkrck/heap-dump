package com.neo4j.causalclustering.catchup;

import com.neo4j.causalclustering.catchup.storecopy.CopiedStoreRecovery;
import com.neo4j.causalclustering.catchup.storecopy.RemoteStore;
import com.neo4j.causalclustering.catchup.storecopy.StoreCopyClient;
import com.neo4j.causalclustering.catchup.storecopy.StoreCopyProcess;
import com.neo4j.causalclustering.catchup.tx.TransactionLogCatchUpFactory;
import com.neo4j.causalclustering.catchup.tx.TxPullClient;
import com.neo4j.causalclustering.common.PipelineBuilders;
import com.neo4j.causalclustering.common.TransactionBackupServiceProvider;
import com.neo4j.causalclustering.core.CausalClusteringSettings;
import com.neo4j.causalclustering.core.SupportedProtocolCreator;
import com.neo4j.causalclustering.net.BootstrapConfiguration;
import com.neo4j.causalclustering.net.InstalledProtocolHandler;
import com.neo4j.causalclustering.net.Server;
import com.neo4j.causalclustering.protocol.handshake.ApplicationSupportedProtocols;
import com.neo4j.causalclustering.protocol.handshake.ModifierSupportedProtocols;
import com.neo4j.dbms.database.ClusteredDatabaseContext;

import java.time.Duration;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import org.neo4j.configuration.Config;
import org.neo4j.configuration.connectors.ConnectorPortRegister;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.graphdb.factory.module.GlobalModule;
import org.neo4j.internal.helpers.ExponentialBackoffStrategy;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.kernel.lifecycle.LifeSupport;
import org.neo4j.logging.LogProvider;
import org.neo4j.logging.internal.DatabaseLogProvider;
import org.neo4j.monitoring.Monitors;
import org.neo4j.scheduler.JobScheduler;
import org.neo4j.storageengine.api.StorageEngineFactory;

public final class CatchupComponentsProvider
{
    private static final String CATCHUP_SERVER_NAME = "catchup-server";
    private final PipelineBuilders pipelineBuilders;
    private final LogProvider logProvider;
    private final ApplicationSupportedProtocols supportedCatchupProtocols;
    private final List<ModifierSupportedProtocols> supportedModifierProtocols;
    private final Config config;
    private final LogProvider userLogProvider;
    private final JobScheduler scheduler;
    private final LifeSupport globalLife;
    private final CatchupClientFactory catchupClientFactory;
    private final ConnectorPortRegister portRegister;
    private final CopiedStoreRecovery copiedStoreRecovery;
    private final PageCache pageCache;
    private final FileSystemAbstraction fileSystem;
    private final StorageEngineFactory storageEngineFactory;
    private final ExponentialBackoffStrategy storeCopyBackoffStrategy;

    public CatchupComponentsProvider( GlobalModule globalModule, PipelineBuilders pipelineBuilders )
    {
        this.pipelineBuilders = pipelineBuilders;
        this.logProvider = globalModule.getLogService().getInternalLogProvider();
        this.config = globalModule.getGlobalConfig();
        SupportedProtocolCreator supportedProtocolCreator = new SupportedProtocolCreator( this.config, this.logProvider );
        this.supportedCatchupProtocols = supportedProtocolCreator.getSupportedCatchupProtocolsFromConfiguration();
        this.supportedModifierProtocols = supportedProtocolCreator.createSupportedModifierProtocols();
        this.userLogProvider = globalModule.getLogService().getUserLogProvider();
        this.scheduler = globalModule.getJobScheduler();
        this.pageCache = globalModule.getPageCache();
        this.globalLife = globalModule.getGlobalLife();
        this.fileSystem = globalModule.getFileSystem();
        this.catchupClientFactory = this.createCatchupClientFactory();
        this.portRegister = globalModule.getConnectorPortRegister();
        this.storageEngineFactory = globalModule.getStorageEngineFactory();
        this.copiedStoreRecovery =
                (CopiedStoreRecovery) this.globalLife.add( new CopiedStoreRecovery( this.pageCache, this.fileSystem, globalModule.getStorageEngineFactory() ) );
        this.storeCopyBackoffStrategy =
                new ExponentialBackoffStrategy( 1L, ((Duration) this.config.get( CausalClusteringSettings.store_copy_backoff_max_wait )).toMillis(),
                        TimeUnit.MILLISECONDS );
    }

    private CatchupClientFactory createCatchupClientFactory()
    {
        CatchupClientFactory catchupClient = CatchupClientBuilder.builder().catchupProtocols( this.supportedCatchupProtocols ).modifierProtocols(
                this.supportedModifierProtocols ).pipelineBuilder( this.pipelineBuilders.client() ).inactivityTimeout(
                (Duration) this.config.get( CausalClusteringSettings.catch_up_client_inactivity_timeout ) ).scheduler( this.scheduler ).bootstrapConfig(
                BootstrapConfiguration.clientConfig( this.config ) ).handShakeTimeout(
                (Duration) this.config.get( CausalClusteringSettings.handshake_timeout ) ).debugLogProvider( this.logProvider ).userLogProvider(
                this.userLogProvider ).build();
        this.globalLife.add( catchupClient );
        return catchupClient;
    }

    public Server createCatchupServer( InstalledProtocolHandler installedProtocolsHandler, CatchupServerHandler catchupServerHandler )
    {
        return CatchupServerBuilder.builder().catchupServerHandler( catchupServerHandler ).catchupProtocols( this.supportedCatchupProtocols ).modifierProtocols(
                this.supportedModifierProtocols ).pipelineBuilder( this.pipelineBuilders.server() ).installedProtocolsHandler(
                installedProtocolsHandler ).listenAddress( (SocketAddress) this.config.get( CausalClusteringSettings.transaction_listen_address ) ).scheduler(
                this.scheduler ).config( this.config ).bootstrapConfig( BootstrapConfiguration.serverConfig( this.config ) ).portRegister(
                this.portRegister ).userLogProvider( this.userLogProvider ).debugLogProvider( this.logProvider ).serverName(
                "catchup-server" ).handshakeTimeout( (Duration) this.config.get( CausalClusteringSettings.handshake_timeout ) ).build();
    }

    public Optional<Server> createBackupServer( InstalledProtocolHandler installedProtocolsHandler, CatchupServerHandler catchupServerHandler )
    {
        TransactionBackupServiceProvider transactionBackupServiceProvider =
                new TransactionBackupServiceProvider( this.logProvider, this.supportedCatchupProtocols, this.supportedModifierProtocols,
                        this.pipelineBuilders.backupServer(), catchupServerHandler, installedProtocolsHandler, this.scheduler, this.portRegister );
        return transactionBackupServiceProvider.resolveIfBackupEnabled( this.config );
    }

    public CatchupComponentsRepository.CatchupComponents createDatabaseComponents( ClusteredDatabaseContext clusteredDatabaseContext )
    {
        DatabaseLogProvider databaseLogProvider = clusteredDatabaseContext.database().getInternalLogProvider();
        Monitors monitors = clusteredDatabaseContext.monitors();
        StoreCopyClient storeCopyClient = new StoreCopyClient( this.catchupClientFactory, clusteredDatabaseContext.databaseId(), () -> {
            return monitors;
        }, databaseLogProvider, this.storeCopyBackoffStrategy );
        TransactionLogCatchUpFactory transactionLogFactory = new TransactionLogCatchUpFactory();
        TxPullClient txPullClient = new TxPullClient( this.catchupClientFactory, clusteredDatabaseContext.databaseId(), () -> {
            return monitors;
        }, databaseLogProvider );
        RemoteStore remoteStore =
                new RemoteStore( databaseLogProvider, this.fileSystem, this.pageCache, storeCopyClient, txPullClient, transactionLogFactory, this.config,
                        monitors, this.storageEngineFactory, clusteredDatabaseContext.databaseId() );
        StoreCopyProcess storeCopy =
                new StoreCopyProcess( this.fileSystem, this.pageCache, clusteredDatabaseContext, this.copiedStoreRecovery, remoteStore, databaseLogProvider );
        return new CatchupComponentsRepository.CatchupComponents( remoteStore, storeCopy );
    }

    public CatchupClientFactory catchupClientFactory()
    {
        return this.catchupClientFactory;
    }
}
