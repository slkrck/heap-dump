package com.neo4j.causalclustering.catchup;

import com.neo4j.causalclustering.catchup.storecopy.FileChunk;
import com.neo4j.causalclustering.catchup.storecopy.FileHeader;
import com.neo4j.causalclustering.catchup.storecopy.GetStoreIdResponse;
import com.neo4j.causalclustering.catchup.storecopy.PrepareStoreCopyResponse;
import com.neo4j.causalclustering.catchup.storecopy.StoreCopyFinishedResponse;
import com.neo4j.causalclustering.catchup.tx.TxPullResponse;
import com.neo4j.causalclustering.catchup.tx.TxStreamFinishedResponse;
import com.neo4j.causalclustering.catchup.v3.databaseid.GetDatabaseIdResponse;
import com.neo4j.causalclustering.core.state.snapshot.CoreSnapshot;

import java.util.concurrent.CompletableFuture;

public class CatchupResponseAdaptor<T> implements CatchupResponseCallback<T>
{
    public void onGetDatabaseIdResponse( CompletableFuture<T> signal, GetDatabaseIdResponse response )
    {
        this.unimplementedMethod( signal, response );
    }

    public void onFileHeader( CompletableFuture<T> signal, FileHeader response )
    {
        this.unimplementedMethod( signal, response );
    }

    public boolean onFileContent( CompletableFuture<T> signal, FileChunk response )
    {
        this.unimplementedMethod( signal, response );
        return false;
    }

    public void onFileStreamingComplete( CompletableFuture<T> signal, StoreCopyFinishedResponse response )
    {
        this.unimplementedMethod( signal, response );
    }

    public void onTxPullResponse( CompletableFuture<T> signal, TxPullResponse response )
    {
        this.unimplementedMethod( signal, response );
    }

    public void onTxStreamFinishedResponse( CompletableFuture<T> signal, TxStreamFinishedResponse response )
    {
        this.unimplementedMethod( signal, response );
    }

    public void onGetStoreIdResponse( CompletableFuture<T> signal, GetStoreIdResponse response )
    {
        this.unimplementedMethod( signal, response );
    }

    public void onCoreSnapshot( CompletableFuture<T> signal, CoreSnapshot response )
    {
        this.unimplementedMethod( signal, response );
    }

    public void onStoreListingResponse( CompletableFuture<T> signal, PrepareStoreCopyResponse response )
    {
        this.unimplementedMethod( signal, response );
    }

    public void onCatchupErrorResponse( CompletableFuture<T> signal, CatchupErrorResponse catchupErrorResponse )
    {
        signal.completeExceptionally( new RuntimeException(
                String.format( "Request returned an error [Status: '%s' Message: '%s']", catchupErrorResponse.status(), catchupErrorResponse.message() ) ) );
    }

    private <U> void unimplementedMethod( CompletableFuture<T> signal, U response )
    {
        signal.completeExceptionally( new CatchUpProtocolViolationException( "This Adaptor has unimplemented methods for: %s", new Object[]{response} ) );
    }
}
