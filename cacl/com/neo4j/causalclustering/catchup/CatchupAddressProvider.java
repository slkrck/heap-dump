package com.neo4j.causalclustering.catchup;

import com.neo4j.causalclustering.core.LeaderProvider;
import com.neo4j.causalclustering.core.consensus.NoLeaderFoundException;
import com.neo4j.causalclustering.discovery.TopologyService;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.upstream.UpstreamDatabaseStrategySelector;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.kernel.database.NamedDatabaseId;

public interface CatchupAddressProvider
{
    SocketAddress primary( NamedDatabaseId var1 ) throws CatchupAddressResolutionException;

    SocketAddress secondary( NamedDatabaseId var1 ) throws CatchupAddressResolutionException;

    public static class LeaderOrUpstreamStrategyBasedAddressProvider implements CatchupAddressProvider
    {
        private final LeaderProvider leaderProvider;
        private final TopologyService topologyService;
        private final UpstreamAddressLookup secondaryUpstreamAddressLookup;

        public LeaderOrUpstreamStrategyBasedAddressProvider( LeaderProvider leaderProvider, TopologyService topologyService,
                UpstreamDatabaseStrategySelector strategySelector )
        {
            this.leaderProvider = leaderProvider;
            this.topologyService = topologyService;
            this.secondaryUpstreamAddressLookup = new UpstreamAddressLookup( strategySelector, topologyService );
        }

        public SocketAddress primary( NamedDatabaseId namedDatabaseId ) throws CatchupAddressResolutionException
        {
            try
            {
                MemberId leadMember = this.leaderProvider.getLeader( namedDatabaseId );
                return this.topologyService.lookupCatchupAddress( leadMember );
            }
            catch ( NoLeaderFoundException var3 )
            {
                throw new CatchupAddressResolutionException( var3 );
            }
        }

        public SocketAddress secondary( NamedDatabaseId namedDatabaseId ) throws CatchupAddressResolutionException
        {
            return this.secondaryUpstreamAddressLookup.lookupAddressForDatabase( namedDatabaseId );
        }
    }

    public static class UpstreamStrategyBasedAddressProvider implements CatchupAddressProvider
    {
        private final UpstreamAddressLookup upstreamAddressLookup;

        public UpstreamStrategyBasedAddressProvider( TopologyService topologyService, UpstreamDatabaseStrategySelector strategySelector )
        {
            this.upstreamAddressLookup = new UpstreamAddressLookup( strategySelector, topologyService );
        }

        public SocketAddress primary( NamedDatabaseId namedDatabaseId ) throws CatchupAddressResolutionException
        {
            return this.upstreamAddressLookup.lookupAddressForDatabase( namedDatabaseId );
        }

        public SocketAddress secondary( NamedDatabaseId namedDatabaseId ) throws CatchupAddressResolutionException
        {
            return this.upstreamAddressLookup.lookupAddressForDatabase( namedDatabaseId );
        }
    }

    public static class SingleAddressProvider implements CatchupAddressProvider
    {
        private final SocketAddress socketAddress;

        public SingleAddressProvider( SocketAddress socketAddress )
        {
            this.socketAddress = socketAddress;
        }

        public SocketAddress primary( NamedDatabaseId namedDatabaseId )
        {
            return this.socketAddress;
        }

        public SocketAddress secondary( NamedDatabaseId namedDatabaseId )
        {
            return this.socketAddress;
        }
    }
}
