package com.neo4j.causalclustering.catchup;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandler;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;

import java.util.HashMap;
import java.util.Map;

import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class RequestDecoderDispatcher<E extends Enum<E>> extends ChannelInboundHandlerAdapter
{
    private final Map<E,ChannelInboundHandler> decoders = new HashMap();
    private final Protocol<E> protocol;
    private final Log log;

    public RequestDecoderDispatcher( Protocol<E> protocol, LogProvider logProvider )
    {
        this.protocol = protocol;
        this.log = logProvider.getLog( this.getClass() );
    }

    public void channelRead( ChannelHandlerContext ctx, Object msg ) throws Exception
    {
        ChannelInboundHandler delegate = (ChannelInboundHandler) this.protocol.select( this.decoders );
        if ( delegate == null )
        {
            this.log.warn( "Unregistered handler for protocol %s", new Object[]{this.protocol} );
            ReferenceCountUtil.release( msg );
        }
        else
        {
            delegate.channelRead( ctx, msg );
        }
    }

    public void register( E type, ChannelInboundHandler decoder )
    {
        assert !this.decoders.containsKey( type ) : "registering twice a decoder for the same type (" + type + ")?";

        this.decoders.put( type, decoder );
    }
}
