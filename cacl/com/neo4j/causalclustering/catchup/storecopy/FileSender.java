package com.neo4j.causalclustering.catchup.storecopy;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.stream.ChunkedInput;

import java.io.IOException;
import java.util.Objects;

import org.neo4j.io.fs.StoreChannel;

class FileSender implements ChunkedInput<FileChunk>
{
    private final StoreResource resource;
    private final int maxChunkSize;
    private StoreChannel channel;
    private ByteBuf nextPayload;
    private FileSender.State state;

    FileSender( StoreResource resource, int maxChunkSize )
    {
        this.state = FileSender.State.PRE_INIT;
        this.resource = resource;
        this.maxChunkSize = maxChunkSize;
    }

    public boolean isEndOfInput()
    {
        return this.state == FileSender.State.FINISHED;
    }

    public void close() throws Exception
    {
        if ( this.channel != null )
        {
            this.channel.close();
            this.channel = null;
        }
    }

    public FileChunk readChunk( ByteBufAllocator allocator ) throws Exception
    {
        if ( this.state == FileSender.State.FINISHED )
        {
            return null;
        }
        else
        {
            if ( this.state == FileSender.State.PRE_INIT )
            {
                this.channel = this.resource.open();
                this.nextPayload = this.prefetch( allocator );
                if ( this.nextPayload == null )
                {
                    this.state = FileSender.State.FINISHED;
                    return FileChunk.create( Unpooled.EMPTY_BUFFER, true, this.maxChunkSize );
                }

                this.state = this.nextPayload.readableBytes() < this.maxChunkSize ? FileSender.State.LAST_PENDING : FileSender.State.FULL_PENDING;
            }

            if ( this.state == FileSender.State.FULL_PENDING )
            {
                ByteBuf toSend = this.nextPayload;
                this.nextPayload = this.prefetch( allocator );
                if ( this.nextPayload == null )
                {
                    this.state = FileSender.State.FINISHED;
                    return FileChunk.create( toSend, true, this.maxChunkSize );
                }
                else if ( this.nextPayload.readableBytes() < this.maxChunkSize )
                {
                    this.state = FileSender.State.LAST_PENDING;
                    return FileChunk.create( toSend, false, this.maxChunkSize );
                }
                else
                {
                    return FileChunk.create( toSend, false, this.maxChunkSize );
                }
            }
            else if ( this.state == FileSender.State.LAST_PENDING )
            {
                this.state = FileSender.State.FINISHED;
                return FileChunk.create( this.nextPayload, true, this.maxChunkSize );
            }
            else
            {
                throw new IllegalStateException();
            }
        }
    }

    public FileChunk readChunk( ChannelHandlerContext ctx ) throws Exception
    {
        return this.readChunk( ctx.alloc() );
    }

    public long length()
    {
        return -1L;
    }

    public long progress()
    {
        return 0L;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            FileSender that = (FileSender) o;
            return Objects.equals( this.resource, that.resource );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.resource} );
    }

    private ByteBuf prefetch( ByteBufAllocator allocator ) throws IOException
    {
        ByteBuf payload = allocator.ioBuffer( this.maxChunkSize );
        int totalRead = 0;

        try
        {
            totalRead = this.read( payload );
        }
        finally
        {
            if ( totalRead == 0 )
            {
                payload.release();
                payload = null;
            }
        }

        return payload;
    }

    private int read( ByteBuf payload ) throws IOException
    {
        int totalRead = 0;

        do
        {
            int bytesReadOrEOF = payload.writeBytes( this.channel, this.maxChunkSize - totalRead );
            if ( bytesReadOrEOF < 0 )
            {
                break;
            }

            totalRead += bytesReadOrEOF;
        }
        while ( totalRead < this.maxChunkSize );

        return totalRead;
    }

    static enum State
    {
        PRE_INIT,
        FULL_PENDING,
        LAST_PENDING,
        FINISHED;
    }
}
