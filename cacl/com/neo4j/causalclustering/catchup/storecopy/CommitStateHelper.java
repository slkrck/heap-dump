package com.neo4j.causalclustering.catchup.storecopy;

import java.io.IOException;
import java.util.Optional;

import org.neo4j.configuration.Config;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.kernel.impl.transaction.CommittedTransactionRepresentation;
import org.neo4j.kernel.impl.transaction.log.NoSuchTransactionException;
import org.neo4j.kernel.impl.transaction.log.ReadOnlyTransactionStore;
import org.neo4j.kernel.impl.transaction.log.TransactionCursor;
import org.neo4j.kernel.impl.transaction.log.files.LogFilesBuilder;
import org.neo4j.kernel.lifecycle.Lifecycle;
import org.neo4j.kernel.lifecycle.Lifespan;
import org.neo4j.monitoring.Monitors;
import org.neo4j.storageengine.api.StorageEngineFactory;
import org.neo4j.storageengine.api.TransactionIdStore;

public class CommitStateHelper
{
    private final StorageEngineFactory storageEngineFactory;
    private PageCache pageCache;
    private FileSystemAbstraction fs;
    private Config config;

    public CommitStateHelper( PageCache pageCache, FileSystemAbstraction fs, Config config, StorageEngineFactory storageEngineFactory )
    {
        this.pageCache = pageCache;
        this.fs = fs;
        this.config = config;
        this.storageEngineFactory = storageEngineFactory;
    }

    CommitState getStoreState( DatabaseLayout databaseLayout ) throws IOException
    {
        TransactionIdStore txIdStore = this.storageEngineFactory.readOnlyTransactionIdStore( this.fs, databaseLayout, this.pageCache );
        long lastCommittedTxId = txIdStore.getLastCommittedTransactionId();
        Optional<Long> latestTransactionLogIndex = this.getLatestTransactionLogIndex( lastCommittedTxId, databaseLayout );
        return latestTransactionLogIndex.isPresent() ? new CommitState( lastCommittedTxId, (Long) latestTransactionLogIndex.get() )
                                                     : new CommitState( lastCommittedTxId );
    }

    private Optional<Long> getLatestTransactionLogIndex( long startTxId, DatabaseLayout databaseLayout ) throws IOException
    {
        if ( !this.hasTxLogs( databaseLayout ) )
        {
            return Optional.empty();
        }
        else
        {
            ReadOnlyTransactionStore txStore = new ReadOnlyTransactionStore( this.pageCache, this.fs, databaseLayout, this.config, new Monitors() );
            long lastTxId = 1L;

            try
            {
                Lifespan ignored = new Lifespan( new Lifecycle[]{txStore} );

                Optional var16;
                try
                {
                    TransactionCursor cursor = txStore.getTransactions( startTxId );

                    try
                    {
                        while ( cursor.next() )
                        {
                            CommittedTransactionRepresentation tx = (CommittedTransactionRepresentation) cursor.get();
                            lastTxId = tx.getCommitEntry().getTxId();
                        }

                        var16 = Optional.of( lastTxId );
                    }
                    catch ( Throwable var13 )
                    {
                        if ( cursor != null )
                        {
                            try
                            {
                                cursor.close();
                            }
                            catch ( Throwable var12 )
                            {
                                var13.addSuppressed( var12 );
                            }
                        }

                        throw var13;
                    }

                    if ( cursor != null )
                    {
                        cursor.close();
                    }
                }
                catch ( Throwable var14 )
                {
                    try
                    {
                        ignored.close();
                    }
                    catch ( Throwable var11 )
                    {
                        var14.addSuppressed( var11 );
                    }

                    throw var14;
                }

                ignored.close();
                return var16;
            }
            catch ( NoSuchTransactionException var15 )
            {
                return Optional.empty();
            }
        }
    }

    public boolean hasTxLogs( DatabaseLayout databaseLayout ) throws IOException
    {
        return LogFilesBuilder.activeFilesBuilder( databaseLayout, this.fs, this.pageCache ).withConfig( this.config ).build().logFiles().length > 0;
    }
}
