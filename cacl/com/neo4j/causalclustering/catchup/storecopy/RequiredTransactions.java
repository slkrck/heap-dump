package com.neo4j.causalclustering.catchup.storecopy;

import org.neo4j.internal.helpers.collection.LongRange;

class RequiredTransactions
{
    private static final long NO_REQUIRED_TX_ID = -1L;
    private final long startTxId;
    private final long requiredTx;

    private RequiredTransactions( long startTxId, long requiredTx )
    {
        if ( startTxId < 0L )
        {
            throw new IllegalArgumentException( "Start tx id cannot be negative. Got: " + startTxId );
        }
        else
        {
            this.startTxId = startTxId;
            this.requiredTx = requiredTx;
        }
    }

    static RequiredTransactions requiredRange( long startTxId, long requiredTxId )
    {
        LongRange.assertIsRange( startTxId, requiredTxId );
        return new RequiredTransactions( startTxId, requiredTxId );
    }

    static RequiredTransactions noConstraint( long myHighestTxId )
    {
        return new RequiredTransactions( myHighestTxId, -1L );
    }

    long startTxId()
    {
        return this.startTxId;
    }

    boolean noRequiredTxId()
    {
        return this.requiredTx == -1L;
    }

    long requiredTxId()
    {
        return this.requiredTx;
    }

    public String toString()
    {
        return "RequiredTransactions{from=" + this.startTxId + ", to=" + this.requiredTx + "}";
    }
}
