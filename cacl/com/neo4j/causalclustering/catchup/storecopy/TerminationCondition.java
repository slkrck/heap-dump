package com.neo4j.causalclustering.catchup.storecopy;

@FunctionalInterface
interface TerminationCondition
{
    TerminationCondition CONTINUE_INDEFINITELY = () -> {
    };

    void assertContinue() throws StoreCopyFailedException;
}
