package com.neo4j.causalclustering.catchup.storecopy;

import java.util.Optional;

class CommitState
{
    private final long metaDataStoreIndex;
    private final Long transactionLogIndex;

    CommitState( long metaDataStoreIndex )
    {
        this.metaDataStoreIndex = metaDataStoreIndex;
        this.transactionLogIndex = null;
    }

    CommitState( long metaDataStoreIndex, long transactionLogIndex )
    {
        assert transactionLogIndex >= metaDataStoreIndex;

        this.metaDataStoreIndex = metaDataStoreIndex;
        this.transactionLogIndex = transactionLogIndex;
    }

    long metaDataStoreIndex()
    {
        return this.metaDataStoreIndex;
    }

    Optional<Long> transactionLogIndex()
    {
        return Optional.ofNullable( this.transactionLogIndex );
    }

    public String toString()
    {
        return "CommitState{metaDataStoreIndex=" + this.metaDataStoreIndex + ", transactionLogIndex=" + this.transactionLogIndex + "}";
    }
}
