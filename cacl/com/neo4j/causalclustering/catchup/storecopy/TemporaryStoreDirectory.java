package com.neo4j.causalclustering.catchup.storecopy;

import java.io.File;
import java.io.IOException;

import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.io.layout.Neo4jLayout;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.kernel.impl.transaction.log.files.LogFiles;
import org.neo4j.kernel.impl.transaction.log.files.LogFilesBuilder;

public class TemporaryStoreDirectory implements AutoCloseable
{
    private final File tempHomeDir;
    private final DatabaseLayout tempDatabaseLayout;
    private final FileSystemAbstraction fs;
    private final StoreFiles storeFiles;
    private final LogFiles tempLogFiles;
    private boolean keepStore;

    TemporaryStoreDirectory( FileSystemAbstraction fs, PageCache pageCache, DatabaseLayout databaseLayout ) throws IOException
    {
        this.tempHomeDir = databaseLayout.file( "temp-copy" );
        this.tempDatabaseLayout = Neo4jLayout.ofFlat( this.tempHomeDir ).databaseLayout( databaseLayout.getDatabaseName() );
        this.fs = fs;
        this.storeFiles = new StoreFiles( fs, pageCache, ( directory, name ) -> {
            return true;
        } );
        this.tempLogFiles = LogFilesBuilder.logFilesBasedOnlyBuilder( this.tempDatabaseLayout.getTransactionLogsDirectory(), fs ).build();
        this.storeFiles.delete( this.tempDatabaseLayout, this.tempLogFiles );
    }

    public DatabaseLayout databaseLayout()
    {
        return this.tempDatabaseLayout;
    }

    void keepStore()
    {
        this.keepStore = true;
    }

    public void close() throws IOException
    {
        if ( !this.keepStore )
        {
            this.storeFiles.delete( this.tempDatabaseLayout, this.tempLogFiles );
            this.fs.deleteFile( this.tempHomeDir );
        }
    }
}
