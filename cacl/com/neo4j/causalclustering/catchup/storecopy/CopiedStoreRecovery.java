package com.neo4j.causalclustering.catchup.storecopy;

import java.io.IOException;
import java.util.Optional;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.kernel.recovery.Recovery;
import org.neo4j.logging.internal.NullLogService;
import org.neo4j.storageengine.api.StorageEngineFactory;
import org.neo4j.storageengine.api.StoreVersion;
import org.neo4j.storageengine.api.StoreVersionCheck;
import org.neo4j.storageengine.migration.UpgradeNotAllowedException;

public class CopiedStoreRecovery extends LifecycleAdapter
{
    private final PageCache pageCache;
    private final FileSystemAbstraction fs;
    private final StorageEngineFactory storageEngineFactory;
    private boolean shutdown;

    public CopiedStoreRecovery( PageCache pageCache, FileSystemAbstraction fs, StorageEngineFactory storageEngineFactory )
    {
        this.pageCache = pageCache;
        this.fs = fs;
        this.storageEngineFactory = storageEngineFactory;
    }

    public synchronized void shutdown()
    {
        this.shutdown = true;
    }

    public synchronized void recoverCopiedStore( Config config, DatabaseLayout databaseLayout ) throws DatabaseShutdownException, IOException
    {
        if ( this.shutdown )
        {
            throw new DatabaseShutdownException( "Abort store-copied store recovery due to database shutdown" );
        }
        else
        {
            StoreVersionCheck storeVersionCheck =
                    this.storageEngineFactory.versionCheck( this.fs, databaseLayout, config, this.pageCache, NullLogService.getInstance() );
            Optional<String> storeVersion = storeVersionCheck.storeVersion();
            if ( databaseLayout.getDatabaseName().equals( "system" ) )
            {
                config = Config.newBuilder().fromConfig( config ).set( GraphDatabaseSettings.record_format, "standard" ).build();
            }
            else if ( storeVersion.isPresent() )
            {
                StoreVersion version = storeVersionCheck.versionInformation( (String) storeVersion.get() );
                String configuredVersion = storeVersionCheck.configuredVersion();
                if ( configuredVersion != null && !version.isCompatibleWith( storeVersionCheck.versionInformation( configuredVersion ) ) )
                {
                    throw new RuntimeException( this.failedToStartMessage( config ) );
                }
            }

            try
            {
                Recovery.performRecovery( this.fs, this.pageCache, config, databaseLayout, this.storageEngineFactory );
            }
            catch ( Exception var7 )
            {
                if ( ExceptionUtils.indexOfThrowable( var7, UpgradeNotAllowedException.class ) != -1 )
                {
                    throw new RuntimeException( this.failedToStartMessage( config ), var7 );
                }
                else
                {
                    throw var7;
                }
            }
        }
    }

    private String failedToStartMessage( Config config )
    {
        String recordFormat = (String) config.get( GraphDatabaseSettings.record_format );
        return String.format(
                "Failed to start database with copied store. This may be because the core servers and read replicas have a different record format. On this machine: `%s=%s`. Check the equivalent value on the core server.",
                GraphDatabaseSettings.record_format.name(), recordFormat );
    }
}
