package com.neo4j.causalclustering.catchup.storecopy;

import java.time.Clock;
import java.time.Duration;

import org.neo4j.time.Clocks;

public class MaximumTotalTime implements TerminationCondition
{
    private final long endTime;
    private final Clock clock;
    private final Duration duration;

    MaximumTotalTime( Duration duration, Clock clock )
    {
        this.duration = duration;
        this.endTime = clock.millis() + duration.toMillis();
        this.clock = clock;
    }

    MaximumTotalTime( Duration duration )
    {
        this( duration, Clocks.systemClock() );
    }

    public void assertContinue() throws StoreCopyFailedException
    {
        if ( this.clock.millis() > this.endTime )
        {
            throw new StoreCopyFailedException( String.format( "Maximum time passed %s. Not allowed to continue", this.duration ) );
        }
    }
}
