package com.neo4j.causalclustering.catchup.storecopy;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

public class FileChunkDecoder extends ByteToMessageDecoder
{
    protected void decode( ChannelHandlerContext ctx, ByteBuf frame, List<Object> out )
    {
        int header = frame.readInt();
        boolean isLast = FileChunk.parseHeader( header );
        ByteBuf payload = frame.readRetainedSlice( frame.readableBytes() );
        boolean success = false;

        try
        {
            FileChunk fileChunk = new FileChunk( isLast, payload );
            out.add( fileChunk );
            success = true;
        }
        finally
        {
            if ( !success )
            {
                payload.release();
            }
        }
    }
}
