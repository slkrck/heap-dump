package com.neo4j.causalclustering.catchup.storecopy;

public class DatabaseShutdownException extends Exception
{
    DatabaseShutdownException( String message )
    {
        super( message );
    }
}
