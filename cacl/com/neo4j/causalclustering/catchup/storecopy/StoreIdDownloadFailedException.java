package com.neo4j.causalclustering.catchup.storecopy;

public class StoreIdDownloadFailedException extends Exception
{
    public StoreIdDownloadFailedException( Throwable cause )
    {
        super( cause );
    }

    public StoreIdDownloadFailedException( String message )
    {
        super( message );
    }
}
