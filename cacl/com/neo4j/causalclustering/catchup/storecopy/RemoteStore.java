package com.neo4j.causalclustering.catchup.storecopy;

import com.neo4j.causalclustering.catchup.CatchupAddressProvider;
import com.neo4j.causalclustering.catchup.tx.TransactionLogCatchUpFactory;
import com.neo4j.causalclustering.catchup.tx.TransactionLogCatchUpWriter;
import com.neo4j.causalclustering.catchup.tx.TxPullClient;
import com.neo4j.causalclustering.core.CausalClusteringSettings;

import java.io.IOException;
import java.time.Duration;

import org.neo4j.configuration.Config;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.internal.helpers.collection.LongRange;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.monitoring.Monitors;
import org.neo4j.storageengine.api.StorageEngineFactory;
import org.neo4j.storageengine.api.StoreId;

public class RemoteStore
{
    private final Log log;
    private final Monitors monitors;
    private final Config config;
    private final FileSystemAbstraction fs;
    private final PageCache pageCache;
    private final LogProvider logProvider;
    private final StoreCopyClient storeCopyClient;
    private final TxPullClient txPullClient;
    private final TransactionLogCatchUpFactory transactionLogFactory;
    private final CommitStateHelper commitStateHelper;
    private final StoreCopyClientMonitor storeCopyClientMonitor;
    private final StorageEngineFactory storageEngineFactory;
    private final NamedDatabaseId namedDatabaseId;

    public RemoteStore( LogProvider logProvider, FileSystemAbstraction fs, PageCache pageCache, StoreCopyClient storeCopyClient, TxPullClient txPullClient,
            TransactionLogCatchUpFactory transactionLogFactory, Config config, Monitors monitors, StorageEngineFactory storageEngineFactory,
            NamedDatabaseId namedDatabaseId )
    {
        this.logProvider = logProvider;
        this.storeCopyClient = storeCopyClient;
        this.txPullClient = txPullClient;
        this.fs = fs;
        this.pageCache = pageCache;
        this.transactionLogFactory = transactionLogFactory;
        this.config = config;
        this.log = logProvider.getLog( this.getClass() );
        this.monitors = monitors;
        this.storeCopyClientMonitor = (StoreCopyClientMonitor) monitors.newMonitor( StoreCopyClientMonitor.class, new String[0] );
        this.storageEngineFactory = storageEngineFactory;
        this.namedDatabaseId = namedDatabaseId;
        this.commitStateHelper = new CommitStateHelper( pageCache, fs, config, storageEngineFactory );
    }

    public void tryCatchingUp( CatchupAddressProvider catchupAddressProvider, StoreId expectedStoreId, DatabaseLayout databaseLayout, boolean keepTxLogsInDir,
            boolean forceTransactionLogRotation ) throws StoreCopyFailedException, IOException
    {
        CommitState commitState = this.commitStateHelper.getStoreState( databaseLayout );
        this.log.info( "Store commit state: " + commitState );
        TxPullRequestContext txPullRequestContext = TxPullRequestContext.createContextFromCatchingUp( expectedStoreId, commitState );
        this.pullTransactions( catchupAddressProvider, databaseLayout, txPullRequestContext, false, keepTxLogsInDir, forceTransactionLogRotation );
    }

    public void copy( CatchupAddressProvider addressProvider, StoreId expectedStoreId, DatabaseLayout destinationLayout, boolean rotateTransactionsManually )
            throws StoreCopyFailedException
    {
        StreamToDiskProvider streamToDiskProvider = new StreamToDiskProvider( destinationLayout.databaseDirectory(), this.fs, this.monitors );
        RequiredTransactions requiredTransactions =
                this.storeCopyClient.copyStoreFiles( addressProvider, expectedStoreId, streamToDiskProvider, this::getTerminationCondition,
                        destinationLayout.databaseDirectory() );
        this.log.info( "Store files need to be recovered starting from: %s", new Object[]{requiredTransactions} );
        TxPullRequestContext context = TxPullRequestContext.createContextFromStoreCopy( requiredTransactions, expectedStoreId );
        this.pullTransactions( addressProvider, destinationLayout, context, true, true, rotateTransactionsManually );
    }

    private MaximumTotalTime getTerminationCondition()
    {
        return new MaximumTotalTime( (Duration) this.config.get( CausalClusteringSettings.store_copy_max_retry_time_per_request ) );
    }

    private void pullTransactions( CatchupAddressProvider catchupAddressProvider, DatabaseLayout databaseLayout, TxPullRequestContext context,
            boolean asPartOfStoreCopy, boolean keepTxLogsInStoreDir, boolean rotateTransactionsManually ) throws StoreCopyFailedException
    {
        this.storeCopyClientMonitor.startReceivingTransactions( context.startTxIdExclusive() );

        try
        {
            TransactionLogCatchUpWriter writer =
                    this.transactionLogFactory.create( databaseLayout, this.fs, this.pageCache, this.config, this.logProvider, this.storageEngineFactory,
                            this.validInitialTxRange( context ), asPartOfStoreCopy, keepTxLogsInStoreDir, rotateTransactionsManually );

            try
            {
                TxPuller txPuller = TxPuller.createTxPuller( catchupAddressProvider, this.logProvider, this.config, this.namedDatabaseId );
                txPuller.pullTransactions( context, writer, this.txPullClient );
                this.storeCopyClientMonitor.finishReceivingTransactions( writer.lastTx() );
            }
            catch ( Throwable var11 )
            {
                if ( writer != null )
                {
                    try
                    {
                        writer.close();
                    }
                    catch ( Throwable var10 )
                    {
                        var11.addSuppressed( var10 );
                    }
                }

                throw var11;
            }

            if ( writer != null )
            {
                writer.close();
            }
        }
        catch ( IOException var12 )
        {
            throw new StoreCopyFailedException( var12 );
        }
    }

    private LongRange validInitialTxRange( TxPullRequestContext context )
    {
        return LongRange.range( context.startTxIdExclusive() + 1L, context.fallbackStartId().orElse( context.startTxIdExclusive() ) + 1L );
    }

    public StoreId getStoreId( SocketAddress from ) throws StoreIdDownloadFailedException
    {
        return this.storeCopyClient.fetchStoreId( from );
    }
}
