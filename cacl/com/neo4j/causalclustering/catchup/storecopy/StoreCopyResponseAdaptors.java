package com.neo4j.causalclustering.catchup.storecopy;

import com.neo4j.causalclustering.catchup.CatchupErrorResponse;
import com.neo4j.causalclustering.catchup.CatchupResponseAdaptor;
import com.neo4j.causalclustering.catchup.CatchupResult;

import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;

import org.neo4j.logging.Log;

public abstract class StoreCopyResponseAdaptors<T> extends CatchupResponseAdaptor<T>
{
    private final StoreFileStreamProvider storeFileStreamProvider;
    private final Log log;
    private StoreFileStream storeFileStream;

    private StoreCopyResponseAdaptors( StoreFileStreamProvider storeFileStreamProvider, Log log )
    {
        this.storeFileStreamProvider = storeFileStreamProvider;
        this.log = log;
    }

    public static StoreCopyResponseAdaptors<StoreCopyFinishedResponse> filesCopyAdaptor( StoreFileStreamProvider storeFileStreamProvider, Log log )
    {
        return new StoreCopyResponseAdaptors.StoreFilesCopyResponseAdaptors( storeFileStreamProvider, log );
    }

    public static StoreCopyResponseAdaptors<PrepareStoreCopyResponse> prepareStoreCopyAdaptor( StoreFileStreamProvider storeFileStreamProvider, Log log )
    {
        return new StoreCopyResponseAdaptors.PrepareStoreCopyResponseAdaptors( storeFileStreamProvider, log );
    }

    public void onFileHeader( CompletableFuture<T> requestOutcomeSignal, FileHeader fileHeader )
    {
        try
        {
            this.log.info( "Receiving file: %s", new Object[]{fileHeader.fileName()} );
            StoreFileStream fileStream = this.storeFileStreamProvider.acquire( fileHeader.fileName(), fileHeader.requiredAlignment() );
            requestOutcomeSignal.whenComplete( new StoreCopyResponseAdaptors.CloseFileStreamOnComplete( fileStream, fileHeader.fileName() ) );
            this.storeFileStream = fileStream;
        }
        catch ( Exception var4 )
        {
            requestOutcomeSignal.completeExceptionally( var4 );
        }
    }

    public boolean onFileContent( CompletableFuture<T> signal, FileChunk fileChunk )
    {
        try
        {
            this.storeFileStream.write( fileChunk.payload() );
        }
        catch ( Exception var4 )
        {
            signal.completeExceptionally( var4 );
        }

        return fileChunk.isLast();
    }

    private static class StoreFilesCopyResponseAdaptors extends StoreCopyResponseAdaptors<StoreCopyFinishedResponse>
    {
        StoreFilesCopyResponseAdaptors( StoreFileStreamProvider storeFileStreamProvider, Log log )
        {
            super( storeFileStreamProvider, log );
        }

        public void onFileStreamingComplete( CompletableFuture<StoreCopyFinishedResponse> signal, StoreCopyFinishedResponse response )
        {
            signal.complete( response );
        }

        public void onCatchupErrorResponse( CompletableFuture<StoreCopyFinishedResponse> signal, CatchupErrorResponse catchupErrorResponse )
        {
            StoreCopyFinishedResponse.Status status =
                    catchupErrorResponse.status() == CatchupResult.E_DATABASE_UNKNOWN ? StoreCopyFinishedResponse.Status.E_DATABASE_UNKNOWN
                                                                                      : StoreCopyFinishedResponse.Status.E_UNKNOWN;
            signal.complete( new StoreCopyFinishedResponse( status, -1L ) );
        }
    }

    private static class PrepareStoreCopyResponseAdaptors extends StoreCopyResponseAdaptors<PrepareStoreCopyResponse>
    {
        PrepareStoreCopyResponseAdaptors( StoreFileStreamProvider storeFileStreamProvider, Log log )
        {
            super( storeFileStreamProvider, log );
        }

        public void onStoreListingResponse( CompletableFuture<PrepareStoreCopyResponse> signal, PrepareStoreCopyResponse response )
        {
            signal.complete( response );
        }
    }

    private class CloseFileStreamOnComplete<RESPONSE> implements BiConsumer<RESPONSE,Throwable>
    {
        private final StoreFileStream fileStream;
        private String fileName;

        private CloseFileStreamOnComplete( StoreFileStream fileStream, String fileName )
        {
            this.fileStream = fileStream;
            this.fileName = fileName;
        }

        public void accept( RESPONSE response, Throwable throwable )
        {
            try
            {
                this.fileStream.close();
            }
            catch ( Exception var4 )
            {
                StoreCopyResponseAdaptors.this.log.error( String.format( "Unable to close store file stream for file '%s'", this.fileName ), var4 );
            }
        }
    }
}
