package com.neo4j.causalclustering.catchup.storecopy;

import java.io.File;
import java.io.IOException;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.fs.FileUtils;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.kernel.database.Database;
import org.neo4j.storageengine.api.StoreFileMetadata;

public class PrepareStoreCopyFiles implements AutoCloseable
{
    private final Database database;
    private final FileSystemAbstraction fileSystemAbstraction;
    private final CloseablesListener closeablesListener = new CloseablesListener();

    PrepareStoreCopyFiles( Database database, FileSystemAbstraction fileSystemAbstraction )
    {
        this.database = database;
        this.fileSystemAbstraction = fileSystemAbstraction;
    }

    private static Predicate<StoreFileMetadata> isCountFile( DatabaseLayout databaseLayout )
    {
        return ( storeFileMetadata ) -> {
            return databaseLayout.countStore().equals( storeFileMetadata.file() );
        };
    }

    StoreResource[] getAtomicFilesSnapshot() throws IOException
    {
        ResourceIterator<StoreFileMetadata> neoStoreFilesIterator =
                (ResourceIterator) this.closeablesListener.add( this.database.getDatabaseFileListing().builder().excludeAll().includeNeoStoreFiles().build() );
        ResourceIterator<StoreFileMetadata> indexIterator = (ResourceIterator) this.closeablesListener.add(
                this.database.getDatabaseFileListing().builder().excludeAll().includeAdditionalProviders().includeLabelScanStoreFiles().includeSchemaIndexStoreFiles().includeIdFiles().build() );
        return (StoreResource[]) Stream.concat( neoStoreFilesIterator.stream().filter( isCountFile( this.database.getDatabaseLayout() ) ),
                indexIterator.stream() ).map( this.mapToStoreResource() ).toArray( ( x$0 ) -> {
            return new StoreResource[x$0];
        } );
    }

    private Function<StoreFileMetadata,StoreResource> mapToStoreResource()
    {
        return ( storeFileMetadata ) -> {
            try
            {
                return this.toStoreResource( storeFileMetadata );
            }
            catch ( IOException var3 )
            {
                throw new IllegalStateException( "Unable to create store resource", var3 );
            }
        };
    }

    File[] listReplayableFiles() throws IOException
    {
        Stream stream = this.database.getDatabaseFileListing().builder().excludeAll().includeNeoStoreFiles().build().stream();

        File[] var2;
        try
        {
            var2 = (File[]) stream.filter( isCountFile( this.database.getDatabaseLayout() ).negate() ).map( StoreFileMetadata::file ).toArray( ( x$0 ) -> {
                return new File[x$0];
            } );
        }
        catch ( Throwable var5 )
        {
            if ( stream != null )
            {
                try
                {
                    stream.close();
                }
                catch ( Throwable var4 )
                {
                    var5.addSuppressed( var4 );
                }
            }

            throw var5;
        }

        if ( stream != null )
        {
            stream.close();
        }

        return var2;
    }

    private StoreResource toStoreResource( StoreFileMetadata storeFileMetadata ) throws IOException
    {
        File databaseDirectory = this.database.getDatabaseLayout().databaseDirectory();
        File file = storeFileMetadata.file();
        String relativePath = FileUtils.relativePath( databaseDirectory, file );
        return new StoreResource( file, relativePath, storeFileMetadata.recordSize(), this.fileSystemAbstraction );
    }

    public void close()
    {
        this.closeablesListener.close();
    }
}
