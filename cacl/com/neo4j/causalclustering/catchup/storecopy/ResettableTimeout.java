package com.neo4j.causalclustering.catchup.storecopy;

import java.time.Clock;
import java.time.Duration;

class ResettableTimeout implements ResettableCondition
{
    private final long timeout;
    private final Clock clock;
    private long absoluteTimeout = -1L;

    ResettableTimeout( Duration timeout, Clock clock )
    {
        this.timeout = timeout.toMillis();
        this.clock = clock;
    }

    public boolean canContinue()
    {
        if ( this.absoluteTimeout == -1L )
        {
            this.absoluteTimeout = this.clock.millis() + this.timeout;
            return true;
        }
        else
        {
            return this.absoluteTimeout > this.clock.millis();
        }
    }

    public void reset()
    {
        this.absoluteTimeout = -1L;
    }
}
