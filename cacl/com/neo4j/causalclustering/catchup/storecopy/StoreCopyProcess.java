package com.neo4j.causalclustering.catchup.storecopy;

import com.neo4j.causalclustering.catchup.CatchupAddressProvider;
import com.neo4j.dbms.database.ClusteredDatabaseContext;

import java.io.IOException;

import org.neo4j.configuration.Config;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.storageengine.api.StoreId;

public class StoreCopyProcess
{
    private final FileSystemAbstraction fs;
    private final PageCache pageCache;
    private final ClusteredDatabaseContext clusteredDatabaseContext;
    private final Config config;
    private final CopiedStoreRecovery copiedStoreRecovery;
    private final Log log;
    private final RemoteStore remoteStore;

    public StoreCopyProcess( FileSystemAbstraction fs, PageCache pageCache, ClusteredDatabaseContext clusteredDatabaseContext,
            CopiedStoreRecovery copiedStoreRecovery, RemoteStore remoteStore, LogProvider logProvider )
    {
        this.fs = fs;
        this.pageCache = pageCache;
        this.clusteredDatabaseContext = clusteredDatabaseContext;
        this.config = clusteredDatabaseContext.database().getConfig();
        this.copiedStoreRecovery = copiedStoreRecovery;
        this.remoteStore = remoteStore;
        this.log = logProvider.getLog( this.getClass() );
    }

    public void replaceWithStoreFrom( CatchupAddressProvider addressProvider, StoreId expectedStoreId )
            throws IOException, StoreCopyFailedException, DatabaseShutdownException
    {
        TemporaryStoreDirectory tempStore = new TemporaryStoreDirectory( this.fs, this.pageCache, this.clusteredDatabaseContext.databaseLayout() );

        try
        {
            this.remoteStore.copy( addressProvider, expectedStoreId, tempStore.databaseLayout(), false );

            try
            {
                this.copiedStoreRecovery.recoverCopiedStore( this.config, tempStore.databaseLayout() );
            }
            catch ( Throwable var7 )
            {
                tempStore.keepStore();
                throw var7;
            }

            this.clusteredDatabaseContext.replaceWith( tempStore.databaseLayout().databaseDirectory() );
        }
        catch ( Throwable var8 )
        {
            try
            {
                tempStore.close();
            }
            catch ( Throwable var6 )
            {
                var8.addSuppressed( var6 );
            }

            throw var8;
        }

        tempStore.close();
        this.log.info( "Replaced store successfully" );
    }
}
