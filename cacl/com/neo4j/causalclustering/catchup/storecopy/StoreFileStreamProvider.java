package com.neo4j.causalclustering.catchup.storecopy;

import java.io.IOException;

public interface StoreFileStreamProvider
{
    StoreFileStream acquire( String var1, int var2 ) throws IOException;
}
