package com.neo4j.causalclustering.catchup.storecopy;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Set;

import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.kernel.impl.transaction.log.files.LogFiles;
import org.neo4j.storageengine.api.StorageEngineFactory;
import org.neo4j.storageengine.api.StoreId;

public class StoreFiles
{
    public static final FilenameFilter EXCLUDE_TEMPORARY_DIRS = ( dir, name ) -> {
        return !name.equals( "temp-copy" ) && !name.equals( "temp-bootstrap" ) && !name.equals( "temp-save" );
    };
    private final FileSystemAbstraction fs;
    private final PageCache pageCache;
    private final FilenameFilter filenameFilter;

    public StoreFiles( FileSystemAbstraction fs, PageCache pageCache )
    {
        this( fs, pageCache, EXCLUDE_TEMPORARY_DIRS );
    }

    public StoreFiles( FileSystemAbstraction fs, PageCache pageCache, FilenameFilter filenameFilter )
    {
        this.fs = fs;
        this.pageCache = pageCache;
        this.filenameFilter = filenameFilter;
    }

    public void delete( DatabaseLayout databaseLayout, LogFiles logFiles ) throws IOException
    {
        File databaseDirectory = databaseLayout.databaseDirectory();
        File[] files = this.fs.listFiles( databaseDirectory, this.filenameFilter );
        File[] var5;
        int var6;
        int var7;
        File txLog;
        if ( files != null )
        {
            var5 = files;
            var6 = files.length;

            for ( var7 = 0; var7 < var6; ++var7 )
            {
                txLog = var5[var7];
                this.fs.deleteRecursively( txLog );
            }
        }

        var5 = logFiles.logFiles();
        var6 = var5.length;

        for ( var7 = 0; var7 < var6; ++var7 )
        {
            txLog = var5[var7];
            this.fs.deleteFile( txLog );
        }

        this.fs.deleteFile( databaseDirectory );
    }

    public void delete( LogFiles logFiles )
    {
        File[] var2 = logFiles.logFiles();
        int var3 = var2.length;

        for ( int var4 = 0; var4 < var3; ++var4 )
        {
            File txLog = var2[var4];
            this.fs.deleteFile( txLog );
        }
    }

    public void moveTo( File source, DatabaseLayout target, LogFiles logFiles ) throws IOException
    {
        this.fs.mkdirs( logFiles.logFilesDirectory() );
        File[] files = this.fs.listFiles( source, this.filenameFilter );
        if ( files != null )
        {
            File[] var5 = files;
            int var6 = files.length;

            for ( int var7 = 0; var7 < var6; ++var7 )
            {
                File file = var5[var7];
                File destination = logFiles.isLogFile( file ) ? target.getTransactionLogsDirectory() : target.databaseDirectory();
                this.fs.moveToDirectory( file, destination );
            }
        }
    }

    public boolean isEmpty( DatabaseLayout databaseLayout )
    {
        Set<File> storeFiles = databaseLayout.storeFiles();
        File[] files = this.fs.listFiles( databaseLayout.databaseDirectory() );
        if ( files != null )
        {
            File[] var4 = files;
            int var5 = files.length;

            for ( int var6 = 0; var6 < var5; ++var6 )
            {
                File file = var4[var6];
                if ( storeFiles.contains( file ) )
                {
                    return false;
                }
            }
        }

        return true;
    }

    public StoreId readStoreId( DatabaseLayout databaseLayout ) throws IOException
    {
        return StorageEngineFactory.selectStorageEngine().storeId( databaseLayout, this.pageCache );
    }
}
