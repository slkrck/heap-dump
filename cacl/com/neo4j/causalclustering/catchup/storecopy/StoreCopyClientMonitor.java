package com.neo4j.causalclustering.catchup.storecopy;

public interface StoreCopyClientMonitor
{
    void start();

    void startReceivingStoreFiles();

    void finishReceivingStoreFiles();

    void startReceivingStoreFile( String var1 );

    void finishReceivingStoreFile( String var1 );

    void startReceivingTransactions( long var1 );

    void finishReceivingTransactions( long var1 );

    void startRecoveringStore();

    void finishRecoveringStore();

    void startReceivingIndexSnapshots();

    void startReceivingIndexSnapshot( long var1 );

    void finishReceivingIndexSnapshot( long var1 );

    void finishReceivingIndexSnapshots();

    void finish();

    public static class Adapter implements StoreCopyClientMonitor
    {
        public void start()
        {
        }

        public void startReceivingStoreFiles()
        {
        }

        public void finishReceivingStoreFiles()
        {
        }

        public void startReceivingStoreFile( String file )
        {
        }

        public void finishReceivingStoreFile( String file )
        {
        }

        public void startReceivingTransactions( long startTxId )
        {
        }

        public void finishReceivingTransactions( long endTxId )
        {
        }

        public void startRecoveringStore()
        {
        }

        public void finishRecoveringStore()
        {
        }

        public void startReceivingIndexSnapshots()
        {
        }

        public void startReceivingIndexSnapshot( long indexId )
        {
        }

        public void finishReceivingIndexSnapshot( long indexId )
        {
        }

        public void finishReceivingIndexSnapshots()
        {
        }

        public void finish()
        {
        }
    }
}
