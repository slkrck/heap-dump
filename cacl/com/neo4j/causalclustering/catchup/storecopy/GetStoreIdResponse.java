package com.neo4j.causalclustering.catchup.storecopy;

import java.util.Objects;

import org.neo4j.storageengine.api.StoreId;

public class GetStoreIdResponse
{
    private final StoreId storeId;

    public GetStoreIdResponse( StoreId storeId )
    {
        this.storeId = storeId;
    }

    public StoreId storeId()
    {
        return this.storeId;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            GetStoreIdResponse that = (GetStoreIdResponse) o;
            return Objects.equals( this.storeId, that.storeId );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.storeId} );
    }

    public String toString()
    {
        return "GetStoreIdResponse{storeId=" + this.storeId + "}";
    }
}
