package com.neo4j.causalclustering.catchup.storecopy;

public class StoreCopyFailedException extends Exception
{
    public StoreCopyFailedException( Throwable cause )
    {
        super( cause );
    }

    public StoreCopyFailedException( String message )
    {
        super( message );
    }
}
