package com.neo4j.causalclustering.catchup.storecopy;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.fs.StoreChannel;

class StoreResource
{
    private final File file;
    private final String path;
    private final int recordSize;
    private final FileSystemAbstraction fs;

    StoreResource( File file, String relativePath, int recordSize, FileSystemAbstraction fs )
    {
        this.file = file;
        this.path = relativePath;
        this.recordSize = recordSize;
        this.fs = fs;
    }

    StoreChannel open() throws IOException
    {
        return this.fs.read( this.file );
    }

    public String path()
    {
        return this.path;
    }

    File file()
    {
        return this.file;
    }

    int recordSize()
    {
        return this.recordSize;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            StoreResource that = (StoreResource) o;
            return this.recordSize == that.recordSize && Objects.equals( this.file, that.file ) && Objects.equals( this.path, that.path );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.file, this.path, this.recordSize} );
    }

    public String toString()
    {
        return "StoreResource{path='" + this.path + "', recordSize=" + this.recordSize + "}";
    }
}
