package com.neo4j.causalclustering.catchup.storecopy;

import com.neo4j.causalclustering.messaging.NetworkReadableChannel;
import com.neo4j.causalclustering.messaging.marshalling.storeid.StoreIdMarshal;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

import org.neo4j.storageengine.api.StoreId;

public class GetStoreIdResponseDecoder extends ByteToMessageDecoder
{
    protected void decode( ChannelHandlerContext ctx, ByteBuf msg, List<Object> out ) throws Exception
    {
        StoreId storeId = (StoreId) StoreIdMarshal.INSTANCE.unmarshal( new NetworkReadableChannel( msg ) );
        out.add( new GetStoreIdResponse( storeId ) );
    }
}
