package com.neo4j.causalclustering.catchup.storecopy;

import com.neo4j.causalclustering.catchup.CatchupClientProtocol;
import com.neo4j.causalclustering.catchup.CatchupResponseHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class FileHeaderHandler extends SimpleChannelInboundHandler<FileHeader>
{
    private final CatchupClientProtocol protocol;
    private final CatchupResponseHandler handler;

    public FileHeaderHandler( CatchupClientProtocol protocol, CatchupResponseHandler handler )
    {
        this.protocol = protocol;
        this.handler = handler;
    }

    protected void channelRead0( ChannelHandlerContext ctx, FileHeader fileHeader )
    {
        this.handler.onFileHeader( fileHeader );
        this.protocol.expect( CatchupClientProtocol.State.FILE_CHUNK );
    }
}
