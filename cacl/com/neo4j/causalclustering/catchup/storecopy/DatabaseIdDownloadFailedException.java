package com.neo4j.causalclustering.catchup.storecopy;

public class DatabaseIdDownloadFailedException extends Exception
{
    public DatabaseIdDownloadFailedException( Exception e )
    {
        super( e );
    }

    public DatabaseIdDownloadFailedException( String description )
    {
        super( description );
    }
}
