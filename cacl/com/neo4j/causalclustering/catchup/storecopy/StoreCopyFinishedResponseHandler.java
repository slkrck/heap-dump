package com.neo4j.causalclustering.catchup.storecopy;

import com.neo4j.causalclustering.catchup.CatchupClientProtocol;
import com.neo4j.causalclustering.catchup.CatchupResponseHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class StoreCopyFinishedResponseHandler extends SimpleChannelInboundHandler<StoreCopyFinishedResponse>
{
    private final CatchupClientProtocol protocol;
    private CatchupResponseHandler handler;

    public StoreCopyFinishedResponseHandler( CatchupClientProtocol protocol, CatchupResponseHandler handler )
    {
        this.protocol = protocol;
        this.handler = handler;
    }

    protected void channelRead0( ChannelHandlerContext ctx, StoreCopyFinishedResponse msg )
    {
        this.handler.onFileStreamingComplete( msg );
        this.protocol.expect( CatchupClientProtocol.State.MESSAGE_TYPE );
    }
}
