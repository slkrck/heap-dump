package com.neo4j.causalclustering.catchup.storecopy;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import org.neo4j.string.UTF8;

public class FileHeaderEncoder extends MessageToByteEncoder<FileHeader>
{
    protected void encode( ChannelHandlerContext ctx, FileHeader msg, ByteBuf out )
    {
        String name = msg.fileName();
        byte[] bytes = UTF8.encode( name );
        out.writeInt( bytes.length );
        out.writeBytes( bytes );
        out.writeInt( msg.requiredAlignment() );
    }
}
