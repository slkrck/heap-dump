package com.neo4j.causalclustering.catchup.storecopy;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;

import java.util.Objects;

public class FileChunk
{
    static final int HEADER_SIZE = 4;
    private static final int HEADER_IS_LAST_FALSE = 0;
    private static final int HEADER_IS_LAST_TRUE = 1;
    private final boolean isLast;
    private final ByteBuf payload;

    FileChunk( boolean isLast, ByteBuf payload )
    {
        this.isLast = isLast;
        this.payload = payload;
    }

    static FileChunk create( ByteBuf payload, boolean isLast, int chunkSize )
    {
        if ( !isLast && payload.readableBytes() != chunkSize )
        {
            throw new IllegalArgumentException( "All chunks except for the last must be of max size." );
        }
        else
        {
            return new FileChunk( isLast, payload );
        }
    }

    static int makeHeader( boolean isLast )
    {
        return isLast ? 1 : 0;
    }

    static boolean parseHeader( int header )
    {
        if ( header == 1 )
        {
            return true;
        }
        else if ( header == 0 )
        {
            return false;
        }
        else
        {
            throw new IllegalStateException( "Illegal header value: " + header );
        }
    }

    public boolean isLast()
    {
        return this.isLast;
    }

    ByteBuf payload()
    {
        return this.payload;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            FileChunk fileChunk = (FileChunk) o;
            return this.isLast == fileChunk.isLast && ByteBufUtil.equals( this.payload, fileChunk.payload );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.isLast, this.payload} );
    }

    public String toString()
    {
        return "FileChunk{isLast=" + this.isLast + ", payload=" + this.payload + "}";
    }
}
