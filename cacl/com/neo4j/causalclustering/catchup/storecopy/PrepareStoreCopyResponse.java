package com.neo4j.causalclustering.catchup.storecopy;

import com.neo4j.causalclustering.core.state.storage.SafeChannelMarshal;
import com.neo4j.causalclustering.messaging.BoundedNetworkWritableChannel;
import com.neo4j.causalclustering.messaging.NetworkReadableChannel;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import io.netty.handler.codec.MessageToByteEncoder;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;
import org.neo4j.string.UTF8;

public class PrepareStoreCopyResponse
{
    private final File[] files;
    private final long lastCheckPointedTransactionId;
    private final PrepareStoreCopyResponse.Status status;

    private PrepareStoreCopyResponse( File[] files, long lastCheckPointedTransactionId, PrepareStoreCopyResponse.Status status )
    {
        this.files = files;
        this.lastCheckPointedTransactionId = lastCheckPointedTransactionId;
        this.status = status;
    }

    public static PrepareStoreCopyResponse error( PrepareStoreCopyResponse.Status errorStatus )
    {
        if ( errorStatus == PrepareStoreCopyResponse.Status.SUCCESS )
        {
            throw new IllegalStateException( "Cannot create error result from state: " + errorStatus );
        }
        else
        {
            return new PrepareStoreCopyResponse( new File[0], 0L, errorStatus );
        }
    }

    public static PrepareStoreCopyResponse success( File[] storeFiles, long lastCheckPointedTransactionId )
    {
        return new PrepareStoreCopyResponse( storeFiles, lastCheckPointedTransactionId, PrepareStoreCopyResponse.Status.SUCCESS );
    }

    public File[] getFiles()
    {
        return this.files;
    }

    public long lastCheckPointedTransactionId()
    {
        return this.lastCheckPointedTransactionId;
    }

    public PrepareStoreCopyResponse.Status status()
    {
        return this.status;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            PrepareStoreCopyResponse that = (PrepareStoreCopyResponse) o;
            return this.lastCheckPointedTransactionId == that.lastCheckPointedTransactionId && Arrays.equals( this.files, that.files ) &&
                    this.status == that.status;
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        int result = Objects.hash( new Object[]{this.lastCheckPointedTransactionId, this.status} );
        result = 31 * result + Arrays.hashCode( this.files );
        return result;
    }

    public static enum Status
    {
        SUCCESS,
        E_STORE_ID_MISMATCH,
        E_LISTING_STORE,
        E_DATABASE_UNKNOWN;
    }

    public static class Decoder extends ByteToMessageDecoder
    {
        protected void decode( ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list ) throws Exception
        {
            list.add( (new PrepareStoreCopyResponse.StoreListingMarshal()).unmarshal( new NetworkReadableChannel( byteBuf ) ) );
        }
    }

    public static class Encoder extends MessageToByteEncoder<PrepareStoreCopyResponse>
    {
        protected void encode( ChannelHandlerContext channelHandlerContext, PrepareStoreCopyResponse prepareStoreCopyResponse, ByteBuf byteBuf )
                throws Exception
        {
            (new PrepareStoreCopyResponse.StoreListingMarshal()).marshal( (PrepareStoreCopyResponse) prepareStoreCopyResponse,
                    new BoundedNetworkWritableChannel( byteBuf ) );
        }
    }

    public static class StoreListingMarshal extends SafeChannelMarshal<PrepareStoreCopyResponse>
    {
        private static void marshalFiles( WritableChannel buffer, File[] files ) throws IOException
        {
            buffer.putInt( files.length );
            File[] var2 = files;
            int var3 = files.length;

            for ( int var4 = 0; var4 < var3; ++var4 )
            {
                File file = var2[var4];
                putBytes( buffer, file.getName() );
            }
        }

        private static File[] unmarshalFiles( ReadableChannel channel ) throws IOException
        {
            int numberOfFiles = channel.getInt();
            File[] files = new File[numberOfFiles];

            for ( int i = 0; i < numberOfFiles; ++i )
            {
                files[i] = unmarshalFile( channel );
            }

            return files;
        }

        private static File unmarshalFile( ReadableChannel channel ) throws IOException
        {
            byte[] name = readBytes( channel );
            return new File( UTF8.decode( name ) );
        }

        private static void putBytes( WritableChannel buffer, String value ) throws IOException
        {
            byte[] bytes = UTF8.encode( value );
            buffer.putInt( bytes.length );
            buffer.put( bytes, bytes.length );
        }

        private static byte[] readBytes( ReadableChannel channel ) throws IOException
        {
            int bytesLength = channel.getInt();
            byte[] bytes = new byte[bytesLength];
            channel.get( bytes, bytesLength );
            return bytes;
        }

        public void marshal( PrepareStoreCopyResponse prepareStoreCopyResponse, WritableChannel buffer ) throws IOException
        {
            buffer.putInt( prepareStoreCopyResponse.status.ordinal() );
            buffer.putLong( prepareStoreCopyResponse.lastCheckPointedTransactionId );
            marshalFiles( buffer, prepareStoreCopyResponse.files );
        }

        protected PrepareStoreCopyResponse unmarshal0( ReadableChannel channel ) throws IOException
        {
            int ordinal = channel.getInt();
            PrepareStoreCopyResponse.Status status = PrepareStoreCopyResponse.Status.values()[ordinal];
            long transactionId = channel.getLong();
            File[] files = unmarshalFiles( channel );
            return new PrepareStoreCopyResponse( files, transactionId, status );
        }
    }
}
