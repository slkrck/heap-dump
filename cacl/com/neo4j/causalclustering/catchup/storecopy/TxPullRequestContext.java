package com.neo4j.causalclustering.catchup.storecopy;

import java.util.OptionalLong;

import org.neo4j.storageengine.api.StoreId;

class TxPullRequestContext
{
    private final RequiredTransactions requiredTransactions;
    private final StoreId expectedStoreId;
    private final boolean hasFallbackStartId;

    private TxPullRequestContext( RequiredTransactions requiredTransactions, StoreId expectedStoreId, boolean hasFallbackStartId )
    {
        this.requiredTransactions = requiredTransactions;
        this.expectedStoreId = expectedStoreId;
        this.hasFallbackStartId = hasFallbackStartId;
    }

    private TxPullRequestContext( RequiredTransactions requiredTransactions, StoreId expectedStoreId )
    {
        this( requiredTransactions, expectedStoreId, false );
    }

    static TxPullRequestContext createContextFromStoreCopy( RequiredTransactions requiredTransactions, StoreId expectedStoreId )
    {
        return new TxPullRequestContext( requiredTransactions, expectedStoreId );
    }

    static TxPullRequestContext createContextFromCatchingUp( StoreId expectedStoreId, CommitState commitState )
    {
        if ( commitState.transactionLogIndex().isPresent() )
        {
            return new TxPullRequestContext( RequiredTransactions.noConstraint( (Long) commitState.transactionLogIndex().get() + 1L ), expectedStoreId );
        }
        else
        {
            long metaDataStoreIndex = commitState.metaDataStoreIndex();
            return metaDataStoreIndex == 1L ? new TxPullRequestContext( RequiredTransactions.noConstraint( metaDataStoreIndex + 1L ), expectedStoreId )
                                            : new TxPullRequestContext( RequiredTransactions.noConstraint( metaDataStoreIndex ), expectedStoreId, true );
        }
    }

    OptionalLong fallbackStartId()
    {
        return this.hasFallbackStartId ? OptionalLong.of( this.startTxIdExclusive() + 1L ) : OptionalLong.empty();
    }

    long startTxIdExclusive()
    {
        return this.requiredTransactions.startTxId() - 1L;
    }

    StoreId expectedStoreId()
    {
        return this.expectedStoreId;
    }

    boolean constraintReached( long lastWrittenTx )
    {
        return this.requiredTransactions.noRequiredTxId() || this.requiredTransactions.requiredTxId() <= lastWrittenTx;
    }
}
