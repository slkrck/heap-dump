package com.neo4j.causalclustering.catchup.storecopy;

import java.util.Objects;

public class FileHeader
{
    private final String fileName;
    private final int requiredAlignment;

    public FileHeader( String fileName )
    {
        this( fileName, 1 );
    }

    public FileHeader( String fileName, int requiredAlignment )
    {
        this.fileName = fileName;
        this.requiredAlignment = requiredAlignment;
    }

    public String fileName()
    {
        return this.fileName;
    }

    public int requiredAlignment()
    {
        return this.requiredAlignment;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            FileHeader that = (FileHeader) o;
            return this.requiredAlignment == that.requiredAlignment && Objects.equals( this.fileName, that.fileName );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.fileName, this.requiredAlignment} );
    }

    public String toString()
    {
        return "FileHeader{fileName='" + this.fileName + "', requiredAlignment=" + this.requiredAlignment + "}";
    }
}
