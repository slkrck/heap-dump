package com.neo4j.causalclustering.catchup.storecopy;

import com.neo4j.causalclustering.catchup.CatchupAddressProvider;
import com.neo4j.causalclustering.catchup.CatchupAddressResolutionException;
import com.neo4j.causalclustering.catchup.CatchupClientFactory;
import com.neo4j.causalclustering.catchup.CatchupResponseAdaptor;
import com.neo4j.causalclustering.catchup.VersionedCatchupClients;

import java.io.File;
import java.net.ConnectException;
import java.nio.file.Paths;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.function.Supplier;

import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.internal.helpers.TimeoutStrategy;
import org.neo4j.internal.helpers.TimeoutStrategy.Timeout;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.monitoring.Monitors;
import org.neo4j.storageengine.api.StoreId;

public class StoreCopyClient
{
    private final CatchupClientFactory catchUpClientFactory;
    private final Supplier<Monitors> monitors;
    private final NamedDatabaseId namedDatabaseId;
    private final Log log;
    private final TimeoutStrategy backOffStrategy;

    public StoreCopyClient( CatchupClientFactory catchUpClientFactory, NamedDatabaseId namedDatabaseId, Supplier<Monitors> monitors, LogProvider logProvider,
            TimeoutStrategy backOffStrategy )
    {
        this.catchUpClientFactory = catchUpClientFactory;
        this.monitors = monitors;
        this.namedDatabaseId = namedDatabaseId;
        this.backOffStrategy = backOffStrategy;
        this.log = logProvider.getLog( this.getClass() );
    }

    RequiredTransactions copyStoreFiles( CatchupAddressProvider catchupAddressProvider, StoreId expectedStoreId,
            StoreFileStreamProvider storeFileStreamProvider, Supplier<TerminationCondition> requestWiseTerminationCondition, File destDir )
            throws StoreCopyFailedException
    {
        try
        {
            SocketAddress fromAddress = catchupAddressProvider.primary( this.namedDatabaseId );
            PrepareStoreCopyResponse prepareStoreCopyResponse = this.prepareStoreCopy( fromAddress, expectedStoreId, storeFileStreamProvider );
            StoreCopyClient.TransactionIdHandler txIdHandler = new StoreCopyClient.TransactionIdHandler( prepareStoreCopyResponse );
            this.copyFilesIndividually( prepareStoreCopyResponse, expectedStoreId, catchupAddressProvider, storeFileStreamProvider,
                    requestWiseTerminationCondition, destDir, txIdHandler );
            return txIdHandler.requiredTransactionRange();
        }
        catch ( StoreCopyFailedException var9 )
        {
            throw var9;
        }
        catch ( Exception var10 )
        {
            throw new StoreCopyFailedException( var10 );
        }
    }

    private void copyFilesIndividually( PrepareStoreCopyResponse prepareStoreCopyResponse, StoreId expectedStoreId, CatchupAddressProvider addressProvider,
            StoreFileStreamProvider storeFileStream, Supplier<TerminationCondition> terminationConditions, File destDir,
            StoreCopyClient.TransactionIdHandler txIdHandler ) throws StoreCopyFailedException
    {
        StoreCopyClientMonitor storeCopyClientMonitor =
                (StoreCopyClientMonitor) ((Monitors) this.monitors.get()).newMonitor( StoreCopyClientMonitor.class, new String[0] );
        storeCopyClientMonitor.startReceivingStoreFiles();
        long lastCheckPointedTxId = prepareStoreCopyResponse.lastCheckPointedTransactionId();
        File[] var11 = prepareStoreCopyResponse.getFiles();
        int var12 = var11.length;

        for ( int var13 = 0; var13 < var12; ++var13 )
        {
            File file = var11[var13];
            storeCopyClientMonitor.startReceivingStoreFile( Paths.get( destDir.toString(), file.getName() ).toString() );
            this.persistentCallToSecondary( addressProvider, ( c ) -> {
                return c.getStoreFile( expectedStoreId, file, lastCheckPointedTxId, this.namedDatabaseId );
            }, storeFileStream, (TerminationCondition) terminationConditions.get(), txIdHandler );
            storeCopyClientMonitor.finishReceivingStoreFile( Paths.get( destDir.toString(), file.getName() ).toString() );
        }

        storeCopyClientMonitor.finishReceivingStoreFiles();
    }

    private void persistentCallToSecondary( CatchupAddressProvider addressProvider,
            Function<VersionedCatchupClients.CatchupClientV3,VersionedCatchupClients.PreparedRequest<StoreCopyFinishedResponse>> v3Request,
            StoreFileStreamProvider storeFileStream, TerminationCondition terminationCondition, StoreCopyClient.TransactionIdHandler txIdHandler )
            throws StoreCopyFailedException
    {
        Timeout timeout = this.backOffStrategy.newTimeout();

        while ( true )
        {
            try
            {
                SocketAddress address = addressProvider.secondary( this.namedDatabaseId );
                this.log.info( String.format( "Sending request StoreCopyRequest to '%s'", address ) );
                StoreCopyFinishedResponse response =
                        (StoreCopyFinishedResponse) this.catchUpClientFactory.getClient( address, this.log ).v3( v3Request ).withResponseHandler(
                                StoreCopyResponseAdaptors.filesCopyAdaptor( storeFileStream, this.log ) ).request();
                if ( this.successfulRequest( response ) )
                {
                    txIdHandler.handle( response );
                    return;
                }
            }
            catch ( CatchupAddressResolutionException var9 )
            {
                this.log.warn( "Unable to resolve address for StoreCopyRequest. %s", new Object[]{var9.getMessage()} );
            }
            catch ( ConnectException var10 )
            {
                this.log.warn( "Unable to connect. %s", new Object[]{var10.getMessage()} );
            }
            catch ( Exception var11 )
            {
                this.log.warn( "StoreCopyRequest failed exceptionally.", var11 );
            }

            terminationCondition.assertContinue();
            this.awaitAndIncrementTimeout( timeout );
        }
    }

    private void awaitAndIncrementTimeout( Timeout timeout ) throws StoreCopyFailedException
    {
        try
        {
            Thread.sleep( timeout.getMillis() );
            timeout.increment();
        }
        catch ( InterruptedException var3 )
        {
            throw new StoreCopyFailedException( "Thread interrupted" );
        }
    }

    private PrepareStoreCopyResponse prepareStoreCopy( SocketAddress from, StoreId expectedStoreId, StoreFileStreamProvider storeFileStream )
            throws StoreCopyFailedException
    {
        PrepareStoreCopyResponse prepareStoreCopyResponse;
        try
        {
            this.log.info( "Requesting store listing from: " + from );
            prepareStoreCopyResponse = (PrepareStoreCopyResponse) this.catchUpClientFactory.getClient( from, this.log ).v3( ( c ) -> {
                return c.prepareStoreCopy( expectedStoreId, this.namedDatabaseId );
            } ).withResponseHandler( StoreCopyResponseAdaptors.prepareStoreCopyAdaptor( storeFileStream, this.log ) ).request();
        }
        catch ( Exception var6 )
        {
            throw new StoreCopyFailedException( var6 );
        }

        if ( prepareStoreCopyResponse.status() != PrepareStoreCopyResponse.Status.SUCCESS )
        {
            throw new StoreCopyFailedException( "Preparing store failed due to: " + prepareStoreCopyResponse.status() );
        }
        else
        {
            return prepareStoreCopyResponse;
        }
    }

    public StoreId fetchStoreId( SocketAddress fromAddress ) throws StoreIdDownloadFailedException
    {
        try
        {
            CatchupResponseAdaptor<StoreId> responseHandler = new CatchupResponseAdaptor<StoreId>()
            {
                public void onGetStoreIdResponse( CompletableFuture<StoreId> signal, GetStoreIdResponse response )
                {
                    signal.complete( response.storeId() );
                }
            };
            return (StoreId) this.catchUpClientFactory.getClient( fromAddress, this.log ).v3( ( c ) -> {
                return c.getStoreId( this.namedDatabaseId );
            } ).withResponseHandler( responseHandler ).request();
        }
        catch ( Exception var3 )
        {
            throw new StoreIdDownloadFailedException( var3 );
        }
    }

    private boolean successfulRequest( StoreCopyFinishedResponse response ) throws StoreCopyFailedException
    {
        switch ( response.status() )
        {
        case SUCCESS:
            this.log.info( "StoreCopyRequest was successful." );
            return true;
        case E_TOO_FAR_BEHIND:
        case E_UNKNOWN:
        case E_STORE_ID_MISMATCH:
        case E_DATABASE_UNKNOWN:
            this.log.warn( String.format( "StoreCopyRequest failed with response: %s", response.status() ) );
            return false;
        default:
            throw new StoreCopyFailedException( String.format( "Request responded with an unknown response type: %s.", response.status() ) );
        }
    }

    private static class TransactionIdHandler
    {
        private static final long MIN_COMMITTED_TRANSACTION_ID = 2L;
        private final long initialTxId;
        private long highestReceivedTxId = -1L;

        TransactionIdHandler( PrepareStoreCopyResponse prepareStoreCopyResponse )
        {
            this.initialTxId = prepareStoreCopyResponse.lastCheckPointedTransactionId();
        }

        void handle( StoreCopyFinishedResponse response )
        {
            if ( response.status() == StoreCopyFinishedResponse.Status.SUCCESS )
            {
                this.highestReceivedTxId = Long.max( this.highestReceivedTxId, response.lastCheckpointedTx() );
            }
        }

        RequiredTransactions requiredTransactionRange()
        {
            return this.highestReceivedTxId < 2L ? RequiredTransactions.noConstraint( Long.max( this.initialTxId, 2L ) )
                                                 : RequiredTransactions.requiredRange( this.initialTxId, this.highestReceivedTxId );
        }
    }
}
