package com.neo4j.causalclustering.catchup.storecopy;

import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;

import java.util.ArrayList;
import java.util.List;

import org.neo4j.io.IOUtils;

class CloseablesListener implements AutoCloseable, GenericFutureListener<Future<Void>>
{
    private final List<AutoCloseable> closeables = new ArrayList();

    <T extends AutoCloseable> T add( T closeable )
    {
        if ( closeable == null )
        {
            throw new IllegalArgumentException( "closeable cannot be null!" );
        }
        else
        {
            this.closeables.add( closeable );
            return closeable;
        }
    }

    public void close()
    {
        IOUtils.close( RuntimeException::new, this.closeables );
    }

    public void operationComplete( Future<Void> future )
    {
        this.close();
    }
}
