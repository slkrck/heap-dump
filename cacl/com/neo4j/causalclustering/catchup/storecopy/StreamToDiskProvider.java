package com.neo4j.causalclustering.catchup.storecopy;

import com.neo4j.causalclustering.catchup.tx.FileCopyMonitor;

import java.io.File;
import java.io.IOException;

import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.monitoring.Monitors;

public class StreamToDiskProvider implements StoreFileStreamProvider
{
    private final File storeDir;
    private final FileSystemAbstraction fs;
    private final FileCopyMonitor fileCopyMonitor;

    public StreamToDiskProvider( File storeDir, FileSystemAbstraction fs, Monitors monitors )
    {
        this.storeDir = storeDir;
        this.fs = fs;
        this.fileCopyMonitor = (FileCopyMonitor) monitors.newMonitor( FileCopyMonitor.class, new String[0] );
    }

    public StoreFileStream acquire( String destination, int requiredAlignment ) throws IOException
    {
        File fileName = new File( this.storeDir, destination );
        this.fs.mkdirs( fileName.getParentFile() );
        this.fileCopyMonitor.copyFile( fileName );
        return StreamToDisk.fromFile( this.fs, fileName );
    }
}
