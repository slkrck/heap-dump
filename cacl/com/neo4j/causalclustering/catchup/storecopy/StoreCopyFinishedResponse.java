package com.neo4j.causalclustering.catchup.storecopy;

import java.util.Objects;

public class StoreCopyFinishedResponse
{
    public static final long LAST_CHECKPOINTED_TX_UNAVAILABLE = -1L;
    private final StoreCopyFinishedResponse.Status status;
    private final long lastCheckpointedTx;

    public StoreCopyFinishedResponse( StoreCopyFinishedResponse.Status status, long lastCheckpointedTx )
    {
        this.status = status;
        this.lastCheckpointedTx = status == StoreCopyFinishedResponse.Status.SUCCESS ? lastCheckpointedTx : -1L;
    }

    public StoreCopyFinishedResponse.Status status()
    {
        return this.status;
    }

    public long lastCheckpointedTx()
    {
        return this.lastCheckpointedTx;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            StoreCopyFinishedResponse that = (StoreCopyFinishedResponse) o;
            return this.lastCheckpointedTx == that.lastCheckpointedTx && this.status == that.status;
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.status, this.lastCheckpointedTx} );
    }

    public static enum Status
    {
        SUCCESS,
        E_STORE_ID_MISMATCH,
        E_TOO_FAR_BEHIND,
        E_UNKNOWN,
        E_DATABASE_UNKNOWN;
    }
}
