package com.neo4j.causalclustering.catchup.storecopy;

import com.neo4j.causalclustering.catchup.ResponseMessageType;
import io.netty.channel.ChannelHandlerContext;
import io.netty.util.concurrent.Future;

public class StoreFileStreamingProtocol
{
    private final int maxChunkSize;

    public StoreFileStreamingProtocol( int maxChunkSize )
    {
        this.maxChunkSize = maxChunkSize;
    }

    void stream( ChannelHandlerContext ctx, StoreResource resource )
    {
        ctx.write( ResponseMessageType.FILE );
        ctx.write( new FileHeader( resource.path(), resource.recordSize() ) );
        ctx.write( new FileSender( resource, this.maxChunkSize ) );
    }

    Future<Void> end( ChannelHandlerContext ctx, StoreCopyFinishedResponse.Status status, long lastCheckpointedTx )
    {
        ctx.write( ResponseMessageType.STORE_COPY_FINISHED );
        return ctx.writeAndFlush( new StoreCopyFinishedResponse( status, lastCheckpointedTx ) );
    }
}
