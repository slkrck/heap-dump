package com.neo4j.causalclustering.catchup.storecopy;

import com.neo4j.causalclustering.catchup.CatchupServerProtocol;
import com.neo4j.causalclustering.catchup.v3.storecopy.GetStoreFileRequest;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.internal.helpers.collection.Iterators;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.fs.FileUtils;
import org.neo4j.kernel.database.Database;
import org.neo4j.kernel.impl.transaction.log.checkpoint.CheckPointer;
import org.neo4j.kernel.impl.transaction.log.checkpoint.SimpleTriggerInfo;
import org.neo4j.logging.Log;
import org.neo4j.scheduler.Group;
import org.neo4j.storageengine.api.StoreFileMetadata;
import org.neo4j.util.VisibleForTesting;

public class GetStoreFileRequestHandler extends SimpleChannelInboundHandler<GetStoreFileRequest>
{
    private final CatchupServerProtocol protocol;
    private final Database db;
    private final StoreFileStreamingProtocol storeFileStreamingProtocol;
    private final FileSystemAbstraction fs;
    private final Log log;

    public GetStoreFileRequestHandler( CatchupServerProtocol protocol, Database db, StoreFileStreamingProtocol storeFileStreamingProtocol,
            FileSystemAbstraction fs )
    {
        this.protocol = protocol;
        this.db = db;
        this.storeFileStreamingProtocol = storeFileStreamingProtocol;
        this.fs = fs;
        this.log = db.getInternalLogProvider().getLog( this.getClass() );
    }

    private static Iterator<StoreFileMetadata> onlyOne( List<StoreFileMetadata> files, String description )
    {
        if ( files.size() != 1 )
        {
            throw new IllegalStateException( String.format( "Expected exactly one file '%s'. Got %d", description, files.size() ) );
        }
        else
        {
            return files.iterator();
        }
    }

    private static Predicate<StoreFileMetadata> matchesRequested( String fileName )
    {
        return ( f ) -> {
            return f.file().getName().equals( fileName );
        };
    }

    protected final void channelRead0( ChannelHandlerContext ctx, GetStoreFileRequest request ) throws Exception
    {
        this.log.debug( "Handling request %s", new Object[]{request} );
        StoreCopyFinishedResponse.Status responseStatus = StoreCopyFinishedResponse.Status.E_UNKNOWN;
        long lastCheckpointedTx = -1L;

        try
        {
            CheckPointer checkPointer = (CheckPointer) this.db.getDependencyResolver().resolveDependency( CheckPointer.class );
            if ( !Objects.equals( request.expectedStoreId(), this.db.getStoreId() ) )
            {
                responseStatus = StoreCopyFinishedResponse.Status.E_STORE_ID_MISMATCH;
            }
            else if ( checkPointer.lastCheckPointedTransactionId() < request.requiredTransactionId() )
            {
                responseStatus = StoreCopyFinishedResponse.Status.E_TOO_FAR_BEHIND;
                this.tryAsyncCheckpoint( this.db, checkPointer );
            }
            else
            {
                File databaseDirectory = this.db.getDatabaseLayout().databaseDirectory();
                ResourceIterator resourceIterator = this.files( request, this.db );

                try
                {
                    while ( resourceIterator.hasNext() )
                    {
                        StoreFileMetadata storeFileMetadata = (StoreFileMetadata) resourceIterator.next();
                        StoreResource storeResource =
                                new StoreResource( storeFileMetadata.file(), FileUtils.relativePath( databaseDirectory, storeFileMetadata.file() ),
                                        storeFileMetadata.recordSize(), this.fs );
                        this.storeFileStreamingProtocol.stream( ctx, storeResource );
                    }
                }
                catch ( Throwable var16 )
                {
                    if ( resourceIterator != null )
                    {
                        try
                        {
                            resourceIterator.close();
                        }
                        catch ( Throwable var15 )
                        {
                            var16.addSuppressed( var15 );
                        }
                    }

                    throw var16;
                }

                if ( resourceIterator != null )
                {
                    resourceIterator.close();
                }

                lastCheckpointedTx = checkPointer.lastCheckPointedTransactionId();
                responseStatus = StoreCopyFinishedResponse.Status.SUCCESS;
            }
        }
        finally
        {
            this.storeFileStreamingProtocol.end( ctx, responseStatus, lastCheckpointedTx );
            this.protocol.expect( CatchupServerProtocol.State.MESSAGE_TYPE );
        }
    }

    @VisibleForTesting
    ResourceIterator<StoreFileMetadata> files( GetStoreFileRequest request, Database database ) throws IOException
    {
        ResourceIterator resourceIterator = database.listStoreFiles( false );

        ResourceIterator var5;
        try
        {
            String fileName = request.file().getName();
            var5 = Iterators.asResourceIterator(
                    onlyOne( (List) resourceIterator.stream().filter( matchesRequested( fileName ) ).collect( Collectors.toList() ), fileName ) );
        }
        catch ( Throwable var7 )
        {
            if ( resourceIterator != null )
            {
                try
                {
                    resourceIterator.close();
                }
                catch ( Throwable var6 )
                {
                    var7.addSuppressed( var6 );
                }
            }

            throw var7;
        }

        if ( resourceIterator != null )
        {
            resourceIterator.close();
        }

        return var5;
    }

    private void tryAsyncCheckpoint( Database db, CheckPointer checkPointer )
    {
        db.getScheduler().schedule( Group.CHECKPOINT, () -> {
            try
            {
                checkPointer.tryCheckPointNoWait( new SimpleTriggerInfo( "Store file copy" ) );
            }
            catch ( IOException var3 )
            {
                this.log.error( "Failed to do a checkpoint that was invoked after a too far behind error on store copy request", var3 );
            }
        } );
    }
}
