package com.neo4j.causalclustering.catchup.storecopy;

import com.neo4j.causalclustering.catchup.CatchupClientProtocol;
import com.neo4j.causalclustering.catchup.CatchupResponseHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class GetStoreIdResponseHandler extends SimpleChannelInboundHandler<GetStoreIdResponse>
{
    private final CatchupResponseHandler handler;
    private final CatchupClientProtocol protocol;

    public GetStoreIdResponseHandler( CatchupClientProtocol protocol, CatchupResponseHandler handler )
    {
        this.protocol = protocol;
        this.handler = handler;
    }

    protected void channelRead0( ChannelHandlerContext ctx, GetStoreIdResponse msg )
    {
        this.handler.onGetStoreIdResponse( msg );
        this.protocol.expect( CatchupClientProtocol.State.MESSAGE_TYPE );
    }
}
