package com.neo4j.causalclustering.catchup.storecopy;

import io.netty.buffer.ByteBuf;

import java.io.IOException;

public interface StoreFileStream extends AutoCloseable
{
    void write( ByteBuf var1 ) throws IOException;
}
