package com.neo4j.causalclustering.catchup.storecopy;

import io.netty.buffer.ByteBuf;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.neo4j.io.IOUtils;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.fs.StoreChannel;

public class StreamToDisk implements StoreFileStream
{
    private StoreChannel storeChannel;
    private List<AutoCloseable> closeables;

    private StreamToDisk( StoreChannel storeChannel, AutoCloseable... closeables )
    {
        this.storeChannel = storeChannel;
        this.closeables = new ArrayList();
        this.closeables.add( storeChannel );
        this.closeables.addAll( Arrays.asList( closeables ) );
    }

    static StreamToDisk fromFile( FileSystemAbstraction fsa, File file ) throws IOException
    {
        return new StreamToDisk( fsa.write( file ), new AutoCloseable[0] );
    }

    public void write( ByteBuf data ) throws IOException
    {
        int expectedTotal = data.readableBytes();

        int bytesWrittenOrEOF;
        for ( int totalWritten = 0; totalWritten < expectedTotal; totalWritten += bytesWrittenOrEOF )
        {
            bytesWrittenOrEOF = data.readBytes( this.storeChannel, expectedTotal );
            if ( bytesWrittenOrEOF < 0 )
            {
                throw new IOException( "Unexpected failure writing to channel: " + bytesWrittenOrEOF );
            }
        }
    }

    public void close() throws IOException
    {
        IOUtils.closeAll( this.closeables );
    }
}
