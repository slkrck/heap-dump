package com.neo4j.causalclustering.catchup.storecopy;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.CompositeByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;

import java.util.List;

public class FileChunkEncoder extends MessageToMessageEncoder<FileChunk>
{
    protected void encode( ChannelHandlerContext ctx, FileChunk chunk, List<Object> out )
    {
        ByteBuf header = ctx.alloc().ioBuffer( 4 );
        header.writeInt( FileChunk.makeHeader( chunk.isLast() ) );
        ByteBuf payload = chunk.payload();
        CompositeByteBuf frame = ctx.alloc().compositeBuffer( 2 );
        frame.addComponent( true, header );
        frame.addComponent( true, payload );
        out.add( frame );
    }
}
