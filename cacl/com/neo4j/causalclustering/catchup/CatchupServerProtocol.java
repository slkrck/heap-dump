package com.neo4j.causalclustering.catchup;

public class CatchupServerProtocol extends Protocol<CatchupServerProtocol.State>
{
    public CatchupServerProtocol()
    {
        super( CatchupServerProtocol.State.MESSAGE_TYPE );
    }

    public static enum State
    {
        MESSAGE_TYPE,
        GET_STORE,
        GET_STORE_ID,
        GET_CORE_SNAPSHOT,
        TX_PULL,
        GET_STORE_FILE,
        PREPARE_STORE_COPY,
        GET_DATABASE_ID;
    }
}
