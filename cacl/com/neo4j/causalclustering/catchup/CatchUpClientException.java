package com.neo4j.causalclustering.catchup;

public class CatchUpClientException extends Exception
{
    public CatchUpClientException()
    {
    }

    CatchUpClientException( String message )
    {
        super( message );
    }

    CatchUpClientException( String operation, Throwable cause )
    {
        super( operation, cause );
    }
}
