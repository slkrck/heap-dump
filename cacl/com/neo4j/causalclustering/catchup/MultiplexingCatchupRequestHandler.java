package com.neo4j.causalclustering.catchup;

import com.neo4j.causalclustering.catchup.error.UnavailableDatabaseHandler;
import com.neo4j.causalclustering.catchup.error.UnknownDatabaseHandler;
import com.neo4j.causalclustering.messaging.CatchupProtocolMessage;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.function.Function;

import org.neo4j.dbms.database.DatabaseContext;
import org.neo4j.dbms.database.DatabaseManager;
import org.neo4j.kernel.availability.DatabaseAvailabilityGuard;
import org.neo4j.kernel.database.Database;
import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.logging.LogProvider;

class MultiplexingCatchupRequestHandler<T extends CatchupProtocolMessage.WithDatabaseId> extends SimpleChannelInboundHandler<T>
{
    private final Class<T> messageType;
    private final DatabaseManager<?> databaseManager;
    private final Function<Database,SimpleChannelInboundHandler<T>> handlerFactory;
    private final CatchupServerProtocol protocol;
    private final LogProvider logProvider;

    MultiplexingCatchupRequestHandler( CatchupServerProtocol protocol, DatabaseManager<?> databaseManager,
            Function<Database,SimpleChannelInboundHandler<T>> handlerFactory, Class<T> messageType, LogProvider logProvider )
    {
        super( messageType );
        this.messageType = messageType;
        this.databaseManager = databaseManager;
        this.handlerFactory = handlerFactory;
        this.protocol = protocol;
        this.logProvider = logProvider;
    }

    protected void channelRead0( ChannelHandlerContext ctx, T request ) throws Exception
    {
        DatabaseId databaseId = request.databaseId();
        DatabaseContext databaseContext = (DatabaseContext) this.databaseManager.getDatabaseContext( databaseId ).orElse( (Object) null );
        SimpleChannelInboundHandler<T> handler = this.createHandler( databaseContext );
        handler.channelRead( ctx, request );
    }

    private SimpleChannelInboundHandler<T> createHandler( DatabaseContext databaseContext )
    {
        if ( databaseContext == null )
        {
            return new UnknownDatabaseHandler( this.messageType, this.protocol, this.logProvider );
        }
        else
        {
            DatabaseAvailabilityGuard availabilityGuard = databaseContext.database().getDatabaseAvailabilityGuard();
            return (SimpleChannelInboundHandler) (!availabilityGuard.isAvailable() ? new UnavailableDatabaseHandler( this.messageType, this.protocol,
                    availabilityGuard, this.logProvider ) : (SimpleChannelInboundHandler) this.handlerFactory.apply( databaseContext.database() ));
        }
    }
}
