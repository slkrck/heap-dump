package com.neo4j.causalclustering.catchup;

import java.time.Duration;

import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.kernel.lifecycle.Lifecycle;
import org.neo4j.logging.Log;

public class CatchupClientFactory implements Lifecycle
{
    private final CatchupChannelPoolService pool;
    private final Duration inactivityTimeout;

    public CatchupClientFactory( Duration inactivityTimeout, CatchupChannelPoolService catchupChannelPoolService )
    {
        this.inactivityTimeout = inactivityTimeout;
        this.pool = catchupChannelPoolService;
    }

    public VersionedCatchupClients getClient( SocketAddress upstream, Log log )
    {
        return new CatchupClient( this.pool.acquire( upstream ).thenApply( CatchupChannel::new ), this.inactivityTimeout, log );
    }

    public void init()
    {
        this.pool.init();
    }

    public void start()
    {
        this.pool.start();
    }

    public void stop()
    {
        this.pool.stop();
    }

    public void shutdown()
    {
        this.pool.shutdown();
    }
}
