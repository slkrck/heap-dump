package com.neo4j.causalclustering.catchup;

import com.neo4j.causalclustering.catchup.v3.CatchupProtocolClientInstaller;
import com.neo4j.causalclustering.net.BootstrapConfiguration;
import com.neo4j.causalclustering.protocol.ModifierProtocolInstaller;
import com.neo4j.causalclustering.protocol.NettyPipelineBuilderFactory;
import com.neo4j.causalclustering.protocol.ProtocolInstaller;
import com.neo4j.causalclustering.protocol.ProtocolInstallerRepository;
import com.neo4j.causalclustering.protocol.application.ApplicationProtocols;
import com.neo4j.causalclustering.protocol.handshake.ApplicationProtocolRepository;
import com.neo4j.causalclustering.protocol.handshake.ApplicationSupportedProtocols;
import com.neo4j.causalclustering.protocol.handshake.HandshakeClientInitializer;
import com.neo4j.causalclustering.protocol.handshake.ModifierProtocolRepository;
import com.neo4j.causalclustering.protocol.handshake.ModifierSupportedProtocols;
import com.neo4j.causalclustering.protocol.init.ClientChannelInitializer;
import com.neo4j.causalclustering.protocol.modifier.ModifierProtocols;
import io.netty.channel.socket.SocketChannel;

import java.time.Clock;
import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

import org.neo4j.logging.LogProvider;
import org.neo4j.logging.NullLogProvider;
import org.neo4j.scheduler.JobScheduler;
import org.neo4j.time.Clocks;

public final class CatchupClientBuilder
{
    private CatchupClientBuilder()
    {
    }

    public static CatchupClientBuilder.NeedsCatchupProtocols builder()
    {
        return new CatchupClientBuilder.StepBuilder();
    }

    public interface AcceptsOptionalParams
    {
        CatchupClientBuilder.AcceptsOptionalParams handShakeTimeout( Duration var1 );

        CatchupClientBuilder.AcceptsOptionalParams clock( Clock var1 );

        CatchupClientBuilder.AcceptsOptionalParams debugLogProvider( LogProvider var1 );

        CatchupClientBuilder.AcceptsOptionalParams userLogProvider( LogProvider var1 );

        CatchupClientFactory build();
    }

    public interface NeedBootstrapConfig
    {
        CatchupClientBuilder.AcceptsOptionalParams bootstrapConfig( BootstrapConfiguration<? extends SocketChannel> var1 );
    }

    public interface NeedsInactivityTimeout
    {
        CatchupClientBuilder.NeedsScheduler inactivityTimeout( Duration var1 );
    }

    public interface NeedsScheduler
    {
        CatchupClientBuilder.NeedBootstrapConfig scheduler( JobScheduler var1 );
    }

    public interface NeedsPipelineBuilder
    {
        CatchupClientBuilder.NeedsInactivityTimeout pipelineBuilder( NettyPipelineBuilderFactory var1 );
    }

    public interface NeedsModifierProtocols
    {
        CatchupClientBuilder.NeedsPipelineBuilder modifierProtocols( Collection<ModifierSupportedProtocols> var1 );
    }

    public interface NeedsCatchupProtocols
    {
        CatchupClientBuilder.NeedsModifierProtocols catchupProtocols( ApplicationSupportedProtocols var1 );
    }

    private static class StepBuilder
            implements CatchupClientBuilder.NeedsCatchupProtocols, CatchupClientBuilder.NeedsModifierProtocols, CatchupClientBuilder.NeedsPipelineBuilder,
            CatchupClientBuilder.NeedsInactivityTimeout, CatchupClientBuilder.NeedsScheduler, CatchupClientBuilder.NeedBootstrapConfig,
            CatchupClientBuilder.AcceptsOptionalParams
    {
        private NettyPipelineBuilderFactory pipelineBuilder;
        private ApplicationSupportedProtocols catchupProtocols;
        private Collection<ModifierSupportedProtocols> modifierProtocols;
        private JobScheduler scheduler;
        private LogProvider debugLogProvider = NullLogProvider.getInstance();
        private LogProvider userLogProvider = NullLogProvider.getInstance();
        private Duration inactivityTimeout;
        private Duration handshakeTimeout = Duration.ofSeconds( 5L );
        private Clock clock = Clocks.systemClock();
        private BootstrapConfiguration<? extends SocketChannel> bootstrapConfiguration;

        public CatchupClientBuilder.NeedsModifierProtocols catchupProtocols( ApplicationSupportedProtocols catchupProtocols )
        {
            this.catchupProtocols = catchupProtocols;
            return this;
        }

        public CatchupClientBuilder.NeedsPipelineBuilder modifierProtocols( Collection<ModifierSupportedProtocols> modifierProtocols )
        {
            this.modifierProtocols = modifierProtocols;
            return this;
        }

        public CatchupClientBuilder.NeedsInactivityTimeout pipelineBuilder( NettyPipelineBuilderFactory pipelineBuilder )
        {
            this.pipelineBuilder = pipelineBuilder;
            return this;
        }

        public CatchupClientBuilder.NeedsScheduler inactivityTimeout( Duration inactivityTimeout )
        {
            this.inactivityTimeout = inactivityTimeout;
            return this;
        }

        public CatchupClientBuilder.NeedBootstrapConfig scheduler( JobScheduler scheduler )
        {
            this.scheduler = scheduler;
            return this;
        }

        public CatchupClientBuilder.AcceptsOptionalParams handShakeTimeout( Duration handshakeTimeout )
        {
            this.handshakeTimeout = handshakeTimeout;
            return this;
        }

        public CatchupClientBuilder.AcceptsOptionalParams clock( Clock clock )
        {
            this.clock = clock;
            return this;
        }

        public CatchupClientBuilder.AcceptsOptionalParams debugLogProvider( LogProvider debugLogProvider )
        {
            this.debugLogProvider = debugLogProvider;
            return this;
        }

        public CatchupClientBuilder.AcceptsOptionalParams userLogProvider( LogProvider userLogProvider )
        {
            this.userLogProvider = userLogProvider;
            return this;
        }

        public CatchupClientBuilder.AcceptsOptionalParams bootstrapConfig( BootstrapConfiguration<? extends SocketChannel> bootstrapConfiguration )
        {
            this.bootstrapConfiguration = bootstrapConfiguration;
            return this;
        }

        public CatchupClientFactory build()
        {
            ApplicationProtocolRepository applicationProtocolRepository =
                    new ApplicationProtocolRepository( ApplicationProtocols.values(), this.catchupProtocols );
            ModifierProtocolRepository modifierProtocolRepository = new ModifierProtocolRepository( ModifierProtocols.values(), this.modifierProtocols );
            Function<CatchupResponseHandler,ClientChannelInitializer> channelInitializerFactory = ( handler ) -> {
                List<ProtocolInstaller.Factory<ProtocolInstaller.Orientation.Client,?>> installers =
                        List.of( new CatchupProtocolClientInstaller.Factory( this.pipelineBuilder, this.debugLogProvider, handler ) );
                ProtocolInstallerRepository<ProtocolInstaller.Orientation.Client> protocolInstallerRepository =
                        new ProtocolInstallerRepository( installers, ModifierProtocolInstaller.allClientInstallers );
                HandshakeClientInitializer handshakeInitializer =
                        new HandshakeClientInitializer( applicationProtocolRepository, modifierProtocolRepository, protocolInstallerRepository,
                                this.pipelineBuilder, this.handshakeTimeout, this.debugLogProvider, this.debugLogProvider );
                return new ClientChannelInitializer( handshakeInitializer, this.pipelineBuilder, this.handshakeTimeout, this.debugLogProvider );
            };
            CatchupChannelPoolService catchupChannelPoolService =
                    new CatchupChannelPoolService( this.bootstrapConfiguration, this.scheduler, this.clock, channelInitializerFactory );
            return new CatchupClientFactory( this.inactivityTimeout, catchupChannelPoolService );
        }
    }
}
