package com.neo4j.causalclustering.catchup;

import com.neo4j.causalclustering.catchup.storecopy.PrepareStoreCopyResponse;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class StoreListingResponseHandler extends SimpleChannelInboundHandler<PrepareStoreCopyResponse>
{
    private final CatchupClientProtocol protocol;
    private final CatchupResponseHandler handler;

    public StoreListingResponseHandler( CatchupClientProtocol protocol, CatchupResponseHandler handler )
    {
        this.protocol = protocol;
        this.handler = handler;
    }

    protected void channelRead0( ChannelHandlerContext ctx, PrepareStoreCopyResponse msg ) throws Exception
    {
        this.handler.onStoreListingResponse( msg );
        this.protocol.expect( CatchupClientProtocol.State.MESSAGE_TYPE );
    }
}
