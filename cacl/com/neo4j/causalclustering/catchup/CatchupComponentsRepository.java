package com.neo4j.causalclustering.catchup;

import com.neo4j.causalclustering.catchup.storecopy.RemoteStore;
import com.neo4j.causalclustering.catchup.storecopy.StoreCopyProcess;
import com.neo4j.dbms.database.ClusteredDatabaseContext;

import java.util.Optional;

import org.neo4j.dbms.database.DatabaseManager;
import org.neo4j.kernel.database.NamedDatabaseId;

public class CatchupComponentsRepository
{
    private final DatabaseManager<ClusteredDatabaseContext> databaseManager;

    public CatchupComponentsRepository( DatabaseManager<ClusteredDatabaseContext> databaseManager )
    {
        this.databaseManager = databaseManager;
    }

    public Optional<CatchupComponentsRepository.CatchupComponents> componentsFor( NamedDatabaseId namedDatabaseId )
    {
        return this.databaseManager.getDatabaseContext( namedDatabaseId ).map( ClusteredDatabaseContext::catchupComponents );
    }

    public static class CatchupComponents
    {
        final RemoteStore remoteStore;
        final StoreCopyProcess storeCopy;

        public CatchupComponents( RemoteStore remoteStore, StoreCopyProcess storeCopy )
        {
            this.remoteStore = remoteStore;
            this.storeCopy = storeCopy;
        }

        public StoreCopyProcess storeCopyProcess()
        {
            return this.storeCopy;
        }

        public RemoteStore remoteStore()
        {
            return this.remoteStore;
        }
    }
}
