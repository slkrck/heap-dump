package com.neo4j.causalclustering.catchup;

import com.neo4j.causalclustering.catchup.storecopy.GetStoreFileRequestHandler;
import com.neo4j.causalclustering.catchup.storecopy.PrepareStoreCopyFilesProvider;
import com.neo4j.causalclustering.catchup.storecopy.PrepareStoreCopyRequestHandler;
import com.neo4j.causalclustering.catchup.storecopy.StoreFileStreamingProtocol;
import com.neo4j.causalclustering.catchup.tx.TxPullRequestHandler;
import com.neo4j.causalclustering.catchup.v3.databaseid.GetDatabaseIdRequestHandler;
import com.neo4j.causalclustering.catchup.v3.storecopy.GetStoreFileRequest;
import com.neo4j.causalclustering.catchup.v3.storecopy.GetStoreIdRequest;
import com.neo4j.causalclustering.catchup.v3.storecopy.GetStoreIdRequestHandler;
import com.neo4j.causalclustering.catchup.v3.storecopy.PrepareStoreCopyRequest;
import com.neo4j.causalclustering.catchup.v3.tx.TxPullRequest;
import com.neo4j.causalclustering.core.state.snapshot.CoreSnapshotRequest;
import com.neo4j.causalclustering.core.state.snapshot.CoreSnapshotRequestHandler;
import io.netty.channel.ChannelHandler;
import org.neo4j.dbms.database.DatabaseManager;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.kernel.database.Database;
import org.neo4j.logging.LogProvider;

public class MultiDatabaseCatchupServerHandler implements CatchupServerHandler
{
    private final DatabaseManager<?> databaseManager;
    private final LogProvider logProvider;
    private final FileSystemAbstraction fs;
    private final int maxChunkSize;

    public MultiDatabaseCatchupServerHandler( DatabaseManager<?> databaseManager, FileSystemAbstraction fs, int maxChunkSize, LogProvider logProvider )
    {
        this.databaseManager = databaseManager;
        this.maxChunkSize = maxChunkSize;
        this.logProvider = logProvider;
        this.fs = fs;
    }

    private static GetStoreIdRequestHandler buildStoreIdRequestHandler( Database db, CatchupServerProtocol protocol )
    {
        return new GetStoreIdRequestHandler( protocol, db );
    }

    private static CoreSnapshotRequestHandler buildCoreSnapshotRequestRequestHandler( Database db, CatchupServerProtocol protocol )
    {
        return new CoreSnapshotRequestHandler( protocol, db );
    }

    public ChannelHandler getDatabaseIdRequestHandler( CatchupServerProtocol protocol )
    {
        return new GetDatabaseIdRequestHandler( this.databaseManager.databaseIdRepository(), protocol );
    }

    public ChannelHandler txPullRequestHandler( CatchupServerProtocol protocol )
    {
        return new MultiplexingCatchupRequestHandler( protocol, this.databaseManager, ( db ) -> {
            return this.buildTxPullRequestHandler( db, protocol );
        }, TxPullRequest.class, this.logProvider );
    }

    public ChannelHandler getStoreIdRequestHandler( CatchupServerProtocol protocol )
    {
        return new MultiplexingCatchupRequestHandler( protocol, this.databaseManager, ( db ) -> {
            return buildStoreIdRequestHandler( db, protocol );
        }, GetStoreIdRequest.class, this.logProvider );
    }

    public ChannelHandler storeListingRequestHandler( CatchupServerProtocol protocol )
    {
        return new MultiplexingCatchupRequestHandler( protocol, this.databaseManager, ( db ) -> {
            return this.buildStoreListingRequestHandler( db, protocol );
        }, PrepareStoreCopyRequest.class, this.logProvider );
    }

    public ChannelHandler getStoreFileRequestHandler( CatchupServerProtocol protocol )
    {
        return new MultiplexingCatchupRequestHandler( protocol, this.databaseManager, ( db ) -> {
            return this.buildStoreFileRequestHandler( db, protocol );
        }, GetStoreFileRequest.class, this.logProvider );
    }

    public ChannelHandler snapshotHandler( CatchupServerProtocol protocol )
    {
        return new MultiplexingCatchupRequestHandler( protocol, this.databaseManager, ( db ) -> {
            return buildCoreSnapshotRequestRequestHandler( db, protocol );
        }, CoreSnapshotRequest.class, this.logProvider );
    }

    private TxPullRequestHandler buildTxPullRequestHandler( Database db, CatchupServerProtocol protocol )
    {
        return new TxPullRequestHandler( protocol, db );
    }

    private PrepareStoreCopyRequestHandler buildStoreListingRequestHandler( Database db, CatchupServerProtocol protocol )
    {
        return new PrepareStoreCopyRequestHandler( protocol, db, new PrepareStoreCopyFilesProvider( this.fs ), this.maxChunkSize );
    }

    private GetStoreFileRequestHandler buildStoreFileRequestHandler( Database db, CatchupServerProtocol protocol )
    {
        return new GetStoreFileRequestHandler( protocol, db, new StoreFileStreamingProtocol( this.maxChunkSize ), this.fs );
    }
}
