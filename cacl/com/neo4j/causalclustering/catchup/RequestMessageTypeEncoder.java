package com.neo4j.causalclustering.catchup;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class RequestMessageTypeEncoder extends MessageToByteEncoder<RequestMessageType>
{
    protected void encode( ChannelHandlerContext ctx, RequestMessageType request, ByteBuf out )
    {
        out.writeByte( request.messageType() );
    }
}
