package com.neo4j.causalclustering.catchup;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class ResponseMessageTypeEncoder extends MessageToByteEncoder<ResponseMessageType>
{
    protected void encode( ChannelHandlerContext ctx, ResponseMessageType response, ByteBuf out )
    {
        out.writeByte( response.messageType() );
    }
}
