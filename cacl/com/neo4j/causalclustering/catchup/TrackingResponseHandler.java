package com.neo4j.causalclustering.catchup;

import com.neo4j.causalclustering.catchup.storecopy.FileChunk;
import com.neo4j.causalclustering.catchup.storecopy.FileHeader;
import com.neo4j.causalclustering.catchup.storecopy.GetStoreIdResponse;
import com.neo4j.causalclustering.catchup.storecopy.PrepareStoreCopyResponse;
import com.neo4j.causalclustering.catchup.storecopy.StoreCopyFinishedResponse;
import com.neo4j.causalclustering.catchup.tx.TxPullResponse;
import com.neo4j.causalclustering.catchup.tx.TxStreamFinishedResponse;
import com.neo4j.causalclustering.catchup.v3.databaseid.GetDatabaseIdResponse;
import com.neo4j.causalclustering.core.state.snapshot.CoreSnapshot;

import java.nio.channels.ClosedChannelException;
import java.time.Clock;
import java.util.OptionalLong;
import java.util.concurrent.CompletableFuture;

class TrackingResponseHandler implements CatchupResponseHandler
{
    private static final CompletableFuture<Object> ILLEGAL_FUTURE = CompletableFuture.failedFuture( new IllegalStateException( "Not expected" ) );
    private static final CatchupResponseAdaptor ILLEGAL_HANDLER = new CatchupResponseAdaptor();
    private static final long NO_RESPONSE_TIME = 1L;
    private final Clock clock;
    private CatchupResponseCallback delegate;
    private CompletableFuture<?> requestOutcomeSignal;
    private long lastResponseTime = 1L;

    TrackingResponseHandler( Clock clock )
    {
        this.clock = clock;
        this.clearResponseHandler();
    }

    void clearResponseHandler()
    {
        this.requestOutcomeSignal = ILLEGAL_FUTURE;
        this.delegate = ILLEGAL_HANDLER;
        this.lastResponseTime = 1L;
    }

    void setResponseHandler( CatchupResponseCallback responseHandler, CompletableFuture<?> requestOutcomeSignal )
    {
        this.delegate = responseHandler;
        this.requestOutcomeSignal = requestOutcomeSignal;
        this.lastResponseTime = 1L;
    }

    public void onFileHeader( FileHeader fileHeader )
    {
        this.ifNotCancelled( () -> {
            this.delegate.onFileHeader( this.requestOutcomeSignal, fileHeader );
        } );
    }

    public boolean onFileContent( FileChunk fileChunk )
    {
        if ( !this.requestOutcomeSignal.isCancelled() )
        {
            this.recordLastResponse();
            return this.delegate.onFileContent( this.requestOutcomeSignal, fileChunk );
        }
        else
        {
            return true;
        }
    }

    public void onFileStreamingComplete( StoreCopyFinishedResponse response )
    {
        this.ifNotCancelled( () -> {
            this.delegate.onFileStreamingComplete( this.requestOutcomeSignal, response );
        } );
    }

    public void onTxPullResponse( TxPullResponse tx )
    {
        this.ifNotCancelled( () -> {
            this.delegate.onTxPullResponse( this.requestOutcomeSignal, tx );
        } );
    }

    public void onTxStreamFinishedResponse( TxStreamFinishedResponse response )
    {
        this.ifNotCancelled( () -> {
            this.delegate.onTxStreamFinishedResponse( this.requestOutcomeSignal, response );
        } );
    }

    public void onGetStoreIdResponse( GetStoreIdResponse response )
    {
        this.ifNotCancelled( () -> {
            this.delegate.onGetStoreIdResponse( this.requestOutcomeSignal, response );
        } );
    }

    public void onGetDatabaseIdResponse( GetDatabaseIdResponse response )
    {
        this.ifNotCancelled( () -> {
            this.delegate.onGetDatabaseIdResponse( this.requestOutcomeSignal, response );
        } );
    }

    public void onCoreSnapshot( CoreSnapshot coreSnapshot )
    {
        this.ifNotCancelled( () -> {
            this.delegate.onCoreSnapshot( this.requestOutcomeSignal, coreSnapshot );
        } );
    }

    public void onStoreListingResponse( PrepareStoreCopyResponse storeListingRequest )
    {
        this.ifNotCancelled( () -> {
            this.delegate.onStoreListingResponse( this.requestOutcomeSignal, storeListingRequest );
        } );
    }

    public void onCatchupErrorResponse( CatchupErrorResponse catchupErrorResponse )
    {
        this.ifNotCancelled( () -> {
            this.delegate.onCatchupErrorResponse( this.requestOutcomeSignal, catchupErrorResponse );
        } );
    }

    private void ifNotCancelled( Runnable runnable )
    {
        if ( !this.requestOutcomeSignal.isCancelled() )
        {
            this.recordLastResponse();
            runnable.run();
        }
    }

    public void onClose()
    {
        this.requestOutcomeSignal.completeExceptionally( new ClosedChannelException() );
    }

    OptionalLong millisSinceLastResponse()
    {
        return this.lastResponseTime == 1L ? OptionalLong.empty() : OptionalLong.of( this.clock.millis() - this.lastResponseTime );
    }

    private void recordLastResponse()
    {
        this.lastResponseTime = this.clock.millis();
    }
}
