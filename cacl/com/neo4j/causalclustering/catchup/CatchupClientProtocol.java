package com.neo4j.causalclustering.catchup;

public class CatchupClientProtocol extends Protocol<CatchupClientProtocol.State>
{
    public CatchupClientProtocol()
    {
        super( CatchupClientProtocol.State.MESSAGE_TYPE );
    }

    public static enum State
    {
        MESSAGE_TYPE,
        STORE_ID,
        CORE_SNAPSHOT,
        TX_PULL_RESPONSE,
        STORE_COPY_FINISHED,
        TX_STREAM_FINISHED,
        FILE_HEADER,
        FILE_CHUNK,
        PREPARE_STORE_COPY_RESPONSE,
        INDEX_SNAPSHOT_RESPONSE,
        ERROR_RESPONSE,
        DATABASE_ID;
    }
}
