package com.neo4j.causalclustering.catchup;

import com.neo4j.causalclustering.core.state.snapshot.TopologyLookupException;
import com.neo4j.causalclustering.identity.MemberId;

public class CatchupAddressResolutionException extends TopologyLookupException
{
    public CatchupAddressResolutionException( MemberId memberId )
    {
        super( memberId );
    }

    CatchupAddressResolutionException( Exception e )
    {
        super( (Throwable) e );
    }
}
