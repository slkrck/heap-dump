package com.neo4j.causalclustering.catchup;

import java.util.Map;

public abstract class Protocol<E extends Enum<E>>
{
    private E state;

    protected Protocol( E initialValue )
    {
        this.state = initialValue;
    }

    public void expect( E state )
    {
        this.state = state;
    }

    public boolean isExpecting( E state )
    {
        return this.state == state;
    }

    public <T> T select( Map<E,T> map )
    {
        return map.get( this.state );
    }

    public String toString()
    {
        String var10000 = this.getClass().getSimpleName();
        return var10000 + "{state=" + this.state + "}";
    }
}
