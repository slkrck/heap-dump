package com.neo4j.causalclustering.catchup;

import com.neo4j.causalclustering.net.Server;

@FunctionalInterface
public interface CatchupServerProvider
{
    Server catchupServer();
}
