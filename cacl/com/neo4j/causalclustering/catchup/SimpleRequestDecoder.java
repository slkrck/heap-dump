package com.neo4j.causalclustering.catchup;

import com.neo4j.causalclustering.messaging.Message;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;

import java.util.List;

import org.neo4j.function.Factory;

public class SimpleRequestDecoder extends MessageToMessageDecoder<ByteBuf>
{
    private Factory<? extends Message> factory;

    public SimpleRequestDecoder( Factory<? extends Message> factory )
    {
        this.factory = factory;
    }

    protected void decode( ChannelHandlerContext ctx, ByteBuf msg, List<Object> out )
    {
        out.add( this.factory.newInstance() );
    }
}
