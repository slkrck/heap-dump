package com.neo4j.causalclustering.catchup;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class ClientMessageTypeHandler extends ChannelInboundHandlerAdapter
{
    private final Log log;
    private final CatchupClientProtocol protocol;

    public ClientMessageTypeHandler( CatchupClientProtocol protocol, LogProvider logProvider )
    {
        this.protocol = protocol;
        this.log = logProvider.getLog( this.getClass() );
    }

    public void channelRead( ChannelHandlerContext ctx, Object msg )
    {
        if ( this.protocol.isExpecting( CatchupClientProtocol.State.MESSAGE_TYPE ) )
        {
            byte byteValue = ((ByteBuf) msg).readByte();
            ResponseMessageType responseMessageType = ResponseMessageType.from( byteValue );
            switch ( responseMessageType )
            {
            case STORE_ID:
                this.protocol.expect( CatchupClientProtocol.State.STORE_ID );
                break;
            case TX:
                this.protocol.expect( CatchupClientProtocol.State.TX_PULL_RESPONSE );
                break;
            case FILE:
                this.protocol.expect( CatchupClientProtocol.State.FILE_HEADER );
                break;
            case STORE_COPY_FINISHED:
                this.protocol.expect( CatchupClientProtocol.State.STORE_COPY_FINISHED );
                break;
            case CORE_SNAPSHOT:
                this.protocol.expect( CatchupClientProtocol.State.CORE_SNAPSHOT );
                break;
            case TX_STREAM_FINISHED:
                this.protocol.expect( CatchupClientProtocol.State.TX_STREAM_FINISHED );
                break;
            case PREPARE_STORE_COPY_RESPONSE:
                this.protocol.expect( CatchupClientProtocol.State.PREPARE_STORE_COPY_RESPONSE );
                break;
            case INDEX_SNAPSHOT_RESPONSE:
                this.protocol.expect( CatchupClientProtocol.State.INDEX_SNAPSHOT_RESPONSE );
                break;
            case ERROR:
                this.protocol.expect( CatchupClientProtocol.State.ERROR_RESPONSE );
                break;
            case DATABASE_ID_RESPONSE:
                this.protocol.expect( CatchupClientProtocol.State.DATABASE_ID );
                break;
            default:
                this.log.warn( "No handler found for message type %s (%d)", new Object[]{responseMessageType.name(), byteValue} );
            }

            ReferenceCountUtil.release( msg );
        }
        else
        {
            ctx.fireChannelRead( msg );
        }
    }
}
