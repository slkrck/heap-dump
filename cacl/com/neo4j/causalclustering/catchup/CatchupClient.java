package com.neo4j.causalclustering.catchup;

import com.neo4j.causalclustering.catchup.storecopy.PrepareStoreCopyResponse;
import com.neo4j.causalclustering.catchup.storecopy.StoreCopyFinishedResponse;
import com.neo4j.causalclustering.catchup.tx.TxStreamFinishedResponse;
import com.neo4j.causalclustering.catchup.v3.databaseid.GetDatabaseIdRequest;
import com.neo4j.causalclustering.catchup.v3.storecopy.GetStoreFileRequest;
import com.neo4j.causalclustering.catchup.v3.storecopy.GetStoreIdRequest;
import com.neo4j.causalclustering.catchup.v3.storecopy.PrepareStoreCopyRequest;
import com.neo4j.causalclustering.catchup.v3.tx.TxPullRequest;
import com.neo4j.causalclustering.core.state.snapshot.CoreSnapshot;
import com.neo4j.causalclustering.core.state.snapshot.CoreSnapshotRequest;
import com.neo4j.causalclustering.helper.OperationProgressMonitor;
import com.neo4j.causalclustering.messaging.CatchupProtocolMessage;
import com.neo4j.causalclustering.protocol.application.ApplicationProtocol;
import com.neo4j.causalclustering.protocol.application.ApplicationProtocols;

import java.io.File;
import java.time.Duration;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.function.Function;

import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.storageengine.api.StoreId;

class CatchupClient implements VersionedCatchupClients
{
    private static final int REQUEST_SENT_TIMEOUT = 1;
    private static final TimeUnit REQUEST_SENT_TIME_UNIT;

    static
    {
        REQUEST_SENT_TIME_UNIT = TimeUnit.MINUTES;
    }

    private final CompletableFuture<CatchupChannel> channelFuture;
    private final Duration inactivityTimeout;
    private final Log log;

    CatchupClient( CompletableFuture<CatchupChannel> channelFuture, Duration inactivityTimeout, Log log )
    {
        this.channelFuture = channelFuture;
        this.inactivityTimeout = inactivityTimeout;
        this.log = log;
    }

    private static <RESULT> CompletableFuture<RESULT> makeBlockingRequest( CatchupProtocolMessage request, CatchupResponseCallback<RESULT> responseHandler,
            CatchupChannel channel )
    {
        CompletableFuture future = new CompletableFuture();

        try
        {
            future.whenComplete( new CatchupClient.ReleaseOnComplete( channel ) );
            channel.setResponseHandler( responseHandler, future );
            channel.send( request );
        }
        catch ( Exception var5 )
        {
            future.completeExceptionally( new CatchUpClientException( "Failed to send request", var5 ) );
        }

        return future;
    }

    public <RESULT> VersionedCatchupClients.NeedsResponseHandler<RESULT> v3(
            Function<VersionedCatchupClients.CatchupClientV3,VersionedCatchupClients.PreparedRequest<RESULT>> v3Request )
    {
        CatchupClient.Builder<RESULT> reqBuilder = new CatchupClient.Builder( this.channelFuture, this.log );
        return reqBuilder.v3( v3Request );
    }

    public void close()
    {
    }

    private static class ReleaseOnComplete implements BiConsumer<Object,Throwable>
    {
        private final CatchupChannel catchUpChannel;

        ReleaseOnComplete( CatchupChannel catchUpChannel )
        {
            this.catchUpChannel = catchUpChannel;
        }

        public void accept( Object o, Throwable throwable )
        {
            if ( throwable != null )
            {
                this.catchUpChannel.dispose();
            }
            else
            {
                this.catchUpChannel.release();
            }
        }
    }

    private static class V3 implements VersionedCatchupClients.CatchupClientV3
    {
        private final CatchupChannel channel;

        private V3( CatchupChannel channel )
        {
            this.channel = channel;
        }

        public VersionedCatchupClients.PreparedRequest<NamedDatabaseId> getDatabaseId( String databaseName )
        {
            return ( handler ) -> {
                return CatchupClient.makeBlockingRequest( new GetDatabaseIdRequest( databaseName ), handler, this.channel );
            };
        }

        public VersionedCatchupClients.PreparedRequest<CoreSnapshot> getCoreSnapshot( NamedDatabaseId namedDatabaseId )
        {
            return ( handler ) -> {
                return CatchupClient.makeBlockingRequest( new CoreSnapshotRequest( namedDatabaseId.databaseId() ), handler, this.channel );
            };
        }

        public VersionedCatchupClients.PreparedRequest<StoreId> getStoreId( NamedDatabaseId namedDatabaseId )
        {
            return ( handler ) -> {
                return CatchupClient.makeBlockingRequest( new GetStoreIdRequest( namedDatabaseId.databaseId() ), handler, this.channel );
            };
        }

        public VersionedCatchupClients.PreparedRequest<TxStreamFinishedResponse> pullTransactions( StoreId storeId, long previousTxId,
                NamedDatabaseId namedDatabaseId )
        {
            return ( handler ) -> {
                return CatchupClient.makeBlockingRequest( new TxPullRequest( previousTxId, storeId, namedDatabaseId.databaseId() ), handler, this.channel );
            };
        }

        public VersionedCatchupClients.PreparedRequest<PrepareStoreCopyResponse> prepareStoreCopy( StoreId storeId, NamedDatabaseId namedDatabaseId )
        {
            return ( handler ) -> {
                return CatchupClient.makeBlockingRequest( new PrepareStoreCopyRequest( storeId, namedDatabaseId.databaseId() ), handler, this.channel );
            };
        }

        public VersionedCatchupClients.PreparedRequest<StoreCopyFinishedResponse> getStoreFile( StoreId storeId, File file, long requiredTxId,
                NamedDatabaseId namedDatabaseId )
        {
            return ( handler ) -> {
                return CatchupClient.makeBlockingRequest( new GetStoreFileRequest( storeId, file, requiredTxId, namedDatabaseId.databaseId() ), handler,
                        this.channel );
            };
        }
    }

    private class Builder<RESULT> implements VersionedCatchupClients.CatchupRequestBuilder<RESULT>
    {
        private final CompletableFuture<CatchupChannel> channel;
        private final Log log;
        private Function<VersionedCatchupClients.CatchupClientV3,VersionedCatchupClients.PreparedRequest<RESULT>> v3Request;
        private CatchupResponseCallback<RESULT> responseHandler;

        private Builder( CompletableFuture<CatchupChannel> channel, Log log )
        {
            this.channel = channel;
            this.log = log;
        }

        public VersionedCatchupClients.NeedsResponseHandler<RESULT> v3(
                Function<VersionedCatchupClients.CatchupClientV3,VersionedCatchupClients.PreparedRequest<RESULT>> v3Request )
        {
            this.v3Request = v3Request;
            return this;
        }

        public VersionedCatchupClients.CatchupRequestBuilder<RESULT> withResponseHandler( CatchupResponseCallback<RESULT> responseHandler )
        {
            this.responseHandler = responseHandler;
            return this;
        }

        public RESULT request() throws Exception
        {
            return ((OperationProgressMonitor) this.channel.thenApply( this::performRequest ).get( 1L, CatchupClient.REQUEST_SENT_TIME_UNIT )).get();
        }

        private OperationProgressMonitor<RESULT> performRequest( CatchupChannel catchupChannel )
        {
            ApplicationProtocol protocol = catchupChannel.protocol();
            if ( protocol.equals( ApplicationProtocols.CATCHUP_3_0 ) )
            {
                CatchupClient.V3 client = new CatchupClient.V3( catchupChannel );
                return this.performRequest( client, this.v3Request, protocol, catchupChannel );
            }
            else
            {
                String message = "Unrecognised protocol " + protocol;
                this.log.error( message );
                throw new IllegalStateException( message );
            }
        }

        private <CLIENT> OperationProgressMonitor<RESULT> performRequest( CLIENT client,
                Function<CLIENT,VersionedCatchupClients.PreparedRequest<RESULT>> specificVersionRequest, ApplicationProtocol protocol,
                CatchupChannel catchupChannel )
        {
            if ( specificVersionRequest != null )
            {
                VersionedCatchupClients.PreparedRequest<RESULT> request = (VersionedCatchupClients.PreparedRequest) specificVersionRequest.apply( client );
                return this.withProgressMonitor( request.execute( this.responseHandler ), catchupChannel );
            }
            else
            {
                String message = "No action specified for protocol " + protocol;
                this.log.error( message );
                throw new IllegalStateException( message );
            }
        }

        private OperationProgressMonitor<RESULT> withProgressMonitor( CompletableFuture<RESULT> request, CatchupChannel catchupChannel )
        {
            long var10001 = CatchupClient.this.inactivityTimeout.toMillis();
            Objects.requireNonNull( catchupChannel );
            return OperationProgressMonitor.of( request, var10001, catchupChannel::millisSinceLastResponse, this.log );
        }
    }
}
