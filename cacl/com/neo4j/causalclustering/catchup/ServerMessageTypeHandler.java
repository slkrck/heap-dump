package com.neo4j.causalclustering.catchup;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class ServerMessageTypeHandler extends ChannelInboundHandlerAdapter
{
    private final Log log;
    private final CatchupServerProtocol protocol;

    public ServerMessageTypeHandler( CatchupServerProtocol protocol, LogProvider logProvider )
    {
        this.protocol = protocol;
        this.log = logProvider.getLog( this.getClass() );
    }

    public void channelRead( ChannelHandlerContext ctx, Object msg )
    {
        if ( this.protocol.isExpecting( CatchupServerProtocol.State.MESSAGE_TYPE ) )
        {
            RequestMessageType requestMessageType = RequestMessageType.from( ((ByteBuf) msg).readByte() );
            if ( requestMessageType.equals( RequestMessageType.TX_PULL_REQUEST ) )
            {
                this.protocol.expect( CatchupServerProtocol.State.TX_PULL );
            }
            else if ( requestMessageType.equals( RequestMessageType.STORE_ID ) )
            {
                this.protocol.expect( CatchupServerProtocol.State.GET_STORE_ID );
            }
            else if ( requestMessageType.equals( RequestMessageType.DATABASE_ID ) )
            {
                this.protocol.expect( CatchupServerProtocol.State.GET_DATABASE_ID );
            }
            else if ( requestMessageType.equals( RequestMessageType.CORE_SNAPSHOT ) )
            {
                this.protocol.expect( CatchupServerProtocol.State.GET_CORE_SNAPSHOT );
            }
            else if ( requestMessageType.equals( RequestMessageType.PREPARE_STORE_COPY ) )
            {
                this.protocol.expect( CatchupServerProtocol.State.PREPARE_STORE_COPY );
            }
            else if ( requestMessageType.equals( RequestMessageType.STORE_FILE ) )
            {
                this.protocol.expect( CatchupServerProtocol.State.GET_STORE_FILE );
            }
            else
            {
                this.log.warn( "No handler found for message type %s", new Object[]{requestMessageType} );
            }

            ReferenceCountUtil.release( msg );
        }
        else
        {
            ctx.fireChannelRead( msg );
        }
    }
}
