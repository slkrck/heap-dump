package com.neo4j.causalclustering.catchup;

import com.neo4j.causalclustering.messaging.Message;

public enum RequestMessageType implements Message
{
    TX_PULL_REQUEST( (byte) 1 ),
    STORE( (byte) 2 ),
    CORE_SNAPSHOT( (byte) 3 ),
    STORE_ID( (byte) 4 ),
    PREPARE_STORE_COPY( (byte) 5 ),
    STORE_FILE( (byte) 6 ),
    DATABASE_ID( (byte) 7 ),
    UNKNOWN( (byte) -108 );

    private byte messageType;

    private RequestMessageType( byte messageType )
    {
        this.messageType = messageType;
    }

    public static RequestMessageType from( byte b )
    {
        RequestMessageType[] var1 = values();
        int var2 = var1.length;

        for ( int var3 = 0; var3 < var2; ++var3 )
        {
            RequestMessageType responseMessageType = var1[var3];
            if ( responseMessageType.messageType == b )
            {
                return responseMessageType;
            }
        }

        return UNKNOWN;
    }

    public byte messageType()
    {
        return this.messageType;
    }

    public String toString()
    {
        return String.format( "RequestMessageType{messageType=%s}", this.messageType );
    }
}
