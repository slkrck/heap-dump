package com.neo4j.causalclustering.catchup;

public enum ResponseMessageType
{
    TX( (byte) 1 ),
    STORE_ID( (byte) 2 ),
    FILE( (byte) 3 ),
    STORE_COPY_FINISHED( (byte) 4 ),
    CORE_SNAPSHOT( (byte) 5 ),
    TX_STREAM_FINISHED( (byte) 6 ),
    PREPARE_STORE_COPY_RESPONSE( (byte) 7 ),
    INDEX_SNAPSHOT_RESPONSE( (byte) 8 ),
    DATABASE_ID_RESPONSE( (byte) 9 ),
    ERROR( (byte) -57 ),
    UNKNOWN( (byte) -56 );

    private byte messageType;

    private ResponseMessageType( byte messageType )
    {
        this.messageType = messageType;
    }

    public static ResponseMessageType from( byte b )
    {
        ResponseMessageType[] var1 = values();
        int var2 = var1.length;

        for ( int var3 = 0; var3 < var2; ++var3 )
        {
            ResponseMessageType responseMessageType = var1[var3];
            if ( responseMessageType.messageType == b )
            {
                return responseMessageType;
            }
        }

        return UNKNOWN;
    }

    public byte messageType()
    {
        return this.messageType;
    }

    public String toString()
    {
        return String.format( "ResponseMessageType{messageType=%s}", this.messageType );
    }
}
