package com.neo4j.causalclustering.catchup;

import com.neo4j.causalclustering.catchup.v3.CatchupProtocolServerInstaller;
import com.neo4j.causalclustering.net.BootstrapConfiguration;
import com.neo4j.causalclustering.net.Server;
import com.neo4j.causalclustering.protocol.ModifierProtocolInstaller;
import com.neo4j.causalclustering.protocol.NettyPipelineBuilderFactory;
import com.neo4j.causalclustering.protocol.ProtocolInstaller;
import com.neo4j.causalclustering.protocol.ProtocolInstallerRepository;
import com.neo4j.causalclustering.protocol.application.ApplicationProtocols;
import com.neo4j.causalclustering.protocol.handshake.ApplicationProtocolRepository;
import com.neo4j.causalclustering.protocol.handshake.ApplicationSupportedProtocols;
import com.neo4j.causalclustering.protocol.handshake.HandshakeServerInitializer;
import com.neo4j.causalclustering.protocol.handshake.ModifierProtocolRepository;
import com.neo4j.causalclustering.protocol.handshake.ModifierSupportedProtocols;
import com.neo4j.causalclustering.protocol.init.ServerChannelInitializer;
import com.neo4j.causalclustering.protocol.modifier.ModifierProtocols;
import io.netty.channel.ChannelInboundHandler;
import io.netty.channel.socket.ServerSocketChannel;

import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Executor;

import org.neo4j.configuration.Config;
import org.neo4j.configuration.connectors.ConnectorPortRegister;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.logging.LogProvider;
import org.neo4j.logging.NullLogProvider;
import org.neo4j.scheduler.Group;
import org.neo4j.scheduler.JobScheduler;

public final class CatchupServerBuilder
{
    private CatchupServerBuilder()
    {
    }

    public static CatchupServerBuilder.NeedsCatchupServerHandler builder()
    {
        return new CatchupServerBuilder.StepBuilder();
    }

    public interface AcceptsOptionalParams
    {
        CatchupServerBuilder.AcceptsOptionalParams serverName( String var1 );

        CatchupServerBuilder.AcceptsOptionalParams userLogProvider( LogProvider var1 );

        CatchupServerBuilder.AcceptsOptionalParams debugLogProvider( LogProvider var1 );

        CatchupServerBuilder.AcceptsOptionalParams handshakeTimeout( Duration var1 );

        Server build();
    }

    public interface NeedsPortRegister
    {
        CatchupServerBuilder.AcceptsOptionalParams portRegister( ConnectorPortRegister var1 );
    }

    public interface NeedsBootstrapConfig
    {
        CatchupServerBuilder.NeedsPortRegister bootstrapConfig( BootstrapConfiguration<? extends ServerSocketChannel> var1 );
    }

    public interface NeedsConfig
    {
        CatchupServerBuilder.NeedsBootstrapConfig config( Config var1 );
    }

    public interface NeedsScheduler
    {
        CatchupServerBuilder.NeedsConfig scheduler( JobScheduler var1 );
    }

    public interface NeedsListenAddress
    {
        CatchupServerBuilder.NeedsScheduler listenAddress( SocketAddress var1 );
    }

    public interface NeedsInstalledProtocolsHandler
    {
        CatchupServerBuilder.NeedsListenAddress installedProtocolsHandler( ChannelInboundHandler var1 );
    }

    public interface NeedsPipelineBuilder
    {
        CatchupServerBuilder.NeedsInstalledProtocolsHandler pipelineBuilder( NettyPipelineBuilderFactory var1 );
    }

    public interface NeedsModifierProtocols
    {
        CatchupServerBuilder.NeedsPipelineBuilder modifierProtocols( Collection<ModifierSupportedProtocols> var1 );
    }

    public interface NeedsCatchupProtocols
    {
        CatchupServerBuilder.NeedsModifierProtocols catchupProtocols( ApplicationSupportedProtocols var1 );
    }

    public interface NeedsCatchupServerHandler
    {
        CatchupServerBuilder.NeedsCatchupProtocols catchupServerHandler( CatchupServerHandler var1 );
    }

    private static class StepBuilder
            implements CatchupServerBuilder.NeedsCatchupServerHandler, CatchupServerBuilder.NeedsCatchupProtocols, CatchupServerBuilder.NeedsModifierProtocols,
            CatchupServerBuilder.NeedsPipelineBuilder, CatchupServerBuilder.NeedsInstalledProtocolsHandler, CatchupServerBuilder.NeedsListenAddress,
            CatchupServerBuilder.NeedsScheduler, CatchupServerBuilder.NeedsConfig, CatchupServerBuilder.NeedsBootstrapConfig,
            CatchupServerBuilder.NeedsPortRegister, CatchupServerBuilder.AcceptsOptionalParams
    {
        private CatchupServerHandler catchupServerHandler;
        private NettyPipelineBuilderFactory pipelineBuilder;
        private ApplicationSupportedProtocols catchupProtocols;
        private Collection<ModifierSupportedProtocols> modifierProtocols;
        private ChannelInboundHandler parentHandler;
        private SocketAddress listenAddress;
        private JobScheduler scheduler;
        private LogProvider debugLogProvider = NullLogProvider.getInstance();
        private LogProvider userLogProvider = NullLogProvider.getInstance();
        private Duration handshakeTimeout = Duration.ofSeconds( 5L );
        private ConnectorPortRegister portRegister;
        private String serverName = "catchup-server";
        private BootstrapConfiguration<? extends ServerSocketChannel> bootstrapConfiguration;
        private Config config = Config.defaults();

        public CatchupServerBuilder.NeedsCatchupProtocols catchupServerHandler( CatchupServerHandler catchupServerHandler )
        {
            this.catchupServerHandler = catchupServerHandler;
            return this;
        }

        public CatchupServerBuilder.NeedsModifierProtocols catchupProtocols( ApplicationSupportedProtocols catchupProtocols )
        {
            this.catchupProtocols = catchupProtocols;
            return this;
        }

        public CatchupServerBuilder.NeedsPipelineBuilder modifierProtocols( Collection<ModifierSupportedProtocols> modifierProtocols )
        {
            this.modifierProtocols = modifierProtocols;
            return this;
        }

        public CatchupServerBuilder.NeedsInstalledProtocolsHandler pipelineBuilder( NettyPipelineBuilderFactory pipelineBuilder )
        {
            this.pipelineBuilder = pipelineBuilder;
            return this;
        }

        public CatchupServerBuilder.NeedsListenAddress installedProtocolsHandler( ChannelInboundHandler parentHandler )
        {
            this.parentHandler = parentHandler;
            return this;
        }

        public CatchupServerBuilder.NeedsScheduler listenAddress( SocketAddress listenAddress )
        {
            this.listenAddress = listenAddress;
            return this;
        }

        public CatchupServerBuilder.NeedsConfig scheduler( JobScheduler scheduler )
        {
            this.scheduler = scheduler;
            return this;
        }

        public CatchupServerBuilder.AcceptsOptionalParams portRegister( ConnectorPortRegister portRegister )
        {
            this.portRegister = portRegister;
            return this;
        }

        public CatchupServerBuilder.AcceptsOptionalParams serverName( String serverName )
        {
            this.serverName = serverName;
            return this;
        }

        public CatchupServerBuilder.AcceptsOptionalParams userLogProvider( LogProvider userLogProvider )
        {
            this.userLogProvider = userLogProvider;
            return this;
        }

        public CatchupServerBuilder.AcceptsOptionalParams debugLogProvider( LogProvider debugLogProvider )
        {
            this.debugLogProvider = debugLogProvider;
            return this;
        }

        public CatchupServerBuilder.AcceptsOptionalParams handshakeTimeout( Duration handshakeTimeout )
        {
            this.handshakeTimeout = handshakeTimeout;
            return this;
        }

        public CatchupServerBuilder.NeedsBootstrapConfig config( Config config )
        {
            this.config = config;
            return this;
        }

        public CatchupServerBuilder.NeedsPortRegister bootstrapConfig( BootstrapConfiguration<? extends ServerSocketChannel> bootstrapConfiguration )
        {
            this.bootstrapConfiguration = bootstrapConfiguration;
            return this;
        }

        public Server build()
        {
            ApplicationProtocolRepository applicationProtocolRepository =
                    new ApplicationProtocolRepository( ApplicationProtocols.values(), this.catchupProtocols );
            ModifierProtocolRepository modifierProtocolRepository = new ModifierProtocolRepository( ModifierProtocols.values(), this.modifierProtocols );
            List<ProtocolInstaller.Factory<ProtocolInstaller.Orientation.Server,?>> protocolInstallers =
                    List.of( new CatchupProtocolServerInstaller.Factory( this.pipelineBuilder, this.debugLogProvider, this.catchupServerHandler ) );
            ProtocolInstallerRepository<ProtocolInstaller.Orientation.Server> protocolInstallerRepository =
                    new ProtocolInstallerRepository( protocolInstallers, ModifierProtocolInstaller.allServerInstallers );
            HandshakeServerInitializer handshakeInitializer =
                    new HandshakeServerInitializer( applicationProtocolRepository, modifierProtocolRepository, protocolInstallerRepository,
                            this.pipelineBuilder, this.debugLogProvider, this.config );
            ServerChannelInitializer channelInitializer =
                    new ServerChannelInitializer( handshakeInitializer, this.pipelineBuilder, this.handshakeTimeout, this.debugLogProvider, this.config );
            Executor executor = this.scheduler.executor( Group.CATCHUP_SERVER );
            return new Server( channelInitializer, this.parentHandler, this.debugLogProvider, this.userLogProvider, this.listenAddress, this.serverName,
                    executor, this.portRegister, this.bootstrapConfiguration );
        }
    }
}
