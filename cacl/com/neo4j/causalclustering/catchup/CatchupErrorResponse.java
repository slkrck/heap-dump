package com.neo4j.causalclustering.catchup;

import java.util.Objects;

public class CatchupErrorResponse
{
    private final CatchupResult status;
    private final String message;

    public CatchupErrorResponse( CatchupResult status, String message )
    {
        this.status = status;
        this.message = message;
    }

    public CatchupResult status()
    {
        return this.status;
    }

    public String message()
    {
        return this.message;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            CatchupErrorResponse that = (CatchupErrorResponse) o;
            return this.status == that.status && Objects.equals( this.message, that.message );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.status, this.message} );
    }

    public String toString()
    {
        return "CatchupErrorResponse{status=" + this.status + ", message='" + this.message + "'}";
    }
}
