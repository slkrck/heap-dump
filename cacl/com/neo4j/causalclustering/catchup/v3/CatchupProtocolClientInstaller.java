package com.neo4j.causalclustering.catchup.v3;

import com.neo4j.causalclustering.catchup.CatchupClientProtocol;
import com.neo4j.causalclustering.catchup.CatchupResponseHandler;
import com.neo4j.causalclustering.catchup.ClientMessageTypeHandler;
import com.neo4j.causalclustering.catchup.RequestDecoderDispatcher;
import com.neo4j.causalclustering.catchup.RequestMessageTypeEncoder;
import com.neo4j.causalclustering.catchup.ResponseMessageTypeEncoder;
import com.neo4j.causalclustering.catchup.StoreListingResponseHandler;
import com.neo4j.causalclustering.catchup.storecopy.FileChunkDecoder;
import com.neo4j.causalclustering.catchup.storecopy.FileChunkHandler;
import com.neo4j.causalclustering.catchup.storecopy.FileHeaderDecoder;
import com.neo4j.causalclustering.catchup.storecopy.FileHeaderHandler;
import com.neo4j.causalclustering.catchup.storecopy.GetStoreIdResponseDecoder;
import com.neo4j.causalclustering.catchup.storecopy.GetStoreIdResponseHandler;
import com.neo4j.causalclustering.catchup.storecopy.PrepareStoreCopyResponse;
import com.neo4j.causalclustering.catchup.storecopy.StoreCopyFinishedResponseHandler;
import com.neo4j.causalclustering.catchup.tx.TxStreamFinishedResponseDecoder;
import com.neo4j.causalclustering.catchup.tx.TxStreamFinishedResponseHandler;
import com.neo4j.causalclustering.catchup.v3.databaseid.GetDatabaseIdRequestEncoder;
import com.neo4j.causalclustering.catchup.v3.databaseid.GetDatabaseIdResponseDecoder;
import com.neo4j.causalclustering.catchup.v3.databaseid.GetDatabaseIdResponseHandler;
import com.neo4j.causalclustering.catchup.v3.storecopy.CatchupErrorResponseDecoder;
import com.neo4j.causalclustering.catchup.v3.storecopy.CatchupErrorResponseHandler;
import com.neo4j.causalclustering.catchup.v3.storecopy.CoreSnapshotRequestEncoder;
import com.neo4j.causalclustering.catchup.v3.storecopy.GetStoreFileRequestEncoder;
import com.neo4j.causalclustering.catchup.v3.storecopy.GetStoreIdRequestEncoder;
import com.neo4j.causalclustering.catchup.v3.storecopy.PrepareStoreCopyRequestEncoder;
import com.neo4j.causalclustering.catchup.v3.storecopy.StoreCopyFinishedResponseDecoder;
import com.neo4j.causalclustering.catchup.v3.tx.TxPullRequestEncoder;
import com.neo4j.causalclustering.catchup.v3.tx.TxPullResponseDecoder;
import com.neo4j.causalclustering.catchup.v3.tx.TxPullResponseHandler;
import com.neo4j.causalclustering.core.state.snapshot.CoreSnapshotDecoder;
import com.neo4j.causalclustering.core.state.snapshot.CoreSnapshotResponseHandler;
import com.neo4j.causalclustering.protocol.ClientNettyPipelineBuilder;
import com.neo4j.causalclustering.protocol.ModifierProtocolInstaller;
import com.neo4j.causalclustering.protocol.NettyPipelineBuilderFactory;
import com.neo4j.causalclustering.protocol.ProtocolInstaller;
import com.neo4j.causalclustering.protocol.application.ApplicationProtocol;
import com.neo4j.causalclustering.protocol.application.ApplicationProtocols;
import com.neo4j.causalclustering.protocol.modifier.ModifierProtocol;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class CatchupProtocolClientInstaller implements ProtocolInstaller<ProtocolInstaller.Orientation.Client>
{
    private static final ApplicationProtocols APPLICATION_PROTOCOL;

    static
    {
        APPLICATION_PROTOCOL = ApplicationProtocols.CATCHUP_3_0;
    }

    private final List<ModifierProtocolInstaller<ProtocolInstaller.Orientation.Client>> modifiers;
    private final LogProvider logProvider;
    private final Log log;
    private final NettyPipelineBuilderFactory pipelineBuilder;
    private final CatchupResponseHandler handler;

    public CatchupProtocolClientInstaller( NettyPipelineBuilderFactory pipelineBuilder,
            List<ModifierProtocolInstaller<ProtocolInstaller.Orientation.Client>> modifiers, LogProvider logProvider, CatchupResponseHandler handler )
    {
        this.modifiers = modifiers;
        this.logProvider = logProvider;
        this.log = logProvider.getLog( this.getClass() );
        this.pipelineBuilder = pipelineBuilder;
        this.handler = handler;
    }

    public void install( Channel channel )
    {
        CatchupClientProtocol protocol = new CatchupClientProtocol();
        RequestDecoderDispatcher<CatchupClientProtocol.State> decoderDispatcher = new RequestDecoderDispatcher( protocol, this.logProvider );
        decoderDispatcher.register( CatchupClientProtocol.State.STORE_ID, new GetStoreIdResponseDecoder() );
        decoderDispatcher.register( CatchupClientProtocol.State.DATABASE_ID, new GetDatabaseIdResponseDecoder() );
        decoderDispatcher.register( CatchupClientProtocol.State.TX_PULL_RESPONSE, new TxPullResponseDecoder() );
        decoderDispatcher.register( CatchupClientProtocol.State.CORE_SNAPSHOT, new CoreSnapshotDecoder() );
        decoderDispatcher.register( CatchupClientProtocol.State.STORE_COPY_FINISHED, new StoreCopyFinishedResponseDecoder() );
        decoderDispatcher.register( CatchupClientProtocol.State.TX_STREAM_FINISHED, new TxStreamFinishedResponseDecoder() );
        decoderDispatcher.register( CatchupClientProtocol.State.FILE_HEADER, new FileHeaderDecoder() );
        decoderDispatcher.register( CatchupClientProtocol.State.PREPARE_STORE_COPY_RESPONSE, new PrepareStoreCopyResponse.Decoder() );
        decoderDispatcher.register( CatchupClientProtocol.State.FILE_CHUNK, new FileChunkDecoder() );
        decoderDispatcher.register( CatchupClientProtocol.State.ERROR_RESPONSE, new CatchupErrorResponseDecoder() );
        ((ClientNettyPipelineBuilder) ((ClientNettyPipelineBuilder) ((ClientNettyPipelineBuilder) ((ClientNettyPipelineBuilder) ((ClientNettyPipelineBuilder) ((ClientNettyPipelineBuilder) ((ClientNettyPipelineBuilder) ((ClientNettyPipelineBuilder) ((ClientNettyPipelineBuilder) ((ClientNettyPipelineBuilder) ((ClientNettyPipelineBuilder) ((ClientNettyPipelineBuilder) ((ClientNettyPipelineBuilder) ((ClientNettyPipelineBuilder) ((ClientNettyPipelineBuilder) ((ClientNettyPipelineBuilder) ((ClientNettyPipelineBuilder) ((ClientNettyPipelineBuilder) ((ClientNettyPipelineBuilder) ((ClientNettyPipelineBuilder) ((ClientNettyPipelineBuilder) this.pipelineBuilder.client(
                channel, this.log ).modify( this.modifiers )).addFraming().add( "enc_req_tx", new ChannelHandler[]{new TxPullRequestEncoder()} )).add(
                "enc_req_store", new ChannelHandler[]{new GetStoreFileRequestEncoder()} )).add( "enc_req_snapshot",
                new ChannelHandler[]{new CoreSnapshotRequestEncoder()} )).add( "enc_req_store_id", new ChannelHandler[]{new GetStoreIdRequestEncoder()} )).add(
                "enc_req_database_id", new ChannelHandler[]{new GetDatabaseIdRequestEncoder()} )).add( "enc_req_type",
                new ChannelHandler[]{new ResponseMessageTypeEncoder()} )).add( "enc_res_type", new ChannelHandler[]{new RequestMessageTypeEncoder()} )).add(
                "enc_req_precopy", new ChannelHandler[]{new PrepareStoreCopyRequestEncoder()} )).add( "in_res_type",
                new ChannelHandler[]{new ClientMessageTypeHandler( protocol, this.logProvider )} )).add( "dec_dispatch",
                new ChannelHandler[]{decoderDispatcher} )).add( "hnd_res_tx", new ChannelHandler[]{new TxPullResponseHandler( protocol, this.handler )} )).add(
                "hnd_res_snapshot", new ChannelHandler[]{new CoreSnapshotResponseHandler( protocol, this.handler )} )).add( "hnd_res_copy_fin",
                new ChannelHandler[]{new StoreCopyFinishedResponseHandler( protocol, this.handler )} )).add( "hnd_res_tx_fin",
                new ChannelHandler[]{new TxStreamFinishedResponseHandler( protocol, this.handler )} )).add( "hnd_res_file_header",
                new ChannelHandler[]{new FileHeaderHandler( protocol, this.handler )} )).add( "hnd_res_file_chunk",
                new ChannelHandler[]{new FileChunkHandler( protocol, this.handler )} )).add( "hnd_res_store_id",
                new ChannelHandler[]{new GetStoreIdResponseHandler( protocol, this.handler )} )).add( "hnd_res_database_id",
                new ChannelHandler[]{new GetDatabaseIdResponseHandler( protocol, this.handler )} )).add( "hnd_res_store_listing",
                new ChannelHandler[]{new StoreListingResponseHandler( protocol, this.handler )} )).add( "hnd_res_catchup_error",
                new ChannelHandler[]{new CatchupErrorResponseHandler( protocol, this.handler )} )).install();
    }

    public ApplicationProtocol applicationProtocol()
    {
        return APPLICATION_PROTOCOL;
    }

    public Collection<Collection<ModifierProtocol>> modifiers()
    {
        return (Collection) this.modifiers.stream().map( ModifierProtocolInstaller::protocols ).collect( Collectors.toList() );
    }

    public static class Factory extends ProtocolInstaller.Factory<ProtocolInstaller.Orientation.Client,CatchupProtocolClientInstaller>
    {
        public Factory( NettyPipelineBuilderFactory pipelineBuilder, LogProvider logProvider, CatchupResponseHandler handler )
        {
            super( CatchupProtocolClientInstaller.APPLICATION_PROTOCOL, ( modifiers ) -> {
                return new CatchupProtocolClientInstaller( pipelineBuilder, modifiers, logProvider, handler );
            } );
        }
    }
}
