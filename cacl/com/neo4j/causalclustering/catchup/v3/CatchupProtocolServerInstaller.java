package com.neo4j.causalclustering.catchup.v3;

import com.neo4j.causalclustering.catchup.CatchupServerHandler;
import com.neo4j.causalclustering.catchup.CatchupServerProtocol;
import com.neo4j.causalclustering.catchup.RequestDecoderDispatcher;
import com.neo4j.causalclustering.catchup.RequestMessageTypeEncoder;
import com.neo4j.causalclustering.catchup.ResponseMessageTypeEncoder;
import com.neo4j.causalclustering.catchup.ServerMessageTypeHandler;
import com.neo4j.causalclustering.catchup.storecopy.FileChunkEncoder;
import com.neo4j.causalclustering.catchup.storecopy.FileHeaderEncoder;
import com.neo4j.causalclustering.catchup.storecopy.PrepareStoreCopyResponse;
import com.neo4j.causalclustering.catchup.tx.TxStreamFinishedResponseEncoder;
import com.neo4j.causalclustering.catchup.v3.databaseid.GetDatabaseIdRequestDecoder;
import com.neo4j.causalclustering.catchup.v3.databaseid.GetDatabaseIdResponseEncoder;
import com.neo4j.causalclustering.catchup.v3.storecopy.CatchupErrorResponseEncoder;
import com.neo4j.causalclustering.catchup.v3.storecopy.CoreSnapshotRequestDecoder;
import com.neo4j.causalclustering.catchup.v3.storecopy.GetStoreFileRequestDecoder;
import com.neo4j.causalclustering.catchup.v3.storecopy.GetStoreIdRequestDecoder;
import com.neo4j.causalclustering.catchup.v3.storecopy.GetStoreIdResponseEncoder;
import com.neo4j.causalclustering.catchup.v3.storecopy.PrepareStoreCopyRequestDecoder;
import com.neo4j.causalclustering.catchup.v3.storecopy.StoreCopyFinishedResponseEncoder;
import com.neo4j.causalclustering.catchup.v3.tx.TxPullRequestDecoder;
import com.neo4j.causalclustering.catchup.v3.tx.TxPullResponseEncoder;
import com.neo4j.causalclustering.core.state.snapshot.CoreSnapshotEncoder;
import com.neo4j.causalclustering.protocol.ModifierProtocolInstaller;
import com.neo4j.causalclustering.protocol.NettyPipelineBuilderFactory;
import com.neo4j.causalclustering.protocol.ProtocolInstaller;
import com.neo4j.causalclustering.protocol.ServerNettyPipelineBuilder;
import com.neo4j.causalclustering.protocol.application.ApplicationProtocol;
import com.neo4j.causalclustering.protocol.application.ApplicationProtocols;
import com.neo4j.causalclustering.protocol.modifier.ModifierProtocol;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInboundHandler;
import io.netty.handler.stream.ChunkedWriteHandler;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class CatchupProtocolServerInstaller implements ProtocolInstaller<ProtocolInstaller.Orientation.Server>
{
    private static final ApplicationProtocols APPLICATION_PROTOCOL;

    static
    {
        APPLICATION_PROTOCOL = ApplicationProtocols.CATCHUP_3_0;
    }

    private final NettyPipelineBuilderFactory pipelineBuilderFactory;
    private final List<ModifierProtocolInstaller<ProtocolInstaller.Orientation.Server>> modifiers;
    private final Log log;
    private final LogProvider logProvider;
    private final CatchupServerHandler catchupServerHandler;

    public CatchupProtocolServerInstaller( NettyPipelineBuilderFactory pipelineBuilderFactory,
            List<ModifierProtocolInstaller<ProtocolInstaller.Orientation.Server>> modifiers, LogProvider logProvider,
            CatchupServerHandler catchupServerHandler )
    {
        this.pipelineBuilderFactory = pipelineBuilderFactory;
        this.modifiers = modifiers;
        this.log = logProvider.getLog( this.getClass() );
        this.logProvider = logProvider;
        this.catchupServerHandler = catchupServerHandler;
    }

    public void install( Channel channel )
    {
        CatchupServerProtocol state = new CatchupServerProtocol();
        ((ServerNettyPipelineBuilder) ((ServerNettyPipelineBuilder) ((ServerNettyPipelineBuilder) ((ServerNettyPipelineBuilder) ((ServerNettyPipelineBuilder) ((ServerNettyPipelineBuilder) ((ServerNettyPipelineBuilder) ((ServerNettyPipelineBuilder) ((ServerNettyPipelineBuilder) ((ServerNettyPipelineBuilder) ((ServerNettyPipelineBuilder) ((ServerNettyPipelineBuilder) ((ServerNettyPipelineBuilder) ((ServerNettyPipelineBuilder) ((ServerNettyPipelineBuilder) ((ServerNettyPipelineBuilder) ((ServerNettyPipelineBuilder) ((ServerNettyPipelineBuilder) ((ServerNettyPipelineBuilder) ((ServerNettyPipelineBuilder) ((ServerNettyPipelineBuilder) ((ServerNettyPipelineBuilder) this.pipelineBuilderFactory.server(
                channel, this.log ).modify( this.modifiers )).addFraming().add( "enc_req_type", new ChannelHandler[]{new RequestMessageTypeEncoder()} )).add(
                "enc_res_type", new ChannelHandler[]{new ResponseMessageTypeEncoder()} )).add( "enc_res_tx_pull",
                new ChannelHandler[]{new TxPullResponseEncoder()} )).add( "enc_res_store_id", new ChannelHandler[]{new GetStoreIdResponseEncoder()} )).add(
                "enc_res_database_id", new ChannelHandler[]{new GetDatabaseIdResponseEncoder()} )).add( "enc_res_copy_fin",
                new ChannelHandler[]{new StoreCopyFinishedResponseEncoder()} )).add( "enc_res_tx_fin",
                new ChannelHandler[]{new TxStreamFinishedResponseEncoder()} )).add( "enc_res_pre_copy",
                new ChannelHandler[]{new PrepareStoreCopyResponse.Encoder()} )).add( "enc_snapshot", new ChannelHandler[]{new CoreSnapshotEncoder()} )).add(
                "enc_file_chunk", new ChannelHandler[]{new FileChunkEncoder()} )).add( "enc_file_header", new ChannelHandler[]{new FileHeaderEncoder()} )).add(
                "enc_catchup_error", new ChannelHandler[]{new CatchupErrorResponseEncoder()} )).add( "in_req_type",
                new ChannelHandler[]{this.serverMessageHandler( state )} )).add( "dec_req_dispatch", new ChannelHandler[]{this.requestDecoders( state )} )).add(
                "out_chunked_write", new ChannelHandler[]{new ChunkedWriteHandler()} )).add( "hnd_req_database_id",
                new ChannelHandler[]{this.catchupServerHandler.getDatabaseIdRequestHandler( state )} )).add( "hnd_req_tx",
                new ChannelHandler[]{this.catchupServerHandler.txPullRequestHandler( state )} )).add( "hnd_req_store_id",
                new ChannelHandler[]{this.catchupServerHandler.getStoreIdRequestHandler( state )} )).add( "hnd_req_store_listing",
                new ChannelHandler[]{this.catchupServerHandler.storeListingRequestHandler( state )} )).add( "hnd_req_store_file",
                new ChannelHandler[]{this.catchupServerHandler.getStoreFileRequestHandler( state )} )).add( "hnd_req_snapshot",
                new ChannelHandler[]{this.catchupServerHandler.snapshotHandler( state )} )).install();
    }

    private ChannelHandler serverMessageHandler( CatchupServerProtocol state )
    {
        return new ServerMessageTypeHandler( state, this.logProvider );
    }

    private ChannelInboundHandler requestDecoders( CatchupServerProtocol protocol )
    {
        RequestDecoderDispatcher<CatchupServerProtocol.State> decoderDispatcher = new RequestDecoderDispatcher( protocol, this.logProvider );
        decoderDispatcher.register( CatchupServerProtocol.State.GET_DATABASE_ID, new GetDatabaseIdRequestDecoder() );
        decoderDispatcher.register( CatchupServerProtocol.State.TX_PULL, new TxPullRequestDecoder() );
        decoderDispatcher.register( CatchupServerProtocol.State.GET_STORE_ID, new GetStoreIdRequestDecoder() );
        decoderDispatcher.register( CatchupServerProtocol.State.GET_CORE_SNAPSHOT, new CoreSnapshotRequestDecoder() );
        decoderDispatcher.register( CatchupServerProtocol.State.PREPARE_STORE_COPY, new PrepareStoreCopyRequestDecoder() );
        decoderDispatcher.register( CatchupServerProtocol.State.GET_STORE_FILE, new GetStoreFileRequestDecoder() );
        return decoderDispatcher;
    }

    public ApplicationProtocol applicationProtocol()
    {
        return APPLICATION_PROTOCOL;
    }

    public Collection<Collection<ModifierProtocol>> modifiers()
    {
        return (Collection) this.modifiers.stream().map( ModifierProtocolInstaller::protocols ).collect( Collectors.toList() );
    }

    public static class Factory extends ProtocolInstaller.Factory<ProtocolInstaller.Orientation.Server,CatchupProtocolServerInstaller>
    {
        public Factory( NettyPipelineBuilderFactory pipelineBuilderFactory, LogProvider logProvider, CatchupServerHandler catchupServerHandler )
        {
            super( CatchupProtocolServerInstaller.APPLICATION_PROTOCOL, ( modifiers ) -> {
                return new CatchupProtocolServerInstaller( pipelineBuilderFactory, modifiers, logProvider, catchupServerHandler );
            } );
        }
    }
}
