package com.neo4j.causalclustering.catchup.v3.databaseid;

import com.neo4j.causalclustering.catchup.RequestMessageType;
import com.neo4j.causalclustering.messaging.CatchupProtocolMessage;

import java.util.Objects;

public class GetDatabaseIdRequest extends CatchupProtocolMessage
{
    private final String databaseName;

    public GetDatabaseIdRequest( String databaseName )
    {
        super( RequestMessageType.DATABASE_ID );
        this.databaseName = databaseName;
    }

    public String databaseName()
    {
        return this.databaseName;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            GetDatabaseIdRequest that = (GetDatabaseIdRequest) o;
            return this.type == that.type && Objects.equals( this.databaseName, that.databaseName );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.type, this.databaseName} );
    }
}
