package com.neo4j.causalclustering.catchup.v3.databaseid;

import com.neo4j.causalclustering.discovery.akka.marshal.DatabaseIdWithoutNameMarshal;
import com.neo4j.causalclustering.messaging.NetworkReadableChannel;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

import org.neo4j.kernel.database.DatabaseId;

public class GetDatabaseIdResponseDecoder extends ByteToMessageDecoder
{
    protected void decode( ChannelHandlerContext ctx, ByteBuf msg, List<Object> out ) throws Exception
    {
        DatabaseId databaseIdRaw = (DatabaseId) DatabaseIdWithoutNameMarshal.INSTANCE.unmarshal( new NetworkReadableChannel( msg ) );
        out.add( new GetDatabaseIdResponse( databaseIdRaw ) );
    }
}
