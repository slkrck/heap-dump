package com.neo4j.causalclustering.catchup.v3.databaseid;

import com.neo4j.causalclustering.catchup.CatchupClientProtocol;
import com.neo4j.causalclustering.catchup.CatchupResponseHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class GetDatabaseIdResponseHandler extends SimpleChannelInboundHandler<GetDatabaseIdResponse>
{
    private final CatchupResponseHandler handler;
    private final CatchupClientProtocol protocol;

    public GetDatabaseIdResponseHandler( CatchupClientProtocol protocol, CatchupResponseHandler handler )
    {
        this.protocol = protocol;
        this.handler = handler;
    }

    protected void channelRead0( ChannelHandlerContext ctx, GetDatabaseIdResponse msg )
    {
        this.handler.onGetDatabaseIdResponse( msg );
        this.protocol.expect( CatchupClientProtocol.State.MESSAGE_TYPE );
    }
}
