package com.neo4j.causalclustering.catchup.v3.databaseid;

import java.util.Objects;

import org.neo4j.kernel.database.DatabaseId;

public class GetDatabaseIdResponse
{
    private final DatabaseId databaseIdRaw;

    public GetDatabaseIdResponse( DatabaseId databaseIdRaw )
    {
        this.databaseIdRaw = databaseIdRaw;
    }

    public DatabaseId databaseId()
    {
        return this.databaseIdRaw;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            GetDatabaseIdResponse that = (GetDatabaseIdResponse) o;
            return Objects.equals( this.databaseIdRaw, that.databaseIdRaw );
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{this.databaseIdRaw} );
    }

    public String toString()
    {
        return "GetDatabaseIdResponse{databaseId=" + this.databaseIdRaw + "}";
    }
}
