package com.neo4j.causalclustering.catchup.v3.databaseid;

import com.neo4j.causalclustering.messaging.marshalling.StringMarshal;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

public class GetDatabaseIdRequestDecoder extends ByteToMessageDecoder
{
    protected void decode( ChannelHandlerContext ctx, ByteBuf in, List<Object> out ) throws Exception
    {
        String databaseName = StringMarshal.unmarshal( in );
        out.add( new GetDatabaseIdRequest( databaseName ) );
    }
}
