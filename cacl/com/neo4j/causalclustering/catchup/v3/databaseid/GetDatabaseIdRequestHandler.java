package com.neo4j.causalclustering.catchup.v3.databaseid;

import com.neo4j.causalclustering.catchup.CatchupErrorResponse;
import com.neo4j.causalclustering.catchup.CatchupResult;
import com.neo4j.causalclustering.catchup.CatchupServerProtocol;
import com.neo4j.causalclustering.catchup.ResponseMessageType;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.Optional;

import org.neo4j.kernel.database.DatabaseIdRepository;
import org.neo4j.kernel.database.NamedDatabaseId;

public class GetDatabaseIdRequestHandler extends SimpleChannelInboundHandler<GetDatabaseIdRequest>
{
    private final CatchupServerProtocol protocol;
    private final DatabaseIdRepository databaseIdRepository;

    public GetDatabaseIdRequestHandler( DatabaseIdRepository databaseIdRepository, CatchupServerProtocol protocol )
    {
        this.protocol = protocol;
        this.databaseIdRepository = databaseIdRepository;
    }

    private static CatchupErrorResponse unknownDatabaseResponse( String databaseName )
    {
        return new CatchupErrorResponse( CatchupResult.E_DATABASE_UNKNOWN, "Database '" + databaseName + "' does not exist" );
    }

    protected void channelRead0( ChannelHandlerContext ctx, GetDatabaseIdRequest msg )
    {
        String databaseName = msg.databaseName();
        Optional<NamedDatabaseId> databaseIdOptional = this.databaseIdRepository.getByName( databaseName );
        if ( databaseIdOptional.isPresent() )
        {
            ctx.write( ResponseMessageType.DATABASE_ID_RESPONSE );
            ctx.writeAndFlush( ((NamedDatabaseId) databaseIdOptional.get()).databaseId() );
        }
        else
        {
            ctx.write( ResponseMessageType.ERROR );
            ctx.writeAndFlush( unknownDatabaseResponse( databaseName ) );
        }

        this.protocol.expect( CatchupServerProtocol.State.MESSAGE_TYPE );
    }
}
