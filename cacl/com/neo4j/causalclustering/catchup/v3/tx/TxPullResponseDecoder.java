package com.neo4j.causalclustering.catchup.v3.tx;

import com.neo4j.causalclustering.catchup.tx.TxPullResponse;
import com.neo4j.causalclustering.messaging.NetworkReadableChannel;
import com.neo4j.causalclustering.messaging.ReadableNetworkChannelDelegator;
import com.neo4j.causalclustering.messaging.marshalling.storeid.StoreIdMarshal;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

import org.neo4j.kernel.impl.transaction.CommittedTransactionRepresentation;
import org.neo4j.kernel.impl.transaction.log.PhysicalTransactionCursor;
import org.neo4j.kernel.impl.transaction.log.ServiceLoadingCommandReaderFactory;
import org.neo4j.kernel.impl.transaction.log.entry.LogEntryReader;
import org.neo4j.kernel.impl.transaction.log.entry.VersionAwareLogEntryReader;
import org.neo4j.storageengine.api.StoreId;

public class TxPullResponseDecoder extends ByteToMessageDecoder
{
    private static final ServiceLoadingCommandReaderFactory commandReaderFactory = new ServiceLoadingCommandReaderFactory();
    private PhysicalTransactionCursor transactionCursor;
    private TxPullResponseDecoder.NextTxInfo nextTxInfo;
    private StoreId storeId;
    private LogEntryReader reader;
    private ReadableNetworkChannelDelegator delegatingChannel;

    public TxPullResponseDecoder()
    {
        this.reader = new VersionAwareLogEntryReader( commandReaderFactory );
        this.delegatingChannel = new ReadableNetworkChannelDelegator();
        this.setCumulator( COMPOSITE_CUMULATOR );
    }

    protected void decode( ChannelHandlerContext ctx, ByteBuf in, List<Object> out ) throws Exception
    {
        this.delegatingChannel.delegateTo( new NetworkReadableChannel( in ) );
        if ( this.isFirstChunk() )
        {
            this.storeId = (StoreId) StoreIdMarshal.INSTANCE.unmarshal( this.delegatingChannel );
            this.transactionCursor = new PhysicalTransactionCursor( this.delegatingChannel, this.reader );
            this.nextTxInfo = new TxPullResponseDecoder.NextTxInfo();
        }

        while ( this.nextTxInfo.canReadNextTx( in ) )
        {
            this.transactionCursor.next();
            CommittedTransactionRepresentation tx = this.transactionCursor.get();
            out.add( new TxPullResponse( this.storeId, tx ) );
            this.nextTxInfo.update( in );
        }

        if ( this.nextTxInfo.noMoreTx() )
        {
            this.transactionCursor.close();
            this.transactionCursor = null;
            this.nextTxInfo = null;
            out.add( TxPullResponse.EMPTY );
        }
    }

    private boolean isFirstChunk()
    {
        return this.transactionCursor == null;
    }

    private static class NextTxInfo
    {
        private boolean unknown = true;
        private int nextSize;

        boolean canReadNextTx( ByteBuf byteBuf )
        {
            if ( this.unknown )
            {
                this.update( byteBuf );
            }

            return !this.unknown && this.nextSize < byteBuf.readableBytes();
        }

        void update( ByteBuf byteBuf )
        {
            this.unknown = !byteBuf.isReadable();
            this.nextSize = this.unknown ? 0 : byteBuf.readInt();
        }

        boolean noMoreTx()
        {
            return !this.unknown && this.nextSize == 0;
        }
    }
}
