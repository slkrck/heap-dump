package com.neo4j.causalclustering.catchup.v3.tx;

import com.neo4j.causalclustering.catchup.tx.TxPullResponse;
import com.neo4j.causalclustering.helper.ErrorHandler;
import com.neo4j.causalclustering.messaging.ChunkingNetworkChannel;
import com.neo4j.causalclustering.messaging.marshalling.storeid.StoreIdMarshal;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPromise;
import io.netty.util.ReferenceCountUtil;

import java.io.IOException;
import java.util.LinkedList;

import org.neo4j.io.ByteUnit;
import org.neo4j.kernel.impl.transaction.log.entry.LogEntryWriter;
import org.neo4j.storageengine.api.StoreId;
import org.neo4j.util.VisibleForTesting;

public class TxPullResponseEncoder extends ChannelOutboundHandlerAdapter
{
    private static final int DEFAULT_CHUNK_SIZE = (int) ByteUnit.mebiBytes( 1L );
    private static final int PLACEHOLDER_TX_INFO = -1;
    private final LinkedList<ByteBuf> pendingChunks = new LinkedList();
    private final int chunkSize;
    private ChunkingNetworkChannel channel;

    @VisibleForTesting
    TxPullResponseEncoder( int chunkSize )
    {
        this.chunkSize = chunkSize;
    }

    public TxPullResponseEncoder()
    {
        this.chunkSize = DEFAULT_CHUNK_SIZE;
    }

    public void write( ChannelHandlerContext ctx, Object msg, ChannelPromise promise ) throws Exception
    {
        if ( msg instanceof TxPullResponse )
        {
            try
            {
                this.writeResponse( ctx, (TxPullResponse) msg, promise );
            }
            catch ( Exception var10 )
            {
                Exception e = var10;
                if ( this.pendingChunks.isEmpty() )
                {
                    throw var10;
                }

                ErrorHandler errorHandler = new ErrorHandler( "Release buffers" );

                try
                {
                    errorHandler.add( e );
                    this.pendingChunks.forEach( ( byteBuf ) -> {
                        errorHandler.execute( () -> {
                            ReferenceCountUtil.release( byteBuf );
                        } );
                    } );
                    this.pendingChunks.clear();
                }
                catch ( Throwable var9 )
                {
                    try
                    {
                        errorHandler.close();
                    }
                    catch ( Throwable var8 )
                    {
                        var9.addSuppressed( var8 );
                    }

                    throw var9;
                }

                errorHandler.close();
            }
        }
        else
        {
            super.write( ctx, msg, promise );
        }
    }

    private void writeResponse( ChannelHandlerContext ctx, TxPullResponse msg, ChannelPromise promise ) throws IOException
    {
        assert this.pendingChunks.isEmpty();

        if ( this.isFirstTx() )
        {
            this.handleFirstTx( ctx, msg );
        }

        int metadataIndex = this.channel.currentIndex();
        this.channel.putInt( -1 );
        if ( this.isEndOfTxStream( msg ) )
        {
            this.channel.close();
            this.channel = null;
        }
        else
        {
            (new LogEntryWriter( this.channel )).serialize( msg.tx() );
        }

        if ( !this.pendingChunks.isEmpty() )
        {
            this.replacePlaceholderSize( metadataIndex );
        }

        if ( this.channel != null && this.channel.currentIndex() + 4 > this.chunkSize )
        {
            this.channel.flush();
        }

        this.writePendingChunks( ctx, promise );
    }

    private void writePendingChunks( ChannelHandlerContext ctx, ChannelPromise promise )
    {
        if ( this.pendingChunks.isEmpty() )
        {
            promise.setSuccess();
        }
        else
        {
            do
            {
                ByteBuf nextChunk = (ByteBuf) this.pendingChunks.poll();
                ChannelPromise nextPromise = this.pendingChunks.isEmpty() ? promise : ctx.voidPromise();
                ctx.write( nextChunk, nextPromise );
            }
            while ( !this.pendingChunks.isEmpty() );
        }
    }

    private void handleFirstTx( ChannelHandlerContext ctx, TxPullResponse msg ) throws IOException
    {
        if ( this.isEndOfTxStream( msg ) )
        {
            throw new IllegalArgumentException( "Requires at least one transaction." );
        }
        else
        {
            this.channel = new ChunkingNetworkChannel( ctx.alloc(), this.chunkSize, this.pendingChunks );
            StoreIdMarshal.INSTANCE.marshal( (StoreId) msg.storeId(), this.channel );
        }
    }

    private void replacePlaceholderSize( int metadataIndex )
    {
        ((ByteBuf) this.pendingChunks.peek()).setInt( metadataIndex, this.calculateTxSize( metadataIndex + 4 ) );
    }

    private int calculateTxSize( int txStartIndex )
    {
        return (this.channel != null ? this.channel.currentIndex() : 0) + this.pendingChunks.stream().mapToInt( ByteBuf::readableBytes ).sum() - txStartIndex;
    }

    private boolean isEndOfTxStream( TxPullResponse msg )
    {
        return msg.equals( TxPullResponse.EMPTY );
    }

    private boolean isFirstTx()
    {
        return this.channel == null;
    }
}
