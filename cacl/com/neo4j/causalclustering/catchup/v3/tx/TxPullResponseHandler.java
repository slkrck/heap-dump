package com.neo4j.causalclustering.catchup.v3.tx;

import com.neo4j.causalclustering.catchup.CatchupClientProtocol;
import com.neo4j.causalclustering.catchup.CatchupResponseHandler;
import com.neo4j.causalclustering.catchup.tx.TxPullResponse;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class TxPullResponseHandler extends SimpleChannelInboundHandler<TxPullResponse>
{
    private final CatchupClientProtocol protocol;
    private final CatchupResponseHandler handler;

    public TxPullResponseHandler( CatchupClientProtocol protocol, CatchupResponseHandler handler )
    {
        this.protocol = protocol;
        this.handler = handler;
    }

    protected void channelRead0( ChannelHandlerContext ctx, TxPullResponse msg )
    {
        if ( this.protocol.isExpecting( CatchupClientProtocol.State.TX_PULL_RESPONSE ) )
        {
            if ( msg.equals( TxPullResponse.EMPTY ) )
            {
                this.protocol.expect( CatchupClientProtocol.State.MESSAGE_TYPE );
            }
            else
            {
                this.handler.onTxPullResponse( msg );
            }
        }
        else
        {
            ctx.fireChannelRead( msg );
        }
    }
}
