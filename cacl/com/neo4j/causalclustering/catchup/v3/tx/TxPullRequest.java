package com.neo4j.causalclustering.catchup.v3.tx;

import com.neo4j.causalclustering.catchup.RequestMessageType;
import com.neo4j.causalclustering.messaging.CatchupProtocolMessage;

import java.util.Objects;

import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.storageengine.api.StoreId;

public class TxPullRequest extends CatchupProtocolMessage.WithDatabaseId
{
    private final long previousTxId;
    private final StoreId expectedStoreId;

    public TxPullRequest( long previousTxId, StoreId expectedStoreId, DatabaseId databaseId )
    {
        super( RequestMessageType.TX_PULL_REQUEST, databaseId );
        if ( previousTxId < 1L )
        {
            throw new IllegalArgumentException( "Cannot request transaction from " + previousTxId );
        }
        else
        {
            this.previousTxId = previousTxId;
            this.expectedStoreId = expectedStoreId;
        }
    }

    public long previousTxId()
    {
        return this.previousTxId;
    }

    public StoreId expectedStoreId()
    {
        return this.expectedStoreId;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            if ( !super.equals( o ) )
            {
                return false;
            }
            else
            {
                TxPullRequest that = (TxPullRequest) o;
                return this.previousTxId == that.previousTxId && Objects.equals( this.expectedStoreId, that.expectedStoreId );
            }
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{super.hashCode(), this.previousTxId, this.expectedStoreId} );
    }

    public String toString()
    {
        long var10000 = this.previousTxId;
        return "TxPullRequest{previousTxId=" + var10000 + ", expectedStoreId=" + this.expectedStoreId + ", databaseId='" + this.databaseId() + "'}";
    }
}
