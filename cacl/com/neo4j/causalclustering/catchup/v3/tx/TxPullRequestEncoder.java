package com.neo4j.causalclustering.catchup.v3.tx;

import com.neo4j.causalclustering.discovery.akka.marshal.DatabaseIdWithoutNameMarshal;
import com.neo4j.causalclustering.messaging.NetworkWritableChannel;
import com.neo4j.causalclustering.messaging.marshalling.storeid.StoreIdMarshal;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.storageengine.api.StoreId;

public class TxPullRequestEncoder extends MessageToByteEncoder<TxPullRequest>
{
    protected void encode( ChannelHandlerContext ctx, TxPullRequest request, ByteBuf out ) throws Exception
    {
        NetworkWritableChannel channel = new NetworkWritableChannel( out );
        DatabaseIdWithoutNameMarshal.INSTANCE.marshal( (DatabaseId) request.databaseId(), channel );
        channel.putLong( request.previousTxId() );
        StoreIdMarshal.INSTANCE.marshal( (StoreId) request.expectedStoreId(), channel );
    }
}
