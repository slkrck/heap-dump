package com.neo4j.causalclustering.catchup.v3.storecopy;

import com.neo4j.causalclustering.core.state.snapshot.CoreSnapshotRequest;
import com.neo4j.causalclustering.discovery.akka.marshal.DatabaseIdWithoutNameMarshal;
import com.neo4j.causalclustering.messaging.EndOfStreamException;
import com.neo4j.causalclustering.messaging.NetworkReadableChannel;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.io.IOException;
import java.util.List;

import org.neo4j.kernel.database.DatabaseId;

public class CoreSnapshotRequestDecoder extends ByteToMessageDecoder
{
    protected void decode( ChannelHandlerContext ctx, ByteBuf in, List<Object> out ) throws IOException, EndOfStreamException
    {
        DatabaseId databaseId = (DatabaseId) DatabaseIdWithoutNameMarshal.INSTANCE.unmarshal( new NetworkReadableChannel( in ) );
        out.add( new CoreSnapshotRequest( databaseId ) );
    }
}
