package com.neo4j.causalclustering.catchup.v3.storecopy;

import com.neo4j.causalclustering.catchup.CatchupServerProtocol;
import com.neo4j.causalclustering.catchup.ResponseMessageType;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.neo4j.kernel.database.Database;

public class GetStoreIdRequestHandler extends SimpleChannelInboundHandler<GetStoreIdRequest>
{
    private final CatchupServerProtocol protocol;
    private final Database db;

    public GetStoreIdRequestHandler( CatchupServerProtocol protocol, Database db )
    {
        this.protocol = protocol;
        this.db = db;
    }

    protected void channelRead0( ChannelHandlerContext ctx, GetStoreIdRequest msg )
    {
        ctx.writeAndFlush( ResponseMessageType.STORE_ID );
        ctx.writeAndFlush( this.db.getStoreId() );
        this.protocol.expect( CatchupServerProtocol.State.MESSAGE_TYPE );
    }
}
