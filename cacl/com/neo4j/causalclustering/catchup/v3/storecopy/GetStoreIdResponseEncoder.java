package com.neo4j.causalclustering.catchup.v3.storecopy;

import com.neo4j.causalclustering.messaging.NetworkWritableChannel;
import com.neo4j.causalclustering.messaging.marshalling.storeid.StoreIdMarshal;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import org.neo4j.storageengine.api.StoreId;

public class GetStoreIdResponseEncoder extends MessageToByteEncoder<StoreId>
{
    protected void encode( ChannelHandlerContext ctx, StoreId storeId, ByteBuf out ) throws Exception
    {
        StoreIdMarshal.INSTANCE.marshal( (StoreId) storeId, new NetworkWritableChannel( out ) );
    }
}
