package com.neo4j.causalclustering.catchup.v3.storecopy;

import com.neo4j.causalclustering.catchup.RequestMessageType;
import com.neo4j.causalclustering.messaging.StoreCopyRequest;

import java.io.File;
import java.util.Objects;

import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.storageengine.api.StoreId;

public class GetStoreFileRequest extends StoreCopyRequest
{
    private final File file;

    public GetStoreFileRequest( StoreId expectedStoreId, File file, long requiredTransactionId, DatabaseId databaseId )
    {
        super( RequestMessageType.STORE_FILE, databaseId, expectedStoreId, requiredTransactionId );
        this.file = file;
    }

    public File file()
    {
        return this.file;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            if ( !super.equals( o ) )
            {
                return false;
            }
            else
            {
                GetStoreFileRequest that = (GetStoreFileRequest) o;
                return Objects.equals( this.file, that.file );
            }
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{super.hashCode(), this.file} );
    }

    public String toString()
    {
        StoreId var10000 = this.expectedStoreId();
        return "GetStoreFileRequest{expectedStoreId=" + var10000 + ", file=" + this.file.getName() + ", requiredTransactionId=" + this.requiredTransactionId() +
                ", databaseId=" + this.databaseId() + "}";
    }
}
