package com.neo4j.causalclustering.catchup.v3.storecopy;

import com.neo4j.causalclustering.discovery.akka.marshal.DatabaseIdWithoutNameMarshal;
import com.neo4j.causalclustering.messaging.NetworkWritableChannel;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import org.neo4j.kernel.database.DatabaseId;

public class GetStoreIdRequestEncoder extends MessageToByteEncoder<GetStoreIdRequest>
{
    protected void encode( ChannelHandlerContext ctx, GetStoreIdRequest request, ByteBuf out ) throws Exception
    {
        DatabaseIdWithoutNameMarshal.INSTANCE.marshal( (DatabaseId) request.databaseId(), new NetworkWritableChannel( out ) );
    }
}
