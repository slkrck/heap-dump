package com.neo4j.causalclustering.catchup.v3.storecopy;

import com.neo4j.causalclustering.catchup.CatchupClientProtocol;
import com.neo4j.causalclustering.catchup.CatchupErrorResponse;
import com.neo4j.causalclustering.catchup.CatchupResponseHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class CatchupErrorResponseHandler extends SimpleChannelInboundHandler<CatchupErrorResponse>
{
    private final CatchupClientProtocol protocol;
    private final CatchupResponseHandler handler;

    public CatchupErrorResponseHandler( CatchupClientProtocol protocol, CatchupResponseHandler handler )
    {
        this.protocol = protocol;
        this.handler = handler;
    }

    protected void channelRead0( ChannelHandlerContext ctx, CatchupErrorResponse msg )
    {
        this.handler.onCatchupErrorResponse( msg );
        this.protocol.expect( CatchupClientProtocol.State.MESSAGE_TYPE );
    }
}
