package com.neo4j.causalclustering.catchup.v3.storecopy;

import com.neo4j.causalclustering.catchup.RequestMessageType;
import com.neo4j.causalclustering.messaging.CatchupProtocolMessage;

import java.util.Objects;

import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.storageengine.api.StoreId;

public class PrepareStoreCopyRequest extends CatchupProtocolMessage.WithDatabaseId
{
    private final StoreId storeId;

    public PrepareStoreCopyRequest( StoreId expectedStoreId, DatabaseId databaseId )
    {
        super( RequestMessageType.PREPARE_STORE_COPY, databaseId );
        this.storeId = expectedStoreId;
    }

    public StoreId storeId()
    {
        return this.storeId;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            if ( !super.equals( o ) )
            {
                return false;
            }
            else
            {
                PrepareStoreCopyRequest that = (PrepareStoreCopyRequest) o;
                return Objects.equals( this.storeId, that.storeId );
            }
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{super.hashCode(), this.storeId} );
    }

    public String toString()
    {
        StoreId var10000 = this.storeId;
        return "PrepareStoreCopyRequest{storeId=" + var10000 + ", databaseId='" + this.databaseId() + "}";
    }
}
