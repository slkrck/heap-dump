package com.neo4j.causalclustering.catchup.v3.storecopy;

import com.neo4j.causalclustering.core.state.snapshot.CoreSnapshotRequest;
import com.neo4j.causalclustering.discovery.akka.marshal.DatabaseIdWithoutNameMarshal;
import com.neo4j.causalclustering.messaging.NetworkWritableChannel;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

import java.io.IOException;

import org.neo4j.kernel.database.DatabaseId;

public class CoreSnapshotRequestEncoder extends MessageToByteEncoder<CoreSnapshotRequest>
{
    protected void encode( ChannelHandlerContext ctx, CoreSnapshotRequest msg, ByteBuf out ) throws IOException
    {
        DatabaseIdWithoutNameMarshal.INSTANCE.marshal( (DatabaseId) msg.databaseId(), new NetworkWritableChannel( out ) );
    }
}
