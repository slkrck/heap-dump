package com.neo4j.causalclustering.catchup.v3.storecopy;

import com.neo4j.causalclustering.discovery.akka.marshal.DatabaseIdWithoutNameMarshal;
import com.neo4j.causalclustering.messaging.NetworkWritableChannel;
import com.neo4j.causalclustering.messaging.marshalling.StringMarshal;
import com.neo4j.causalclustering.messaging.marshalling.storeid.StoreIdMarshal;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.storageengine.api.StoreId;

public class GetStoreFileRequestEncoder extends MessageToByteEncoder<GetStoreFileRequest>
{
    protected void encode( ChannelHandlerContext ctx, GetStoreFileRequest msg, ByteBuf out ) throws Exception
    {
        NetworkWritableChannel channel = new NetworkWritableChannel( out );
        DatabaseIdWithoutNameMarshal.INSTANCE.marshal( (DatabaseId) msg.databaseId(), channel );
        StoreIdMarshal.INSTANCE.marshal( (StoreId) msg.expectedStoreId(), channel );
        out.writeLong( msg.requiredTransactionId() );
        String name = msg.file().getName();
        StringMarshal.marshal( out, name );
    }
}
