package com.neo4j.causalclustering.catchup.v3.storecopy;

import com.neo4j.causalclustering.catchup.storecopy.StoreCopyFinishedResponse;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

public class StoreCopyFinishedResponseDecoder extends ByteToMessageDecoder
{
    protected void decode( ChannelHandlerContext ctx, ByteBuf msg, List<Object> out )
    {
        int statusOrdinal = msg.readInt();
        long lastCheckpointedTx = msg.readLong();
        out.add( new StoreCopyFinishedResponse( StoreCopyFinishedResponse.Status.values()[statusOrdinal], lastCheckpointedTx ) );
    }
}
