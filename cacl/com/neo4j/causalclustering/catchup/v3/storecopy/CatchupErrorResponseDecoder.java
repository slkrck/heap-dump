package com.neo4j.causalclustering.catchup.v3.storecopy;

import com.neo4j.causalclustering.catchup.CatchupErrorResponse;
import com.neo4j.causalclustering.catchup.CatchupResult;
import com.neo4j.causalclustering.messaging.marshalling.StringMarshal;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

public class CatchupErrorResponseDecoder extends ByteToMessageDecoder
{
    protected void decode( ChannelHandlerContext ctx, ByteBuf in, List<Object> out )
    {
        int statusOrdinal = in.readInt();
        CatchupResult result = CatchupResult.values()[statusOrdinal];
        String message = StringMarshal.unmarshal( in );
        out.add( new CatchupErrorResponse( result, message ) );
    }
}
