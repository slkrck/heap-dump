package com.neo4j.causalclustering.logging;

import com.neo4j.causalclustering.core.consensus.RaftMessages;

public interface RaftMessageLogger<MEMBER>
{
    void logOutbound( MEMBER var1, RaftMessages.RaftMessage var2, MEMBER var3 );

    void logInbound( MEMBER var1, RaftMessages.RaftMessage var2, MEMBER var3 );
}
