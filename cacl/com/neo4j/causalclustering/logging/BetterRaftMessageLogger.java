package com.neo4j.causalclustering.logging;

import com.neo4j.causalclustering.core.consensus.RaftMessages;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.Clock;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.neo4j.io.IOUtils;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.util.Preconditions;
import org.neo4j.util.VisibleForTesting;

public class BetterRaftMessageLogger<MEMBER> extends LifecycleAdapter implements RaftMessageLogger<MEMBER>
{
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern( "yyyy-MM-dd HH:mm:ss.SSSZ" );
    private final MEMBER me;
    private final File logFile;
    private final FileSystemAbstraction fs;
    private final Clock clock;
    private final ReadWriteLock lifecycleLock = new ReentrantReadWriteLock();
    private PrintWriter printWriter;

    public BetterRaftMessageLogger( MEMBER me, File logFile, FileSystemAbstraction fs, Clock clock )
    {
        this.me = me;
        this.logFile = logFile;
        this.fs = fs;
        this.clock = clock;
    }

    private static String nullSafeMessageType( RaftMessages.RaftMessage message )
    {
        return Objects.isNull( message ) ? "null" : message.type().toString();
    }

    public void start() throws IOException
    {
        this.lifecycleLock.writeLock().lock();

        try
        {
            Preconditions.checkState( this.printWriter == null, "Already started" );
            this.printWriter = this.openPrintWriter();
            this.log( this.me, BetterRaftMessageLogger.Direction.INFO, this.me, "Info", "I am " + this.me );
        }
        finally
        {
            this.lifecycleLock.writeLock().unlock();
        }
    }

    public void stop()
    {
        this.lifecycleLock.writeLock().lock();

        try
        {
            IOUtils.closeAllSilently( new PrintWriter[]{this.printWriter} );
            this.printWriter = null;
        }
        finally
        {
            this.lifecycleLock.writeLock().unlock();
        }
    }

    public void logOutbound( MEMBER me, RaftMessages.RaftMessage message, MEMBER remote )
    {
        this.log( me, BetterRaftMessageLogger.Direction.OUTBOUND, remote, nullSafeMessageType( message ), String.valueOf( message ) );
    }

    public void logInbound( MEMBER me, RaftMessages.RaftMessage message, MEMBER remote )
    {
        this.log( me, BetterRaftMessageLogger.Direction.INBOUND, remote, nullSafeMessageType( message ), String.valueOf( message ) );
    }

    @VisibleForTesting
    protected PrintWriter openPrintWriter() throws IOException
    {
        this.fs.mkdirs( this.logFile.getParentFile() );
        return new PrintWriter( this.fs.openAsOutputStream( this.logFile, true ) );
    }

    private void log( MEMBER me, BetterRaftMessageLogger.Direction direction, MEMBER remote, String type, String message )
    {
        this.lifecycleLock.readLock().lock();

        try
        {
            if ( this.printWriter != null )
            {
                String timestamp = ZonedDateTime.now( this.clock ).format( DATE_TIME_FORMATTER );
                this.printWriter.println( String.format( "%s %s %s %s %s \"%s\"", timestamp, me, direction.arrow, remote, type, message ) );
                this.printWriter.flush();
            }
        }
        finally
        {
            this.lifecycleLock.readLock().unlock();
        }
    }

    private static enum Direction
    {
        INFO( "---" ),
        OUTBOUND( "-->" ),
        INBOUND( "<--" );

        public final String arrow;

        private Direction( String arrow )
        {
            this.arrow = arrow;
        }
    }
}
