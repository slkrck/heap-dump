package com.neo4j.causalclustering.logging;

import com.neo4j.causalclustering.core.consensus.RaftMessages;

public class NullRaftMessageLogger<MEMBER> implements RaftMessageLogger<MEMBER>
{
    public void logOutbound( MEMBER me, RaftMessages.RaftMessage message, MEMBER remote )
    {
    }

    public void logInbound( MEMBER remote, RaftMessages.RaftMessage message, MEMBER me )
    {
    }
}
