package com.neo4j.causalclustering.readreplica;

import com.neo4j.causalclustering.catchup.storecopy.StoreFiles;
import com.neo4j.dbms.ClusterInternalDbmsOperator;

import java.io.IOException;

import org.neo4j.collection.Dependencies;
import org.neo4j.kernel.database.Database;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.impl.transaction.log.files.LogFiles;
import org.neo4j.logging.Log;
import org.neo4j.monitoring.Monitors;
import org.neo4j.storageengine.api.StoreId;

public class ReadReplicaDatabaseContext
{
    private final Database kernelDatabase;
    private final Monitors monitors;
    private final Dependencies dependencies;
    private final StoreFiles storeFiles;
    private final LogFiles transactionLogs;
    private final Log log;
    private final ClusterInternalDbmsOperator internalOperator;

    ReadReplicaDatabaseContext( Database kernelDatabase, Monitors monitors, Dependencies dependencies, StoreFiles storeFiles, LogFiles transactionLogs,
            ClusterInternalDbmsOperator internalOperator )
    {
        this.kernelDatabase = kernelDatabase;
        this.monitors = monitors;
        this.dependencies = dependencies;
        this.storeFiles = storeFiles;
        this.transactionLogs = transactionLogs;
        this.log = kernelDatabase.getInternalLogProvider().getLog( this.getClass() );
        this.internalOperator = internalOperator;
    }

    public NamedDatabaseId databaseId()
    {
        return this.kernelDatabase.getNamedDatabaseId();
    }

    public StoreId storeId()
    {
        return this.readStoreIdFromDisk();
    }

    private StoreId readStoreIdFromDisk()
    {
        try
        {
            return this.storeFiles.readStoreId( this.kernelDatabase.getDatabaseLayout() );
        }
        catch ( IOException var2 )
        {
            this.log.warn( "Failure reading store id", var2 );
            return null;
        }
    }

    ClusterInternalDbmsOperator.StoreCopyHandle stopForStoreCopy()
    {
        return this.internalOperator.stopForStoreCopy( this.kernelDatabase.getNamedDatabaseId() );
    }

    public Database kernelDatabase()
    {
        return this.kernelDatabase;
    }

    public Monitors monitors()
    {
        return this.monitors;
    }

    public Dependencies dependencies()
    {
        return this.dependencies;
    }

    public boolean isEmpty()
    {
        return this.storeFiles.isEmpty( this.kernelDatabase.getDatabaseLayout() );
    }

    public void delete() throws IOException
    {
        this.storeFiles.delete( this.kernelDatabase.getDatabaseLayout(), this.transactionLogs );
    }
}
