package com.neo4j.causalclustering.readreplica;

import com.neo4j.causalclustering.catchup.tx.PullRequestMonitor;
import com.neo4j.causalclustering.core.state.machines.CommandIndexTracker;
import com.neo4j.causalclustering.core.state.machines.tx.LogIndexTxHeaderEncoding;
import com.neo4j.dbms.ReplicatedDatabaseEventService;

import java.util.Objects;
import java.util.function.Supplier;

import org.neo4j.io.pagecache.tracing.cursor.PageCursorTracer;
import org.neo4j.io.pagecache.tracing.cursor.PageCursorTracerSupplier;
import org.neo4j.io.pagecache.tracing.cursor.context.VersionContextSupplier;
import org.neo4j.kernel.impl.api.TransactionCommitProcess;
import org.neo4j.kernel.impl.api.TransactionQueue;
import org.neo4j.kernel.impl.api.TransactionToApply;
import org.neo4j.kernel.impl.transaction.CommittedTransactionRepresentation;
import org.neo4j.kernel.impl.transaction.tracing.CommitEvent;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.monitoring.Monitors;
import org.neo4j.storageengine.api.TransactionApplicationMode;
import org.neo4j.storageengine.api.TransactionIdStore;

public class BatchingTxApplier extends LifecycleAdapter
{
    private final int maxBatchSize;
    private final Supplier<TransactionIdStore> txIdStoreSupplier;
    private final Supplier<TransactionCommitProcess> commitProcessSupplier;
    private final VersionContextSupplier versionContextSupplier;
    private final PullRequestMonitor monitor;
    private final PageCursorTracerSupplier pageCursorTracerSupplier;
    private final CommandIndexTracker commandIndexTracker;
    private final Log log;
    private final ReplicatedDatabaseEventService.ReplicatedDatabaseEventDispatch databaseEventDispatch;
    private TransactionQueue txQueue;
    private TransactionCommitProcess commitProcess;
    private volatile long lastQueuedTxId;
    private volatile boolean stopped;

    BatchingTxApplier( int maxBatchSize, Supplier<TransactionIdStore> txIdStoreSupplier, Supplier<TransactionCommitProcess> commitProcessSupplier,
            Monitors monitors, PageCursorTracerSupplier pageCursorTracerSupplier, VersionContextSupplier versionContextSupplier,
            CommandIndexTracker commandIndexTracker, LogProvider logProvider,
            ReplicatedDatabaseEventService.ReplicatedDatabaseEventDispatch databaseEventDispatch )
    {
        this.maxBatchSize = maxBatchSize;
        this.txIdStoreSupplier = txIdStoreSupplier;
        this.commitProcessSupplier = commitProcessSupplier;
        this.pageCursorTracerSupplier = pageCursorTracerSupplier;
        this.log = logProvider.getLog( this.getClass() );
        this.monitor = (PullRequestMonitor) monitors.newMonitor( PullRequestMonitor.class, new String[0] );
        this.versionContextSupplier = versionContextSupplier;
        this.commandIndexTracker = commandIndexTracker;
        this.databaseEventDispatch = databaseEventDispatch;
    }

    public void start()
    {
        this.stopped = false;
        this.refreshFromNewStore();
        this.txQueue = new TransactionQueue( this.maxBatchSize, ( first, last ) -> {
            this.commitProcess.commit( first, CommitEvent.NULL, TransactionApplicationMode.EXTERNAL );
            ((PageCursorTracer) this.pageCursorTracerSupplier.get()).reportEvents();
            long lastAppliedRaftLogIndex = LogIndexTxHeaderEncoding.decodeLogIndexFromTxHeader( last.transactionRepresentation().additionalHeader() );
            this.commandIndexTracker.setAppliedCommandIndex( lastAppliedRaftLogIndex );
        } );
    }

    public void stop()
    {
        this.stopped = true;
    }

    void refreshFromNewStore()
    {
        assert this.txQueue == null || this.txQueue.isEmpty();

        this.lastQueuedTxId = ((TransactionIdStore) this.txIdStoreSupplier.get()).getLastCommittedTransactionId();

        assert this.lastQueuedTxId == ((TransactionIdStore) this.txIdStoreSupplier.get()).getLastClosedTransactionId();

        this.commitProcess = (TransactionCommitProcess) this.commitProcessSupplier.get();
    }

    public void queue( CommittedTransactionRepresentation tx ) throws Exception
    {
        long receivedTxId = tx.getCommitEntry().getTxId();
        long expectedTxId = this.lastQueuedTxId + 1L;
        if ( receivedTxId != expectedTxId )
        {
            this.log.warn( "Out of order transaction. Received: %d Expected: %d", new Object[]{receivedTxId, expectedTxId} );
        }
        else
        {
            TransactionToApply toApply =
                    new TransactionToApply( tx.getTransactionRepresentation(), receivedTxId, this.versionContextSupplier.getVersionContext() );
            ReplicatedDatabaseEventService.ReplicatedDatabaseEventDispatch var10001 = this.databaseEventDispatch;
            Objects.requireNonNull( var10001 );
            toApply.onClose( var10001::fireTransactionCommitted );
            this.txQueue.queue( toApply );
            if ( !this.stopped )
            {
                this.lastQueuedTxId = receivedTxId;
                this.monitor.txPullResponse( receivedTxId );
            }
        }
    }

    void applyBatch() throws Exception
    {
        this.txQueue.empty();
    }

    long lastQueuedTxId()
    {
        return this.lastQueuedTxId;
    }
}
