package com.neo4j.causalclustering.readreplica;

import com.neo4j.causalclustering.common.ClusteredDatabase;
import com.neo4j.causalclustering.common.DatabaseTopologyNotifier;

import java.util.Objects;

import org.neo4j.kernel.database.Database;
import org.neo4j.kernel.lifecycle.Lifecycle;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;

class ReadReplicaDatabase extends ClusteredDatabase
{
    ReadReplicaDatabase( CatchupProcessManager catchupProcess, Database kernelDatabase, Lifecycle clusterComponents, ReadReplicaBootstrap bootstrap,
            ReadReplicaPanicHandlers panicHandler, RaftIdCheck raftIdCheck, DatabaseTopologyNotifier topologyNotifier )
    {
        this.addComponent( panicHandler );
        Objects.requireNonNull( raftIdCheck );
        this.addComponent( LifecycleAdapter.onInit( raftIdCheck::perform ) );
        this.addComponent( clusterComponents );
        this.addComponent( topologyNotifier );
        Objects.requireNonNull( bootstrap );
        this.addComponent( LifecycleAdapter.onStart( bootstrap::perform ) );
        this.addComponent( kernelDatabase );
        this.addComponent( catchupProcess );
    }
}
