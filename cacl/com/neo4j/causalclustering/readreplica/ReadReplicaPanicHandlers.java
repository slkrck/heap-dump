package com.neo4j.causalclustering.readreplica;

import com.neo4j.causalclustering.common.DatabasePanicHandlers;
import com.neo4j.causalclustering.error_handling.DatabasePanicEventHandler;
import com.neo4j.causalclustering.error_handling.PanicService;
import com.neo4j.dbms.ClusterInternalDbmsOperator;

import java.util.List;

import org.neo4j.kernel.database.Database;

public class ReadReplicaPanicHandlers extends DatabasePanicHandlers
{
    public ReadReplicaPanicHandlers( PanicService panicService, Database kernelDatabase, ClusterInternalDbmsOperator clusterInternalOperator )
    {
        super( panicService, kernelDatabase.getNamedDatabaseId(),
                List.of( DatabasePanicEventHandler.raiseAvailabilityGuard( kernelDatabase ), DatabasePanicEventHandler.markUnhealthy( kernelDatabase ),
                        DatabasePanicEventHandler.stopDatabase( kernelDatabase, clusterInternalOperator ) ) );
    }
}
