package com.neo4j.causalclustering.readreplica;

import com.neo4j.causalclustering.core.state.storage.SimpleStorage;
import com.neo4j.causalclustering.identity.RaftId;

import java.util.Objects;

import org.neo4j.kernel.database.NamedDatabaseId;

class RaftIdCheck
{
    private final SimpleStorage<RaftId> raftIdStorage;
    private final NamedDatabaseId namedDatabaseId;

    RaftIdCheck( SimpleStorage<RaftId> raftIdStorage, NamedDatabaseId namedDatabaseId )
    {
        this.raftIdStorage = raftIdStorage;
        this.namedDatabaseId = namedDatabaseId;
    }

    public void perform() throws Exception
    {
        RaftId raftId;
        if ( this.raftIdStorage.exists() )
        {
            raftId = (RaftId) this.raftIdStorage.readState();
            if ( !Objects.equals( raftId.uuid(), this.namedDatabaseId.databaseId().uuid() ) )
            {
                throw new IllegalStateException( String.format(
                        "Pre-existing cluster state found with an unexpected id %s. The id for this database is %s. This may indicate a previous DROP operation for %s did not complete.",
                        raftId.uuid(), this.namedDatabaseId.databaseId().uuid(), this.namedDatabaseId.name() ) );
            }
        }
        else
        {
            raftId = RaftId.from( this.namedDatabaseId.databaseId() );
            this.raftIdStorage.writeState( raftId );
        }
    }
}
