package com.neo4j.causalclustering.readreplica;

import java.util.stream.Stream;

import org.neo4j.kernel.api.exceptions.ReadOnlyDbException;
import org.neo4j.kernel.impl.api.LeaseClient;
import org.neo4j.kernel.impl.locking.ActiveLock;
import org.neo4j.kernel.impl.locking.Locks;
import org.neo4j.kernel.impl.locking.Locks.Visitor;
import org.neo4j.lock.AcquireLockTimeoutException;
import org.neo4j.lock.LockTracer;
import org.neo4j.lock.ResourceType;

public class ReadReplicaLockManager implements Locks
{
    public org.neo4j.kernel.impl.locking.Locks.Client newClient()
    {
        return new ReadReplicaLockManager.Client();
    }

    public void accept( Visitor visitor )
    {
    }

    public void close()
    {
    }

    private static class Client implements org.neo4j.kernel.impl.locking.Locks.Client
    {
        public void initialize( LeaseClient leaseClient )
        {
        }

        public void acquireShared( LockTracer tracer, ResourceType resourceType, long... resourceIds ) throws AcquireLockTimeoutException
        {
        }

        public void acquireExclusive( LockTracer tracer, ResourceType resourceType, long... resourceIds ) throws AcquireLockTimeoutException
        {
            throw new RuntimeException( new ReadOnlyDbException() );
        }

        public boolean tryExclusiveLock( ResourceType resourceType, long resourceId )
        {
            throw new RuntimeException( new ReadOnlyDbException() );
        }

        public boolean trySharedLock( ResourceType resourceType, long resourceId )
        {
            return false;
        }

        public boolean reEnterShared( ResourceType resourceType, long resourceId )
        {
            return false;
        }

        public boolean reEnterExclusive( ResourceType resourceType, long resourceId )
        {
            throw new IllegalStateException( "Should never happen" );
        }

        public void releaseShared( ResourceType resourceType, long... resourceIds )
        {
        }

        public void releaseExclusive( ResourceType resourceType, long... resourceIds )
        {
            throw new IllegalStateException( "Should never happen" );
        }

        public void prepare()
        {
        }

        public void stop()
        {
        }

        public void close()
        {
        }

        public int getLockSessionId()
        {
            return 0;
        }

        public Stream<ActiveLock> activeLocks()
        {
            return Stream.empty();
        }

        public long activeLockCount()
        {
            return 0L;
        }
    }
}
