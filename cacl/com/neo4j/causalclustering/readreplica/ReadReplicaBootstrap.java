package com.neo4j.causalclustering.readreplica;

import com.neo4j.causalclustering.catchup.CatchupAddressProvider;
import com.neo4j.causalclustering.catchup.CatchupComponentsRepository;
import com.neo4j.causalclustering.catchup.storecopy.DatabaseShutdownException;
import com.neo4j.causalclustering.catchup.storecopy.RemoteStore;
import com.neo4j.causalclustering.catchup.storecopy.StoreCopyFailedException;
import com.neo4j.causalclustering.catchup.storecopy.StoreIdDownloadFailedException;
import com.neo4j.causalclustering.core.state.snapshot.TopologyLookupException;
import com.neo4j.causalclustering.discovery.TopologyService;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.upstream.UpstreamDatabaseSelectionException;
import com.neo4j.causalclustering.upstream.UpstreamDatabaseStrategySelector;
import com.neo4j.dbms.ClusterInternalDbmsOperator;
import com.neo4j.dbms.DatabaseStartAborter;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.dbms.database.DatabaseStartAbortedException;
import org.neo4j.internal.helpers.ExponentialBackoffStrategy;
import org.neo4j.internal.helpers.TimeoutStrategy;
import org.neo4j.internal.helpers.TimeoutStrategy.Timeout;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.storageengine.api.StoreId;

class ReadReplicaBootstrap
{
    private final Log debugLog;
    private final Log userLog;
    private final TimeoutStrategy syncRetryStrategy;
    private final UpstreamDatabaseStrategySelector selectionStrategy;
    private final DatabaseStartAborter databaseStartAborter;
    private final ClusterInternalDbmsOperator internalOperator;
    private final TopologyService topologyService;
    private final Supplier<CatchupComponentsRepository.CatchupComponents> catchupComponentsSupplier;
    private final ReadReplicaDatabaseContext databaseContext;

    ReadReplicaBootstrap( ReadReplicaDatabaseContext databaseContext, UpstreamDatabaseStrategySelector selectionStrategy, LogProvider debugLogProvider,
            LogProvider userLogProvider, TopologyService topologyService, Supplier<CatchupComponentsRepository.CatchupComponents> catchupComponentsSupplier,
            ClusterInternalDbmsOperator internalOperator, DatabaseStartAborter databaseStartAborter )
    {
        this.databaseContext = databaseContext;
        this.catchupComponentsSupplier = catchupComponentsSupplier;
        this.selectionStrategy = selectionStrategy;
        this.databaseStartAborter = databaseStartAborter;
        this.syncRetryStrategy = new ExponentialBackoffStrategy( 1L, 30L, TimeUnit.SECONDS );
        this.debugLog = debugLogProvider.getLog( this.getClass() );
        this.userLog = userLogProvider.getLog( this.getClass() );
        this.topologyService = topologyService;
        this.internalOperator = internalOperator;
    }

    public void perform() throws Exception
    {
        ClusterInternalDbmsOperator.BootstrappingHandle bootstrapHandle = this.internalOperator.bootstrap( this.databaseContext.databaseId() );
        boolean shouldAbort = false;

        try
        {
            Timeout syncRetryWaitPeriod = this.syncRetryStrategy.newTimeout();
            boolean synced = false;

            while ( !synced && !shouldAbort )
            {
                try
                {
                    this.debugLog.info( "Syncing db: %s", new Object[]{this.databaseContext.databaseId()} );
                    synced = this.doSyncStoreCopyWithUpstream( this.databaseContext );
                    if ( synced )
                    {
                        this.debugLog.info( "Successfully synced db: %s", new Object[]{this.databaseContext.databaseId()} );
                    }
                    else
                    {
                        Thread.sleep( syncRetryWaitPeriod.getMillis() );
                        syncRetryWaitPeriod.increment();
                    }

                    shouldAbort = this.databaseStartAborter.shouldAbort( this.databaseContext.databaseId() );
                }
                catch ( InterruptedException var10 )
                {
                    Thread.currentThread().interrupt();
                    this.userLog.error( "Interrupted while trying to start read replica" );
                    throw new RuntimeException( var10 );
                }
                catch ( Exception var11 )
                {
                    this.debugLog.error( "Unexpected error when syncing stores", var11 );
                    throw new RuntimeException( var11 );
                }
            }
        }
        finally
        {
            this.databaseStartAborter.started( this.databaseContext.databaseId() );
            bootstrapHandle.release();
        }

        if ( shouldAbort )
        {
            throw new DatabaseStartAbortedException( this.databaseContext.databaseId() );
        }
    }

    private boolean doSyncStoreCopyWithUpstream( ReadReplicaDatabaseContext databaseContext )
    {
        MemberId source;
        try
        {
            source = this.selectionStrategy.bestUpstreamMemberForDatabase( databaseContext.databaseId() );
        }
        catch ( UpstreamDatabaseSelectionException var8 )
        {
            this.debugLog.warn( "Unable to find upstream member for database " + databaseContext.databaseId().name() );
            return false;
        }

        try
        {
            this.syncStoreWithUpstream( databaseContext, source );
            return true;
        }
        catch ( TopologyLookupException var4 )
        {
            this.debugLog.warn( "Unable to get address of %s", new Object[]{source} );
            return false;
        }
        catch ( StoreIdDownloadFailedException var5 )
        {
            this.debugLog.warn( "Unable to get store ID from %s", new Object[]{source} );
            return false;
        }
        catch ( StoreCopyFailedException var6 )
        {
            this.debugLog.warn( "Unable to copy store files from %s", new Object[]{source} );
            return false;
        }
        catch ( IOException | DatabaseShutdownException var7 )
        {
            this.debugLog.warn( String.format( "Syncing of stores failed unexpectedly from %s", source ), var7 );
            return false;
        }
    }

    private void syncStoreWithUpstream( ReadReplicaDatabaseContext databaseContext, MemberId source )
            throws IOException, StoreIdDownloadFailedException, StoreCopyFailedException, TopologyLookupException, DatabaseShutdownException
    {
        CatchupComponentsRepository.CatchupComponents catchupComponents = (CatchupComponentsRepository.CatchupComponents) this.catchupComponentsSupplier.get();
        if ( databaseContext.isEmpty() )
        {
            this.debugLog.info( "Local database is empty, attempting to replace with copy from upstream server %s", new Object[]{source} );
            this.debugLog.info( "Finding store ID of upstream server %s", new Object[]{source} );
            SocketAddress fromAddress = this.topologyService.lookupCatchupAddress( source );
            StoreId storeId = catchupComponents.remoteStore().getStoreId( fromAddress );
            this.debugLog.info( "Copying store from upstream server %s", new Object[]{source} );
            databaseContext.delete();
            catchupComponents.storeCopyProcess().replaceWithStoreFrom( new CatchupAddressProvider.SingleAddressProvider( fromAddress ), storeId );
            this.debugLog.info( "Restarting local database after copy.", new Object[]{source} );
        }
        else
        {
            this.ensureStoreIsPresentAt( databaseContext, catchupComponents.remoteStore(), source );
        }
    }

    private void ensureStoreIsPresentAt( ReadReplicaDatabaseContext databaseContext, RemoteStore remoteStore, MemberId upstream )
            throws StoreIdDownloadFailedException, TopologyLookupException
    {
        StoreId localStoreId = databaseContext.storeId();
        SocketAddress advertisedSocketAddress = this.topologyService.lookupCatchupAddress( upstream );
        StoreId remoteStoreId = remoteStore.getStoreId( advertisedSocketAddress );
        if ( !localStoreId.equals( remoteStoreId ) )
        {
            throw new IllegalStateException( String.format(
                    "This read replica cannot join the cluster. The local version of database %s is not empty and has a mismatching storeId: expected %s actual %s.",
                    databaseContext.databaseId().name(), remoteStoreId, localStoreId ) );
        }
    }
}
