package com.neo4j.causalclustering.readreplica;

import com.neo4j.causalclustering.catchup.CatchupAddressProvider;
import com.neo4j.causalclustering.catchup.CatchupClientFactory;
import com.neo4j.causalclustering.catchup.CatchupComponentsRepository;
import com.neo4j.causalclustering.core.CausalClusteringSettings;
import com.neo4j.causalclustering.core.consensus.schedule.TimeoutFactory;
import com.neo4j.causalclustering.core.consensus.schedule.Timer;
import com.neo4j.causalclustering.core.consensus.schedule.TimerService;
import com.neo4j.causalclustering.core.state.machines.CommandIndexTracker;
import com.neo4j.causalclustering.discovery.TopologyService;
import com.neo4j.causalclustering.error_handling.DatabasePanicker;
import com.neo4j.causalclustering.upstream.UpstreamDatabaseStrategySelector;
import com.neo4j.dbms.ReplicatedDatabaseEventService;

import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import org.neo4j.configuration.Config;
import org.neo4j.io.pagecache.tracing.cursor.PageCursorTracerSupplier;
import org.neo4j.kernel.impl.api.TransactionCommitProcess;
import org.neo4j.kernel.impl.api.TransactionRepresentationCommitProcess;
import org.neo4j.kernel.impl.transaction.log.TransactionAppender;
import org.neo4j.kernel.lifecycle.LifeSupport;
import org.neo4j.kernel.lifecycle.SafeLifecycle;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.scheduler.Group;
import org.neo4j.storageengine.api.StorageEngine;
import org.neo4j.storageengine.api.TransactionIdStore;
import org.neo4j.util.VisibleForTesting;

public class CatchupProcessManager extends SafeLifecycle
{
    private final TopologyService topologyService;
    private final CatchupClientFactory catchupClient;
    private final UpstreamDatabaseStrategySelector selectionStrategyPipeline;
    private final TimerService timerService;
    private final long txPullIntervalMillis;
    private final CommandIndexTracker commandIndexTracker;
    private final PageCursorTracerSupplier pageCursorTracerSupplier;
    private final Executor executor;
    private final ReadReplicaDatabaseContext databaseContext;
    private final LogProvider logProvider;
    private final Log log;
    private final Config config;
    private final CatchupComponentsRepository catchupComponents;
    private final DatabasePanicker panicker;
    private final ReplicatedDatabaseEventService.ReplicatedDatabaseEventDispatch databaseEventDispatch;
    private CatchupPollingProcess catchupProcess;
    private LifeSupport txPulling;
    private volatile boolean isPanicked;
    private Timer timer;

    CatchupProcessManager( Executor executor, CatchupComponentsRepository catchupComponents, ReadReplicaDatabaseContext databaseContext,
            DatabasePanicker panicker, TopologyService topologyService, CatchupClientFactory catchUpClient,
            UpstreamDatabaseStrategySelector selectionStrategyPipeline, TimerService timerService, CommandIndexTracker commandIndexTracker,
            LogProvider logProvider, PageCursorTracerSupplier pageCursorTracerSupplier, Config config,
            ReplicatedDatabaseEventService.ReplicatedDatabaseEventDispatch databaseEventDispatch )
    {
        this.logProvider = logProvider;
        this.log = logProvider.getLog( this.getClass() );
        this.pageCursorTracerSupplier = pageCursorTracerSupplier;
        this.config = config;
        this.commandIndexTracker = commandIndexTracker;
        this.timerService = timerService;
        this.executor = executor;
        this.catchupComponents = catchupComponents;
        this.databaseContext = databaseContext;
        this.panicker = panicker;
        this.topologyService = topologyService;
        this.catchupClient = catchUpClient;
        this.selectionStrategyPipeline = selectionStrategyPipeline;
        this.txPullIntervalMillis = ((Duration) config.get( CausalClusteringSettings.pull_interval )).toMillis();
        this.databaseEventDispatch = databaseEventDispatch;
        this.txPulling = new LifeSupport();
        this.isPanicked = false;
    }

    public void start0()
    {
        this.log.info( "Starting " + this.getClass().getSimpleName() );
        this.txPulling = new LifeSupport();
        this.catchupProcess = this.createCatchupProcess( this.databaseContext );
        this.txPulling.start();
        this.initTimer();
    }

    public void stop0()
    {
        this.log.info( "Shutting down " + this.getClass().getSimpleName() );
        this.timer.kill( Timer.CancelMode.SYNC_WAIT );
        this.txPulling.stop();
    }

    synchronized CompletableFuture<Void> panic( Throwable e )
    {
        this.log.error( "Unexpected issue in catchup process. No more catchup requests will be scheduled.", e );
        this.isPanicked = true;
        return this.panicker.panicAsync( e );
    }

    private CatchupPollingProcess createCatchupProcess( ReadReplicaDatabaseContext databaseContext )
    {
        CatchupComponentsRepository.CatchupComponents dbCatchupComponents =
                (CatchupComponentsRepository.CatchupComponents) this.catchupComponents.componentsFor( databaseContext.databaseId() ).orElseThrow( () -> {
                    return new IllegalArgumentException(
                            String.format( "No StoreCopyProcess instance exists for database %s.", databaseContext.databaseId() ) );
                } );
        Supplier<TransactionCommitProcess> writableCommitProcess = () -> {
            return new TransactionRepresentationCommitProcess(
                    (TransactionAppender) databaseContext.kernelDatabase().getDependencyResolver().resolveDependency( TransactionAppender.class ),
                    (StorageEngine) databaseContext.kernelDatabase().getDependencyResolver().resolveDependency( StorageEngine.class ) );
        };
        int maxBatchSize = (Integer) this.config.get( CausalClusteringSettings.read_replica_transaction_applier_batch_size );
        BatchingTxApplier batchingTxApplier = new BatchingTxApplier( maxBatchSize, () -> {
            return (TransactionIdStore) databaseContext.kernelDatabase().getDependencyResolver().resolveDependency( TransactionIdStore.class );
        }, writableCommitProcess, databaseContext.monitors(), this.pageCursorTracerSupplier, databaseContext.kernelDatabase().getVersionContextSupplier(),
                this.commandIndexTracker, this.logProvider, this.databaseEventDispatch );
        CatchupPollingProcess catchupProcess =
                new CatchupPollingProcess( this.executor, databaseContext, this.catchupClient, batchingTxApplier, this.databaseEventDispatch,
                        dbCatchupComponents.storeCopyProcess(), this.logProvider, this::panic,
                        new CatchupAddressProvider.UpstreamStrategyBasedAddressProvider( this.topologyService, this.selectionStrategyPipeline ) );
        this.txPulling.add( batchingTxApplier );
        this.txPulling.add( catchupProcess );
        return catchupProcess;
    }

    private void onTimeout() throws Exception
    {
        this.catchupProcess.tick().get();
        if ( !this.isPanicked )
        {
            this.timer.reset();
        }
    }

    @VisibleForTesting
    public CatchupPollingProcess getCatchupProcess()
    {
        return this.catchupProcess;
    }

    @VisibleForTesting
    void setCatchupProcess( CatchupPollingProcess catchupProcess )
    {
        this.catchupProcess = catchupProcess;
    }

    void initTimer()
    {
        this.timer = this.timerService.create( CatchupProcessManager.Timers.TX_PULLER_TIMER, Group.PULL_UPDATES, ( timeout ) -> {
            this.onTimeout();
        } );
        this.timer.set( TimeoutFactory.fixedTimeout( this.txPullIntervalMillis, TimeUnit.MILLISECONDS ) );
    }

    public static enum Timers implements TimerService.TimerName
    {
        TX_PULLER_TIMER;
    }
}
