package com.neo4j.causalclustering.readreplica;

import com.neo4j.causalclustering.common.ClusteringEditionModule;
import com.neo4j.causalclustering.discovery.DiscoveryServiceFactory;
import com.neo4j.causalclustering.identity.MemberId;

import java.util.UUID;

import org.neo4j.configuration.Config;
import org.neo4j.dbms.api.DatabaseManagementService;
import org.neo4j.graphdb.facade.DatabaseManagementServiceFactory;
import org.neo4j.graphdb.facade.ExternalDependencies;
import org.neo4j.graphdb.factory.module.GlobalModule;
import org.neo4j.kernel.impl.factory.DatabaseInfo;

public class ReadReplicaGraphDatabase
{
    private final DatabaseManagementService managementService;

    public ReadReplicaGraphDatabase( Config config, ExternalDependencies dependencies, DiscoveryServiceFactory discoveryServiceFactory,
            ReadReplicaGraphDatabase.ReadReplicaEditionModuleFactory editionModuleFactory )
    {
        this( config, dependencies, discoveryServiceFactory, new MemberId( UUID.randomUUID() ), editionModuleFactory );
    }

    public ReadReplicaGraphDatabase( Config config, ExternalDependencies dependencies, DiscoveryServiceFactory discoveryServiceFactory, MemberId memberId,
            ReadReplicaGraphDatabase.ReadReplicaEditionModuleFactory editionModuleFactory )
    {
        this.managementService = this.createManagementService( config, dependencies, discoveryServiceFactory, memberId, editionModuleFactory );
    }

    protected DatabaseManagementService createManagementService( Config config, ExternalDependencies dependencies,
            DiscoveryServiceFactory discoveryServiceFactory, MemberId memberId, ReadReplicaGraphDatabase.ReadReplicaEditionModuleFactory editionModuleFactory )
    {
        return (new DatabaseManagementServiceFactory( DatabaseInfo.READ_REPLICA, ( globalModule ) -> {
            return editionModuleFactory.create( globalModule, discoveryServiceFactory, memberId );
        } )).build( config, dependencies );
    }

    public DatabaseManagementService getManagementService()
    {
        return this.managementService;
    }

    public interface ReadReplicaEditionModuleFactory
    {
        ClusteringEditionModule create( GlobalModule var1, DiscoveryServiceFactory var2, MemberId var3 );
    }
}
