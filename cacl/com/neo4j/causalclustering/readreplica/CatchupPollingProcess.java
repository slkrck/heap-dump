package com.neo4j.causalclustering.readreplica;

import com.neo4j.causalclustering.catchup.CatchupAddressProvider;
import com.neo4j.causalclustering.catchup.CatchupAddressResolutionException;
import com.neo4j.causalclustering.catchup.CatchupClientFactory;
import com.neo4j.causalclustering.catchup.CatchupResponseAdaptor;
import com.neo4j.causalclustering.catchup.storecopy.DatabaseShutdownException;
import com.neo4j.causalclustering.catchup.storecopy.StoreCopyFailedException;
import com.neo4j.causalclustering.catchup.storecopy.StoreCopyProcess;
import com.neo4j.causalclustering.catchup.tx.PullRequestMonitor;
import com.neo4j.causalclustering.catchup.tx.TxPullResponse;
import com.neo4j.causalclustering.catchup.tx.TxStreamFinishedResponse;
import com.neo4j.causalclustering.error_handling.DatabasePanicker;
import com.neo4j.dbms.ClusterInternalDbmsOperator;
import com.neo4j.dbms.ReplicatedDatabaseEventService;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.Executor;

import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.kernel.impl.transaction.CommittedTransactionRepresentation;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.storageengine.api.StoreId;
import org.neo4j.util.Preconditions;
import org.neo4j.util.VisibleForTesting;

public class CatchupPollingProcess extends LifecycleAdapter
{
    private final ReadReplicaDatabaseContext databaseContext;
    private final CatchupAddressProvider upstreamProvider;
    private final Log log;
    private final StoreCopyProcess storeCopyProcess;
    private final CatchupClientFactory catchUpClient;
    private final DatabasePanicker panicker;
    private final BatchingTxApplier applier;
    private final ReplicatedDatabaseEventService.ReplicatedDatabaseEventDispatch databaseEventDispatch;
    private final PullRequestMonitor pullRequestMonitor;
    private final Executor executor;
    private volatile CatchupPollingProcess.State state;
    private CompletableFuture<Boolean> upToDateFuture;
    private ClusterInternalDbmsOperator.StoreCopyHandle storeCopyHandle;

    CatchupPollingProcess( Executor executor, ReadReplicaDatabaseContext databaseContext, CatchupClientFactory catchUpClient, BatchingTxApplier applier,
            ReplicatedDatabaseEventService.ReplicatedDatabaseEventDispatch databaseEventDispatch, StoreCopyProcess storeCopyProcess, LogProvider logProvider,
            DatabasePanicker panicker, CatchupAddressProvider upstreamProvider )
    {
        this.state = CatchupPollingProcess.State.TX_PULLING;
        this.databaseContext = databaseContext;
        this.upstreamProvider = upstreamProvider;
        this.catchUpClient = catchUpClient;
        this.applier = applier;
        this.databaseEventDispatch = databaseEventDispatch;
        this.pullRequestMonitor = (PullRequestMonitor) databaseContext.monitors().newMonitor( PullRequestMonitor.class, new String[0] );
        this.storeCopyProcess = storeCopyProcess;
        this.log = logProvider.getLog( this.getClass() );
        this.panicker = panicker;
        this.executor = executor;
    }

    public synchronized void start()
    {
        this.state = CatchupPollingProcess.State.TX_PULLING;
        this.upToDateFuture = new CompletableFuture();
    }

    @VisibleForTesting
    public CompletableFuture<Boolean> upToDateFuture()
    {
        return this.upToDateFuture;
    }

    public void stop()
    {
        this.state = CatchupPollingProcess.State.CANCELLED;
    }

    public CatchupPollingProcess.State state()
    {
        return this.state;
    }

    public CompletableFuture<Void> tick()
    {
        return this.state != CatchupPollingProcess.State.CANCELLED && this.state != CatchupPollingProcess.State.PANIC ? CompletableFuture.runAsync( () -> {
            try
            {
                switch ( this.state )
                {
                case TX_PULLING:
                    this.pullTransactions();
                    break;
                case STORE_COPYING:
                    this.copyStore();
                    break;
                default:
                    throw new IllegalStateException( "Tried to execute catchup but was in state " + this.state );
                }
            }
            catch ( Throwable var2 )
            {
                throw new CompletionException( var2 );
            }
        }, this.executor ).exceptionally( ( e ) -> {
            this.panic( e );
            return null;
        } ) : CompletableFuture.completedFuture( (Object) null );
    }

    private synchronized void panic( Throwable e )
    {
        this.upToDateFuture.completeExceptionally( e );
        this.state = CatchupPollingProcess.State.PANIC;
        this.panicker.panic( e );
    }

    private void pullTransactions()
    {
        SocketAddress address;
        try
        {
            address = this.upstreamProvider.primary( this.databaseContext.databaseId() );
        }
        catch ( CatchupAddressResolutionException var3 )
        {
            this.log.warn( "Could not find upstream database from which to pull. [Message: %s].", new Object[]{var3.getMessage()} );
            return;
        }

        this.pullAndApplyBatchOfTransactions( address, this.databaseContext.storeId() );
    }

    private synchronized void handleTransaction( CommittedTransactionRepresentation tx )
    {
        if ( this.state != CatchupPollingProcess.State.PANIC )
        {
            try
            {
                this.applier.queue( tx );
            }
            catch ( Throwable var3 )
            {
                this.panic( var3 );
            }
        }
    }

    private synchronized void streamComplete()
    {
        if ( this.state != CatchupPollingProcess.State.PANIC )
        {
            try
            {
                this.applier.applyBatch();
            }
            catch ( Throwable var2 )
            {
                this.panic( var2 );
            }
        }
    }

    private void pullAndApplyBatchOfTransactions( SocketAddress address, StoreId localStoreId )
    {
        long lastQueuedTxId = this.applier.lastQueuedTxId();
        this.pullRequestMonitor.txPullRequest( lastQueuedTxId );
        this.log.debug( "Pull transactions from %s where tx id > %d", new Object[]{address, lastQueuedTxId} );
        CatchupResponseAdaptor responseHandler = new CatchupResponseAdaptor<TxStreamFinishedResponse>()
        {
            public void onTxPullResponse( CompletableFuture<TxStreamFinishedResponse> signal, TxPullResponse response )
            {
                CatchupPollingProcess.this.handleTransaction( response.tx() );
            }

            public void onTxStreamFinishedResponse( CompletableFuture<TxStreamFinishedResponse> signal, TxStreamFinishedResponse response )
            {
                CatchupPollingProcess.this.streamComplete();
                signal.complete( response );
            }
        };

        TxStreamFinishedResponse result;
        try
        {
            result = (TxStreamFinishedResponse) this.catchUpClient.getClient( address, this.log ).v3( ( c ) -> {
                return c.pullTransactions( localStoreId, lastQueuedTxId, this.databaseContext.databaseId() );
            } ).withResponseHandler( responseHandler ).request();
        }
        catch ( Exception var8 )
        {
            this.log.warn( "Exception occurred while pulling transactions. Will retry shortly.", var8 );
            this.streamComplete();
            return;
        }

        switch ( result.status() )
        {
        case SUCCESS_END_OF_STREAM:
            this.log.debug( "Successfully pulled transactions from tx id %d", new Object[]{lastQueuedTxId} );
            this.upToDateFuture.complete( Boolean.TRUE );
            break;
        case E_TRANSACTION_PRUNED:
            this.log.info( "Tx pull unable to get transactions starting from %d since transactions have been pruned. Attempting a store copy.",
                    new Object[]{lastQueuedTxId} );
            this.transitionToStoreCopy();
            break;
        default:
            this.log.info( "Tx pull request unable to get transactions > %d ", new Object[]{lastQueuedTxId} );
        }
    }

    private void transitionToStoreCopy()
    {
        this.state = CatchupPollingProcess.State.STORE_COPYING;
    }

    private void transitionToTxPulling()
    {
        this.state = CatchupPollingProcess.State.TX_PULLING;
    }

    private void copyStore()
    {
        try
        {
            this.ensureKernelStopped();
            this.storeCopyProcess.replaceWithStoreFrom( this.upstreamProvider, this.databaseContext.storeId() );
            if ( this.restartDatabaseAfterStoreCopy() )
            {
                this.transitionToTxPulling();
                this.databaseEventDispatch.fireStoreReplaced( this.applier.lastQueuedTxId() );
            }
        }
        catch ( StoreCopyFailedException | IOException var2 )
        {
            this.log.warn( "Error copying store. Will retry shortly.", var2 );
        }
        catch ( DatabaseShutdownException var3 )
        {
            this.log.warn( "Store copy aborted due to shutdown.", var3 );
        }
    }

    private void ensureKernelStopped()
    {
        if ( this.storeCopyHandle == null )
        {
            this.log.info( "Stopping kernel for store copy" );
            this.storeCopyHandle = this.databaseContext.stopForStoreCopy();
        }
        else
        {
            this.log.info( "Kernel still stopped for store copy" );
        }
    }

    private boolean restartDatabaseAfterStoreCopy()
    {
        Preconditions.checkState( this.storeCopyHandle != null, "Store copy handle not initialized" );
        this.log.info( "Attempting kernel start after store copy" );
        ClusterInternalDbmsOperator.StoreCopyHandle handle = this.storeCopyHandle;
        this.storeCopyHandle = null;
        boolean triggeredReconciler = handle.release();
        if ( !triggeredReconciler )
        {
            this.log.warn( "Reconciler could not be triggered at this time." );
            return false;
        }
        else if ( !this.databaseContext.kernelDatabase().isStarted() )
        {
            this.log.warn( "Kernel did not start properly after the store copy. This might be because of unexpected errors or normal early-exit paths." );
            return false;
        }
        else
        {
            this.log.info( "Kernel started after store copy" );
            this.applier.refreshFromNewStore();
            return true;
        }
    }

    static enum State
    {
        TX_PULLING,
        STORE_COPYING,
        PANIC,
        CANCELLED;
    }
}
