package com.neo4j.causalclustering.messaging;

import java.io.IOException;

public class MessageTooBigException extends IOException
{
    public MessageTooBigException( String message )
    {
        super( message );
    }
}
