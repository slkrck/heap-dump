package com.neo4j.causalclustering.messaging;

public interface Outbound<MEMBER, MESSAGE extends Message>
{
    default void send( MEMBER to, MESSAGE message )
    {
        this.send( to, message, false );
    }

    void send( MEMBER var1, MESSAGE var2, boolean var3 );
}
