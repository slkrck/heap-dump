package com.neo4j.causalclustering.messaging;

import com.neo4j.causalclustering.net.ChannelPoolService;
import com.neo4j.causalclustering.net.PooledChannel;
import io.netty.channel.ChannelFuture;

import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutionException;

import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class RaftSender implements Outbound<SocketAddress,Message>
{
    private final ChannelPoolService channels;
    private final Log log;

    public RaftSender( LogProvider logProvider, RaftChannelPoolService channelPoolService )
    {
        this.channels = channelPoolService;
        this.log = logProvider.getLog( this.getClass() );
    }

    public void send( SocketAddress to, Message message, boolean block )
    {
        CompletableFuture<Void> fOperation = this.channels.acquire( to ).thenCompose( ( pooledChannel ) -> {
            return this.sendMessage( pooledChannel, message );
        } );
        if ( block )
        {
            try
            {
                fOperation.get();
            }
            catch ( ExecutionException var6 )
            {
                this.log.error( "Exception while sending to: " + to, var6 );
            }
            catch ( InterruptedException var7 )
            {
                fOperation.cancel( true );
                this.log.info( "Interrupted while sending", var7 );
            }
        }
        else
        {
            fOperation.whenComplete( ( ignore, throwable ) -> {
                if ( throwable != null )
                {
                    this.log.warn( "Raft sender failed exceptionally [Address: " + to + "]", throwable );
                }
            } );
        }
    }

    private CompletableFuture<Void> sendMessage( PooledChannel pooledChannel, Message message )
    {
        try
        {
            CompletableFuture<Void> fOperation = new CompletableFuture();
            fOperation.whenComplete( ( ignore, ex ) -> {
                if ( ex instanceof CancellationException )
                {
                    pooledChannel.dispose();
                }
            } );
            ChannelFuture fWrite = pooledChannel.channel().writeAndFlush( message );
            fWrite.addListener( ( writeComplete ) -> {
                if ( !writeComplete.isSuccess() )
                {
                    pooledChannel.dispose();
                    fOperation.completeExceptionally( this.wrapCause( pooledChannel, writeComplete.cause() ) );
                }
                else
                {
                    try
                    {
                        pooledChannel.release().addListener( ( f ) -> {
                            fOperation.complete( (Object) null );
                        } );
                    }
                    catch ( Throwable var5 )
                    {
                        fOperation.complete( (Object) null );
                    }
                }
            } );
            return fOperation;
        }
        catch ( Throwable var5 )
        {
            pooledChannel.dispose();
            throw this.wrapCause( pooledChannel, var5 );
        }
    }

    private CompletionException wrapCause( PooledChannel pooledChannel, Throwable e )
    {
        return new CompletionException( "[ChannelId: " + pooledChannel.channel().id() + "]", e );
    }
}
