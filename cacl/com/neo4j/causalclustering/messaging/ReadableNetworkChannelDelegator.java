package com.neo4j.causalclustering.messaging;

import java.io.IOException;
import java.util.Objects;

import org.neo4j.kernel.impl.transaction.log.LogPositionMarker;
import org.neo4j.kernel.impl.transaction.log.ReadableClosablePositionAwareChecksumChannel;

public class ReadableNetworkChannelDelegator implements ReadableClosablePositionAwareChecksumChannel
{
    private ReadableClosablePositionAwareChecksumChannel delegate;

    public void delegateTo( ReadableClosablePositionAwareChecksumChannel channel )
    {
        this.delegate = (ReadableClosablePositionAwareChecksumChannel) Objects.requireNonNull( channel );
    }

    public LogPositionMarker getCurrentPosition( LogPositionMarker positionMarker ) throws IOException
    {
        this.assertAssigned();
        return this.delegate.getCurrentPosition( positionMarker );
    }

    public byte get() throws IOException
    {
        this.assertAssigned();
        return this.delegate.get();
    }

    public short getShort() throws IOException
    {
        this.assertAssigned();
        return this.delegate.getShort();
    }

    public int getInt() throws IOException
    {
        this.assertAssigned();
        return this.delegate.getInt();
    }

    public long getLong() throws IOException
    {
        this.assertAssigned();
        return this.delegate.getLong();
    }

    public float getFloat() throws IOException
    {
        this.assertAssigned();
        return this.delegate.getFloat();
    }

    public double getDouble() throws IOException
    {
        this.assertAssigned();
        return this.delegate.getDouble();
    }

    public void get( byte[] bytes, int length ) throws IOException
    {
        this.assertAssigned();
        this.delegate.get( bytes, length );
    }

    public void close() throws IOException
    {
        this.assertAssigned();
        this.delegate.close();
    }

    private void assertAssigned()
    {
        if ( this.delegate == null )
        {
            throw new IllegalArgumentException( "No assigned channel to delegate reads" );
        }
    }

    public void beginChecksum()
    {
    }

    public int endChecksumAndValidate() throws IOException
    {
        return 0;
    }
}
