package com.neo4j.causalclustering.messaging;

import java.util.concurrent.Future;

import org.neo4j.logging.Log;

public class SimpleNettyChannel implements Channel
{
    private final Log log;
    private final io.netty.channel.Channel channel;
    private volatile boolean disposed;

    public SimpleNettyChannel( io.netty.channel.Channel channel, Log log )
    {
        this.channel = channel;
        this.log = log;
    }

    public boolean isDisposed()
    {
        return this.disposed;
    }

    public synchronized void dispose()
    {
        this.log.info( "Disposing channel: " + this.channel );
        this.disposed = true;
        this.channel.close();
    }

    public boolean isOpen()
    {
        return this.channel.isOpen();
    }

    public Future<Void> write( Object msg )
    {
        this.checkDisposed();
        return this.channel.write( msg );
    }

    public Future<Void> writeAndFlush( Object msg )
    {
        this.checkDisposed();
        return this.channel.writeAndFlush( msg );
    }

    private void checkDisposed()
    {
        if ( this.disposed )
        {
            throw new IllegalStateException( "sending on disposed channel" );
        }
    }
}
