package com.neo4j.causalclustering.messaging;

import com.neo4j.causalclustering.core.consensus.RaftMessages;
import com.neo4j.causalclustering.logging.RaftMessageLogger;

public class LoggingOutbound<MEMBER, MESSAGE extends RaftMessages.RaftMessage> implements Outbound<MEMBER,MESSAGE>
{
    private final Outbound<MEMBER,MESSAGE> outbound;
    private final MEMBER me;
    private final RaftMessageLogger<MEMBER> raftMessageLogger;

    public LoggingOutbound( Outbound<MEMBER,MESSAGE> outbound, MEMBER me, RaftMessageLogger<MEMBER> raftMessageLogger )
    {
        this.outbound = outbound;
        this.me = me;
        this.raftMessageLogger = raftMessageLogger;
    }

    public void send( MEMBER to, MESSAGE message, boolean block )
    {
        this.raftMessageLogger.logOutbound( this.me, message, to );
        this.outbound.send( to, message );
    }
}
