package com.neo4j.causalclustering.messaging;

import io.netty.buffer.ByteBuf;

public interface ByteBufBacked
{
    ByteBuf byteBuf();
}
