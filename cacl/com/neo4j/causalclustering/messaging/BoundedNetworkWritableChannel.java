package com.neo4j.causalclustering.messaging;

import io.netty.buffer.ByteBuf;
import org.neo4j.io.ByteUnit;
import org.neo4j.io.fs.WritableChecksumChannel;

public class BoundedNetworkWritableChannel implements WritableChecksumChannel, ByteBufBacked
{
    private static final long DEFAULT_SIZE_LIMIT = ByteUnit.mebiBytes( 2L );
    private final ByteBuf delegate;
    private final int initialWriterIndex;
    private final long sizeLimit;

    public BoundedNetworkWritableChannel( ByteBuf delegate )
    {
        this( delegate, DEFAULT_SIZE_LIMIT );
    }

    public BoundedNetworkWritableChannel( ByteBuf delegate, long sizeLimit )
    {
        this.delegate = delegate;
        this.initialWriterIndex = delegate.writerIndex();
        this.sizeLimit = sizeLimit;
    }

    public WritableChecksumChannel put( byte value ) throws MessageTooBigException
    {
        this.checkSize( 1 );
        this.delegate.writeByte( value );
        return this;
    }

    public WritableChecksumChannel putShort( short value ) throws MessageTooBigException
    {
        this.checkSize( 2 );
        this.delegate.writeShort( value );
        return this;
    }

    public WritableChecksumChannel putInt( int value ) throws MessageTooBigException
    {
        this.checkSize( 4 );
        this.delegate.writeInt( value );
        return this;
    }

    public WritableChecksumChannel putLong( long value ) throws MessageTooBigException
    {
        this.checkSize( 8 );
        this.delegate.writeLong( value );
        return this;
    }

    public WritableChecksumChannel putFloat( float value ) throws MessageTooBigException
    {
        this.checkSize( 4 );
        this.delegate.writeFloat( value );
        return this;
    }

    public WritableChecksumChannel putDouble( double value ) throws MessageTooBigException
    {
        this.checkSize( 8 );
        this.delegate.writeDouble( value );
        return this;
    }

    public WritableChecksumChannel put( byte[] value, int length ) throws MessageTooBigException
    {
        this.checkSize( length );
        this.delegate.writeBytes( value, 0, length );
        return this;
    }

    private void checkSize( int additional ) throws MessageTooBigException
    {
        int writtenSoFar = this.delegate.writerIndex() - this.initialWriterIndex;
        int countToCheck = writtenSoFar + additional;
        if ( (long) countToCheck > this.sizeLimit )
        {
            throw new MessageTooBigException(
                    String.format( "Size limit exceeded. Limit is %d, wanted to write %d with the writer index at %d (started at %d), written so far %d",
                            this.sizeLimit, additional, this.delegate.writerIndex(), this.initialWriterIndex, writtenSoFar ) );
        }
    }

    public ByteBuf byteBuf()
    {
        return this.delegate;
    }

    public void beginChecksum()
    {
    }

    public int putChecksum()
    {
        return 0;
    }
}
