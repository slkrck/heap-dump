package com.neo4j.causalclustering.messaging;

import com.neo4j.causalclustering.net.NettyUtil;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.pool.ChannelPoolHandler;
import io.netty.channel.pool.SimpleChannelPool;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.Promise;

public class OneMultiplexedChannel extends SimpleChannelPool
{
    private volatile Future<io.netty.channel.Channel> fChannel;

    OneMultiplexedChannel( Bootstrap bootstrap, ChannelPoolHandler handler )
    {
        super( bootstrap, handler );
    }

    private static void closeAwait( Future<io.netty.channel.Channel> fChannel )
    {
        if ( fChannel != null )
        {
            fChannel.awaitUninterruptibly();
            io.netty.channel.Channel channel = (io.netty.channel.Channel) fChannel.getNow();
            if ( channel != null )
            {
                channel.close().awaitUninterruptibly();
            }
        }
    }

    public Future<io.netty.channel.Channel> acquire( Promise<io.netty.channel.Channel> promise )
    {
        Future<io.netty.channel.Channel> fChannelAlias = this.fChannel;
        if ( fChannelAlias != null && fChannelAlias.isSuccess() )
        {
            io.netty.channel.Channel channel = (io.netty.channel.Channel) fChannelAlias.getNow();
            if ( channel != null && channel.isActive() )
            {
                promise.trySuccess( channel );
                return promise;
            }
        }

        return this.acquireSync( promise );
    }

    private synchronized Future<io.netty.channel.Channel> acquireSync( Promise<io.netty.channel.Channel> promise )
    {
        if ( this.fChannel == null )
        {
            return this.fChannel = super.acquire( promise );
        }
        else if ( !this.fChannel.isDone() )
        {
            return NettyUtil.chain( this.fChannel, promise );
        }
        else
        {
            io.netty.channel.Channel channel = (io.netty.channel.Channel) this.fChannel.getNow();
            if ( channel != null && channel.isActive() )
            {
                promise.trySuccess( channel );
                return promise;
            }
            else
            {
                return this.fChannel = super.acquire( promise );
            }
        }
    }

    public Future<Void> release( io.netty.channel.Channel channel, Promise<Void> promise )
    {
        promise.trySuccess( (Object) null );
        return promise;
    }

    public void close()
    {
        super.close();
        closeAwait( this.fChannel );
    }
}
