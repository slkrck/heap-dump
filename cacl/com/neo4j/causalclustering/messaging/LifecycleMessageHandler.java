package com.neo4j.causalclustering.messaging;

import com.neo4j.causalclustering.identity.RaftId;

public interface LifecycleMessageHandler<M extends Message> extends Inbound.MessageHandler<M>
{
    void start( RaftId var1 ) throws Exception;

    void stop() throws Exception;
}
