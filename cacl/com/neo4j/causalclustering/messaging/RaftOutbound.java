package com.neo4j.causalclustering.messaging;

import com.neo4j.causalclustering.core.consensus.RaftMessages;
import com.neo4j.causalclustering.discovery.CoreServerInfo;
import com.neo4j.causalclustering.discovery.CoreTopologyService;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.identity.RaftId;
import com.neo4j.causalclustering.messaging.address.UnknownAddressMonitor;

import java.time.Clock;
import java.util.Optional;
import java.util.function.Supplier;

import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.time.Clocks;

public class RaftOutbound implements Outbound<MemberId,RaftMessages.RaftMessage>
{
    private final CoreTopologyService coreTopologyService;
    private final Outbound<SocketAddress,Message> outbound;
    private final Supplier<Optional<RaftId>> boundRaftId;
    private final UnknownAddressMonitor unknownAddressMonitor;
    private final Log log;
    private final MemberId myself;
    private final Clock clock;
    private final Inbound.MessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> localMessageHandler;

    public RaftOutbound( CoreTopologyService coreTopologyService, Outbound<SocketAddress,Message> outbound,
            Inbound.MessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> localMessageHandler, Supplier<Optional<RaftId>> boundRaftId,
            LogProvider logProvider, long logThresholdMillis, MemberId myself, Clock clock )
    {
        this.coreTopologyService = coreTopologyService;
        this.outbound = outbound;
        this.boundRaftId = boundRaftId;
        this.log = logProvider.getLog( this.getClass() );
        this.unknownAddressMonitor = new UnknownAddressMonitor( this.log, Clocks.systemClock(), logThresholdMillis );
        this.myself = myself;
        this.clock = clock;
        this.localMessageHandler = localMessageHandler;
    }

    public void send( MemberId to, RaftMessages.RaftMessage message, boolean block )
    {
        Optional<RaftId> raftId = (Optional) this.boundRaftId.get();
        if ( raftId.isEmpty() )
        {
            this.log.warn( "Attempting to send a message before bound to a cluster" );
        }
        else
        {
            if ( to.equals( this.myself ) )
            {
                this.localMessageHandler.handle( RaftMessages.ReceivedInstantRaftIdAwareMessage.of( this.clock.instant(), (RaftId) raftId.get(), message ) );
            }
            else
            {
                CoreServerInfo targetCoreInfo = (CoreServerInfo) this.coreTopologyService.allCoreServers().get( to );
                if ( targetCoreInfo != null )
                {
                    this.outbound.send( targetCoreInfo.getRaftServer(), RaftMessages.RaftIdAwareMessage.of( (RaftId) raftId.get(), message ), block );
                }
                else
                {
                    this.unknownAddressMonitor.logAttemptToSendToMemberWithNoKnownAddress( to );
                }
            }
        }
    }
}
