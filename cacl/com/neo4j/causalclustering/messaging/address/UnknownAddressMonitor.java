package com.neo4j.causalclustering.messaging.address;

import com.neo4j.causalclustering.identity.MemberId;

import java.time.Clock;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.neo4j.logging.Log;
import org.neo4j.logging.internal.CappedLogger;

public class UnknownAddressMonitor
{
    private final Log log;
    private final Clock clock;
    private final long timeLimitMs;
    private Map<MemberId,CappedLogger> loggers = new ConcurrentHashMap();

    public UnknownAddressMonitor( Log log, Clock clock, long timeLimitMs )
    {
        this.log = log;
        this.clock = clock;
        this.timeLimitMs = timeLimitMs;
    }

    public void logAttemptToSendToMemberWithNoKnownAddress( MemberId to )
    {
        CappedLogger cappedLogger = (CappedLogger) this.loggers.get( to );
        if ( cappedLogger == null )
        {
            cappedLogger = new CappedLogger( this.log );
            cappedLogger.setTimeLimit( this.timeLimitMs, TimeUnit.MILLISECONDS, this.clock );
            this.loggers.put( to, cappedLogger );
        }

        cappedLogger.info( "No address found for %s, probably because the member has been shut down.", new Object[]{to} );
    }
}
