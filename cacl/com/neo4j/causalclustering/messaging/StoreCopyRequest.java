package com.neo4j.causalclustering.messaging;

import com.neo4j.causalclustering.catchup.RequestMessageType;

import java.util.Objects;

import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.storageengine.api.StoreId;

public abstract class StoreCopyRequest extends CatchupProtocolMessage.WithDatabaseId
{
    private final StoreId expectedStoreId;
    private final long requiredTransactionId;

    protected StoreCopyRequest( RequestMessageType type, DatabaseId databaseId, StoreId expectedStoreId, long requiredTransactionId )
    {
        super( type, databaseId );
        this.expectedStoreId = expectedStoreId;
        this.requiredTransactionId = requiredTransactionId;
    }

    public final StoreId expectedStoreId()
    {
        return this.expectedStoreId;
    }

    public final long requiredTransactionId()
    {
        return this.requiredTransactionId;
    }

    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        else if ( o != null && this.getClass() == o.getClass() )
        {
            if ( !super.equals( o ) )
            {
                return false;
            }
            else
            {
                StoreCopyRequest that = (StoreCopyRequest) o;
                return this.requiredTransactionId == that.requiredTransactionId && Objects.equals( this.expectedStoreId, that.expectedStoreId );
            }
        }
        else
        {
            return false;
        }
    }

    public int hashCode()
    {
        return Objects.hash( new Object[]{super.hashCode(), this.expectedStoreId, this.requiredTransactionId} );
    }
}
