package com.neo4j.causalclustering.messaging.marshalling;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.stream.ChunkedInput;

import java.util.Objects;

import org.neo4j.util.Preconditions;

public class ByteArrayChunkedEncoder implements ChunkedInput<ByteBuf>
{
    private static final int DEFAULT_CHUNK_SIZE = 32768;
    private final byte[] content;
    private int chunkSize;
    private int pos;
    private boolean hasRead;

    ByteArrayChunkedEncoder( byte[] content, int chunkSize )
    {
        Objects.requireNonNull( content, "content cannot be null" );
        Preconditions.requireNonNegative( content.length );
        Preconditions.requirePositive( chunkSize );
        this.content = content;
        this.chunkSize = chunkSize;
    }

    public ByteArrayChunkedEncoder( byte[] content )
    {
        this( content, 32768 );
    }

    private int available()
    {
        return this.content.length - this.pos;
    }

    public boolean isEndOfInput()
    {
        return this.pos == this.content.length && this.hasRead;
    }

    public void close()
    {
        this.pos = this.content.length;
    }

    public ByteBuf readChunk( ChannelHandlerContext ctx )
    {
        return this.readChunk( ctx.alloc() );
    }

    public ByteBuf readChunk( ByteBufAllocator allocator )
    {
        this.hasRead = true;
        if ( this.isEndOfInput() )
        {
            return null;
        }
        else
        {
            int toWrite = Math.min( this.available(), this.chunkSize );
            ByteBuf buffer = allocator.buffer( toWrite );

            try
            {
                buffer.writeBytes( this.content, this.pos, toWrite );
                this.pos += toWrite;
                return buffer;
            }
            catch ( Throwable var5 )
            {
                buffer.release();
                throw var5;
            }
        }
    }

    public long length()
    {
        return (long) this.content.length;
    }

    public long progress()
    {
        return (long) this.pos;
    }
}
