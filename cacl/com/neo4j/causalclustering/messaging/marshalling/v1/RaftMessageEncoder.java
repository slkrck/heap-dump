package com.neo4j.causalclustering.messaging.marshalling.v1;

import com.neo4j.causalclustering.core.consensus.RaftMessages;
import com.neo4j.causalclustering.core.consensus.log.RaftLogEntry;
import com.neo4j.causalclustering.core.replication.ReplicatedContent;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.identity.RaftId;
import com.neo4j.causalclustering.messaging.BoundedNetworkWritableChannel;
import com.neo4j.causalclustering.messaging.NetworkWritableChannel;
import com.neo4j.causalclustering.messaging.marshalling.ChannelMarshal;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import org.neo4j.io.ByteUnit;

public class RaftMessageEncoder extends MessageToByteEncoder<RaftMessages.RaftIdAwareMessage>
{
    private final ChannelMarshal<ReplicatedContent> marshal;

    public RaftMessageEncoder( ChannelMarshal<ReplicatedContent> marshal )
    {
        this.marshal = marshal;
    }

    public synchronized void encode( ChannelHandlerContext ctx, RaftMessages.RaftIdAwareMessage decoratedMessage, ByteBuf out ) throws Exception
    {
        RaftMessages.RaftMessage message = decoratedMessage.message();
        RaftId raftId = decoratedMessage.raftId();
        MemberId.Marshal memberMarshal = new MemberId.Marshal();
        NetworkWritableChannel channel = new NetworkWritableChannel( out );
        RaftId.Marshal.INSTANCE.marshal( (RaftId) raftId, channel );
        channel.putInt( message.type().ordinal() );
        memberMarshal.marshal( (MemberId) message.from(), channel );
        message.dispatch( new RaftMessageEncoder.Handler( this.marshal, memberMarshal, channel ) );
    }

    private static class Handler implements RaftMessages.Handler<Void,Exception>
    {
        private final ChannelMarshal<ReplicatedContent> marshal;
        private final MemberId.Marshal memberMarshal;
        private final NetworkWritableChannel channel;

        Handler( ChannelMarshal<ReplicatedContent> marshal, MemberId.Marshal memberMarshal, NetworkWritableChannel channel )
        {
            this.marshal = marshal;
            this.memberMarshal = memberMarshal;
            this.channel = channel;
        }

        public Void handle( RaftMessages.Vote.Request voteRequest ) throws Exception
        {
            this.memberMarshal.marshal( (MemberId) voteRequest.candidate(), this.channel );
            this.channel.putLong( voteRequest.term() );
            this.channel.putLong( voteRequest.lastLogIndex() );
            this.channel.putLong( voteRequest.lastLogTerm() );
            return null;
        }

        public Void handle( RaftMessages.Vote.Response voteResponse )
        {
            this.channel.putLong( voteResponse.term() );
            this.channel.put( (byte) (voteResponse.voteGranted() ? 1 : 0) );
            return null;
        }

        public Void handle( RaftMessages.PreVote.Request preVoteRequest ) throws Exception
        {
            this.memberMarshal.marshal( (MemberId) preVoteRequest.candidate(), this.channel );
            this.channel.putLong( preVoteRequest.term() );
            this.channel.putLong( preVoteRequest.lastLogIndex() );
            this.channel.putLong( preVoteRequest.lastLogTerm() );
            return null;
        }

        public Void handle( RaftMessages.PreVote.Response preVoteResponse )
        {
            this.channel.putLong( preVoteResponse.term() );
            this.channel.put( (byte) (preVoteResponse.voteGranted() ? 1 : 0) );
            return null;
        }

        public Void handle( RaftMessages.AppendEntries.Request appendRequest ) throws Exception
        {
            this.channel.putLong( appendRequest.leaderTerm() );
            this.channel.putLong( appendRequest.prevLogIndex() );
            this.channel.putLong( appendRequest.prevLogTerm() );
            this.channel.putLong( appendRequest.leaderCommit() );
            this.channel.putLong( (long) appendRequest.entries().length );
            RaftLogEntry[] var2 = appendRequest.entries();
            int var3 = var2.length;

            for ( int var4 = 0; var4 < var3; ++var4 )
            {
                RaftLogEntry raftLogEntry = var2[var4];
                this.channel.putLong( raftLogEntry.term() );
                this.marshal.marshal( raftLogEntry.content(), this.channel );
            }

            return null;
        }

        public Void handle( RaftMessages.AppendEntries.Response appendResponse )
        {
            this.channel.putLong( appendResponse.term() );
            this.channel.put( (byte) (appendResponse.success() ? 1 : 0) );
            this.channel.putLong( appendResponse.matchIndex() );
            this.channel.putLong( appendResponse.appendIndex() );
            return null;
        }

        public Void handle( RaftMessages.NewEntry.Request newEntryRequest ) throws Exception
        {
            BoundedNetworkWritableChannel sizeBoundChannel = new BoundedNetworkWritableChannel( this.channel.byteBuf(), ByteUnit.gibiBytes( 1L ) );
            this.marshal.marshal( newEntryRequest.content(), sizeBoundChannel );
            return null;
        }

        public Void handle( RaftMessages.Heartbeat heartbeat )
        {
            this.channel.putLong( heartbeat.leaderTerm() );
            this.channel.putLong( heartbeat.commitIndexTerm() );
            this.channel.putLong( heartbeat.commitIndex() );
            return null;
        }

        public Void handle( RaftMessages.HeartbeatResponse heartbeatResponse )
        {
            return null;
        }

        public Void handle( RaftMessages.LogCompactionInfo logCompactionInfo )
        {
            this.channel.putLong( logCompactionInfo.leaderTerm() );
            this.channel.putLong( logCompactionInfo.prevIndex() );
            return null;
        }

        public Void handle( RaftMessages.Timeout.Election election )
        {
            return null;
        }

        public Void handle( RaftMessages.Timeout.Heartbeat heartbeat )
        {
            return null;
        }

        public Void handle( RaftMessages.NewEntry.BatchRequest batchRequest )
        {
            return null;
        }

        public Void handle( RaftMessages.PruneRequest pruneRequest )
        {
            return null;
        }
    }
}
