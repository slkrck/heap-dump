package com.neo4j.causalclustering.messaging.marshalling.v1;

import com.neo4j.causalclustering.core.consensus.RaftMessages;
import com.neo4j.causalclustering.core.consensus.log.RaftLogEntry;
import com.neo4j.causalclustering.core.replication.ReplicatedContent;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.identity.RaftId;
import com.neo4j.causalclustering.messaging.EndOfStreamException;
import com.neo4j.causalclustering.messaging.NetworkReadableChannel;
import com.neo4j.causalclustering.messaging.marshalling.ChannelMarshal;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.io.IOException;
import java.time.Clock;
import java.util.List;

import org.neo4j.io.fs.ReadableChannel;

public class RaftMessageDecoder extends ByteToMessageDecoder
{
    private final ChannelMarshal<ReplicatedContent> marshal;
    private final Clock clock;

    public RaftMessageDecoder( ChannelMarshal<ReplicatedContent> marshal, Clock clock )
    {
        this.marshal = marshal;
        this.clock = clock;
    }

    public void decode( ChannelHandlerContext ctx, ByteBuf buffer, List<Object> list ) throws Exception
    {
        ReadableChannel channel = new NetworkReadableChannel( buffer );
        RaftId raftId = (RaftId) RaftId.Marshal.INSTANCE.unmarshal( channel );
        int messageTypeWire = channel.getInt();
        RaftMessages.Type[] values = RaftMessages.Type.values();
        RaftMessages.Type messageType = values[messageTypeWire];
        MemberId from = this.retrieveMember( channel );
        Object result;
        MemberId candidate;
        long term;
        long matchIndex;
        long appendIndex;
        if ( messageType.equals( RaftMessages.Type.VOTE_REQUEST ) )
        {
            candidate = this.retrieveMember( channel );
            term = channel.getLong();
            matchIndex = channel.getLong();
            appendIndex = channel.getLong();
            result = new RaftMessages.Vote.Request( from, term, candidate, matchIndex, appendIndex );
        }
        else
        {
            boolean success;
            long leaderTerm;
            if ( messageType.equals( RaftMessages.Type.VOTE_RESPONSE ) )
            {
                leaderTerm = channel.getLong();
                success = channel.get() == 1;
                result = new RaftMessages.Vote.Response( from, leaderTerm, success );
            }
            else if ( messageType.equals( RaftMessages.Type.PRE_VOTE_REQUEST ) )
            {
                candidate = this.retrieveMember( channel );
                term = channel.getLong();
                matchIndex = channel.getLong();
                appendIndex = channel.getLong();
                result = new RaftMessages.PreVote.Request( from, term, candidate, matchIndex, appendIndex );
            }
            else if ( messageType.equals( RaftMessages.Type.PRE_VOTE_RESPONSE ) )
            {
                leaderTerm = channel.getLong();
                success = channel.get() == 1;
                result = new RaftMessages.PreVote.Response( from, leaderTerm, success );
            }
            else
            {
                long commitIndex;
                long prevIndex;
                if ( messageType.equals( RaftMessages.Type.APPEND_ENTRIES_REQUEST ) )
                {
                    leaderTerm = channel.getLong();
                    prevIndex = channel.getLong();
                    commitIndex = channel.getLong();
                    long leaderCommit = channel.getLong();
                    long count = channel.getLong();
                    RaftLogEntry[] entries = new RaftLogEntry[(int) count];

                    for ( int i = 0; (long) i < count; ++i )
                    {
                        long entryTerm = channel.getLong();
                        ReplicatedContent content = (ReplicatedContent) this.marshal.unmarshal( channel );
                        entries[i] = new RaftLogEntry( entryTerm, content );
                    }

                    result = new RaftMessages.AppendEntries.Request( from, leaderTerm, prevIndex, commitIndex, entries, leaderCommit );
                }
                else if ( messageType.equals( RaftMessages.Type.APPEND_ENTRIES_RESPONSE ) )
                {
                    leaderTerm = channel.getLong();
                    success = channel.get() == 1;
                    matchIndex = channel.getLong();
                    appendIndex = channel.getLong();
                    result = new RaftMessages.AppendEntries.Response( from, leaderTerm, success, matchIndex, appendIndex );
                }
                else if ( messageType.equals( RaftMessages.Type.NEW_ENTRY_REQUEST ) )
                {
                    ReplicatedContent content = (ReplicatedContent) this.marshal.unmarshal( channel );
                    result = new RaftMessages.NewEntry.Request( from, content );
                }
                else if ( messageType.equals( RaftMessages.Type.HEARTBEAT ) )
                {
                    leaderTerm = channel.getLong();
                    prevIndex = channel.getLong();
                    commitIndex = channel.getLong();
                    result = new RaftMessages.Heartbeat( from, leaderTerm, commitIndex, prevIndex );
                }
                else if ( messageType.equals( RaftMessages.Type.HEARTBEAT_RESPONSE ) )
                {
                    result = new RaftMessages.HeartbeatResponse( from );
                }
                else
                {
                    if ( !messageType.equals( RaftMessages.Type.LOG_COMPACTION_INFO ) )
                    {
                        throw new IllegalArgumentException( "Unknown message type" );
                    }

                    leaderTerm = channel.getLong();
                    prevIndex = channel.getLong();
                    result = new RaftMessages.LogCompactionInfo( from, leaderTerm, prevIndex );
                }
            }
        }

        list.add( RaftMessages.ReceivedInstantRaftIdAwareMessage.of( this.clock.instant(), raftId, (RaftMessages.RaftMessage) result ) );
    }

    private MemberId retrieveMember( ReadableChannel buffer ) throws IOException, EndOfStreamException
    {
        MemberId.Marshal memberIdMarshal = new MemberId.Marshal();
        return (MemberId) memberIdMarshal.unmarshal( buffer );
    }
}
