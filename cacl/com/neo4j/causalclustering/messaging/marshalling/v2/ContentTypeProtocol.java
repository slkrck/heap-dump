package com.neo4j.causalclustering.messaging.marshalling.v2;

import com.neo4j.causalclustering.catchup.Protocol;

public class ContentTypeProtocol extends Protocol<ContentType>
{
    public ContentTypeProtocol()
    {
        super( ContentType.ContentType );
    }
}
