package com.neo4j.causalclustering.messaging.marshalling.v2;

public enum ContentType
{
    ContentType( (byte) 0 ),
    ReplicatedContent( (byte) 1 ),
    RaftLogEntryTerms( (byte) 2 ),
    Message( (byte) 3 );

    private final byte messageCode;

    private ContentType( byte messageCode )
    {
        this.messageCode = messageCode;
    }

    public byte get()
    {
        return this.messageCode;
    }
}
