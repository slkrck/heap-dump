package com.neo4j.causalclustering.messaging.marshalling.v2.encoding;

import com.neo4j.causalclustering.core.consensus.RaftMessages;
import com.neo4j.causalclustering.core.consensus.log.RaftLogEntry;
import com.neo4j.causalclustering.core.replication.ReplicatedContent;
import com.neo4j.causalclustering.messaging.marshalling.Codec;
import com.neo4j.causalclustering.messaging.marshalling.v2.ContentType;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;

import java.io.IOException;
import java.util.List;

public class RaftMessageContentEncoder extends MessageToMessageEncoder<RaftMessages.RaftIdAwareMessage>
{
    private final Codec<ReplicatedContent> codec;

    public RaftMessageContentEncoder( Codec<ReplicatedContent> replicatedContentCodec )
    {
        this.codec = replicatedContentCodec;
    }

    protected void encode( ChannelHandlerContext ctx, RaftMessages.RaftIdAwareMessage msg, List<Object> out ) throws Exception
    {
        out.add( msg );
        RaftMessageContentEncoder.Handler replicatedContentHandler = new RaftMessageContentEncoder.Handler( out, ctx.alloc() );
        msg.message().dispatch( replicatedContentHandler );
    }

    private class Handler implements RaftMessages.Handler<Void,Exception>
    {
        private final List<Object> out;
        private final ByteBufAllocator alloc;

        private Handler( List<Object> out, ByteBufAllocator alloc )
        {
            this.out = out;
            this.alloc = alloc;
        }

        public Void handle( RaftMessages.Vote.Request request ) throws Exception
        {
            return null;
        }

        public Void handle( RaftMessages.Vote.Response response ) throws Exception
        {
            return null;
        }

        public Void handle( RaftMessages.PreVote.Request request ) throws Exception
        {
            return null;
        }

        public Void handle( RaftMessages.PreVote.Response response ) throws Exception
        {
            return null;
        }

        public Void handle( RaftMessages.AppendEntries.Request request ) throws Exception
        {
            this.out.add( RaftLogEntryTermsSerializer.serializeTerms( request.entries(), this.alloc ) );
            RaftLogEntry[] var2 = request.entries();
            int var3 = var2.length;

            for ( int var4 = 0; var4 < var3; ++var4 )
            {
                RaftLogEntry entry = var2[var4];
                this.serializableContents( entry.content(), this.out );
            }

            return null;
        }

        public Void handle( RaftMessages.AppendEntries.Response response ) throws Exception
        {
            return null;
        }

        public Void handle( RaftMessages.Heartbeat heartbeat ) throws Exception
        {
            return null;
        }

        public Void handle( RaftMessages.LogCompactionInfo logCompactionInfo ) throws Exception
        {
            return null;
        }

        public Void handle( RaftMessages.HeartbeatResponse heartbeatResponse ) throws Exception
        {
            return null;
        }

        public Void handle( RaftMessages.NewEntry.Request request ) throws Exception
        {
            this.serializableContents( request.content(), this.out );
            return null;
        }

        public Void handle( RaftMessages.Timeout.Election election ) throws Exception
        {
            return this.illegalOutbound( election );
        }

        public Void handle( RaftMessages.Timeout.Heartbeat heartbeat ) throws Exception
        {
            return this.illegalOutbound( heartbeat );
        }

        public Void handle( RaftMessages.NewEntry.BatchRequest batchRequest ) throws Exception
        {
            return this.illegalOutbound( batchRequest );
        }

        public Void handle( RaftMessages.PruneRequest pruneRequest ) throws Exception
        {
            return this.illegalOutbound( pruneRequest );
        }

        private Void illegalOutbound( RaftMessages.BaseRaftMessage raftMessage )
        {
            throw new IllegalStateException( "Illegal outbound call: " + raftMessage.getClass() );
        }

        private void serializableContents( ReplicatedContent content, List<Object> out ) throws IOException
        {
            out.add( ContentType.ReplicatedContent );
            RaftMessageContentEncoder.this.codec.encode( content, out );
        }
    }
}
