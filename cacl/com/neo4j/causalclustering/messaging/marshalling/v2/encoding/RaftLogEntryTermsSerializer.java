package com.neo4j.causalclustering.messaging.marshalling.v2.encoding;

import com.neo4j.causalclustering.core.consensus.log.RaftLogEntry;
import com.neo4j.causalclustering.messaging.marshalling.v2.ContentType;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;

class RaftLogEntryTermsSerializer
{
    static ByteBuf serializeTerms( RaftLogEntry[] raftLogEntries, ByteBufAllocator byteBufAllocator )
    {
        int capacity = 40 + 64 * raftLogEntries.length;
        ByteBuf buffer = byteBufAllocator.buffer( capacity, capacity );
        buffer.writeByte( ContentType.RaftLogEntryTerms.get() );
        buffer.writeInt( raftLogEntries.length );
        RaftLogEntry[] var4 = raftLogEntries;
        int var5 = raftLogEntries.length;

        for ( int var6 = 0; var6 < var5; ++var6 )
        {
            RaftLogEntry raftLogEntry = var4[var6];
            buffer.writeLong( raftLogEntry.term() );
        }

        return buffer;
    }
}
