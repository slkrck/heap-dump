package com.neo4j.causalclustering.messaging.marshalling.v2.encoding;

import com.neo4j.causalclustering.messaging.marshalling.v2.ContentType;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class ContentTypeEncoder extends MessageToByteEncoder<ContentType>
{
    protected void encode( ChannelHandlerContext ctx, ContentType msg, ByteBuf out )
    {
        out.writeByte( msg.get() );
    }
}
