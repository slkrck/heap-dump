package com.neo4j.causalclustering.messaging.marshalling.v2.decoding;

import com.neo4j.causalclustering.catchup.Protocol;
import com.neo4j.causalclustering.catchup.RequestDecoderDispatcher;
import com.neo4j.causalclustering.messaging.marshalling.v2.ContentType;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

import org.neo4j.logging.LogProvider;

public class DecodingDispatcher extends RequestDecoderDispatcher<ContentType>
{
    public DecodingDispatcher( Protocol<ContentType> protocol, LogProvider logProvider )
    {
        super( protocol, logProvider );
        this.register( ContentType.ContentType, new ByteToMessageDecoder()
        {
            protected void decode( ChannelHandlerContext ctx, ByteBuf in, List<Object> out )
            {
                if ( in.readableBytes() > 0 )
                {
                    throw new IllegalStateException( "Not expecting any data here" );
                }
            }
        } );
        this.register( ContentType.RaftLogEntryTerms, new RaftLogEntryTermsDecoder( protocol ) );
        this.register( ContentType.ReplicatedContent, new ReplicatedContentChunkDecoder() );
        this.register( ContentType.Message, new RaftMessageDecoder( protocol ) );
    }
}
