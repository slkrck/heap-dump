package com.neo4j.causalclustering.messaging.marshalling.v2.decoding;

import com.neo4j.causalclustering.catchup.Protocol;
import com.neo4j.causalclustering.messaging.marshalling.v2.ContentType;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;

public class ContentTypeDispatcher extends ChannelInboundHandlerAdapter
{
    private final Protocol<ContentType> contentTypeProtocol;

    public ContentTypeDispatcher( Protocol<ContentType> contentTypeProtocol )
    {
        this.contentTypeProtocol = contentTypeProtocol;
    }

    public void channelRead( ChannelHandlerContext ctx, Object msg )
    {
        if ( msg instanceof ByteBuf )
        {
            ByteBuf buffer = (ByteBuf) msg;
            if ( this.contentTypeProtocol.isExpecting( ContentType.ContentType ) )
            {
                byte messageCode = buffer.readByte();
                ContentType contentType = this.getContentType( messageCode );
                this.contentTypeProtocol.expect( contentType );
                if ( buffer.readableBytes() == 0 )
                {
                    ReferenceCountUtil.release( msg );
                    return;
                }
            }
        }

        ctx.fireChannelRead( msg );
    }

    private ContentType getContentType( byte messageCode )
    {
        ContentType[] var2 = ContentType.values();
        int var3 = var2.length;

        for ( int var4 = 0; var4 < var3; ++var4 )
        {
            ContentType contentType = var2[var4];
            if ( contentType.get() == messageCode )
            {
                return contentType;
            }
        }

        throw new IllegalArgumentException( "Illegal inbound. Could not find a ContentType with value " + messageCode );
    }
}
