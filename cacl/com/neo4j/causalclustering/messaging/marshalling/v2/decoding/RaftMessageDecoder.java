package com.neo4j.causalclustering.messaging.marshalling.v2.decoding;

import com.neo4j.causalclustering.catchup.Protocol;
import com.neo4j.causalclustering.core.consensus.RaftMessages;
import com.neo4j.causalclustering.core.consensus.log.RaftLogEntry;
import com.neo4j.causalclustering.core.replication.ReplicatedContent;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.identity.RaftId;
import com.neo4j.causalclustering.messaging.EndOfStreamException;
import com.neo4j.causalclustering.messaging.NetworkReadableChannel;
import com.neo4j.causalclustering.messaging.marshalling.v2.ContentType;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.io.IOException;
import java.time.Clock;
import java.util.List;
import java.util.Optional;
import java.util.Queue;

import org.neo4j.io.fs.ReadableChannel;

public class RaftMessageDecoder extends ByteToMessageDecoder
{
    private final Protocol<ContentType> protocol;

    RaftMessageDecoder( Protocol<ContentType> protocol )
    {
        this.protocol = protocol;
    }

    public void decode( ChannelHandlerContext ctx, ByteBuf buffer, List<Object> list ) throws Exception
    {
        ReadableChannel channel = new NetworkReadableChannel( buffer );
        RaftId raftId = (RaftId) RaftId.Marshal.INSTANCE.unmarshal( channel );
        int messageTypeWire = channel.getInt();
        RaftMessages.Type[] values = RaftMessages.Type.values();
        RaftMessages.Type messageType = values[messageTypeWire];
        MemberId from = this.retrieveMember( channel );
        Object composer;
        MemberId candidate;
        long term;
        long matchIndex;
        long appendIndex;
        if ( messageType.equals( RaftMessages.Type.VOTE_REQUEST ) )
        {
            candidate = this.retrieveMember( channel );
            term = channel.getLong();
            matchIndex = channel.getLong();
            appendIndex = channel.getLong();
            composer = new RaftMessageDecoder.SimpleMessageComposer( new RaftMessages.Vote.Request( from, term, candidate, matchIndex, appendIndex ) );
        }
        else
        {
            boolean success;
            long leaderTerm;
            if ( messageType.equals( RaftMessages.Type.VOTE_RESPONSE ) )
            {
                leaderTerm = channel.getLong();
                success = channel.get() == 1;
                composer = new RaftMessageDecoder.SimpleMessageComposer( new RaftMessages.Vote.Response( from, leaderTerm, success ) );
            }
            else if ( messageType.equals( RaftMessages.Type.PRE_VOTE_REQUEST ) )
            {
                candidate = this.retrieveMember( channel );
                term = channel.getLong();
                matchIndex = channel.getLong();
                appendIndex = channel.getLong();
                composer = new RaftMessageDecoder.SimpleMessageComposer( new RaftMessages.PreVote.Request( from, term, candidate, matchIndex, appendIndex ) );
            }
            else if ( messageType.equals( RaftMessages.Type.PRE_VOTE_RESPONSE ) )
            {
                leaderTerm = channel.getLong();
                success = channel.get() == 1;
                composer = new RaftMessageDecoder.SimpleMessageComposer( new RaftMessages.PreVote.Response( from, leaderTerm, success ) );
            }
            else
            {
                long commitIndex;
                long prevIndex;
                if ( messageType.equals( RaftMessages.Type.APPEND_ENTRIES_REQUEST ) )
                {
                    leaderTerm = channel.getLong();
                    prevIndex = channel.getLong();
                    commitIndex = channel.getLong();
                    long leaderCommit = channel.getLong();
                    int entryCount = channel.getInt();
                    composer = new RaftMessageDecoder.AppendEntriesComposer( entryCount, from, leaderTerm, prevIndex, commitIndex, leaderCommit );
                }
                else if ( messageType.equals( RaftMessages.Type.APPEND_ENTRIES_RESPONSE ) )
                {
                    leaderTerm = channel.getLong();
                    success = channel.get() == 1;
                    matchIndex = channel.getLong();
                    appendIndex = channel.getLong();
                    composer = new RaftMessageDecoder.SimpleMessageComposer(
                            new RaftMessages.AppendEntries.Response( from, leaderTerm, success, matchIndex, appendIndex ) );
                }
                else if ( messageType.equals( RaftMessages.Type.NEW_ENTRY_REQUEST ) )
                {
                    composer = new RaftMessageDecoder.NewEntryRequestComposer( from );
                }
                else if ( messageType.equals( RaftMessages.Type.HEARTBEAT ) )
                {
                    leaderTerm = channel.getLong();
                    prevIndex = channel.getLong();
                    commitIndex = channel.getLong();
                    composer = new RaftMessageDecoder.SimpleMessageComposer( new RaftMessages.Heartbeat( from, leaderTerm, commitIndex, prevIndex ) );
                }
                else if ( messageType.equals( RaftMessages.Type.HEARTBEAT_RESPONSE ) )
                {
                    composer = new RaftMessageDecoder.SimpleMessageComposer( new RaftMessages.HeartbeatResponse( from ) );
                }
                else
                {
                    if ( !messageType.equals( RaftMessages.Type.LOG_COMPACTION_INFO ) )
                    {
                        throw new IllegalArgumentException( "Unknown message type" );
                    }

                    leaderTerm = channel.getLong();
                    prevIndex = channel.getLong();
                    composer = new RaftMessageDecoder.SimpleMessageComposer( new RaftMessages.LogCompactionInfo( from, leaderTerm, prevIndex ) );
                }
            }
        }

        list.add( new RaftMessageDecoder.RaftIdAwareMessageComposer( (RaftMessageDecoder.LazyComposer) composer, raftId ) );
        this.protocol.expect( ContentType.ContentType );
    }

    private MemberId retrieveMember( ReadableChannel buffer ) throws IOException, EndOfStreamException
    {
        MemberId.Marshal memberIdMarshal = new MemberId.Marshal();
        return (MemberId) memberIdMarshal.unmarshal( buffer );
    }

    interface LazyComposer
    {
        Optional<RaftMessages.RaftMessage> maybeComplete( Queue<Long> var1, Queue<ReplicatedContent> var2 );
    }

    private static class NewEntryRequestComposer implements RaftMessageDecoder.LazyComposer
    {
        private final MemberId from;

        NewEntryRequestComposer( MemberId from )
        {
            this.from = from;
        }

        public Optional<RaftMessages.RaftMessage> maybeComplete( Queue<Long> terms, Queue<ReplicatedContent> contents )
        {
            return contents.isEmpty() ? Optional.empty() : Optional.of( new RaftMessages.NewEntry.Request( this.from, (ReplicatedContent) contents.remove() ) );
        }
    }

    private static class AppendEntriesComposer implements RaftMessageDecoder.LazyComposer
    {
        private final int entryCount;
        private final MemberId from;
        private final long term;
        private final long prevLogIndex;
        private final long prevLogTerm;
        private final long leaderCommit;

        AppendEntriesComposer( int entryCount, MemberId from, long term, long prevLogIndex, long prevLogTerm, long leaderCommit )
        {
            this.entryCount = entryCount;
            this.from = from;
            this.term = term;
            this.prevLogIndex = prevLogIndex;
            this.prevLogTerm = prevLogTerm;
            this.leaderCommit = leaderCommit;
        }

        public Optional<RaftMessages.RaftMessage> maybeComplete( Queue<Long> terms, Queue<ReplicatedContent> contents )
        {
            if ( terms.size() >= this.entryCount && contents.size() >= this.entryCount )
            {
                RaftLogEntry[] entries = new RaftLogEntry[this.entryCount];

                for ( int i = 0; i < this.entryCount; ++i )
                {
                    long term = (Long) terms.remove();
                    ReplicatedContent content = (ReplicatedContent) contents.remove();
                    entries[i] = new RaftLogEntry( term, content );
                }

                return Optional.of(
                        new RaftMessages.AppendEntries.Request( this.from, this.term, this.prevLogIndex, this.prevLogTerm, entries, this.leaderCommit ) );
            }
            else
            {
                return Optional.empty();
            }
        }
    }

    private static class SimpleMessageComposer implements RaftMessageDecoder.LazyComposer
    {
        private final RaftMessages.RaftMessage message;

        private SimpleMessageComposer( RaftMessages.RaftMessage message )
        {
            this.message = message;
        }

        public Optional<RaftMessages.RaftMessage> maybeComplete( Queue<Long> terms, Queue<ReplicatedContent> contents )
        {
            return Optional.of( this.message );
        }
    }

    static class RaftIdAwareMessageComposer
    {
        private final RaftMessageDecoder.LazyComposer composer;
        private final RaftId raftId;

        RaftIdAwareMessageComposer( RaftMessageDecoder.LazyComposer composer, RaftId raftId )
        {
            this.composer = composer;
            this.raftId = raftId;
        }

        Optional<RaftMessages.RaftIdAwareMessage> maybeCompose( Clock clock, Queue<Long> terms, Queue<ReplicatedContent> contents )
        {
            return this.composer.maybeComplete( terms, contents ).map( ( m ) -> {
                return RaftMessages.ReceivedInstantRaftIdAwareMessage.of( clock.instant(), this.raftId, m );
            } );
        }
    }
}
