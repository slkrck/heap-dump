package com.neo4j.causalclustering.messaging.marshalling.v2.decoding;

import com.neo4j.causalclustering.core.replication.ReplicatedContent;
import com.neo4j.causalclustering.messaging.marshalling.Codec;
import com.neo4j.causalclustering.messaging.marshalling.ReplicatedContentCodec;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import io.netty.handler.codec.ByteToMessageDecoder.Cumulator;

import java.util.List;

public class ReplicatedContentChunkDecoder extends ByteToMessageDecoder
{
    private final Codec<ReplicatedContent> codec;
    private boolean isLast;

    ReplicatedContentChunkDecoder()
    {
        this.setCumulator( new ReplicatedContentChunkDecoder.ContentChunkCumulator() );
        this.codec = new ReplicatedContentCodec();
    }

    protected void decode( ChannelHandlerContext ctx, ByteBuf in, List<Object> out ) throws Exception
    {
        if ( this.isLast )
        {
            out.add( this.codec.decode( in ) );
            this.isLast = false;
        }
    }

    private class ContentChunkCumulator implements Cumulator
    {
        public ByteBuf cumulate( ByteBufAllocator alloc, ByteBuf cumulation, ByteBuf in )
        {
            ReplicatedContentChunkDecoder.this.isLast = in.readBoolean();
            return ByteToMessageDecoder.COMPOSITE_CUMULATOR.cumulate( alloc, cumulation, in );
        }
    }
}
