package com.neo4j.causalclustering.messaging.marshalling.v2.decoding;

import com.neo4j.causalclustering.core.consensus.RaftMessages;
import com.neo4j.causalclustering.core.replication.ReplicatedContent;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;

import java.time.Clock;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Queue;

public class RaftMessageComposer extends MessageToMessageDecoder<Object>
{
    private final Queue<ReplicatedContent> replicatedContents = new LinkedList();
    private final Queue<Long> raftLogEntryTerms = new LinkedList();
    private final Clock clock;
    private RaftMessageDecoder.RaftIdAwareMessageComposer messageComposer;

    public RaftMessageComposer( Clock clock )
    {
        this.clock = clock;
    }

    protected void decode( ChannelHandlerContext ctx, Object msg, List<Object> out )
    {
        if ( msg instanceof ReplicatedContent )
        {
            this.replicatedContents.add( (ReplicatedContent) msg );
        }
        else if ( msg instanceof RaftLogEntryTermsDecoder.RaftLogEntryTerms )
        {
            long[] var4 = ((RaftLogEntryTermsDecoder.RaftLogEntryTerms) msg).terms();
            int var5 = var4.length;

            for ( int var6 = 0; var6 < var5; ++var6 )
            {
                long term = var4[var6];
                this.raftLogEntryTerms.add( term );
            }
        }
        else
        {
            if ( !(msg instanceof RaftMessageDecoder.RaftIdAwareMessageComposer) )
            {
                throw new IllegalStateException( "Unexpected object in the pipeline: " + msg );
            }

            if ( this.messageComposer != null )
            {
                throw new IllegalStateException( "Received raft message header. Pipeline already contains message header waiting to build." );
            }

            this.messageComposer = (RaftMessageDecoder.RaftIdAwareMessageComposer) msg;
        }

        if ( this.messageComposer != null )
        {
            Optional<RaftMessages.RaftIdAwareMessage> raftIdAwareMessage =
                    this.messageComposer.maybeCompose( this.clock, this.raftLogEntryTerms, this.replicatedContents );
            raftIdAwareMessage.ifPresent( ( message ) -> {
                this.clear( message );
                out.add( message );
            } );
        }
    }

    private void clear( RaftMessages.RaftIdAwareMessage message )
    {
        this.messageComposer = null;
        if ( !this.replicatedContents.isEmpty() || !this.raftLogEntryTerms.isEmpty() )
        {
            throw new IllegalStateException( String.format(
                    "Message [%s] was composed without using all resources in the pipeline. Pipeline still contains Replicated contents[%s] and RaftLogEntryTerms [%s]",
                    message, this.stringify( this.replicatedContents ), this.stringify( this.raftLogEntryTerms ) ) );
        }
    }

    private String stringify( Iterable<?> objects )
    {
        StringBuilder stringBuilder = new StringBuilder();
        Iterator iterator = objects.iterator();

        while ( iterator.hasNext() )
        {
            stringBuilder.append( iterator.next() );
            if ( iterator.hasNext() )
            {
                stringBuilder.append( ", " );
            }
        }

        return stringBuilder.toString();
    }
}
