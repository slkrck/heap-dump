package com.neo4j.causalclustering.messaging.marshalling;

import com.neo4j.causalclustering.core.consensus.NewLeaderBarrier;
import com.neo4j.causalclustering.core.consensus.membership.MemberIdSet;
import com.neo4j.causalclustering.core.replication.DistributedOperation;
import com.neo4j.causalclustering.core.state.machines.dummy.DummyRequest;
import com.neo4j.causalclustering.core.state.machines.lease.ReplicatedLeaseRequest;
import com.neo4j.causalclustering.core.state.machines.token.ReplicatedTokenRequest;
import com.neo4j.causalclustering.core.state.machines.tx.ByteArrayReplicatedTransaction;
import com.neo4j.causalclustering.core.state.machines.tx.TransactionRepresentationReplicatedTransaction;

import java.io.IOException;

public interface ReplicatedContentHandler
{
    void handle( ByteArrayReplicatedTransaction var1 ) throws IOException;

    void handle( TransactionRepresentationReplicatedTransaction var1 ) throws IOException;

    void handle( MemberIdSet var1 ) throws IOException;

    void handle( ReplicatedTokenRequest var1 ) throws IOException;

    void handle( NewLeaderBarrier var1 ) throws IOException;

    void handle( ReplicatedLeaseRequest var1 ) throws IOException;

    void handle( DistributedOperation var1 ) throws IOException;

    void handle( DummyRequest var1 ) throws IOException;
}
