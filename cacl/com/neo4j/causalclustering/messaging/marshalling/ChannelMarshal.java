package com.neo4j.causalclustering.messaging.marshalling;

import com.neo4j.causalclustering.messaging.EndOfStreamException;

import java.io.IOException;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public interface ChannelMarshal<STATE>
{
    void marshal( STATE var1, WritableChannel var2 ) throws IOException;

    STATE unmarshal( ReadableChannel var1 ) throws IOException, EndOfStreamException;
}
