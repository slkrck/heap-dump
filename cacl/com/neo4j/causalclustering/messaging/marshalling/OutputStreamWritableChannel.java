package com.neo4j.causalclustering.messaging.marshalling;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.neo4j.io.fs.WritableChannel;

public class OutputStreamWritableChannel implements WritableChannel
{
    private final DataOutputStream dataOutputStream;

    public OutputStreamWritableChannel( OutputStream outputStream )
    {
        this.dataOutputStream = new DataOutputStream( outputStream );
    }

    public WritableChannel put( byte value ) throws IOException
    {
        this.dataOutputStream.writeByte( value );
        return this;
    }

    public WritableChannel putShort( short value ) throws IOException
    {
        this.dataOutputStream.writeShort( value );
        return this;
    }

    public WritableChannel putInt( int value ) throws IOException
    {
        this.dataOutputStream.writeInt( value );
        return this;
    }

    public WritableChannel putLong( long value ) throws IOException
    {
        this.dataOutputStream.writeLong( value );
        return this;
    }

    public WritableChannel putFloat( float value ) throws IOException
    {
        this.dataOutputStream.writeFloat( value );
        return this;
    }

    public WritableChannel putDouble( double value ) throws IOException
    {
        this.dataOutputStream.writeDouble( value );
        return this;
    }

    public WritableChannel put( byte[] value, int length ) throws IOException
    {
        this.dataOutputStream.write( value, 0, length );
        return this;
    }
}
