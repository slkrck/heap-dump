package com.neo4j.causalclustering.messaging.marshalling;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.neo4j.io.fs.ReadableChannel;

public class InputStreamReadableChannel implements ReadableChannel
{
    private final DataInputStream dataInputStream;

    public InputStreamReadableChannel( InputStream inputStream )
    {
        this.dataInputStream = new DataInputStream( inputStream );
    }

    public byte get() throws IOException
    {
        return this.dataInputStream.readByte();
    }

    public short getShort() throws IOException
    {
        return this.dataInputStream.readShort();
    }

    public int getInt() throws IOException
    {
        return this.dataInputStream.readInt();
    }

    public long getLong() throws IOException
    {
        return this.dataInputStream.readLong();
    }

    public float getFloat() throws IOException
    {
        return this.dataInputStream.readFloat();
    }

    public double getDouble() throws IOException
    {
        return this.dataInputStream.readDouble();
    }

    public void get( byte[] bytes, int length ) throws IOException
    {
        this.dataInputStream.read( bytes, 0, length );
    }

    public void close() throws IOException
    {
        this.dataInputStream.close();
    }
}
