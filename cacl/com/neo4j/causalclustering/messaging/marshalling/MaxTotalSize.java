package com.neo4j.causalclustering.messaging.marshalling;

import com.neo4j.causalclustering.messaging.MessageTooBigException;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.stream.ChunkedInput;
import org.neo4j.io.ByteUnit;
import org.neo4j.util.Preconditions;

public class MaxTotalSize implements ChunkedInput<ByteBuf>
{
    private static final int DEFAULT_MAX_SIZE = (int) ByteUnit.gibiBytes( 1L );
    private final ChunkedInput<ByteBuf> chunkedInput;
    private final int maxSize;
    private int totalSize;

    MaxTotalSize( ChunkedInput<ByteBuf> chunkedInput, int maxSize )
    {
        Preconditions.requirePositive( maxSize );
        this.chunkedInput = chunkedInput;
        this.maxSize = maxSize;
    }

    MaxTotalSize( ChunkedInput<ByteBuf> chunkedInput )
    {
        this( chunkedInput, DEFAULT_MAX_SIZE );
    }

    public boolean isEndOfInput() throws Exception
    {
        return this.chunkedInput.isEndOfInput();
    }

    public void close() throws Exception
    {
        this.chunkedInput.close();
    }

    public ByteBuf readChunk( ChannelHandlerContext ctx ) throws Exception
    {
        return this.readChunk( ctx.alloc() );
    }

    public ByteBuf readChunk( ByteBufAllocator allocator ) throws Exception
    {
        ByteBuf byteBuf = (ByteBuf) this.chunkedInput.readChunk( allocator );
        if ( byteBuf != null )
        {
            int additionalBytes = byteBuf.readableBytes();
            this.totalSize += additionalBytes;
            if ( this.totalSize > this.maxSize )
            {
                throw new MessageTooBigException(
                        String.format( "Size limit exceeded. Limit is %d, wanted to write %d, written so far %d", this.maxSize, additionalBytes,
                                this.totalSize - additionalBytes ) );
            }
        }

        return byteBuf;
    }

    public long length()
    {
        return this.chunkedInput.length();
    }

    public long progress()
    {
        return this.chunkedInput.progress();
    }
}
