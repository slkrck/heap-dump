package com.neo4j.causalclustering.messaging.marshalling;

import com.neo4j.causalclustering.core.state.machines.tx.ByteArrayReplicatedTransaction;
import com.neo4j.causalclustering.discovery.akka.marshal.DatabaseIdWithoutNameMarshal;
import com.neo4j.causalclustering.messaging.NetworkWritableChannel;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.stream.ChunkedInput;
import org.neo4j.kernel.database.DatabaseId;

public class ByteArrayTransactionChunker implements ChunkedInput<ByteBuf>
{
    private final DatabaseId databaseId;
    private final ByteArrayChunkedEncoder byteChunker;
    private boolean dbNameWritten;

    public ByteArrayTransactionChunker( ByteArrayReplicatedTransaction tx )
    {
        this.byteChunker = new ByteArrayChunkedEncoder( tx.getTxBytes() );
        this.databaseId = tx.databaseId();
    }

    public boolean isEndOfInput()
    {
        return this.byteChunker.isEndOfInput();
    }

    public void close()
    {
        this.byteChunker.close();
    }

    public ByteBuf readChunk( ChannelHandlerContext ctx )
    {
        return this.readChunk( ctx.alloc() );
    }

    public ByteBuf readChunk( ByteBufAllocator allocator )
    {
        if ( this.isEndOfInput() )
        {
            return null;
        }
        else if ( !this.dbNameWritten )
        {
            ByteBuf buffer = allocator.buffer();

            try
            {
                DatabaseIdWithoutNameMarshal.INSTANCE.marshal( (DatabaseId) this.databaseId, new NetworkWritableChannel( buffer ) );
                this.dbNameWritten = true;
                return buffer;
            }
            catch ( Throwable var4 )
            {
                buffer.release();
                throw new RuntimeException( var4 );
            }
        }
        else
        {
            return this.byteChunker.readChunk( allocator );
        }
    }

    public long length()
    {
        return this.byteChunker.length();
    }

    public long progress()
    {
        return this.byteChunker.progress();
    }
}
