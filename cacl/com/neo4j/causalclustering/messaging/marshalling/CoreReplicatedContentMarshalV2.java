package com.neo4j.causalclustering.messaging.marshalling;

import com.neo4j.causalclustering.core.consensus.NewLeaderBarrier;
import com.neo4j.causalclustering.core.consensus.membership.MemberIdSet;
import com.neo4j.causalclustering.core.consensus.membership.MemberIdSetSerializer;
import com.neo4j.causalclustering.core.replication.DistributedOperation;
import com.neo4j.causalclustering.core.replication.ReplicatedContent;
import com.neo4j.causalclustering.core.state.machines.dummy.DummyRequest;
import com.neo4j.causalclustering.core.state.machines.lease.ReplicatedLeaseMarshalV2;
import com.neo4j.causalclustering.core.state.machines.lease.ReplicatedLeaseRequest;
import com.neo4j.causalclustering.core.state.machines.token.ReplicatedTokenRequest;
import com.neo4j.causalclustering.core.state.machines.token.ReplicatedTokenRequestMarshalV2;
import com.neo4j.causalclustering.core.state.machines.tx.ByteArrayReplicatedTransaction;
import com.neo4j.causalclustering.core.state.machines.tx.ReplicatedTransactionMarshalV2;
import com.neo4j.causalclustering.core.state.machines.tx.TransactionRepresentationReplicatedTransaction;
import com.neo4j.causalclustering.core.state.storage.SafeChannelMarshal;
import com.neo4j.causalclustering.messaging.EndOfStreamException;

import java.io.IOException;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class CoreReplicatedContentMarshalV2 extends SafeChannelMarshal<ReplicatedContent>
{
    public static ContentBuilder<ReplicatedContent> unmarshal( byte contentType, ReadableChannel channel ) throws IOException, EndOfStreamException
    {
        switch ( contentType )
        {
        case 0:
            return ContentBuilder.finished( ReplicatedTransactionMarshalV2.unmarshal( channel ) );
        case 1:
            return ContentBuilder.finished( MemberIdSetSerializer.unmarshal( channel ) );
        case 2:
        case 3:
        default:
            throw new IllegalStateException( "Not a recognized content type: " + contentType );
        case 4:
            return ContentBuilder.finished( ReplicatedTokenRequestMarshalV2.unmarshal( channel ) );
        case 5:
            return ContentBuilder.finished( new NewLeaderBarrier() );
        case 6:
            return ContentBuilder.finished( ReplicatedLeaseMarshalV2.unmarshal( channel ) );
        case 7:
            return DistributedOperation.deserialize( channel );
        case 8:
            return ContentBuilder.finished( (ReplicatedContent) DummyRequest.Marshal.INSTANCE.unmarshal( channel ) );
        }
    }

    public void marshal( ReplicatedContent replicatedContent, WritableChannel channel ) throws IOException
    {
        replicatedContent.dispatch( new CoreReplicatedContentMarshalV2.MarshallingHandler( channel ) );
    }

    protected ReplicatedContent unmarshal0( ReadableChannel channel ) throws IOException, EndOfStreamException
    {
        byte type = channel.get();

        ContentBuilder contentBuilder;
        for ( contentBuilder = unmarshal( type, channel ); !contentBuilder.isComplete(); contentBuilder = contentBuilder.combine( unmarshal( type, channel ) ) )
        {
            type = channel.get();
        }

        return (ReplicatedContent) contentBuilder.build();
    }

    private static class MarshallingHandler implements ReplicatedContentHandler
    {
        private final WritableChannel writableChannel;

        MarshallingHandler( WritableChannel writableChannel )
        {
            this.writableChannel = writableChannel;
        }

        public void handle( ByteArrayReplicatedTransaction tx ) throws IOException
        {
            this.writableChannel.put( (byte) 0 );
            ReplicatedTransactionMarshalV2.marshal( this.writableChannel, tx );
        }

        public void handle( TransactionRepresentationReplicatedTransaction tx ) throws IOException
        {
            this.writableChannel.put( (byte) 0 );
            ReplicatedTransactionMarshalV2.marshal( this.writableChannel, tx );
        }

        public void handle( MemberIdSet memberIdSet ) throws IOException
        {
            this.writableChannel.put( (byte) 1 );
            MemberIdSetSerializer.marshal( memberIdSet, this.writableChannel );
        }

        public void handle( ReplicatedTokenRequest replicatedTokenRequest ) throws IOException
        {
            this.writableChannel.put( (byte) 4 );
            ReplicatedTokenRequestMarshalV2.marshal( replicatedTokenRequest, this.writableChannel );
        }

        public void handle( NewLeaderBarrier newLeaderBarrier ) throws IOException
        {
            this.writableChannel.put( (byte) 5 );
        }

        public void handle( ReplicatedLeaseRequest replicatedLeaseRequest ) throws IOException
        {
            this.writableChannel.put( (byte) 6 );
            ReplicatedLeaseMarshalV2.marshal( replicatedLeaseRequest, this.writableChannel );
        }

        public void handle( DistributedOperation distributedOperation ) throws IOException
        {
            this.writableChannel.put( (byte) 7 );
            distributedOperation.marshalMetaData( this.writableChannel );
        }

        public void handle( DummyRequest dummyRequest ) throws IOException
        {
            this.writableChannel.put( (byte) 8 );
            DummyRequest.Marshal.INSTANCE.marshal( dummyRequest, this.writableChannel );
        }
    }
}
