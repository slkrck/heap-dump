package com.neo4j.causalclustering.messaging.marshalling;

import com.neo4j.causalclustering.core.consensus.NewLeaderBarrier;
import com.neo4j.causalclustering.core.consensus.membership.MemberIdSet;
import com.neo4j.causalclustering.core.consensus.membership.MemberIdSetSerializer;
import com.neo4j.causalclustering.core.replication.DistributedOperation;
import com.neo4j.causalclustering.core.replication.ReplicatedContent;
import com.neo4j.causalclustering.core.state.machines.dummy.DummyRequest;
import com.neo4j.causalclustering.core.state.machines.lease.ReplicatedLeaseMarshalV2;
import com.neo4j.causalclustering.core.state.machines.lease.ReplicatedLeaseRequest;
import com.neo4j.causalclustering.core.state.machines.token.ReplicatedTokenRequest;
import com.neo4j.causalclustering.core.state.machines.token.ReplicatedTokenRequestMarshalV2;
import com.neo4j.causalclustering.core.state.machines.tx.ByteArrayReplicatedTransaction;
import com.neo4j.causalclustering.core.state.machines.tx.ReplicatedTransaction;
import com.neo4j.causalclustering.core.state.machines.tx.TransactionRepresentationReplicatedTransaction;
import com.neo4j.causalclustering.discovery.akka.marshal.DatabaseIdWithoutNameMarshal;
import com.neo4j.causalclustering.messaging.EndOfStreamException;
import com.neo4j.causalclustering.messaging.NetworkReadableChannel;
import io.netty.buffer.ByteBuf;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

import org.neo4j.kernel.database.DatabaseId;

public class ReplicatedContentCodec implements Codec<ReplicatedContent>
{
    private static ReplicatedTransaction decodeTx( ByteBuf byteBuf ) throws IOException, EndOfStreamException
    {
        DatabaseId databaseId = (DatabaseId) DatabaseIdWithoutNameMarshal.INSTANCE.unmarshal( new NetworkReadableChannel( byteBuf ) );
        int length = byteBuf.readableBytes();
        byte[] bytes = new byte[length];
        byteBuf.readBytes( bytes );
        return ReplicatedTransaction.from( bytes, databaseId );
    }

    public void encode( ReplicatedContent type, List<Object> output ) throws IOException
    {
        type.dispatch( new ReplicatedContentCodec.ChunkingHandler( output ) );
    }

    public ContentBuilder<ReplicatedContent> decode( ByteBuf byteBuf ) throws IOException, EndOfStreamException
    {
        byte contentType = byteBuf.readByte();
        return this.unmarshal( contentType, byteBuf );
    }

    private ContentBuilder<ReplicatedContent> unmarshal( byte contentType, ByteBuf buffer ) throws IOException, EndOfStreamException
    {
        switch ( contentType )
        {
        case 0:
            return ContentBuilder.finished( decodeTx( buffer ) );
        case 8:
            return ContentBuilder.finished( DummyRequest.decode( buffer ) );
        default:
            return CoreReplicatedContentMarshalV2.unmarshal( contentType, new NetworkReadableChannel( buffer ) );
        }
    }

    private static class ChunkingHandler implements ReplicatedContentHandler
    {
        private final List<Object> output;

        ChunkingHandler( List<Object> output )
        {
            this.output = output;
        }

        public void handle( ByteArrayReplicatedTransaction replicatedTransaction )
        {
            this.output.add( ChunkedReplicatedContent.chunked( (byte) 0, new MaxTotalSize( replicatedTransaction.encode() ) ) );
        }

        public void handle( TransactionRepresentationReplicatedTransaction replicatedTransaction )
        {
            this.output.add( ChunkedReplicatedContent.chunked( (byte) 0, new MaxTotalSize( replicatedTransaction.encode() ) ) );
        }

        public void handle( MemberIdSet memberIdSet )
        {
            this.output.add( ChunkedReplicatedContent.single( (byte) 1, ( channel ) -> {
                MemberIdSetSerializer.marshal( memberIdSet, channel );
            } ) );
        }

        public void handle( ReplicatedTokenRequest replicatedTokenRequest )
        {
            this.output.add( ChunkedReplicatedContent.single( (byte) 4, ( channel ) -> {
                ReplicatedTokenRequestMarshalV2.marshal( replicatedTokenRequest, channel );
            } ) );
        }

        public void handle( NewLeaderBarrier newLeaderBarrier )
        {
            this.output.add( ChunkedReplicatedContent.single( (byte) 5, ( channel ) -> {
            } ) );
        }

        public void handle( ReplicatedLeaseRequest replicatedLeaseRequest )
        {
            this.output.add( ChunkedReplicatedContent.single( (byte) 6, ( channel ) -> {
                ReplicatedLeaseMarshalV2.marshal( replicatedLeaseRequest, channel );
            } ) );
        }

        public void handle( DistributedOperation distributedOperation )
        {
            List var10000 = this.output;
            Objects.requireNonNull( distributedOperation );
            var10000.add( ChunkedReplicatedContent.single( (byte) 7, distributedOperation::marshalMetaData ) );
        }

        public void handle( DummyRequest dummyRequest )
        {
            this.output.add( ChunkedReplicatedContent.chunked( (byte) 8, dummyRequest.encoder() ) );
        }
    }
}
