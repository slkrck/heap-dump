package com.neo4j.causalclustering.messaging.marshalling;

import io.netty.buffer.ByteBuf;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class StringMarshal
{
    private static final int NULL_STRING_LENGTH = -1;

    private StringMarshal()
    {
    }

    public static void marshal( ByteBuf buffer, String string )
    {
        if ( string == null )
        {
            buffer.writeInt( -1 );
        }
        else
        {
            byte[] bytes = string.getBytes( StandardCharsets.UTF_8 );
            buffer.writeInt( bytes.length );
            buffer.writeBytes( bytes );
        }
    }

    public static void marshal( ByteBuffer buffer, String string )
    {
        if ( string == null )
        {
            buffer.putInt( -1 );
        }
        else
        {
            byte[] bytes = string.getBytes( StandardCharsets.UTF_8 );
            buffer.putInt( bytes.length );
            buffer.put( bytes );
        }
    }

    public static String unmarshal( ByteBuf buffer )
    {
        int len = buffer.readInt();
        if ( len == -1 )
        {
            return null;
        }
        else
        {
            byte[] bytes = new byte[len];
            buffer.readBytes( bytes );
            return new String( bytes, StandardCharsets.UTF_8 );
        }
    }

    public static void marshal( WritableChannel channel, String string ) throws IOException
    {
        if ( string == null )
        {
            channel.putInt( -1 );
        }
        else
        {
            byte[] bytes = string.getBytes( StandardCharsets.UTF_8 );
            channel.putInt( bytes.length );
            channel.put( bytes, bytes.length );
        }
    }

    public static String unmarshal( ReadableChannel channel ) throws IOException
    {
        int len = channel.getInt();
        if ( len == -1 )
        {
            return null;
        }
        else
        {
            byte[] stringBytes = new byte[len];
            channel.get( stringBytes, stringBytes.length );
            return new String( stringBytes, StandardCharsets.UTF_8 );
        }
    }
}
