package com.neo4j.causalclustering.messaging.marshalling;

import com.neo4j.causalclustering.messaging.EndOfStreamException;
import io.netty.buffer.ByteBuf;

import java.io.IOException;
import java.util.List;

public interface Codec<CONTENT>
{
    void encode( CONTENT var1, List<Object> var2 ) throws IOException;

    ContentBuilder<CONTENT> decode( ByteBuf var1 ) throws IOException, EndOfStreamException;
}
