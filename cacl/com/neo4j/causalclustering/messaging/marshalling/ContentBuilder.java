package com.neo4j.causalclustering.messaging.marshalling;

import java.util.function.Function;

public class ContentBuilder<CONTENT>
{
    private boolean isComplete;
    private Function<CONTENT,CONTENT> contentFunction;

    private ContentBuilder( Function<CONTENT,CONTENT> contentFunction, boolean isComplete )
    {
        this.contentFunction = contentFunction;
        this.isComplete = isComplete;
    }

    public static <C> ContentBuilder<C> emptyUnfinished()
    {
        return new ContentBuilder( ( content ) -> {
            return content;
        }, false );
    }

    public static <C> ContentBuilder<C> unfinished( Function<C,C> contentFunction )
    {
        return new ContentBuilder( contentFunction, false );
    }

    public static <C> ContentBuilder<C> finished( C content )
    {
        return new ContentBuilder( ( ignored ) -> {
            return content;
        }, true );
    }

    public boolean isComplete()
    {
        return this.isComplete;
    }

    public ContentBuilder<CONTENT> combine( ContentBuilder<CONTENT> contentBuilder )
    {
        if ( this.isComplete )
        {
            throw new IllegalStateException( "This content builder has already completed and cannot be combined." );
        }
        else
        {
            this.contentFunction = this.contentFunction.compose( contentBuilder.contentFunction );
            this.isComplete = contentBuilder.isComplete;
            return this;
        }
    }

    public CONTENT build()
    {
        if ( !this.isComplete )
        {
            throw new IllegalStateException( "Cannot build unfinished content" );
        }
        else
        {
            return this.contentFunction.apply( (Object) null );
        }
    }
}
