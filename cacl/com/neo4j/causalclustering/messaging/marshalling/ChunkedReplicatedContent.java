package com.neo4j.causalclustering.messaging.marshalling;

import com.neo4j.causalclustering.messaging.BoundedNetworkWritableChannel;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.CompositeByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.stream.ChunkedInput;

import java.io.IOException;

import org.neo4j.function.ThrowingConsumer;
import org.neo4j.io.fs.WritableChannel;

public class ChunkedReplicatedContent implements ChunkedInput<ByteBuf>
{
    private static final int METADATA_SIZE = 1;
    private final byte contentType;
    private final ChunkedInput<ByteBuf> byteBufAwareMarshal;
    private boolean endOfInput;
    private int progress;

    private ChunkedReplicatedContent( byte contentType, ChunkedInput<ByteBuf> byteBufAwareMarshal )
    {
        this.byteBufAwareMarshal = byteBufAwareMarshal;
        this.contentType = contentType;
    }

    static ChunkedInput<ByteBuf> single( byte contentType, ThrowingConsumer<WritableChannel,IOException> marshaller )
    {
        return chunked( contentType, new ChunkedReplicatedContent.Single( marshaller ) );
    }

    static ChunkedInput<ByteBuf> chunked( byte contentType, ChunkedInput<ByteBuf> chunkedInput )
    {
        return new ChunkedReplicatedContent( contentType, chunkedInput );
    }

    private static int metadataSize( boolean isFirstChunk )
    {
        return 1 + (isFirstChunk ? 1 : 0);
    }

    private static ByteBuf writeMetadata( boolean isFirstChunk, boolean isLastChunk, byte contentType, ByteBuf buffer )
    {
        buffer.writeBoolean( isLastChunk );
        if ( isFirstChunk )
        {
            buffer.writeByte( contentType );
        }

        return buffer;
    }

    public boolean isEndOfInput()
    {
        return this.endOfInput;
    }

    public void close()
    {
    }

    public ByteBuf readChunk( ChannelHandlerContext ctx ) throws Exception
    {
        return this.readChunk( ctx.alloc() );
    }

    public ByteBuf readChunk( ByteBufAllocator allocator ) throws Exception
    {
        if ( this.endOfInput )
        {
            return null;
        }
        else
        {
            ByteBuf data = (ByteBuf) this.byteBufAwareMarshal.readChunk( allocator );
            if ( data == null )
            {
                return null;
            }
            else
            {
                this.endOfInput = this.byteBufAwareMarshal.isEndOfInput();
                CompositeByteBuf allData = new CompositeByteBuf( allocator, false, 2 );
                allData.addComponent( true, data );

                try
                {
                    boolean isFirstChunk = this.progress() == 0L;
                    int metaDataCapacity = metadataSize( isFirstChunk );
                    ByteBuf metaDataBuffer = allocator.buffer( metaDataCapacity, metaDataCapacity );
                    allData.addComponent( true, 0, writeMetadata( isFirstChunk, this.byteBufAwareMarshal.isEndOfInput(), this.contentType, metaDataBuffer ) );
                    this.progress += allData.readableBytes();

                    assert this.progress > 0;

                    return allData;
                }
                catch ( Throwable var7 )
                {
                    allData.release();
                    throw var7;
                }
            }
        }
    }

    public long length()
    {
        return -1L;
    }

    public long progress()
    {
        return (long) this.progress;
    }

    private static class Single implements ChunkedInput<ByteBuf>
    {
        private final ThrowingConsumer<WritableChannel,IOException> marshaller;
        boolean isEndOfInput;
        int offset;

        private Single( ThrowingConsumer<WritableChannel,IOException> marshaller )
        {
            this.marshaller = marshaller;
        }

        public boolean isEndOfInput()
        {
            return this.isEndOfInput;
        }

        public void close()
        {
            this.isEndOfInput = true;
        }

        public ByteBuf readChunk( ChannelHandlerContext ctx ) throws Exception
        {
            return this.readChunk( ctx.alloc() );
        }

        public ByteBuf readChunk( ByteBufAllocator allocator ) throws Exception
        {
            if ( this.isEndOfInput )
            {
                return null;
            }
            else
            {
                ByteBuf buffer = allocator.buffer();
                this.marshaller.accept( new BoundedNetworkWritableChannel( buffer ) );
                this.isEndOfInput = true;
                this.offset = buffer.readableBytes();
                return buffer;
            }
        }

        public long length()
        {
            return -1L;
        }

        public long progress()
        {
            return (long) this.offset;
        }
    }
}
