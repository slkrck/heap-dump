package com.neo4j.causalclustering.messaging.marshalling;

import com.neo4j.causalclustering.core.state.storage.SafeChannelMarshal;
import com.neo4j.causalclustering.messaging.EndOfStreamException;

import java.io.IOException;
import java.util.UUID;

import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class UUIDMarshal extends SafeChannelMarshal<UUID>
{
    public static final UUIDMarshal INSTANCE = new UUIDMarshal();

    protected UUID unmarshal0( ReadableChannel channel ) throws IOException, EndOfStreamException
    {
        long msb = channel.getLong();
        long lsb = channel.getLong();
        return new UUID( msb, lsb );
    }

    public void marshal( UUID uuid, WritableChannel channel ) throws IOException
    {
        channel.putLong( uuid.getMostSignificantBits() );
        channel.putLong( uuid.getLeastSignificantBits() );
    }
}
