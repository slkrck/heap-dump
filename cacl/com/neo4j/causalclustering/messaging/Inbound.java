package com.neo4j.causalclustering.messaging;

public interface Inbound<M extends Message>
{
    void registerHandler( Inbound.MessageHandler<M> var1 );

    public interface MessageHandler<M extends Message>
    {
        void handle( M var1 );
    }
}
