package com.neo4j.causalclustering.messaging;

import com.neo4j.causalclustering.core.consensus.RaftMessages;
import com.neo4j.causalclustering.identity.MemberId;
import com.neo4j.causalclustering.logging.RaftMessageLogger;

public class LoggingInbound<M extends RaftMessages.RaftMessage> implements Inbound<M>
{
    private final Inbound<M> inbound;
    private final RaftMessageLogger<MemberId> raftMessageLogger;
    private final MemberId me;

    public LoggingInbound( Inbound<M> inbound, RaftMessageLogger<MemberId> raftMessageLogger, MemberId me )
    {
        this.inbound = inbound;
        this.raftMessageLogger = raftMessageLogger;
        this.me = me;
    }

    public void registerHandler( final Inbound.MessageHandler<M> handler )
    {
        this.inbound.registerHandler( new Inbound.MessageHandler<M>()
        {
            public synchronized void handle( M message )
            {
                LoggingInbound.this.raftMessageLogger.logInbound( message.from(), message, LoggingInbound.this.me );
                handler.handle( message );
            }
        } );
    }
}
