package com.neo4j.causalclustering.messaging;

import java.util.concurrent.Future;

public interface Channel
{
    void dispose();

    boolean isDisposed();

    boolean isOpen();

    Future<Void> write( Object var1 );

    Future<Void> writeAndFlush( Object var1 );
}
