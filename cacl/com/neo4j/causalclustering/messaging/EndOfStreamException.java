package com.neo4j.causalclustering.messaging;

public class EndOfStreamException extends Exception
{
    public EndOfStreamException( Throwable e )
    {
        this.initCause( e );
    }
}
