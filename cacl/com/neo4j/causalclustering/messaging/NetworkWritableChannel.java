package com.neo4j.causalclustering.messaging;

import io.netty.buffer.ByteBuf;
import org.neo4j.io.fs.WritableChecksumChannel;

public class NetworkWritableChannel implements WritableChecksumChannel, ByteBufBacked
{
    private final ByteBuf delegate;

    public NetworkWritableChannel( ByteBuf byteBuf )
    {
        this.delegate = byteBuf;
    }

    public WritableChecksumChannel put( byte value )
    {
        this.delegate.writeByte( value );
        return this;
    }

    public WritableChecksumChannel putShort( short value )
    {
        this.delegate.writeShort( value );
        return this;
    }

    public WritableChecksumChannel putInt( int value )
    {
        this.delegate.writeInt( value );
        return this;
    }

    public WritableChecksumChannel putLong( long value )
    {
        this.delegate.writeLong( value );
        return this;
    }

    public WritableChecksumChannel putFloat( float value )
    {
        this.delegate.writeFloat( value );
        return this;
    }

    public WritableChecksumChannel putDouble( double value )
    {
        this.delegate.writeDouble( value );
        return this;
    }

    public WritableChecksumChannel put( byte[] value, int length )
    {
        this.delegate.writeBytes( value, 0, length );
        return this;
    }

    public ByteBuf byteBuf()
    {
        return this.delegate;
    }

    public void beginChecksum()
    {
    }

    public int putChecksum()
    {
        return 0;
    }
}
