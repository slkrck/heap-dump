package com.neo4j.causalclustering.messaging;

import io.netty.buffer.ByteBuf;

import java.io.IOException;

import org.neo4j.io.fs.ReadPastEndException;
import org.neo4j.kernel.impl.transaction.log.LogPositionMarker;
import org.neo4j.kernel.impl.transaction.log.ReadableClosablePositionAwareChecksumChannel;

public class NetworkReadableChannel implements ReadableClosablePositionAwareChecksumChannel
{
    private final ByteBuf delegate;

    public NetworkReadableChannel( ByteBuf input )
    {
        this.delegate = input;
    }

    public byte get() throws IOException
    {
        this.ensureBytes( 1 );
        return this.delegate.readByte();
    }

    public short getShort() throws IOException
    {
        this.ensureBytes( 2 );
        return this.delegate.readShort();
    }

    public int getInt() throws IOException
    {
        this.ensureBytes( 4 );
        return this.delegate.readInt();
    }

    public long getLong() throws IOException
    {
        this.ensureBytes( 8 );
        return this.delegate.readLong();
    }

    public float getFloat() throws IOException
    {
        this.ensureBytes( 4 );
        return this.delegate.readFloat();
    }

    public double getDouble() throws IOException
    {
        this.ensureBytes( 8 );
        return this.delegate.readDouble();
    }

    public void get( byte[] bytes, int length ) throws IOException
    {
        this.ensureBytes( length );
        this.delegate.readBytes( bytes, 0, length );
    }

    private void ensureBytes( int byteCount ) throws ReadPastEndException
    {
        if ( this.delegate.readableBytes() < byteCount )
        {
            throw ReadPastEndException.INSTANCE;
        }
    }

    public LogPositionMarker getCurrentPosition( LogPositionMarker positionMarker )
    {
        positionMarker.unspecified();
        return positionMarker;
    }

    public void close()
    {
    }

    public void beginChecksum()
    {
    }

    public int endChecksumAndValidate()
    {
        return 0;
    }
}
