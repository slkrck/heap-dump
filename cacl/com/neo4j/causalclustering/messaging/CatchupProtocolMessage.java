package com.neo4j.causalclustering.messaging;

import com.neo4j.causalclustering.catchup.RequestMessageType;

import java.util.Objects;

import org.neo4j.kernel.database.DatabaseId;

public abstract class CatchupProtocolMessage
{
    protected final RequestMessageType type;

    public CatchupProtocolMessage( RequestMessageType type )
    {
        this.type = type;
    }

    public final RequestMessageType messageType()
    {
        return this.type;
    }

    public abstract static class WithDatabaseId extends CatchupProtocolMessage
    {
        private final DatabaseId databaseId;

        protected WithDatabaseId( RequestMessageType type, DatabaseId databaseId )
        {
            super( type );
            this.databaseId = databaseId;
        }

        public final DatabaseId databaseId()
        {
            return this.databaseId;
        }

        public boolean equals( Object o )
        {
            if ( this == o )
            {
                return true;
            }
            else if ( o != null && this.getClass() == o.getClass() )
            {
                CatchupProtocolMessage.WithDatabaseId that = (CatchupProtocolMessage.WithDatabaseId) o;
                return this.type == that.type && Objects.equals( this.databaseId, that.databaseId );
            }
            else
            {
                return false;
            }
        }

        public int hashCode()
        {
            return Objects.hash( new Object[]{this.type, this.databaseId} );
        }

        public String toString()
        {
            String var10000 = this.getClass().getSimpleName();
            return var10000 + "{type=" + this.type + ", databaseId='" + this.databaseId + "'}";
        }
    }
}
