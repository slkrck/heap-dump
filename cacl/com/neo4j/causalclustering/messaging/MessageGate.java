package com.neo4j.causalclustering.messaging;

import com.neo4j.causalclustering.protocol.handshake.GateEvent;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import io.netty.channel.ChannelHandler.Sharable;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;

@Sharable
public class MessageGate extends ChannelDuplexHandler
{
    private final Predicate<Object> gated;
    private List<MessageGate.GatedWrite> pending = new ArrayList();

    public MessageGate( Predicate<Object> gated )
    {
        this.gated = gated;
    }

    public void userEventTriggered( ChannelHandlerContext ctx, Object evt ) throws Exception
    {
        if ( evt instanceof GateEvent )
        {
            if ( GateEvent.getSuccess().equals( evt ) )
            {
                Iterator var3 = this.pending.iterator();

                while ( var3.hasNext() )
                {
                    MessageGate.GatedWrite write = (MessageGate.GatedWrite) var3.next();
                    ctx.write( write.msg, write.promise );
                }

                ctx.channel().pipeline().remove( this );
            }

            this.pending.clear();
            this.pending = null;
        }
        else
        {
            super.userEventTriggered( ctx, evt );
        }
    }

    public void write( ChannelHandlerContext ctx, Object msg, ChannelPromise promise )
    {
        if ( !this.gated.test( msg ) )
        {
            ctx.write( msg, promise );
        }
        else if ( this.pending != null )
        {
            this.pending.add( new MessageGate.GatedWrite( msg, promise ) );
        }
        else
        {
            promise.setFailure( new RuntimeException( "Gate failed and has been permanently closed." ) );
        }
    }

    static class GatedWrite
    {
        final Object msg;
        final ChannelPromise promise;

        GatedWrite( Object msg, ChannelPromise promise )
        {
            this.msg = msg;
            this.promise = promise;
        }
    }
}
