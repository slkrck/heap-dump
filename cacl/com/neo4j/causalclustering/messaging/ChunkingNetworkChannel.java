package com.neo4j.causalclustering.messaging;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;

import java.util.Objects;
import java.util.Queue;

import org.neo4j.io.fs.WritableChecksumChannel;

public class ChunkingNetworkChannel implements WritableChecksumChannel, AutoCloseable
{
    private static final int DEFAULT_INIT_CHUNK_SIZE = 512;
    private final ByteBufAllocator allocator;
    private final int maxChunkSize;
    private final int initSize;
    private final Queue<ByteBuf> byteBuffs;
    private ByteBuf current;
    private boolean isClosed;

    public ChunkingNetworkChannel( ByteBufAllocator allocator, int maxChunkSize, Queue<ByteBuf> outputQueue )
    {
        Objects.requireNonNull( allocator, "allocator cannot be null" );
        Objects.requireNonNull( outputQueue, "outputQueue cannot be null" );
        this.allocator = allocator;
        this.maxChunkSize = maxChunkSize;
        this.initSize = Integer.min( 512, maxChunkSize );
        if ( maxChunkSize < 8 )
        {
            throw new IllegalArgumentException( "Chunk size must be at least 8. Got " + maxChunkSize );
        }
        else
        {
            this.byteBuffs = outputQueue;
        }
    }

    public WritableChecksumChannel put( byte value )
    {
        this.checkState();
        this.prepareWrite( 1 );
        this.current.writeByte( value );
        return this;
    }

    public WritableChecksumChannel putShort( short value )
    {
        this.checkState();
        this.prepareWrite( 2 );
        this.current.writeShort( value );
        return this;
    }

    public WritableChecksumChannel putInt( int value )
    {
        this.checkState();
        this.prepareWrite( 4 );
        this.current.writeInt( value );
        return this;
    }

    public WritableChecksumChannel putLong( long value )
    {
        this.checkState();
        this.prepareWrite( 8 );
        this.current.writeLong( value );
        return this;
    }

    public WritableChecksumChannel putFloat( float value )
    {
        this.checkState();
        this.prepareWrite( 4 );
        this.current.writeFloat( value );
        return this;
    }

    public WritableChecksumChannel putDouble( double value )
    {
        this.checkState();
        this.prepareWrite( 8 );
        this.current.writeDouble( value );
        return this;
    }

    public WritableChecksumChannel put( byte[] value, int length )
    {
        this.checkState();
        int writeIndex = 0;

        for ( int remaining = length; remaining != 0; remaining = length - writeIndex )
        {
            int toWrite = this.prepareGently( remaining );
            ByteBuf current = this.getOrCreateCurrent();
            current.writeBytes( value, writeIndex, toWrite );
            writeIndex += toWrite;
        }

        return this;
    }

    public WritableChecksumChannel flush()
    {
        this.storeCurrent();
        return this;
    }

    public int currentIndex()
    {
        return this.current != null ? this.current.writerIndex() : 0;
    }

    private int prepareGently( int size )
    {
        if ( this.getOrCreateCurrent().writerIndex() == this.maxChunkSize )
        {
            this.prepareWrite( size );
        }

        return Integer.min( this.maxChunkSize - this.current.writerIndex(), size );
    }

    private ByteBuf getOrCreateCurrent()
    {
        if ( this.current == null )
        {
            this.current = this.allocateNewBuffer();
        }

        return this.current;
    }

    private void prepareWrite( int size )
    {
        if ( this.getOrCreateCurrent().writerIndex() + size > this.maxChunkSize )
        {
            this.storeCurrent();
        }

        this.getOrCreateCurrent();
    }

    private void storeCurrent()
    {
        if ( this.current != null )
        {
            this.byteBuffs.add( this.current );
            this.current = null;
        }
    }

    private void releaseCurrent()
    {
        if ( this.current != null )
        {
            this.current.release();
        }
    }

    private ByteBuf allocateNewBuffer()
    {
        return this.allocator.buffer( this.initSize, this.maxChunkSize );
    }

    private void checkState()
    {
        if ( this.isClosed )
        {
            throw new IllegalStateException( "Channel has been closed already" );
        }
    }

    public void close()
    {
        try
        {
            this.flush();
        }
        finally
        {
            this.isClosed = true;
            this.releaseCurrent();
        }
    }

    public boolean closed()
    {
        return this.isClosed;
    }

    public void beginChecksum()
    {
    }

    public int putChecksum()
    {
        return 0;
    }
}
