package com.neo4j.causalclustering.messaging;

import com.neo4j.causalclustering.core.consensus.RaftMessages;

import java.util.function.Function;

@FunctionalInterface
public interface ComposableMessageHandler extends
        Function<LifecycleMessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>>,LifecycleMessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>>>
{
    LifecycleMessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> apply(
            LifecycleMessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> var1 );
}
