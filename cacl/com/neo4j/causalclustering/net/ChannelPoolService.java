package com.neo4j.causalclustering.net;

import com.neo4j.causalclustering.protocol.handshake.ChannelAttribute;
import com.neo4j.causalclustering.protocol.handshake.ProtocolStack;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.pool.ChannelPool;
import io.netty.channel.pool.ChannelPoolHandler;
import io.netty.channel.socket.SocketChannel;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;
import java.util.function.Function;
import java.util.stream.Stream;

import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.internal.helpers.collection.Pair;
import org.neo4j.kernel.lifecycle.Lifecycle;
import org.neo4j.scheduler.Group;
import org.neo4j.scheduler.JobScheduler;

public class ChannelPoolService implements Lifecycle
{
    private final BootstrapConfiguration<? extends SocketChannel> bootstrapConfiguration;
    private final JobScheduler scheduler;
    private final Group group;
    private final ChannelPoolHandler poolHandler;
    private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    private final WriteLock exclusiveService;
    private final ReadLock sharedService;
    private CompletableFuture<PooledChannel> endOfLife;
    private TrackingChannelPoolMap poolMap;
    private ChannelPoolFactory poolFactory;
    private EventLoopGroup eventLoopGroup;

    public ChannelPoolService( BootstrapConfiguration<? extends SocketChannel> bootstrapConfiguration, JobScheduler scheduler, Group group,
            ChannelPoolHandler channelPoolHandler, ChannelPoolFactory poolFactory )
    {
        this.exclusiveService = this.lock.writeLock();
        this.sharedService = this.lock.readLock();
        this.bootstrapConfiguration = bootstrapConfiguration;
        this.scheduler = scheduler;
        this.group = group;
        this.poolHandler = channelPoolHandler;
        this.poolFactory = poolFactory;
    }

    private static CompletionStage<PooledChannel> createPooledChannel( Channel channel, ChannelPool pool )
    {
        CompletableFuture<ProtocolStack> protocolFuture = (CompletableFuture) channel.attr( ChannelAttribute.PROTOCOL_STACK ).get();
        return protocolFuture == null ? CompletableFuture.failedFuture(
                new IllegalStateException( "Channel " + channel + " does not contain a protocol stack attribute" ) )
                                      : protocolFuture.thenApply( ( protocol ) -> {
                                          return new PooledChannel( channel, pool, protocol );
                                      } );
    }

    public CompletableFuture<PooledChannel> acquire( SocketAddress address )
    {
        this.sharedService.lock();

        CompletableFuture var3;
        try
        {
            if ( this.poolMap == null )
            {
                CompletableFuture var7 = CompletableFuture.failedFuture( new IllegalStateException( "Channel pool service is not in a started state." ) );
                return var7;
            }

            ChannelPool pool = this.poolMap.get( address );
            var3 = NettyUtil.toCompletableFuture( pool.acquire() ).thenCompose( ( channel ) -> {
                return createPooledChannel( channel, pool );
            } ).applyToEither( this.endOfLife, Function.identity() );
        }
        finally
        {
            this.sharedService.unlock();
        }

        return var3;
    }

    public void init()
    {
    }

    public void start()
    {
        this.exclusiveService.lock();

        try
        {
            this.endOfLife = new CompletableFuture();
            this.eventLoopGroup = this.bootstrapConfiguration.eventLoopGroup( this.scheduler.executor( this.group ) );
            Bootstrap baseBootstrap =
                    (Bootstrap) ((Bootstrap) (new Bootstrap()).group( this.eventLoopGroup )).channel( this.bootstrapConfiguration.channelClass() );
            this.poolMap = new TrackingChannelPoolMap( baseBootstrap, this.poolHandler, this.poolFactory );
        }
        finally
        {
            this.exclusiveService.unlock();
        }
    }

    public void stop()
    {
        this.endOfLife.completeExceptionally( new IllegalStateException( "Pool is closed. Lifecycle issue?" ) );
        this.exclusiveService.lock();

        try
        {
            if ( this.poolMap != null )
            {
                this.poolMap.close();
                this.poolMap = null;
            }

            this.eventLoopGroup.shutdownGracefully( 100L, 5000L, TimeUnit.MILLISECONDS ).syncUninterruptibly();
        }
        finally
        {
            this.exclusiveService.unlock();
        }
    }

    public void shutdown()
    {
    }

    public Stream<Pair<SocketAddress,ProtocolStack>> installedProtocols()
    {
        this.sharedService.lock();

        Stream var1;
        try
        {
            var1 = this.poolMap == null ? Stream.empty() : this.poolMap.installedProtocols();
        }
        finally
        {
            this.sharedService.unlock();
        }

        return var1;
    }
}
