package com.neo4j.causalclustering.net;

import com.neo4j.causalclustering.protocol.handshake.ProtocolStack;
import io.netty.channel.Channel;
import io.netty.channel.pool.ChannelPool;
import io.netty.util.AttributeKey;
import io.netty.util.concurrent.Future;

public class PooledChannel
{
    private final Channel channel;
    private final ChannelPool pool;
    private final ProtocolStack protocolStack;

    PooledChannel( Channel channel, ChannelPool pool, ProtocolStack protocolStack )
    {
        this.channel = channel;
        this.pool = pool;
        this.protocolStack = protocolStack;
    }

    public <ATTR> ATTR getAttribute( AttributeKey<ATTR> key )
    {
        return this.channel.attr( key ).get();
    }

    public Channel channel()
    {
        return this.channel;
    }

    public ProtocolStack protocolStack()
    {
        return this.protocolStack;
    }

    public Future<Void> release()
    {
        return this.pool.release( this.channel );
    }

    public Future<Void> dispose()
    {
        this.channel.close();
        return this.pool.release( this.channel );
    }
}
