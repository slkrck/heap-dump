package com.neo4j.causalclustering.net;

import com.neo4j.causalclustering.core.CausalClusteringSettings;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.epoll.Epoll;
import io.netty.channel.socket.ServerSocketChannel;
import io.netty.channel.socket.SocketChannel;

import java.util.concurrent.Executor;

import org.neo4j.configuration.Config;

public interface BootstrapConfiguration<TYPE extends Channel>
{
    static BootstrapConfiguration<? extends ServerSocketChannel> serverConfig( Config config )
    {
        return (BootstrapConfiguration) (preferNative( config ) && Epoll.isAvailable() ? EpollBootstrapConfig.epollServerConfig()
                                                                                       : NioBootstrapConfig.nioServerConfig());
    }

    static boolean preferNative( Config config )
    {
        return (Boolean) config.get( CausalClusteringSettings.use_native_transport );
    }

    static BootstrapConfiguration<? extends SocketChannel> clientConfig( Config config )
    {
        return (BootstrapConfiguration) (preferNative( config ) && Epoll.isAvailable() ? EpollBootstrapConfig.epollClientConfig()
                                                                                       : NioBootstrapConfig.nioClientConfig());
    }

    EventLoopGroup eventLoopGroup( Executor var1 );

    Class<TYPE> channelClass();
}
