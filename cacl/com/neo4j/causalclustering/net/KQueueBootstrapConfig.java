package com.neo4j.causalclustering.net;

import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.kqueue.KQueueEventLoopGroup;
import io.netty.channel.kqueue.KQueueServerSocketChannel;
import io.netty.channel.kqueue.KQueueSocketChannel;

import java.util.concurrent.Executor;

public abstract class KQueueBootstrapConfig<CHANNEL extends Channel> implements BootstrapConfiguration<CHANNEL>
{
    public static KQueueBootstrapConfig<KQueueServerSocketChannel> kQueueServerConfig()
    {
        return new KQueueBootstrapConfig<KQueueServerSocketChannel>()
        {
            public Class<KQueueServerSocketChannel> channelClass()
            {
                return KQueueServerSocketChannel.class;
            }
        };
    }

    public static KQueueBootstrapConfig<KQueueSocketChannel> kQueueClientConfig()
    {
        return new KQueueBootstrapConfig<KQueueSocketChannel>()
        {
            public Class<KQueueSocketChannel> channelClass()
            {
                return KQueueSocketChannel.class;
            }
        };
    }

    public EventLoopGroup eventLoopGroup( Executor executor )
    {
        return new KQueueEventLoopGroup( 0, executor );
    }
}
