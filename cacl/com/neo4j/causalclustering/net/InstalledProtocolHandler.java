package com.neo4j.causalclustering.net;

import com.neo4j.causalclustering.protocol.handshake.ProtocolStack;
import com.neo4j.causalclustering.protocol.handshake.ServerHandshakeFinishedEvent;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelHandler.Sharable;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Stream;

import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.internal.helpers.collection.Pair;

@Sharable
public class InstalledProtocolHandler extends ChannelInboundHandlerAdapter
{
    private ConcurrentMap<SocketAddress,ProtocolStack> installedProtocols = new ConcurrentHashMap();

    public void userEventTriggered( ChannelHandlerContext ctx, Object evt ) throws Exception
    {
        if ( evt instanceof ServerHandshakeFinishedEvent.Created )
        {
            ServerHandshakeFinishedEvent.Created created = (ServerHandshakeFinishedEvent.Created) evt;
            this.installedProtocols.put( created.advertisedSocketAddress, created.protocolStack );
        }
        else if ( evt instanceof ServerHandshakeFinishedEvent.Closed )
        {
            ServerHandshakeFinishedEvent.Closed closed = (ServerHandshakeFinishedEvent.Closed) evt;
            this.installedProtocols.remove( closed.advertisedSocketAddress );
        }
        else
        {
            super.userEventTriggered( ctx, evt );
        }
    }

    public Stream<Pair<SocketAddress,ProtocolStack>> installedProtocols()
    {
        return this.installedProtocols.entrySet().stream().map( ( entry ) -> {
            return Pair.of( (SocketAddress) entry.getKey(), (ProtocolStack) entry.getValue() );
        } );
    }
}
