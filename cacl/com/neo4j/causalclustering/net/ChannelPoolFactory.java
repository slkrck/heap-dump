package com.neo4j.causalclustering.net;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.pool.ChannelPool;
import io.netty.channel.pool.ChannelPoolHandler;

public interface ChannelPoolFactory
{
    ChannelPool create( Bootstrap var1, ChannelPoolHandler var2 );
}
