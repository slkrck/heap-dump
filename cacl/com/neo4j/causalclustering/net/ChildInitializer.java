package com.neo4j.causalclustering.net;

import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;

public interface ChildInitializer
{
    void initChannel( Channel var1 ) throws Exception;

    default ChannelInitializer<Channel> asChannelInitializer()
    {
        return new ChannelInitializer<Channel>()
        {
            protected void initChannel( Channel channel ) throws Exception
            {
                ChildInitializer.this.initChannel( channel );
            }
        };
    }
}
