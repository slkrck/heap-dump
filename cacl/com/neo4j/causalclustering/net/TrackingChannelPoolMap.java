package com.neo4j.causalclustering.net;

import com.neo4j.causalclustering.helper.ErrorHandler;
import com.neo4j.causalclustering.protocol.handshake.ChannelAttribute;
import com.neo4j.causalclustering.protocol.handshake.ProtocolStack;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.pool.AbstractChannelPoolHandler;
import io.netty.channel.pool.AbstractChannelPoolMap;
import io.netty.channel.pool.ChannelPool;
import io.netty.channel.pool.ChannelPoolHandler;

import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.internal.helpers.collection.Pair;

class TrackingChannelPoolMap extends AbstractChannelPoolMap<SocketAddress,ChannelPool>
{
    private final Bootstrap baseBootstrap;
    private final TrackingChannelPoolMap.ChannelPoolHandlers poolHandlers;
    private final TrackingChannelPoolMap.InstalledProtocolsTracker protocolsTracker;
    private final ChannelPoolFactory poolFactory;

    TrackingChannelPoolMap( Bootstrap baseBootstrap, ChannelPoolHandler poolHandler, ChannelPoolFactory poolFactory )
    {
        this.baseBootstrap = baseBootstrap;
        this.protocolsTracker = new TrackingChannelPoolMap.InstalledProtocolsTracker();
        this.poolHandlers = new TrackingChannelPoolMap.ChannelPoolHandlers( Arrays.asList( poolHandler, this.protocolsTracker ) );
        this.poolFactory = poolFactory;
    }

    Stream<Pair<SocketAddress,ProtocolStack>> installedProtocols()
    {
        return this.protocolsTracker.installedProtocols();
    }

    protected ChannelPool newPool( SocketAddress address )
    {
        return this.poolFactory.create(
                this.baseBootstrap.clone().remoteAddress( InetSocketAddress.createUnresolved( address.getHostname(), address.getPort() ) ), this.poolHandlers );
    }

    private static final class ChannelPoolHandlers implements ChannelPoolHandler
    {
        private final Collection<ChannelPoolHandler> poolHandlers;

        ChannelPoolHandlers( Collection<ChannelPoolHandler> poolHandlers )
        {
            Objects.requireNonNull( poolHandlers );
            this.poolHandlers = Collections.unmodifiableCollection( poolHandlers );
        }

        public void channelReleased( Channel ch )
        {
            ErrorHandler errorHandler = new ErrorHandler( "Channel released" );

            try
            {
                this.poolHandlers.forEach( ( chh ) -> {
                    errorHandler.execute( () -> {
                        chh.channelReleased( ch );
                    } );
                } );
            }
            catch ( Throwable var6 )
            {
                try
                {
                    errorHandler.close();
                }
                catch ( Throwable var5 )
                {
                    var6.addSuppressed( var5 );
                }

                throw var6;
            }

            errorHandler.close();
        }

        public void channelAcquired( Channel ch )
        {
            ErrorHandler errorHandler = new ErrorHandler( "Channel acquired" );

            try
            {
                this.poolHandlers.forEach( ( chh ) -> {
                    errorHandler.execute( () -> {
                        chh.channelAcquired( ch );
                    } );
                } );
            }
            catch ( Throwable var6 )
            {
                try
                {
                    errorHandler.close();
                }
                catch ( Throwable var5 )
                {
                    var6.addSuppressed( var5 );
                }

                throw var6;
            }

            errorHandler.close();
        }

        public void channelCreated( Channel ch )
        {
            ErrorHandler errorHandler = new ErrorHandler( "Channel created" );

            try
            {
                this.poolHandlers.forEach( ( chh ) -> {
                    errorHandler.execute( () -> {
                        chh.channelCreated( ch );
                    } );
                } );
            }
            catch ( Throwable var6 )
            {
                try
                {
                    errorHandler.close();
                }
                catch ( Throwable var5 )
                {
                    var6.addSuppressed( var5 );
                }

                throw var6;
            }

            errorHandler.close();
        }
    }

    private static class InstalledProtocolsTracker extends AbstractChannelPoolHandler
    {
        private final Collection<Channel> createdChannels = ConcurrentHashMap.newKeySet();

        Stream<Pair<SocketAddress,ProtocolStack>> installedProtocols()
        {
            return this.createdChannels.stream().filter( Channel::isActive ).map( ( ch ) -> {
                InetSocketAddress address = (InetSocketAddress) ch.remoteAddress();
                return Pair.of( new SocketAddress( address.getHostName(), address.getPort() ),
                        (ProtocolStack) ((CompletableFuture) ch.attr( ChannelAttribute.PROTOCOL_STACK ).get()).getNow( (Object) null ) );
            } ).filter( ( pair ) -> {
                return pair.other() != null;
            } );
        }

        public void channelCreated( Channel ch )
        {
            this.createdChannels.add( ch );
            ch.closeFuture().addListener( ( f ) -> {
                this.createdChannels.remove( ch );
            } );
        }
    }
}
