package com.neo4j.causalclustering.net;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInboundHandler;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.socket.ServerSocketChannel;
import io.netty.util.concurrent.Future;

import java.net.InetSocketAddress;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

import org.neo4j.configuration.connectors.ConnectorPortRegister;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class Server extends LifecycleAdapter
{
    private final Log debugLog;
    private final Log userLog;
    private final String serverName;
    private final Executor executor;
    private final BootstrapConfiguration<? extends ServerSocketChannel> bootstrapConfiguration;
    private final ChildInitializer childInitializer;
    private final ChannelInboundHandler parentHandler;
    private final ConnectorPortRegister portRegister;
    private EventLoopGroup workerGroup;
    private Channel channel;
    private SocketAddress listenAddress;

    public Server( ChildInitializer childInitializer, ChannelInboundHandler parentHandler, LogProvider debugLogProvider, LogProvider userLogProvider,
            SocketAddress listenAddress, String serverName, Executor executor, ConnectorPortRegister portRegister,
            BootstrapConfiguration<? extends ServerSocketChannel> bootstrapConfiguration )
    {
        this.childInitializer = childInitializer;
        this.parentHandler = parentHandler;
        this.listenAddress = listenAddress;
        this.debugLog = debugLogProvider.getLog( this.getClass() );
        this.userLog = userLogProvider.getLog( this.getClass() );
        this.serverName = serverName;
        this.executor = executor;
        this.portRegister = portRegister;
        this.bootstrapConfiguration = bootstrapConfiguration;
    }

    public void start()
    {
        if ( this.channel == null )
        {
            this.workerGroup = this.bootstrapConfiguration.eventLoopGroup( this.executor );
            ServerBootstrap bootstrap = ((ServerBootstrap) ((ServerBootstrap) ((ServerBootstrap) (new ServerBootstrap()).group( this.workerGroup ).channel(
                    this.bootstrapConfiguration.channelClass() )).option( ChannelOption.SO_REUSEADDR, Boolean.TRUE )).localAddress(
                    this.listenAddress.socketAddress() )).childHandler( this.childInitializer.asChannelInitializer() );
            if ( this.parentHandler != null )
            {
                bootstrap.handler( this.parentHandler );
            }

            try
            {
                this.channel = bootstrap.bind().syncUninterruptibly().channel();
                this.listenAddress = this.actualListenAddress( this.channel );
                this.registerListenAddress();
                this.debugLog.info( "%s: bound to '%s' with transport '%s'",
                        new Object[]{this.serverName, this.listenAddress, this.bootstrapConfiguration.channelClass().getSimpleName()} );
            }
            catch ( Exception var4 )
            {
                String message = String.format( "%s: cannot bind to '%s' with transport '%s'.", this.serverName, this.listenAddress,
                        this.bootstrapConfiguration.channelClass().getSimpleName() );
                this.userLog.error( message + " Message: " + var4.getMessage() );
                this.debugLog.error( message, var4 );
                this.workerGroup.shutdownGracefully();
                throw var4;
            }
        }
    }

    public void stop()
    {
        if ( this.channel != null )
        {
            this.debugLog.info( this.serverName + ": stopping and unbinding from: " + this.listenAddress );

            try
            {
                this.deregisterListenAddress();
                this.channel.close().sync();
                this.channel = null;
            }
            catch ( InterruptedException var2 )
            {
                Thread.currentThread().interrupt();
                this.debugLog.warn( "Interrupted while closing channel." );
            }

            if ( this.workerGroup != null )
            {
                Future<?> fShutdown = this.workerGroup.shutdownGracefully( 100L, 5000L, TimeUnit.MILLISECONDS );
                if ( !fShutdown.awaitUninterruptibly( 15000L, TimeUnit.MILLISECONDS ) )
                {
                    this.debugLog.warn( "Worker group not shutdown within time limit." );
                }
            }

            this.workerGroup = null;
        }
    }

    public String name()
    {
        return this.serverName;
    }

    public SocketAddress address()
    {
        return this.listenAddress;
    }

    public String toString()
    {
        return String.format( "Server[%s]", this.serverName );
    }

    private SocketAddress actualListenAddress( Channel channel )
    {
        java.net.SocketAddress address = channel.localAddress();
        if ( address instanceof InetSocketAddress )
        {
            InetSocketAddress inetAddress = (InetSocketAddress) address;
            return new SocketAddress( inetAddress.getHostString(), inetAddress.getPort() );
        }
        else
        {
            return this.listenAddress;
        }
    }

    private void registerListenAddress()
    {
        this.portRegister.register( this.serverName, this.listenAddress );
    }

    private void deregisterListenAddress()
    {
        this.portRegister.deregister( this.serverName );
    }
}
