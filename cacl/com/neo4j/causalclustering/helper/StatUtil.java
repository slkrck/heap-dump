package com.neo4j.causalclustering.helper;

import java.util.concurrent.TimeUnit;

import org.neo4j.logging.FormattedLogProvider;
import org.neo4j.logging.Log;
import org.neo4j.time.Stopwatch;

public class StatUtil
{
    private StatUtil()
    {
    }

    public static synchronized StatUtil.StatContext create( String name, long printEvery, boolean clearAfterPrint )
    {
        return create( name, FormattedLogProvider.toOutputStream( System.out ).getLog( name ), printEvery, clearAfterPrint );
    }

    public static synchronized StatUtil.StatContext create( String name, Log log, long printEvery, boolean clearAfterPrint )
    {
        return new StatUtil.StatContext( name, log, printEvery, clearAfterPrint );
    }

    private static class BasicStats
    {
        private Double min;
        private Double max;
        private Double avg = 0.0D;
        private long count;

        void collect( double val )
        {
            ++this.count;
            this.avg = this.avg + (val - this.avg) / (double) this.count;
            this.min = this.min == null ? val : Math.min( this.min, val );
            this.max = this.max == null ? val : Math.max( this.max, val );
        }

        public String toString()
        {
            return String.format( "{min=%s, max=%s, avg=%s, count=%d}", this.min, this.max, this.avg, this.count );
        }
    }

    public static class TimingContext
    {
        private final StatUtil.StatContext context;
        private final Stopwatch startTime = Stopwatch.start();

        TimingContext( StatUtil.StatContext context )
        {
            this.context = context;
        }

        public void end()
        {
            this.context.collect( (double) this.startTime.elapsed( TimeUnit.MILLISECONDS ) );
        }
    }

    public static class StatContext
    {
        private static final int N_BUCKETS = 10;
        private final String name;
        private final Log log;
        private final long printEvery;
        private final boolean clearAfterPrint;
        private StatUtil.BasicStats[] bucket = new StatUtil.BasicStats[10];
        private long totalCount;

        private StatContext( String name, Log log, long printEvery, boolean clearAfterPrint )
        {
            this.name = name;
            this.log = log;
            this.printEvery = printEvery;
            this.clearAfterPrint = clearAfterPrint;
            this.clear();
        }

        public synchronized void clear()
        {
            for ( int i = 0; i < 10; ++i )
            {
                this.bucket[i] = new StatUtil.BasicStats();
            }

            this.totalCount = 0L;
        }

        public void collect( double value )
        {
            int bucketIndex = this.bucketFor( value );
            synchronized ( this )
            {
                ++this.totalCount;
                this.bucket[bucketIndex].collect( value );
                if ( this.totalCount % this.printEvery == 0L )
                {
                    this.print();
                }
            }
        }

        private int bucketFor( double value )
        {
            int bucketIndex;
            if ( value <= 0.0D )
            {
                bucketIndex = 0;
            }
            else
            {
                bucketIndex = (int) Math.log10( value );
                bucketIndex = Math.min( bucketIndex, 9 );
            }

            return bucketIndex;
        }

        public StatUtil.TimingContext time()
        {
            return new StatUtil.TimingContext( this );
        }

        public synchronized void print()
        {
            StatUtil.BasicStats[] var1 = this.bucket;
            int var2 = var1.length;

            for ( int var3 = 0; var3 < var2; ++var3 )
            {
                StatUtil.BasicStats stats = var1[var3];
                if ( stats.count > 0L )
                {
                    this.log.info( String.format( "%s%s", this.name, stats ) );
                }
            }

            if ( this.clearAfterPrint )
            {
                this.clear();
            }
        }
    }
}
