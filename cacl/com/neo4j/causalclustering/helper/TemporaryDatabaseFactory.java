package com.neo4j.causalclustering.helper;

import java.io.File;

import org.neo4j.configuration.Config;

public interface TemporaryDatabaseFactory
{
    TemporaryDatabase startTemporaryDatabase( File var1, Config var2 );
}
