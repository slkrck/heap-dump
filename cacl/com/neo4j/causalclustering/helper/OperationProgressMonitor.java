package com.neo4j.causalclustering.helper;

import java.util.OptionalLong;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Supplier;

import org.neo4j.logging.Log;

public final class OperationProgressMonitor<T>
{
    private final Future<T> future;
    private final long inactivityTimeoutMillis;
    private final Supplier<OptionalLong> millisSinceLastResponseSupplier;
    private final Log log;

    private OperationProgressMonitor( Future<T> future, long inactivityTimeoutMillis, Supplier<OptionalLong> millisSinceLastResponseSupplier, Log log )
    {
        this.future = future;
        this.inactivityTimeoutMillis = inactivityTimeoutMillis;
        this.millisSinceLastResponseSupplier = millisSinceLastResponseSupplier;
        this.log = log;
    }

    public static <T> OperationProgressMonitor<T> of( Future<T> future, long inactivityTimeoutMillis, Supplier<OptionalLong> millisSinceLastResponseSupplier,
            Log log )
    {
        return new OperationProgressMonitor( future, inactivityTimeoutMillis, millisSinceLastResponseSupplier, log );
    }

    public T get() throws ExecutionException, InterruptedException, TimeoutException
    {
        long remainingTimeoutMillis = this.inactivityTimeoutMillis;

        while ( true )
        {
            try
            {
                return this.future.get( remainingTimeoutMillis, TimeUnit.MILLISECONDS );
            }
            catch ( ExecutionException | InterruptedException var5 )
            {
                this.future.cancel( false );
                throw var5;
            }
            catch ( TimeoutException var6 )
            {
                OptionalLong millisSinceLastResponse = (OptionalLong) this.millisSinceLastResponseSupplier.get();
                if ( !millisSinceLastResponse.isPresent() )
                {
                    this.throwOnTimeout( "Request timed out with no responses after " + this.inactivityTimeoutMillis + " ms." );
                }
                else if ( millisSinceLastResponse.getAsLong() < this.inactivityTimeoutMillis )
                {
                    remainingTimeoutMillis = this.inactivityTimeoutMillis - millisSinceLastResponse.getAsLong();
                }
                else
                {
                    this.throwOnTimeout( "Request timed out after period of inactivity. Time since last response: " + millisSinceLastResponse + " ms." );
                }
            }
        }
    }

    private void throwOnTimeout( String message ) throws TimeoutException
    {
        this.log.warn( message );
        this.future.cancel( false );
        throw new TimeoutException( message );
    }
}
