package com.neo4j.causalclustering.helper;

import org.neo4j.dbms.api.DatabaseManagementService;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.kernel.internal.GraphDatabaseAPI;

public class TemporaryDatabase implements AutoCloseable
{
    private final DatabaseManagementService managementService;
    private final GraphDatabaseAPI defaultDatabase;

    public TemporaryDatabase( DatabaseManagementService managementService )
    {
        this.managementService = managementService;
        this.defaultDatabase = (GraphDatabaseAPI) managementService.database( "neo4j" );
    }

    public GraphDatabaseService graphDatabaseService()
    {
        return this.defaultDatabase;
    }

    public DatabaseLayout defaultDatabaseDirectory()
    {
        return this.defaultDatabase.databaseLayout();
    }

    public void close()
    {
        this.managementService.shutdown();
    }
}
