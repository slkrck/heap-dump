package com.neo4j.causalclustering.helper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ErrorHandler implements AutoCloseable
{
    private final List<Throwable> throwables = new ArrayList();
    private final String message;

    public ErrorHandler( String message )
    {
        this.message = message;
    }

    public static void runAll( String description, ErrorHandler.ThrowingRunnable... actions ) throws RuntimeException
    {
        ErrorHandler errorHandler = new ErrorHandler( description );

        try
        {
            ErrorHandler.ThrowingRunnable[] var3 = actions;
            int var4 = actions.length;

            for ( int var5 = 0; var5 < var4; ++var5 )
            {
                ErrorHandler.ThrowingRunnable action = var3[var5];
                errorHandler.execute( action );
            }
        }
        catch ( Throwable var8 )
        {
            try
            {
                errorHandler.close();
            }
            catch ( Throwable var7 )
            {
                var8.addSuppressed( var7 );
            }

            throw var8;
        }

        errorHandler.close();
    }

    public void execute( ErrorHandler.ThrowingRunnable throwingRunnable )
    {
        try
        {
            throwingRunnable.run();
        }
        catch ( Throwable var3 )
        {
            this.throwables.add( var3 );
        }
    }

    public void add( Throwable throwable )
    {
        this.throwables.add( throwable );
    }

    public void close() throws RuntimeException
    {
        this.throwIfException();
    }

    private void throwIfException()
    {
        RuntimeException runtimeException = null;
        Iterator var2 = this.throwables.iterator();

        while ( var2.hasNext() )
        {
            Throwable throwable = (Throwable) var2.next();
            if ( runtimeException == null )
            {
                runtimeException = new RuntimeException( this.message, throwable );
            }
            else
            {
                runtimeException.addSuppressed( throwable );
            }
        }

        if ( runtimeException != null )
        {
            throw runtimeException;
        }
    }

    public interface ThrowingRunnable
    {
        void run() throws Throwable;
    }
}
