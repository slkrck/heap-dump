package com.neo4j.dbms.database;

import com.neo4j.causalclustering.catchup.CatchupComponentsRepository;
import com.neo4j.causalclustering.common.ClusteredDatabase;

import java.io.File;
import java.io.IOException;

import org.neo4j.dbms.database.DatabaseContext;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.monitoring.Monitors;
import org.neo4j.storageengine.api.StoreId;

public interface ClusteredDatabaseContext extends DatabaseContext
{
    StoreId storeId();

    Monitors monitors();

    void delete() throws IOException;

    boolean isEmpty();

    DatabaseLayout databaseLayout();

    void replaceWith( File var1 ) throws IOException;

    NamedDatabaseId databaseId();

    CatchupComponentsRepository.CatchupComponents catchupComponents();

    ClusteredDatabase clusteredDatabase();
}
