package com.neo4j.dbms.database;

import com.neo4j.causalclustering.catchup.CatchupComponentsFactory;
import com.neo4j.causalclustering.catchup.CatchupComponentsRepository;
import com.neo4j.causalclustering.catchup.storecopy.StoreFiles;
import com.neo4j.causalclustering.common.ClusteredDatabase;

import java.io.File;
import java.io.IOException;

import org.neo4j.collection.Dependencies;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.kernel.database.Database;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.impl.factory.GraphDatabaseFacade;
import org.neo4j.kernel.impl.transaction.log.files.LogFiles;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.monitoring.Monitors;
import org.neo4j.storageengine.api.StoreId;

public class DefaultClusteredDatabaseContext implements ClusteredDatabaseContext
{
    private final DatabaseLayout databaseLayout;
    private final StoreFiles storeFiles;
    private final Log log;
    private final NamedDatabaseId namedDatabaseId;
    private final LogFiles txLogs;
    private final Database database;
    private final GraphDatabaseFacade facade;
    private final CatchupComponentsRepository.CatchupComponents catchupComponents;
    private final ClusteredDatabase clusterDatabase;
    private final Monitors clusterDatabaseMonitors;
    private volatile Throwable failureCause;
    private volatile StoreId storeId;

    DefaultClusteredDatabaseContext( Database database, GraphDatabaseFacade facade, LogFiles txLogs, StoreFiles storeFiles, LogProvider logProvider,
            CatchupComponentsFactory catchupComponentsFactory, ClusteredDatabase clusterDatabase, Monitors clusterDatabaseMonitors )
    {
        this.database = database;
        this.facade = facade;
        this.databaseLayout = database.getDatabaseLayout();
        this.storeFiles = storeFiles;
        this.txLogs = txLogs;
        this.namedDatabaseId = database.getNamedDatabaseId();
        this.log = logProvider.getLog( this.getClass() );
        this.clusterDatabase = clusterDatabase;
        this.clusterDatabaseMonitors = clusterDatabaseMonitors;
        this.catchupComponents = catchupComponentsFactory.createDatabaseComponents( this );
    }

    public StoreId storeId()
    {
        if ( this.storeId == null )
        {
            this.storeId = this.readStoreIdFromDisk();
        }

        return this.storeId;
    }

    private StoreId readStoreIdFromDisk()
    {
        try
        {
            return this.storeFiles.readStoreId( this.databaseLayout );
        }
        catch ( IOException var2 )
        {
            this.log.error( "Failure reading store id", var2 );
            return null;
        }
    }

    public Monitors monitors()
    {
        return this.clusterDatabaseMonitors;
    }

    public Dependencies dependencies()
    {
        return this.database().getDependencyResolver();
    }

    public void delete() throws IOException
    {
        this.storeFiles.delete( this.databaseLayout, this.txLogs );
    }

    public void fail( Throwable failureCause )
    {
        this.failureCause = failureCause;
    }

    public Throwable failureCause()
    {
        return this.failureCause;
    }

    public boolean isFailed()
    {
        return this.failureCause != null;
    }

    public boolean isEmpty()
    {
        return this.storeFiles.isEmpty( this.databaseLayout );
    }

    public DatabaseLayout databaseLayout()
    {
        return this.databaseLayout;
    }

    public void replaceWith( File sourceDir ) throws IOException
    {
        this.storeFiles.delete( this.databaseLayout, this.txLogs );
        this.storeFiles.moveTo( sourceDir, this.databaseLayout, this.txLogs );
    }

    public Database database()
    {
        return this.database;
    }

    public GraphDatabaseFacade databaseFacade()
    {
        return this.facade;
    }

    public NamedDatabaseId databaseId()
    {
        return this.namedDatabaseId;
    }

    public CatchupComponentsRepository.CatchupComponents catchupComponents()
    {
        return this.catchupComponents;
    }

    public ClusteredDatabase clusteredDatabase()
    {
        return this.clusterDatabase;
    }
}
