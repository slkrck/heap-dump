package com.neo4j.dbms;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.neo4j.function.ThrowingConsumer;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class SystemDbOnlyReplicatedDatabaseEventService implements ReplicatedDatabaseEventService
{
    private final Log log;
    private final List<ReplicatedDatabaseEventService.ReplicatedDatabaseEventListener> listeners = new CopyOnWriteArrayList();

    public SystemDbOnlyReplicatedDatabaseEventService( LogProvider logProvider )
    {
        this.log = logProvider.getLog( this.getClass() );
    }

    public void registerListener( NamedDatabaseId namedDatabaseId, ReplicatedDatabaseEventService.ReplicatedDatabaseEventListener listener )
    {
        this.assertIsSystemDatabase( namedDatabaseId );
        this.listeners.add( listener );
    }

    public void unregisterListener( NamedDatabaseId namedDatabaseId, ReplicatedDatabaseEventService.ReplicatedDatabaseEventListener listener )
    {
        this.assertIsSystemDatabase( namedDatabaseId );
        this.listeners.remove( listener );
    }

    public ReplicatedDatabaseEventService.ReplicatedDatabaseEventDispatch getDatabaseEventDispatch( NamedDatabaseId namedDatabaseId )
    {
        return !namedDatabaseId.isSystemDatabase() ? NO_EVENT_DISPATCH : new ReplicatedDatabaseEventService.ReplicatedDatabaseEventDispatch()
        {
            public void fireTransactionCommitted( long txId )
            {
                SystemDbOnlyReplicatedDatabaseEventService.this.dispatchEvent( ( listener ) -> {
                    listener.transactionCommitted( txId );
                } );
            }

            public void fireStoreReplaced( long txId )
            {
                SystemDbOnlyReplicatedDatabaseEventService.this.dispatchEvent( ( listener ) -> {
                    listener.storeReplaced( txId );
                } );
            }
        };
    }

    private void dispatchEvent( ThrowingConsumer<ReplicatedDatabaseEventService.ReplicatedDatabaseEventListener,Exception> action )
    {
        this.listeners.forEach( ( listener ) -> {
            try
            {
                action.accept( listener );
            }
            catch ( Exception var4 )
            {
                this.log.error( "Error handling database event", var4 );
            }
        } );
    }

    private void assertIsSystemDatabase( NamedDatabaseId namedDatabaseId )
    {
        if ( !namedDatabaseId.isSystemDatabase() )
        {
            throw new IllegalArgumentException( "Not supported " + namedDatabaseId );
        }
    }
}
