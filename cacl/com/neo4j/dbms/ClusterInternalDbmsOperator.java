package com.neo4j.dbms;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import org.neo4j.kernel.database.NamedDatabaseId;

public class ClusterInternalDbmsOperator extends DbmsOperator
{
    private final List<ClusterInternalDbmsOperator.StoreCopyHandle> storeCopying = new CopyOnWriteArrayList();
    private final Set<NamedDatabaseId> bootstrapping = ConcurrentHashMap.newKeySet();
    private final Set<NamedDatabaseId> panicked = ConcurrentHashMap.newKeySet();

    protected Map<String,EnterpriseDatabaseState> desired0()
    {
        HashMap<String,EnterpriseDatabaseState> result = new HashMap();
        Iterator var2 = this.storeCopying.iterator();

        while ( var2.hasNext() )
        {
            ClusterInternalDbmsOperator.StoreCopyHandle storeCopyHandle = (ClusterInternalDbmsOperator.StoreCopyHandle) var2.next();
            NamedDatabaseId id = storeCopyHandle.namedDatabaseId;
            if ( !this.bootstrapping.contains( id ) )
            {
                result.put( id.name(), new EnterpriseDatabaseState( id, EnterpriseOperatorState.STORE_COPYING ) );
            }
        }

        var2 = this.panicked.iterator();

        while ( var2.hasNext() )
        {
            NamedDatabaseId id = (NamedDatabaseId) var2.next();
            result.put( id.name(), new EnterpriseDatabaseState( id, EnterpriseOperatorState.STOPPED ) );
        }

        return result;
    }

    public ClusterInternalDbmsOperator.StoreCopyHandle stopForStoreCopy( NamedDatabaseId namedDatabaseId )
    {
        ClusterInternalDbmsOperator.StoreCopyHandle storeCopyHandle = new ClusterInternalDbmsOperator.StoreCopyHandle( this, namedDatabaseId );
        this.storeCopying.add( storeCopyHandle );
        this.triggerReconcilerOnStoreCopy( namedDatabaseId );
        return storeCopyHandle;
    }

    public void stopOnPanic( NamedDatabaseId namedDatabaseId, Throwable causeOfPanic )
    {
        Objects.requireNonNull( causeOfPanic, "The cause of a panic cannot be null!" );
        this.panicked.add( namedDatabaseId );
        ReconcilerResult reconcilerResult = this.trigger( ReconcilerRequest.forPanickedDatabase( namedDatabaseId, causeOfPanic ) );
        reconcilerResult.whenComplete( () -> {
            this.panicked.remove( namedDatabaseId );
        } );
    }

    private boolean triggerReconcilerOnStoreCopy( NamedDatabaseId namedDatabaseId )
    {
        if ( !this.bootstrapping.contains( namedDatabaseId ) && !this.panicked.contains( namedDatabaseId ) )
        {
            this.trigger( ReconcilerRequest.simple() ).await( namedDatabaseId );
            return true;
        }
        else
        {
            return false;
        }
    }

    public ClusterInternalDbmsOperator.BootstrappingHandle bootstrap( NamedDatabaseId namedDatabaseId )
    {
        this.bootstrapping.add( namedDatabaseId );
        return new ClusterInternalDbmsOperator.BootstrappingHandle( this, namedDatabaseId );
    }

    public static class BootstrappingHandle
    {
        private final ClusterInternalDbmsOperator operator;
        private final NamedDatabaseId namedDatabaseId;

        private BootstrappingHandle( ClusterInternalDbmsOperator operator, NamedDatabaseId namedDatabaseId )
        {
            this.operator = operator;
            this.namedDatabaseId = namedDatabaseId;
        }

        public void release()
        {
            if ( !this.operator.bootstrapping.remove( this.namedDatabaseId ) )
            {
                throw new IllegalStateException( "Bootstrapped was already called for " + this.namedDatabaseId );
            }
        }
    }

    public static class StoreCopyHandle
    {
        private final ClusterInternalDbmsOperator operator;
        private final NamedDatabaseId namedDatabaseId;

        private StoreCopyHandle( ClusterInternalDbmsOperator operator, NamedDatabaseId namedDatabaseId )
        {
            this.operator = operator;
            this.namedDatabaseId = namedDatabaseId;
        }

        public boolean release()
        {
            boolean exists = this.operator.storeCopying.remove( this );
            if ( !exists )
            {
                throw new IllegalStateException( "Restart was already called for " + this.namedDatabaseId );
            }
            else
            {
                return this.operator.triggerReconcilerOnStoreCopy( this.namedDatabaseId );
            }
        }

        public NamedDatabaseId databaseId()
        {
            return this.namedDatabaseId;
        }

        public boolean equals( Object o )
        {
            if ( this == o )
            {
                return true;
            }
            else if ( o != null && this.getClass() == o.getClass() )
            {
                ClusterInternalDbmsOperator.StoreCopyHandle that = (ClusterInternalDbmsOperator.StoreCopyHandle) o;
                return Objects.equals( this.namedDatabaseId, that.namedDatabaseId );
            }
            else
            {
                return false;
            }
        }

        public int hashCode()
        {
            return Objects.hash( new Object[]{this.namedDatabaseId} );
        }
    }
}
