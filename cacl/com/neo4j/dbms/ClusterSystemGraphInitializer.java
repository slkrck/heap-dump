package com.neo4j.dbms;

import org.neo4j.configuration.Config;
import org.neo4j.dbms.database.DatabaseManager;
import org.neo4j.dbms.database.SystemGraphDbmsModel;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;

public class ClusterSystemGraphInitializer extends EnterpriseSystemGraphInitializer
{
    public ClusterSystemGraphInitializer( DatabaseManager<?> databaseManager, Config config )
    {
        super( databaseManager, config );
    }

    public void initializeSystemGraph( GraphDatabaseService system ) throws Exception
    {
        super.initializeSystemGraph( system );
        this.clearClusterProperties( system );
    }

    private void clearClusterProperties( GraphDatabaseService system )
    {
        Transaction tx = system.beginTx();

        try
        {
            ResourceIterator databaseItr = tx.findNodes( SystemGraphDbmsModel.DATABASE_LABEL );

            try
            {
                while ( databaseItr.hasNext() )
                {
                    Node database = (Node) databaseItr.next();
                    database.removeProperty( "initial_members" );
                    database.removeProperty( "store_creation_time" );
                    database.removeProperty( "store_random_id" );
                    database.removeProperty( "store_version" );
                }
            }
            catch ( Throwable var8 )
            {
                if ( databaseItr != null )
                {
                    try
                    {
                        databaseItr.close();
                    }
                    catch ( Throwable var7 )
                    {
                        var8.addSuppressed( var7 );
                    }
                }

                throw var8;
            }

            if ( databaseItr != null )
            {
                databaseItr.close();
            }

            tx.commit();
        }
        catch ( Throwable var9 )
        {
            if ( tx != null )
            {
                try
                {
                    tx.close();
                }
                catch ( Throwable var6 )
                {
                    var9.addSuppressed( var6 );
                }
            }

            throw var9;
        }

        if ( tx != null )
        {
            tx.close();
        }
    }
}
