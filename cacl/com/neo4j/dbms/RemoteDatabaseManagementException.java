package com.neo4j.dbms;

import org.neo4j.dbms.api.DatabaseManagementException;

public class RemoteDatabaseManagementException extends DatabaseManagementException
{
    public RemoteDatabaseManagementException( String message )
    {
        super( message );
    }
}
