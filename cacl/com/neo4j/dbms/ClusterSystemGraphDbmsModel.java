package com.neo4j.dbms;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.NotFoundException;
import org.neo4j.graphdb.Transaction;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.impl.store.MetaDataStore;
import org.neo4j.storageengine.api.StoreId;

public class ClusterSystemGraphDbmsModel extends EnterpriseSystemGraphDbmsModel
{
    static final String INITIAL_MEMBERS = "initial_members";
    static final String STORE_CREATION_TIME = "store_creation_time";
    static final String STORE_RANDOM_ID = "store_random_id";
    static final String STORE_VERSION = "store_version";

    public ClusterSystemGraphDbmsModel( Supplier<GraphDatabaseService> systemDatabase )
    {
        super( systemDatabase );
    }

    public Set<UUID> getInitialMembers( NamedDatabaseId namedDatabaseId )
    {
        try
        {
            Transaction tx = ((GraphDatabaseService) this.systemDatabase.get()).beginTx();

            Set var5;
            try
            {
                Node node = this.databaseNode( namedDatabaseId, tx );
                String[] initialMembers = (String[]) node.getProperty( "initial_members" );
                var5 = (Set) Arrays.stream( initialMembers ).map( UUID::fromString ).collect( Collectors.toSet() );
            }
            catch ( Throwable var7 )
            {
                if ( tx != null )
                {
                    try
                    {
                        tx.close();
                    }
                    catch ( Throwable var6 )
                    {
                        var7.addSuppressed( var6 );
                    }
                }

                throw var7;
            }

            if ( tx != null )
            {
                tx.close();
            }

            return var5;
        }
        catch ( NotFoundException var8 )
        {
            return Collections.emptySet();
        }
    }

    public StoreId getStoreId( NamedDatabaseId namedDatabaseId )
    {
        Transaction tx = ((GraphDatabaseService) this.systemDatabase.get()).beginTx();

        StoreId var9;
        try
        {
            Node node = this.databaseNode( namedDatabaseId, tx );
            long creationTime = (Long) node.getProperty( "store_creation_time" );
            long randomId = (Long) node.getProperty( "store_random_id" );
            String storeVersion = (String) node.getProperty( "store_version" );
            var9 = new StoreId( creationTime, randomId, MetaDataStore.versionStringToLong( storeVersion ) );
        }
        catch ( Throwable var11 )
        {
            if ( tx != null )
            {
                try
                {
                    tx.close();
                }
                catch ( Throwable var10 )
                {
                    var11.addSuppressed( var10 );
                }
            }

            throw var11;
        }

        if ( tx != null )
        {
            tx.close();
        }

        return var9;
    }

    private Node databaseNode( NamedDatabaseId databaseId, Transaction tx )
    {
        return (Node) Optional.ofNullable( tx.findNode( DATABASE_LABEL, "uuid", databaseId.databaseId().uuid().toString() ) ).or( () -> {
            return Optional.ofNullable( tx.findNode( DELETED_DATABASE_LABEL, "uuid", databaseId.databaseId().uuid().toString() ) );
        } ).orElseThrow( () -> {
            return new IllegalStateException( "System database must contain a node for database " + databaseId.name() );
        } );
    }
}
