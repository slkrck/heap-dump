package com.neo4j.dbms;

import org.neo4j.kernel.database.NamedDatabaseId;

public interface ReplicatedDatabaseEventService
{
    ReplicatedDatabaseEventService.ReplicatedDatabaseEventDispatch NO_EVENT_DISPATCH = new ReplicatedDatabaseEventService.ReplicatedDatabaseEventDispatch()
    {
        public void fireTransactionCommitted( long txId )
        {
        }

        public void fireStoreReplaced( long txId )
        {
        }
    };

    void registerListener( NamedDatabaseId var1, ReplicatedDatabaseEventService.ReplicatedDatabaseEventListener var2 );

    void unregisterListener( NamedDatabaseId var1, ReplicatedDatabaseEventService.ReplicatedDatabaseEventListener var2 );

    ReplicatedDatabaseEventService.ReplicatedDatabaseEventDispatch getDatabaseEventDispatch( NamedDatabaseId var1 );

    public interface ReplicatedDatabaseEventDispatch
    {
        void fireTransactionCommitted( long var1 );

        void fireStoreReplaced( long var1 );
    }

    public interface ReplicatedDatabaseEventListener
    {
        void transactionCommitted( long var1 );

        void storeReplaced( long var1 );
    }
}
