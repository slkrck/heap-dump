package com.neo4j.collection;

import java.util.Arrays;

public class CircularBuffer<V>
{
    private final int arraySize;
    private Object[] elementArr;
    private int S;
    private int E;

    public CircularBuffer( int capacity )
    {
        if ( capacity <= 0 )
        {
            throw new IllegalArgumentException( "Capacity must be > 0." );
        }
        else
        {
            this.arraySize = capacity + 1;
            this.elementArr = new Object[this.arraySize];
        }
    }

    public void clear( V[] evictions )
    {
        if ( evictions.length != this.arraySize - 1 )
        {
            throw new IllegalArgumentException( "The eviction array must be of the same size as the capacity of the circular buffer." );
        }
        else
        {
            for ( int var2 = 0; this.S != this.E; this.S = this.pos( this.S, 1 ) )
            {
                evictions[var2++] = this.elementArr[this.S];
            }

            this.S = 0;
            this.E = 0;
            Arrays.fill( this.elementArr, (Object) null );
        }
    }

    private int pos( int base, int delta )
    {
        return Math.floorMod( base + delta, this.arraySize );
    }

    public V append( V e )
    {
        this.elementArr[this.E] = e;
        this.E = this.pos( this.E, 1 );
        if ( this.E == this.S )
        {
            V old = this.elementArr[this.E];
            this.elementArr[this.E] = null;
            this.S = this.pos( this.S, 1 );
            return old;
        }
        else
        {
            return null;
        }
    }

    public V read( int idx )
    {
        return this.elementArr[this.pos( this.S, idx )];
    }

    public V remove()
    {
        if ( this.S == this.E )
        {
            return null;
        }
        else
        {
            V e = this.elementArr[this.S];
            this.elementArr[this.S] = null;
            this.S = this.pos( this.S, 1 );
            return e;
        }
    }

    public V removeHead()
    {
        if ( this.S == this.E )
        {
            return null;
        }
        else
        {
            this.E = this.pos( this.E, -1 );
            V e = this.elementArr[this.E];
            this.elementArr[this.E] = null;
            return e;
        }
    }

    public int size()
    {
        return Math.floorMod( this.E - this.S, this.arraySize );
    }
}
