package org.neo4j.cypher.internal.physicalplanning;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple5;
import scala.None.;

public final class BufferDefinition$ implements Serializable
{
    public static BufferDefinition$ MODULE$;

    static
    {
        new BufferDefinition$();
    }

    private BufferDefinition$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "BufferDefinition";
    }

    public BufferDefinition apply( final int id, final ArgumentStateMapId[] reducers, final ArgumentStateMapId[] workCancellers,
            final ArgumentStateMapId[] downstreamStates, final BufferVariant variant, final SlotConfiguration bufferSlotConfiguration )
    {
        return new BufferDefinition( id, reducers, workCancellers, downstreamStates, variant, bufferSlotConfiguration );
    }

    public Option<Tuple5<BufferId,int[],int[],int[],BufferVariant>> unapply( final BufferDefinition x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :
        new Some( new Tuple5( new BufferId( x$0.id() ), x$0.reducers(), x$0.workCancellers(), x$0.downstreamStates(), x$0.variant() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
