package org.neo4j.cypher.internal.physicalplanning;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.collection.immutable.Set;
import scala.runtime.AbstractFunction2;

public final class LongSlotWithAliases$ extends AbstractFunction2<LongSlot,Set<String>,LongSlotWithAliases> implements Serializable
{
    public static LongSlotWithAliases$ MODULE$;

    static
    {
        new LongSlotWithAliases$();
    }

    private LongSlotWithAliases$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "LongSlotWithAliases";
    }

    public LongSlotWithAliases apply( final LongSlot slot, final Set<String> aliases )
    {
        return new LongSlotWithAliases( slot, aliases );
    }

    public Option<Tuple2<LongSlot,Set<String>>> unapply( final LongSlotWithAliases x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.slot(), x$0.aliases() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
