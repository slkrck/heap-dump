package org.neo4j.cypher.internal.physicalplanning;

public final class OperatorFusionPolicy$
{
    public static OperatorFusionPolicy$ MODULE$;

    static
    {
        new OperatorFusionPolicy$();
    }

    private OperatorFusionPolicy$()
    {
        MODULE$ = this;
    }

    public OperatorFusionPolicy apply( final boolean fusionEnabled, final boolean fusionOverPipelinesEnabled )
    {
        return (OperatorFusionPolicy) (!fusionEnabled ? OperatorFusionPolicy.OPERATOR_FUSION_DISABLED$.MODULE$
                                                      : (fusionOverPipelinesEnabled ? OperatorFusionPolicy.OPERATOR_FUSION_OVER_PIPELINES$.MODULE$
                                                                                    : OperatorFusionPolicy.OPERATOR_FUSION_WITHIN_PIPELINE$.MODULE$));
    }
}
