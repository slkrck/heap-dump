package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.v4_0.util.attribution.Id;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.runtime.AbstractFunction3;
import scala.runtime.BoxesRunTime;

public final class ArgumentStateDefinition$ extends AbstractFunction3<ArgumentStateMapId,Id,Object,ArgumentStateDefinition> implements Serializable
{
    public static ArgumentStateDefinition$ MODULE$;

    static
    {
        new ArgumentStateDefinition$();
    }

    private ArgumentStateDefinition$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "ArgumentStateDefinition";
    }

    public ArgumentStateDefinition apply( final int id, final int planId, final int argumentSlotOffset )
    {
        return new ArgumentStateDefinition( id, planId, argumentSlotOffset );
    }

    public Option<Tuple3<ArgumentStateMapId,Id,Object>> unapply( final ArgumentStateDefinition x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :
        new Some( new Tuple3( new ArgumentStateMapId( x$0.id() ), new Id( x$0.planId() ), BoxesRunTime.boxToInteger( x$0.argumentSlotOffset() ) ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
