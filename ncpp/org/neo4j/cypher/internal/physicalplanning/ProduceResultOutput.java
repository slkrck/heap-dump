package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.logical.plans.ProduceResult;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class ProduceResultOutput implements OutputDefinition, Product, Serializable
{
    private final ProduceResult plan;

    public ProduceResultOutput( final ProduceResult plan )
    {
        this.plan = plan;
        Product.$init$( this );
    }

    public static Option<ProduceResult> unapply( final ProduceResultOutput x$0 )
    {
        return ProduceResultOutput$.MODULE$.unapply( var0 );
    }

    public static ProduceResultOutput apply( final ProduceResult plan )
    {
        return ProduceResultOutput$.MODULE$.apply( var0 );
    }

    public static <A> Function1<ProduceResult,A> andThen( final Function1<ProduceResultOutput,A> g )
    {
        return ProduceResultOutput$.MODULE$.andThen( var0 );
    }

    public static <A> Function1<A,ProduceResultOutput> compose( final Function1<A,ProduceResult> g )
    {
        return ProduceResultOutput$.MODULE$.compose( var0 );
    }

    public ProduceResult plan()
    {
        return this.plan;
    }

    public ProduceResultOutput copy( final ProduceResult plan )
    {
        return new ProduceResultOutput( plan );
    }

    public ProduceResult copy$default$1()
    {
        return this.plan();
    }

    public String productPrefix()
    {
        return "ProduceResultOutput";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return this.plan();
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof ProduceResultOutput;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        label47:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof ProduceResultOutput )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label47;
                }

                label35:
                {
                    label34:
                    {
                        ProduceResultOutput var4 = (ProduceResultOutput) x$1;
                        ProduceResult var10000 = this.plan();
                        ProduceResult var5 = var4.plan();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label34;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label34;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var6 = true;
                            break label35;
                        }
                    }

                    var6 = false;
                }

                if ( !var6 )
                {
                    break label47;
                }
            }

            var6 = true;
            return var6;
        }

        var6 = false;
        return var6;
    }
}
