package org.neo4j.cypher.internal.physicalplanning;

import java.util.function.ToLongFunction;

import org.neo4j.cypher.internal.runtime.EntityById;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import org.neo4j.values.AnyValue;
import scala.Function1;
import scala.Function2;
import scala.Function3;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public final class SlotConfigurationUtils
{
    public static boolean makeGetPrimitiveRelationshipFromSlotFunctionFor$default$2()
    {
        return SlotConfigurationUtils$.MODULE$.makeGetPrimitiveRelationshipFromSlotFunctionFor$default$2();
    }

    public static boolean makeGetPrimitiveNodeFromSlotFunctionFor$default$2()
    {
        return SlotConfigurationUtils$.MODULE$.makeGetPrimitiveNodeFromSlotFunctionFor$default$2();
    }

    public static boolean makeGetPrimitiveFromSlotFunctionFor$default$3()
    {
        return SlotConfigurationUtils$.MODULE$.makeGetPrimitiveFromSlotFunctionFor$default$3();
    }

    public static void generateSlotAccessorFunctions( final SlotConfiguration slots )
    {
        SlotConfigurationUtils$.MODULE$.generateSlotAccessorFunctions( var0 );
    }

    public static Function3<ExecutionContext,Object,EntityById,BoxedUnit> makeSetPrimitiveRelationshipInSlotFunctionFor( final Slot slot )
    {
        return SlotConfigurationUtils$.MODULE$.makeSetPrimitiveRelationshipInSlotFunctionFor( var0 );
    }

    public static Function3<ExecutionContext,Object,EntityById,BoxedUnit> makeSetPrimitiveNodeInSlotFunctionFor( final Slot slot )
    {
        return SlotConfigurationUtils$.MODULE$.makeSetPrimitiveNodeInSlotFunctionFor( var0 );
    }

    public static Function3<ExecutionContext,Object,EntityById,BoxedUnit> makeSetPrimitiveInSlotFunctionFor( final Slot slot, final CypherType valueType )
    {
        return SlotConfigurationUtils$.MODULE$.makeSetPrimitiveInSlotFunctionFor( var0, var1 );
    }

    public static Function2<ExecutionContext,AnyValue,BoxedUnit> makeSetValueInSlotFunctionFor( final Slot slot )
    {
        return SlotConfigurationUtils$.MODULE$.makeSetValueInSlotFunctionFor( var0 );
    }

    public static ToLongFunction<ExecutionContext> NO_ENTITY_FUNCTION()
    {
        return SlotConfigurationUtils$.MODULE$.NO_ENTITY_FUNCTION();
    }

    public static ToLongFunction<ExecutionContext> makeGetPrimitiveRelationshipFromSlotFunctionFor( final Slot slot, final boolean throwOfTypeError )
    {
        return SlotConfigurationUtils$.MODULE$.makeGetPrimitiveRelationshipFromSlotFunctionFor( var0, var1 );
    }

    public static ToLongFunction<ExecutionContext> makeGetPrimitiveNodeFromSlotFunctionFor( final Slot slot, final boolean throwOnTypeError )
    {
        return SlotConfigurationUtils$.MODULE$.makeGetPrimitiveNodeFromSlotFunctionFor( var0, var1 );
    }

    public static ToLongFunction<ExecutionContext> makeGetPrimitiveFromSlotFunctionFor( final Slot slot, final CypherType returnType,
            final boolean throwOfTypeError )
    {
        return SlotConfigurationUtils$.MODULE$.makeGetPrimitiveFromSlotFunctionFor( var0, var1, var2 );
    }

    public static Function1<ExecutionContext,AnyValue> makeGetValueFromSlotFunctionFor( final Slot slot )
    {
        return SlotConfigurationUtils$.MODULE$.makeGetValueFromSlotFunctionFor( var0 );
    }

    public static long PRIMITIVE_NULL()
    {
        return SlotConfigurationUtils$.MODULE$.PRIMITIVE_NULL();
    }
}
