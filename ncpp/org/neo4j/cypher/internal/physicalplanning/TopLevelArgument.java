package org.neo4j.cypher.internal.physicalplanning;

import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class TopLevelArgument
{
    public static int SLOT_OFFSET()
    {
        return TopLevelArgument$.MODULE$.SLOT_OFFSET();
    }

    public static long VALUE()
    {
        return TopLevelArgument$.MODULE$.VALUE();
    }

    public static void assertTopLevelArgument( final long argument )
    {
        TopLevelArgument$.MODULE$.assertTopLevelArgument( var0 );
    }

    public static class TopLevelArgumentException extends RuntimeException
    {
        public TopLevelArgumentException( final long argument )
        {
            super( (new StringBuilder( 44 )).append( "The top level argument has to be 0, but got " ).append( argument ).toString() );
        }
    }
}
