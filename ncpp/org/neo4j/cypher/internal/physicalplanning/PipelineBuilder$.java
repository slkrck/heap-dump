package org.neo4j.cypher.internal.physicalplanning;

import scala.Function1;
import scala.Serializable;
import scala.collection.IndexedSeq;
import scala.collection.IndexedSeqOptimized;
import scala.collection.TraversableOnce;
import scala.collection.mutable.ArrayBuffer;
import scala.collection.mutable.ArrayBuffer.;

public final class PipelineBuilder$
{
    public static PipelineBuilder$ MODULE$;

    static
    {
        new PipelineBuilder$();
    }

    private PipelineBuilder$()
    {
        MODULE$ = this;
    }

    public ExecutionGraphDefinition build( final PipelineBreakingPolicy breakingPolicy, final OperatorFusionPolicy operatorFusionPolicy,
            final PhysicalPlan physicalPlan )
    {
        PipelineTreeBuilder.ExecutionStateDefinitionBuild executionStateDefinitionBuild = new PipelineTreeBuilder.ExecutionStateDefinitionBuild( physicalPlan );
        PipelineTreeBuilder pipelineTreeBuilder =
                new PipelineTreeBuilder( breakingPolicy, operatorFusionPolicy, executionStateDefinitionBuild, physicalPlan.slotConfigurations(),
                        physicalPlan.argumentSizes() );
        pipelineTreeBuilder.build( physicalPlan.logicalPlan() );
        return new ExecutionGraphDefinition( physicalPlan, (IndexedSeq) executionStateDefinitionBuild.buffers().map( ( bufferDefinition ) -> {
            return MODULE$.mapBuffer( bufferDefinition );
        },.MODULE$.canBuildFrom() ),(IndexedSeq) executionStateDefinitionBuild.argumentStateMaps().map( ( build ) -> {
        return MODULE$.mapArgumentStateDefinition( build );
    },.MODULE$.canBuildFrom()),(IndexedSeq) pipelineTreeBuilder.pipelines().map( ( pipeline ) -> {
        return MODULE$.mapPipeline( pipeline );
    },.MODULE$.canBuildFrom()),pipelineTreeBuilder.applyRhsPlans().toMap( scala.Predef..MODULE$.$conforms()));
    }

    private PipelineDefinition mapPipeline( final PipelineTreeBuilder.PipelineDefinitionBuild pipeline )
    {
        return new PipelineDefinition( pipeline.id(), pipeline.lhs(), pipeline.rhs(), pipeline.headPlan(), pipeline.fusedPlans(),
                this.mapBuffer( pipeline.inputBuffer() ), pipeline.outputDefinition(), pipeline.middlePlans(), pipeline.serial() );
    }

    private BufferDefinition mapBuffer( final PipelineTreeBuilder.BufferDefinitionBuild bufferDefinition )
    {
        ArrayBuffer downstreamReducers = (ArrayBuffer) bufferDefinition.downstreamStates().collect( new Serializable()
        {
            public static final long serialVersionUID = 0L;

            public final <A1 extends PipelineTreeBuilder.DownstreamStateOperator, B1> B1 applyOrElse( final A1 x1, final Function1<A1,B1> default )
            {
                Object var3;
                if ( x1 instanceof PipelineTreeBuilder.DownstreamReduce )
                {
                    PipelineTreeBuilder.DownstreamReduce var5 = (PipelineTreeBuilder.DownstreamReduce) x1;
                    var3 = new ArgumentStateMapId( var5.id() );
                }
                else
                {
                    var3 = var2.apply( x1 );
                }

                return var3;
            }

            public final boolean isDefinedAt( final PipelineTreeBuilder.DownstreamStateOperator x1 )
            {
                boolean var2;
                if ( x1 instanceof PipelineTreeBuilder.DownstreamReduce )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                return var2;
            }
        },.MODULE$.canBuildFrom());
        ArrayBuffer workCancellerIDs = (ArrayBuffer) bufferDefinition.downstreamStates().collect( new Serializable()
        {
            public static final long serialVersionUID = 0L;

            public final <A1 extends PipelineTreeBuilder.DownstreamStateOperator, B1> B1 applyOrElse( final A1 x2, final Function1<A1,B1> default )
            {
                Object var3;
                if ( x2 instanceof PipelineTreeBuilder.DownstreamWorkCanceller )
                {
                    PipelineTreeBuilder.DownstreamWorkCanceller var5 = (PipelineTreeBuilder.DownstreamWorkCanceller) x2;
                    var3 = new ArgumentStateMapId( var5.id() );
                }
                else
                {
                    var3 = var2.apply( x2 );
                }

                return var3;
            }

            public final boolean isDefinedAt( final PipelineTreeBuilder.DownstreamStateOperator x2 )
            {
                boolean var2;
                if ( x2 instanceof PipelineTreeBuilder.DownstreamWorkCanceller )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                return var2;
            }
        },.MODULE$.canBuildFrom());
        ArrayBuffer downstreamStateIDs = (ArrayBuffer) bufferDefinition.downstreamStates().collect( new Serializable()
        {
            public static final long serialVersionUID = 0L;

            public final <A1 extends PipelineTreeBuilder.DownstreamStateOperator, B1> B1 applyOrElse( final A1 x3, final Function1<A1,B1> default )
            {
                Object var3;
                if ( x3 instanceof PipelineTreeBuilder.DownstreamState )
                {
                    PipelineTreeBuilder.DownstreamState var5 = (PipelineTreeBuilder.DownstreamState) x3;
                    var3 = new ArgumentStateMapId( var5.id() );
                }
                else
                {
                    var3 = var2.apply( x3 );
                }

                return var3;
            }

            public final boolean isDefinedAt( final PipelineTreeBuilder.DownstreamStateOperator x3 )
            {
                boolean var2;
                if ( x3 instanceof PipelineTreeBuilder.DownstreamState )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                return var2;
            }
        },.MODULE$.canBuildFrom());
        Object var2;
        if ( bufferDefinition instanceof PipelineTreeBuilder.AttachBufferDefinitionBuild )
        {
            PipelineTreeBuilder.AttachBufferDefinitionBuild var8 = (PipelineTreeBuilder.AttachBufferDefinitionBuild) bufferDefinition;
            var2 = new AttachBufferVariant( this.mapBuffer( var8.applyBuffer() ), var8.outputSlotConfiguration(), var8.argumentSlotOffset(),
                    var8.argumentSize() );
        }
        else if ( bufferDefinition instanceof PipelineTreeBuilder.ApplyBufferDefinitionBuild )
        {
            PipelineTreeBuilder.ApplyBufferDefinitionBuild var9 = (PipelineTreeBuilder.ApplyBufferDefinitionBuild) bufferDefinition;
            var2 = new ApplyBufferVariant( var9.argumentSlotOffset(),
                    (ArgumentStateMapId[]) ((TraversableOnce) ((IndexedSeqOptimized) var9.reducersOnRHS().map( ( argStateBuild ) -> {
                        return new ArgumentStateMapId( $anonfun$mapBuffer$1( argStateBuild ) );
                    },.MODULE$.canBuildFrom())).reverse() ).toArray( scala.reflect.ClassTag..MODULE$.apply( ArgumentStateMapId.class )),
            (BufferId[]) var9.delegates().toArray( scala.reflect.ClassTag..MODULE$.apply( BufferId.class )));
        }
        else if ( bufferDefinition instanceof PipelineTreeBuilder.ArgumentStateBufferDefinitionBuild )
        {
            PipelineTreeBuilder.ArgumentStateBufferDefinitionBuild var10 = (PipelineTreeBuilder.ArgumentStateBufferDefinitionBuild) bufferDefinition;
            var2 = new ArgumentStateBufferVariant( var10.argumentStateMapId() );
        }
        else if ( bufferDefinition instanceof PipelineTreeBuilder.MorselBufferDefinitionBuild )
        {
            var2 = RegularBufferVariant$.MODULE$;
        }
        else if ( bufferDefinition instanceof PipelineTreeBuilder.OptionalMorselBufferDefinitionBuild )
        {
            PipelineTreeBuilder.OptionalMorselBufferDefinitionBuild var11 = (PipelineTreeBuilder.OptionalMorselBufferDefinitionBuild) bufferDefinition;
            var2 = new OptionalBufferVariant( var11.argumentStateMapId() );
        }
        else if ( bufferDefinition instanceof PipelineTreeBuilder.DelegateBufferDefinitionBuild )
        {
            var2 = RegularBufferVariant$.MODULE$;
        }
        else
        {
            if ( !(bufferDefinition instanceof PipelineTreeBuilder.LHSAccumulatingRHSStreamingBufferDefinitionBuild) )
            {
                throw new IllegalStateException(
                        (new StringBuilder( 42 )).append( "Unexpected type of BufferDefinitionBuild: " ).append( bufferDefinition ).toString() );
            }

            PipelineTreeBuilder.LHSAccumulatingRHSStreamingBufferDefinitionBuild var12 =
                    (PipelineTreeBuilder.LHSAccumulatingRHSStreamingBufferDefinitionBuild) bufferDefinition;
            var2 = new LHSAccumulatingRHSStreamingBufferVariant( var12.lhsPipelineId(), var12.rhsPipelineId(), var12.lhsArgumentStateMapId(),
                    var12.rhsArgumentStateMapId() );
        }

        return new BufferDefinition( bufferDefinition.id(),
                (ArgumentStateMapId[]) downstreamReducers.toArray( scala.reflect.ClassTag..MODULE$.apply( ArgumentStateMapId.class ) ),
        (ArgumentStateMapId[]) workCancellerIDs.toArray( scala.reflect.ClassTag..MODULE$.apply( ArgumentStateMapId.class )),
        (ArgumentStateMapId[]) downstreamStateIDs.toArray( scala.reflect.ClassTag..MODULE$.apply( ArgumentStateMapId.class )),
        (BufferVariant) var2, bufferDefinition.bufferConfiguration());
    }

    private ArgumentStateDefinition mapArgumentStateDefinition( final PipelineTreeBuilder.ArgumentStateDefinitionBuild build )
    {
        return new ArgumentStateDefinition( build.id(), build.planId(), build.argumentSlotOffset() );
    }
}
