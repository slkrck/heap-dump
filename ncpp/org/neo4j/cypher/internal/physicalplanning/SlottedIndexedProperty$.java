package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.logical.plans.IndexedProperty;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.BoxesRunTime;

public final class SlottedIndexedProperty$ implements Serializable
{
    public static SlottedIndexedProperty$ MODULE$;

    static
    {
        new SlottedIndexedProperty$();
    }

    private SlottedIndexedProperty$()
    {
        MODULE$ = this;
    }

    public SlottedIndexedProperty apply( final String node, final IndexedProperty property, final SlotConfiguration slots )
    {
        Option maybeOffset =
                property.shouldGetValue() ? new Some( BoxesRunTime.boxToInteger( slots.getCachedPropertyOffsetFor( property.asCachedProperty( node ) ) ) ) : .
        MODULE$;
        return new SlottedIndexedProperty( property.propertyKeyToken().nameId().id(), (Option) maybeOffset );
    }

    public SlottedIndexedProperty apply( final int propertyKeyId, final Option<Object> maybeCachedNodePropertySlot )
    {
        return new SlottedIndexedProperty( propertyKeyId, maybeCachedNodePropertySlot );
    }

    public Option<Tuple2<Object,Option<Object>>> unapply( final SlottedIndexedProperty x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( BoxesRunTime.boxToInteger( x$0.propertyKeyId() ), x$0.maybeCachedNodePropertySlot() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
