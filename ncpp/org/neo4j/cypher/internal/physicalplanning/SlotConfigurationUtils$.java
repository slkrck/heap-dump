package org.neo4j.cypher.internal.physicalplanning;

import java.util.function.ToLongFunction;

import org.neo4j.cypher.internal.runtime.EntityById;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import org.neo4j.cypher.internal.v4_0.util.symbols.NodeType;
import org.neo4j.cypher.internal.v4_0.util.symbols.RelationshipType;
import org.neo4j.cypher.internal.v4_0.util.symbols.package.;
import org.neo4j.exceptions.InternalException;
import org.neo4j.exceptions.ParameterWrongTypeException;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Values;
import org.neo4j.values.virtual.VirtualNodeValue;
import org.neo4j.values.virtual.VirtualRelationshipValue;
import org.neo4j.values.virtual.VirtualValues;
import scala.Function1;
import scala.Function2;
import scala.Function3;
import scala.Tuple2;
import scala.Tuple3;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

public final class SlotConfigurationUtils$
{
    public static SlotConfigurationUtils$ MODULE$;

    static
    {
        new SlotConfigurationUtils$();
    }

    private final long PRIMITIVE_NULL;
    private final ToLongFunction<ExecutionContext> NO_ENTITY_FUNCTION;

    private SlotConfigurationUtils$()
    {
        MODULE$ = this;
        this.PRIMITIVE_NULL = -1L;
        this.NO_ENTITY_FUNCTION = ( value ) -> {
            return MODULE$.PRIMITIVE_NULL();
        };
    }

    public long PRIMITIVE_NULL()
    {
        return this.PRIMITIVE_NULL;
    }

    public Function1<ExecutionContext,AnyValue> makeGetValueFromSlotFunctionFor( final Slot slot )
    {
        Function1 var2;
        int offset;
        label106:
        {
            boolean var3 = false;
            LongSlot var4 = null;
            NodeType var10000;
            if ( slot instanceof LongSlot )
            {
                var3 = true;
                var4 = (LongSlot) slot;
                offset = var4.offset();
                boolean var7 = var4.nullable();
                CypherType var8 = var4.typ();
                if ( !var7 )
                {
                    var10000 = .MODULE$.CTNode();
                    if ( var10000 == null )
                    {
                        if ( var8 == null )
                        {
                            break label106;
                        }
                    }
                    else if ( var10000.equals( var8 ) )
                    {
                        break label106;
                    }
                }
            }

            int offset;
            label107:
            {
                RelationshipType var24;
                if ( var3 )
                {
                    offset = var4.offset();
                    boolean var11 = var4.nullable();
                    CypherType var12 = var4.typ();
                    if ( !var11 )
                    {
                        var24 = .MODULE$.CTRelationship();
                        if ( var24 == null )
                        {
                            if ( var12 == null )
                            {
                                break label107;
                            }
                        }
                        else if ( var24.equals( var12 ) )
                        {
                            break label107;
                        }
                    }
                }

                if ( var3 )
                {
                    int offset = var4.offset();
                    boolean var15 = var4.nullable();
                    CypherType var16 = var4.typ();
                    if ( var15 )
                    {
                        label108:
                        {
                            var10000 = .MODULE$.CTNode();
                            if ( var10000 == null )
                            {
                                if ( var16 != null )
                                {
                                    break label108;
                                }
                            }
                            else if ( !var10000.equals( var16 ) )
                            {
                                break label108;
                            }

                            var2 = ( context ) -> {
                                long nodeId = context.getLongAt( offset );
                                return (AnyValue) (nodeId == MODULE$.PRIMITIVE_NULL() ? Values.NO_VALUE : VirtualValues.node( nodeId ));
                            };
                            return var2;
                        }
                    }
                }

                int offset;
                label109:
                {
                    if ( var3 )
                    {
                        offset = var4.offset();
                        boolean var19 = var4.nullable();
                        CypherType var20 = var4.typ();
                        if ( var19 )
                        {
                            var24 = .MODULE$.CTRelationship();
                            if ( var24 == null )
                            {
                                if ( var20 == null )
                                {
                                    break label109;
                                }
                            }
                            else if ( var24.equals( var20 ) )
                            {
                                break label109;
                            }
                        }
                    }

                    if ( !(slot instanceof RefSlot) )
                    {
                        throw new InternalException( (new StringBuilder( 40 )).append( "Do not know how to make getter for slot " ).append( slot ).toString() );
                    }

                    RefSlot var22 = (RefSlot) slot;
                    int offset = var22.offset();
                    var2 = ( context ) -> {
                        return context.getRefAt( offset );
                    };
                    return var2;
                }

                var2 = ( context ) -> {
                    long relId = context.getLongAt( offset );
                    return (AnyValue) (relId == MODULE$.PRIMITIVE_NULL() ? Values.NO_VALUE : VirtualValues.relationship( relId ));
                };
                return var2;
            }

            var2 = ( context ) -> {
                return VirtualValues.relationship( context.getLongAt( offset ) );
            };
            return var2;
        }

        var2 = ( context ) -> {
            return VirtualValues.node( context.getLongAt( offset ) );
        };
        return var2;
    }

    public ToLongFunction<ExecutionContext> makeGetPrimitiveFromSlotFunctionFor( final Slot slot, final CypherType returnType, final boolean throwOfTypeError )
    {
        Tuple3 var6 = new Tuple3( slot, returnType, BoxesRunTime.boxToBoolean( throwOfTypeError ) );
        ToLongFunction var4;
        NodeType var10000;
        RelationshipType var53;
        if ( var6 != null )
        {
            Slot var7 = (Slot) var6._1();
            CypherType var8 = (CypherType) var6._2();
            if ( var7 instanceof LongSlot )
            {
                boolean var5;
                int offset;
                label180:
                {
                    label171:
                    {
                        LongSlot var9 = (LongSlot) var7;
                        offset = var9.offset();
                        var10000 = .MODULE$.CTNode();
                        if ( var10000 == null )
                        {
                            if ( var8 != null )
                            {
                                break label171;
                            }
                        }
                        else if ( !var10000.equals( var8 ) )
                        {
                            break label171;
                        }

                        var5 = true;
                        break label180;
                    }

                    label165:
                    {
                        var53 = .MODULE$.CTRelationship();
                        if ( var53 == null )
                        {
                            if ( var8 == null )
                            {
                                break label165;
                            }
                        }
                        else if ( var53.equals( var8 ) )
                        {
                            break label165;
                        }

                        var5 = false;
                        break label180;
                    }

                    var5 = true;
                }

                if ( var5 )
                {
                    var4 = ( context ) -> {
                        return context.getLongAt( offset );
                    };
                    return var4;
                }
            }
        }

        if ( var6 != null )
        {
            Slot var13 = (Slot) var6._1();
            CypherType var14 = (CypherType) var6._2();
            boolean var15 = BoxesRunTime.unboxToBoolean( var6._3() );
            if ( var13 instanceof RefSlot )
            {
                RefSlot var16 = (RefSlot) var13;
                int offset = var16.offset();
                boolean var18 = var16.nullable();
                if ( !var18 )
                {
                    label154:
                    {
                        var10000 = .MODULE$.CTNode();
                        if ( var10000 == null )
                        {
                            if ( var14 != null )
                            {
                                break label154;
                            }
                        }
                        else if ( !var10000.equals( var14 ) )
                        {
                            break label154;
                        }

                        if ( var15 )
                        {
                            var4 = ( context ) -> {
                                AnyValue value = context.getRefAt( offset );

                                try
                                {
                                    return ((VirtualNodeValue) value).id();
                                }
                                catch ( ClassCastException var3 )
                                {
                                    throw new ParameterWrongTypeException(
                                            (new StringBuilder( 55 )).append( "Expected to find a node at ref slot " ).append( offset ).append(
                                                    " but found " ).append( value ).append( " instead" ).toString() );
                                }
                            };
                            return var4;
                        }
                    }
                }
            }
        }

        if ( var6 != null )
        {
            Slot var20 = (Slot) var6._1();
            CypherType var21 = (CypherType) var6._2();
            boolean var22 = BoxesRunTime.unboxToBoolean( var6._3() );
            if ( var20 instanceof RefSlot )
            {
                RefSlot var23 = (RefSlot) var20;
                int offset = var23.offset();
                boolean var25 = var23.nullable();
                if ( !var25 )
                {
                    label145:
                    {
                        var53 = .MODULE$.CTRelationship();
                        if ( var53 == null )
                        {
                            if ( var21 != null )
                            {
                                break label145;
                            }
                        }
                        else if ( !var53.equals( var21 ) )
                        {
                            break label145;
                        }

                        if ( var22 )
                        {
                            var4 = ( context ) -> {
                                AnyValue value = context.getRefAt( offset );

                                try
                                {
                                    return ((VirtualRelationshipValue) value).id();
                                }
                                catch ( ClassCastException var3 )
                                {
                                    throw new ParameterWrongTypeException(
                                            (new StringBuilder( 63 )).append( "Expected to find a relationship at ref slot " ).append( offset ).append(
                                                    " but found " ).append( value ).append( " instead" ).toString() );
                                }
                            };
                            return var4;
                        }
                    }
                }
            }
        }

        if ( var6 != null )
        {
            Slot var27 = (Slot) var6._1();
            CypherType var28 = (CypherType) var6._2();
            boolean var29 = BoxesRunTime.unboxToBoolean( var6._3() );
            if ( var27 instanceof RefSlot )
            {
                RefSlot var30 = (RefSlot) var27;
                int offset = var30.offset();
                boolean var32 = var30.nullable();
                if ( var32 )
                {
                    label136:
                    {
                        var10000 = .MODULE$.CTNode();
                        if ( var10000 == null )
                        {
                            if ( var28 != null )
                            {
                                break label136;
                            }
                        }
                        else if ( !var10000.equals( var28 ) )
                        {
                            break label136;
                        }

                        if ( var29 )
                        {
                            var4 = ( context ) -> {
                                AnyValue value = context.getRefAt( offset );

                                try
                                {
                                    return value == Values.NO_VALUE ? MODULE$.PRIMITIVE_NULL() : ((VirtualNodeValue) value).id();
                                }
                                catch ( ClassCastException var3 )
                                {
                                    throw new ParameterWrongTypeException(
                                            (new StringBuilder( 55 )).append( "Expected to find a node at ref slot " ).append( offset ).append(
                                                    " but found " ).append( value ).append( " instead" ).toString() );
                                }
                            };
                            return var4;
                        }
                    }
                }
            }
        }

        if ( var6 != null )
        {
            Slot var34 = (Slot) var6._1();
            CypherType var35 = (CypherType) var6._2();
            boolean var36 = BoxesRunTime.unboxToBoolean( var6._3() );
            if ( var34 instanceof RefSlot )
            {
                RefSlot var37 = (RefSlot) var34;
                int offset = var37.offset();
                boolean var39 = var37.nullable();
                if ( var39 )
                {
                    label127:
                    {
                        var53 = .MODULE$.CTRelationship();
                        if ( var53 == null )
                        {
                            if ( var35 != null )
                            {
                                break label127;
                            }
                        }
                        else if ( !var53.equals( var35 ) )
                        {
                            break label127;
                        }

                        if ( var36 )
                        {
                            var4 = ( context ) -> {
                                AnyValue value = context.getRefAt( offset );

                                try
                                {
                                    return value == Values.NO_VALUE ? MODULE$.PRIMITIVE_NULL() : ((VirtualRelationshipValue) value).id();
                                }
                                catch ( ClassCastException var3 )
                                {
                                    throw new ParameterWrongTypeException(
                                            (new StringBuilder( 63 )).append( "Expected to find a relationship at ref slot " ).append( offset ).append(
                                                    " but found " ).append( value ).append( " instead" ).toString() );
                                }
                            };
                            return var4;
                        }
                    }
                }
            }
        }

        if ( var6 != null )
        {
            Slot var41 = (Slot) var6._1();
            CypherType var42 = (CypherType) var6._2();
            boolean var43 = BoxesRunTime.unboxToBoolean( var6._3() );
            if ( var41 instanceof RefSlot )
            {
                label119:
                {
                    RefSlot var44 = (RefSlot) var41;
                    int offset = var44.offset();
                    var10000 = .MODULE$.CTNode();
                    if ( var10000 == null )
                    {
                        if ( var42 != null )
                        {
                            break label119;
                        }
                    }
                    else if ( !var10000.equals( var42 ) )
                    {
                        break label119;
                    }

                    if ( !var43 )
                    {
                        var4 = ( context ) -> {
                            AnyValue var4 = context.getRefAt( offset );
                            long var2;
                            if ( var4 instanceof VirtualNodeValue )
                            {
                                VirtualNodeValue var5 = (VirtualNodeValue) var4;
                                var2 = var5.id();
                            }
                            else
                            {
                                var2 = MODULE$.PRIMITIVE_NULL();
                            }

                            return var2;
                        };
                        return var4;
                    }
                }
            }
        }

        if ( var6 == null )
        {
            throw new InternalException(
                    (new StringBuilder( 63 )).append( "Do not know how to make a primitive getter for slot " ).append( slot ).append( " with type " ).append(
                            returnType ).toString() );
        }
        else
        {
            Slot var47 = (Slot) var6._1();
            CypherType var48 = (CypherType) var6._2();
            boolean var49 = BoxesRunTime.unboxToBoolean( var6._3() );
            if ( !(var47 instanceof RefSlot) )
            {
                throw new InternalException( (new StringBuilder( 63 )).append( "Do not know how to make a primitive getter for slot " ).append( slot ).append(
                        " with type " ).append( returnType ).toString() );
            }
            else
            {
                RefSlot var50 = (RefSlot) var47;
                int offset = var50.offset();
                var53 = .MODULE$.CTRelationship();
                if ( var53 == null )
                {
                    if ( var48 != null )
                    {
                        throw new InternalException(
                                (new StringBuilder( 63 )).append( "Do not know how to make a primitive getter for slot " ).append( slot ).append(
                                        " with type " ).append( returnType ).toString() );
                    }
                }
                else if ( !var53.equals( var48 ) )
                {
                    throw new InternalException(
                            (new StringBuilder( 63 )).append( "Do not know how to make a primitive getter for slot " ).append( slot ).append(
                                    " with type " ).append( returnType ).toString() );
                }

                if ( var49 )
                {
                    throw new InternalException(
                            (new StringBuilder( 63 )).append( "Do not know how to make a primitive getter for slot " ).append( slot ).append(
                                    " with type " ).append( returnType ).toString() );
                }
                else
                {
                    var4 = ( context ) -> {
                        AnyValue var4 = context.getRefAt( offset );
                        long var2;
                        if ( var4 instanceof VirtualRelationshipValue )
                        {
                            VirtualRelationshipValue var5 = (VirtualRelationshipValue) var4;
                            var2 = var5.id();
                        }
                        else
                        {
                            var2 = MODULE$.PRIMITIVE_NULL();
                        }

                        return var2;
                    };
                    return var4;
                }
            }
        }
    }

    public boolean makeGetPrimitiveFromSlotFunctionFor$default$3()
    {
        return true;
    }

    public ToLongFunction<ExecutionContext> makeGetPrimitiveNodeFromSlotFunctionFor( final Slot slot, final boolean throwOnTypeError )
    {
        return this.makeGetPrimitiveFromSlotFunctionFor( slot,.MODULE$.CTNode(),throwOnTypeError);
    }

    public boolean makeGetPrimitiveNodeFromSlotFunctionFor$default$2()
    {
        return true;
    }

    public ToLongFunction<ExecutionContext> makeGetPrimitiveRelationshipFromSlotFunctionFor( final Slot slot, final boolean throwOfTypeError )
    {
        return this.makeGetPrimitiveFromSlotFunctionFor( slot,.MODULE$.CTRelationship(),throwOfTypeError);
    }

    public boolean makeGetPrimitiveRelationshipFromSlotFunctionFor$default$2()
    {
        return true;
    }

    public ToLongFunction<ExecutionContext> NO_ENTITY_FUNCTION()
    {
        return this.NO_ENTITY_FUNCTION;
    }

    public Function2<ExecutionContext,AnyValue,BoxedUnit> makeSetValueInSlotFunctionFor( final Slot slot )
    {
        Function2 var2;
        int offset;
        label106:
        {
            boolean var3 = false;
            LongSlot var4 = null;
            NodeType var10000;
            if ( slot instanceof LongSlot )
            {
                var3 = true;
                var4 = (LongSlot) slot;
                offset = var4.offset();
                boolean var7 = var4.nullable();
                CypherType var8 = var4.typ();
                if ( !var7 )
                {
                    var10000 = .MODULE$.CTNode();
                    if ( var10000 == null )
                    {
                        if ( var8 == null )
                        {
                            break label106;
                        }
                    }
                    else if ( var10000.equals( var8 ) )
                    {
                        break label106;
                    }
                }
            }

            int offset;
            label107:
            {
                RelationshipType var24;
                if ( var3 )
                {
                    offset = var4.offset();
                    boolean var11 = var4.nullable();
                    CypherType var12 = var4.typ();
                    if ( !var11 )
                    {
                        var24 = .MODULE$.CTRelationship();
                        if ( var24 == null )
                        {
                            if ( var12 == null )
                            {
                                break label107;
                            }
                        }
                        else if ( var24.equals( var12 ) )
                        {
                            break label107;
                        }
                    }
                }

                if ( var3 )
                {
                    int offset = var4.offset();
                    boolean var15 = var4.nullable();
                    CypherType var16 = var4.typ();
                    if ( var15 )
                    {
                        label108:
                        {
                            var10000 = .MODULE$.CTNode();
                            if ( var10000 == null )
                            {
                                if ( var16 != null )
                                {
                                    break label108;
                                }
                            }
                            else if ( !var10000.equals( var16 ) )
                            {
                                break label108;
                            }

                            var2 = ( context, value ) -> {
                                $anonfun$makeSetValueInSlotFunctionFor$3( offset, context, value );
                                return BoxedUnit.UNIT;
                            };
                            return var2;
                        }
                    }
                }

                int offset;
                label109:
                {
                    if ( var3 )
                    {
                        offset = var4.offset();
                        boolean var19 = var4.nullable();
                        CypherType var20 = var4.typ();
                        if ( var19 )
                        {
                            var24 = .MODULE$.CTRelationship();
                            if ( var24 == null )
                            {
                                if ( var20 == null )
                                {
                                    break label109;
                                }
                            }
                            else if ( var24.equals( var20 ) )
                            {
                                break label109;
                            }
                        }
                    }

                    if ( !(slot instanceof RefSlot) )
                    {
                        throw new InternalException( (new StringBuilder( 40 )).append( "Do not know how to make setter for slot " ).append( slot ).toString() );
                    }

                    RefSlot var22 = (RefSlot) slot;
                    int offset = var22.offset();
                    var2 = ( context, value ) -> {
                        $anonfun$makeSetValueInSlotFunctionFor$5( offset, context, value );
                        return BoxedUnit.UNIT;
                    };
                    return var2;
                }

                var2 = ( context, value ) -> {
                    $anonfun$makeSetValueInSlotFunctionFor$4( offset, context, value );
                    return BoxedUnit.UNIT;
                };
                return var2;
            }

            var2 = ( context, value ) -> {
                $anonfun$makeSetValueInSlotFunctionFor$2( offset, context, value );
                return BoxedUnit.UNIT;
            };
            return var2;
        }

        var2 = ( context, value ) -> {
            $anonfun$makeSetValueInSlotFunctionFor$1( offset, context, value );
            return BoxedUnit.UNIT;
        };
        return var2;
    }

    public Function3<ExecutionContext,Object,EntityById,BoxedUnit> makeSetPrimitiveInSlotFunctionFor( final Slot slot, final CypherType valueType )
    {
        Tuple2 var4 = new Tuple2( slot, valueType );
        Function3 var3;
        NodeType var10000;
        if ( var4 != null )
        {
            Slot var5 = (Slot) var4._1();
            CypherType var6 = (CypherType) var4._2();
            if ( var5 instanceof LongSlot )
            {
                label234:
                {
                    LongSlot var7 = (LongSlot) var5;
                    int offset = var7.offset();
                    boolean nullable = var7.nullable();
                    CypherType var10 = var7.typ();
                    var10000 = .MODULE$.CTNode();
                    if ( var10000 == null )
                    {
                        if ( var10 != null )
                        {
                            break label234;
                        }
                    }
                    else if ( !var10000.equals( var10 ) )
                    {
                        break label234;
                    }

                    var10000 = .MODULE$.CTNode();
                    if ( var10000 == null )
                    {
                        if ( var6 != null )
                        {
                            break label234;
                        }
                    }
                    else if ( !var10000.equals( var6 ) )
                    {
                        break label234;
                    }

                    var3 = org.neo4j.cypher.internal.v4_0.util.AssertionUtils..MODULE$.assertionsEnabled() && !nullable ? ( context, value, x$1 ) -> {
                    $anonfun$makeSetPrimitiveInSlotFunctionFor$1( offset, context, BoxesRunTime.unboxToLong( value ), x$1 );
                    return BoxedUnit.UNIT;
                } : ( context, value, x$2 ) -> {
                    $anonfun$makeSetPrimitiveInSlotFunctionFor$2( offset, context, BoxesRunTime.unboxToLong( value ), x$2 );
                    return BoxedUnit.UNIT;
                };
                    return var3;
                }
            }
        }

        int offset;
        boolean nullable;
        label235:
        {
            RelationshipType var49;
            if ( var4 != null )
            {
                Slot var13 = (Slot) var4._1();
                CypherType var14 = (CypherType) var4._2();
                if ( var13 instanceof LongSlot )
                {
                    label194:
                    {
                        LongSlot var15 = (LongSlot) var13;
                        offset = var15.offset();
                        nullable = var15.nullable();
                        CypherType var18 = var15.typ();
                        var49 = .MODULE$.CTRelationship();
                        if ( var49 == null )
                        {
                            if ( var18 != null )
                            {
                                break label194;
                            }
                        }
                        else if ( !var49.equals( var18 ) )
                        {
                            break label194;
                        }

                        var49 = .MODULE$.CTRelationship();
                        if ( var49 == null )
                        {
                            if ( var14 == null )
                            {
                                break label235;
                            }
                        }
                        else if ( var49.equals( var14 ) )
                        {
                            break label235;
                        }
                    }
                }
            }

            if ( var4 != null )
            {
                Slot var21 = (Slot) var4._1();
                CypherType var22 = (CypherType) var4._2();
                if ( var21 instanceof RefSlot )
                {
                    RefSlot var23 = (RefSlot) var21;
                    int offset = var23.offset();
                    boolean var25 = var23.nullable();
                    CypherType typ = var23.typ();
                    if ( !var25 )
                    {
                        label150:
                        {
                            var10000 = .MODULE$.CTNode();
                            if ( var10000 == null )
                            {
                                if ( var22 != null )
                                {
                                    break label150;
                                }
                            }
                            else if ( !var10000.equals( var22 ) )
                            {
                                break label150;
                            }

                            if ( typ.isAssignableFrom(.MODULE$.CTNode() )){
                            var3 = org.neo4j.cypher.internal.v4_0.util.AssertionUtils..MODULE$.assertionsEnabled() ? ( context, value, entityById ) -> {
                                $anonfun$makeSetPrimitiveInSlotFunctionFor$5( offset, context, BoxesRunTime.unboxToLong( value ), entityById );
                                return BoxedUnit.UNIT;
                            } : ( context, value, entityById ) -> {
                                $anonfun$makeSetPrimitiveInSlotFunctionFor$6( offset, context, BoxesRunTime.unboxToLong( value ), entityById );
                                return BoxedUnit.UNIT;
                            };
                            return var3;
                        }
                        }
                    }
                }
            }

            if ( var4 != null )
            {
                Slot var28 = (Slot) var4._1();
                CypherType var29 = (CypherType) var4._2();
                if ( var28 instanceof RefSlot )
                {
                    RefSlot var30 = (RefSlot) var28;
                    int offset = var30.offset();
                    boolean var32 = var30.nullable();
                    CypherType typ = var30.typ();
                    if ( !var32 )
                    {
                        label141:
                        {
                            var49 = .MODULE$.CTRelationship();
                            if ( var49 == null )
                            {
                                if ( var29 != null )
                                {
                                    break label141;
                                }
                            }
                            else if ( !var49.equals( var29 ) )
                            {
                                break label141;
                            }

                            if ( typ.isAssignableFrom(.MODULE$.CTRelationship() )){
                            var3 = org.neo4j.cypher.internal.v4_0.util.AssertionUtils..MODULE$.assertionsEnabled() ? ( context, value, entityById ) -> {
                                $anonfun$makeSetPrimitiveInSlotFunctionFor$7( offset, context, BoxesRunTime.unboxToLong( value ), entityById );
                                return BoxedUnit.UNIT;
                            } : ( context, value, entityById ) -> {
                                $anonfun$makeSetPrimitiveInSlotFunctionFor$8( offset, context, BoxesRunTime.unboxToLong( value ), entityById );
                                return BoxedUnit.UNIT;
                            };
                            return var3;
                        }
                        }
                    }
                }
            }

            if ( var4 != null )
            {
                Slot var35 = (Slot) var4._1();
                CypherType var36 = (CypherType) var4._2();
                if ( var35 instanceof RefSlot )
                {
                    RefSlot var37 = (RefSlot) var35;
                    int offset = var37.offset();
                    boolean var39 = var37.nullable();
                    CypherType typ = var37.typ();
                    if ( var39 )
                    {
                        label132:
                        {
                            var10000 = .MODULE$.CTNode();
                            if ( var10000 == null )
                            {
                                if ( var36 != null )
                                {
                                    break label132;
                                }
                            }
                            else if ( !var10000.equals( var36 ) )
                            {
                                break label132;
                            }

                            if ( typ.isAssignableFrom(.MODULE$.CTNode() )){
                            var3 = ( context, value, entityById ) -> {
                                $anonfun$makeSetPrimitiveInSlotFunctionFor$9( offset, context, BoxesRunTime.unboxToLong( value ), entityById );
                                return BoxedUnit.UNIT;
                            };
                            return var3;
                        }
                        }
                    }
                }
            }

            if ( var4 == null )
            {
                throw new InternalException(
                        (new StringBuilder( 53 )).append( "Do not know how to make a primitive " ).append( valueType ).append( " setter for slot " ).append(
                                slot ).toString() );
            }

            Slot var42 = (Slot) var4._1();
            CypherType var43 = (CypherType) var4._2();
            if ( !(var42 instanceof RefSlot) )
            {
                throw new InternalException(
                        (new StringBuilder( 53 )).append( "Do not know how to make a primitive " ).append( valueType ).append( " setter for slot " ).append(
                                slot ).toString() );
            }

            RefSlot var44 = (RefSlot) var42;
            int offset = var44.offset();
            boolean var46 = var44.nullable();
            CypherType typ = var44.typ();
            if ( !var46 )
            {
                throw new InternalException(
                        (new StringBuilder( 53 )).append( "Do not know how to make a primitive " ).append( valueType ).append( " setter for slot " ).append(
                                slot ).toString() );
            }

            var49 = .MODULE$.CTRelationship();
            if ( var49 == null )
            {
                if ( var43 != null )
                {
                    throw new InternalException(
                            (new StringBuilder( 53 )).append( "Do not know how to make a primitive " ).append( valueType ).append( " setter for slot " ).append(
                                    slot ).toString() );
                }
            }
            else if ( !var49.equals( var43 ) )
            {
                throw new InternalException(
                        (new StringBuilder( 53 )).append( "Do not know how to make a primitive " ).append( valueType ).append( " setter for slot " ).append(
                                slot ).toString() );
            }

            if ( !typ.isAssignableFrom(.MODULE$.CTRelationship() )){
            throw new InternalException(
                    (new StringBuilder( 53 )).append( "Do not know how to make a primitive " ).append( valueType ).append( " setter for slot " ).append(
                            slot ).toString() );
        }

            var3 = ( context, value, entityById ) -> {
                $anonfun$makeSetPrimitiveInSlotFunctionFor$10( offset, context, BoxesRunTime.unboxToLong( value ), entityById );
                return BoxedUnit.UNIT;
            };
            return var3;
        }

        var3 = org.neo4j.cypher.internal.v4_0.util.AssertionUtils..MODULE$.assertionsEnabled() && !nullable ? ( context, value, x$3 ) -> {
        $anonfun$makeSetPrimitiveInSlotFunctionFor$3( offset, context, BoxesRunTime.unboxToLong( value ), x$3 );
        return BoxedUnit.UNIT;
    } : ( context, value, x$4 ) -> {
        $anonfun$makeSetPrimitiveInSlotFunctionFor$4( offset, context, BoxesRunTime.unboxToLong( value ), x$4 );
        return BoxedUnit.UNIT;
    };
        return var3;
    }

    public Function3<ExecutionContext,Object,EntityById,BoxedUnit> makeSetPrimitiveNodeInSlotFunctionFor( final Slot slot )
    {
        return this.makeSetPrimitiveInSlotFunctionFor( slot,.MODULE$.CTNode());
    }

    public Function3<ExecutionContext,Object,EntityById,BoxedUnit> makeSetPrimitiveRelationshipInSlotFunctionFor( final Slot slot )
    {
        return this.makeSetPrimitiveInSlotFunctionFor( slot,.MODULE$.CTRelationship());
    }

    public void generateSlotAccessorFunctions( final SlotConfiguration slots )
    {
        slots.foreachSlot( ( x0$1 ) -> {
            $anonfun$generateSlotAccessorFunctions$1( slots, x0$1 );
            return BoxedUnit.UNIT;
        }, ( notDoingForCachedNodePropertiesYet ) -> {
            $anonfun$generateSlotAccessorFunctions$2( notDoingForCachedNodePropertiesYet );
            return BoxedUnit.UNIT;
        } );
    }
}
