package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.logical.plans.ProduceResult;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;

public final class ProduceResultOutput$ extends AbstractFunction1<ProduceResult,ProduceResultOutput> implements Serializable
{
    public static ProduceResultOutput$ MODULE$;

    static
    {
        new ProduceResultOutput$();
    }

    private ProduceResultOutput$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "ProduceResultOutput";
    }

    public ProduceResultOutput apply( final ProduceResult plan )
    {
        return new ProduceResultOutput( plan );
    }

    public Option<ProduceResult> unapply( final ProduceResultOutput x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( x$0.plan() ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
