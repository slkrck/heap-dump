package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.runtime.EntityById;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.v4_0.expressions.ASTCachedProperty;
import org.neo4j.cypher.internal.v4_0.util.ASTNode;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id;
import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import org.neo4j.exceptions.InternalException;
import org.neo4j.values.AnyValue;
import scala.Function1;
import scala.Function2;
import scala.Function3;
import scala.MatchError;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.Tuple3;
import scala.Tuple2.mcII.sp;
import scala.collection.GenTraversableOnce;
import scala.collection.Iterable;
import scala.collection.Iterator;
import scala.collection.MapLike;
import scala.collection.Seq;
import scala.collection.SeqLike;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.immutable.IndexedSeq;
import scala.collection.mutable.HashMap;
import scala.collection.mutable.Map;
import scala.collection.mutable.MultiMap;
import scala.collection.mutable.Set;
import scala.math.Ordering;
import scala.math.PartialOrdering;
import scala.math.Ordering.Ops;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.ObjectRef;
import scala.runtime.Statics;

@JavaDocToJava
public class SlotConfiguration
{
    private final Map<String,Slot> slots;
    private final Map<ASTCachedProperty,RefSlot> cachedProperties;
    private final Map<Id,Object> applyPlans;
    private final Set<String> org$neo4j$cypher$internal$physicalplanning$SlotConfiguration$$aliases;
    private final HashMap<Slot,Set<String>> slotAliases;
    private final Map<String,Function1<ExecutionContext,AnyValue>> getters;
    private final Map<String,Function2<ExecutionContext,AnyValue,BoxedUnit>> setters;
    private final Map<String,Function3<ExecutionContext,Object,EntityById,BoxedUnit>> primitiveNodeSetters;
    private final Map<String,Function3<ExecutionContext,Object,EntityById,BoxedUnit>> primitiveRelationshipSetters;
    private volatile SlotConfiguration.SlotWithAliasesOrdering$ SlotWithAliasesOrdering$module;
    private volatile SlotConfiguration.SlotOrdering$ SlotOrdering$module;
    private int numberOfLongs;
    private int numberOfReferences;

    public SlotConfiguration( final Map<String,Slot> slots, final Map<ASTCachedProperty,RefSlot> cachedProperties, final Map<Id,Object> applyPlans,
            final int numberOfLongs, final int numberOfReferences )
    {
        this.slots = slots;
        this.cachedProperties = cachedProperties;
        this.applyPlans = applyPlans;
        this.numberOfLongs = numberOfLongs;
        this.numberOfReferences = numberOfReferences;
        super();
        this.org$neo4j$cypher$internal$physicalplanning$SlotConfiguration$$aliases = (Set) scala.collection.mutable.Set..
        MODULE$.apply( scala.collection.immutable.Nil..MODULE$);
        this.slotAliases = new MultiMap<Slot,String>( (SlotConfiguration) null )
        {
            public
            {
                MultiMap.$init$( this );
            }

            public Set<String> makeSet()
            {
                return MultiMap.makeSet$( this );
            }

            public MultiMap addBinding( final Object key, final Object value )
            {
                return MultiMap.addBinding$( this, key, value );
            }

            public MultiMap removeBinding( final Object key, final Object value )
            {
                return MultiMap.removeBinding$( this, key, value );
            }

            public boolean entryExists( final Object key, final Function1 p )
            {
                return MultiMap.entryExists$( this, key, p );
            }
        };
        this.getters = new HashMap();
        this.setters = new HashMap();
        this.primitiveNodeSetters = new HashMap();
        this.primitiveRelationshipSetters = new HashMap();
    }

    public static boolean isRefSlotAndNotAlias( final SlotConfiguration slots, final String k )
    {
        return SlotConfiguration$.MODULE$.isRefSlotAndNotAlias( var0, var1 );
    }

    public static boolean isLongSlot( final Slot slot )
    {
        return SlotConfiguration$.MODULE$.isLongSlot( var0 );
    }

    public static SlotConfiguration empty()
    {
        return SlotConfiguration$.MODULE$.empty();
    }

    private static final void _onVariable$1( final Tuple2 tuple, final Function2 onVariable$1, final ObjectRef sortedRefs$1, final ObjectRef sortedLongs$1 )
    {
        if ( tuple != null )
        {
            String variable = (String) tuple._1();
            Slot slot = (Slot) tuple._2();
            Tuple2 var4 = new Tuple2( variable, slot );
            String variable = (String) var4._1();
            Slot slot = (Slot) var4._2();
            if ( slot.isLongSlot() )
            {
                onVariable$1.apply( variable, slot );
                sortedLongs$1.elem = (Seq) ((Seq) sortedLongs$1.elem).tail();
            }
            else
            {
                onVariable$1.apply( variable, slot );
                sortedRefs$1.elem = (Seq) ((Seq) sortedRefs$1.elem).tail();
            }
        }
        else
        {
            throw new MatchError( tuple );
        }
    }

    private static final void _onCached$1( final Tuple2 tuple, final Function1 onCachedProperty$1, final ObjectRef sortedCached$1 )
    {
        if ( tuple != null )
        {
            ASTCachedProperty cached = (ASTCachedProperty) tuple._1();
            onCachedProperty$1.apply( cached );
            sortedCached$1.elem = (Seq) ((Seq) sortedCached$1.elem).tail();
        }
        else
        {
            throw new MatchError( tuple );
        }
    }

    private static final void _onApplyPlanId$1( final Tuple2 tuple, final Function1 onApplyPlan$1, final ObjectRef sortedApplyPlanIds$1 )
    {
        if ( tuple != null )
        {
            int id = ((Id) tuple._1()).x();
            onApplyPlan$1.apply( new Id( id ) );
            sortedApplyPlanIds$1.elem = (Seq) ((Seq) sortedApplyPlanIds$1.elem).tail();
        }
        else
        {
            throw new MatchError( tuple );
        }
    }

    public SlotConfiguration.SlotWithAliasesOrdering$ SlotWithAliasesOrdering()
    {
        if ( this.SlotWithAliasesOrdering$module == null )
        {
            this.SlotWithAliasesOrdering$lzycompute$1();
        }

        return this.SlotWithAliasesOrdering$module;
    }

    public SlotConfiguration.SlotOrdering$ SlotOrdering()
    {
        if ( this.SlotOrdering$module == null )
        {
            this.SlotOrdering$lzycompute$1();
        }

        return this.SlotOrdering$module;
    }

    private Map<String,Slot> slots()
    {
        return this.slots;
    }

    private Map<ASTCachedProperty,RefSlot> cachedProperties()
    {
        return this.cachedProperties;
    }

    private Map<Id,Object> applyPlans()
    {
        return this.applyPlans;
    }

    public int numberOfLongs()
    {
        return this.numberOfLongs;
    }

    public void numberOfLongs_$eq( final int x$1 )
    {
        this.numberOfLongs = x$1;
    }

    public int numberOfReferences()
    {
        return this.numberOfReferences;
    }

    public void numberOfReferences_$eq( final int x$1 )
    {
        this.numberOfReferences = x$1;
    }

    public Set<String> org$neo4j$cypher$internal$physicalplanning$SlotConfiguration$$aliases()
    {
        return this.org$neo4j$cypher$internal$physicalplanning$SlotConfiguration$$aliases;
    }

    private HashMap<Slot,Set<String>> slotAliases()
    {
        return this.slotAliases;
    }

    private Map<String,Function1<ExecutionContext,AnyValue>> getters()
    {
        return this.getters;
    }

    private Map<String,Function2<ExecutionContext,AnyValue,BoxedUnit>> setters()
    {
        return this.setters;
    }

    private Map<String,Function3<ExecutionContext,Object,EntityById,BoxedUnit>> primitiveNodeSetters()
    {
        return this.primitiveNodeSetters;
    }

    private Map<String,Function3<ExecutionContext,Object,EntityById,BoxedUnit>> primitiveRelationshipSetters()
    {
        return this.primitiveRelationshipSetters;
    }

    public SlotConfiguration.Size size()
    {
        return new SlotConfiguration.Size( this.numberOfLongs(), this.numberOfReferences() );
    }

    public SlotConfiguration addAlias( final String newKey, final String existingKey )
    {
        Slot slot = (Slot) this.slots().getOrElse( existingKey, () -> {
            throw new SlotAllocationFailed(
                    (new StringBuilder( 50 )).append( "Tried to alias non-existing slot '" ).append( existingKey ).append( "'  with alias '" ).append(
                            newKey ).append( "'" ).toString() );
        } );
        this.slots().put( newKey, slot );
        this.org$neo4j$cypher$internal$physicalplanning$SlotConfiguration$$aliases().add( newKey );
        ((MultiMap) this.slotAliases()).addBinding( slot, newKey );
        return this;
    }

    public boolean isAlias( final String key )
    {
        return this.org$neo4j$cypher$internal$physicalplanning$SlotConfiguration$$aliases().contains( key );
    }

    public Slot apply( final String key )
    {
        return (Slot) this.slots().apply( key );
    }

    public Option<String> nameOfLongSlot( final int offset )
    {
        return this.slots().collectFirst( new Serializable( this, offset )
        {
            public static final long serialVersionUID = 0L;
            private final int offset$1;

            public
            {
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                    this.offset$1 = offset$1;
                }
            }

            public final <A1 extends Tuple2<String,Slot>, B1> B1 applyOrElse( final A1 x1, final Function1<A1,B1> default )
            {
                Object var3;
                if ( x1 != null )
                {
                    String name = (String) x1._1();
                    Slot var6 = (Slot) x1._2();
                    if ( var6 instanceof LongSlot )
                    {
                        LongSlot var7 = (LongSlot) var6;
                        int o = var7.offset();
                        if ( o == this.offset$1 && !this.$outer.org$neo4j$cypher$internal$physicalplanning$SlotConfiguration$$aliases().apply( name ) )
                        {
                            var3 = name;
                            return var3;
                        }
                    }
                }

                var3 = var2.apply( x1 );
                return var3;
            }

            public final boolean isDefinedAt( final Tuple2<String,Slot> x1 )
            {
                boolean var2;
                if ( x1 != null )
                {
                    String name = (String) x1._1();
                    Slot var5 = (Slot) x1._2();
                    if ( var5 instanceof LongSlot )
                    {
                        LongSlot var6 = (LongSlot) var5;
                        int o = var6.offset();
                        if ( o == this.offset$1 && !this.$outer.org$neo4j$cypher$internal$physicalplanning$SlotConfiguration$$aliases().apply( name ) )
                        {
                            var2 = true;
                            return var2;
                        }
                    }
                }

                var2 = false;
                return var2;
            }
        } );
    }

    public <U> Iterable<Slot> filterSlots( final Function1<Tuple2<String,Slot>,Object> onVariable,
            final Function1<Tuple2<ASTCachedProperty,RefSlot>,Object> onCachedProperty )
    {
        return ((MapLike) ((TraversableLike) this.slots().filter( onVariable )).$plus$plus(
                (GenTraversableOnce) this.cachedProperties().filter( onCachedProperty ), scala.collection.mutable.Map..MODULE$.canBuildFrom())).values();
    }

    public Option<Slot> get( final String key )
    {
        return this.slots().get( key );
    }

    public void add( final String key, final Slot slot )
    {
        BoxedUnit var3;
        if ( slot instanceof LongSlot )
        {
            LongSlot var5 = (LongSlot) slot;
            boolean nullable = var5.nullable();
            CypherType typ = var5.typ();
            this.newLong( key, nullable, typ );
            var3 = BoxedUnit.UNIT;
        }
        else
        {
            if ( !(slot instanceof RefSlot) )
            {
                throw new MatchError( slot );
            }

            RefSlot var8 = (RefSlot) slot;
            boolean nullable = var8.nullable();
            CypherType typ = var8.typ();
            this.newReference( key, nullable, typ );
            var3 = BoxedUnit.UNIT;
        }
    }

    public SlotConfiguration copy()
    {
        SlotConfiguration newPipeline =
                new SlotConfiguration( this.slots().clone(), this.cachedProperties().clone(), this.applyPlans().clone(), this.numberOfLongs(),
                        this.numberOfReferences() );
        newPipeline.org$neo4j$cypher$internal$physicalplanning$SlotConfiguration$$aliases().$plus$plus$eq(
                this.org$neo4j$cypher$internal$physicalplanning$SlotConfiguration$$aliases() );
        newPipeline.slotAliases().$plus$plus$eq( this.slotAliases() );
        return newPipeline;
    }

    public SlotConfiguration emptyUnderSameApply()
    {
        return new SlotConfiguration( scala.collection.mutable.Map..MODULE$.empty(),scala.collection.mutable.Map..
        MODULE$.empty(), this.applyPlans().clone(), 0, 0);
    }

    private void replaceExistingSlot( final String key, final Slot existingSlot, final Slot modifiedSlot )
    {
        Set existingAliases = (Set) this.slotAliases().getOrElse( existingSlot, () -> {
            throw new InternalError(
                    (new StringBuilder( 44 )).append( "Slot allocation failure - missing slot " ).append( existingSlot ).append( " for " ).append(
                            key ).toString() );
        } );
        scala.Predef..MODULE$. assert (existingAliases.contains( key ));
        this.slotAliases().put( modifiedSlot, existingAliases );
        existingAliases.foreach( ( alias ) -> {
            return this.slots().put( alias, modifiedSlot );
        } );
        this.slotAliases().remove( existingSlot );
    }

    private void unifyTypeAndNullability( final String key, final Slot existingSlot, final Slot newSlot )
    {
        boolean updateNullable;
        boolean var44;
        label132:
        {
            label131:
            {
                updateNullable = !existingSlot.nullable() && newSlot.nullable();
                CypherType var10000 = existingSlot.typ();
                CypherType var7 = newSlot.typ();
                if ( var10000 == null )
                {
                    if ( var7 == null )
                    {
                        break label131;
                    }
                }
                else if ( var10000.equals( var7 ) )
                {
                    break label131;
                }

                if ( !existingSlot.typ().isAssignableFrom( newSlot.typ() ) )
                {
                    var44 = true;
                    break label132;
                }
            }

            var44 = false;
        }

        boolean updateTyp = var44;
        scala.Predef..MODULE$. assert (!updateTyp || newSlot.typ().isAssignableFrom( existingSlot.typ() ));
        if ( updateNullable || updateTyp )
        {
            Object var4;
            label141:
            {
                Tuple3 var9 = new Tuple3( existingSlot, BoxesRunTime.boxToBoolean( updateNullable ), BoxesRunTime.boxToBoolean( updateTyp ) );
                if ( var9 != null )
                {
                    Slot var10 = (Slot) var9._1();
                    boolean var11 = BoxesRunTime.unboxToBoolean( var9._2() );
                    boolean var12 = BoxesRunTime.unboxToBoolean( var9._3() );
                    if ( var10 instanceof LongSlot )
                    {
                        LongSlot var13 = (LongSlot) var10;
                        int offset = var13.offset();
                        if ( var11 && var12 )
                        {
                            var4 = new LongSlot( offset, true, newSlot.typ() );
                            break label141;
                        }
                    }
                }

                if ( var9 != null )
                {
                    Slot var15 = (Slot) var9._1();
                    boolean var16 = BoxesRunTime.unboxToBoolean( var9._2() );
                    boolean var17 = BoxesRunTime.unboxToBoolean( var9._3() );
                    if ( var15 instanceof RefSlot )
                    {
                        RefSlot var18 = (RefSlot) var15;
                        int offset = var18.offset();
                        if ( var16 && var17 )
                        {
                            var4 = new RefSlot( offset, true, newSlot.typ() );
                            break label141;
                        }
                    }
                }

                if ( var9 != null )
                {
                    Slot var20 = (Slot) var9._1();
                    boolean var21 = BoxesRunTime.unboxToBoolean( var9._2() );
                    boolean var22 = BoxesRunTime.unboxToBoolean( var9._3() );
                    if ( var20 instanceof LongSlot )
                    {
                        LongSlot var23 = (LongSlot) var20;
                        int offset = var23.offset();
                        CypherType typ = var23.typ();
                        if ( var21 && !var22 )
                        {
                            var4 = new LongSlot( offset, true, typ );
                            break label141;
                        }
                    }
                }

                if ( var9 != null )
                {
                    Slot var26 = (Slot) var9._1();
                    boolean var27 = BoxesRunTime.unboxToBoolean( var9._2() );
                    boolean var28 = BoxesRunTime.unboxToBoolean( var9._3() );
                    if ( var26 instanceof RefSlot )
                    {
                        RefSlot var29 = (RefSlot) var26;
                        int offset = var29.offset();
                        CypherType typ = var29.typ();
                        if ( var27 && !var28 )
                        {
                            var4 = new RefSlot( offset, true, typ );
                            break label141;
                        }
                    }
                }

                if ( var9 != null )
                {
                    Slot var32 = (Slot) var9._1();
                    boolean var33 = BoxesRunTime.unboxToBoolean( var9._2() );
                    boolean var34 = BoxesRunTime.unboxToBoolean( var9._3() );
                    if ( var32 instanceof LongSlot )
                    {
                        LongSlot var35 = (LongSlot) var32;
                        int offset = var35.offset();
                        boolean nullable = var35.nullable();
                        if ( !var33 && var34 )
                        {
                            var4 = new LongSlot( offset, nullable, newSlot.typ() );
                            break label141;
                        }
                    }
                }

                if ( var9 == null )
                {
                    throw new InternalException( (new StringBuilder( 30 )).append( "Unxpected slot configuration: " ).append( var9 ).toString() );
                }

                Slot var38 = (Slot) var9._1();
                boolean var39 = BoxesRunTime.unboxToBoolean( var9._2() );
                boolean var40 = BoxesRunTime.unboxToBoolean( var9._3() );
                if ( !(var38 instanceof RefSlot) )
                {
                    throw new InternalException( (new StringBuilder( 30 )).append( "Unxpected slot configuration: " ).append( var9 ).toString() );
                }

                RefSlot var41 = (RefSlot) var38;
                int offset = var41.offset();
                boolean nullable = var41.nullable();
                if ( var39 || !var40 )
                {
                    throw new InternalException( (new StringBuilder( 30 )).append( "Unxpected slot configuration: " ).append( var9 ).toString() );
                }

                var4 = new RefSlot( offset, nullable, newSlot.typ() );
            }

            this.replaceExistingSlot( key, existingSlot, (Slot) var4 );
        }
    }

    public SlotConfiguration newLong( final String key, final boolean nullable, final CypherType typ )
    {
        LongSlot slot = new LongSlot( this.numberOfLongs(), nullable, typ );
        Option var6 = this.slots().get( key );
        BoxedUnit var4;
        if ( var6 instanceof Some )
        {
            Some var7 = (Some) var6;
            Slot existingSlot = (Slot) var7.value();
            if ( !existingSlot.isTypeCompatibleWith( slot ) )
            {
                throw new InternalException(
                        (new StringBuilder( 58 )).append( "Tried overwriting already taken variable name " ).append( key ).append( " as " ).append(
                                slot ).append( " (was: " ).append( existingSlot ).append( ")" ).toString() );
            }

            this.unifyTypeAndNullability( key, existingSlot, slot );
            var4 = BoxedUnit.UNIT;
        }
        else
        {
            if ( !scala.None..MODULE$.equals( var6 )){
            throw new MatchError( var6 );
        }

            this.slots().put( key, slot );
            ((MultiMap) this.slotAliases()).addBinding( slot, key );
            this.numberOfLongs_$eq( this.numberOfLongs() + 1 );
            var4 = BoxedUnit.UNIT;
        }

        return this;
    }

    public SlotConfiguration newArgument( final int applyPlanId )
    {
        if ( this.applyPlans().contains( new Id( applyPlanId ) ) )
        {
            throw new IllegalStateException(
                    (new StringBuilder( 60 )).append( "Should only add argument once per plan, got plan with " ).append( new Id( applyPlanId ) ).append(
                            " twice" ).toString() );
        }
        else
        {
            if ( applyPlanId != org.neo4j.cypher.internal.v4_0.util.attribution.Id..MODULE$.INVALID_ID()){
            this.applyPlans().put( new Id( applyPlanId ), BoxesRunTime.boxToInteger( this.numberOfLongs() ) );
            this.numberOfLongs_$eq( this.numberOfLongs() + 1 );
        }

            return this;
        }
    }

    public SlotConfiguration newReference( final String key, final boolean nullable, final CypherType typ )
    {
        RefSlot slot = new RefSlot( this.numberOfReferences(), nullable, typ );
        Option var6 = this.slots().get( key );
        BoxedUnit var4;
        if ( var6 instanceof Some )
        {
            Some var7 = (Some) var6;
            Slot existingSlot = (Slot) var7.value();
            if ( !existingSlot.isTypeCompatibleWith( slot ) )
            {
                throw new InternalException(
                        (new StringBuilder( 58 )).append( "Tried overwriting already taken variable name " ).append( key ).append( " as " ).append(
                                slot ).append( " (was: " ).append( existingSlot ).append( ")" ).toString() );
            }

            this.unifyTypeAndNullability( key, existingSlot, slot );
            var4 = BoxedUnit.UNIT;
        }
        else
        {
            if ( !scala.None..MODULE$.equals( var6 )){
            throw new MatchError( var6 );
        }

            this.slots().put( key, slot );
            ((MultiMap) this.slotAliases()).addBinding( slot, key );
            this.numberOfReferences_$eq( this.numberOfReferences() + 1 );
            var4 = BoxedUnit.UNIT;
        }

        return this;
    }

    public SlotConfiguration newCachedProperty( final ASTCachedProperty key, final boolean shouldDuplicate )
    {
        Option var4 = this.cachedProperties().get( key );
        if ( var4 instanceof Some )
        {
            BoxedUnit var10000;
            if ( shouldDuplicate )
            {
                this.numberOfReferences_$eq( this.numberOfReferences() + 1 );
                var10000 = BoxedUnit.UNIT;
            }
            else
            {
                var10000 = BoxedUnit.UNIT;
            }
        }
        else
        {
            if ( !scala.None..MODULE$.equals( var4 )){
            throw new MatchError( var4 );
        }

            this.cachedProperties().put( key, new RefSlot( this.numberOfReferences(), false, org.neo4j.cypher.internal.v4_0.util.symbols.package..
            MODULE$.CTAny()));
            this.numberOfReferences_$eq( this.numberOfReferences() + 1 );
            BoxedUnit var3 = BoxedUnit.UNIT;
        }

        return this;
    }

    public boolean newCachedProperty$default$2()
    {
        return false;
    }

    public int getReferenceOffsetFor( final String name )
    {
        boolean var3 = false;
        Some var4 = null;
        Option var5 = this.slots().get( name );
        if ( var5 instanceof Some )
        {
            var3 = true;
            var4 = (Some) var5;
            Slot s = (Slot) var4.value();
            if ( s instanceof RefSlot )
            {
                RefSlot var7 = (RefSlot) s;
                int var2 = var7.offset();
                return var2;
            }
        }

        if ( var3 )
        {
            Slot s = (Slot) var4.value();
            throw new InternalException(
                    (new StringBuilder( 54 )).append( "Uh oh... There was no reference slot for `" ).append( name ).append( "`. It was a " ).append(
                            s ).toString() );
        }
        else
        {
            throw new InternalException( (new StringBuilder( 33 )).append( "Uh oh... There was no slot for `" ).append( name ).append( "`" ).toString() );
        }
    }

    public int getLongOffsetFor( final String name )
    {
        boolean var3 = false;
        Some var4 = null;
        Option var5 = this.slots().get( name );
        if ( var5 instanceof Some )
        {
            var3 = true;
            var4 = (Some) var5;
            Slot s = (Slot) var4.value();
            if ( s instanceof LongSlot )
            {
                LongSlot var7 = (LongSlot) s;
                int var2 = var7.offset();
                return var2;
            }
        }

        if ( var3 )
        {
            Slot s = (Slot) var4.value();
            throw new InternalException(
                    (new StringBuilder( 49 )).append( "Uh oh... There was no long slot for `" ).append( name ).append( "`. It was a " ).append(
                            s ).toString() );
        }
        else
        {
            throw new InternalException( (new StringBuilder( 33 )).append( "Uh oh... There was no slot for `" ).append( name ).append( "`" ).toString() );
        }
    }

    public Slot getLongSlotFor( final String name )
    {
        boolean var3 = false;
        Some var4 = null;
        Option var5 = this.slots().get( name );
        if ( var5 instanceof Some )
        {
            var3 = true;
            var4 = (Some) var5;
            Slot s = (Slot) var4.value();
            if ( s instanceof LongSlot )
            {
                LongSlot var7 = (LongSlot) s;
                return var7;
            }
        }

        if ( var3 )
        {
            Slot s = (Slot) var4.value();
            throw new InternalException(
                    (new StringBuilder( 49 )).append( "Uh oh... There was no long slot for `" ).append( name ).append( "`. It was a " ).append(
                            s ).toString() );
        }
        else
        {
            throw new InternalException( (new StringBuilder( 33 )).append( "Uh oh... There was no slot for `" ).append( name ).append( "`" ).toString() );
        }
    }

    public int getArgumentLongOffsetFor( final int applyPlanId )
    {
        return applyPlanId == org.neo4j.cypher.internal.v4_0.util.attribution.Id..
        MODULE$.INVALID_ID() ? TopLevelArgument$.MODULE$.SLOT_OFFSET() : BoxesRunTime.unboxToInt( this.applyPlans().getOrElse( new Id( applyPlanId ), () -> {
            throw new InternalException(
                    (new StringBuilder( 41 )).append( "No argument slot allocated for plan with " ).append( new Id( applyPlanId ) ).toString() );
        } ) );
    }

    public int getCachedPropertyOffsetFor( final ASTCachedProperty key )
    {
        return ((RefSlot) this.cachedProperties().apply( key )).offset();
    }

    public void updateAccessorFunctions( final String key, final Function1<ExecutionContext,AnyValue> getter,
            final Function2<ExecutionContext,AnyValue,BoxedUnit> setter,
            final Option<Function3<ExecutionContext,Object,EntityById,BoxedUnit>> primitiveNodeSetter,
            final Option<Function3<ExecutionContext,Object,EntityById,BoxedUnit>> primitiveRelationshipSetter )
    {
        this.getters().$plus$eq( scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( key ), getter));
        this.setters().$plus$eq( scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc( key ), setter));
        primitiveNodeSetter.map( ( x$2 ) -> {
            return (Map) this.primitiveNodeSetters().$plus$eq( scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc(
                    key ),x$2));
        } ); primitiveRelationshipSetter.map( ( x$3 ) -> {
        return (Map) this.primitiveRelationshipSetters().$plus$eq( scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc(
                key ),x$3));
    } );
    }

    public Function1<ExecutionContext,AnyValue> getter( final String key )
    {
        return (Function1) this.getters().apply( key );
    }

    public Function2<ExecutionContext,AnyValue,BoxedUnit> setter( final String key )
    {
        return (Function2) this.setters().apply( key );
    }

    public Option<Function1<ExecutionContext,AnyValue>> maybeGetter( final String key )
    {
        return this.getters().get( key );
    }

    public Option<Function2<ExecutionContext,AnyValue,BoxedUnit>> maybeSetter( final String key )
    {
        return this.setters().get( key );
    }

    public Option<Function3<ExecutionContext,Object,EntityById,BoxedUnit>> maybePrimitiveNodeSetter( final String key )
    {
        return this.primitiveNodeSetters().get( key );
    }

    public Option<Function3<ExecutionContext,Object,EntityById,BoxedUnit>> maybePrimitiveRelationshipSetter( final String key )
    {
        return this.primitiveRelationshipSetters().get( key );
    }

    public <U> void foreachSlot( final Function1<Tuple2<String,Slot>,U> onVariable,
            final Function1<Tuple2<ASTCachedProperty,RefSlot>,BoxedUnit> onCachedProperty )
    {
        this.slots().foreach( onVariable );
        this.cachedProperties().foreach( onCachedProperty );
    }

    public void foreachSlotOrdered( final Function2<String,Slot,BoxedUnit> onVariable, final Function1<ASTCachedProperty,BoxedUnit> onCachedProperty,
            final Function1<Id,BoxedUnit> onApplyPlan, final SlotConfiguration.Size skipFirst )
    {
        Tuple2 var9 = this.slots().toSeq().partition( ( x$5 ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$foreachSlotOrdered$1( x$5 ) );
        } );
        if ( var9 == null )
        {
            throw new MatchError( var9 );
        }
        else
        {
            Seq longs = (Seq) var9._1();
            Seq refs = (Seq) var9._2();
            Tuple2 var7 = new Tuple2( longs, refs );
            Seq longs = (Seq) var7._1();
            Seq refs = (Seq) var7._2();
            ObjectRef sortedRefs = ObjectRef.create( (Seq) ((SeqLike) refs.filter( ( x$7 ) -> {
                return BoxesRunTime.boxToBoolean( $anonfun$foreachSlotOrdered$2( skipFirst, x$7 ) );
            } )).sortBy( ( x$8 ) -> {
                return BoxesRunTime.boxToInteger( $anonfun$foreachSlotOrdered$3( x$8 ) );
            }, scala.math.Ordering.Int..MODULE$ ));
            ObjectRef sortedLongs = ObjectRef.create( (Seq) ((SeqLike) longs.filter( ( x$9 ) -> {
                return BoxesRunTime.boxToBoolean( $anonfun$foreachSlotOrdered$4( skipFirst, x$9 ) );
            } )).sortBy( ( x$10 ) -> {
                return BoxesRunTime.boxToInteger( $anonfun$foreachSlotOrdered$5( x$10 ) );
            }, scala.math.Ordering.Int..MODULE$ ));
            ObjectRef sortedCached = ObjectRef.create( (Seq) ((SeqLike) this.cachedProperties().toSeq().filter( ( x$11 ) -> {
                return BoxesRunTime.boxToBoolean( $anonfun$foreachSlotOrdered$6( skipFirst, x$11 ) );
            } )).sortBy( ( x$12 ) -> {
                return BoxesRunTime.boxToInteger( $anonfun$foreachSlotOrdered$7( x$12 ) );
            }, scala.math.Ordering.Int..MODULE$ ));
            ObjectRef sortedApplyPlanIds = ObjectRef.create( (Seq) ((SeqLike) this.applyPlans().toSeq().filter( ( x$13 ) -> {
                return BoxesRunTime.boxToBoolean( $anonfun$foreachSlotOrdered$8( skipFirst, x$13 ) );
            } )).sortBy( ( x$14 ) -> {
                return BoxesRunTime.boxToInteger( $anonfun$foreachSlotOrdered$9( x$14 ) );
            }, scala.math.Ordering.Int..MODULE$ ));

            Tuple2 var18;
            while ( true )
            {
                BoxedUnit var6;
                while ( true )
                {
                    while ( true )
                    {
                        while ( true )
                        {
                            if ( !((Seq) sortedRefs.elem).nonEmpty() && !((Seq) sortedCached.elem).nonEmpty() )
                            {
                                Tuple2 var39;
                                while ( true )
                                {
                                    BoxedUnit var5;
                                    while ( true )
                                    {
                                        while ( true )
                                        {
                                            while ( true )
                                            {
                                                if ( !((Seq) sortedLongs.elem).nonEmpty() && !((Seq) sortedApplyPlanIds.elem).nonEmpty() )
                                                {
                                                    return;
                                                }

                                                var39 = new Tuple2( ((Seq) sortedLongs.elem).headOption(), ((Seq) sortedApplyPlanIds.elem).headOption() );
                                                if ( var39 == null )
                                                {
                                                    break;
                                                }

                                                Option var40 = (Option) var39._1();
                                                Option var41 = (Option) var39._2();
                                                if ( !(var40 instanceof Some) )
                                                {
                                                    break;
                                                }

                                                Some var42 = (Some) var40;
                                                Tuple2 var43 = (Tuple2) var42.value();
                                                if ( !scala.None..MODULE$.equals( var41 )){
                                                break;
                                            }

                                                _onVariable$1( var43, onVariable, sortedRefs, sortedLongs );
                                                var5 = BoxedUnit.UNIT;
                                            }

                                            if ( var39 == null )
                                            {
                                                break;
                                            }

                                            Option var44 = (Option) var39._1();
                                            Option var45 = (Option) var39._2();
                                            if ( !scala.None..MODULE$.equals( var44 ) || !(var45 instanceof Some)){
                                            break;
                                        }

                                            Some var46 = (Some) var45;
                                            Tuple2 applyPlanId = (Tuple2) var46.value();
                                            _onApplyPlanId$1( applyPlanId, onApplyPlan, sortedApplyPlanIds );
                                            var5 = BoxedUnit.UNIT;
                                        }

                                        if ( var39 == null )
                                        {
                                            break;
                                        }

                                        Option var48 = (Option) var39._1();
                                        Option var49 = (Option) var39._2();
                                        if ( !(var48 instanceof Some) )
                                        {
                                            break;
                                        }

                                        Some var50 = (Some) var48;
                                        Tuple2 var51 = (Tuple2) var50.value();
                                        if ( !(var49 instanceof Some) )
                                        {
                                            break;
                                        }

                                        Some var52 = (Some) var49;
                                        Tuple2 applyPlanId = (Tuple2) var52.value();
                                        if ( ((Slot) var51._2()).offset() >= applyPlanId._2$mcI$sp() )
                                        {
                                            break;
                                        }

                                        _onVariable$1( var51, onVariable, sortedRefs, sortedLongs );
                                        var5 = BoxedUnit.UNIT;
                                    }

                                    if ( var39 == null )
                                    {
                                        break;
                                    }

                                    Option var54 = (Option) var39._1();
                                    Option var55 = (Option) var39._2();
                                    if ( !(var54 instanceof Some) )
                                    {
                                        break;
                                    }

                                    Some var56 = (Some) var54;
                                    Tuple2 var57 = (Tuple2) var56.value();
                                    if ( !(var55 instanceof Some) )
                                    {
                                        break;
                                    }

                                    Some var58 = (Some) var55;
                                    Tuple2 applyPlanId = (Tuple2) var58.value();
                                    if ( ((Slot) var57._2()).offset() <= applyPlanId._2$mcI$sp() )
                                    {
                                        break;
                                    }

                                    _onApplyPlanId$1( applyPlanId, onApplyPlan, sortedApplyPlanIds );
                                    var5 = BoxedUnit.UNIT;
                                }

                                throw new MatchError( var39 );
                            }

                            var18 = new Tuple2( ((Seq) sortedRefs.elem).headOption(), ((Seq) sortedCached.elem).headOption() );
                            if ( var18 == null )
                            {
                                break;
                            }

                            Option var19 = (Option) var18._1();
                            Option var20 = (Option) var18._2();
                            if ( !(var19 instanceof Some) )
                            {
                                break;
                            }

                            Some var21 = (Some) var19;
                            Tuple2 ref = (Tuple2) var21.value();
                            if ( !scala.None..MODULE$.equals( var20 )){
                            break;
                        }

                            _onVariable$1( ref, onVariable, sortedRefs, sortedLongs );
                            var6 = BoxedUnit.UNIT;
                        }

                        if ( var18 == null )
                        {
                            break;
                        }

                        Option var23 = (Option) var18._1();
                        Option var24 = (Option) var18._2();
                        if ( !scala.None..MODULE$.equals( var23 ) || !(var24 instanceof Some)){
                        break;
                    }

                        Some var25 = (Some) var24;
                        Tuple2 cached = (Tuple2) var25.value();
                        _onCached$1( cached, onCachedProperty, sortedCached );
                        var6 = BoxedUnit.UNIT;
                    }

                    if ( var18 == null )
                    {
                        break;
                    }

                    Option var27 = (Option) var18._1();
                    Option var28 = (Option) var18._2();
                    if ( !(var27 instanceof Some) )
                    {
                        break;
                    }

                    Some var29 = (Some) var27;
                    Tuple2 ref = (Tuple2) var29.value();
                    if ( !(var28 instanceof Some) )
                    {
                        break;
                    }

                    Some var31 = (Some) var28;
                    Tuple2 cached = (Tuple2) var31.value();
                    if ( ((Slot) ref._2()).offset() >= ((RefSlot) cached._2()).offset() )
                    {
                        break;
                    }

                    _onVariable$1( ref, onVariable, sortedRefs, sortedLongs );
                    var6 = BoxedUnit.UNIT;
                }

                if ( var18 == null )
                {
                    break;
                }

                Option var33 = (Option) var18._1();
                Option var34 = (Option) var18._2();
                if ( !(var33 instanceof Some) )
                {
                    break;
                }

                Some var35 = (Some) var33;
                Tuple2 ref = (Tuple2) var35.value();
                if ( !(var34 instanceof Some) )
                {
                    break;
                }

                Some var37 = (Some) var34;
                Tuple2 cached = (Tuple2) var37.value();
                if ( ((Slot) ref._2()).offset() <= ((RefSlot) cached._2()).offset() )
                {
                    break;
                }

                _onCached$1( cached, onCachedProperty, sortedCached );
                var6 = BoxedUnit.UNIT;
            }

            throw new MatchError( var18 );
        }
    }

    public Function1<Id,BoxedUnit> foreachSlotOrdered$default$3()
    {
        return ( x$4 ) -> {
            $anonfun$foreachSlotOrdered$default$3$1( ((Id) x$4).x() );
            return BoxedUnit.UNIT;
        };
    }

    public SlotConfiguration.Size foreachSlotOrdered$default$4()
    {
        return SlotConfiguration.Size$.MODULE$.zero();
    }

    public <U> void foreachCachedSlot( final Function1<Tuple2<ASTCachedProperty,RefSlot>,BoxedUnit> onCachedProperty )
    {
        this.cachedProperties().foreach( onCachedProperty );
    }

    public <U> Iterable<U> mapSlot( final Function1<Tuple2<String,Slot>,U> onVariable, final Function1<Tuple2<ASTCachedProperty,RefSlot>,U> onCachedProperty )
    {
        return (Iterable) ((TraversableLike) this.slots().map( onVariable, scala.collection.mutable.Iterable..MODULE$.canBuildFrom())).$plus$plus(
            (GenTraversableOnce) this.cachedProperties().map( onCachedProperty,
                    scala.collection.mutable.Iterable..MODULE$.canBuildFrom() ), scala.collection.mutable.Iterable..MODULE$.canBuildFrom());
    }

    public boolean canEqual( final Object other )
    {
        return other instanceof SlotConfiguration;
    }

    public boolean equals( final Object other )
    {
        boolean var2;
        if ( other instanceof SlotConfiguration )
        {
            boolean var6;
            label27:
            {
                SlotConfiguration var4 = (SlotConfiguration) other;
                if ( var4.canEqual( this ) )
                {
                    label25:
                    {
                        Map var10000 = this.slots();
                        Map var5 = var4.slots();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label25;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label25;
                        }

                        if ( this.numberOfLongs() == var4.numberOfLongs() && this.numberOfReferences() == var4.numberOfReferences() )
                        {
                            var6 = true;
                            break label27;
                        }
                    }
                }

                var6 = false;
            }

            var2 = var6;
        }
        else
        {
            var2 = false;
        }

        return var2;
    }

    public int hashCode()
    {
        Seq state = (Seq) scala.collection.Seq..MODULE$.apply( scala.Predef..MODULE$.genericWrapArray(
            new Object[]{this.slots(), BoxesRunTime.boxToInteger( this.numberOfLongs() ), BoxesRunTime.boxToInteger( this.numberOfReferences() )} ));
        return BoxesRunTime.unboxToInt( ((TraversableOnce) state.map( ( x$16 ) -> {
            return BoxesRunTime.boxToInteger( $anonfun$hashCode$1( x$16 ) );
        }, scala.collection.Seq..MODULE$.canBuildFrom()) ).foldLeft( BoxesRunTime.boxToInteger( 0 ), ( a, b ) -> {
            return 31 * a + b;
        } ));
    }

    public String toString()
    {
        return (new StringBuilder( 59 )).append( "SlotConfiguration(longs=" ).append( this.numberOfLongs() ).append( ", refs=" ).append(
                this.numberOfReferences() ).append( ", slots=" ).append( this.slots() ).append( ", cachedProperties=" ).append(
                this.cachedProperties() ).append( ")" ).toString();
    }

    public IndexedSeq<SlotWithAliases> getLongSlots()
    {
        return (IndexedSeq) ((SeqLike) this.slotAliases().toIndexedSeq().collect( new Serializable( (SlotConfiguration) null )
        {
            public static final long serialVersionUID = 0L;

            public final <A1 extends Tuple2<Slot,Set<String>>, B1> B1 applyOrElse( final A1 x2, final Function1<A1,B1> default )
            {
                Object var3;
                if ( x2 != null )
                {
                    Slot slot = (Slot) x2._1();
                    Set aliases = (Set) x2._2();
                    if ( slot instanceof LongSlot )
                    {
                        LongSlot var7 = (LongSlot) slot;
                        var3 = new LongSlotWithAliases( var7, aliases.toSet() );
                        return var3;
                    }
                }

                var3 = var2.apply( x2 );
                return var3;
            }

            public final boolean isDefinedAt( final Tuple2<Slot,Set<String>> x2 )
            {
                boolean var2;
                if ( x2 != null )
                {
                    Slot slot = (Slot) x2._1();
                    if ( slot instanceof LongSlot )
                    {
                        var2 = true;
                        return var2;
                    }
                }

                var2 = false;
                return var2;
            }
        }, scala.collection.immutable.IndexedSeq..MODULE$.canBuildFrom())).sorted( this.SlotWithAliasesOrdering() );
    }

    public IndexedSeq<SlotWithAliases> getRefSlots()
    {
        return (IndexedSeq) ((SeqLike) this.slotAliases().toIndexedSeq().collect( new Serializable( (SlotConfiguration) null )
        {
            public static final long serialVersionUID = 0L;

            public final <A1 extends Tuple2<Slot,Set<String>>, B1> B1 applyOrElse( final A1 x3, final Function1<A1,B1> default )
            {
                Object var3;
                if ( x3 != null )
                {
                    Slot slot = (Slot) x3._1();
                    Set aliases = (Set) x3._2();
                    if ( slot instanceof RefSlot )
                    {
                        RefSlot var7 = (RefSlot) slot;
                        var3 = new RefSlotWithAliases( var7, aliases.toSet() );
                        return var3;
                    }
                }

                var3 = var2.apply( x3 );
                return var3;
            }

            public final boolean isDefinedAt( final Tuple2<Slot,Set<String>> x3 )
            {
                boolean var2;
                if ( x3 != null )
                {
                    Slot slot = (Slot) x3._1();
                    if ( slot instanceof RefSlot )
                    {
                        var2 = true;
                        return var2;
                    }
                }

                var2 = false;
                return var2;
            }
        }, scala.collection.immutable.IndexedSeq..MODULE$.canBuildFrom())).sorted( this.SlotWithAliasesOrdering() );
    }

    public IndexedSeq<SlotWithAliases> getCachedPropertySlots()
    {
        return (IndexedSeq) ((SeqLike) this.cachedProperties().toIndexedSeq().map( ( x0$1 ) -> {
            if ( x0$1 != null )
            {
                ASTCachedProperty cachedNodeProperty = (ASTCachedProperty) x0$1._1();
                RefSlot slot = (RefSlot) x0$1._2();
                RefSlotWithAliases var1 =
                        new RefSlotWithAliases( slot, (scala.collection.immutable.Set) scala.Predef..MODULE$.Set().apply( scala.Predef..MODULE$.wrapRefArray(
                                (Object[]) (new String[]{((ASTNode) cachedNodeProperty).asCanonicalStringVal()}) )));
                return var1;
            }
            else
            {
                throw new MatchError( x0$1 );
            }
        }, scala.collection.immutable.IndexedSeq..MODULE$.canBuildFrom())).sorted( this.SlotWithAliasesOrdering() );
    }

    public boolean hasCachedPropertySlot( final ASTCachedProperty key )
    {
        return this.cachedProperties().contains( key );
    }

    public Option<RefSlot> getCachedPropertySlot( final ASTCachedProperty key )
    {
        return this.cachedProperties().get( key );
    }

    private final void SlotWithAliasesOrdering$lzycompute$1()
    {
        synchronized ( this )
        {
        }

        try
        {
            if ( this.SlotWithAliasesOrdering$module == null )
            {
                this.SlotWithAliasesOrdering$module = new SlotConfiguration.SlotWithAliasesOrdering$( this );
            }
        }
        catch ( Throwable var3 )
        {
            throw var3;
        }
    }

    private final void SlotOrdering$lzycompute$1()
    {
        synchronized ( this )
        {
        }

        try
        {
            if ( this.SlotOrdering$module == null )
            {
                this.SlotOrdering$module = new SlotConfiguration.SlotOrdering$( this );
            }
        }
        catch ( Throwable var3 )
        {
            throw var3;
        }
    }

    public static class Size implements Product, Serializable
    {
        private final int nLongs;
        private final int nReferences;

        public Size( final int nLongs, final int nReferences )
        {
            this.nLongs = nLongs;
            this.nReferences = nReferences;
            Product.$init$( this );
        }

        public int nLongs()
        {
            return this.nLongs;
        }

        public int nReferences()
        {
            return this.nReferences;
        }

        public SlotConfiguration.Size copy( final int nLongs, final int nReferences )
        {
            return new SlotConfiguration.Size( nLongs, nReferences );
        }

        public int copy$default$1()
        {
            return this.nLongs();
        }

        public int copy$default$2()
        {
            return this.nReferences();
        }

        public String productPrefix()
        {
            return "Size";
        }

        public int productArity()
        {
            return 2;
        }

        public Object productElement( final int x$1 )
        {
            Integer var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = BoxesRunTime.boxToInteger( this.nLongs() );
                break;
            case 1:
                var10000 = BoxesRunTime.boxToInteger( this.nReferences() );
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof SlotConfiguration.Size;
        }

        public int hashCode()
        {
            int var1 = -889275714;
            var1 = Statics.mix( var1, this.nLongs() );
            var1 = Statics.mix( var1, this.nReferences() );
            return Statics.finalizeHash( var1, 2 );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var10000;
            if ( this != x$1 )
            {
                label51:
                {
                    boolean var2;
                    if ( x$1 instanceof SlotConfiguration.Size )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        SlotConfiguration.Size var4 = (SlotConfiguration.Size) x$1;
                        if ( this.nLongs() == var4.nLongs() && this.nReferences() == var4.nReferences() && var4.canEqual( this ) )
                        {
                            break label51;
                        }
                    }

                    var10000 = false;
                    return var10000;
                }
            }

            var10000 = true;
            return var10000;
        }
    }

    public static class Size$ implements Serializable
    {
        public static SlotConfiguration.Size$ MODULE$;

        static
        {
            new SlotConfiguration.Size$();
        }

        private final SlotConfiguration.Size zero;

        public Size$()
        {
            MODULE$ = this;
            this.zero = new SlotConfiguration.Size( 0, 0 );
        }

        public SlotConfiguration.Size zero()
        {
            return this.zero;
        }

        public SlotConfiguration.Size apply( final int nLongs, final int nReferences )
        {
            return new SlotConfiguration.Size( nLongs, nReferences );
        }

        public Option<Tuple2<Object,Object>> unapply( final SlotConfiguration.Size x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new sp( x$0.nLongs(), x$0.nReferences() ) ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }

    public class SlotOrdering$ implements Ordering<Slot>
    {
        public SlotOrdering$( final SlotConfiguration $outer )
        {
            PartialOrdering.$init$( this );
            Ordering.$init$( this );
        }

        public Some tryCompare( final Object x, final Object y )
        {
            return Ordering.tryCompare$( this, x, y );
        }

        public boolean lteq( final Object x, final Object y )
        {
            return Ordering.lteq$( this, x, y );
        }

        public boolean gteq( final Object x, final Object y )
        {
            return Ordering.gteq$( this, x, y );
        }

        public boolean lt( final Object x, final Object y )
        {
            return Ordering.lt$( this, x, y );
        }

        public boolean gt( final Object x, final Object y )
        {
            return Ordering.gt$( this, x, y );
        }

        public boolean equiv( final Object x, final Object y )
        {
            return Ordering.equiv$( this, x, y );
        }

        public Object max( final Object x, final Object y )
        {
            return Ordering.max$( this, x, y );
        }

        public Object min( final Object x, final Object y )
        {
            return Ordering.min$( this, x, y );
        }

        public Ordering<Slot> reverse()
        {
            return Ordering.reverse$( this );
        }

        public <U> Ordering<U> on( final Function1<U,Slot> f )
        {
            return Ordering.on$( this, f );
        }

        public Ops mkOrderingOps( final Object lhs )
        {
            return Ordering.mkOrderingOps$( this, lhs );
        }

        public int compare( final Slot x, final Slot y )
        {
            Tuple2 var4 = new Tuple2( x, y );
            int var3;
            if ( var4 != null && var4._1() instanceof LongSlot && var4._2() instanceof RefSlot )
            {
                var3 = -1;
            }
            else if ( var4 != null && var4._1() instanceof RefSlot && var4._2() instanceof LongSlot )
            {
                var3 = 1;
            }
            else
            {
                var3 = x.offset() - y.offset();
            }

            return var3;
        }
    }

    public class SlotWithAliasesOrdering$ implements Ordering<SlotWithAliases>
    {
        public SlotWithAliasesOrdering$( final SlotConfiguration $outer )
        {
            PartialOrdering.$init$( this );
            Ordering.$init$( this );
        }

        public Some tryCompare( final Object x, final Object y )
        {
            return Ordering.tryCompare$( this, x, y );
        }

        public boolean lteq( final Object x, final Object y )
        {
            return Ordering.lteq$( this, x, y );
        }

        public boolean gteq( final Object x, final Object y )
        {
            return Ordering.gteq$( this, x, y );
        }

        public boolean lt( final Object x, final Object y )
        {
            return Ordering.lt$( this, x, y );
        }

        public boolean gt( final Object x, final Object y )
        {
            return Ordering.gt$( this, x, y );
        }

        public boolean equiv( final Object x, final Object y )
        {
            return Ordering.equiv$( this, x, y );
        }

        public Object max( final Object x, final Object y )
        {
            return Ordering.max$( this, x, y );
        }

        public Object min( final Object x, final Object y )
        {
            return Ordering.min$( this, x, y );
        }

        public Ordering<SlotWithAliases> reverse()
        {
            return Ordering.reverse$( this );
        }

        public <U> Ordering<U> on( final Function1<U,SlotWithAliases> f )
        {
            return Ordering.on$( this, f );
        }

        public Ops mkOrderingOps( final Object lhs )
        {
            return Ordering.mkOrderingOps$( this, lhs );
        }

        public int compare( final SlotWithAliases x, final SlotWithAliases y )
        {
            Tuple2 var4 = new Tuple2( x, y );
            int var3;
            if ( var4 != null && var4._1() instanceof LongSlotWithAliases && var4._2() instanceof RefSlotWithAliases )
            {
                var3 = -1;
            }
            else if ( var4 != null && var4._1() instanceof RefSlotWithAliases && var4._2() instanceof LongSlotWithAliases )
            {
                var3 = 1;
            }
            else
            {
                var3 = x.slot().offset() - y.slot().offset();
            }

            return var3;
        }
    }
}
