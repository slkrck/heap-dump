package org.neo4j.cypher.internal.physicalplanning;

import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class NoOutput
{
    public static String toString()
    {
        return NoOutput$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return NoOutput$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return NoOutput$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return NoOutput$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return NoOutput$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return NoOutput$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return NoOutput$.MODULE$.productPrefix();
    }
}
