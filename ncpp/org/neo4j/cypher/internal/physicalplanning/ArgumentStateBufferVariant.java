package org.neo4j.cypher.internal.physicalplanning;

import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class ArgumentStateBufferVariant implements BufferVariant, Product, Serializable
{
    private final int argumentStateMapId;

    public ArgumentStateBufferVariant( final int argumentStateMapId )
    {
        this.argumentStateMapId = argumentStateMapId;
        Product.$init$( this );
    }

    public static Option<ArgumentStateMapId> unapply( final ArgumentStateBufferVariant x$0 )
    {
        return ArgumentStateBufferVariant$.MODULE$.unapply( var0 );
    }

    public static ArgumentStateBufferVariant apply( final int argumentStateMapId )
    {
        return ArgumentStateBufferVariant$.MODULE$.apply( var0 );
    }

    public static <A> Function1<ArgumentStateMapId,A> andThen( final Function1<ArgumentStateBufferVariant,A> g )
    {
        return ArgumentStateBufferVariant$.MODULE$.andThen( var0 );
    }

    public static <A> Function1<A,ArgumentStateBufferVariant> compose( final Function1<A,ArgumentStateMapId> g )
    {
        return ArgumentStateBufferVariant$.MODULE$.compose( var0 );
    }

    public int argumentStateMapId()
    {
        return this.argumentStateMapId;
    }

    public ArgumentStateBufferVariant copy( final int argumentStateMapId )
    {
        return new ArgumentStateBufferVariant( argumentStateMapId );
    }

    public int copy$default$1()
    {
        return this.argumentStateMapId();
    }

    public String productPrefix()
    {
        return "ArgumentStateBufferVariant";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return new ArgumentStateMapId( this.argumentStateMapId() );
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof ArgumentStateBufferVariant;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var10000;
        if ( this != x$1 )
        {
            label49:
            {
                boolean var2;
                if ( x$1 instanceof ArgumentStateBufferVariant )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    ArgumentStateBufferVariant var4 = (ArgumentStateBufferVariant) x$1;
                    if ( this.argumentStateMapId() == var4.argumentStateMapId() && var4.canEqual( this ) )
                    {
                        break label49;
                    }
                }

                var10000 = false;
                return var10000;
            }
        }

        var10000 = true;
        return var10000;
    }
}
