package org.neo4j.cypher.internal.physicalplanning;

import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple3;
import scala.Predef.;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class ApplyBufferVariant implements BufferVariant, Product, Serializable
{
    private final int argumentSlotOffset;
    private final ArgumentStateMapId[] reducersOnRHSReversed;
    private final BufferId[] delegates;

    public ApplyBufferVariant( final int argumentSlotOffset, final ArgumentStateMapId[] reducersOnRHSReversed, final BufferId[] delegates )
    {
        this.argumentSlotOffset = argumentSlotOffset;
        this.reducersOnRHSReversed = reducersOnRHSReversed;
        this.delegates = delegates;
        Product.$init$( this );
    }

    public static Option<Tuple3<Object,int[],int[]>> unapply( final ApplyBufferVariant x$0 )
    {
        return ApplyBufferVariant$.MODULE$.unapply( var0 );
    }

    public static ApplyBufferVariant apply( final int argumentSlotOffset, final ArgumentStateMapId[] reducersOnRHSReversed, final BufferId[] delegates )
    {
        return ApplyBufferVariant$.MODULE$.apply( var0, var1, var2 );
    }

    public static Function1<Tuple3<Object,int[],int[]>,ApplyBufferVariant> tupled()
    {
        return ApplyBufferVariant$.MODULE$.tupled();
    }

    public static Function1<Object,Function1<int[],Function1<int[],ApplyBufferVariant>>> curried()
    {
        return ApplyBufferVariant$.MODULE$.curried();
    }

    public int argumentSlotOffset()
    {
        return this.argumentSlotOffset;
    }

    public ArgumentStateMapId[] reducersOnRHSReversed()
    {
        return this.reducersOnRHSReversed;
    }

    public BufferId[] delegates()
    {
        return this.delegates;
    }

    public boolean canEqual( final Object that )
    {
        return that instanceof ApplyBufferVariant;
    }

    public boolean equals( final Object obj )
    {
        boolean var10000;
        if ( obj instanceof ApplyBufferVariant )
        {
            ApplyBufferVariant other = (ApplyBufferVariant) obj;
            if ( this.asTuple().equals( other.asTuple() ) )
            {
                var10000 = true;
                return var10000;
            }
        }

        var10000 = false;
        return var10000;
    }

    public int hashCode()
    {
        return this.asTuple().hashCode();
    }

    private Tuple3<Object,Seq<ArgumentStateMapId>,Seq<BufferId>> asTuple()
    {
        return new Tuple3( BoxesRunTime.boxToInteger( this.argumentSlotOffset() ),.MODULE$.genericArrayOps( this.reducersOnRHSReversed() ).toList(), .
        MODULE$.genericArrayOps( this.delegates() ).toList());
    }

    public String toString()
    {
        return (new StringBuilder( 18 )).append( "ApplyBufferVariant" ).append( this.asTuple() ).toString();
    }

    public ApplyBufferVariant copy( final int argumentSlotOffset, final ArgumentStateMapId[] reducersOnRHSReversed, final BufferId[] delegates )
    {
        return new ApplyBufferVariant( argumentSlotOffset, reducersOnRHSReversed, delegates );
    }

    public int copy$default$1()
    {
        return this.argumentSlotOffset();
    }

    public ArgumentStateMapId[] copy$default$2()
    {
        return this.reducersOnRHSReversed();
    }

    public BufferId[] copy$default$3()
    {
        return this.delegates();
    }

    public String productPrefix()
    {
        return "ApplyBufferVariant";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = BoxesRunTime.boxToInteger( this.argumentSlotOffset() );
            break;
        case 1:
            var10000 = this.reducersOnRHSReversed();
            break;
        case 2:
            var10000 = this.delegates();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }
}
