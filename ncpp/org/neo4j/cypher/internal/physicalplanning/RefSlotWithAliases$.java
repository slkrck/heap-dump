package org.neo4j.cypher.internal.physicalplanning;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.collection.immutable.Set;
import scala.runtime.AbstractFunction2;

public final class RefSlotWithAliases$ extends AbstractFunction2<RefSlot,Set<String>,RefSlotWithAliases> implements Serializable
{
    public static RefSlotWithAliases$ MODULE$;

    static
    {
        new RefSlotWithAliases$();
    }

    private RefSlotWithAliases$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "RefSlotWithAliases";
    }

    public RefSlotWithAliases apply( final RefSlot slot, final Set<String> aliases )
    {
        return new RefSlotWithAliases( slot, aliases );
    }

    public Option<Tuple2<RefSlot,Set<String>>> unapply( final RefSlotWithAliases x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( x$0.slot(), x$0.aliases() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
