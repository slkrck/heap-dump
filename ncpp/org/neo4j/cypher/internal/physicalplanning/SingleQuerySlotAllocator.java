package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.ir.CSVFormat;
import org.neo4j.cypher.internal.ir.ShortestPathPattern;
import org.neo4j.cypher.internal.logical.plans.AbstractSelectOrSemiApply;
import org.neo4j.cypher.internal.logical.plans.AbstractSemiApply;
import org.neo4j.cypher.internal.logical.plans.Aggregation;
import org.neo4j.cypher.internal.logical.plans.AntiConditionalApply;
import org.neo4j.cypher.internal.logical.plans.Apply;
import org.neo4j.cypher.internal.logical.plans.ApplyPlan;
import org.neo4j.cypher.internal.logical.plans.Argument;
import org.neo4j.cypher.internal.logical.plans.AssertSameNode;
import org.neo4j.cypher.internal.logical.plans.CacheProperties;
import org.neo4j.cypher.internal.logical.plans.CartesianProduct;
import org.neo4j.cypher.internal.logical.plans.ConditionalApply;
import org.neo4j.cypher.internal.logical.plans.Create;
import org.neo4j.cypher.internal.logical.plans.DeleteExpression;
import org.neo4j.cypher.internal.logical.plans.DeleteNode;
import org.neo4j.cypher.internal.logical.plans.DeletePath;
import org.neo4j.cypher.internal.logical.plans.DeleteRelationship;
import org.neo4j.cypher.internal.logical.plans.DetachDeleteExpression;
import org.neo4j.cypher.internal.logical.plans.DetachDeleteNode;
import org.neo4j.cypher.internal.logical.plans.DetachDeletePath;
import org.neo4j.cypher.internal.logical.plans.DirectedRelationshipByIdSeek;
import org.neo4j.cypher.internal.logical.plans.DropResult;
import org.neo4j.cypher.internal.logical.plans.Eager;
import org.neo4j.cypher.internal.logical.plans.EmptyResult;
import org.neo4j.cypher.internal.logical.plans.ErrorPlan;
import org.neo4j.cypher.internal.logical.plans.Expand;
import org.neo4j.cypher.internal.logical.plans.ExpansionMode;
import org.neo4j.cypher.internal.logical.plans.FindShortestPaths;
import org.neo4j.cypher.internal.logical.plans.ForeachApply;
import org.neo4j.cypher.internal.logical.plans.IndexLeafPlan;
import org.neo4j.cypher.internal.logical.plans.Input;
import org.neo4j.cypher.internal.logical.plans.LeftOuterHashJoin;
import org.neo4j.cypher.internal.logical.plans.LetAntiSemiApply;
import org.neo4j.cypher.internal.logical.plans.LetSelectOrAntiSemiApply;
import org.neo4j.cypher.internal.logical.plans.LetSelectOrSemiApply;
import org.neo4j.cypher.internal.logical.plans.LetSemiApply;
import org.neo4j.cypher.internal.logical.plans.Limit;
import org.neo4j.cypher.internal.logical.plans.LoadCSV;
import org.neo4j.cypher.internal.logical.plans.LockNodes;
import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.logical.plans.MergeCreateNode;
import org.neo4j.cypher.internal.logical.plans.MergeCreateRelationship;
import org.neo4j.cypher.internal.logical.plans.NestedPlanExpression;
import org.neo4j.cypher.internal.logical.plans.NodeCountFromCountStore;
import org.neo4j.cypher.internal.logical.plans.NodeHashJoin;
import org.neo4j.cypher.internal.logical.plans.NodeLogicalLeafPlan;
import org.neo4j.cypher.internal.logical.plans.Optional;
import org.neo4j.cypher.internal.logical.plans.OptionalExpand;
import org.neo4j.cypher.internal.logical.plans.OrderedAggregation;
import org.neo4j.cypher.internal.logical.plans.PartialSort;
import org.neo4j.cypher.internal.logical.plans.PartialTop;
import org.neo4j.cypher.internal.logical.plans.ProcedureCall;
import org.neo4j.cypher.internal.logical.plans.ProduceResult;
import org.neo4j.cypher.internal.logical.plans.ProjectEndpoints;
import org.neo4j.cypher.internal.logical.plans.ProjectingPlan;
import org.neo4j.cypher.internal.logical.plans.PruningVarExpand;
import org.neo4j.cypher.internal.logical.plans.RelationshipCountFromCountStore;
import org.neo4j.cypher.internal.logical.plans.RemoveLabels;
import org.neo4j.cypher.internal.logical.plans.ResolvedCall;
import org.neo4j.cypher.internal.logical.plans.RightOuterHashJoin;
import org.neo4j.cypher.internal.logical.plans.RollUpApply;
import org.neo4j.cypher.internal.logical.plans.Selection;
import org.neo4j.cypher.internal.logical.plans.SetLabels;
import org.neo4j.cypher.internal.logical.plans.SetNodePropertiesFromMap;
import org.neo4j.cypher.internal.logical.plans.SetNodeProperty;
import org.neo4j.cypher.internal.logical.plans.SetPropertiesFromMap;
import org.neo4j.cypher.internal.logical.plans.SetProperty;
import org.neo4j.cypher.internal.logical.plans.SetRelationshipPropertiesFromMap;
import org.neo4j.cypher.internal.logical.plans.SetRelationshipProperty;
import org.neo4j.cypher.internal.logical.plans.Skip;
import org.neo4j.cypher.internal.logical.plans.Sort;
import org.neo4j.cypher.internal.logical.plans.Top;
import org.neo4j.cypher.internal.logical.plans.TriadicSelection;
import org.neo4j.cypher.internal.logical.plans.UndirectedRelationshipByIdSeek;
import org.neo4j.cypher.internal.logical.plans.Union;
import org.neo4j.cypher.internal.logical.plans.UnwindCollection;
import org.neo4j.cypher.internal.logical.plans.ValueHashJoin;
import org.neo4j.cypher.internal.logical.plans.VarExpand;
import org.neo4j.cypher.internal.runtime.expressionVariableAllocation.AvailableExpressionVariables;
import org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticTable;
import org.neo4j.cypher.internal.v4_0.expressions.CachedProperty;
import org.neo4j.cypher.internal.v4_0.expressions.Equals;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import org.neo4j.cypher.internal.v4_0.expressions.LogicalVariable;
import org.neo4j.cypher.internal.v4_0.expressions.PatternElement;
import org.neo4j.cypher.internal.v4_0.expressions.RelationshipChain;
import org.neo4j.cypher.internal.v4_0.expressions.RelationshipPattern;
import org.neo4j.cypher.internal.v4_0.expressions.ShortestPaths;
import org.neo4j.cypher.internal.v4_0.expressions.Variable;
import org.neo4j.cypher.internal.v4_0.util.Foldable;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id;
import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import org.neo4j.cypher.internal.v4_0.util.symbols.NodeType;
import org.neo4j.cypher.internal.v4_0.util.symbols.RelationshipType;
import scala.Function1;
import scala.Function2;
import scala.MatchError;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.Tuple2.mcZZ.sp;
import scala.collection.IndexedSeq;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.immutable.Map;
import scala.collection.immutable.Set;
import scala.collection.mutable.Stack;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.LazyRef;
import scala.runtime.ObjectRef;

@JavaDocToJava
public class SingleQuerySlotAllocator
{
    public final PipelineBreakingPolicy org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$breakingPolicy;
    public final AvailableExpressionVariables org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$availableExpressionVariables;
    private final boolean allocateArgumentSlots;
    private final PhysicalPlanningAttributes.SlotConfigurations allocations;
    private final PhysicalPlanningAttributes.ArgumentSizes argumentSizes;
    private final PhysicalPlanningAttributes.ApplyPlans org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$applyPlans;
    private final PhysicalPlanningAttributes.NestedPlanArgumentConfigurations
            org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$nestedPlanArgumentConfigurations;

    public SingleQuerySlotAllocator( final boolean allocateArgumentSlots, final PipelineBreakingPolicy breakingPolicy,
            final AvailableExpressionVariables availableExpressionVariables )
    {
        this.allocateArgumentSlots = allocateArgumentSlots;
        this.org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$breakingPolicy = breakingPolicy;
        this.org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$availableExpressionVariables = availableExpressionVariables;
        this.allocations = new PhysicalPlanningAttributes.SlotConfigurations();
        this.argumentSizes = new PhysicalPlanningAttributes.ArgumentSizes();
        this.org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$applyPlans = new PhysicalPlanningAttributes.ApplyPlans();
        this.org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$nestedPlanArgumentConfigurations =
                new PhysicalPlanningAttributes.NestedPlanArgumentConfigurations();
    }

    private static final Stack populate$1( final LogicalPlan plan, final boolean nullIn, final Stack planStack$1, final ObjectRef comingFrom$1 )
    {
        boolean nullable = nullIn;

        LogicalPlan current;
        for ( current = plan; !current.isLeaf(); current = (LogicalPlan) current.lhs().get() )
        {
            if ( current instanceof Optional )
            {
                nullable = true;
            }

            planStack$1.push( new Tuple2( BoxesRunTime.boxToBoolean( nullable ), current ) );
        }

        comingFrom$1.elem = current;
        return planStack$1.push( new Tuple2( BoxesRunTime.boxToBoolean( nullable ), current ) );
    }

    private static final void onVariableSlot$1( final String key, final Slot slot, final Set nodes$1, final SlotConfiguration result$2 )
    {
        if ( !nodes$1.apply( key ) )
        {
            result$2.add( key, slot.asNullable() );
        }
    }

    private static final void onVariableSlot$2( final String key, final Slot slot, final Set nodes$2, final SlotConfiguration result$3 )
    {
        if ( !nodes$2.apply( key ) )
        {
            result$3.add( key, slot.asNullable() );
        }
    }

    private static final void onVariableSlot$3( final String key, final Slot slot, final Set nodes$3, final SlotConfiguration result$4 )
    {
        if ( !nodes$3.apply( key ) )
        {
            result$4.add( key, slot );
        }
    }

    private PhysicalPlanningAttributes.SlotConfigurations allocations()
    {
        return this.allocations;
    }

    private PhysicalPlanningAttributes.ArgumentSizes argumentSizes()
    {
        return this.argumentSizes;
    }

    public PhysicalPlanningAttributes.ApplyPlans org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$applyPlans()
    {
        return this.org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$applyPlans;
    }

    public PhysicalPlanningAttributes.NestedPlanArgumentConfigurations org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$nestedPlanArgumentConfigurations()
    {
        return this.org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$nestedPlanArgumentConfigurations;
    }

    private boolean argumentRowIdSlotForCartesianProductNeeded( final LogicalPlan plan )
    {
        return this.allocateArgumentSlots && plan instanceof CartesianProduct;
    }

    public SlotAllocation.SlotMetaData allocateSlots( final LogicalPlan lp, final SemanticTable semanticTable,
            final Option<SlotAllocation.SlotsAndArgument> initialSlotsAndArgument )
    {
        Stack planStack = new Stack();
        Stack resultStack = new Stack();
        Stack argumentStack = new Stack();
        initialSlotsAndArgument.foreach( ( elem ) -> {
            return argumentStack.push( elem );
        } );
        ObjectRef comingFrom = ObjectRef.create( lp );
        populate$1( lp, false, planStack, comingFrom );

        Tuple2 var17;
        while ( true )
        {
            if ( !planStack.nonEmpty() )
            {
                return new SlotAllocation.SlotMetaData( this.allocations(), this.argumentSizes(),
                        this.org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$applyPlans(),
                        this.org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$nestedPlanArgumentConfigurations() );
            }

            Tuple2 var11 = (Tuple2) planStack.pop();
            if ( var11 == null )
            {
                throw new MatchError( var11 );
            }

            LogicalPlan current;
            label149:
            {
                boolean nullable = var11._1$mcZ$sp();
                LogicalPlan current = (LogicalPlan) var11._2();
                Tuple2 var5 = new Tuple2( BoxesRunTime.boxToBoolean( nullable ), current );
                boolean nullable = var5._1$mcZ$sp();
                current = (LogicalPlan) var5._2();
                int outerApplyPlan = argumentStack.isEmpty() ? org.neo4j.cypher.internal.v4_0.util.attribution.Id..MODULE$.INVALID_ID() :
                ((SlotAllocation.SlotsAndArgument) argumentStack.top()).argumentPlan();
                this.org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$applyPlans().set( current.id(), new Id( outerApplyPlan ) );
                var17 = new Tuple2( current.lhs(), current.rhs() );
                Stack var4;
                if ( var17 != null )
                {
                    Option var18 = (Option) var17._1();
                    Option var19 = (Option) var17._2();
                    if ( scala.None..MODULE$.equals( var18 ) && scala.None..MODULE$.equals( var19 )){
                    SlotAllocation.SlotsAndArgument argument = argumentStack.isEmpty() ? SlotAllocation$.MODULE$.NO_ARGUMENT( this.allocateArgumentSlots )
                                                                                       : (SlotAllocation.SlotsAndArgument) argumentStack.top();
                    this.recordArgument$1( current, argument );
                    SlotConfiguration slots = this.org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$breakingPolicy.invoke( current,
                            argument.slotConfiguration(), argument.slotConfiguration() );
                    this.allocateExpressions( current, nullable, slots, semanticTable, this.allocateExpressions$default$5() );
                    this.allocateLeaf( current, nullable, slots );
                    this.allocations().set( current.id(), slots );
                    var4 = resultStack.push( slots );
                    break label149;
                }
                }

                if ( var17 != null )
                {
                    Option var22 = (Option) var17._1();
                    Option var23 = (Option) var17._2();
                    if ( var22 instanceof Some && scala.None..MODULE$.equals( var23 )){
                    SlotConfiguration sourceSlots = (SlotConfiguration) resultStack.pop();
                    SlotAllocation.SlotsAndArgument argument = argumentStack.isEmpty() ? SlotAllocation$.MODULE$.NO_ARGUMENT( this.allocateArgumentSlots )
                                                                                       : (SlotAllocation.SlotsAndArgument) argumentStack.top();
                    this.allocateExpressions( current, nullable, sourceSlots, semanticTable, this.allocateExpressions$default$5() );
                    SlotConfiguration slots =
                            this.org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$breakingPolicy.invoke( current, sourceSlots,
                                    argument.slotConfiguration() );
                    this.allocateOneChild( current, nullable, sourceSlots, slots, ( x$2 ) -> {
                        $anonfun$allocateSlots$2( this, argument, x$2 );
                        return BoxedUnit.UNIT;
                    }, semanticTable );
                    this.allocations().set( current.id(), slots );
                    var4 = resultStack.push( slots );
                    break label149;
                }
                }

                BoxedUnit var10000;
                if ( var17 != null )
                {
                    Option var27 = (Option) var17._1();
                    Option var28 = (Option) var17._2();
                    if ( var27 instanceof Some )
                    {
                        Some var29 = (Some) var27;
                        LogicalPlan left = (LogicalPlan) var29.value();
                        if ( var28 instanceof Some )
                        {
                            Some var31 = (Some) var28;
                            LogicalPlan right = (LogicalPlan) var31.value();
                            if ( (LogicalPlan) comingFrom.elem == left && current instanceof ApplyPlan )
                            {
                                planStack.push( new Tuple2( BoxesRunTime.boxToBoolean( nullable ), current ) );
                                SlotConfiguration argumentSlots = (SlotConfiguration) resultStack.top();
                                if ( this.allocateArgumentSlots )
                                {
                                    argumentSlots.newArgument( current.id() );
                                }
                                else
                                {
                                    var10000 = BoxedUnit.UNIT;
                                }

                                this.allocateLhsOfApply( current, nullable, argumentSlots, semanticTable );
                                argumentStack.push( new SlotAllocation.SlotsAndArgument( argumentSlots, argumentSlots.size(), current.id() ) );
                                var4 = populate$1( right, nullable, planStack, comingFrom );
                                break label149;
                            }
                        }
                    }
                }

                if ( var17 != null )
                {
                    Option var34 = (Option) var17._1();
                    Option var35 = (Option) var17._2();
                    if ( var34 instanceof Some )
                    {
                        Some var36 = (Some) var34;
                        LogicalPlan left = (LogicalPlan) var36.value();
                        if ( var35 instanceof Some )
                        {
                            Some var38 = (Some) var35;
                            LogicalPlan right = (LogicalPlan) var38.value();
                            if ( (LogicalPlan) comingFrom.elem == left )
                            {
                                planStack.push( new Tuple2( BoxesRunTime.boxToBoolean( nullable ), current ) );
                                if ( this.argumentRowIdSlotForCartesianProductNeeded( current ) )
                                {
                                    SlotAllocation.SlotsAndArgument previousArgument =
                                            argumentStack.isEmpty() ? SlotAllocation$.MODULE$.NO_ARGUMENT( this.allocateArgumentSlots )
                                                                    : (SlotAllocation.SlotsAndArgument) argumentStack.top();
                                    SlotConfiguration newArgument = previousArgument.slotConfiguration().copy().newArgument( current.id() );
                                    argumentStack.push( new SlotAllocation.SlotsAndArgument( newArgument, newArgument.size(), current.id() ) );
                                }
                                else
                                {
                                    var10000 = BoxedUnit.UNIT;
                                }

                                var4 = populate$1( right, nullable, planStack, comingFrom );
                                break label149;
                            }
                        }
                    }
                }

                if ( var17 == null )
                {
                    break;
                }

                Option var42 = (Option) var17._1();
                Option var43 = (Option) var17._2();
                if ( !(var42 instanceof Some) || !(var43 instanceof Some) )
                {
                    break;
                }

                Some var44 = (Some) var43;
                LogicalPlan right = (LogicalPlan) var44.value();
                if ( (LogicalPlan) comingFrom.elem != right )
                {
                    break;
                }

                SlotConfiguration rhsSlots = (SlotConfiguration) resultStack.pop();
                SlotConfiguration lhsSlots = (SlotConfiguration) resultStack.pop();
                SlotAllocation.SlotsAndArgument argument = argumentStack.isEmpty() ? SlotAllocation$.MODULE$.NO_ARGUMENT( this.allocateArgumentSlots )
                                                                                   : (SlotAllocation.SlotsAndArgument) argumentStack.top();
                this.allocateExpressions( current, nullable, lhsSlots, semanticTable, true );
                this.allocateExpressions( current, nullable, rhsSlots, semanticTable, false );
                SlotConfiguration result = this.allocateTwoChild( current, nullable, lhsSlots, rhsSlots, ( x$3 ) -> {
                    $anonfun$allocateSlots$3( this, argument, x$3 );
                    return BoxedUnit.UNIT;
                }, argument );
                this.allocations().set( current.id(), result );
                if ( !(current instanceof ApplyPlan) && !this.argumentRowIdSlotForCartesianProductNeeded( current ) )
                {
                    var10000 = BoxedUnit.UNIT;
                }
                else
                {
                    argumentStack.pop();
                }

                var4 = resultStack.push( result );
            }

            comingFrom.elem = current;
        }

        throw new MatchError( var17 );
    }

    private void allocateExpressions( final LogicalPlan lp, final boolean nullable, final SlotConfiguration slots, final SemanticTable semanticTable,
            final boolean shouldAllocateLhs )
    {
        this.org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$allocateExpressionsInternal( lp, nullable, slots, semanticTable,
                shouldAllocateLhs, lp.id() );
    }

    private boolean allocateExpressions$default$5()
    {
        return true;
    }

    public void org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$allocateExpressionsInternal( final Foldable p, final boolean nullable,
            final SlotConfiguration slots, final SemanticTable semanticTable, final boolean shouldAllocateLhs, final int planId )
    {
        LazyRef Accumulator$module = new LazyRef();
        Some TRAVERSE_INTO_CHILDREN = new Some( ( s ) -> {
            return s;
        } );
        scala.None.DO_NOT_TRAVERSE_INTO_CHILDREN = scala.None..MODULE$;
        org.neo4j.cypher.internal.v4_0.util.Foldable.FoldableAny..MODULE$.treeFold$extension( org.neo4j.cypher.internal.v4_0.util.Foldable..MODULE$.FoldableAny(
            p ), this.org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$Accumulator$2( Accumulator$module ).apply(
            (Option) scala.None..MODULE$),
        new         class Accumulator$3 implements Product, Serializable
        {
            private final Option<Expression> doNotTraverseExpression;

            public Accumulator$3( final SingleQuerySlotAllocator $outer, final Option<Expression> doNotTraverseExpression )
            {
                this.doNotTraverseExpression = doNotTraverseExpression;
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                    super();
                    Product.$init$( this );
                }
            }

            public Option<Expression> doNotTraverseExpression()
            {
                return this.doNotTraverseExpression;
            }

            public Accumulator$3 copy( final Option<Expression> doNotTraverseExpression )
            {
                return new Accumulator$3( this.$outer, doNotTraverseExpression );
            }

            public Option<Expression> copy$default$1()
            {
                return this.doNotTraverseExpression();
            }

            public String productPrefix()
            {
                return "Accumulator";
            }

            public int productArity()
            {
                return 1;
            }

            public Object productElement( final int x$1 )
            {
                switch ( x$1 )
                {
                case 0:
                    return this.doNotTraverseExpression();
                default:
                    throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
                }
            }

            public Iterator<Object> productIterator()
            {
                return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
            }

            public boolean canEqual( final Object x$1 )
            {
                return x$1 instanceof Accumulator$3;
            }

            public int hashCode()
            {
                return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
            }

            public String toString()
            {
                return scala.runtime.ScalaRunTime..MODULE$._toString( this );
            }

            public boolean equals( final Object x$1 )
            {
                boolean var6;
                label47:
                {
                    if ( this != x$1 )
                    {
                        boolean var2;
                        if ( x$1 instanceof Accumulator$3 )
                        {
                            var2 = true;
                        }
                        else
                        {
                            var2 = false;
                        }

                        if ( !var2 )
                        {
                            break label47;
                        }

                        label35:
                        {
                            label34:
                            {
                                Accumulator$3 var4 = (Accumulator$3) x$1;
                                Option var10000 = this.doNotTraverseExpression();
                                Option var5 = var4.doNotTraverseExpression();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label34;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label34;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var6 = true;
                                    break label35;
                                }
                            }

                            var6 = false;
                        }

                        if ( !var6 )
                        {
                            break label47;
                        }
                    }

                    var6 = true;
                    return var6;
                }

                var6 = false;
                return var6;
            }
        });

Serializable( this, nullable, slots, semanticTable, shouldAllocateLhs, planId, TRAVERSE_INTO_CHILDREN, DO_NOT_TRAVERSE_INTO_CHILDREN,
                Accumulator$module )
        {
            public static final long serialVersionUID = 0L;
            private final boolean nullable$1;
            private final SlotConfiguration slots$3;
            private final SemanticTable semanticTable$2;
            private final boolean shouldAllocateLhs$1;
            private final int planId$1;
            private final Some TRAVERSE_INTO_CHILDREN$1;
            private final scala.None.DO_NOT_TRAVERSE_INTO_CHILDREN$1;
            private final LazyRef Accumulator$module$1;

            public
            {
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                    this.nullable$1 = nullable$1;
                    this.slots$3 = slots$3;
                    this.semanticTable$2 = semanticTable$2;
                    this.shouldAllocateLhs$1 = shouldAllocateLhs$1;
                    this.planId$1 = planId$1;
                    this.TRAVERSE_INTO_CHILDREN$1 = TRAVERSE_INTO_CHILDREN$1;
                    this.DO_NOT_TRAVERSE_INTO_CHILDREN$1 = DO_NOT_TRAVERSE_INTO_CHILDREN$1;
                    this.Accumulator$module$1 = Accumulator$module$1;
                }
            }

            public final <A1, B1> B1 applyOrElse( final A1 x1, final Function1<A1,B1> default )
            {
                boolean var4 = false;
                ValueHashJoin var5 = null;
                Object var3;
                if ( x1 instanceof LogicalPlan )
                {
                    LogicalPlan var7 = (LogicalPlan) x1;
                    if ( var7.id() != this.planId$1 )
                    {
                        var3 = ( acc ) -> {
                            return new Tuple2( acc, this.DO_NOT_TRAVERSE_INTO_CHILDREN$1 );
                        };
                        return var3;
                    }
                }

                if ( x1 instanceof ValueHashJoin )
                {
                    var4 = true;
                    var5 = (ValueHashJoin) x1;
                    Equals var8 = var5.join();
                    if ( var8 != null )
                    {
                        Expression rhsExpression = var8.rhs();
                        if ( this.shouldAllocateLhs$1 )
                        {
                            var3 = ( x$4 ) -> {
                                return new Tuple2( this.$outer.org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$Accumulator$2(
                                        this.Accumulator$module$1 ).apply( (Option) (new Some( rhsExpression )) ), this.TRAVERSE_INTO_CHILDREN$1 );
                            };
                            return var3;
                        }
                    }
                }

                if ( var4 )
                {
                    Equals var10 = var5.join();
                    if ( var10 != null )
                    {
                        Expression lhsExpression = var10.lhs();
                        if ( !this.shouldAllocateLhs$1 )
                        {
                            var3 = ( x$5 ) -> {
                                return new Tuple2( this.$outer.org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$Accumulator$2(
                                        this.Accumulator$module$1 ).apply( (Option) (new Some( lhsExpression )) ), this.TRAVERSE_INTO_CHILDREN$1 );
                            };
                            return var3;
                        }
                    }
                }

                if ( x1 instanceof FindShortestPaths )
                {
                    FindShortestPaths var12 = (FindShortestPaths) x1;
                    ShortestPathPattern shortestPathPattern = var12.shortestPath();
                    var3 = ( acc ) -> {
                        this.$outer.org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$allocateShortestPathPattern( shortestPathPattern,
                                this.slots$3, this.nullable$1 );
                        return new Tuple2( acc, this.TRAVERSE_INTO_CHILDREN$1 );
                    };
                }
                else if ( x1 instanceof ProjectEndpoints )
                {
                    ProjectEndpoints var14 = (ProjectEndpoints) x1;
                    String start = var14.start();
                    boolean startInScope = var14.startInScope();
                    String end = var14.end();
                    boolean endInScope = var14.endInScope();
                    var3 = ( acc ) -> {
                        BoxedUnit var10000;
                        if ( !startInScope )
                        {
                            this.slots$3.newLong( start, this.nullable$1, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode());
                        }
                        else
                        {
                            var10000 = BoxedUnit.UNIT;
                        }

                        if ( !endInScope )
                        {
                            this.slots$3.newLong( end, this.nullable$1, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode());
                        }
                        else
                        {
                            var10000 = BoxedUnit.UNIT;
                        }

                        return new Tuple2( acc, this.TRAVERSE_INTO_CHILDREN$1 );
                    };
                }
                else if ( x1 instanceof ApplyPlan && !this.shouldAllocateLhs$1 )
                {
                    var3 = ( acc ) -> {
                        return new Tuple2( acc, this.DO_NOT_TRAVERSE_INTO_CHILDREN$1 );
                    };
                }
                else if ( x1 instanceof NestedPlanExpression )
                {
                    NestedPlanExpression var19 = (NestedPlanExpression) x1;
                    var3 = ( acc ) -> {
                        Tuple2 var10000;
                        if ( acc.doNotTraverseExpression().contains( var19 ) )
                        {
                            var10000 = new Tuple2( acc, this.DO_NOT_TRAVERSE_INTO_CHILDREN$1 );
                        }
                        else
                        {
                            this.$outer.org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$breakingPolicy.onNestedPlanBreak();
                            SlotConfiguration argumentSlotConfiguration = this.slots$3.copy();
                            ((IterableLike) this.$outer.org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$availableExpressionVariables.apply(
                                    var19.plan().id() )).foreach( ( expVar ) -> {
                                return argumentSlotConfiguration.newReference( expVar.name(), true, org.neo4j.cypher.internal.v4_0.util.symbols.package..
                                MODULE$.CTAny());
                            } ); this.$outer.org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$nestedPlanArgumentConfigurations().set(
                                var19.plan().id(), argumentSlotConfiguration );
                            SlotAllocation.SlotsAndArgument slotsAndArgument =
                                    new SlotAllocation.SlotsAndArgument( argumentSlotConfiguration.copy(), argumentSlotConfiguration.size(),
                                            ((Id) this.$outer.org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$applyPlans().apply(
                                                    this.planId$1 )).x() );
                            SlotAllocation.SlotMetaData nestedPhysicalPlan =
                                    this.$outer.allocateSlots( var19.plan(), this.semanticTable$2, new Some( slotsAndArgument ) );
                            SlotConfiguration nestedSlots = (SlotConfiguration) nestedPhysicalPlan.slotConfigurations().apply( var19.plan().id() );
                            this.$outer.org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$allocateExpressionsInternal( var19.projection(),
                                    this.nullable$1, nestedSlots, this.semanticTable$2, this.shouldAllocateLhs$1, this.planId$1 );
                            var10000 = new Tuple2( acc, this.DO_NOT_TRAVERSE_INTO_CHILDREN$1 );
                        }

                        return var10000;
                    };
                }
                else if ( x1 instanceof Expression )
                {
                    Expression var20 = (Expression) x1;
                    var3 = ( acc ) -> {
                        Tuple2 var10000;
                        if ( acc.doNotTraverseExpression().contains( var20 ) )
                        {
                            var10000 = new Tuple2( acc, this.DO_NOT_TRAVERSE_INTO_CHILDREN$1 );
                        }
                        else
                        {
                            if ( var20 instanceof CachedProperty )
                            {
                                CachedProperty var5 = (CachedProperty) var20;
                                SlotConfiguration var3 = this.slots$3.newCachedProperty( var5, this.slots$3.newCachedProperty$default$2() );
                            }
                            else
                            {
                                BoxedUnit var6 = BoxedUnit.UNIT;
                            }

                            var10000 = new Tuple2( acc, this.TRAVERSE_INTO_CHILDREN$1 );
                        }

                        return var10000;
                    };
                }
                else
                {
                    var3 = var2.apply( x1 );
                }

                return var3;
            }

            public final boolean isDefinedAt( final Object x1 )
            {
                boolean var3 = false;
                ValueHashJoin var4 = null;
                boolean var2;
                if ( x1 instanceof LogicalPlan )
                {
                    LogicalPlan var6 = (LogicalPlan) x1;
                    if ( var6.id() != this.planId$1 )
                    {
                        var2 = true;
                        return var2;
                    }
                }

                if ( x1 instanceof ValueHashJoin )
                {
                    var3 = true;
                    var4 = (ValueHashJoin) x1;
                    Equals var7 = var4.join();
                    if ( var7 != null && this.shouldAllocateLhs$1 )
                    {
                        var2 = true;
                        return var2;
                    }
                }

                if ( var3 )
                {
                    Equals var8 = var4.join();
                    if ( var8 != null && !this.shouldAllocateLhs$1 )
                    {
                        var2 = true;
                        return var2;
                    }
                }

                if ( x1 instanceof FindShortestPaths )
                {
                    var2 = true;
                }
                else if ( x1 instanceof ProjectEndpoints )
                {
                    var2 = true;
                }
                else if ( x1 instanceof ApplyPlan && !this.shouldAllocateLhs$1 )
                {
                    var2 = true;
                }
                else if ( x1 instanceof NestedPlanExpression )
                {
                    var2 = true;
                }
                else if ( x1 instanceof Expression )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                return var2;
            }
        }
    }

    private boolean allocateExpressionsInternal$default$5()
    {
        return true;
    }

    private void allocateLeaf( final LogicalPlan lp, final boolean nullable, final SlotConfiguration slots )
    {
        BoxedUnit var4;
        if ( lp instanceof IndexLeafPlan )
        {
            IndexLeafPlan var6 = (IndexLeafPlan) lp;
            slots.newLong( var6.idName(), nullable, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode());
            var6.cachedProperties().foreach( ( x$6 ) -> {
                return slots.newCachedProperty( x$6, slots.newCachedProperty$default$2() );
            } );
            var4 = BoxedUnit.UNIT;
        }
        else if ( lp instanceof NodeLogicalLeafPlan )
        {
            NodeLogicalLeafPlan var7 = (NodeLogicalLeafPlan) lp;
            slots.newLong( var7.idName(), nullable, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode());
            var4 = BoxedUnit.UNIT;
        }
        else if ( lp instanceof Argument )
        {
            var4 = BoxedUnit.UNIT;
        }
        else if ( lp instanceof DirectedRelationshipByIdSeek )
        {
            DirectedRelationshipByIdSeek var8 = (DirectedRelationshipByIdSeek) lp;
            slots.newLong( var8.idName(), nullable, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship());
            slots.newLong( var8.startNode(), nullable, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode());
            slots.newLong( var8.endNode(), nullable, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode());
            var4 = BoxedUnit.UNIT;
        }
        else if ( lp instanceof UndirectedRelationshipByIdSeek )
        {
            UndirectedRelationshipByIdSeek var9 = (UndirectedRelationshipByIdSeek) lp;
            slots.newLong( var9.idName(), nullable, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship());
            slots.newLong( var9.leftNode(), nullable, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode());
            slots.newLong( var9.rightNode(), nullable, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode());
            var4 = BoxedUnit.UNIT;
        }
        else if ( lp instanceof NodeCountFromCountStore )
        {
            NodeCountFromCountStore var10 = (NodeCountFromCountStore) lp;
            slots.newReference( var10.idName(), false, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTInteger());
            var4 = BoxedUnit.UNIT;
        }
        else if ( lp instanceof RelationshipCountFromCountStore )
        {
            RelationshipCountFromCountStore var11 = (RelationshipCountFromCountStore) lp;
            slots.newReference( var11.idName(), false, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTInteger());
            var4 = BoxedUnit.UNIT;
        }
        else
        {
            if ( !(lp instanceof Input) )
            {
                throw new SlotAllocationFailed( (new StringBuilder( 25 )).append( "Don't know how to handle " ).append( lp ).toString() );
            }

            Input var12 = (Input) lp;
            Seq nodes = var12.nodes();
            Seq relationships = var12.relationships();
            Seq variables = var12.variables();
            boolean nullableInput = var12.nullable();
            nodes.foreach( ( v ) -> {
                return slots.newLong( v, nullableInput, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode());
            } ); relationships.foreach( ( v ) -> {
            return slots.newLong( v, nullableInput, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship());
        } ); variables.foreach( ( v ) -> {
            return slots.newReference( v, nullableInput, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTAny());
        } ); var4 = BoxedUnit.UNIT;
        }
    }

    private void allocateOneChild( final LogicalPlan lp, final boolean nullable, final SlotConfiguration source, final SlotConfiguration slots,
            final Function1<LogicalPlan,BoxedUnit> recordArgument, final SemanticTable semanticTable )
    {
        boolean var12 = false;
        Expand var13 = null;
        boolean var14 = false;
        OptionalExpand var15 = null;
        boolean var16 = false;
        LoadCSV var17 = null;
        BoxedUnit var7;
        if ( lp instanceof Aggregation )
        {
            Aggregation var19 = (Aggregation) lp;
            Map groupingExpressions = var19.groupingExpressions();
            Map aggregationExpressions = var19.aggregationExpression();
            this.addGroupingSlots( groupingExpressions, source, slots );
            aggregationExpressions.foreach( ( x0$1 ) -> {
                if ( x0$1 != null )
                {
                    String key = (String) x0$1._1();
                    SlotConfiguration var2 = slots.newReference( key, true, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTAny());
                    return var2;
                }
                else
                {
                    throw new MatchError( x0$1 );
                }
            } ); var7 = BoxedUnit.UNIT;
        }
        else if ( lp instanceof OrderedAggregation )
        {
            OrderedAggregation var22 = (OrderedAggregation) lp;
            Map groupingExpressions = var22.groupingExpressions();
            Map aggregationExpressions = var22.aggregationExpression();
            this.addGroupingSlots( groupingExpressions, source, slots );
            aggregationExpressions.foreach( ( x0$2 ) -> {
                if ( x0$2 != null )
                {
                    String key = (String) x0$2._1();
                    SlotConfiguration var2 = slots.newReference( key, true, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTAny());
                    return var2;
                }
                else
                {
                    throw new MatchError( x0$2 );
                }
            } ); var7 = BoxedUnit.UNIT;
        }
        else
        {
            if ( lp instanceof Expand )
            {
                var12 = true;
                var13 = (Expand) lp;
                String to = var13.to();
                String relName = var13.relName();
                ExpansionMode var27 = var13.mode();
                if ( org.neo4j.cypher.internal.logical.plans.ExpandAll..MODULE$.equals( var27 )){
                slots.newLong( relName, nullable, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship());
                slots.newLong( to, nullable, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode());
                var7 = BoxedUnit.UNIT;
                return;
            }
            }

            if ( var12 )
            {
                String relName = var13.relName();
                ExpansionMode var29 = var13.mode();
                if ( org.neo4j.cypher.internal.logical.plans.ExpandInto..MODULE$.equals( var29 )){
                slots.newLong( relName, nullable, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship());
                var7 = BoxedUnit.UNIT;
                return;
            }
            }

            if ( lp instanceof Optional )
            {
                var7 = (BoxedUnit) recordArgument.apply( lp );
            }
            else
            {
                boolean var11;
                if ( lp instanceof ProduceResult )
                {
                    var11 = true;
                }
                else if ( lp instanceof Selection )
                {
                    var11 = true;
                }
                else if ( lp instanceof Limit )
                {
                    var11 = true;
                }
                else if ( lp instanceof Skip )
                {
                    var11 = true;
                }
                else if ( lp instanceof Sort )
                {
                    var11 = true;
                }
                else if ( lp instanceof PartialSort )
                {
                    var11 = true;
                }
                else if ( lp instanceof Top )
                {
                    var11 = true;
                }
                else if ( lp instanceof PartialTop )
                {
                    var11 = true;
                }
                else if ( lp instanceof CacheProperties )
                {
                    var11 = true;
                }
                else
                {
                    var11 = false;
                }

                if ( var11 )
                {
                    var7 = BoxedUnit.UNIT;
                }
                else if ( lp instanceof ProjectingPlan )
                {
                    ProjectingPlan var30 = (ProjectingPlan) lp;
                    var30.projectExpressions().foreach( ( x0$3 ) -> {
                        Object var2;
                        if ( x0$3 != null )
                        {
                            String keyx = (String) x0$3._1();
                            Expression var5 = (Expression) x0$3._2();
                            if ( var5 instanceof Variable )
                            {
                                label65:
                                {
                                    Variable var6 = (Variable) var5;
                                    String identx = var6.name();
                                    if ( keyx == null )
                                    {
                                        if ( identx != null )
                                        {
                                            break label65;
                                        }
                                    }
                                    else if ( !keyx.equals( identx ) )
                                    {
                                        break label65;
                                    }

                                    var2 = BoxedUnit.UNIT;
                                    return var2;
                                }
                            }
                        }

                        String newKey;
                        String ident;
                        label66:
                        {
                            if ( x0$3 != null )
                            {
                                newKey = (String) x0$3._1();
                                Expression var10 = (Expression) x0$3._2();
                                if ( var10 instanceof Variable )
                                {
                                    Variable var11 = (Variable) var10;
                                    ident = var11.name();
                                    if ( newKey == null )
                                    {
                                        if ( ident != null )
                                        {
                                            break label66;
                                        }
                                    }
                                    else if ( !newKey.equals( ident ) )
                                    {
                                        break label66;
                                    }
                                }
                            }

                            if ( x0$3 == null )
                            {
                                throw new MatchError( x0$3 );
                            }

                            String key = (String) x0$3._1();
                            var2 = slots.newReference( key, true, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTAny());
                            return var2;
                        }

                        var2 = slots.addAlias( newKey, ident );
                        return var2;
                    } ); var7 = BoxedUnit.UNIT;
                }
                else
                {
                    if ( lp instanceof OptionalExpand )
                    {
                        var14 = true;
                        var15 = (OptionalExpand) lp;
                        String to = var15.to();
                        String rel = var15.relName();
                        ExpansionMode var33 = var15.mode();
                        if ( org.neo4j.cypher.internal.logical.plans.ExpandAll..MODULE$.equals( var33 )){
                        slots.newLong( rel, true, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship());
                        slots.newLong( to, true, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode());
                        var7 = BoxedUnit.UNIT;
                        return;
                    }
                    }

                    if ( var14 )
                    {
                        String rel = var15.relName();
                        ExpansionMode var35 = var15.mode();
                        if ( org.neo4j.cypher.internal.logical.plans.ExpandInto..MODULE$.equals( var35 )){
                        slots.newLong( rel, true, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship());
                        var7 = BoxedUnit.UNIT;
                        return;
                    }
                    }

                    if ( lp instanceof VarExpand )
                    {
                        String relationship;
                        label247:
                        {
                            String to;
                            label246:
                            {
                                VarExpand var36 = (VarExpand) lp;
                                to = var36.to();
                                relationship = var36.relName();
                                ExpansionMode expansionMode = var36.mode();
                                org.neo4j.cypher.internal.logical.plans.ExpandAll.var40 = org.neo4j.cypher.internal.logical.plans.ExpandAll..MODULE$;
                                if ( expansionMode == null )
                                {
                                    if ( var40 == null )
                                    {
                                        break label246;
                                    }
                                }
                                else if ( expansionMode.equals( var40 ) )
                                {
                                    break label246;
                                }

                                BoxedUnit var10000 = BoxedUnit.UNIT;
                                break label247;
                            }

                            slots.newLong( to, nullable, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode());
                        }

                        slots.newReference( relationship, nullable, org.neo4j.cypher.internal.v4_0.util.symbols.package..
                        MODULE$.CTList( org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship()));
                        var7 = BoxedUnit.UNIT;
                    }
                    else if ( lp instanceof PruningVarExpand )
                    {
                        PruningVarExpand var41 = (PruningVarExpand) lp;
                        String to = var41.to();
                        slots.newLong( to, nullable, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode());
                        var7 = BoxedUnit.UNIT;
                    }
                    else if ( lp instanceof Create )
                    {
                        Create var43 = (Create) lp;
                        Seq nodes = var43.nodes();
                        Seq relationships = var43.relationships();
                        nodes.foreach( ( n ) -> {
                            return slots.newLong( n.idName(), false, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode());
                        } ); relationships.foreach( ( r ) -> {
                        return slots.newLong( r.idName(), false, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship());
                    } ); var7 = BoxedUnit.UNIT;
                    }
                    else if ( lp instanceof MergeCreateNode )
                    {
                        var7 = BoxedUnit.UNIT;
                    }
                    else if ( lp instanceof MergeCreateRelationship )
                    {
                        MergeCreateRelationship var46 = (MergeCreateRelationship) lp;
                        String name = var46.idName();
                        slots.newLong( name, false, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship());
                        var7 = BoxedUnit.UNIT;
                    }
                    else
                    {
                        boolean var10;
                        if ( lp instanceof EmptyResult )
                        {
                            var10 = true;
                        }
                        else if ( lp instanceof DropResult )
                        {
                            var10 = true;
                        }
                        else if ( lp instanceof ErrorPlan )
                        {
                            var10 = true;
                        }
                        else if ( lp instanceof Eager )
                        {
                            var10 = true;
                        }
                        else
                        {
                            var10 = false;
                        }

                        if ( var10 )
                        {
                            var7 = BoxedUnit.UNIT;
                        }
                        else if ( lp instanceof UnwindCollection )
                        {
                            UnwindCollection var48 = (UnwindCollection) lp;
                            String variable = var48.variable();
                            slots.newReference( variable, true, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTAny());
                            var7 = BoxedUnit.UNIT;
                        }
                        else
                        {
                            boolean var9;
                            if ( lp instanceof DeleteNode )
                            {
                                var9 = true;
                            }
                            else if ( lp instanceof DeleteRelationship )
                            {
                                var9 = true;
                            }
                            else if ( lp instanceof DeletePath )
                            {
                                var9 = true;
                            }
                            else if ( lp instanceof DeleteExpression )
                            {
                                var9 = true;
                            }
                            else if ( lp instanceof DetachDeleteNode )
                            {
                                var9 = true;
                            }
                            else if ( lp instanceof DetachDeletePath )
                            {
                                var9 = true;
                            }
                            else if ( lp instanceof DetachDeleteExpression )
                            {
                                var9 = true;
                            }
                            else
                            {
                                var9 = false;
                            }

                            if ( var9 )
                            {
                                var7 = BoxedUnit.UNIT;
                            }
                            else
                            {
                                boolean var8;
                                if ( lp instanceof SetLabels )
                                {
                                    var8 = true;
                                }
                                else if ( lp instanceof SetNodeProperty )
                                {
                                    var8 = true;
                                }
                                else if ( lp instanceof SetNodePropertiesFromMap )
                                {
                                    var8 = true;
                                }
                                else if ( lp instanceof SetRelationshipProperty )
                                {
                                    var8 = true;
                                }
                                else if ( lp instanceof SetRelationshipPropertiesFromMap )
                                {
                                    var8 = true;
                                }
                                else if ( lp instanceof SetProperty )
                                {
                                    var8 = true;
                                }
                                else if ( lp instanceof SetPropertiesFromMap )
                                {
                                    var8 = true;
                                }
                                else if ( lp instanceof RemoveLabels )
                                {
                                    var8 = true;
                                }
                                else
                                {
                                    var8 = false;
                                }

                                if ( var8 )
                                {
                                    var7 = BoxedUnit.UNIT;
                                }
                                else if ( lp instanceof LockNodes )
                                {
                                    var7 = BoxedUnit.UNIT;
                                }
                                else if ( lp instanceof ProjectEndpoints )
                                {
                                    var7 = BoxedUnit.UNIT;
                                }
                                else
                                {
                                    if ( lp instanceof LoadCSV )
                                    {
                                        var16 = true;
                                        var17 = (LoadCSV) lp;
                                        String variableName = var17.variableName();
                                        CSVFormat var51 = var17.format();
                                        if ( org.neo4j.cypher.internal.ir.NoHeaders..MODULE$.equals( var51 )){
                                        slots.newReference( variableName, nullable, org.neo4j.cypher.internal.v4_0.util.symbols.package..
                                        MODULE$.CTList( org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTAny()));
                                        var7 = BoxedUnit.UNIT;
                                        return;
                                    }
                                    }

                                    if ( var16 )
                                    {
                                        String variableName = var17.variableName();
                                        CSVFormat var53 = var17.format();
                                        if ( org.neo4j.cypher.internal.ir.HasHeaders..MODULE$.equals( var53 )){
                                        slots.newReference( variableName, nullable, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTMap());
                                        var7 = BoxedUnit.UNIT;
                                        return;
                                    }
                                    }

                                    if ( lp instanceof ProcedureCall )
                                    {
                                        ProcedureCall var54 = (ProcedureCall) lp;
                                        ResolvedCall var55 = var54.call();
                                        if ( var55 != null )
                                        {
                                            IndexedSeq callResults = var55.callResults();
                                            callResults.foreach( ( x0$4 ) -> {
                                                if ( x0$4 != null )
                                                {
                                                    LogicalVariable variable = x0$4.variable();
                                                    SlotConfiguration var2 =
                                                            slots.newReference( variable.name(), true, org.neo4j.cypher.internal.v4_0.util.symbols.package..
                                                    MODULE$.CTAny());
                                                    return var2;
                                                }
                                                else
                                                {
                                                    throw new MatchError( x0$4 );
                                                }
                                            } ); var7 = BoxedUnit.UNIT;
                                            return;
                                        }
                                    }

                                    if ( !(lp instanceof FindShortestPaths) )
                                    {
                                        throw new SlotAllocationFailed(
                                                (new StringBuilder( 25 )).append( "Don't know how to handle " ).append( lp ).toString() );
                                    }

                                    var7 = BoxedUnit.UNIT;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private SlotConfiguration allocateTwoChild( final LogicalPlan lp, final boolean nullable, final SlotConfiguration lhs, final SlotConfiguration rhs,
            final Function1<LogicalPlan,BoxedUnit> recordArgument, final SlotAllocation.SlotsAndArgument argument )
    {
        SlotConfiguration var7;
        if ( lp instanceof Apply )
        {
            var7 = rhs;
        }
        else if ( lp instanceof TriadicSelection )
        {
            var7 = rhs;
        }
        else
        {
            boolean var9;
            if ( lp instanceof AbstractSemiApply )
            {
                var9 = true;
            }
            else if ( lp instanceof AbstractSelectOrSemiApply )
            {
                var9 = true;
            }
            else
            {
                var9 = false;
            }

            if ( var9 )
            {
                var7 = lhs;
            }
            else
            {
                boolean var8;
                if ( lp instanceof AntiConditionalApply )
                {
                    var8 = true;
                }
                else if ( lp instanceof ConditionalApply )
                {
                    var8 = true;
                }
                else
                {
                    var8 = false;
                }

                if ( var8 )
                {
                    var7 = rhs;
                }
                else if ( lp instanceof LetSemiApply )
                {
                    LetSemiApply var11 = (LetSemiApply) lp;
                    String name = var11.idName();
                    lhs.newReference( name, false, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTBoolean());
                    var7 = lhs;
                }
                else if ( lp instanceof LetAntiSemiApply )
                {
                    LetAntiSemiApply var13 = (LetAntiSemiApply) lp;
                    String name = var13.idName();
                    lhs.newReference( name, false, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTBoolean());
                    var7 = lhs;
                }
                else if ( lp instanceof LetSelectOrSemiApply )
                {
                    LetSelectOrSemiApply var15 = (LetSelectOrSemiApply) lp;
                    String name = var15.idName();
                    lhs.newReference( name, false, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTBoolean());
                    var7 = lhs;
                }
                else if ( lp instanceof LetSelectOrAntiSemiApply )
                {
                    LetSelectOrAntiSemiApply var17 = (LetSelectOrAntiSemiApply) lp;
                    String name = var17.idName();
                    lhs.newReference( name, false, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTBoolean());
                    var7 = lhs;
                }
                else if ( lp instanceof CartesianProduct )
                {
                    recordArgument.apply( lp );
                    SlotConfiguration result = this.org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$breakingPolicy.invoke( lp, lhs,
                            argument.slotConfiguration() );
                    rhs.foreachSlotOrdered( ( key, slot ) -> {
                        $anonfun$allocateTwoChild$1( result, key, slot );
                        return BoxedUnit.UNIT;
                    }, ( p ) -> {
                        $anonfun$allocateTwoChild$2( result, p );
                        return BoxedUnit.UNIT;
                    }, ( applyPlan ) -> {
                        $anonfun$allocateTwoChild$3( result, ((Id) applyPlan).x() );
                        return BoxedUnit.UNIT;
                    }, argument.argumentSize() );
                    var7 = result;
                }
                else if ( lp instanceof RightOuterHashJoin )
                {
                    RightOuterHashJoin var20 = (RightOuterHashJoin) lp;
                    Set nodes = var20.nodes();
                    recordArgument.apply( lp );
                    SlotConfiguration result = this.org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$breakingPolicy.invoke( lp, rhs,
                            argument.slotConfiguration() );
                    lhs.foreachSlotOrdered( ( key, slot ) -> {
                        $anonfun$allocateTwoChild$4( nodes, result, key, slot );
                        return BoxedUnit.UNIT;
                    }, ( x$7 ) -> {
                        $anonfun$allocateTwoChild$5( result, x$7 );
                        return BoxedUnit.UNIT;
                    }, lhs.foreachSlotOrdered$default$3(), lhs.foreachSlotOrdered$default$4() );
                    var7 = result;
                }
                else if ( lp instanceof LeftOuterHashJoin )
                {
                    LeftOuterHashJoin var23 = (LeftOuterHashJoin) lp;
                    Set nodes = var23.nodes();
                    recordArgument.apply( lp );
                    SlotConfiguration result = this.org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$breakingPolicy.invoke( lp, lhs,
                            argument.slotConfiguration() );
                    rhs.foreachSlotOrdered( ( key, slot ) -> {
                        $anonfun$allocateTwoChild$6( nodes, result, key, slot );
                        return BoxedUnit.UNIT;
                    }, ( x$8 ) -> {
                        $anonfun$allocateTwoChild$7( result, x$8 );
                        return BoxedUnit.UNIT;
                    }, rhs.foreachSlotOrdered$default$3(), rhs.foreachSlotOrdered$default$4() );
                    var7 = result;
                }
                else if ( lp instanceof NodeHashJoin )
                {
                    NodeHashJoin var26 = (NodeHashJoin) lp;
                    Set nodes = var26.nodes();
                    recordArgument.apply( lp );
                    SlotConfiguration result = this.org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$breakingPolicy.invoke( lp, lhs,
                            argument.slotConfiguration() );
                    rhs.foreachSlotOrdered( ( key, slot ) -> {
                        $anonfun$allocateTwoChild$8( nodes, result, key, slot );
                        return BoxedUnit.UNIT;
                    }, ( x$9 ) -> {
                        $anonfun$allocateTwoChild$9( result, x$9 );
                        return BoxedUnit.UNIT;
                    }, rhs.foreachSlotOrdered$default$3(), rhs.foreachSlotOrdered$default$4() );
                    var7 = result;
                }
                else if ( lp instanceof ValueHashJoin )
                {
                    recordArgument.apply( lp );
                    SlotConfiguration result = this.org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$breakingPolicy.invoke( lp, lhs,
                            argument.slotConfiguration() );
                    Function2 x$13 = ( key, slot ) -> {
                        $anonfun$allocateTwoChild$10( result, key, slot );
                        return BoxedUnit.UNIT;
                    };
                    Function1 x$14 = ( p ) -> {
                        $anonfun$allocateTwoChild$11( result, p );
                        return BoxedUnit.UNIT;
                    };
                    SlotConfiguration.Size x$15 = argument.argumentSize();
                    Function1 x$16 = rhs.foreachSlotOrdered$default$3();
                    rhs.foreachSlotOrdered( x$13, x$14, x$16, x$15 );
                    var7 = result;
                }
                else if ( lp instanceof RollUpApply )
                {
                    RollUpApply var34 = (RollUpApply) lp;
                    String collectionName = var34.collectionName();
                    lhs.newReference( collectionName, nullable, org.neo4j.cypher.internal.v4_0.util.symbols.package..
                    MODULE$.CTList( org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTAny()));
                    var7 = lhs;
                }
                else if ( lp instanceof ForeachApply )
                {
                    var7 = lhs;
                }
                else if ( lp instanceof Union )
                {
                    SlotConfiguration result = lhs.emptyUnderSameApply();
                    lhs.foreachSlot( ( x0$5 ) -> {
                        $anonfun$allocateTwoChild$12( rhs, result, x0$5 );
                        return BoxedUnit.UNIT;
                    }, ( x0$7 ) -> {
                        $anonfun$allocateTwoChild$15( rhs, result, x0$7 );
                        return BoxedUnit.UNIT;
                    } );
                    var7 = result;
                }
                else
                {
                    if ( !(lp instanceof AssertSameNode) )
                    {
                        throw new SlotAllocationFailed( (new StringBuilder( 25 )).append( "Don't know how to handle " ).append( lp ).toString() );
                    }

                    var7 = lhs;
                }
            }
        }

        return var7;
    }

    private void allocateLhsOfApply( final LogicalPlan plan, final boolean nullable, final SlotConfiguration lhs, final SemanticTable semanticTable )
    {
        BoxedUnit var5;
        if ( plan instanceof ForeachApply )
        {
            label34:
            {
                ForeachApply var8 = (ForeachApply) plan;
                String variableName = var8.variable();
                Expression listExpression = var8.expression();
                Option maybeTypeSpec = scala.util.Try..MODULE$.apply( () -> {
                return semanticTable.getActualTypeFor( listExpression );
            } ).toOption();
                boolean listOfNodes = maybeTypeSpec.exists( ( x$10 ) -> {
                    return BoxesRunTime.boxToBoolean( $anonfun$allocateLhsOfApply$2( x$10 ) );
                } );
                boolean listOfRels = maybeTypeSpec.exists( ( x$11 ) -> {
                    return BoxesRunTime.boxToBoolean( $anonfun$allocateLhsOfApply$3( x$11 ) );
                } );
                sp var14 = new sp( listOfNodes, listOfRels );
                BoxedUnit var6;
                if ( var14 != null )
                {
                    boolean var15 = var14._1$mcZ$sp();
                    boolean var16 = var14._2$mcZ$sp();
                    if ( var15 && !var16 )
                    {
                        lhs.newLong( variableName, true, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode());
                        var6 = BoxedUnit.UNIT;
                        break label34;
                    }
                }

                if ( var14 != null )
                {
                    boolean var17 = var14._1$mcZ$sp();
                    boolean var18 = var14._2$mcZ$sp();
                    if ( !var17 && var18 )
                    {
                        lhs.newLong( variableName, true, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship());
                        var6 = BoxedUnit.UNIT;
                        break label34;
                    }
                }

                lhs.newReference( variableName, true, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTAny());
                var6 = BoxedUnit.UNIT;
            }

            var5 = BoxedUnit.UNIT;
        }
        else
        {
            var5 = BoxedUnit.UNIT;
        }
    }

    private void addGroupingSlots( final Map<String,Expression> groupingExpressions, final SlotConfiguration incoming, final SlotConfiguration outgoing )
    {
        groupingExpressions.foreach( ( x0$8 ) -> {
            SlotConfiguration var3;
            if ( x0$8 != null )
            {
                String keyx = (String) x0$8._1();
                Expression var8 = (Expression) x0$8._2();
                if ( var8 instanceof Variable )
                {
                    boolean var5;
                    Slot slotInfo;
                    label48:
                    {
                        label57:
                        {
                            Variable var9 = (Variable) var8;
                            String ident = var9.name();
                            slotInfo = incoming.apply( ident );
                            CypherType var12 = slotInfo.typ();
                            NodeType var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                            if ( var10000 == null )
                            {
                                if ( var12 == null )
                                {
                                    break label57;
                                }
                            }
                            else if ( var10000.equals( var12 ) )
                            {
                                break label57;
                            }

                            label41:
                            {
                                RelationshipType var16 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                                if ( var16 == null )
                                {
                                    if ( var12 == null )
                                    {
                                        break label41;
                                    }
                                }
                                else if ( var16.equals( var12 ) )
                                {
                                    break label41;
                                }

                                var5 = false;
                                break label48;
                            }

                            var5 = true;
                            break label48;
                        }

                        var5 = true;
                    }

                    SlotConfiguration var4;
                    if ( var5 )
                    {
                        var4 = outgoing.newLong( keyx, slotInfo.nullable(), slotInfo.typ() );
                    }
                    else
                    {
                        var4 = outgoing.newReference( keyx, slotInfo.nullable(), slotInfo.typ() );
                    }

                    var3 = var4;
                    return var3;
                }
            }

            if ( x0$8 == null )
            {
                throw new MatchError( x0$8 );
            }
            else
            {
                String key = (String) x0$8._1();
                var3 = outgoing.newReference( key, true, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTAny());
                return var3;
            }
        } );
    }

    public Object org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$allocateShortestPathPattern(
            final ShortestPathPattern shortestPathPattern, final SlotConfiguration slots, final boolean nullable )
    {
        Option maybePathName = shortestPathPattern.name();
        ShortestPaths part = shortestPathPattern.expr();
        String pathName = (String) maybePathName.getOrElse( () -> {
            return org.neo4j.cypher.internal.v4_0.util.UnNamedNameGenerator..MODULE$.name( part.position() );
        } ); PatternElement var9 = part.element();
        if ( var9 instanceof RelationshipChain )
        {
            RelationshipChain var10 = (RelationshipChain) var9;
            RelationshipPattern relationshipPattern = var10.relationship();
            Option relIteratorName = relationshipPattern.variable().map( ( x$12 ) -> {
                return x$12.name();
            } );
            slots.newReference( pathName, nullable, org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTPath());
            return relIteratorName.isDefined() ? slots.newReference( (String) relIteratorName.get(), nullable, org.neo4j.cypher.internal.v4_0.util.symbols.package..
            MODULE$.CTList( org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship())) :BoxedUnit.UNIT;
        }
        else
        {
            throw new IllegalStateException( "This should be caught during semantic checking" );
        }
    }

    private final void recordArgument$1( final LogicalPlan plan, final SlotAllocation.SlotsAndArgument argument )
    {
        this.argumentSizes().set( plan.id(), argument.argumentSize() );
    }

    public final Accumulator$4$ org$neo4j$cypher$internal$physicalplanning$SingleQuerySlotAllocator$$Accumulator$2( final LazyRef Accumulator$module$1 )
    {
        return Accumulator$module$1.initialized() ? (Accumulator$4$) Accumulator$module$1.value() : this.Accumulator$lzycompute$1( Accumulator$module$1 );
    }
}
