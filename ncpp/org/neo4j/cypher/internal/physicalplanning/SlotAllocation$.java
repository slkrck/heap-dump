package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.runtime.expressionVariableAllocation.AvailableExpressionVariables;
import org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticTable;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id.;
import scala.runtime.BoxedUnit;

public final class SlotAllocation$
{
    public static SlotAllocation$ MODULE$;

    static
    {
        new SlotAllocation$();
    }

    private final SlotConfiguration INITIAL_SLOT_CONFIGURATION;

    private SlotAllocation$()
    {
        MODULE$ = this;
        this.INITIAL_SLOT_CONFIGURATION = this.NO_ARGUMENT( true ).slotConfiguration();
    }

    public SlotAllocation.SlotsAndArgument NO_ARGUMENT( final boolean allocateArgumentSlots )
    {
        SlotConfiguration slots = SlotConfiguration$.MODULE$.empty();
        if ( allocateArgumentSlots )
        {
            slots.newArgument(.MODULE$.INVALID_ID());
        }
        else
        {
            BoxedUnit var10000 = BoxedUnit.UNIT;
        }

        return new SlotAllocation.SlotsAndArgument( slots, SlotConfiguration.Size$.MODULE$.zero(),.MODULE$.INVALID_ID());
    }

    public SlotConfiguration INITIAL_SLOT_CONFIGURATION()
    {
        return this.INITIAL_SLOT_CONFIGURATION;
    }

    public SlotAllocation.SlotMetaData allocateSlots( final LogicalPlan lp, final SemanticTable semanticTable, final PipelineBreakingPolicy breakingPolicy,
            final AvailableExpressionVariables availableExpressionVariables, final boolean allocateArgumentSlots )
    {
        return (new SingleQuerySlotAllocator( allocateArgumentSlots, breakingPolicy, availableExpressionVariables )).allocateSlots( lp, semanticTable,
                scala.None..MODULE$);
    }

    public boolean allocateSlots$default$5()
    {
        return false;
    }
}
