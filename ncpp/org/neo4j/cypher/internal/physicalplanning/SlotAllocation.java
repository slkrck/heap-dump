package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.runtime.expressionVariableAllocation.AvailableExpressionVariables;
import org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticTable;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.Tuple4;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.AbstractFunction3;
import scala.runtime.AbstractFunction4;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public final class SlotAllocation
{
    public static boolean allocateSlots$default$5()
    {
        return SlotAllocation$.MODULE$.allocateSlots$default$5();
    }

    public static SlotAllocation.SlotMetaData allocateSlots( final LogicalPlan lp, final SemanticTable semanticTable,
            final PipelineBreakingPolicy breakingPolicy, final AvailableExpressionVariables availableExpressionVariables, final boolean allocateArgumentSlots )
    {
        return SlotAllocation$.MODULE$.allocateSlots( var0, var1, var2, var3, var4 );
    }

    public static SlotConfiguration INITIAL_SLOT_CONFIGURATION()
    {
        return SlotAllocation$.MODULE$.INITIAL_SLOT_CONFIGURATION();
    }

    public static class SlotMetaData implements Product, Serializable
    {
        private final PhysicalPlanningAttributes.SlotConfigurations slotConfigurations;
        private final PhysicalPlanningAttributes.ArgumentSizes argumentSizes;
        private final PhysicalPlanningAttributes.ApplyPlans applyPlans;
        private final PhysicalPlanningAttributes.NestedPlanArgumentConfigurations nestedPlanArgumentConfigurations;

        public SlotMetaData( final PhysicalPlanningAttributes.SlotConfigurations slotConfigurations,
                final PhysicalPlanningAttributes.ArgumentSizes argumentSizes, final PhysicalPlanningAttributes.ApplyPlans applyPlans,
                final PhysicalPlanningAttributes.NestedPlanArgumentConfigurations nestedPlanArgumentConfigurations )
        {
            this.slotConfigurations = slotConfigurations;
            this.argumentSizes = argumentSizes;
            this.applyPlans = applyPlans;
            this.nestedPlanArgumentConfigurations = nestedPlanArgumentConfigurations;
            Product.$init$( this );
        }

        public PhysicalPlanningAttributes.SlotConfigurations slotConfigurations()
        {
            return this.slotConfigurations;
        }

        public PhysicalPlanningAttributes.ArgumentSizes argumentSizes()
        {
            return this.argumentSizes;
        }

        public PhysicalPlanningAttributes.ApplyPlans applyPlans()
        {
            return this.applyPlans;
        }

        public PhysicalPlanningAttributes.NestedPlanArgumentConfigurations nestedPlanArgumentConfigurations()
        {
            return this.nestedPlanArgumentConfigurations;
        }

        public SlotAllocation.SlotMetaData copy( final PhysicalPlanningAttributes.SlotConfigurations slotConfigurations,
                final PhysicalPlanningAttributes.ArgumentSizes argumentSizes, final PhysicalPlanningAttributes.ApplyPlans applyPlans,
                final PhysicalPlanningAttributes.NestedPlanArgumentConfigurations nestedPlanArgumentConfigurations )
        {
            return new SlotAllocation.SlotMetaData( slotConfigurations, argumentSizes, applyPlans, nestedPlanArgumentConfigurations );
        }

        public PhysicalPlanningAttributes.SlotConfigurations copy$default$1()
        {
            return this.slotConfigurations();
        }

        public PhysicalPlanningAttributes.ArgumentSizes copy$default$2()
        {
            return this.argumentSizes();
        }

        public PhysicalPlanningAttributes.ApplyPlans copy$default$3()
        {
            return this.applyPlans();
        }

        public PhysicalPlanningAttributes.NestedPlanArgumentConfigurations copy$default$4()
        {
            return this.nestedPlanArgumentConfigurations();
        }

        public String productPrefix()
        {
            return "SlotMetaData";
        }

        public int productArity()
        {
            return 4;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.slotConfigurations();
                break;
            case 1:
                var10000 = this.argumentSizes();
                break;
            case 2:
                var10000 = this.applyPlans();
                break;
            case 3:
                var10000 = this.nestedPlanArgumentConfigurations();
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof SlotAllocation.SlotMetaData;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var12;
            if ( this != x$1 )
            {
                label81:
                {
                    boolean var2;
                    if ( x$1 instanceof SlotAllocation.SlotMetaData )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label63:
                        {
                            label72:
                            {
                                SlotAllocation.SlotMetaData var4 = (SlotAllocation.SlotMetaData) x$1;
                                PhysicalPlanningAttributes.SlotConfigurations var10000 = this.slotConfigurations();
                                PhysicalPlanningAttributes.SlotConfigurations var5 = var4.slotConfigurations();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label72;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label72;
                                }

                                PhysicalPlanningAttributes.ArgumentSizes var9 = this.argumentSizes();
                                PhysicalPlanningAttributes.ArgumentSizes var6 = var4.argumentSizes();
                                if ( var9 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label72;
                                    }
                                }
                                else if ( !var9.equals( var6 ) )
                                {
                                    break label72;
                                }

                                PhysicalPlanningAttributes.ApplyPlans var10 = this.applyPlans();
                                PhysicalPlanningAttributes.ApplyPlans var7 = var4.applyPlans();
                                if ( var10 == null )
                                {
                                    if ( var7 != null )
                                    {
                                        break label72;
                                    }
                                }
                                else if ( !var10.equals( var7 ) )
                                {
                                    break label72;
                                }

                                PhysicalPlanningAttributes.NestedPlanArgumentConfigurations var11 = this.nestedPlanArgumentConfigurations();
                                PhysicalPlanningAttributes.NestedPlanArgumentConfigurations var8 = var4.nestedPlanArgumentConfigurations();
                                if ( var11 == null )
                                {
                                    if ( var8 != null )
                                    {
                                        break label72;
                                    }
                                }
                                else if ( !var11.equals( var8 ) )
                                {
                                    break label72;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var12 = true;
                                    break label63;
                                }
                            }

                            var12 = false;
                        }

                        if ( var12 )
                        {
                            break label81;
                        }
                    }

                    var12 = false;
                    return var12;
                }
            }

            var12 = true;
            return var12;
        }
    }

    public static class SlotMetaData$ extends
            AbstractFunction4<PhysicalPlanningAttributes.SlotConfigurations,PhysicalPlanningAttributes.ArgumentSizes,PhysicalPlanningAttributes.ApplyPlans,PhysicalPlanningAttributes.NestedPlanArgumentConfigurations,SlotAllocation.SlotMetaData>
            implements Serializable
    {
        public static SlotAllocation.SlotMetaData$ MODULE$;

        static
        {
            new SlotAllocation.SlotMetaData$();
        }

        public SlotMetaData$()
        {
            MODULE$ = this;
        }

        public final String toString()
        {
            return "SlotMetaData";
        }

        public SlotAllocation.SlotMetaData apply( final PhysicalPlanningAttributes.SlotConfigurations slotConfigurations,
                final PhysicalPlanningAttributes.ArgumentSizes argumentSizes, final PhysicalPlanningAttributes.ApplyPlans applyPlans,
                final PhysicalPlanningAttributes.NestedPlanArgumentConfigurations nestedPlanArgumentConfigurations )
        {
            return new SlotAllocation.SlotMetaData( slotConfigurations, argumentSizes, applyPlans, nestedPlanArgumentConfigurations );
        }

        public Option<Tuple4<PhysicalPlanningAttributes.SlotConfigurations,PhysicalPlanningAttributes.ArgumentSizes,PhysicalPlanningAttributes.ApplyPlans,PhysicalPlanningAttributes.NestedPlanArgumentConfigurations>> unapply(
                final SlotAllocation.SlotMetaData x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :
            new Some( new Tuple4( x$0.slotConfigurations(), x$0.argumentSizes(), x$0.applyPlans(), x$0.nestedPlanArgumentConfigurations() ) ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }

    public static class SlotsAndArgument implements Product, Serializable
    {
        private final SlotConfiguration slotConfiguration;
        private final SlotConfiguration.Size argumentSize;
        private final int argumentPlan;

        public SlotsAndArgument( final SlotConfiguration slotConfiguration, final SlotConfiguration.Size argumentSize, final int argumentPlan )
        {
            this.slotConfiguration = slotConfiguration;
            this.argumentSize = argumentSize;
            this.argumentPlan = argumentPlan;
            Product.$init$( this );
        }

        public SlotConfiguration slotConfiguration()
        {
            return this.slotConfiguration;
        }

        public SlotConfiguration.Size argumentSize()
        {
            return this.argumentSize;
        }

        public int argumentPlan()
        {
            return this.argumentPlan;
        }

        public SlotAllocation.SlotsAndArgument copy( final SlotConfiguration slotConfiguration, final SlotConfiguration.Size argumentSize,
                final int argumentPlan )
        {
            return new SlotAllocation.SlotsAndArgument( slotConfiguration, argumentSize, argumentPlan );
        }

        public SlotConfiguration copy$default$1()
        {
            return this.slotConfiguration();
        }

        public SlotConfiguration.Size copy$default$2()
        {
            return this.argumentSize();
        }

        public int copy$default$3()
        {
            return this.argumentPlan();
        }

        public String productPrefix()
        {
            return "SlotsAndArgument";
        }

        public int productArity()
        {
            return 3;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = this.slotConfiguration();
                break;
            case 1:
                var10000 = this.argumentSize();
                break;
            case 2:
                var10000 = new Id( this.argumentPlan() );
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof SlotAllocation.SlotsAndArgument;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var8;
            if ( this != x$1 )
            {
                label65:
                {
                    boolean var2;
                    if ( x$1 instanceof SlotAllocation.SlotsAndArgument )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        label47:
                        {
                            label56:
                            {
                                SlotAllocation.SlotsAndArgument var4 = (SlotAllocation.SlotsAndArgument) x$1;
                                SlotConfiguration var10000 = this.slotConfiguration();
                                SlotConfiguration var5 = var4.slotConfiguration();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label56;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label56;
                                }

                                SlotConfiguration.Size var7 = this.argumentSize();
                                SlotConfiguration.Size var6 = var4.argumentSize();
                                if ( var7 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label56;
                                    }
                                }
                                else if ( !var7.equals( var6 ) )
                                {
                                    break label56;
                                }

                                if ( this.argumentPlan() == var4.argumentPlan() && var4.canEqual( this ) )
                                {
                                    var8 = true;
                                    break label47;
                                }
                            }

                            var8 = false;
                        }

                        if ( var8 )
                        {
                            break label65;
                        }
                    }

                    var8 = false;
                    return var8;
                }
            }

            var8 = true;
            return var8;
        }
    }

    public static class SlotsAndArgument$ extends AbstractFunction3<SlotConfiguration,SlotConfiguration.Size,Id,SlotAllocation.SlotsAndArgument>
            implements Serializable
    {
        public static SlotAllocation.SlotsAndArgument$ MODULE$;

        static
        {
            new SlotAllocation.SlotsAndArgument$();
        }

        public SlotsAndArgument$()
        {
            MODULE$ = this;
        }

        public final String toString()
        {
            return "SlotsAndArgument";
        }

        public SlotAllocation.SlotsAndArgument apply( final SlotConfiguration slotConfiguration, final SlotConfiguration.Size argumentSize,
                final int argumentPlan )
        {
            return new SlotAllocation.SlotsAndArgument( slotConfiguration, argumentSize, argumentPlan );
        }

        public Option<Tuple3<SlotConfiguration,SlotConfiguration.Size,Id>> unapply( final SlotAllocation.SlotsAndArgument x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :
            new Some( new Tuple3( x$0.slotConfiguration(), x$0.argumentSize(), new Id( x$0.argumentPlan() ) ) ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }
}
