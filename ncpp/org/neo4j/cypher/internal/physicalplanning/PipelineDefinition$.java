package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple9;
import scala.None.;
import scala.collection.IndexedSeq;
import scala.runtime.AbstractFunction9;
import scala.runtime.BoxesRunTime;

public final class PipelineDefinition$ extends
        AbstractFunction9<PipelineId,PipelineId,PipelineId,LogicalPlan,IndexedSeq<LogicalPlan>,BufferDefinition,OutputDefinition,IndexedSeq<LogicalPlan>,Object,PipelineDefinition>
        implements Serializable
{
    public static PipelineDefinition$ MODULE$;

    static
    {
        new PipelineDefinition$();
    }

    private PipelineDefinition$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "PipelineDefinition";
    }

    public PipelineDefinition apply( final int id, final int lhs, final int rhs, final LogicalPlan headPlan, final IndexedSeq<LogicalPlan> fusedPlans,
            final BufferDefinition inputBuffer, final OutputDefinition outputDefinition, final IndexedSeq<LogicalPlan> middlePlans, final boolean serial )
    {
        return new PipelineDefinition( id, lhs, rhs, headPlan, fusedPlans, inputBuffer, outputDefinition, middlePlans, serial );
    }

    public Option<Tuple9<PipelineId,PipelineId,PipelineId,LogicalPlan,IndexedSeq<LogicalPlan>,BufferDefinition,OutputDefinition,IndexedSeq<LogicalPlan>,Object>> unapply(
            final PipelineDefinition x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some(
            new Tuple9( new PipelineId( x$0.id() ), new PipelineId( x$0.lhs() ), new PipelineId( x$0.rhs() ), x$0.headPlan(), x$0.fusedPlans(),
                    x$0.inputBuffer(), x$0.outputDefinition(), x$0.middlePlans(), BoxesRunTime.boxToBoolean( x$0.serial() ) ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
