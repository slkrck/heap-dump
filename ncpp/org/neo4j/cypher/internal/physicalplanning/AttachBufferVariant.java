package org.neo4j.cypher.internal.physicalplanning;

import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple4;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class AttachBufferVariant implements BufferVariant, Product, Serializable
{
    private final BufferDefinition applyBuffer;
    private final SlotConfiguration outputSlots;
    private final int argumentSlotOffset;
    private final SlotConfiguration.Size argumentSize;

    public AttachBufferVariant( final BufferDefinition applyBuffer, final SlotConfiguration outputSlots, final int argumentSlotOffset,
            final SlotConfiguration.Size argumentSize )
    {
        this.applyBuffer = applyBuffer;
        this.outputSlots = outputSlots;
        this.argumentSlotOffset = argumentSlotOffset;
        this.argumentSize = argumentSize;
        Product.$init$( this );
    }

    public static Option<Tuple4<BufferDefinition,SlotConfiguration,Object,SlotConfiguration.Size>> unapply( final AttachBufferVariant x$0 )
    {
        return AttachBufferVariant$.MODULE$.unapply( var0 );
    }

    public static AttachBufferVariant apply( final BufferDefinition applyBuffer, final SlotConfiguration outputSlots, final int argumentSlotOffset,
            final SlotConfiguration.Size argumentSize )
    {
        return AttachBufferVariant$.MODULE$.apply( var0, var1, var2, var3 );
    }

    public static Function1<Tuple4<BufferDefinition,SlotConfiguration,Object,SlotConfiguration.Size>,AttachBufferVariant> tupled()
    {
        return AttachBufferVariant$.MODULE$.tupled();
    }

    public static Function1<BufferDefinition,Function1<SlotConfiguration,Function1<Object,Function1<SlotConfiguration.Size,AttachBufferVariant>>>> curried()
    {
        return AttachBufferVariant$.MODULE$.curried();
    }

    public BufferDefinition applyBuffer()
    {
        return this.applyBuffer;
    }

    public SlotConfiguration outputSlots()
    {
        return this.outputSlots;
    }

    public int argumentSlotOffset()
    {
        return this.argumentSlotOffset;
    }

    public SlotConfiguration.Size argumentSize()
    {
        return this.argumentSize;
    }

    public AttachBufferVariant copy( final BufferDefinition applyBuffer, final SlotConfiguration outputSlots, final int argumentSlotOffset,
            final SlotConfiguration.Size argumentSize )
    {
        return new AttachBufferVariant( applyBuffer, outputSlots, argumentSlotOffset, argumentSize );
    }

    public BufferDefinition copy$default$1()
    {
        return this.applyBuffer();
    }

    public SlotConfiguration copy$default$2()
    {
        return this.outputSlots();
    }

    public int copy$default$3()
    {
        return this.argumentSlotOffset();
    }

    public SlotConfiguration.Size copy$default$4()
    {
        return this.argumentSize();
    }

    public String productPrefix()
    {
        return "AttachBufferVariant";
    }

    public int productArity()
    {
        return 4;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.applyBuffer();
            break;
        case 1:
            var10000 = this.outputSlots();
            break;
        case 2:
            var10000 = BoxesRunTime.boxToInteger( this.argumentSlotOffset() );
            break;
        case 3:
            var10000 = this.argumentSize();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof AttachBufferVariant;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.anyHash( this.applyBuffer() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.outputSlots() ) );
        var1 = Statics.mix( var1, this.argumentSlotOffset() );
        var1 = Statics.mix( var1, Statics.anyHash( this.argumentSize() ) );
        return Statics.finalizeHash( var1, 4 );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var10;
        if ( this != x$1 )
        {
            label74:
            {
                boolean var2;
                if ( x$1 instanceof AttachBufferVariant )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label56:
                    {
                        label65:
                        {
                            AttachBufferVariant var4 = (AttachBufferVariant) x$1;
                            BufferDefinition var10000 = this.applyBuffer();
                            BufferDefinition var5 = var4.applyBuffer();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label65;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label65;
                            }

                            SlotConfiguration var8 = this.outputSlots();
                            SlotConfiguration var6 = var4.outputSlots();
                            if ( var8 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label65;
                                }
                            }
                            else if ( !var8.equals( var6 ) )
                            {
                                break label65;
                            }

                            if ( this.argumentSlotOffset() == var4.argumentSlotOffset() )
                            {
                                label44:
                                {
                                    SlotConfiguration.Size var9 = this.argumentSize();
                                    SlotConfiguration.Size var7 = var4.argumentSize();
                                    if ( var9 == null )
                                    {
                                        if ( var7 != null )
                                        {
                                            break label44;
                                        }
                                    }
                                    else if ( !var9.equals( var7 ) )
                                    {
                                        break label44;
                                    }

                                    if ( var4.canEqual( this ) )
                                    {
                                        var10 = true;
                                        break label56;
                                    }
                                }
                            }
                        }

                        var10 = false;
                    }

                    if ( var10 )
                    {
                        break label74;
                    }
                }

                var10 = false;
                return var10;
            }
        }

        var10 = true;
        return var10;
    }
}
