package org.neo4j.cypher.internal.physicalplanning;

import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.collection.immutable.Set;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class RefSlotWithAliases implements SlotWithAliases, Product, Serializable
{
    private final RefSlot slot;
    private final Set<String> aliases;

    public RefSlotWithAliases( final RefSlot slot, final Set<String> aliases )
    {
        this.slot = slot;
        this.aliases = aliases;
        SlotWithAliases.$init$( this );
        Product.$init$( this );
    }

    public static Option<Tuple2<RefSlot,Set<String>>> unapply( final RefSlotWithAliases x$0 )
    {
        return RefSlotWithAliases$.MODULE$.unapply( var0 );
    }

    public static RefSlotWithAliases apply( final RefSlot slot, final Set<String> aliases )
    {
        return RefSlotWithAliases$.MODULE$.apply( var0, var1 );
    }

    public static Function1<Tuple2<RefSlot,Set<String>>,RefSlotWithAliases> tupled()
    {
        return RefSlotWithAliases$.MODULE$.tupled();
    }

    public static Function1<RefSlot,Function1<Set<String>,RefSlotWithAliases>> curried()
    {
        return RefSlotWithAliases$.MODULE$.curried();
    }

    public String makeString()
    {
        return SlotWithAliases.makeString$( this );
    }

    public RefSlot slot()
    {
        return this.slot;
    }

    public Set<String> aliases()
    {
        return this.aliases;
    }

    public String toString()
    {
        return this.makeString();
    }

    public RefSlotWithAliases copy( final RefSlot slot, final Set<String> aliases )
    {
        return new RefSlotWithAliases( slot, aliases );
    }

    public RefSlot copy$default$1()
    {
        return this.slot();
    }

    public Set<String> copy$default$2()
    {
        return this.aliases();
    }

    public String productPrefix()
    {
        return "RefSlotWithAliases";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.slot();
            break;
        case 1:
            var10000 = this.aliases();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof RefSlotWithAliases;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var8;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof RefSlotWithAliases )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            RefSlotWithAliases var4 = (RefSlotWithAliases) x$1;
                            RefSlot var10000 = this.slot();
                            RefSlot var5 = var4.slot();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            Set var7 = this.aliases();
                            Set var6 = var4.aliases();
                            if ( var7 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var7.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var8 = true;
                                break label45;
                            }
                        }

                        var8 = false;
                    }

                    if ( var8 )
                    {
                        break label63;
                    }
                }

                var8 = false;
                return var8;
            }
        }

        var8 = true;
        return var8;
    }
}
