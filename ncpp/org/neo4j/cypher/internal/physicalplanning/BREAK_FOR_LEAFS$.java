package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.logical.plans.LogicalLeafPlan;
import org.neo4j.cypher.internal.logical.plans.LogicalPlan;

public final class BREAK_FOR_LEAFS$ implements PipelineBreakingPolicy
{
    public static BREAK_FOR_LEAFS$ MODULE$;

    static
    {
        new BREAK_FOR_LEAFS$();
    }

    private BREAK_FOR_LEAFS$()
    {
        MODULE$ = this;
        PipelineBreakingPolicy.$init$( this );
    }

    public SlotConfiguration invoke( final LogicalPlan lp, final SlotConfiguration slots, final SlotConfiguration argumentSlots )
    {
        return PipelineBreakingPolicy.invoke$( this, lp, slots, argumentSlots );
    }

    public boolean breakOn( final LogicalPlan lp )
    {
        return lp instanceof LogicalLeafPlan;
    }

    public void onNestedPlanBreak()
    {
    }
}
