package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.exceptions.InternalException;
import scala.MatchError;
import scala.Option;
import scala.Some;
import scala.Tuple2;
import scala.Predef.;
import scala.collection.mutable.Stack;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxedUnit;
import scala.runtime.ObjectRef;

@JavaDocToJava
public interface TreeBuilder<T, ARGUMENT>
{
    static void $init$( final TreeBuilder $this )
    {
    }

    ARGUMENT initialArgument( final LogicalPlan leftLeaf );

    T onLeaf( final LogicalPlan plan, final ARGUMENT argument );

    T onOneChildPlan( final LogicalPlan plan, final T source, final ARGUMENT argument );

    ARGUMENT onTwoChildPlanComingFromLeft( final LogicalPlan plan, final T lhs, final ARGUMENT argument );

    T onTwoChildPlanComingFromRight( final LogicalPlan plan, final T lhs, final T rhs, final ARGUMENT argument );

    void validatePlan( final LogicalPlan plan );

    default T build( final LogicalPlan plan )
    {
        Stack planStack = new Stack();
        Stack outputStack = new Stack();
        Stack argumentStack = new Stack();
        ObjectRef comingFrom = ObjectRef.create( plan );
        this.populate$1( plan, planStack, comingFrom );
        argumentStack.push( this.initialArgument( (LogicalPlan) planStack.top() ) );

        Tuple2 var9;
        while ( true )
        {
            if ( !planStack.nonEmpty() )
            {
                Object result = outputStack.pop();
            .MODULE$. assert (outputStack.isEmpty(),() -> {
                return "Should have emptied the stack of output by now!";
            });
                return result;
            }

            LogicalPlan current;
            label85:
            {
                current = (LogicalPlan) planStack.pop();
                Object argument = argumentStack.top();
                var9 = new Tuple2( current.lhs(), current.rhs() );
                Stack var2;
                if ( var9 != null )
                {
                    Option var10 = (Option) var9._1();
                    Option var11 = (Option) var9._2();
                    if ( scala.None..MODULE$.equals( var10 ) && scala.None..MODULE$.equals( var11 )){
                    Object output = this.onLeaf( current, argument );
                    var2 = outputStack.push( output );
                    break label85;
                }
                }

                if ( var9 != null )
                {
                    Option var13 = (Option) var9._1();
                    Option var14 = (Option) var9._2();
                    if ( var13 instanceof Some && scala.None..MODULE$.equals( var14 )){
                    Object source = outputStack.pop();
                    Object output = this.onOneChildPlan( current, source, argument );
                    var2 = outputStack.push( output );
                    break label85;
                }
                }

                if ( var9 != null )
                {
                    Option var17 = (Option) var9._1();
                    Option var18 = (Option) var9._2();
                    if ( var17 instanceof Some )
                    {
                        Some var19 = (Some) var17;
                        LogicalPlan left = (LogicalPlan) var19.value();
                        if ( var18 instanceof Some )
                        {
                            Some var21 = (Some) var18;
                            LogicalPlan right = (LogicalPlan) var21.value();
                            if ( right == left )
                            {
                                throw new InternalException(
                                        (new StringBuilder( 83 )).append( "Tried to map bad logical plan. LHS and RHS must never be the same: op: " ).append(
                                                current ).append( "\nfull plan: " ).append( plan ).toString() );
                            }
                        }
                    }
                }

                if ( var9 != null )
                {
                    Option var23 = (Option) var9._1();
                    Option var24 = (Option) var9._2();
                    if ( var23 instanceof Some )
                    {
                        Some var25 = (Some) var23;
                        LogicalPlan left = (LogicalPlan) var25.value();
                        if ( var24 instanceof Some )
                        {
                            Some var27 = (Some) var24;
                            LogicalPlan right = (LogicalPlan) var27.value();
                            if ( (LogicalPlan) comingFrom.elem == left )
                            {
                                Object leftOutput = outputStack.top();
                                Object newArgument = this.onTwoChildPlanComingFromLeft( current, leftOutput, argument );
                                argumentStack.push( newArgument );
                                planStack.push( current );
                                this.populate$1( right, planStack, comingFrom );
                                BoxedUnit var39 = BoxedUnit.UNIT;
                                break label85;
                            }
                        }
                    }
                }

                if ( var9 == null )
                {
                    break;
                }

                Option var31 = (Option) var9._1();
                Option var32 = (Option) var9._2();
                if ( !(var31 instanceof Some) || !(var32 instanceof Some) )
                {
                    break;
                }

                Some var33 = (Some) var32;
                LogicalPlan right = (LogicalPlan) var33.value();
                if ( (LogicalPlan) comingFrom.elem != right )
                {
                    break;
                }

                Object rightOutput = outputStack.pop();
                Object leftOutput = outputStack.pop();
                Object output = this.onTwoChildPlanComingFromRight( current, leftOutput, rightOutput, argument );
                argumentStack.pop();
                var2 = outputStack.push( output );
            }

            comingFrom.elem = current;
        }

        throw new MatchError( var9 );
    }

    private default void populate$1( final LogicalPlan plan, final Stack planStack$1, final ObjectRef comingFrom$1 )
    {
        LogicalPlan current;
        for ( current = plan; !current.isLeaf(); current = (LogicalPlan) current.lhs().get() )
        {
            this.validatePlan( current );
            planStack$1.push( current );
        }

        comingFrom$1.elem = current;
        planStack$1.push( current );
    }
}
