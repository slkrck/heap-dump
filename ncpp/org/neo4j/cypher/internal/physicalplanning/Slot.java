package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface Slot
{
    int offset();

    boolean nullable();

    CypherType typ();

    boolean isTypeCompatibleWith( final Slot other );

    boolean isLongSlot();

    Slot asNullable();
}
