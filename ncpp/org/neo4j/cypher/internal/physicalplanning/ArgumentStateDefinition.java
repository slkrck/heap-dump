package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.v4_0.util.attribution.Id;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple3;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class ArgumentStateDefinition implements Product, Serializable
{
    private final int id;
    private final int planId;
    private final int argumentSlotOffset;

    public ArgumentStateDefinition( final int id, final int planId, final int argumentSlotOffset )
    {
        this.id = id;
        this.planId = planId;
        this.argumentSlotOffset = argumentSlotOffset;
        Product.$init$( this );
    }

    public static Option<Tuple3<ArgumentStateMapId,Id,Object>> unapply( final ArgumentStateDefinition x$0 )
    {
        return ArgumentStateDefinition$.MODULE$.unapply( var0 );
    }

    public static ArgumentStateDefinition apply( final int id, final int planId, final int argumentSlotOffset )
    {
        return ArgumentStateDefinition$.MODULE$.apply( var0, var1, var2 );
    }

    public static Function1<Tuple3<ArgumentStateMapId,Id,Object>,ArgumentStateDefinition> tupled()
    {
        return ArgumentStateDefinition$.MODULE$.tupled();
    }

    public static Function1<ArgumentStateMapId,Function1<Id,Function1<Object,ArgumentStateDefinition>>> curried()
    {
        return ArgumentStateDefinition$.MODULE$.curried();
    }

    public int id()
    {
        return this.id;
    }

    public int planId()
    {
        return this.planId;
    }

    public int argumentSlotOffset()
    {
        return this.argumentSlotOffset;
    }

    public ArgumentStateDefinition copy( final int id, final int planId, final int argumentSlotOffset )
    {
        return new ArgumentStateDefinition( id, planId, argumentSlotOffset );
    }

    public int copy$default$1()
    {
        return this.id();
    }

    public int copy$default$2()
    {
        return this.planId();
    }

    public int copy$default$3()
    {
        return this.argumentSlotOffset();
    }

    public String productPrefix()
    {
        return "ArgumentStateDefinition";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = new ArgumentStateMapId( this.id() );
            break;
        case 1:
            var10000 = new Id( this.planId() );
            break;
        case 2:
            var10000 = BoxesRunTime.boxToInteger( this.argumentSlotOffset() );
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof ArgumentStateDefinition;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.anyHash( new ArgumentStateMapId( this.id() ) ) );
        var1 = Statics.mix( var1, Statics.anyHash( new Id( this.planId() ) ) );
        var1 = Statics.mix( var1, this.argumentSlotOffset() );
        return Statics.finalizeHash( var1, 3 );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var10000;
        if ( this != x$1 )
        {
            label53:
            {
                boolean var2;
                if ( x$1 instanceof ArgumentStateDefinition )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    ArgumentStateDefinition var4 = (ArgumentStateDefinition) x$1;
                    if ( this.id() == var4.id() && this.planId() == var4.planId() && this.argumentSlotOffset() == var4.argumentSlotOffset() &&
                            var4.canEqual( this ) )
                    {
                        break label53;
                    }
                }

                var10000 = false;
                return var10000;
            }
        }

        var10000 = true;
        return var10000;
    }
}
