package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.logical.plans.AllNodesScan;
import org.neo4j.cypher.internal.logical.plans.Apply;
import org.neo4j.cypher.internal.logical.plans.Argument;
import org.neo4j.cypher.internal.logical.plans.CacheProperties;
import org.neo4j.cypher.internal.logical.plans.DirectedRelationshipByIdSeek;
import org.neo4j.cypher.internal.logical.plans.Expand;
import org.neo4j.cypher.internal.logical.plans.ExpansionMode;
import org.neo4j.cypher.internal.logical.plans.Input;
import org.neo4j.cypher.internal.logical.plans.Limit;
import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.logical.plans.NodeByIdSeek;
import org.neo4j.cypher.internal.logical.plans.NodeByLabelScan;
import org.neo4j.cypher.internal.logical.plans.NodeCountFromCountStore;
import org.neo4j.cypher.internal.logical.plans.NodeIndexContainsScan;
import org.neo4j.cypher.internal.logical.plans.NodeIndexEndsWithScan;
import org.neo4j.cypher.internal.logical.plans.NodeIndexScan;
import org.neo4j.cypher.internal.logical.plans.NodeIndexSeek;
import org.neo4j.cypher.internal.logical.plans.NodeUniqueIndexSeek;
import org.neo4j.cypher.internal.logical.plans.Projection;
import org.neo4j.cypher.internal.logical.plans.RelationshipCountFromCountStore;
import org.neo4j.cypher.internal.logical.plans.Selection;
import org.neo4j.cypher.internal.logical.plans.UndirectedRelationshipByIdSeek;
import org.neo4j.cypher.internal.logical.plans.UnwindCollection;
import org.neo4j.cypher.internal.logical.plans.VarExpand;
import scala.Function1;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public interface OperatorFusionPolicy
{
    static OperatorFusionPolicy apply( final boolean fusionEnabled, final boolean fusionOverPipelinesEnabled )
    {
        return OperatorFusionPolicy$.MODULE$.apply( var0, var1 );
    }

    boolean fusionEnabled();

    boolean fusionOverPipelineEnabled();

    boolean canFuse( final LogicalPlan lp );

    boolean canFuseOverPipeline( final LogicalPlan lp );

    public static class OPERATOR_FUSION_DISABLED$ implements OperatorFusionPolicy, Product, Serializable
    {
        public static OperatorFusionPolicy.OPERATOR_FUSION_DISABLED$ MODULE$;

        static
        {
            new OperatorFusionPolicy.OPERATOR_FUSION_DISABLED$();
        }

        public OPERATOR_FUSION_DISABLED$()
        {
            MODULE$ = this;
            Product.$init$( this );
        }

        public boolean canFuse( final LogicalPlan lp )
        {
            return false;
        }

        public boolean canFuseOverPipeline( final LogicalPlan lp )
        {
            return false;
        }

        public boolean fusionEnabled()
        {
            return false;
        }

        public boolean fusionOverPipelineEnabled()
        {
            return false;
        }

        public String productPrefix()
        {
            return "OPERATOR_FUSION_DISABLED";
        }

        public int productArity()
        {
            return 0;
        }

        public Object productElement( final int x$1 )
        {
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof OperatorFusionPolicy.OPERATOR_FUSION_DISABLED$;
        }

        public int hashCode()
        {
            return 2071914172;
        }

        public String toString()
        {
            return "OPERATOR_FUSION_DISABLED";
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }

    public static class OPERATOR_FUSION_OVER_PIPELINES$ implements OperatorFusionPolicy, Product, Serializable
    {
        public static OperatorFusionPolicy.OPERATOR_FUSION_OVER_PIPELINES$ MODULE$;

        static
        {
            new OperatorFusionPolicy.OPERATOR_FUSION_OVER_PIPELINES$();
        }

        public OPERATOR_FUSION_OVER_PIPELINES$()
        {
            MODULE$ = this;
            Product.$init$( this );
        }

        public boolean fusionEnabled()
        {
            return true;
        }

        public boolean fusionOverPipelineEnabled()
        {
            return true;
        }

        public boolean canFuse( final LogicalPlan lp )
        {
            boolean var4;
            if ( lp instanceof AllNodesScan )
            {
                var4 = true;
            }
            else if ( lp instanceof NodeByLabelScan )
            {
                var4 = true;
            }
            else if ( lp instanceof NodeIndexSeek )
            {
                var4 = true;
            }
            else if ( lp instanceof NodeUniqueIndexSeek )
            {
                var4 = true;
            }
            else if ( lp instanceof NodeIndexContainsScan )
            {
                var4 = true;
            }
            else if ( lp instanceof NodeIndexEndsWithScan )
            {
                var4 = true;
            }
            else if ( lp instanceof NodeIndexScan )
            {
                var4 = true;
            }
            else if ( lp instanceof NodeByIdSeek )
            {
                var4 = true;
            }
            else if ( lp instanceof DirectedRelationshipByIdSeek )
            {
                var4 = true;
            }
            else if ( lp instanceof UndirectedRelationshipByIdSeek )
            {
                var4 = true;
            }
            else if ( lp instanceof NodeCountFromCountStore )
            {
                var4 = true;
            }
            else if ( lp instanceof RelationshipCountFromCountStore )
            {
                var4 = true;
            }
            else if ( lp instanceof Input )
            {
                var4 = true;
            }
            else if ( lp instanceof Argument )
            {
                var4 = true;
            }
            else
            {
                var4 = false;
            }

            boolean var2;
            if ( var4 )
            {
                var2 = true;
            }
            else
            {
                label124:
                {
                    if ( lp instanceof Expand )
                    {
                        Expand var6 = (Expand) lp;
                        ExpansionMode var10000 = var6.mode();
                        org.neo4j.cypher.internal.logical.plans.ExpandAll.var7 = org.neo4j.cypher.internal.logical.plans.ExpandAll..MODULE$;
                        if ( var10000 == null )
                        {
                            if ( var7 == null )
                            {
                                break label124;
                            }
                        }
                        else if ( var10000.equals( var7 ) )
                        {
                            break label124;
                        }
                    }

                    boolean var3;
                    if ( lp instanceof Selection )
                    {
                        var3 = true;
                    }
                    else if ( lp instanceof Projection )
                    {
                        var3 = true;
                    }
                    else if ( lp instanceof UnwindCollection )
                    {
                        var3 = true;
                    }
                    else if ( lp instanceof Limit )
                    {
                        var3 = true;
                    }
                    else if ( lp instanceof VarExpand )
                    {
                        var3 = true;
                    }
                    else if ( lp instanceof CacheProperties )
                    {
                        var3 = true;
                    }
                    else
                    {
                        var3 = false;
                    }

                    if ( var3 )
                    {
                        var2 = true;
                        return var2;
                    }
                    else
                    {
                        if ( lp instanceof Apply )
                        {
                            var2 = true;
                        }
                        else
                        {
                            var2 = false;
                        }

                        return var2;
                    }
                }

                var2 = true;
            }

            return var2;
        }

        public boolean canFuseOverPipeline( final LogicalPlan lp )
        {
            boolean var3;
            if ( lp instanceof Argument )
            {
                var3 = true;
            }
            else if ( lp instanceof Input )
            {
                var3 = true;
            }
            else
            {
                var3 = false;
            }

            boolean var2;
            if ( var3 )
            {
                var2 = false;
            }
            else
            {
                if ( lp instanceof VarExpand )
                {
                    VarExpand var5 = (VarExpand) lp;
                    if ( var5.nodePredicate().isDefined() || var5.relationshipPredicate().isDefined() )
                    {
                        var2 = false;
                        return var2;
                    }
                }

                var2 = this.canFuse( lp ) && !org.neo4j.cypher.internal.v4_0.util.Foldable.FoldableAny..
                MODULE$.treeExists$extension( org.neo4j.cypher.internal.v4_0.util.Foldable..MODULE$.FoldableAny( lp ), new Serializable()
                {
                    public static final long serialVersionUID = 0L;

                    public final <A1, B1> B1 applyOrElse( final A1 x1, final Function1<A1,B1> default )
                    {
                        Object var3;
                        if ( x1 instanceof Limit )
                        {
                            var3 = BoxesRunTime.boxToBoolean( true );
                        }
                        else
                        {
                            var3 = var2.apply( x1 );
                        }

                        return var3;
                    }

                    public final boolean isDefinedAt( final Object x1 )
                    {
                        boolean var2;
                        if ( x1 instanceof Limit )
                        {
                            var2 = true;
                        }
                        else
                        {
                            var2 = false;
                        }

                        return var2;
                    }
                });
            }

            return var2;
        }

        public String productPrefix()
        {
            return "OPERATOR_FUSION_OVER_PIPELINES";
        }

        public int productArity()
        {
            return 0;
        }

        public Object productElement( final int x$1 )
        {
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof OperatorFusionPolicy.OPERATOR_FUSION_OVER_PIPELINES$;
        }

        public int hashCode()
        {
            return -239567738;
        }

        public String toString()
        {
            return "OPERATOR_FUSION_OVER_PIPELINES";
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }

    public static class OPERATOR_FUSION_WITHIN_PIPELINE$ implements OperatorFusionPolicy, Product, Serializable
    {
        public static OperatorFusionPolicy.OPERATOR_FUSION_WITHIN_PIPELINE$ MODULE$;

        static
        {
            new OperatorFusionPolicy.OPERATOR_FUSION_WITHIN_PIPELINE$();
        }

        public OPERATOR_FUSION_WITHIN_PIPELINE$()
        {
            MODULE$ = this;
            Product.$init$( this );
        }

        public boolean fusionEnabled()
        {
            return true;
        }

        public boolean fusionOverPipelineEnabled()
        {
            return false;
        }

        public boolean canFuse( final LogicalPlan lp )
        {
            return OperatorFusionPolicy.OPERATOR_FUSION_OVER_PIPELINES$.MODULE$.canFuse( lp );
        }

        public boolean canFuseOverPipeline( final LogicalPlan lp )
        {
            return false;
        }

        public String productPrefix()
        {
            return "OPERATOR_FUSION_WITHIN_PIPELINE";
        }

        public int productArity()
        {
            return 0;
        }

        public Object productElement( final int x$1 )
        {
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof OperatorFusionPolicy.OPERATOR_FUSION_WITHIN_PIPELINE$;
        }

        public int hashCode()
        {
            return 747009974;
        }

        public String toString()
        {
            return "OPERATOR_FUSION_WITHIN_PIPELINE";
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }
}
