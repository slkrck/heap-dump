package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.v4_0.util.attribution.Id;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class MorselBufferOutput implements OutputDefinition, Product, Serializable
{
    private final int id;
    private final int nextPipelineHeadPlanId;

    public MorselBufferOutput( final int id, final int nextPipelineHeadPlanId )
    {
        this.id = id;
        this.nextPipelineHeadPlanId = nextPipelineHeadPlanId;
        Product.$init$( this );
    }

    public static Option<Tuple2<BufferId,Id>> unapply( final MorselBufferOutput x$0 )
    {
        return MorselBufferOutput$.MODULE$.unapply( var0 );
    }

    public static MorselBufferOutput apply( final int id, final int nextPipelineHeadPlanId )
    {
        return MorselBufferOutput$.MODULE$.apply( var0, var1 );
    }

    public static Function1<Tuple2<BufferId,Id>,MorselBufferOutput> tupled()
    {
        return MorselBufferOutput$.MODULE$.tupled();
    }

    public static Function1<BufferId,Function1<Id,MorselBufferOutput>> curried()
    {
        return MorselBufferOutput$.MODULE$.curried();
    }

    public int id()
    {
        return this.id;
    }

    public int nextPipelineHeadPlanId()
    {
        return this.nextPipelineHeadPlanId;
    }

    public MorselBufferOutput copy( final int id, final int nextPipelineHeadPlanId )
    {
        return new MorselBufferOutput( id, nextPipelineHeadPlanId );
    }

    public int copy$default$1()
    {
        return this.id();
    }

    public int copy$default$2()
    {
        return this.nextPipelineHeadPlanId();
    }

    public String productPrefix()
    {
        return "MorselBufferOutput";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = new BufferId( this.id() );
            break;
        case 1:
            var10000 = new Id( this.nextPipelineHeadPlanId() );
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof MorselBufferOutput;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var10000;
        if ( this != x$1 )
        {
            label51:
            {
                boolean var2;
                if ( x$1 instanceof MorselBufferOutput )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    MorselBufferOutput var4 = (MorselBufferOutput) x$1;
                    if ( this.id() == var4.id() && this.nextPipelineHeadPlanId() == var4.nextPipelineHeadPlanId() && var4.canEqual( this ) )
                    {
                        break label51;
                    }
                }

                var10000 = false;
                return var10000;
            }
        }

        var10000 = true;
        return var10000;
    }
}
