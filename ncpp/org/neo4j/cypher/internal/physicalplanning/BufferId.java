package org.neo4j.cypher.internal.physicalplanning;

import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class BufferId implements Product, Serializable
{
    private final int x;

    public BufferId( final int x )
    {
        this.x = x;
        Product.$init$( this );
    }

    public static String toString$extension( final int $this )
    {
        return BufferId$.MODULE$.toString$extension( var0 );
    }

    public static boolean equals$extension( final int $this, final Object x$1 )
    {
        return BufferId$.MODULE$.equals$extension( var0, var1 );
    }

    public static int hashCode$extension( final int $this )
    {
        return BufferId$.MODULE$.hashCode$extension( var0 );
    }

    public static boolean canEqual$extension( final int $this, final Object x$1 )
    {
        return BufferId$.MODULE$.canEqual$extension( var0, var1 );
    }

    public static Iterator<Object> productIterator$extension( final int $this )
    {
        return BufferId$.MODULE$.productIterator$extension( var0 );
    }

    public static Object productElement$extension( final int $this, final int x$1 )
    {
        return BufferId$.MODULE$.productElement$extension( var0, var1 );
    }

    public static int productArity$extension( final int $this )
    {
        return BufferId$.MODULE$.productArity$extension( var0 );
    }

    public static String productPrefix$extension( final int $this )
    {
        return BufferId$.MODULE$.productPrefix$extension( var0 );
    }

    public static int copy$default$1$extension( final int $this )
    {
        return BufferId$.MODULE$.copy$default$1$extension( var0 );
    }

    public static int copy$extension( final int $this, final int x )
    {
        return BufferId$.MODULE$.copy$extension( var0, var1 );
    }

    public static Option<Object> unapply( final int x$0 )
    {
        return BufferId$.MODULE$.unapply( var0 );
    }

    public static int apply( final int x )
    {
        return BufferId$.MODULE$.apply( var0 );
    }

    public static <A> Function1<Object,A> andThen( final Function1<BufferId,A> g )
    {
        return BufferId$.MODULE$.andThen( var0 );
    }

    public static <A> Function1<A,BufferId> compose( final Function1<A,Object> g )
    {
        return BufferId$.MODULE$.compose( var0 );
    }

    public int x()
    {
        return this.x;
    }

    public int copy( final int x )
    {
        return BufferId$.MODULE$.copy$extension( this.x(), x );
    }

    public int copy$default$1()
    {
        return BufferId$.MODULE$.copy$default$1$extension( this.x() );
    }

    public String productPrefix()
    {
        return BufferId$.MODULE$.productPrefix$extension( this.x() );
    }

    public int productArity()
    {
        return BufferId$.MODULE$.productArity$extension( this.x() );
    }

    public Object productElement( final int x$1 )
    {
        return BufferId$.MODULE$.productElement$extension( this.x(), x$1 );
    }

    public Iterator<Object> productIterator()
    {
        return BufferId$.MODULE$.productIterator$extension( this.x() );
    }

    public boolean canEqual( final Object x$1 )
    {
        return BufferId$.MODULE$.canEqual$extension( this.x(), x$1 );
    }

    public int hashCode()
    {
        return BufferId$.MODULE$.hashCode$extension( this.x() );
    }

    public boolean equals( final Object x$1 )
    {
        return BufferId$.MODULE$.equals$extension( this.x(), x$1 );
    }

    public String toString()
    {
        return BufferId$.MODULE$.toString$extension( this.x() );
    }
}
