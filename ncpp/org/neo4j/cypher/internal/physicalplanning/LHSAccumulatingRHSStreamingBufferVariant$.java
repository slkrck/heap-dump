package org.neo4j.cypher.internal.physicalplanning;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.None.;
import scala.runtime.AbstractFunction4;

public final class LHSAccumulatingRHSStreamingBufferVariant$
        extends AbstractFunction4<PipelineId,PipelineId,ArgumentStateMapId,ArgumentStateMapId,LHSAccumulatingRHSStreamingBufferVariant> implements Serializable
{
    public static LHSAccumulatingRHSStreamingBufferVariant$ MODULE$;

    static
    {
        new LHSAccumulatingRHSStreamingBufferVariant$();
    }

    private LHSAccumulatingRHSStreamingBufferVariant$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "LHSAccumulatingRHSStreamingBufferVariant";
    }

    public LHSAccumulatingRHSStreamingBufferVariant apply( final int lhsPipelineId, final int rhsPipelineId, final int lhsArgumentStateMapId,
            final int rhsArgumentStateMapId )
    {
        return new LHSAccumulatingRHSStreamingBufferVariant( lhsPipelineId, rhsPipelineId, lhsArgumentStateMapId, rhsArgumentStateMapId );
    }

    public Option<Tuple4<PipelineId,PipelineId,ArgumentStateMapId,ArgumentStateMapId>> unapply( final LHSAccumulatingRHSStreamingBufferVariant x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some(
            new Tuple4( new PipelineId( x$0.lhsPipelineId() ), new PipelineId( x$0.rhsPipelineId() ), new ArgumentStateMapId( x$0.lhsArgumentStateMapId() ),
                    new ArgumentStateMapId( x$0.rhsArgumentStateMapId() ) ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
