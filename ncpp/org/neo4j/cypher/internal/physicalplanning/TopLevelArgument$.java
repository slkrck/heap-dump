package org.neo4j.cypher.internal.physicalplanning;

public final class TopLevelArgument$
{
    public static TopLevelArgument$ MODULE$;

    static
    {
        new TopLevelArgument$();
    }

    private final long VALUE;
    private final int SLOT_OFFSET;

    private TopLevelArgument$()
    {
        MODULE$ = this;
        this.VALUE = 0L;
        this.SLOT_OFFSET = -1;
    }

    public void assertTopLevelArgument( final long argument )
    {
        if ( argument != this.VALUE() )
        {
            throw new TopLevelArgument.TopLevelArgumentException( argument );
        }
    }

    public long VALUE()
    {
        return this.VALUE;
    }

    public int SLOT_OFFSET()
    {
        return this.SLOT_OFFSET;
    }
}
