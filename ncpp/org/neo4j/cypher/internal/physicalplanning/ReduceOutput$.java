package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;

public final class ReduceOutput$ extends AbstractFunction2<BufferId,LogicalPlan,ReduceOutput> implements Serializable
{
    public static ReduceOutput$ MODULE$;

    static
    {
        new ReduceOutput$();
    }

    private ReduceOutput$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "ReduceOutput";
    }

    public ReduceOutput apply( final int bufferId, final LogicalPlan plan )
    {
        return new ReduceOutput( bufferId, plan );
    }

    public Option<Tuple2<BufferId,LogicalPlan>> unapply( final ReduceOutput x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( new BufferId( x$0.bufferId() ), x$0.plan() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
