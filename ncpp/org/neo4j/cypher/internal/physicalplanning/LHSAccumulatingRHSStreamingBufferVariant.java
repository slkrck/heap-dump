package org.neo4j.cypher.internal.physicalplanning;

import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple4;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class LHSAccumulatingRHSStreamingBufferVariant implements BufferVariant, Product, Serializable
{
    private final int lhsPipelineId;
    private final int rhsPipelineId;
    private final int lhsArgumentStateMapId;
    private final int rhsArgumentStateMapId;

    public LHSAccumulatingRHSStreamingBufferVariant( final int lhsPipelineId, final int rhsPipelineId, final int lhsArgumentStateMapId,
            final int rhsArgumentStateMapId )
    {
        this.lhsPipelineId = lhsPipelineId;
        this.rhsPipelineId = rhsPipelineId;
        this.lhsArgumentStateMapId = lhsArgumentStateMapId;
        this.rhsArgumentStateMapId = rhsArgumentStateMapId;
        Product.$init$( this );
    }

    public static Option<Tuple4<PipelineId,PipelineId,ArgumentStateMapId,ArgumentStateMapId>> unapply( final LHSAccumulatingRHSStreamingBufferVariant x$0 )
    {
        return LHSAccumulatingRHSStreamingBufferVariant$.MODULE$.unapply( var0 );
    }

    public static LHSAccumulatingRHSStreamingBufferVariant apply( final int lhsPipelineId, final int rhsPipelineId, final int lhsArgumentStateMapId,
            final int rhsArgumentStateMapId )
    {
        return LHSAccumulatingRHSStreamingBufferVariant$.MODULE$.apply( var0, var1, var2, var3 );
    }

    public static Function1<Tuple4<PipelineId,PipelineId,ArgumentStateMapId,ArgumentStateMapId>,LHSAccumulatingRHSStreamingBufferVariant> tupled()
    {
        return LHSAccumulatingRHSStreamingBufferVariant$.MODULE$.tupled();
    }

    public static Function1<PipelineId,Function1<PipelineId,Function1<ArgumentStateMapId,Function1<ArgumentStateMapId,LHSAccumulatingRHSStreamingBufferVariant>>>> curried()
    {
        return LHSAccumulatingRHSStreamingBufferVariant$.MODULE$.curried();
    }

    public int lhsPipelineId()
    {
        return this.lhsPipelineId;
    }

    public int rhsPipelineId()
    {
        return this.rhsPipelineId;
    }

    public int lhsArgumentStateMapId()
    {
        return this.lhsArgumentStateMapId;
    }

    public int rhsArgumentStateMapId()
    {
        return this.rhsArgumentStateMapId;
    }

    public LHSAccumulatingRHSStreamingBufferVariant copy( final int lhsPipelineId, final int rhsPipelineId, final int lhsArgumentStateMapId,
            final int rhsArgumentStateMapId )
    {
        return new LHSAccumulatingRHSStreamingBufferVariant( lhsPipelineId, rhsPipelineId, lhsArgumentStateMapId, rhsArgumentStateMapId );
    }

    public int copy$default$1()
    {
        return this.lhsPipelineId();
    }

    public int copy$default$2()
    {
        return this.rhsPipelineId();
    }

    public int copy$default$3()
    {
        return this.lhsArgumentStateMapId();
    }

    public int copy$default$4()
    {
        return this.rhsArgumentStateMapId();
    }

    public String productPrefix()
    {
        return "LHSAccumulatingRHSStreamingBufferVariant";
    }

    public int productArity()
    {
        return 4;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = new PipelineId( this.lhsPipelineId() );
            break;
        case 1:
            var10000 = new PipelineId( this.rhsPipelineId() );
            break;
        case 2:
            var10000 = new ArgumentStateMapId( this.lhsArgumentStateMapId() );
            break;
        case 3:
            var10000 = new ArgumentStateMapId( this.rhsArgumentStateMapId() );
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof LHSAccumulatingRHSStreamingBufferVariant;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var10000;
        label49:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof LHSAccumulatingRHSStreamingBufferVariant )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label49;
                }

                LHSAccumulatingRHSStreamingBufferVariant var4 = (LHSAccumulatingRHSStreamingBufferVariant) x$1;
                if ( this.lhsPipelineId() != var4.lhsPipelineId() || this.rhsPipelineId() != var4.rhsPipelineId() ||
                        this.lhsArgumentStateMapId() != var4.lhsArgumentStateMapId() || this.rhsArgumentStateMapId() != var4.rhsArgumentStateMapId() ||
                        !var4.canEqual( this ) )
                {
                    break label49;
                }
            }

            var10000 = true;
            return var10000;
        }

        var10000 = false;
        return var10000;
    }
}
