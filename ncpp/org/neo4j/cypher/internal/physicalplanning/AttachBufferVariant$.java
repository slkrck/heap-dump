package org.neo4j.cypher.internal.physicalplanning;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.None.;
import scala.runtime.AbstractFunction4;
import scala.runtime.BoxesRunTime;

public final class AttachBufferVariant$ extends AbstractFunction4<BufferDefinition,SlotConfiguration,Object,SlotConfiguration.Size,AttachBufferVariant>
        implements Serializable
{
    public static AttachBufferVariant$ MODULE$;

    static
    {
        new AttachBufferVariant$();
    }

    private AttachBufferVariant$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "AttachBufferVariant";
    }

    public AttachBufferVariant apply( final BufferDefinition applyBuffer, final SlotConfiguration outputSlots, final int argumentSlotOffset,
            final SlotConfiguration.Size argumentSize )
    {
        return new AttachBufferVariant( applyBuffer, outputSlots, argumentSlotOffset, argumentSize );
    }

    public Option<Tuple4<BufferDefinition,SlotConfiguration,Object,SlotConfiguration.Size>> unapply( final AttachBufferVariant x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :
        new Some( new Tuple4( x$0.applyBuffer(), x$0.outputSlots(), BoxesRunTime.boxToInteger( x$0.argumentSlotOffset() ), x$0.argumentSize() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
