package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.v4_0.util.attribution.Id;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple3;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class MorselArgumentStateBufferOutput implements OutputDefinition, Product, Serializable
{
    private final int id;
    private final int argumentSlotOffset;
    private final int nextPipelineHeadPlanId;

    public MorselArgumentStateBufferOutput( final int id, final int argumentSlotOffset, final int nextPipelineHeadPlanId )
    {
        this.id = id;
        this.argumentSlotOffset = argumentSlotOffset;
        this.nextPipelineHeadPlanId = nextPipelineHeadPlanId;
        Product.$init$( this );
    }

    public static Option<Tuple3<BufferId,Object,Id>> unapply( final MorselArgumentStateBufferOutput x$0 )
    {
        return MorselArgumentStateBufferOutput$.MODULE$.unapply( var0 );
    }

    public static MorselArgumentStateBufferOutput apply( final int id, final int argumentSlotOffset, final int nextPipelineHeadPlanId )
    {
        return MorselArgumentStateBufferOutput$.MODULE$.apply( var0, var1, var2 );
    }

    public static Function1<Tuple3<BufferId,Object,Id>,MorselArgumentStateBufferOutput> tupled()
    {
        return MorselArgumentStateBufferOutput$.MODULE$.tupled();
    }

    public static Function1<BufferId,Function1<Object,Function1<Id,MorselArgumentStateBufferOutput>>> curried()
    {
        return MorselArgumentStateBufferOutput$.MODULE$.curried();
    }

    public int id()
    {
        return this.id;
    }

    public int argumentSlotOffset()
    {
        return this.argumentSlotOffset;
    }

    public int nextPipelineHeadPlanId()
    {
        return this.nextPipelineHeadPlanId;
    }

    public MorselArgumentStateBufferOutput copy( final int id, final int argumentSlotOffset, final int nextPipelineHeadPlanId )
    {
        return new MorselArgumentStateBufferOutput( id, argumentSlotOffset, nextPipelineHeadPlanId );
    }

    public int copy$default$1()
    {
        return this.id();
    }

    public int copy$default$2()
    {
        return this.argumentSlotOffset();
    }

    public int copy$default$3()
    {
        return this.nextPipelineHeadPlanId();
    }

    public String productPrefix()
    {
        return "MorselArgumentStateBufferOutput";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = new BufferId( this.id() );
            break;
        case 1:
            var10000 = BoxesRunTime.boxToInteger( this.argumentSlotOffset() );
            break;
        case 2:
            var10000 = new Id( this.nextPipelineHeadPlanId() );
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof MorselArgumentStateBufferOutput;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.anyHash( new BufferId( this.id() ) ) );
        var1 = Statics.mix( var1, this.argumentSlotOffset() );
        var1 = Statics.mix( var1, Statics.anyHash( new Id( this.nextPipelineHeadPlanId() ) ) );
        return Statics.finalizeHash( var1, 3 );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var10000;
        if ( this != x$1 )
        {
            label53:
            {
                boolean var2;
                if ( x$1 instanceof MorselArgumentStateBufferOutput )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    MorselArgumentStateBufferOutput var4 = (MorselArgumentStateBufferOutput) x$1;
                    if ( this.id() == var4.id() && this.argumentSlotOffset() == var4.argumentSlotOffset() &&
                            this.nextPipelineHeadPlanId() == var4.nextPipelineHeadPlanId() && var4.canEqual( this ) )
                    {
                        break label53;
                    }
                }

                var10000 = false;
                return var10000;
            }
        }

        var10000 = true;
        return var10000;
    }
}
