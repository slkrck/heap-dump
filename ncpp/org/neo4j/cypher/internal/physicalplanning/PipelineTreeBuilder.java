package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.logical.plans.Aggregation;
import org.neo4j.cypher.internal.logical.plans.Apply;
import org.neo4j.cypher.internal.logical.plans.CartesianProduct;
import org.neo4j.cypher.internal.logical.plans.Distinct;
import org.neo4j.cypher.internal.logical.plans.Limit;
import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.logical.plans.NodeHashJoin;
import org.neo4j.cypher.internal.logical.plans.Optional;
import org.neo4j.cypher.internal.logical.plans.ProduceResult;
import org.neo4j.cypher.internal.logical.plans.Sort;
import org.neo4j.cypher.internal.logical.plans.Top;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id;
import scala.Function1;
import scala.MatchError;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple4;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.mutable.ArrayBuffer;
import scala.collection.mutable.HashMap;
import scala.reflect.ScalaSignature;
import scala.runtime.AbstractFunction1;
import scala.runtime.AbstractFunction4;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;

@JavaDocToJava
public class PipelineTreeBuilder implements TreeBuilder<PipelineTreeBuilder.PipelineDefinitionBuild,PipelineTreeBuilder.ApplyBufferDefinitionBuild>
{
    private final PipelineBreakingPolicy breakingPolicy;
    private final OperatorFusionPolicy operatorFusionPolicy;
    private final PipelineTreeBuilder.ExecutionStateDefinitionBuild stateDefinition;
    private final PhysicalPlanningAttributes.SlotConfigurations slotConfigurations;
    private final PhysicalPlanningAttributes.ArgumentSizes argumentSizes;
    private final ArrayBuffer<PipelineTreeBuilder.PipelineDefinitionBuild> pipelines;
    private final HashMap<Object,Object> applyRhsPlans;

    public PipelineTreeBuilder( final PipelineBreakingPolicy breakingPolicy, final OperatorFusionPolicy operatorFusionPolicy,
            final PipelineTreeBuilder.ExecutionStateDefinitionBuild stateDefinition, final PhysicalPlanningAttributes.SlotConfigurations slotConfigurations,
            final PhysicalPlanningAttributes.ArgumentSizes argumentSizes )
    {
        this.breakingPolicy = breakingPolicy;
        this.operatorFusionPolicy = operatorFusionPolicy;
        this.stateDefinition = stateDefinition;
        this.slotConfigurations = slotConfigurations;
        this.argumentSizes = argumentSizes;
        TreeBuilder.$init$( this );
        this.pipelines = new ArrayBuffer();
        this.applyRhsPlans = new HashMap();
    }

    public static PipelineTreeBuilder.PipelineDefinitionBuild NO_PIPELINE_BUILD()
    {
        return PipelineTreeBuilder$.MODULE$.NO_PIPELINE_BUILD();
    }

    public Object build( final LogicalPlan plan )
    {
        return TreeBuilder.build$( this, plan );
    }

    public ArrayBuffer<PipelineTreeBuilder.PipelineDefinitionBuild> pipelines()
    {
        return this.pipelines;
    }

    public HashMap<Object,Object> applyRhsPlans()
    {
        return this.applyRhsPlans;
    }

    private PipelineTreeBuilder.PipelineDefinitionBuild newPipeline( final LogicalPlan plan )
    {
        PipelineTreeBuilder.PipelineDefinitionBuild pipeline = new PipelineTreeBuilder.PipelineDefinitionBuild( this.pipelines().size(), plan );
        this.pipelines().$plus$eq( pipeline );
        if ( this.operatorFusionPolicy.canFuse( plan ) )
        {
            pipeline.fusedPlans().$plus$eq( plan );
        }
        else
        {
            BoxedUnit var10000 = BoxedUnit.UNIT;
        }

        return pipeline;
    }

    private PipelineTreeBuilder.MorselBufferDefinitionBuild outputToBuffer( final PipelineTreeBuilder.PipelineDefinitionBuild pipeline,
            final LogicalPlan nextPipelineHeadPlan )
    {
        PipelineTreeBuilder.MorselBufferDefinitionBuild output =
                this.stateDefinition.newBuffer( pipeline.id(), (SlotConfiguration) this.slotConfigurations.apply( pipeline.headPlan().id() ) );
        pipeline.outputDefinition_$eq( new MorselBufferOutput( output.id(), nextPipelineHeadPlan.id() ) );
        return output;
    }

    private PipelineTreeBuilder.ApplyBufferDefinitionBuild outputToApplyBuffer( final PipelineTreeBuilder.PipelineDefinitionBuild pipeline,
            final int argumentSlotOffset, final LogicalPlan nextPipelineHeadPlan )
    {
        PipelineTreeBuilder.ApplyBufferDefinitionBuild output = this.stateDefinition.newApplyBuffer( pipeline.id(), argumentSlotOffset,
                (SlotConfiguration) this.slotConfigurations.apply( pipeline.headPlan().id() ) );
        pipeline.outputDefinition_$eq( new MorselBufferOutput( output.id(), nextPipelineHeadPlan.id() ) );
        return output;
    }

    private PipelineTreeBuilder.AttachBufferDefinitionBuild outputToAttachApplyBuffer( final PipelineTreeBuilder.PipelineDefinitionBuild pipeline,
            final int attachingPlanId, final int argumentSlotOffset, final SlotConfiguration postAttachSlotConfiguration, final int outerArgumentSlotOffset,
            final SlotConfiguration.Size outerArgumentSize )
    {
        PipelineTreeBuilder.AttachBufferDefinitionBuild output =
                this.stateDefinition.newAttachBuffer( pipeline.id(), (SlotConfiguration) this.slotConfigurations.apply( pipeline.headPlan().id() ),
                        postAttachSlotConfiguration, outerArgumentSlotOffset, outerArgumentSize );
        output.applyBuffer_$eq( this.stateDefinition.newApplyBuffer( pipeline.id(), argumentSlotOffset, postAttachSlotConfiguration ) );
        pipeline.outputDefinition_$eq( new MorselBufferOutput( output.id(), attachingPlanId ) );
        return output;
    }

    private PipelineTreeBuilder.ArgumentStateBufferDefinitionBuild outputToArgumentStateBuffer( final PipelineTreeBuilder.PipelineDefinitionBuild pipeline,
            final LogicalPlan plan, final PipelineTreeBuilder.ApplyBufferDefinitionBuild applyBuffer, final int argumentSlotOffset )
    {
        PipelineTreeBuilder.ArgumentStateDefinitionBuild asm = this.stateDefinition.newArgumentStateMap( plan.id(), argumentSlotOffset, true );
        PipelineTreeBuilder.ArgumentStateBufferDefinitionBuild output = this.stateDefinition.newArgumentStateBuffer( pipeline.id(), asm.id(),
                (SlotConfiguration) this.slotConfigurations.apply( pipeline.headPlan().id() ) );
        pipeline.outputDefinition_$eq( new ReduceOutput( output.id(), plan ) );
        this.markReducerInUpstreamBuffers( pipeline.inputBuffer(), applyBuffer, asm );
        return output;
    }

    private PipelineTreeBuilder.OptionalMorselBufferDefinitionBuild outputToOptionalPipelinedBuffer( final PipelineTreeBuilder.PipelineDefinitionBuild pipeline,
            final LogicalPlan plan, final PipelineTreeBuilder.ApplyBufferDefinitionBuild applyBuffer, final int argumentSlotOffset )
    {
        PipelineTreeBuilder.ArgumentStateDefinitionBuild asm = this.stateDefinition.newArgumentStateMap( plan.id(), argumentSlotOffset, true );
        PipelineTreeBuilder.OptionalMorselBufferDefinitionBuild output = this.stateDefinition.newOptionalBuffer( pipeline.id(), asm.id(), argumentSlotOffset,
                (SlotConfiguration) this.slotConfigurations.apply( pipeline.headPlan().id() ) );
        pipeline.outputDefinition_$eq( new MorselArgumentStateBufferOutput( output.id(), argumentSlotOffset, plan.id() ) );
        this.markReducerInUpstreamBuffers( pipeline.inputBuffer(), applyBuffer, asm );
        return output;
    }

    private PipelineTreeBuilder.LHSAccumulatingRHSStreamingBufferDefinitionBuild outputToLhsAccumulatingRhsStreamingBuffer(
            final PipelineTreeBuilder.PipelineDefinitionBuild lhs, final PipelineTreeBuilder.PipelineDefinitionBuild rhs, final int planId,
            final PipelineTreeBuilder.ApplyBufferDefinitionBuild applyBuffer, final int argumentSlotOffset )
    {
        PipelineTreeBuilder.ArgumentStateDefinitionBuild rhsAsm;
        PipelineTreeBuilder.LHSAccumulatingRHSStreamingBufferDefinitionBuild output;
        label17:
        {
            PipelineTreeBuilder.ArgumentStateDefinitionBuild lhsAsm;
            label16:
            {
                lhsAsm = this.stateDefinition.newArgumentStateMap( planId, argumentSlotOffset, true );
                rhsAsm = this.stateDefinition.newArgumentStateMap( planId, argumentSlotOffset, true );
                output = this.stateDefinition.newLhsAccumulatingRhsStreamingBuffer( lhs.id(), rhs.id(), lhsAsm.id(), rhsAsm.id(),
                        (SlotConfiguration) this.slotConfigurations.apply( rhs.headPlan().id() ) );
                PipelineTreeBuilder.PipelineDefinitionBuild var9 = PipelineTreeBuilder$.MODULE$.NO_PIPELINE_BUILD();
                if ( lhs == null )
                {
                    if ( var9 != null )
                    {
                        break label16;
                    }
                }
                else if ( !lhs.equals( var9 ) )
                {
                    break label16;
                }

                applyBuffer.reducersOnRHS().$plus$eq( lhsAsm );
                break label17;
            }

            lhs.outputDefinition_$eq( new MorselArgumentStateBufferOutput( output.id(), argumentSlotOffset, planId ) );
            this.markReducerInUpstreamBuffers( lhs.inputBuffer(), applyBuffer, lhsAsm );
            BoxedUnit var10000 = BoxedUnit.UNIT;
        }

        rhs.outputDefinition_$eq( new MorselArgumentStateBufferOutput( output.id(), argumentSlotOffset, planId ) );
        this.markReducerInUpstreamBuffers( rhs.inputBuffer(), applyBuffer, rhsAsm );
        return output;
    }

    public void validatePlan( final LogicalPlan plan )
    {
        this.breakingPolicy.breakOn( plan );
    }

    public PipelineTreeBuilder.ApplyBufferDefinitionBuild initialArgument( final LogicalPlan leftLeaf )
    {
        int initialArgumentSlotOffset = ((SlotConfiguration) this.slotConfigurations.apply( leftLeaf.id() )).getArgumentLongOffsetFor(
                org.neo4j.cypher.internal.v4_0.util.attribution.Id..MODULE$.INVALID_ID());
        this.stateDefinition.initBuffer_$eq( this.stateDefinition.newApplyBuffer( PipelineId$.MODULE$.NO_PIPELINE(), initialArgumentSlotOffset,
                SlotAllocation$.MODULE$.INITIAL_SLOT_CONFIGURATION() ) );
        return this.stateDefinition.initBuffer();
    }

    public PipelineTreeBuilder.PipelineDefinitionBuild onLeaf( final LogicalPlan plan, final PipelineTreeBuilder.ApplyBufferDefinitionBuild argument )
    {
        if ( this.breakingPolicy.breakOn( plan ) )
        {
            PipelineTreeBuilder.PipelineDefinitionBuild pipeline = this.newPipeline( plan );
            PipelineTreeBuilder.DelegateBufferDefinitionBuild delegate = this.stateDefinition.newDelegateBuffer( argument, argument.bufferConfiguration() );
            argument.delegates().$plus$eq( new BufferId( delegate.id() ) );
            pipeline.inputBuffer_$eq( delegate );
            pipeline.lhs_$eq( argument.producingPipelineId() );
            return pipeline;
        }
        else
        {
            throw new UnsupportedOperationException( (new StringBuilder( 34 )).append( "Not breaking on " ).append( plan.getClass().getSimpleName() ).append(
                    " is not supported." ).toString() );
        }
    }

    public PipelineTreeBuilder.PipelineDefinitionBuild onOneChildPlan( final LogicalPlan plan, final PipelineTreeBuilder.PipelineDefinitionBuild source,
            final PipelineTreeBuilder.ApplyBufferDefinitionBuild argument )
    {
        PipelineTreeBuilder.PipelineDefinitionBuild var4;
        PipelineTreeBuilder.PipelineDefinitionBuild var10000;
        if ( plan instanceof ProduceResult )
        {
            ProduceResult var7 = (ProduceResult) plan;
            if ( this.breakingPolicy.breakOn( plan ) )
            {
                PipelineTreeBuilder.PipelineDefinitionBuild pipeline = this.newPipeline( plan );
                pipeline.inputBuffer_$eq( this.outputToBuffer( source, plan ) );
                pipeline.lhs_$eq( source.id() );
                pipeline.serial_$eq( true );
                var10000 = pipeline;
            }
            else
            {
                source.outputDefinition_$eq( new ProduceResultOutput( var7 ) );
                source.serial_$eq( true );
                var10000 = source;
            }

            var4 = var10000;
        }
        else
        {
            boolean var5;
            if ( plan instanceof Sort )
            {
                var5 = true;
            }
            else if ( plan instanceof Aggregation )
            {
                var5 = true;
            }
            else if ( plan instanceof Top )
            {
                var5 = true;
            }
            else
            {
                var5 = false;
            }

            if ( var5 )
            {
                if ( !this.breakingPolicy.breakOn( plan ) )
                {
                    throw new UnsupportedOperationException(
                            (new StringBuilder( 34 )).append( "Not breaking on " ).append( plan.getClass().getSimpleName() ).append(
                                    " is not supported." ).toString() );
                }

                PipelineTreeBuilder.PipelineDefinitionBuild pipeline = this.newPipeline( plan );
                PipelineTreeBuilder.ArgumentStateBufferDefinitionBuild argumentStateBuffer =
                        this.outputToArgumentStateBuffer( source, plan, argument, argument.argumentSlotOffset() );
                pipeline.inputBuffer_$eq( argumentStateBuffer );
                pipeline.lhs_$eq( source.id() );
                var4 = pipeline;
            }
            else if ( plan instanceof Optional )
            {
                if ( !this.breakingPolicy.breakOn( plan ) )
                {
                    throw new UnsupportedOperationException(
                            (new StringBuilder( 34 )).append( "Not breaking on " ).append( plan.getClass().getSimpleName() ).append(
                                    " is not supported." ).toString() );
                }

                PipelineTreeBuilder.PipelineDefinitionBuild pipeline = this.newPipeline( plan );
                PipelineTreeBuilder.OptionalMorselBufferDefinitionBuild optionalPipelinedBuffer =
                        this.outputToOptionalPipelinedBuffer( source, plan, argument, argument.argumentSlotOffset() );
                pipeline.inputBuffer_$eq( optionalPipelinedBuffer );
                pipeline.lhs_$eq( source.id() );
                var4 = pipeline;
            }
            else if ( plan instanceof Limit )
            {
                PipelineTreeBuilder.ArgumentStateDefinitionBuild asm =
                        this.stateDefinition.newArgumentStateMap( plan.id(), argument.argumentSlotOffset(), false );
                this.markInUpstreamBuffers( source.inputBuffer(), argument, new PipelineTreeBuilder.DownstreamWorkCanceller( asm.id() ) );
                if ( this.canFuseOverPipeline$1( plan, source ) )
                {
                    source.fusedPlans().$plus$eq( plan );
                    var10000 = source;
                }
                else
                {
                    source.middlePlans().$plus$eq( plan );
                    var10000 = source;
                }

                var4 = var10000;
            }
            else if ( plan instanceof Distinct )
            {
                PipelineTreeBuilder.ArgumentStateDefinitionBuild asm =
                        this.stateDefinition.newArgumentStateMap( plan.id(), argument.argumentSlotOffset(), false );
                argument.downstreamStates().$plus$eq( new PipelineTreeBuilder.DownstreamState( asm.id() ) );
                source.middlePlans().$plus$eq( plan );
                var4 = source;
            }
            else
            {
                if ( this.canFuseOverPipeline$1( plan, source ) )
                {
                    source.fusedPlans().$plus$eq( plan );
                    var10000 = source;
                }
                else if ( this.breakingPolicy.breakOn( plan ) )
                {
                    PipelineTreeBuilder.PipelineDefinitionBuild pipeline = this.newPipeline( plan );
                    pipeline.inputBuffer_$eq( this.outputToBuffer( source, plan ) );
                    pipeline.lhs_$eq( source.id() );
                    var10000 = pipeline;
                }
                else if ( this.canFuse$1( plan, source ) )
                {
                    source.fusedPlans().$plus$eq( plan );
                    var10000 = source;
                }
                else
                {
                    source.middlePlans().$plus$eq( plan );
                    var10000 = source;
                }

                var4 = var10000;
            }
        }

        return var4;
    }

    public PipelineTreeBuilder.ApplyBufferDefinitionBuild onTwoChildPlanComingFromLeft( final LogicalPlan plan,
            final PipelineTreeBuilder.PipelineDefinitionBuild lhs, final PipelineTreeBuilder.ApplyBufferDefinitionBuild argument )
    {
        PipelineTreeBuilder.ApplyBufferDefinitionBuild var4;
        if ( plan instanceof Apply )
        {
            int argumentSlotOffset = ((SlotConfiguration) this.slotConfigurations.apply( plan.id() )).getArgumentLongOffsetFor( plan.id() );
            var4 = this.outputToApplyBuffer( lhs, argumentSlotOffset, plan );
        }
        else if ( plan instanceof CartesianProduct )
        {
            SlotConfiguration rhsSlots = (SlotConfiguration) this.slotConfigurations.apply( ((LogicalPlan) plan.rhs().get()).id() );
            int argumentSlotOffset = rhsSlots.getArgumentLongOffsetFor( plan.id() );
            SlotConfiguration.Size outerArgumentSize =
                    (SlotConfiguration.Size) this.argumentSizes.get( org.neo4j.cypher.internal.logical.plans.LogicalPlans..MODULE$.leftLeaf( plan ).id());
            int outerArgumentSlotOffset = argument.argumentSlotOffset();
            var4 = this.outputToAttachApplyBuffer( lhs, plan.id(), argumentSlotOffset, rhsSlots, outerArgumentSlotOffset, outerArgumentSize ).applyBuffer();
        }
        else
        {
            var4 = argument;
        }

        return var4;
    }

    public PipelineTreeBuilder.PipelineDefinitionBuild onTwoChildPlanComingFromRight( final LogicalPlan plan,
            final PipelineTreeBuilder.PipelineDefinitionBuild lhs, final PipelineTreeBuilder.PipelineDefinitionBuild rhs,
            final PipelineTreeBuilder.ApplyBufferDefinitionBuild argument )
    {
        PipelineTreeBuilder.PipelineDefinitionBuild var5;
        if ( plan instanceof Apply )
        {
            Apply var7 = (Apply) plan;
            LogicalPlan applyRhsPlan = rhs.middlePlans().isEmpty() && rhs.fusedPlans().size() <= 1 ? rhs.headPlan() : (rhs.middlePlans().isEmpty()
                                                                                                                       ? (LogicalPlan) rhs.fusedPlans().last()
                                                                                                                       : (LogicalPlan) rhs.middlePlans().last());
            this.applyRhsPlans().update( BoxesRunTime.boxToInteger( var7.id() ), BoxesRunTime.boxToInteger( applyRhsPlan.id() ) );
            var5 = rhs;
        }
        else if ( plan instanceof CartesianProduct )
        {
            if ( !this.breakingPolicy.breakOn( plan ) )
            {
                throw new UnsupportedOperationException(
                        (new StringBuilder( 34 )).append( "Not breaking on " ).append( plan.getClass().getSimpleName() ).append(
                                " is not supported." ).toString() );
            }

            PipelineTreeBuilder.PipelineDefinitionBuild pipeline = this.newPipeline( plan );
            PipelineTreeBuilder.LHSAccumulatingRHSStreamingBufferDefinitionBuild buffer =
                    this.outputToLhsAccumulatingRhsStreamingBuffer( PipelineTreeBuilder$.MODULE$.NO_PIPELINE_BUILD(), rhs, plan.id(), argument,
                            argument.argumentSlotOffset() );
            pipeline.inputBuffer_$eq( buffer );
            pipeline.lhs_$eq( lhs.id() );
            pipeline.rhs_$eq( rhs.id() );
            var5 = pipeline;
        }
        else
        {
            if ( !(plan instanceof NodeHashJoin) )
            {
                throw new MatchError( plan );
            }

            if ( !this.breakingPolicy.breakOn( plan ) )
            {
                throw new UnsupportedOperationException(
                        (new StringBuilder( 34 )).append( "Not breaking on " ).append( plan.getClass().getSimpleName() ).append(
                                " is not supported." ).toString() );
            }

            PipelineTreeBuilder.PipelineDefinitionBuild pipeline = this.newPipeline( plan );
            PipelineTreeBuilder.LHSAccumulatingRHSStreamingBufferDefinitionBuild buffer =
                    this.outputToLhsAccumulatingRhsStreamingBuffer( lhs, rhs, plan.id(), argument, argument.argumentSlotOffset() );
            pipeline.inputBuffer_$eq( buffer );
            pipeline.lhs_$eq( lhs.id() );
            pipeline.rhs_$eq( rhs.id() );
            var5 = pipeline;
        }

        return var5;
    }

    private void markReducerInUpstreamBuffers( final PipelineTreeBuilder.BufferDefinitionBuild buffer,
            final PipelineTreeBuilder.ApplyBufferDefinitionBuild applyBuffer, final PipelineTreeBuilder.ArgumentStateDefinitionBuild argumentStateDefinition )
    {
        PipelineTreeBuilder.DownstreamReduce downstreamReduce = new PipelineTreeBuilder.DownstreamReduce( argumentStateDefinition.id() );
        this.traverseBuffers( buffer, applyBuffer, ( inputBuffer ) -> {
            $anonfun$markReducerInUpstreamBuffers$1( downstreamReduce, inputBuffer );
            return BoxedUnit.UNIT;
        }, ( lHSAccumulatingRHSStreamingBufferDefinition ) -> {
            $anonfun$markReducerInUpstreamBuffers$2( downstreamReduce, lHSAccumulatingRHSStreamingBufferDefinition );
            return BoxedUnit.UNIT;
        }, ( delegateBuffer ) -> {
            $anonfun$markReducerInUpstreamBuffers$3( downstreamReduce, delegateBuffer );
            return BoxedUnit.UNIT;
        }, ( lastDelegateBuffer ) -> {
            $anonfun$markReducerInUpstreamBuffers$4( argumentStateDefinition, downstreamReduce, lastDelegateBuffer );
            return BoxedUnit.UNIT;
        } );
    }

    private void markInUpstreamBuffers( final PipelineTreeBuilder.BufferDefinitionBuild buffer,
            final PipelineTreeBuilder.ApplyBufferDefinitionBuild applyBuffer, final PipelineTreeBuilder.DownstreamStateOperator downstreamState )
    {
        this.traverseBuffers( buffer, applyBuffer, ( inputBuffer ) -> {
            $anonfun$markInUpstreamBuffers$1( downstreamState, inputBuffer );
            return BoxedUnit.UNIT;
        }, ( lHSAccumulatingRHSStreamingBufferDefinition ) -> {
            $anonfun$markInUpstreamBuffers$2( downstreamState, lHSAccumulatingRHSStreamingBufferDefinition );
            return BoxedUnit.UNIT;
        }, ( delegateBuffer ) -> {
            $anonfun$markInUpstreamBuffers$3( downstreamState, delegateBuffer );
            return BoxedUnit.UNIT;
        }, ( lastDelegateBuffer ) -> {
            $anonfun$markInUpstreamBuffers$4( downstreamState, lastDelegateBuffer );
            return BoxedUnit.UNIT;
        } );
    }

    private void traverseBuffers( final PipelineTreeBuilder.BufferDefinitionBuild buffer, final PipelineTreeBuilder.ApplyBufferDefinitionBuild applyBuffer,
            final Function1<PipelineTreeBuilder.BufferDefinitionBuild,BoxedUnit> onInputBuffer,
            final Function1<PipelineTreeBuilder.LHSAccumulatingRHSStreamingBufferDefinitionBuild,BoxedUnit> onLHSAccumulatingRHSStreamingBuffer,
            final Function1<PipelineTreeBuilder.DelegateBufferDefinitionBuild,BoxedUnit> onDelegateBuffer,
            final Function1<PipelineTreeBuilder.DelegateBufferDefinitionBuild,BoxedUnit> onLastDelegate )
    {
        Seq buffers = (Seq) scala.collection.Seq..
        MODULE$.apply( scala.Predef..MODULE$.wrapRefArray( (Object[]) (new PipelineTreeBuilder.BufferDefinitionBuild[]{buffer}) ));
        this.bfs$1( buffers, applyBuffer, onInputBuffer, onLHSAccumulatingRHSStreamingBuffer, onDelegateBuffer, onLastDelegate );
    }

    private final boolean canFuseOverPipeline$1( final LogicalPlan plan$1, final PipelineTreeBuilder.PipelineDefinitionBuild source$1 )
    {
        return source$1.fusedPlans().nonEmpty() && source$1.middlePlans().isEmpty() && this.operatorFusionPolicy.canFuseOverPipeline( plan$1 );
    }

    private final boolean canFuse$1( final LogicalPlan plan$1, final PipelineTreeBuilder.PipelineDefinitionBuild source$1 )
    {
        return source$1.fusedPlans().nonEmpty() && source$1.middlePlans().isEmpty() && this.operatorFusionPolicy.canFuse( plan$1 );
    }

    private final void bfs$1( final Seq buffers, final PipelineTreeBuilder.ApplyBufferDefinitionBuild applyBuffer$1, final Function1 onInputBuffer$1,
            final Function1 onLHSAccumulatingRHSStreamingBuffer$1, final Function1 onDelegateBuffer$1, final Function1 onLastDelegate$1 )
    {
        while ( true )
        {
            ArrayBuffer upstreams = new ArrayBuffer();
            ((Seq) buffers).foreach( ( x0$1 ) -> {
                Object var8;
                PipelineTreeBuilder.DelegateBufferDefinitionBuild var10;
                label54:
                {
                    boolean var9 = false;
                    var10 = null;
                    if ( x0$1 instanceof PipelineTreeBuilder.DelegateBufferDefinitionBuild )
                    {
                        var9 = true;
                        var10 = (PipelineTreeBuilder.DelegateBufferDefinitionBuild) x0$1;
                        PipelineTreeBuilder.ApplyBufferDefinitionBuild var10000 = var10.applyBuffer();
                        if ( var10000 == null )
                        {
                            if ( applyBuffer$1 == null )
                            {
                                break label54;
                            }
                        }
                        else if ( var10000.equals( applyBuffer$1 ) )
                        {
                            break label54;
                        }
                    }

                    if ( x0$1 instanceof PipelineTreeBuilder.AttachBufferDefinitionBuild )
                    {
                        throw new IllegalStateException( "Nothing should have an attach buffer as immediate input, it should have a delegate buffer instead." );
                    }

                    if ( x0$1 instanceof PipelineTreeBuilder.ApplyBufferDefinitionBuild )
                    {
                        throw new IllegalStateException( "Nothing should have an apply buffer as immediate input, it should have a delegate buffer instead." );
                    }

                    if ( x0$1 instanceof PipelineTreeBuilder.LHSAccumulatingRHSStreamingBufferDefinitionBuild )
                    {
                        PipelineTreeBuilder.LHSAccumulatingRHSStreamingBufferDefinitionBuild var13 =
                                (PipelineTreeBuilder.LHSAccumulatingRHSStreamingBufferDefinitionBuild) x0$1;
                        onLHSAccumulatingRHSStreamingBuffer$1.apply( var13 );
                        var8 = upstreams.$plus$eq(
                                ((PipelineTreeBuilder.PipelineDefinitionBuild) this.pipelines().apply( var13.rhsPipelineId() )).inputBuffer() );
                        return var8;
                    }
                    else if ( var9 )
                    {
                        onDelegateBuffer$1.apply( var10 );
                        var8 = upstreams.$plus$eq( ((PipelineTreeBuilder.PipelineDefinitionBuild) this.pipelines().apply(
                                var10.applyBuffer().producingPipelineId() )).inputBuffer() );
                        return var8;
                    }
                    else
                    {
                        if ( x0$1 instanceof PipelineTreeBuilder.MorselBufferDefinitionBuild )
                        {
                            PipelineTreeBuilder.MorselBufferDefinitionBuild var14 = (PipelineTreeBuilder.MorselBufferDefinitionBuild) x0$1;
                            onInputBuffer$1.apply( var14 );
                            var8 = upstreams.$plus$eq(
                                    ((PipelineTreeBuilder.PipelineDefinitionBuild) this.pipelines().apply( var14.producingPipelineId() )).inputBuffer() );
                        }
                        else
                        {
                            if ( !(x0$1 instanceof PipelineTreeBuilder.OptionalMorselBufferDefinitionBuild) )
                            {
                                throw new MatchError( x0$1 );
                            }

                            PipelineTreeBuilder.OptionalMorselBufferDefinitionBuild var15 = (PipelineTreeBuilder.OptionalMorselBufferDefinitionBuild) x0$1;
                            onInputBuffer$1.apply( var15 );
                            var8 = upstreams.$plus$eq(
                                    ((PipelineTreeBuilder.PipelineDefinitionBuild) this.pipelines().apply( var15.producingPipelineId() )).inputBuffer() );
                        }

                        return var8;
                    }
                }

                var8 = onLastDelegate$1.apply( var10 );
                return var8;
            } );
            if ( !upstreams.nonEmpty() )
            {
                BoxedUnit var10000 = BoxedUnit.UNIT;
                return;
            }

            buffers = upstreams;
        }
    }

    public interface DownstreamStateOperator
    {
    }

    public static class ApplyBufferDefinitionBuild extends PipelineTreeBuilder.MorselBufferDefinitionBuild
    {
        private final int argumentSlotOffset;
        private final ArrayBuffer<PipelineTreeBuilder.ArgumentStateDefinitionBuild> reducersOnRHS;
        private final ArrayBuffer<BufferId> delegates;

        public ApplyBufferDefinitionBuild( final int id, final int producingPipelineId, final int argumentSlotOffset,
                final SlotConfiguration bufferSlotConfiguration )
        {
            super( id, producingPipelineId, bufferSlotConfiguration );
            this.argumentSlotOffset = argumentSlotOffset;
            this.reducersOnRHS = new ArrayBuffer();
            this.delegates = new ArrayBuffer();
        }

        public int argumentSlotOffset()
        {
            return this.argumentSlotOffset;
        }

        public ArrayBuffer<PipelineTreeBuilder.ArgumentStateDefinitionBuild> reducersOnRHS()
        {
            return this.reducersOnRHS;
        }

        public ArrayBuffer<BufferId> delegates()
        {
            return this.delegates;
        }
    }

    public static class ArgumentStateBufferDefinitionBuild extends PipelineTreeBuilder.MorselBufferDefinitionBuild
    {
        private final int argumentStateMapId;

        public ArgumentStateBufferDefinitionBuild( final int id, final int producingPipelineId, final int argumentStateMapId,
                final SlotConfiguration bufferSlotConfiguration )
        {
            super( id, producingPipelineId, bufferSlotConfiguration );
            this.argumentStateMapId = argumentStateMapId;
        }

        public int argumentStateMapId()
        {
            return this.argumentStateMapId;
        }
    }

    public static class ArgumentStateDefinitionBuild implements Product, Serializable
    {
        private final int id;
        private final int planId;
        private final int argumentSlotOffset;
        private final boolean counts;

        public ArgumentStateDefinitionBuild( final int id, final int planId, final int argumentSlotOffset, final boolean counts )
        {
            this.id = id;
            this.planId = planId;
            this.argumentSlotOffset = argumentSlotOffset;
            this.counts = counts;
            Product.$init$( this );
        }

        public int id()
        {
            return this.id;
        }

        public int planId()
        {
            return this.planId;
        }

        public int argumentSlotOffset()
        {
            return this.argumentSlotOffset;
        }

        public boolean counts()
        {
            return this.counts;
        }

        public PipelineTreeBuilder.ArgumentStateDefinitionBuild copy( final int id, final int planId, final int argumentSlotOffset, final boolean counts )
        {
            return new PipelineTreeBuilder.ArgumentStateDefinitionBuild( id, planId, argumentSlotOffset, counts );
        }

        public int copy$default$1()
        {
            return this.id();
        }

        public int copy$default$2()
        {
            return this.planId();
        }

        public int copy$default$3()
        {
            return this.argumentSlotOffset();
        }

        public boolean copy$default$4()
        {
            return this.counts();
        }

        public String productPrefix()
        {
            return "ArgumentStateDefinitionBuild";
        }

        public int productArity()
        {
            return 4;
        }

        public Object productElement( final int x$1 )
        {
            Object var10000;
            switch ( x$1 )
            {
            case 0:
                var10000 = new ArgumentStateMapId( this.id() );
                break;
            case 1:
                var10000 = new Id( this.planId() );
                break;
            case 2:
                var10000 = BoxesRunTime.boxToInteger( this.argumentSlotOffset() );
                break;
            case 3:
                var10000 = BoxesRunTime.boxToBoolean( this.counts() );
                break;
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }

            return var10000;
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof PipelineTreeBuilder.ArgumentStateDefinitionBuild;
        }

        public int hashCode()
        {
            int var1 = -889275714;
            var1 = Statics.mix( var1, Statics.anyHash( new ArgumentStateMapId( this.id() ) ) );
            var1 = Statics.mix( var1, Statics.anyHash( new Id( this.planId() ) ) );
            var1 = Statics.mix( var1, this.argumentSlotOffset() );
            var1 = Statics.mix( var1, this.counts() ? 1231 : 1237 );
            return Statics.finalizeHash( var1, 4 );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var10000;
            label49:
            {
                if ( this != x$1 )
                {
                    boolean var2;
                    if ( x$1 instanceof PipelineTreeBuilder.ArgumentStateDefinitionBuild )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( !var2 )
                    {
                        break label49;
                    }

                    PipelineTreeBuilder.ArgumentStateDefinitionBuild var4 = (PipelineTreeBuilder.ArgumentStateDefinitionBuild) x$1;
                    if ( this.id() != var4.id() || this.planId() != var4.planId() || this.argumentSlotOffset() != var4.argumentSlotOffset() ||
                            this.counts() != var4.counts() || !var4.canEqual( this ) )
                    {
                        break label49;
                    }
                }

                var10000 = true;
                return var10000;
            }

            var10000 = false;
            return var10000;
        }
    }

    public static class ArgumentStateDefinitionBuild$
            extends AbstractFunction4<ArgumentStateMapId,Id,Object,Object,PipelineTreeBuilder.ArgumentStateDefinitionBuild> implements Serializable
    {
        public static PipelineTreeBuilder.ArgumentStateDefinitionBuild$ MODULE$;

        static
        {
            new PipelineTreeBuilder.ArgumentStateDefinitionBuild$();
        }

        public ArgumentStateDefinitionBuild$()
        {
            MODULE$ = this;
        }

        public final String toString()
        {
            return "ArgumentStateDefinitionBuild";
        }

        public PipelineTreeBuilder.ArgumentStateDefinitionBuild apply( final int id, final int planId, final int argumentSlotOffset, final boolean counts )
        {
            return new PipelineTreeBuilder.ArgumentStateDefinitionBuild( id, planId, argumentSlotOffset, counts );
        }

        public Option<Tuple4<ArgumentStateMapId,Id,Object,Object>> unapply( final PipelineTreeBuilder.ArgumentStateDefinitionBuild x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some(
                new Tuple4( new ArgumentStateMapId( x$0.id() ), new Id( x$0.planId() ), BoxesRunTime.boxToInteger( x$0.argumentSlotOffset() ),
                        BoxesRunTime.boxToBoolean( x$0.counts() ) ) ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }

    public static class AttachBufferDefinitionBuild extends PipelineTreeBuilder.BufferDefinitionBuild
    {
        private final SlotConfiguration outputSlotConfiguration;
        private final int argumentSlotOffset;
        private final SlotConfiguration.Size argumentSize;
        private PipelineTreeBuilder.ApplyBufferDefinitionBuild applyBuffer;

        public AttachBufferDefinitionBuild( final int id, final SlotConfiguration inputSlotConfiguration, final SlotConfiguration outputSlotConfiguration,
                final int argumentSlotOffset, final SlotConfiguration.Size argumentSize )
        {
            super( id, inputSlotConfiguration );
            this.outputSlotConfiguration = outputSlotConfiguration;
            this.argumentSlotOffset = argumentSlotOffset;
            this.argumentSize = argumentSize;
        }

        public SlotConfiguration outputSlotConfiguration()
        {
            return this.outputSlotConfiguration;
        }

        public int argumentSlotOffset()
        {
            return this.argumentSlotOffset;
        }

        public SlotConfiguration.Size argumentSize()
        {
            return this.argumentSize;
        }

        public PipelineTreeBuilder.ApplyBufferDefinitionBuild applyBuffer()
        {
            return this.applyBuffer;
        }

        public void applyBuffer_$eq( final PipelineTreeBuilder.ApplyBufferDefinitionBuild x$1 )
        {
            this.applyBuffer = x$1;
        }
    }

    public abstract static class BufferDefinitionBuild
    {
        private final int id;
        private final SlotConfiguration bufferConfiguration;
        private final ArrayBuffer<PipelineTreeBuilder.DownstreamStateOperator> downstreamStates;

        public BufferDefinitionBuild( final int id, final SlotConfiguration bufferConfiguration )
        {
            this.id = id;
            this.bufferConfiguration = bufferConfiguration;
            this.downstreamStates = new ArrayBuffer();
        }

        public int id()
        {
            return this.id;
        }

        public SlotConfiguration bufferConfiguration()
        {
            return this.bufferConfiguration;
        }

        public ArrayBuffer<PipelineTreeBuilder.DownstreamStateOperator> downstreamStates()
        {
            return this.downstreamStates;
        }
    }

    public static class DelegateBufferDefinitionBuild extends PipelineTreeBuilder.BufferDefinitionBuild
    {
        private final PipelineTreeBuilder.ApplyBufferDefinitionBuild applyBuffer;

        public DelegateBufferDefinitionBuild( final int id, final PipelineTreeBuilder.ApplyBufferDefinitionBuild applyBuffer,
                final SlotConfiguration bufferConfiguration )
        {
            super( id, bufferConfiguration );
            this.applyBuffer = applyBuffer;
        }

        public PipelineTreeBuilder.ApplyBufferDefinitionBuild applyBuffer()
        {
            return this.applyBuffer;
        }
    }

    public static class DownstreamReduce implements PipelineTreeBuilder.DownstreamStateOperator, Product, Serializable
    {
        private final int id;

        public DownstreamReduce( final int id )
        {
            this.id = id;
            Product.$init$( this );
        }

        public int id()
        {
            return this.id;
        }

        public PipelineTreeBuilder.DownstreamReduce copy( final int id )
        {
            return new PipelineTreeBuilder.DownstreamReduce( id );
        }

        public int copy$default$1()
        {
            return this.id();
        }

        public String productPrefix()
        {
            return "DownstreamReduce";
        }

        public int productArity()
        {
            return 1;
        }

        public Object productElement( final int x$1 )
        {
            switch ( x$1 )
            {
            case 0:
                return new ArgumentStateMapId( this.id() );
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof PipelineTreeBuilder.DownstreamReduce;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var10000;
            if ( this != x$1 )
            {
                label49:
                {
                    boolean var2;
                    if ( x$1 instanceof PipelineTreeBuilder.DownstreamReduce )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        PipelineTreeBuilder.DownstreamReduce var4 = (PipelineTreeBuilder.DownstreamReduce) x$1;
                        if ( this.id() == var4.id() && var4.canEqual( this ) )
                        {
                            break label49;
                        }
                    }

                    var10000 = false;
                    return var10000;
                }
            }

            var10000 = true;
            return var10000;
        }
    }

    public static class DownstreamReduce$ extends AbstractFunction1<ArgumentStateMapId,PipelineTreeBuilder.DownstreamReduce> implements Serializable
    {
        public static PipelineTreeBuilder.DownstreamReduce$ MODULE$;

        static
        {
            new PipelineTreeBuilder.DownstreamReduce$();
        }

        public DownstreamReduce$()
        {
            MODULE$ = this;
        }

        public final String toString()
        {
            return "DownstreamReduce";
        }

        public PipelineTreeBuilder.DownstreamReduce apply( final int id )
        {
            return new PipelineTreeBuilder.DownstreamReduce( id );
        }

        public Option<ArgumentStateMapId> unapply( final PipelineTreeBuilder.DownstreamReduce x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new ArgumentStateMapId( x$0.id() ) ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }

    public static class DownstreamState implements PipelineTreeBuilder.DownstreamStateOperator, Product, Serializable
    {
        private final int id;

        public DownstreamState( final int id )
        {
            this.id = id;
            Product.$init$( this );
        }

        public int id()
        {
            return this.id;
        }

        public PipelineTreeBuilder.DownstreamState copy( final int id )
        {
            return new PipelineTreeBuilder.DownstreamState( id );
        }

        public int copy$default$1()
        {
            return this.id();
        }

        public String productPrefix()
        {
            return "DownstreamState";
        }

        public int productArity()
        {
            return 1;
        }

        public Object productElement( final int x$1 )
        {
            switch ( x$1 )
            {
            case 0:
                return new ArgumentStateMapId( this.id() );
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof PipelineTreeBuilder.DownstreamState;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var10000;
            if ( this != x$1 )
            {
                label49:
                {
                    boolean var2;
                    if ( x$1 instanceof PipelineTreeBuilder.DownstreamState )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        PipelineTreeBuilder.DownstreamState var4 = (PipelineTreeBuilder.DownstreamState) x$1;
                        if ( this.id() == var4.id() && var4.canEqual( this ) )
                        {
                            break label49;
                        }
                    }

                    var10000 = false;
                    return var10000;
                }
            }

            var10000 = true;
            return var10000;
        }
    }

    public static class DownstreamState$ extends AbstractFunction1<ArgumentStateMapId,PipelineTreeBuilder.DownstreamState> implements Serializable
    {
        public static PipelineTreeBuilder.DownstreamState$ MODULE$;

        static
        {
            new PipelineTreeBuilder.DownstreamState$();
        }

        public DownstreamState$()
        {
            MODULE$ = this;
        }

        public final String toString()
        {
            return "DownstreamState";
        }

        public PipelineTreeBuilder.DownstreamState apply( final int id )
        {
            return new PipelineTreeBuilder.DownstreamState( id );
        }

        public Option<ArgumentStateMapId> unapply( final PipelineTreeBuilder.DownstreamState x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new ArgumentStateMapId( x$0.id() ) ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }

    public static class DownstreamWorkCanceller implements PipelineTreeBuilder.DownstreamStateOperator, Product, Serializable
    {
        private final int id;

        public DownstreamWorkCanceller( final int id )
        {
            this.id = id;
            Product.$init$( this );
        }

        public int id()
        {
            return this.id;
        }

        public PipelineTreeBuilder.DownstreamWorkCanceller copy( final int id )
        {
            return new PipelineTreeBuilder.DownstreamWorkCanceller( id );
        }

        public int copy$default$1()
        {
            return this.id();
        }

        public String productPrefix()
        {
            return "DownstreamWorkCanceller";
        }

        public int productArity()
        {
            return 1;
        }

        public Object productElement( final int x$1 )
        {
            switch ( x$1 )
            {
            case 0:
                return new ArgumentStateMapId( this.id() );
            default:
                throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
            }
        }

        public Iterator<Object> productIterator()
        {
            return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
        }

        public boolean canEqual( final Object x$1 )
        {
            return x$1 instanceof PipelineTreeBuilder.DownstreamWorkCanceller;
        }

        public int hashCode()
        {
            return scala.runtime.ScalaRunTime..MODULE$._hashCode( this );
        }

        public String toString()
        {
            return scala.runtime.ScalaRunTime..MODULE$._toString( this );
        }

        public boolean equals( final Object x$1 )
        {
            boolean var10000;
            if ( this != x$1 )
            {
                label49:
                {
                    boolean var2;
                    if ( x$1 instanceof PipelineTreeBuilder.DownstreamWorkCanceller )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }

                    if ( var2 )
                    {
                        PipelineTreeBuilder.DownstreamWorkCanceller var4 = (PipelineTreeBuilder.DownstreamWorkCanceller) x$1;
                        if ( this.id() == var4.id() && var4.canEqual( this ) )
                        {
                            break label49;
                        }
                    }

                    var10000 = false;
                    return var10000;
                }
            }

            var10000 = true;
            return var10000;
        }
    }

    public static class DownstreamWorkCanceller$ extends AbstractFunction1<ArgumentStateMapId,PipelineTreeBuilder.DownstreamWorkCanceller>
            implements Serializable
    {
        public static PipelineTreeBuilder.DownstreamWorkCanceller$ MODULE$;

        static
        {
            new PipelineTreeBuilder.DownstreamWorkCanceller$();
        }

        public DownstreamWorkCanceller$()
        {
            MODULE$ = this;
        }

        public final String toString()
        {
            return "DownstreamWorkCanceller";
        }

        public PipelineTreeBuilder.DownstreamWorkCanceller apply( final int id )
        {
            return new PipelineTreeBuilder.DownstreamWorkCanceller( id );
        }

        public Option<ArgumentStateMapId> unapply( final PipelineTreeBuilder.DownstreamWorkCanceller x$0 )
        {
            return (Option) (x$0 == null ? scala.None..MODULE$ :new Some( new ArgumentStateMapId( x$0.id() ) ));
        }

        private Object readResolve()
        {
            return MODULE$;
        }
    }

    public static class ExecutionStateDefinitionBuild
    {
        private final PhysicalPlan physicalPlan;
        private final ArrayBuffer<PipelineTreeBuilder.BufferDefinitionBuild> buffers;
        private final ArrayBuffer<PipelineTreeBuilder.ArgumentStateDefinitionBuild> argumentStateMaps;
        private PipelineTreeBuilder.ApplyBufferDefinitionBuild initBuffer;

        public ExecutionStateDefinitionBuild( final PhysicalPlan physicalPlan )
        {
            this.physicalPlan = physicalPlan;
            this.buffers = new ArrayBuffer();
            this.argumentStateMaps = new ArrayBuffer();
        }

        public PhysicalPlan physicalPlan()
        {
            return this.physicalPlan;
        }

        public ArrayBuffer<PipelineTreeBuilder.BufferDefinitionBuild> buffers()
        {
            return this.buffers;
        }

        public ArrayBuffer<PipelineTreeBuilder.ArgumentStateDefinitionBuild> argumentStateMaps()
        {
            return this.argumentStateMaps;
        }

        public PipelineTreeBuilder.ApplyBufferDefinitionBuild initBuffer()
        {
            return this.initBuffer;
        }

        public void initBuffer_$eq( final PipelineTreeBuilder.ApplyBufferDefinitionBuild x$1 )
        {
            this.initBuffer = x$1;
        }

        public PipelineTreeBuilder.ArgumentStateDefinitionBuild newArgumentStateMap( final int planId, final int argumentSlotOffset, final boolean counts )
        {
            int x = this.argumentStateMaps().size();
            PipelineTreeBuilder.ArgumentStateDefinitionBuild asm =
                    new PipelineTreeBuilder.ArgumentStateDefinitionBuild( x, planId, argumentSlotOffset, counts );
            this.argumentStateMaps().$plus$eq( asm );
            return asm;
        }

        public PipelineTreeBuilder.MorselBufferDefinitionBuild newBuffer( final int producingPipelineId, final SlotConfiguration bufferSlotConfiguration )
        {
            int x = this.buffers().size();
            PipelineTreeBuilder.MorselBufferDefinitionBuild buffer =
                    new PipelineTreeBuilder.MorselBufferDefinitionBuild( x, producingPipelineId, bufferSlotConfiguration );
            this.buffers().$plus$eq( buffer );
            return buffer;
        }

        public PipelineTreeBuilder.OptionalMorselBufferDefinitionBuild newOptionalBuffer( final int producingPipelineId, final int argumentStateMapId,
                final int argumentSlotOffset, final SlotConfiguration bufferSlotConfiguration )
        {
            int x = this.buffers().size();
            PipelineTreeBuilder.OptionalMorselBufferDefinitionBuild buffer =
                    new PipelineTreeBuilder.OptionalMorselBufferDefinitionBuild( x, producingPipelineId, argumentStateMapId, argumentSlotOffset,
                            bufferSlotConfiguration );
            this.buffers().$plus$eq( buffer );
            return buffer;
        }

        public PipelineTreeBuilder.DelegateBufferDefinitionBuild newDelegateBuffer( final PipelineTreeBuilder.ApplyBufferDefinitionBuild applyBufferDefinition,
                final SlotConfiguration bufferSlotConfiguration )
        {
            int x = this.buffers().size();
            PipelineTreeBuilder.DelegateBufferDefinitionBuild buffer =
                    new PipelineTreeBuilder.DelegateBufferDefinitionBuild( x, applyBufferDefinition, bufferSlotConfiguration );
            this.buffers().$plus$eq( buffer );
            return buffer;
        }

        public PipelineTreeBuilder.ApplyBufferDefinitionBuild newApplyBuffer( final int producingPipelineId, final int argumentSlotOffset,
                final SlotConfiguration bufferSlotConfiguration )
        {
            int x = this.buffers().size();
            PipelineTreeBuilder.ApplyBufferDefinitionBuild buffer =
                    new PipelineTreeBuilder.ApplyBufferDefinitionBuild( x, producingPipelineId, argumentSlotOffset, bufferSlotConfiguration );
            this.buffers().$plus$eq( buffer );
            return buffer;
        }

        public PipelineTreeBuilder.AttachBufferDefinitionBuild newAttachBuffer( final int producingPipelineId, final SlotConfiguration inputSlotConfiguration,
                final SlotConfiguration postAttachSlotConfiguration, final int outerArgumentSlotOffset, final SlotConfiguration.Size outerArgumentSize )
        {
            int x = this.buffers().size();
            PipelineTreeBuilder.AttachBufferDefinitionBuild buffer =
                    new PipelineTreeBuilder.AttachBufferDefinitionBuild( x, inputSlotConfiguration, postAttachSlotConfiguration, outerArgumentSlotOffset,
                            outerArgumentSize );
            this.buffers().$plus$eq( buffer );
            return buffer;
        }

        public PipelineTreeBuilder.ArgumentStateBufferDefinitionBuild newArgumentStateBuffer( final int producingPipelineId, final int argumentStateMapId,
                final SlotConfiguration bufferSlotConfiguration )
        {
            int x = this.buffers().size();
            PipelineTreeBuilder.ArgumentStateBufferDefinitionBuild buffer =
                    new PipelineTreeBuilder.ArgumentStateBufferDefinitionBuild( x, producingPipelineId, argumentStateMapId, bufferSlotConfiguration );
            this.buffers().$plus$eq( buffer );
            return buffer;
        }

        public PipelineTreeBuilder.LHSAccumulatingRHSStreamingBufferDefinitionBuild newLhsAccumulatingRhsStreamingBuffer( final int lhsProducingPipelineId,
                final int rhsProducingPipelineId, final int lhsargumentStateMapId, final int rhsargumentStateMapId,
                final SlotConfiguration bufferSlotConfiguration )
        {
            int x = this.buffers().size();
            PipelineTreeBuilder.LHSAccumulatingRHSStreamingBufferDefinitionBuild buffer =
                    new PipelineTreeBuilder.LHSAccumulatingRHSStreamingBufferDefinitionBuild( x, lhsProducingPipelineId, rhsProducingPipelineId,
                            lhsargumentStateMapId, rhsargumentStateMapId, bufferSlotConfiguration );
            this.buffers().$plus$eq( buffer );
            return buffer;
        }
    }

    public static class LHSAccumulatingRHSStreamingBufferDefinitionBuild extends PipelineTreeBuilder.BufferDefinitionBuild
    {
        private final int lhsPipelineId;
        private final int rhsPipelineId;
        private final int lhsArgumentStateMapId;
        private final int rhsArgumentStateMapId;

        public LHSAccumulatingRHSStreamingBufferDefinitionBuild( final int id, final int lhsPipelineId, final int rhsPipelineId,
                final int lhsArgumentStateMapId, final int rhsArgumentStateMapId, final SlotConfiguration bufferSlotConfiguration )
        {
            super( id, bufferSlotConfiguration );
            this.lhsPipelineId = lhsPipelineId;
            this.rhsPipelineId = rhsPipelineId;
            this.lhsArgumentStateMapId = lhsArgumentStateMapId;
            this.rhsArgumentStateMapId = rhsArgumentStateMapId;
        }

        public int lhsPipelineId()
        {
            return this.lhsPipelineId;
        }

        public int rhsPipelineId()
        {
            return this.rhsPipelineId;
        }

        public int lhsArgumentStateMapId()
        {
            return this.lhsArgumentStateMapId;
        }

        public int rhsArgumentStateMapId()
        {
            return this.rhsArgumentStateMapId;
        }
    }

    public static class MorselBufferDefinitionBuild extends PipelineTreeBuilder.BufferDefinitionBuild
    {
        private final int producingPipelineId;

        public MorselBufferDefinitionBuild( final int id, final int producingPipelineId, final SlotConfiguration bufferConfiguration )
        {
            super( id, bufferConfiguration );
            this.producingPipelineId = producingPipelineId;
        }

        public int producingPipelineId()
        {
            return this.producingPipelineId;
        }
    }

    public static class OptionalMorselBufferDefinitionBuild extends PipelineTreeBuilder.BufferDefinitionBuild
    {
        private final int producingPipelineId;
        private final int argumentStateMapId;
        private final int argumentSlotOffset;

        public OptionalMorselBufferDefinitionBuild( final int id, final int producingPipelineId, final int argumentStateMapId, final int argumentSlotOffset,
                final SlotConfiguration bufferConfiguration )
        {
            super( id, bufferConfiguration );
            this.producingPipelineId = producingPipelineId;
            this.argumentStateMapId = argumentStateMapId;
            this.argumentSlotOffset = argumentSlotOffset;
        }

        public int producingPipelineId()
        {
            return this.producingPipelineId;
        }

        public int argumentStateMapId()
        {
            return this.argumentStateMapId;
        }

        public int argumentSlotOffset()
        {
            return this.argumentSlotOffset;
        }
    }

    public static class PipelineDefinitionBuild
    {
        private final int id;
        private final LogicalPlan headPlan;
        private final ArrayBuffer<LogicalPlan> fusedPlans;
        private final ArrayBuffer<LogicalPlan> middlePlans;
        private int lhs;
        private int rhs;
        private PipelineTreeBuilder.BufferDefinitionBuild inputBuffer;
        private OutputDefinition outputDefinition;
        private boolean serial;

        public PipelineDefinitionBuild( final int id, final LogicalPlan headPlan )
        {
            this.id = id;
            this.headPlan = headPlan;
            this.lhs = PipelineId$.MODULE$.NO_PIPELINE();
            this.rhs = PipelineId$.MODULE$.NO_PIPELINE();
            this.outputDefinition = NoOutput$.MODULE$;
            this.fusedPlans = new ArrayBuffer();
            this.middlePlans = new ArrayBuffer();
            this.serial = false;
        }

        public int id()
        {
            return this.id;
        }

        public LogicalPlan headPlan()
        {
            return this.headPlan;
        }

        public int lhs()
        {
            return this.lhs;
        }

        public void lhs_$eq( final int x$1 )
        {
            this.lhs = x$1;
        }

        public int rhs()
        {
            return this.rhs;
        }

        public void rhs_$eq( final int x$1 )
        {
            this.rhs = x$1;
        }

        public PipelineTreeBuilder.BufferDefinitionBuild inputBuffer()
        {
            return this.inputBuffer;
        }

        public void inputBuffer_$eq( final PipelineTreeBuilder.BufferDefinitionBuild x$1 )
        {
            this.inputBuffer = x$1;
        }

        public OutputDefinition outputDefinition()
        {
            return this.outputDefinition;
        }

        public void outputDefinition_$eq( final OutputDefinition x$1 )
        {
            this.outputDefinition = x$1;
        }

        public ArrayBuffer<LogicalPlan> fusedPlans()
        {
            return this.fusedPlans;
        }

        public ArrayBuffer<LogicalPlan> middlePlans()
        {
            return this.middlePlans;
        }

        public boolean serial()
        {
            return this.serial;
        }

        public void serial_$eq( final boolean x$1 )
        {
            this.serial = x$1;
        }
    }
}
