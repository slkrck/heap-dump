package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id;
import scala.collection.Seq;

public final class PipelineBreakingPolicy$
{
    public static PipelineBreakingPolicy$ MODULE$;

    static
    {
        new PipelineBreakingPolicy$();
    }

    private PipelineBreakingPolicy$()
    {
        MODULE$ = this;
    }

    public PipelineBreakingPolicy breakFor( final Seq<LogicalPlan> logicalPlans )
    {
        return new PipelineBreakingPolicy( logicalPlans )
        {
            private final Seq logicalPlans$1;

            public
            {
                this.logicalPlans$1 = logicalPlans$1;
                PipelineBreakingPolicy.$init$( this );
            }

            public SlotConfiguration invoke( final LogicalPlan lp, final SlotConfiguration slots, final SlotConfiguration argumentSlots )
            {
                return PipelineBreakingPolicy.invoke$( this, lp, slots, argumentSlots );
            }

            public boolean breakOn( final LogicalPlan lp )
            {
                return this.logicalPlans$1.contains( lp );
            }

            public void onNestedPlanBreak()
            {
            }
        };
    }

    public PipelineBreakingPolicy breakForIds( final Seq<Id> ids )
    {
        return new PipelineBreakingPolicy( ids )
        {
            private final Seq ids$1;

            public
            {
                this.ids$1 = ids$1;
                PipelineBreakingPolicy.$init$( this );
            }

            public SlotConfiguration invoke( final LogicalPlan lp, final SlotConfiguration slots, final SlotConfiguration argumentSlots )
            {
                return PipelineBreakingPolicy.invoke$( this, lp, slots, argumentSlots );
            }

            public boolean breakOn( final LogicalPlan lp )
            {
                return this.ids$1.contains( new Id( lp.id() ) );
            }

            public void onNestedPlanBreak()
            {
            }
        };
    }
}
