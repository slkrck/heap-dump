package org.neo4j.cypher.internal.physicalplanning;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;

public final class ArgumentStateBufferVariant$ extends AbstractFunction1<ArgumentStateMapId,ArgumentStateBufferVariant> implements Serializable
{
    public static ArgumentStateBufferVariant$ MODULE$;

    static
    {
        new ArgumentStateBufferVariant$();
    }

    private ArgumentStateBufferVariant$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "ArgumentStateBufferVariant";
    }

    public ArgumentStateBufferVariant apply( final int argumentStateMapId )
    {
        return new ArgumentStateBufferVariant( argumentStateMapId );
    }

    public Option<ArgumentStateMapId> unapply( final ArgumentStateBufferVariant x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new ArgumentStateMapId( x$0.argumentStateMapId() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
