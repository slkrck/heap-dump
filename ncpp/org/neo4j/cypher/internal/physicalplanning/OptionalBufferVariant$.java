package org.neo4j.cypher.internal.physicalplanning;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;

public final class OptionalBufferVariant$ extends AbstractFunction1<ArgumentStateMapId,OptionalBufferVariant> implements Serializable
{
    public static OptionalBufferVariant$ MODULE$;

    static
    {
        new OptionalBufferVariant$();
    }

    private OptionalBufferVariant$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "OptionalBufferVariant";
    }

    public OptionalBufferVariant apply( final int argumentStateMapId )
    {
        return new OptionalBufferVariant( argumentStateMapId );
    }

    public Option<ArgumentStateMapId> unapply( final OptionalBufferVariant x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new ArgumentStateMapId( x$0.argumentStateMapId() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
