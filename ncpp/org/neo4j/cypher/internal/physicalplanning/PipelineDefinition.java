package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple9;
import scala.collection.IndexedSeq;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class PipelineDefinition implements Product, Serializable
{
    private final int id;
    private final int lhs;
    private final int rhs;
    private final LogicalPlan headPlan;
    private final IndexedSeq<LogicalPlan> fusedPlans;
    private final BufferDefinition inputBuffer;
    private final OutputDefinition outputDefinition;
    private final IndexedSeq<LogicalPlan> middlePlans;
    private final boolean serial;

    public PipelineDefinition( final int id, final int lhs, final int rhs, final LogicalPlan headPlan, final IndexedSeq<LogicalPlan> fusedPlans,
            final BufferDefinition inputBuffer, final OutputDefinition outputDefinition, final IndexedSeq<LogicalPlan> middlePlans, final boolean serial )
    {
        this.id = id;
        this.lhs = lhs;
        this.rhs = rhs;
        this.headPlan = headPlan;
        this.fusedPlans = fusedPlans;
        this.inputBuffer = inputBuffer;
        this.outputDefinition = outputDefinition;
        this.middlePlans = middlePlans;
        this.serial = serial;
        Product.$init$( this );
    }

    public static Option<Tuple9<PipelineId,PipelineId,PipelineId,LogicalPlan,IndexedSeq<LogicalPlan>,BufferDefinition,OutputDefinition,IndexedSeq<LogicalPlan>,Object>> unapply(
            final PipelineDefinition x$0 )
    {
        return PipelineDefinition$.MODULE$.unapply( var0 );
    }

    public static PipelineDefinition apply( final int id, final int lhs, final int rhs, final LogicalPlan headPlan, final IndexedSeq<LogicalPlan> fusedPlans,
            final BufferDefinition inputBuffer, final OutputDefinition outputDefinition, final IndexedSeq<LogicalPlan> middlePlans, final boolean serial )
    {
        return PipelineDefinition$.MODULE$.apply( var0, var1, var2, var3, var4, var5, var6, var7, var8 );
    }

    public static Function1<Tuple9<PipelineId,PipelineId,PipelineId,LogicalPlan,IndexedSeq<LogicalPlan>,BufferDefinition,OutputDefinition,IndexedSeq<LogicalPlan>,Object>,PipelineDefinition> tupled()
    {
        return PipelineDefinition$.MODULE$.tupled();
    }

    public static Function1<PipelineId,Function1<PipelineId,Function1<PipelineId,Function1<LogicalPlan,Function1<IndexedSeq<LogicalPlan>,Function1<BufferDefinition,Function1<OutputDefinition,Function1<IndexedSeq<LogicalPlan>,Function1<Object,PipelineDefinition>>>>>>>>> curried()
    {
        return PipelineDefinition$.MODULE$.curried();
    }

    public int id()
    {
        return this.id;
    }

    public int lhs()
    {
        return this.lhs;
    }

    public int rhs()
    {
        return this.rhs;
    }

    public LogicalPlan headPlan()
    {
        return this.headPlan;
    }

    public IndexedSeq<LogicalPlan> fusedPlans()
    {
        return this.fusedPlans;
    }

    public BufferDefinition inputBuffer()
    {
        return this.inputBuffer;
    }

    public OutputDefinition outputDefinition()
    {
        return this.outputDefinition;
    }

    public IndexedSeq<LogicalPlan> middlePlans()
    {
        return this.middlePlans;
    }

    public boolean serial()
    {
        return this.serial;
    }

    public PipelineDefinition copy( final int id, final int lhs, final int rhs, final LogicalPlan headPlan, final IndexedSeq<LogicalPlan> fusedPlans,
            final BufferDefinition inputBuffer, final OutputDefinition outputDefinition, final IndexedSeq<LogicalPlan> middlePlans, final boolean serial )
    {
        return new PipelineDefinition( id, lhs, rhs, headPlan, fusedPlans, inputBuffer, outputDefinition, middlePlans, serial );
    }

    public int copy$default$1()
    {
        return this.id();
    }

    public int copy$default$2()
    {
        return this.lhs();
    }

    public int copy$default$3()
    {
        return this.rhs();
    }

    public LogicalPlan copy$default$4()
    {
        return this.headPlan();
    }

    public IndexedSeq<LogicalPlan> copy$default$5()
    {
        return this.fusedPlans();
    }

    public BufferDefinition copy$default$6()
    {
        return this.inputBuffer();
    }

    public OutputDefinition copy$default$7()
    {
        return this.outputDefinition();
    }

    public IndexedSeq<LogicalPlan> copy$default$8()
    {
        return this.middlePlans();
    }

    public boolean copy$default$9()
    {
        return this.serial();
    }

    public String productPrefix()
    {
        return "PipelineDefinition";
    }

    public int productArity()
    {
        return 9;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = new PipelineId( this.id() );
            break;
        case 1:
            var10000 = new PipelineId( this.lhs() );
            break;
        case 2:
            var10000 = new PipelineId( this.rhs() );
            break;
        case 3:
            var10000 = this.headPlan();
            break;
        case 4:
            var10000 = this.fusedPlans();
            break;
        case 5:
            var10000 = this.inputBuffer();
            break;
        case 6:
            var10000 = this.outputDefinition();
            break;
        case 7:
            var10000 = this.middlePlans();
            break;
        case 8:
            var10000 = BoxesRunTime.boxToBoolean( this.serial() );
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof PipelineDefinition;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.anyHash( new PipelineId( this.id() ) ) );
        var1 = Statics.mix( var1, Statics.anyHash( new PipelineId( this.lhs() ) ) );
        var1 = Statics.mix( var1, Statics.anyHash( new PipelineId( this.rhs() ) ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.headPlan() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.fusedPlans() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.inputBuffer() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.outputDefinition() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.middlePlans() ) );
        var1 = Statics.mix( var1, this.serial() ? 1231 : 1237 );
        return Statics.finalizeHash( var1, 9 );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var13;
        if ( this != x$1 )
        {
            label98:
            {
                boolean var2;
                if ( x$1 instanceof PipelineDefinition )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label80:
                    {
                        PipelineDefinition var4 = (PipelineDefinition) x$1;
                        if ( this.id() == var4.id() && this.lhs() == var4.lhs() && this.rhs() == var4.rhs() )
                        {
                            label89:
                            {
                                LogicalPlan var10000 = this.headPlan();
                                LogicalPlan var5 = var4.headPlan();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label89;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label89;
                                }

                                IndexedSeq var10 = this.fusedPlans();
                                IndexedSeq var6 = var4.fusedPlans();
                                if ( var10 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label89;
                                    }
                                }
                                else if ( !var10.equals( var6 ) )
                                {
                                    break label89;
                                }

                                BufferDefinition var11 = this.inputBuffer();
                                BufferDefinition var7 = var4.inputBuffer();
                                if ( var11 == null )
                                {
                                    if ( var7 != null )
                                    {
                                        break label89;
                                    }
                                }
                                else if ( !var11.equals( var7 ) )
                                {
                                    break label89;
                                }

                                OutputDefinition var12 = this.outputDefinition();
                                OutputDefinition var8 = var4.outputDefinition();
                                if ( var12 == null )
                                {
                                    if ( var8 != null )
                                    {
                                        break label89;
                                    }
                                }
                                else if ( !var12.equals( var8 ) )
                                {
                                    break label89;
                                }

                                var10 = this.middlePlans();
                                IndexedSeq var9 = var4.middlePlans();
                                if ( var10 == null )
                                {
                                    if ( var9 != null )
                                    {
                                        break label89;
                                    }
                                }
                                else if ( !var10.equals( var9 ) )
                                {
                                    break label89;
                                }

                                if ( this.serial() == var4.serial() && var4.canEqual( this ) )
                                {
                                    var13 = true;
                                    break label80;
                                }
                            }
                        }

                        var13 = false;
                    }

                    if ( var13 )
                    {
                        break label98;
                    }
                }

                var13 = false;
                return var13;
            }
        }

        var13 = true;
        return var13;
    }
}
