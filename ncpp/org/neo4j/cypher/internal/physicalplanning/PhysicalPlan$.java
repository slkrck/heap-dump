package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.runtime.ParameterMapping;
import org.neo4j.cypher.internal.runtime.expressionVariableAllocation.AvailableExpressionVariables;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple8;
import scala.None.;
import scala.runtime.AbstractFunction8;
import scala.runtime.BoxesRunTime;

public final class PhysicalPlan$ extends
        AbstractFunction8<LogicalPlan,Object,PhysicalPlanningAttributes.SlotConfigurations,PhysicalPlanningAttributes.ArgumentSizes,PhysicalPlanningAttributes.ApplyPlans,PhysicalPlanningAttributes.NestedPlanArgumentConfigurations,AvailableExpressionVariables,ParameterMapping,PhysicalPlan>
        implements Serializable
{
    public static PhysicalPlan$ MODULE$;

    static
    {
        new PhysicalPlan$();
    }

    private PhysicalPlan$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "PhysicalPlan";
    }

    public PhysicalPlan apply( final LogicalPlan logicalPlan, final int nExpressionSlots,
            final PhysicalPlanningAttributes.SlotConfigurations slotConfigurations, final PhysicalPlanningAttributes.ArgumentSizes argumentSizes,
            final PhysicalPlanningAttributes.ApplyPlans applyPlans,
            final PhysicalPlanningAttributes.NestedPlanArgumentConfigurations nestedPlanArgumentConfigurations,
            final AvailableExpressionVariables availableExpressionVariables, final ParameterMapping parameterMapping )
    {
        return new PhysicalPlan( logicalPlan, nExpressionSlots, slotConfigurations, argumentSizes, applyPlans, nestedPlanArgumentConfigurations,
                availableExpressionVariables, parameterMapping );
    }

    public Option<Tuple8<LogicalPlan,Object,PhysicalPlanningAttributes.SlotConfigurations,PhysicalPlanningAttributes.ArgumentSizes,PhysicalPlanningAttributes.ApplyPlans,PhysicalPlanningAttributes.NestedPlanArgumentConfigurations,AvailableExpressionVariables,ParameterMapping>> unapply(
            final PhysicalPlan x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some(
            new Tuple8( x$0.logicalPlan(), BoxesRunTime.boxToInteger( x$0.nExpressionSlots() ), x$0.slotConfigurations(), x$0.argumentSizes(), x$0.applyPlans(),
                    x$0.nestedPlanArgumentConfigurations(), x$0.availableExpressionVariables(), x$0.parameterMapping() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
