package org.neo4j.cypher.internal.physicalplanning;

import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class ArgumentStateMapId implements Product, Serializable
{
    private final int x;

    public ArgumentStateMapId( final int x )
    {
        this.x = x;
        Product.$init$( this );
    }

    public static String toString$extension( final int $this )
    {
        return ArgumentStateMapId$.MODULE$.toString$extension( var0 );
    }

    public static boolean equals$extension( final int $this, final Object x$1 )
    {
        return ArgumentStateMapId$.MODULE$.equals$extension( var0, var1 );
    }

    public static int hashCode$extension( final int $this )
    {
        return ArgumentStateMapId$.MODULE$.hashCode$extension( var0 );
    }

    public static boolean canEqual$extension( final int $this, final Object x$1 )
    {
        return ArgumentStateMapId$.MODULE$.canEqual$extension( var0, var1 );
    }

    public static Iterator<Object> productIterator$extension( final int $this )
    {
        return ArgumentStateMapId$.MODULE$.productIterator$extension( var0 );
    }

    public static Object productElement$extension( final int $this, final int x$1 )
    {
        return ArgumentStateMapId$.MODULE$.productElement$extension( var0, var1 );
    }

    public static int productArity$extension( final int $this )
    {
        return ArgumentStateMapId$.MODULE$.productArity$extension( var0 );
    }

    public static String productPrefix$extension( final int $this )
    {
        return ArgumentStateMapId$.MODULE$.productPrefix$extension( var0 );
    }

    public static int copy$default$1$extension( final int $this )
    {
        return ArgumentStateMapId$.MODULE$.copy$default$1$extension( var0 );
    }

    public static int copy$extension( final int $this, final int x )
    {
        return ArgumentStateMapId$.MODULE$.copy$extension( var0, var1 );
    }

    public static Option<Object> unapply( final int x$0 )
    {
        return ArgumentStateMapId$.MODULE$.unapply( var0 );
    }

    public static int apply( final int x )
    {
        return ArgumentStateMapId$.MODULE$.apply( var0 );
    }

    public static <A> Function1<Object,A> andThen( final Function1<ArgumentStateMapId,A> g )
    {
        return ArgumentStateMapId$.MODULE$.andThen( var0 );
    }

    public static <A> Function1<A,ArgumentStateMapId> compose( final Function1<A,Object> g )
    {
        return ArgumentStateMapId$.MODULE$.compose( var0 );
    }

    public int x()
    {
        return this.x;
    }

    public int copy( final int x )
    {
        return ArgumentStateMapId$.MODULE$.copy$extension( this.x(), x );
    }

    public int copy$default$1()
    {
        return ArgumentStateMapId$.MODULE$.copy$default$1$extension( this.x() );
    }

    public String productPrefix()
    {
        return ArgumentStateMapId$.MODULE$.productPrefix$extension( this.x() );
    }

    public int productArity()
    {
        return ArgumentStateMapId$.MODULE$.productArity$extension( this.x() );
    }

    public Object productElement( final int x$1 )
    {
        return ArgumentStateMapId$.MODULE$.productElement$extension( this.x(), x$1 );
    }

    public Iterator<Object> productIterator()
    {
        return ArgumentStateMapId$.MODULE$.productIterator$extension( this.x() );
    }

    public boolean canEqual( final Object x$1 )
    {
        return ArgumentStateMapId$.MODULE$.canEqual$extension( this.x(), x$1 );
    }

    public int hashCode()
    {
        return ArgumentStateMapId$.MODULE$.hashCode$extension( this.x() );
    }

    public boolean equals( final Object x$1 )
    {
        return ArgumentStateMapId$.MODULE$.equals$extension( this.x(), x$1 );
    }

    public String toString()
    {
        return ArgumentStateMapId$.MODULE$.toString$extension( this.x() );
    }
}
