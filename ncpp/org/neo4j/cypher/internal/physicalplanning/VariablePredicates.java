package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.logical.plans.VariablePredicate;
import scala.Option;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class VariablePredicates
{
    public static int expressionSlotForPredicate( final Option<VariablePredicate> predicate )
    {
        return VariablePredicates$.MODULE$.expressionSlotForPredicate( var0 );
    }

    public static int NO_PREDICATE_OFFSET()
    {
        return VariablePredicates$.MODULE$.NO_PREDICATE_OFFSET();
    }
}
