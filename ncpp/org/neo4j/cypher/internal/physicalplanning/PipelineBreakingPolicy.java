package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.logical.plans.AggregatingPlan;
import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id;
import scala.collection.Seq;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface PipelineBreakingPolicy
{
    static PipelineBreakingPolicy breakForIds( final Seq<Id> ids )
    {
        return PipelineBreakingPolicy$.MODULE$.breakForIds( var0 );
    }

    static PipelineBreakingPolicy breakFor( final Seq<LogicalPlan> logicalPlans )
    {
        return PipelineBreakingPolicy$.MODULE$.breakFor( var0 );
    }

    static void $init$( final PipelineBreakingPolicy $this )
    {
    }

    boolean breakOn( final LogicalPlan lp );

    void onNestedPlanBreak();

    default SlotConfiguration invoke( final LogicalPlan lp, final SlotConfiguration slots, final SlotConfiguration argumentSlots )
    {
        SlotConfiguration var10000;
        if ( this.breakOn( lp ) )
        {
            SlotConfiguration var4;
            if ( lp instanceof AggregatingPlan )
            {
                var4 = argumentSlots.copy();
            }
            else
            {
                var4 = slots.copy();
            }

            var10000 = var4;
        }
        else
        {
            var10000 = slots;
        }

        return var10000;
    }
}
