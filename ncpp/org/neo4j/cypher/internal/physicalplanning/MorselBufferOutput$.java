package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.v4_0.util.attribution.Id;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;

public final class MorselBufferOutput$ extends AbstractFunction2<BufferId,Id,MorselBufferOutput> implements Serializable
{
    public static MorselBufferOutput$ MODULE$;

    static
    {
        new MorselBufferOutput$();
    }

    private MorselBufferOutput$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "MorselBufferOutput";
    }

    public MorselBufferOutput apply( final int id, final int nextPipelineHeadPlanId )
    {
        return new MorselBufferOutput( id, nextPipelineHeadPlanId );
    }

    public Option<Tuple2<BufferId,Id>> unapply( final MorselBufferOutput x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( new BufferId( x$0.id() ), new Id( x$0.nextPipelineHeadPlanId() ) ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
