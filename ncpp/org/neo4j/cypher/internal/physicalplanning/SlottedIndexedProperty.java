package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.logical.plans.IndexedProperty;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class SlottedIndexedProperty implements Product, Serializable
{
    private final int propertyKeyId;
    private final Option<Object> maybeCachedNodePropertySlot;

    public SlottedIndexedProperty( final int propertyKeyId, final Option<Object> maybeCachedNodePropertySlot )
    {
        this.propertyKeyId = propertyKeyId;
        this.maybeCachedNodePropertySlot = maybeCachedNodePropertySlot;
        Product.$init$( this );
    }

    public static Option<Tuple2<Object,Option<Object>>> unapply( final SlottedIndexedProperty x$0 )
    {
        return SlottedIndexedProperty$.MODULE$.unapply( var0 );
    }

    public static SlottedIndexedProperty apply( final int propertyKeyId, final Option<Object> maybeCachedNodePropertySlot )
    {
        return SlottedIndexedProperty$.MODULE$.apply( var0, var1 );
    }

    public static SlottedIndexedProperty apply( final String node, final IndexedProperty property, final SlotConfiguration slots )
    {
        return SlottedIndexedProperty$.MODULE$.apply( var0, var1, var2 );
    }

    public int propertyKeyId()
    {
        return this.propertyKeyId;
    }

    public Option<Object> maybeCachedNodePropertySlot()
    {
        return this.maybeCachedNodePropertySlot;
    }

    public boolean getValueFromIndex()
    {
        return this.maybeCachedNodePropertySlot().isDefined();
    }

    public SlottedIndexedProperty copy( final int propertyKeyId, final Option<Object> maybeCachedNodePropertySlot )
    {
        return new SlottedIndexedProperty( propertyKeyId, maybeCachedNodePropertySlot );
    }

    public int copy$default$1()
    {
        return this.propertyKeyId();
    }

    public Option<Object> copy$default$2()
    {
        return this.maybeCachedNodePropertySlot();
    }

    public String productPrefix()
    {
        return "SlottedIndexedProperty";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = BoxesRunTime.boxToInteger( this.propertyKeyId() );
            break;
        case 1:
            var10000 = this.maybeCachedNodePropertySlot();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof SlottedIndexedProperty;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, this.propertyKeyId() );
        var1 = Statics.mix( var1, Statics.anyHash( this.maybeCachedNodePropertySlot() ) );
        return Statics.finalizeHash( var1, 2 );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        if ( this != x$1 )
        {
            label55:
            {
                boolean var2;
                if ( x$1 instanceof SlottedIndexedProperty )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label38:
                    {
                        SlottedIndexedProperty var4 = (SlottedIndexedProperty) x$1;
                        if ( this.propertyKeyId() == var4.propertyKeyId() )
                        {
                            label36:
                            {
                                Option var10000 = this.maybeCachedNodePropertySlot();
                                Option var5 = var4.maybeCachedNodePropertySlot();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label36;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label36;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var6 = true;
                                    break label38;
                                }
                            }
                        }

                        var6 = false;
                    }

                    if ( var6 )
                    {
                        break label55;
                    }
                }

                var6 = false;
                return var6;
            }
        }

        var6 = true;
        return var6;
    }
}
