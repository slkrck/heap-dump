package org.neo4j.cypher.internal.physicalplanning;

import scala.collection.Iterator;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class RegularBufferVariant
{
    public static String toString()
    {
        return RegularBufferVariant$.MODULE$.toString();
    }

    public static int hashCode()
    {
        return RegularBufferVariant$.MODULE$.hashCode();
    }

    public static boolean canEqual( final Object x$1 )
    {
        return RegularBufferVariant$.MODULE$.canEqual( var0 );
    }

    public static Iterator<Object> productIterator()
    {
        return RegularBufferVariant$.MODULE$.productIterator();
    }

    public static Object productElement( final int x$1 )
    {
        return RegularBufferVariant$.MODULE$.productElement( var0 );
    }

    public static int productArity()
    {
        return RegularBufferVariant$.MODULE$.productArity();
    }

    public static String productPrefix()
    {
        return RegularBufferVariant$.MODULE$.productPrefix();
    }
}
