package org.neo4j.cypher.internal.physicalplanning;

import scala.Predef.;
import scala.collection.immutable.Set;
import scala.collection.immutable.StringOps;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface SlotWithAliases
{
    static void $init$( final SlotWithAliases $this )
    {
    }

    Slot slot();

    Set<String> aliases();

    default String makeString()
    {
        String aliasesString = String.valueOf( this.aliases().mkString( "'", "','", "'" ) );
        Object arg$macro$1 = this.slot();
        return (new StringOps( "%-30s %-10s" )).format(.MODULE$.genericWrapArray( new Object[]{arg$macro$1, aliasesString} ));
    }
}
