package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.planner.spi.TokenContext;
import org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticTable;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class PhysicalPlanner
{
    public static boolean plan$default$5()
    {
        return PhysicalPlanner$.MODULE$.plan$default$5();
    }

    public static PhysicalPlan plan( final TokenContext tokenContext, final LogicalPlan beforeRewrite, final SemanticTable semanticTable,
            final PipelineBreakingPolicy breakingPolicy, final boolean allocateArgumentSlots )
    {
        return PhysicalPlanner$.MODULE$.plan( var0, var1, var2, var3, var4 );
    }
}
