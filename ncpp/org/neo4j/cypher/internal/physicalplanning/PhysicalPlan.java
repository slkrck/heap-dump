package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.runtime.ParameterMapping;
import org.neo4j.cypher.internal.runtime.expressionVariableAllocation.AvailableExpressionVariables;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple8;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class PhysicalPlan implements Product, Serializable
{
    private final LogicalPlan logicalPlan;
    private final int nExpressionSlots;
    private final PhysicalPlanningAttributes.SlotConfigurations slotConfigurations;
    private final PhysicalPlanningAttributes.ArgumentSizes argumentSizes;
    private final PhysicalPlanningAttributes.ApplyPlans applyPlans;
    private final PhysicalPlanningAttributes.NestedPlanArgumentConfigurations nestedPlanArgumentConfigurations;
    private final AvailableExpressionVariables availableExpressionVariables;
    private final ParameterMapping parameterMapping;

    public PhysicalPlan( final LogicalPlan logicalPlan, final int nExpressionSlots, final PhysicalPlanningAttributes.SlotConfigurations slotConfigurations,
            final PhysicalPlanningAttributes.ArgumentSizes argumentSizes, final PhysicalPlanningAttributes.ApplyPlans applyPlans,
            final PhysicalPlanningAttributes.NestedPlanArgumentConfigurations nestedPlanArgumentConfigurations,
            final AvailableExpressionVariables availableExpressionVariables, final ParameterMapping parameterMapping )
    {
        this.logicalPlan = logicalPlan;
        this.nExpressionSlots = nExpressionSlots;
        this.slotConfigurations = slotConfigurations;
        this.argumentSizes = argumentSizes;
        this.applyPlans = applyPlans;
        this.nestedPlanArgumentConfigurations = nestedPlanArgumentConfigurations;
        this.availableExpressionVariables = availableExpressionVariables;
        this.parameterMapping = parameterMapping;
        Product.$init$( this );
    }

    public static Option<Tuple8<LogicalPlan,Object,PhysicalPlanningAttributes.SlotConfigurations,PhysicalPlanningAttributes.ArgumentSizes,PhysicalPlanningAttributes.ApplyPlans,PhysicalPlanningAttributes.NestedPlanArgumentConfigurations,AvailableExpressionVariables,ParameterMapping>> unapply(
            final PhysicalPlan x$0 )
    {
        return PhysicalPlan$.MODULE$.unapply( var0 );
    }

    public static PhysicalPlan apply( final LogicalPlan logicalPlan, final int nExpressionSlots,
            final PhysicalPlanningAttributes.SlotConfigurations slotConfigurations, final PhysicalPlanningAttributes.ArgumentSizes argumentSizes,
            final PhysicalPlanningAttributes.ApplyPlans applyPlans,
            final PhysicalPlanningAttributes.NestedPlanArgumentConfigurations nestedPlanArgumentConfigurations,
            final AvailableExpressionVariables availableExpressionVariables, final ParameterMapping parameterMapping )
    {
        return PhysicalPlan$.MODULE$.apply( var0, var1, var2, var3, var4, var5, var6, var7 );
    }

    public static Function1<Tuple8<LogicalPlan,Object,PhysicalPlanningAttributes.SlotConfigurations,PhysicalPlanningAttributes.ArgumentSizes,PhysicalPlanningAttributes.ApplyPlans,PhysicalPlanningAttributes.NestedPlanArgumentConfigurations,AvailableExpressionVariables,ParameterMapping>,PhysicalPlan> tupled()
    {
        return PhysicalPlan$.MODULE$.tupled();
    }

    public static Function1<LogicalPlan,Function1<Object,Function1<PhysicalPlanningAttributes.SlotConfigurations,Function1<PhysicalPlanningAttributes.ArgumentSizes,Function1<PhysicalPlanningAttributes.ApplyPlans,Function1<PhysicalPlanningAttributes.NestedPlanArgumentConfigurations,Function1<AvailableExpressionVariables,Function1<ParameterMapping,PhysicalPlan>>>>>>>> curried()
    {
        return PhysicalPlan$.MODULE$.curried();
    }

    public LogicalPlan logicalPlan()
    {
        return this.logicalPlan;
    }

    public int nExpressionSlots()
    {
        return this.nExpressionSlots;
    }

    public PhysicalPlanningAttributes.SlotConfigurations slotConfigurations()
    {
        return this.slotConfigurations;
    }

    public PhysicalPlanningAttributes.ArgumentSizes argumentSizes()
    {
        return this.argumentSizes;
    }

    public PhysicalPlanningAttributes.ApplyPlans applyPlans()
    {
        return this.applyPlans;
    }

    public PhysicalPlanningAttributes.NestedPlanArgumentConfigurations nestedPlanArgumentConfigurations()
    {
        return this.nestedPlanArgumentConfigurations;
    }

    public AvailableExpressionVariables availableExpressionVariables()
    {
        return this.availableExpressionVariables;
    }

    public ParameterMapping parameterMapping()
    {
        return this.parameterMapping;
    }

    public PhysicalPlan copy( final LogicalPlan logicalPlan, final int nExpressionSlots, final PhysicalPlanningAttributes.SlotConfigurations slotConfigurations,
            final PhysicalPlanningAttributes.ArgumentSizes argumentSizes, final PhysicalPlanningAttributes.ApplyPlans applyPlans,
            final PhysicalPlanningAttributes.NestedPlanArgumentConfigurations nestedPlanArgumentConfigurations,
            final AvailableExpressionVariables availableExpressionVariables, final ParameterMapping parameterMapping )
    {
        return new PhysicalPlan( logicalPlan, nExpressionSlots, slotConfigurations, argumentSizes, applyPlans, nestedPlanArgumentConfigurations,
                availableExpressionVariables, parameterMapping );
    }

    public LogicalPlan copy$default$1()
    {
        return this.logicalPlan();
    }

    public int copy$default$2()
    {
        return this.nExpressionSlots();
    }

    public PhysicalPlanningAttributes.SlotConfigurations copy$default$3()
    {
        return this.slotConfigurations();
    }

    public PhysicalPlanningAttributes.ArgumentSizes copy$default$4()
    {
        return this.argumentSizes();
    }

    public PhysicalPlanningAttributes.ApplyPlans copy$default$5()
    {
        return this.applyPlans();
    }

    public PhysicalPlanningAttributes.NestedPlanArgumentConfigurations copy$default$6()
    {
        return this.nestedPlanArgumentConfigurations();
    }

    public AvailableExpressionVariables copy$default$7()
    {
        return this.availableExpressionVariables();
    }

    public ParameterMapping copy$default$8()
    {
        return this.parameterMapping();
    }

    public String productPrefix()
    {
        return "PhysicalPlan";
    }

    public int productArity()
    {
        return 8;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.logicalPlan();
            break;
        case 1:
            var10000 = BoxesRunTime.boxToInteger( this.nExpressionSlots() );
            break;
        case 2:
            var10000 = this.slotConfigurations();
            break;
        case 3:
            var10000 = this.argumentSizes();
            break;
        case 4:
            var10000 = this.applyPlans();
            break;
        case 5:
            var10000 = this.nestedPlanArgumentConfigurations();
            break;
        case 6:
            var10000 = this.availableExpressionVariables();
            break;
        case 7:
            var10000 = this.parameterMapping();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof PhysicalPlan;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, Statics.anyHash( this.logicalPlan() ) );
        var1 = Statics.mix( var1, this.nExpressionSlots() );
        var1 = Statics.mix( var1, Statics.anyHash( this.slotConfigurations() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.argumentSizes() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.applyPlans() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.nestedPlanArgumentConfigurations() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.availableExpressionVariables() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.parameterMapping() ) );
        return Statics.finalizeHash( var1, 8 );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var18;
        if ( this != x$1 )
        {
            label110:
            {
                boolean var2;
                if ( x$1 instanceof PhysicalPlan )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label92:
                    {
                        label91:
                        {
                            PhysicalPlan var4 = (PhysicalPlan) x$1;
                            LogicalPlan var10000 = this.logicalPlan();
                            LogicalPlan var5 = var4.logicalPlan();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label91;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label91;
                            }

                            if ( this.nExpressionSlots() == var4.nExpressionSlots() )
                            {
                                label101:
                                {
                                    PhysicalPlanningAttributes.SlotConfigurations var12 = this.slotConfigurations();
                                    PhysicalPlanningAttributes.SlotConfigurations var6 = var4.slotConfigurations();
                                    if ( var12 == null )
                                    {
                                        if ( var6 != null )
                                        {
                                            break label101;
                                        }
                                    }
                                    else if ( !var12.equals( var6 ) )
                                    {
                                        break label101;
                                    }

                                    PhysicalPlanningAttributes.ArgumentSizes var13 = this.argumentSizes();
                                    PhysicalPlanningAttributes.ArgumentSizes var7 = var4.argumentSizes();
                                    if ( var13 == null )
                                    {
                                        if ( var7 != null )
                                        {
                                            break label101;
                                        }
                                    }
                                    else if ( !var13.equals( var7 ) )
                                    {
                                        break label101;
                                    }

                                    PhysicalPlanningAttributes.ApplyPlans var14 = this.applyPlans();
                                    PhysicalPlanningAttributes.ApplyPlans var8 = var4.applyPlans();
                                    if ( var14 == null )
                                    {
                                        if ( var8 != null )
                                        {
                                            break label101;
                                        }
                                    }
                                    else if ( !var14.equals( var8 ) )
                                    {
                                        break label101;
                                    }

                                    PhysicalPlanningAttributes.NestedPlanArgumentConfigurations var15 = this.nestedPlanArgumentConfigurations();
                                    PhysicalPlanningAttributes.NestedPlanArgumentConfigurations var9 = var4.nestedPlanArgumentConfigurations();
                                    if ( var15 == null )
                                    {
                                        if ( var9 != null )
                                        {
                                            break label101;
                                        }
                                    }
                                    else if ( !var15.equals( var9 ) )
                                    {
                                        break label101;
                                    }

                                    AvailableExpressionVariables var16 = this.availableExpressionVariables();
                                    AvailableExpressionVariables var10 = var4.availableExpressionVariables();
                                    if ( var16 == null )
                                    {
                                        if ( var10 != null )
                                        {
                                            break label101;
                                        }
                                    }
                                    else if ( !var16.equals( var10 ) )
                                    {
                                        break label101;
                                    }

                                    ParameterMapping var17 = this.parameterMapping();
                                    ParameterMapping var11 = var4.parameterMapping();
                                    if ( var17 == null )
                                    {
                                        if ( var11 != null )
                                        {
                                            break label101;
                                        }
                                    }
                                    else if ( !var17.equals( var11 ) )
                                    {
                                        break label101;
                                    }

                                    if ( var4.canEqual( this ) )
                                    {
                                        var18 = true;
                                        break label92;
                                    }
                                }
                            }
                        }

                        var18 = false;
                    }

                    if ( var18 )
                    {
                        break label110;
                    }
                }

                var18 = false;
                return var18;
            }
        }

        var18 = true;
        return var18;
    }
}
