package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class BREAK_FOR_LEAFS
{
    public static SlotConfiguration invoke( final LogicalPlan lp, final SlotConfiguration slots, final SlotConfiguration argumentSlots )
    {
        return BREAK_FOR_LEAFS$.MODULE$.invoke( var0, var1, var2 );
    }

    public static void onNestedPlanBreak()
    {
        BREAK_FOR_LEAFS$.MODULE$.onNestedPlanBreak();
    }

    public static boolean breakOn( final LogicalPlan lp )
    {
        return BREAK_FOR_LEAFS$.MODULE$.breakOn( var0 );
    }
}
