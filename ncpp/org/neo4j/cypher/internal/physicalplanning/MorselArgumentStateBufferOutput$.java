package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.v4_0.util.attribution.Id;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.runtime.AbstractFunction3;
import scala.runtime.BoxesRunTime;

public final class MorselArgumentStateBufferOutput$ extends AbstractFunction3<BufferId,Object,Id,MorselArgumentStateBufferOutput> implements Serializable
{
    public static MorselArgumentStateBufferOutput$ MODULE$;

    static
    {
        new MorselArgumentStateBufferOutput$();
    }

    private MorselArgumentStateBufferOutput$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "MorselArgumentStateBufferOutput";
    }

    public MorselArgumentStateBufferOutput apply( final int id, final int argumentSlotOffset, final int nextPipelineHeadPlanId )
    {
        return new MorselArgumentStateBufferOutput( id, argumentSlotOffset, nextPipelineHeadPlanId );
    }

    public Option<Tuple3<BufferId,Object,Id>> unapply( final MorselArgumentStateBufferOutput x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :
        new Some( new Tuple3( new BufferId( x$0.id() ), BoxesRunTime.boxToInteger( x$0.argumentSlotOffset() ), new Id( x$0.nextPipelineHeadPlanId() ) ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
