package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.runtime.AbstractFunction3;
import scala.runtime.BoxesRunTime;

public final class LongSlot$ extends AbstractFunction3<Object,Object,CypherType,LongSlot> implements Serializable
{
    public static LongSlot$ MODULE$;

    static
    {
        new LongSlot$();
    }

    private LongSlot$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "LongSlot";
    }

    public LongSlot apply( final int offset, final boolean nullable, final CypherType typ )
    {
        return new LongSlot( offset, nullable, typ );
    }

    public Option<Tuple3<Object,Object,CypherType>> unapply( final LongSlot x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :
        new Some( new Tuple3( BoxesRunTime.boxToInteger( x$0.offset() ), BoxesRunTime.boxToBoolean( x$0.nullable() ), x$0.typ() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
