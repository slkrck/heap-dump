package org.neo4j.cypher.internal.physicalplanning;

import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class PipelineBuilder
{
    public static ExecutionGraphDefinition build( final PipelineBreakingPolicy breakingPolicy, final OperatorFusionPolicy operatorFusionPolicy,
            final PhysicalPlan physicalPlan )
    {
        return PipelineBuilder$.MODULE$.build( var0, var1, var2 );
    }
}
