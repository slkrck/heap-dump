package org.neo4j.cypher.internal.physicalplanning.ast;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;
import scala.runtime.BoxesRunTime;

public final class NodeFromSlot$ extends AbstractFunction2<Object,String,NodeFromSlot> implements Serializable
{
    public static NodeFromSlot$ MODULE$;

    static
    {
        new NodeFromSlot$();
    }

    private NodeFromSlot$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "NodeFromSlot";
    }

    public NodeFromSlot apply( final int offset, final String name )
    {
        return new NodeFromSlot( offset, name );
    }

    public Option<Tuple2<Object,String>> unapply( final NodeFromSlot x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( BoxesRunTime.boxToInteger( x$0.offset() ), x$0.name() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
