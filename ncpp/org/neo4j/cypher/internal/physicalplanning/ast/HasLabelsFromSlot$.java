package org.neo4j.cypher.internal.physicalplanning.ast;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.collection.Seq;
import scala.runtime.AbstractFunction3;
import scala.runtime.BoxesRunTime;

public final class HasLabelsFromSlot$ extends AbstractFunction3<Object,Seq<Object>,Seq<String>,HasLabelsFromSlot> implements Serializable
{
    public static HasLabelsFromSlot$ MODULE$;

    static
    {
        new HasLabelsFromSlot$();
    }

    private HasLabelsFromSlot$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "HasLabelsFromSlot";
    }

    public HasLabelsFromSlot apply( final int offset, final Seq<Object> resolvedLabelTokens, final Seq<String> lateLabels )
    {
        return new HasLabelsFromSlot( offset, resolvedLabelTokens, lateLabels );
    }

    public Option<Tuple3<Object,Seq<Object>,Seq<String>>> unapply( final HasLabelsFromSlot x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple3( BoxesRunTime.boxToInteger( x$0.offset() ), x$0.resolvedLabelTokens(), x$0.lateLabels() ) ))
        ;
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
