package org.neo4j.cypher.internal.physicalplanning.ast;

import org.neo4j.cypher.internal.v4_0.expressions.EntityType;
import org.neo4j.cypher.internal.v4_0.expressions.PropertyKeyName;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple8;
import scala.None.;
import scala.runtime.AbstractFunction8;
import scala.runtime.BoxesRunTime;

public final class SlottedCachedPropertyWithPropertyToken$
        extends AbstractFunction8<String,PropertyKeyName,Object,Object,Object,Object,EntityType,Object,SlottedCachedPropertyWithPropertyToken>
        implements Serializable
{
    public static SlottedCachedPropertyWithPropertyToken$ MODULE$;

    static
    {
        new SlottedCachedPropertyWithPropertyToken$();
    }

    private SlottedCachedPropertyWithPropertyToken$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "SlottedCachedPropertyWithPropertyToken";
    }

    public SlottedCachedPropertyWithPropertyToken apply( final String entityName, final PropertyKeyName propertyKey, final int offset,
            final boolean offsetIsForLongSlot, final int propToken, final int cachedPropertyOffset, final EntityType entityType, final boolean nullable )
    {
        return new SlottedCachedPropertyWithPropertyToken( entityName, propertyKey, offset, offsetIsForLongSlot, propToken, cachedPropertyOffset, entityType,
                nullable );
    }

    public Option<Tuple8<String,PropertyKeyName,Object,Object,Object,Object,EntityType,Object>> unapply( final SlottedCachedPropertyWithPropertyToken x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some(
            new Tuple8( x$0.entityName(), x$0.propertyKey(), BoxesRunTime.boxToInteger( x$0.offset() ), BoxesRunTime.boxToBoolean( x$0.offsetIsForLongSlot() ),
                    BoxesRunTime.boxToInteger( x$0.propToken() ), BoxesRunTime.boxToInteger( x$0.cachedPropertyOffset() ), x$0.entityType(),
                    BoxesRunTime.boxToBoolean( x$0.nullable() ) ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
