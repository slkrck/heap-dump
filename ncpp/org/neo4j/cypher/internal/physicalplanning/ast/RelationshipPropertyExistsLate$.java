package org.neo4j.cypher.internal.physicalplanning.ast;

import org.neo4j.cypher.internal.v4_0.expressions.LogicalProperty;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.runtime.BoxesRunTime;

public final class RelationshipPropertyExistsLate$ implements Serializable
{
    public static RelationshipPropertyExistsLate$ MODULE$;

    static
    {
        new RelationshipPropertyExistsLate$();
    }

    private RelationshipPropertyExistsLate$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "RelationshipPropertyExistsLate";
    }

    public RelationshipPropertyExistsLate apply( final int offset, final String propKey, final String name, final LogicalProperty prop )
    {
        return new RelationshipPropertyExistsLate( offset, propKey, name, prop );
    }

    public Option<Tuple3<Object,String,String>> unapply( final RelationshipPropertyExistsLate x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple3( BoxesRunTime.boxToInteger( x$0.offset() ), x$0.propKey(), x$0.name() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
