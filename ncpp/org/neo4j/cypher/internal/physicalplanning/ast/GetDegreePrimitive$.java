package org.neo4j.cypher.internal.physicalplanning.ast;

import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.runtime.AbstractFunction3;
import scala.runtime.BoxesRunTime;

public final class GetDegreePrimitive$ extends AbstractFunction3<Object,Option<String>,SemanticDirection,GetDegreePrimitive> implements Serializable
{
    public static GetDegreePrimitive$ MODULE$;

    static
    {
        new GetDegreePrimitive$();
    }

    private GetDegreePrimitive$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "GetDegreePrimitive";
    }

    public GetDegreePrimitive apply( final int offset, final Option<String> typ, final SemanticDirection direction )
    {
        return new GetDegreePrimitive( offset, typ, direction );
    }

    public Option<Tuple3<Object,Option<String>,SemanticDirection>> unapply( final GetDegreePrimitive x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple3( BoxesRunTime.boxToInteger( x$0.offset() ), x$0.typ(), x$0.direction() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
