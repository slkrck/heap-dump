package org.neo4j.cypher.internal.physicalplanning.ast;

import org.neo4j.cypher.internal.runtime.ast.RuntimeExpression;
import org.neo4j.cypher.internal.v4_0.expressions.ASTCachedProperty;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public interface SlottedCachedProperty extends ASTCachedProperty, RuntimeExpression
{
    static void $init$( final SlottedCachedProperty $this )
    {
    }

    int offset();

    boolean offsetIsForLongSlot();

    int cachedPropertyOffset();

    default String originalEntityName()
    {
        return this.entityName();
    }
}
