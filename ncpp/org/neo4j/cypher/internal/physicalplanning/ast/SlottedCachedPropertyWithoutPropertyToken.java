package org.neo4j.cypher.internal.physicalplanning.ast;

import org.neo4j.cypher.internal.runtime.ast.RuntimeExpression;
import org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticCheckResult;
import org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticState;
import org.neo4j.cypher.internal.v4_0.expressions.ASTCachedProperty;
import org.neo4j.cypher.internal.v4_0.expressions.EntityType;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import org.neo4j.cypher.internal.v4_0.expressions.LogicalProperty;
import org.neo4j.cypher.internal.v4_0.expressions.PropertyKeyName;
import org.neo4j.cypher.internal.v4_0.expressions.Expression.SemanticContext;
import org.neo4j.cypher.internal.v4_0.util.InputPosition;
import scala.Function1;
import scala.Option;
import scala.Serializable;
import scala.Tuple8;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class SlottedCachedPropertyWithoutPropertyToken extends LogicalProperty implements SlottedCachedProperty, Serializable
{
    private final String entityName;
    private final PropertyKeyName propertyKey;
    private final int offset;
    private final boolean offsetIsForLongSlot;
    private final String propKey;
    private final int cachedPropertyOffset;
    private final EntityType entityType;
    private final boolean nullable;
    private final Expression map;

    public SlottedCachedPropertyWithoutPropertyToken( final String entityName, final PropertyKeyName propertyKey, final int offset,
            final boolean offsetIsForLongSlot, final String propKey, final int cachedPropertyOffset, final EntityType entityType, final boolean nullable )
    {
        this.entityName = entityName;
        this.propertyKey = propertyKey;
        this.offset = offset;
        this.offsetIsForLongSlot = offsetIsForLongSlot;
        this.propKey = propKey;
        this.cachedPropertyOffset = cachedPropertyOffset;
        this.entityType = entityType;
        this.nullable = nullable;
        ASTCachedProperty.$init$( this );
        RuntimeExpression.$init$( this );
        SlottedCachedProperty.$init$( this );
    }

    public static Option<Tuple8<String,PropertyKeyName,Object,Object,String,Object,EntityType,Object>> unapply(
            final SlottedCachedPropertyWithoutPropertyToken x$0 )
    {
        return SlottedCachedPropertyWithoutPropertyToken$.MODULE$.unapply( var0 );
    }

    public static SlottedCachedPropertyWithoutPropertyToken apply( final String entityName, final PropertyKeyName propertyKey, final int offset,
            final boolean offsetIsForLongSlot, final String propKey, final int cachedPropertyOffset, final EntityType entityType, final boolean nullable )
    {
        return SlottedCachedPropertyWithoutPropertyToken$.MODULE$.apply( var0, var1, var2, var3, var4, var5, var6, var7 );
    }

    public static Function1<Tuple8<String,PropertyKeyName,Object,Object,String,Object,EntityType,Object>,SlottedCachedPropertyWithoutPropertyToken> tupled()
    {
        return SlottedCachedPropertyWithoutPropertyToken$.MODULE$.tupled();
    }

    public static Function1<String,Function1<PropertyKeyName,Function1<Object,Function1<Object,Function1<String,Function1<Object,Function1<EntityType,Function1<Object,SlottedCachedPropertyWithoutPropertyToken>>>>>>>> curried()
    {
        return SlottedCachedPropertyWithoutPropertyToken$.MODULE$.curried();
    }

    public String originalEntityName()
    {
        return SlottedCachedProperty.originalEntityName$( this );
    }

    public Function1<SemanticState,SemanticCheckResult> semanticCheck( final SemanticContext ctx )
    {
        return RuntimeExpression.semanticCheck$( this, ctx );
    }

    public InputPosition position()
    {
        return RuntimeExpression.position$( this );
    }

    public String propertyAccessString()
    {
        return ASTCachedProperty.propertyAccessString$( this );
    }

    public final boolean equals( final Object obj )
    {
        return ASTCachedProperty.equals$( this, obj );
    }

    public final int hashCode()
    {
        return ASTCachedProperty.hashCode$( this );
    }

    public Expression map()
    {
        return this.map;
    }

    public void org$neo4j$cypher$internal$v4_0$expressions$ASTCachedProperty$_setter_$map_$eq( final Expression x$1 )
    {
        this.map = x$1;
    }

    public String entityName()
    {
        return this.entityName;
    }

    public PropertyKeyName propertyKey()
    {
        return this.propertyKey;
    }

    public int offset()
    {
        return this.offset;
    }

    public boolean offsetIsForLongSlot()
    {
        return this.offsetIsForLongSlot;
    }

    public String propKey()
    {
        return this.propKey;
    }

    public int cachedPropertyOffset()
    {
        return this.cachedPropertyOffset;
    }

    public EntityType entityType()
    {
        return this.entityType;
    }

    public boolean nullable()
    {
        return this.nullable;
    }

    public SlottedCachedPropertyWithoutPropertyToken copy( final String entityName, final PropertyKeyName propertyKey, final int offset,
            final boolean offsetIsForLongSlot, final String propKey, final int cachedPropertyOffset, final EntityType entityType, final boolean nullable )
    {
        return new SlottedCachedPropertyWithoutPropertyToken( entityName, propertyKey, offset, offsetIsForLongSlot, propKey, cachedPropertyOffset, entityType,
                nullable );
    }

    public String copy$default$1()
    {
        return this.entityName();
    }

    public PropertyKeyName copy$default$2()
    {
        return this.propertyKey();
    }

    public int copy$default$3()
    {
        return this.offset();
    }

    public boolean copy$default$4()
    {
        return this.offsetIsForLongSlot();
    }

    public String copy$default$5()
    {
        return this.propKey();
    }

    public int copy$default$6()
    {
        return this.cachedPropertyOffset();
    }

    public EntityType copy$default$7()
    {
        return this.entityType();
    }

    public boolean copy$default$8()
    {
        return this.nullable();
    }

    public String productPrefix()
    {
        return "SlottedCachedPropertyWithoutPropertyToken";
    }

    public int productArity()
    {
        return 8;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.entityName();
            break;
        case 1:
            var10000 = this.propertyKey();
            break;
        case 2:
            var10000 = BoxesRunTime.boxToInteger( this.offset() );
            break;
        case 3:
            var10000 = BoxesRunTime.boxToBoolean( this.offsetIsForLongSlot() );
            break;
        case 4:
            var10000 = this.propKey();
            break;
        case 5:
            var10000 = BoxesRunTime.boxToInteger( this.cachedPropertyOffset() );
            break;
        case 6:
            var10000 = this.entityType();
            break;
        case 7:
            var10000 = BoxesRunTime.boxToBoolean( this.nullable() );
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof SlottedCachedPropertyWithoutPropertyToken;
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }
}
