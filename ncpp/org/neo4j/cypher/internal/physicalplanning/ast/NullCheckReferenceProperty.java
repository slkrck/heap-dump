package org.neo4j.cypher.internal.physicalplanning.ast;

import org.neo4j.cypher.internal.runtime.ast.RuntimeProperty;
import org.neo4j.cypher.internal.v4_0.expressions.LogicalProperty;
import scala.Function1;
import scala.Option;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class NullCheckReferenceProperty extends RuntimeProperty implements Serializable
{
    private final int offset;

    public NullCheckReferenceProperty( final int offset, final LogicalProperty inner )
    {
        super( inner );
        this.offset = offset;
    }

    public static Option<Tuple2<Object,LogicalProperty>> unapply( final NullCheckReferenceProperty x$0 )
    {
        return NullCheckReferenceProperty$.MODULE$.unapply( var0 );
    }

    public static NullCheckReferenceProperty apply( final int offset, final LogicalProperty inner )
    {
        return NullCheckReferenceProperty$.MODULE$.apply( var0, var1 );
    }

    public static Function1<Tuple2<Object,LogicalProperty>,NullCheckReferenceProperty> tupled()
    {
        return NullCheckReferenceProperty$.MODULE$.tupled();
    }

    public static Function1<Object,Function1<LogicalProperty,NullCheckReferenceProperty>> curried()
    {
        return NullCheckReferenceProperty$.MODULE$.curried();
    }

    public int offset()
    {
        return this.offset;
    }

    public LogicalProperty inner()
    {
        return super.prop();
    }

    public NullCheckReferenceProperty copy( final int offset, final LogicalProperty inner )
    {
        return new NullCheckReferenceProperty( offset, inner );
    }

    public int copy$default$1()
    {
        return this.offset();
    }

    public LogicalProperty copy$default$2()
    {
        return this.inner();
    }

    public String productPrefix()
    {
        return "NullCheckReferenceProperty";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = BoxesRunTime.boxToInteger( this.offset() );
            break;
        case 1:
            var10000 = this.inner();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof NullCheckReferenceProperty;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, this.offset() );
        var1 = Statics.mix( var1, Statics.anyHash( this.inner() ) );
        return Statics.finalizeHash( var1, 2 );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        if ( this != x$1 )
        {
            label55:
            {
                boolean var2;
                if ( x$1 instanceof NullCheckReferenceProperty )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label38:
                    {
                        NullCheckReferenceProperty var4 = (NullCheckReferenceProperty) x$1;
                        if ( this.offset() == var4.offset() )
                        {
                            label36:
                            {
                                LogicalProperty var10000 = this.inner();
                                LogicalProperty var5 = var4.inner();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label36;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label36;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var6 = true;
                                    break label38;
                                }
                            }
                        }

                        var6 = false;
                    }

                    if ( var6 )
                    {
                        break label55;
                    }
                }

                var6 = false;
                return var6;
            }
        }

        var6 = true;
        return var6;
    }
}
