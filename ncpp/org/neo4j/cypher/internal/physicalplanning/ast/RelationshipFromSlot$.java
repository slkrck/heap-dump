package org.neo4j.cypher.internal.physicalplanning.ast;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;
import scala.runtime.BoxesRunTime;

public final class RelationshipFromSlot$ extends AbstractFunction2<Object,String,RelationshipFromSlot> implements Serializable
{
    public static RelationshipFromSlot$ MODULE$;

    static
    {
        new RelationshipFromSlot$();
    }

    private RelationshipFromSlot$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "RelationshipFromSlot";
    }

    public RelationshipFromSlot apply( final int offset, final String name )
    {
        return new RelationshipFromSlot( offset, name );
    }

    public Option<Tuple2<Object,String>> unapply( final RelationshipFromSlot x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( BoxesRunTime.boxToInteger( x$0.offset() ), x$0.name() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
