package org.neo4j.cypher.internal.physicalplanning.ast;

import org.neo4j.cypher.internal.runtime.ast.RuntimeExpression;
import org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticCheckResult;
import org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticState;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import org.neo4j.cypher.internal.v4_0.expressions.Expression.SemanticContext;
import org.neo4j.cypher.internal.v4_0.util.InputPosition;
import scala.Function1;
import scala.Option;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class PrimitiveEquals extends Expression implements RuntimeExpression, Serializable
{
    private final Expression a;
    private final Expression b;

    public PrimitiveEquals( final Expression a, final Expression b )
    {
        this.a = a;
        this.b = b;
        RuntimeExpression.$init$( this );
    }

    public static Option<Tuple2<Expression,Expression>> unapply( final PrimitiveEquals x$0 )
    {
        return PrimitiveEquals$.MODULE$.unapply( var0 );
    }

    public static PrimitiveEquals apply( final Expression a, final Expression b )
    {
        return PrimitiveEquals$.MODULE$.apply( var0, var1 );
    }

    public static Function1<Tuple2<Expression,Expression>,PrimitiveEquals> tupled()
    {
        return PrimitiveEquals$.MODULE$.tupled();
    }

    public static Function1<Expression,Function1<Expression,PrimitiveEquals>> curried()
    {
        return PrimitiveEquals$.MODULE$.curried();
    }

    public Function1<SemanticState,SemanticCheckResult> semanticCheck( final SemanticContext ctx )
    {
        return RuntimeExpression.semanticCheck$( this, ctx );
    }

    public InputPosition position()
    {
        return RuntimeExpression.position$( this );
    }

    public Expression a()
    {
        return this.a;
    }

    public Expression b()
    {
        return this.b;
    }

    public PrimitiveEquals copy( final Expression a, final Expression b )
    {
        return new PrimitiveEquals( a, b );
    }

    public Expression copy$default$1()
    {
        return this.a();
    }

    public Expression copy$default$2()
    {
        return this.b();
    }

    public String productPrefix()
    {
        return "PrimitiveEquals";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Expression var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.a();
            break;
        case 1:
            var10000 = this.b();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof PrimitiveEquals;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var7;
        if ( this != x$1 )
        {
            label63:
            {
                boolean var2;
                if ( x$1 instanceof PrimitiveEquals )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label45:
                    {
                        label54:
                        {
                            PrimitiveEquals var4 = (PrimitiveEquals) x$1;
                            Expression var10000 = this.a();
                            Expression var5 = var4.a();
                            if ( var10000 == null )
                            {
                                if ( var5 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var5 ) )
                            {
                                break label54;
                            }

                            var10000 = this.b();
                            Expression var6 = var4.b();
                            if ( var10000 == null )
                            {
                                if ( var6 != null )
                                {
                                    break label54;
                                }
                            }
                            else if ( !var10000.equals( var6 ) )
                            {
                                break label54;
                            }

                            if ( var4.canEqual( this ) )
                            {
                                var7 = true;
                                break label45;
                            }
                        }

                        var7 = false;
                    }

                    if ( var7 )
                    {
                        break label63;
                    }
                }

                var7 = false;
                return var7;
            }
        }

        var7 = true;
        return var7;
    }
}
