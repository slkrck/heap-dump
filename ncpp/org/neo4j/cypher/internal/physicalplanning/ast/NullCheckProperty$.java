package org.neo4j.cypher.internal.physicalplanning.ast;

import org.neo4j.cypher.internal.v4_0.expressions.LogicalProperty;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;
import scala.runtime.BoxesRunTime;

public final class NullCheckProperty$ extends AbstractFunction2<Object,LogicalProperty,NullCheckProperty> implements Serializable
{
    public static NullCheckProperty$ MODULE$;

    static
    {
        new NullCheckProperty$();
    }

    private NullCheckProperty$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "NullCheckProperty";
    }

    public NullCheckProperty apply( final int offset, final LogicalProperty inner )
    {
        return new NullCheckProperty( offset, inner );
    }

    public Option<Tuple2<Object,LogicalProperty>> unapply( final NullCheckProperty x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( BoxesRunTime.boxToInteger( x$0.offset() ), x$0.inner() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
