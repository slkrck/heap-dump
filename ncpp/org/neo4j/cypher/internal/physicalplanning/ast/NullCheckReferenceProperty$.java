package org.neo4j.cypher.internal.physicalplanning.ast;

import org.neo4j.cypher.internal.v4_0.expressions.LogicalProperty;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;
import scala.runtime.BoxesRunTime;

public final class NullCheckReferenceProperty$ extends AbstractFunction2<Object,LogicalProperty,NullCheckReferenceProperty> implements Serializable
{
    public static NullCheckReferenceProperty$ MODULE$;

    static
    {
        new NullCheckReferenceProperty$();
    }

    private NullCheckReferenceProperty$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "NullCheckReferenceProperty";
    }

    public NullCheckReferenceProperty apply( final int offset, final LogicalProperty inner )
    {
        return new NullCheckReferenceProperty( offset, inner );
    }

    public Option<Tuple2<Object,LogicalProperty>> unapply( final NullCheckReferenceProperty x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( BoxesRunTime.boxToInteger( x$0.offset() ), x$0.inner() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
