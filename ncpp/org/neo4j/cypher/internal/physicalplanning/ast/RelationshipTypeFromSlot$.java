package org.neo4j.cypher.internal.physicalplanning.ast;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

public final class RelationshipTypeFromSlot$ extends AbstractFunction1<Object,RelationshipTypeFromSlot> implements Serializable
{
    public static RelationshipTypeFromSlot$ MODULE$;

    static
    {
        new RelationshipTypeFromSlot$();
    }

    private RelationshipTypeFromSlot$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "RelationshipTypeFromSlot";
    }

    public RelationshipTypeFromSlot apply( final int offset )
    {
        return new RelationshipTypeFromSlot( offset );
    }

    public Option<Object> unapply( final RelationshipTypeFromSlot x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( BoxesRunTime.boxToInteger( x$0.offset() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
