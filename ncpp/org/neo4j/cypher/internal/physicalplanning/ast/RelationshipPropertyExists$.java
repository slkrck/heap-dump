package org.neo4j.cypher.internal.physicalplanning.ast;

import org.neo4j.cypher.internal.v4_0.expressions.LogicalProperty;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.runtime.BoxesRunTime;

public final class RelationshipPropertyExists$ implements Serializable
{
    public static RelationshipPropertyExists$ MODULE$;

    static
    {
        new RelationshipPropertyExists$();
    }

    private RelationshipPropertyExists$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "RelationshipPropertyExists";
    }

    public RelationshipPropertyExists apply( final int offset, final int propToken, final String name, final LogicalProperty prop )
    {
        return new RelationshipPropertyExists( offset, propToken, name, prop );
    }

    public Option<Tuple3<Object,Object,String>> unapply( final RelationshipPropertyExists x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :
        new Some( new Tuple3( BoxesRunTime.boxToInteger( x$0.offset() ), BoxesRunTime.boxToInteger( x$0.propToken() ), x$0.name() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
