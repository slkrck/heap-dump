package org.neo4j.cypher.internal.physicalplanning.ast;

import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;
import scala.runtime.BoxesRunTime;

public final class NullCheck$ extends AbstractFunction2<Object,Expression,NullCheck> implements Serializable
{
    public static NullCheck$ MODULE$;

    static
    {
        new NullCheck$();
    }

    private NullCheck$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "NullCheck";
    }

    public NullCheck apply( final int offset, final Expression inner )
    {
        return new NullCheck( offset, inner );
    }

    public Option<Tuple2<Object,Expression>> unapply( final NullCheck x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( BoxesRunTime.boxToInteger( x$0.offset() ), x$0.inner() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
