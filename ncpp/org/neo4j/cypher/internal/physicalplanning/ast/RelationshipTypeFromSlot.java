package org.neo4j.cypher.internal.physicalplanning.ast;

import org.neo4j.cypher.internal.runtime.ast.RuntimeExpression;
import org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticCheckResult;
import org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticState;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import org.neo4j.cypher.internal.v4_0.expressions.Expression.SemanticContext;
import org.neo4j.cypher.internal.v4_0.util.InputPosition;
import scala.Function1;
import scala.Option;
import scala.Serializable;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class RelationshipTypeFromSlot extends Expression implements RuntimeExpression, Serializable
{
    private final int offset;

    public RelationshipTypeFromSlot( final int offset )
    {
        this.offset = offset;
        RuntimeExpression.$init$( this );
    }

    public static Option<Object> unapply( final RelationshipTypeFromSlot x$0 )
    {
        return RelationshipTypeFromSlot$.MODULE$.unapply( var0 );
    }

    public static RelationshipTypeFromSlot apply( final int offset )
    {
        return RelationshipTypeFromSlot$.MODULE$.apply( var0 );
    }

    public static <A> Function1<Object,A> andThen( final Function1<RelationshipTypeFromSlot,A> g )
    {
        return RelationshipTypeFromSlot$.MODULE$.andThen( var0 );
    }

    public static <A> Function1<A,RelationshipTypeFromSlot> compose( final Function1<A,Object> g )
    {
        return RelationshipTypeFromSlot$.MODULE$.compose( var0 );
    }

    public Function1<SemanticState,SemanticCheckResult> semanticCheck( final SemanticContext ctx )
    {
        return RuntimeExpression.semanticCheck$( this, ctx );
    }

    public InputPosition position()
    {
        return RuntimeExpression.position$( this );
    }

    public int offset()
    {
        return this.offset;
    }

    public RelationshipTypeFromSlot copy( final int offset )
    {
        return new RelationshipTypeFromSlot( offset );
    }

    public int copy$default$1()
    {
        return this.offset();
    }

    public String productPrefix()
    {
        return "RelationshipTypeFromSlot";
    }

    public int productArity()
    {
        return 1;
    }

    public Object productElement( final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return BoxesRunTime.boxToInteger( this.offset() );
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof RelationshipTypeFromSlot;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, this.offset() );
        return Statics.finalizeHash( var1, 1 );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var10000;
        if ( this != x$1 )
        {
            label49:
            {
                boolean var2;
                if ( x$1 instanceof RelationshipTypeFromSlot )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    RelationshipTypeFromSlot var4 = (RelationshipTypeFromSlot) x$1;
                    if ( this.offset() == var4.offset() && var4.canEqual( this ) )
                    {
                        break label49;
                    }
                }

                var10000 = false;
                return var10000;
            }
        }

        var10000 = true;
        return var10000;
    }
}
