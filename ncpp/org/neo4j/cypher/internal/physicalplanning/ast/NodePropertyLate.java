package org.neo4j.cypher.internal.physicalplanning.ast;

import org.neo4j.cypher.internal.runtime.ast.RuntimeProperty;
import org.neo4j.cypher.internal.v4_0.expressions.Property;
import scala.Option;
import scala.Serializable;
import scala.Tuple3;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class NodePropertyLate extends RuntimeProperty implements Serializable
{
    private final int offset;
    private final String propKey;
    private final String name;

    public NodePropertyLate( final int offset, final String propKey, final String name, final Property prop )
    {
        super( prop );
        this.offset = offset;
        this.propKey = propKey;
        this.name = name;
    }

    public static Option<Tuple3<Object,String,String>> unapply( final NodePropertyLate x$0 )
    {
        return NodePropertyLate$.MODULE$.unapply( var0 );
    }

    public static NodePropertyLate apply( final int offset, final String propKey, final String name, final Property prop )
    {
        return NodePropertyLate$.MODULE$.apply( var0, var1, var2, var3 );
    }

    public int offset()
    {
        return this.offset;
    }

    public String propKey()
    {
        return this.propKey;
    }

    public String name()
    {
        return this.name;
    }

    public String asCanonicalStringVal()
    {
        return this.name();
    }

    public NodePropertyLate copy( final int offset, final String propKey, final String name, final Property prop )
    {
        return new NodePropertyLate( offset, propKey, name, prop );
    }

    public int copy$default$1()
    {
        return this.offset();
    }

    public String copy$default$2()
    {
        return this.propKey();
    }

    public String copy$default$3()
    {
        return this.name();
    }

    public String productPrefix()
    {
        return "NodePropertyLate";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = BoxesRunTime.boxToInteger( this.offset() );
            break;
        case 1:
            var10000 = this.propKey();
            break;
        case 2:
            var10000 = this.name();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof NodePropertyLate;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, this.offset() );
        var1 = Statics.mix( var1, Statics.anyHash( this.propKey() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.name() ) );
        return Statics.finalizeHash( var1, 3 );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var7;
        if ( this != x$1 )
        {
            label65:
            {
                boolean var2;
                if ( x$1 instanceof NodePropertyLate )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label47:
                    {
                        NodePropertyLate var4 = (NodePropertyLate) x$1;
                        if ( this.offset() == var4.offset() )
                        {
                            label56:
                            {
                                String var10000 = this.propKey();
                                String var5 = var4.propKey();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label56;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label56;
                                }

                                var10000 = this.name();
                                String var6 = var4.name();
                                if ( var10000 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label56;
                                    }
                                }
                                else if ( !var10000.equals( var6 ) )
                                {
                                    break label56;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var7 = true;
                                    break label47;
                                }
                            }
                        }

                        var7 = false;
                    }

                    if ( var7 )
                    {
                        break label65;
                    }
                }

                var7 = false;
                return var7;
            }
        }

        var7 = true;
        return var7;
    }
}
