package org.neo4j.cypher.internal.physicalplanning.ast;

import org.neo4j.cypher.internal.v4_0.expressions.Property;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.runtime.BoxesRunTime;

public final class NodeProperty$ implements Serializable
{
    public static NodeProperty$ MODULE$;

    static
    {
        new NodeProperty$();
    }

    private NodeProperty$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "NodeProperty";
    }

    public NodeProperty apply( final int offset, final int propToken, final String name, final Property prop )
    {
        return new NodeProperty( offset, propToken, name, prop );
    }

    public Option<Tuple3<Object,Object,String>> unapply( final NodeProperty x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :
        new Some( new Tuple3( BoxesRunTime.boxToInteger( x$0.offset() ), BoxesRunTime.boxToInteger( x$0.propToken() ), x$0.name() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
