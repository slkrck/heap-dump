package org.neo4j.cypher.internal.physicalplanning.ast;

import org.neo4j.cypher.internal.runtime.ast.RuntimeProperty;
import org.neo4j.cypher.internal.v4_0.expressions.LogicalProperty;
import scala.Option;
import scala.Serializable;
import scala.Tuple3;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class RelationshipPropertyExists extends RuntimeProperty implements Serializable
{
    private final int offset;
    private final int propToken;
    private final String name;

    public RelationshipPropertyExists( final int offset, final int propToken, final String name, final LogicalProperty prop )
    {
        super( prop );
        this.offset = offset;
        this.propToken = propToken;
        this.name = name;
    }

    public static Option<Tuple3<Object,Object,String>> unapply( final RelationshipPropertyExists x$0 )
    {
        return RelationshipPropertyExists$.MODULE$.unapply( var0 );
    }

    public static RelationshipPropertyExists apply( final int offset, final int propToken, final String name, final LogicalProperty prop )
    {
        return RelationshipPropertyExists$.MODULE$.apply( var0, var1, var2, var3 );
    }

    public int offset()
    {
        return this.offset;
    }

    public int propToken()
    {
        return this.propToken;
    }

    public String name()
    {
        return this.name;
    }

    public String asCanonicalStringVal()
    {
        return this.name();
    }

    public RelationshipPropertyExists copy( final int offset, final int propToken, final String name, final LogicalProperty prop )
    {
        return new RelationshipPropertyExists( offset, propToken, name, prop );
    }

    public int copy$default$1()
    {
        return this.offset();
    }

    public int copy$default$2()
    {
        return this.propToken();
    }

    public String copy$default$3()
    {
        return this.name();
    }

    public String productPrefix()
    {
        return "RelationshipPropertyExists";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = BoxesRunTime.boxToInteger( this.offset() );
            break;
        case 1:
            var10000 = BoxesRunTime.boxToInteger( this.propToken() );
            break;
        case 2:
            var10000 = this.name();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof RelationshipPropertyExists;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, this.offset() );
        var1 = Statics.mix( var1, this.propToken() );
        var1 = Statics.mix( var1, Statics.anyHash( this.name() ) );
        return Statics.finalizeHash( var1, 3 );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        if ( this != x$1 )
        {
            label57:
            {
                boolean var2;
                if ( x$1 instanceof RelationshipPropertyExists )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label40:
                    {
                        RelationshipPropertyExists var4 = (RelationshipPropertyExists) x$1;
                        if ( this.offset() == var4.offset() && this.propToken() == var4.propToken() )
                        {
                            label37:
                            {
                                String var10000 = this.name();
                                String var5 = var4.name();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label37;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label37;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var6 = true;
                                    break label40;
                                }
                            }
                        }

                        var6 = false;
                    }

                    if ( var6 )
                    {
                        break label57;
                    }
                }

                var6 = false;
                return var6;
            }
        }

        var6 = true;
        return var6;
    }
}
