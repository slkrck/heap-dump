package org.neo4j.cypher.internal.physicalplanning.ast;

import org.neo4j.cypher.internal.v4_0.expressions.LogicalVariable;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.None.;
import scala.runtime.AbstractFunction2;
import scala.runtime.BoxesRunTime;

public final class NullCheckVariable$ extends AbstractFunction2<Object,LogicalVariable,NullCheckVariable> implements Serializable
{
    public static NullCheckVariable$ MODULE$;

    static
    {
        new NullCheckVariable$();
    }

    private NullCheckVariable$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "NullCheckVariable";
    }

    public NullCheckVariable apply( final int offset, final LogicalVariable inner )
    {
        return new NullCheckVariable( offset, inner );
    }

    public Option<Tuple2<Object,LogicalVariable>> unapply( final NullCheckVariable x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :new Some( new Tuple2( BoxesRunTime.boxToInteger( x$0.offset() ), x$0.inner() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
