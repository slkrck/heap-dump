package org.neo4j.cypher.internal.physicalplanning.ast;

import org.neo4j.cypher.internal.runtime.ast.RuntimeExpression;
import org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticCheckResult;
import org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticState;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import org.neo4j.cypher.internal.v4_0.expressions.Expression.SemanticContext;
import org.neo4j.cypher.internal.v4_0.util.InputPosition;
import scala.Function1;
import scala.Option;
import scala.Serializable;
import scala.Tuple3;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class HasLabelsFromSlot extends Expression implements RuntimeExpression, Serializable
{
    private final int offset;
    private final Seq<Object> resolvedLabelTokens;
    private final Seq<String> lateLabels;

    public HasLabelsFromSlot( final int offset, final Seq<Object> resolvedLabelTokens, final Seq<String> lateLabels )
    {
        this.offset = offset;
        this.resolvedLabelTokens = resolvedLabelTokens;
        this.lateLabels = lateLabels;
        RuntimeExpression.$init$( this );
    }

    public static Option<Tuple3<Object,Seq<Object>,Seq<String>>> unapply( final HasLabelsFromSlot x$0 )
    {
        return HasLabelsFromSlot$.MODULE$.unapply( var0 );
    }

    public static HasLabelsFromSlot apply( final int offset, final Seq<Object> resolvedLabelTokens, final Seq<String> lateLabels )
    {
        return HasLabelsFromSlot$.MODULE$.apply( var0, var1, var2 );
    }

    public static Function1<Tuple3<Object,Seq<Object>,Seq<String>>,HasLabelsFromSlot> tupled()
    {
        return HasLabelsFromSlot$.MODULE$.tupled();
    }

    public static Function1<Object,Function1<Seq<Object>,Function1<Seq<String>,HasLabelsFromSlot>>> curried()
    {
        return HasLabelsFromSlot$.MODULE$.curried();
    }

    public Function1<SemanticState,SemanticCheckResult> semanticCheck( final SemanticContext ctx )
    {
        return RuntimeExpression.semanticCheck$( this, ctx );
    }

    public InputPosition position()
    {
        return RuntimeExpression.position$( this );
    }

    public int offset()
    {
        return this.offset;
    }

    public Seq<Object> resolvedLabelTokens()
    {
        return this.resolvedLabelTokens;
    }

    public Seq<String> lateLabels()
    {
        return this.lateLabels;
    }

    public HasLabelsFromSlot copy( final int offset, final Seq<Object> resolvedLabelTokens, final Seq<String> lateLabels )
    {
        return new HasLabelsFromSlot( offset, resolvedLabelTokens, lateLabels );
    }

    public int copy$default$1()
    {
        return this.offset();
    }

    public Seq<Object> copy$default$2()
    {
        return this.resolvedLabelTokens();
    }

    public Seq<String> copy$default$3()
    {
        return this.lateLabels();
    }

    public String productPrefix()
    {
        return "HasLabelsFromSlot";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = BoxesRunTime.boxToInteger( this.offset() );
            break;
        case 1:
            var10000 = this.resolvedLabelTokens();
            break;
        case 2:
            var10000 = this.lateLabels();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof HasLabelsFromSlot;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, this.offset() );
        var1 = Statics.mix( var1, Statics.anyHash( this.resolvedLabelTokens() ) );
        var1 = Statics.mix( var1, Statics.anyHash( this.lateLabels() ) );
        return Statics.finalizeHash( var1, 3 );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var7;
        if ( this != x$1 )
        {
            label65:
            {
                boolean var2;
                if ( x$1 instanceof HasLabelsFromSlot )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label47:
                    {
                        HasLabelsFromSlot var4 = (HasLabelsFromSlot) x$1;
                        if ( this.offset() == var4.offset() )
                        {
                            label56:
                            {
                                Seq var10000 = this.resolvedLabelTokens();
                                Seq var5 = var4.resolvedLabelTokens();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label56;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label56;
                                }

                                var10000 = this.lateLabels();
                                Seq var6 = var4.lateLabels();
                                if ( var10000 == null )
                                {
                                    if ( var6 != null )
                                    {
                                        break label56;
                                    }
                                }
                                else if ( !var10000.equals( var6 ) )
                                {
                                    break label56;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var7 = true;
                                    break label47;
                                }
                            }
                        }

                        var7 = false;
                    }

                    if ( var7 )
                    {
                        break label65;
                    }
                }

                var7 = false;
                return var7;
            }
        }

        var7 = true;
        return var7;
    }
}
