package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.planner.spi.TokenContext;
import org.neo4j.cypher.internal.runtime.ParameterMapping;
import org.neo4j.cypher.internal.runtime.expressionVariableAllocation.;
import org.neo4j.cypher.internal.runtime.expressionVariableAllocation.AvailableExpressionVariables;
import org.neo4j.cypher.internal.runtime.expressionVariableAllocation.Result;
import org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticTable;
import scala.MatchError;
import scala.Tuple2;
import scala.Tuple3;
import scala.runtime.BoxesRunTime;

public final class PhysicalPlanner$
{
    public static PhysicalPlanner$ MODULE$;

    static
    {
        new PhysicalPlanner$();
    }

    private PhysicalPlanner$()
    {
        MODULE$ = this;
    }

    public PhysicalPlan plan( final TokenContext tokenContext, final LogicalPlan beforeRewrite, final SemanticTable semanticTable,
            final PipelineBreakingPolicy breakingPolicy, final boolean allocateArgumentSlots )
    {
        Result var9 = .MODULE$.allocate( beforeRewrite );
        if ( var9 != null )
        {
            LogicalPlan logicalPlan = (LogicalPlan) var9.rewritten();
            int nExpressionSlots = var9.nExpressionSlots();
            AvailableExpressionVariables availableExpressionVars = var9.availableExpressionVars();
            Tuple3 var7 = new Tuple3( logicalPlan, BoxesRunTime.boxToInteger( nExpressionSlots ), availableExpressionVars );
            LogicalPlan logicalPlan = (LogicalPlan) var7._1();
            int nExpressionSlots = BoxesRunTime.unboxToInt( var7._2() );
            AvailableExpressionVariables availableExpressionVars = (AvailableExpressionVariables) var7._3();
            Tuple2 var17 = org.neo4j.cypher.internal.runtime.slottedParameters..MODULE$.apply( logicalPlan );
            if ( var17 != null )
            {
                LogicalPlan withSlottedParameters = (LogicalPlan) var17._1();
                ParameterMapping parameterMapping = (ParameterMapping) var17._2();
                Tuple2 var6 = new Tuple2( withSlottedParameters, parameterMapping );
                LogicalPlan withSlottedParameters = (LogicalPlan) var6._1();
                ParameterMapping parameterMapping = (ParameterMapping) var6._2();
                SlotAllocation.SlotMetaData slotMetaData =
                        SlotAllocation$.MODULE$.allocateSlots( withSlottedParameters, semanticTable, breakingPolicy, availableExpressionVars,
                                allocateArgumentSlots );
                SlottedRewriter slottedRewriter = new SlottedRewriter( tokenContext );
                LogicalPlan finalLogicalPlan = slottedRewriter.apply( withSlottedParameters, slotMetaData.slotConfigurations() );
                return new PhysicalPlan( finalLogicalPlan, nExpressionSlots, slotMetaData.slotConfigurations(), slotMetaData.argumentSizes(),
                        slotMetaData.applyPlans(), slotMetaData.nestedPlanArgumentConfigurations(), availableExpressionVars, parameterMapping );
            }
            else
            {
                throw new MatchError( var17 );
            }
        }
        else
        {
            throw new MatchError( var9 );
        }
    }

    public boolean plan$default$5()
    {
        return false;
    }
}
