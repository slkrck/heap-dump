package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.exceptions.InternalException;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public class SlotAllocationFailed extends InternalException
{
    public SlotAllocationFailed( final String str )
    {
        super( str );
    }
}
