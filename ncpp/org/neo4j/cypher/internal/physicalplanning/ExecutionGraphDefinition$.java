package org.neo4j.cypher.internal.physicalplanning;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple5;
import scala.None.;
import scala.collection.IndexedSeq;
import scala.collection.immutable.Map;

public final class ExecutionGraphDefinition$ implements Serializable
{
    public static ExecutionGraphDefinition$ MODULE$;

    static
    {
        new ExecutionGraphDefinition$();
    }

    private final ArgumentStateMapId[] NO_ARGUMENT_STATE_MAPS;
    private final BufferId[] NO_BUFFERS;

    private ExecutionGraphDefinition$()
    {
        MODULE$ = this;
        this.NO_ARGUMENT_STATE_MAPS = new ArgumentStateMapId[0];
        this.NO_BUFFERS = new BufferId[0];
    }

    public ArgumentStateMapId[] NO_ARGUMENT_STATE_MAPS()
    {
        return this.NO_ARGUMENT_STATE_MAPS;
    }

    public BufferId[] NO_BUFFERS()
    {
        return this.NO_BUFFERS;
    }

    public ExecutionGraphDefinition apply( final PhysicalPlan physicalPlan, final IndexedSeq<BufferDefinition> buffers,
            final IndexedSeq<ArgumentStateDefinition> argumentStateMaps, final IndexedSeq<PipelineDefinition> pipelines,
            final Map<Object,Object> applyRhsPlans )
    {
        return new ExecutionGraphDefinition( physicalPlan, buffers, argumentStateMaps, pipelines, applyRhsPlans );
    }

    public Option<Tuple5<PhysicalPlan,IndexedSeq<BufferDefinition>,IndexedSeq<ArgumentStateDefinition>,IndexedSeq<PipelineDefinition>,Map<Object,Object>>> unapply(
            final ExecutionGraphDefinition x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :
        new Some( new Tuple5( x$0.physicalPlan(), x$0.buffers(), x$0.argumentStateMaps(), x$0.pipelines(), x$0.applyRhsPlans() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
