package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.v4_0.util.Unchangeable;
import org.neo4j.cypher.internal.v4_0.util.attribution.Attribute;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id;
import scala.Function0;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.collection.mutable.ArrayBuffer;
import scala.reflect.ScalaSignature;

@JavaDocToJava
public final class PhysicalPlanningAttributes
{
    public static class ApplyPlans implements Attribute<LogicalPlan,Id>
    {
        private final ArrayBuffer<Unchangeable<Id>> org$neo4j$cypher$internal$v4_0$util$attribution$Attribute$$array;

        public ApplyPlans()
        {
            Attribute.$init$( this );
        }

        public void set( final int id, final Object t )
        {
            Attribute.set$( this, id, t );
        }

        public Object get( final int id )
        {
            return Attribute.get$( this, id );
        }

        public boolean isDefinedAt( final int id )
        {
            return Attribute.isDefinedAt$( this, id );
        }

        public Object getOrElse( final int id, final Function0 other )
        {
            return Attribute.getOrElse$( this, id, other );
        }

        public Iterator<Tuple2<Id,Id>> iterator()
        {
            return Attribute.iterator$( this );
        }

        public int size()
        {
            return Attribute.size$( this );
        }

        public Object apply( final int id )
        {
            return Attribute.apply$( this, id );
        }

        public void copy( final int from, final int to )
        {
            Attribute.copy$( this, from, to );
        }

        public String toString()
        {
            return Attribute.toString$( this );
        }

        public ArrayBuffer<Unchangeable<Id>> org$neo4j$cypher$internal$v4_0$util$attribution$Attribute$$array()
        {
            return this.org$neo4j$cypher$internal$v4_0$util$attribution$Attribute$$array;
        }

        public final void org$neo4j$cypher$internal$v4_0$util$attribution$Attribute$_setter_$org$neo4j$cypher$internal$v4_0$util$attribution$Attribute$$array_$eq(
                final ArrayBuffer<Unchangeable<Id>> x$1 )
        {
            this.org$neo4j$cypher$internal$v4_0$util$attribution$Attribute$$array = x$1;
        }
    }

    public static class ArgumentSizes implements Attribute<LogicalPlan,SlotConfiguration.Size>
    {
        private final ArrayBuffer<Unchangeable<SlotConfiguration.Size>> org$neo4j$cypher$internal$v4_0$util$attribution$Attribute$$array;

        public ArgumentSizes()
        {
            Attribute.$init$( this );
        }

        public void set( final int id, final Object t )
        {
            Attribute.set$( this, id, t );
        }

        public Object get( final int id )
        {
            return Attribute.get$( this, id );
        }

        public boolean isDefinedAt( final int id )
        {
            return Attribute.isDefinedAt$( this, id );
        }

        public Object getOrElse( final int id, final Function0 other )
        {
            return Attribute.getOrElse$( this, id, other );
        }

        public Iterator<Tuple2<Id,SlotConfiguration.Size>> iterator()
        {
            return Attribute.iterator$( this );
        }

        public int size()
        {
            return Attribute.size$( this );
        }

        public Object apply( final int id )
        {
            return Attribute.apply$( this, id );
        }

        public void copy( final int from, final int to )
        {
            Attribute.copy$( this, from, to );
        }

        public String toString()
        {
            return Attribute.toString$( this );
        }

        public ArrayBuffer<Unchangeable<SlotConfiguration.Size>> org$neo4j$cypher$internal$v4_0$util$attribution$Attribute$$array()
        {
            return this.org$neo4j$cypher$internal$v4_0$util$attribution$Attribute$$array;
        }

        public final void org$neo4j$cypher$internal$v4_0$util$attribution$Attribute$_setter_$org$neo4j$cypher$internal$v4_0$util$attribution$Attribute$$array_$eq(
                final ArrayBuffer<Unchangeable<SlotConfiguration.Size>> x$1 )
        {
            this.org$neo4j$cypher$internal$v4_0$util$attribution$Attribute$$array = x$1;
        }
    }

    public static class NestedPlanArgumentConfigurations implements Attribute<LogicalPlan,SlotConfiguration>
    {
        private final ArrayBuffer<Unchangeable<SlotConfiguration>> org$neo4j$cypher$internal$v4_0$util$attribution$Attribute$$array;

        public NestedPlanArgumentConfigurations()
        {
            Attribute.$init$( this );
        }

        public void set( final int id, final Object t )
        {
            Attribute.set$( this, id, t );
        }

        public Object get( final int id )
        {
            return Attribute.get$( this, id );
        }

        public boolean isDefinedAt( final int id )
        {
            return Attribute.isDefinedAt$( this, id );
        }

        public Object getOrElse( final int id, final Function0 other )
        {
            return Attribute.getOrElse$( this, id, other );
        }

        public Iterator<Tuple2<Id,SlotConfiguration>> iterator()
        {
            return Attribute.iterator$( this );
        }

        public int size()
        {
            return Attribute.size$( this );
        }

        public Object apply( final int id )
        {
            return Attribute.apply$( this, id );
        }

        public void copy( final int from, final int to )
        {
            Attribute.copy$( this, from, to );
        }

        public String toString()
        {
            return Attribute.toString$( this );
        }

        public ArrayBuffer<Unchangeable<SlotConfiguration>> org$neo4j$cypher$internal$v4_0$util$attribution$Attribute$$array()
        {
            return this.org$neo4j$cypher$internal$v4_0$util$attribution$Attribute$$array;
        }

        public final void org$neo4j$cypher$internal$v4_0$util$attribution$Attribute$_setter_$org$neo4j$cypher$internal$v4_0$util$attribution$Attribute$$array_$eq(
                final ArrayBuffer<Unchangeable<SlotConfiguration>> x$1 )
        {
            this.org$neo4j$cypher$internal$v4_0$util$attribution$Attribute$$array = x$1;
        }
    }

    public static class SlotConfigurations implements Attribute<LogicalPlan,SlotConfiguration>
    {
        private final ArrayBuffer<Unchangeable<SlotConfiguration>> org$neo4j$cypher$internal$v4_0$util$attribution$Attribute$$array;

        public SlotConfigurations()
        {
            Attribute.$init$( this );
        }

        public void set( final int id, final Object t )
        {
            Attribute.set$( this, id, t );
        }

        public Object get( final int id )
        {
            return Attribute.get$( this, id );
        }

        public boolean isDefinedAt( final int id )
        {
            return Attribute.isDefinedAt$( this, id );
        }

        public Object getOrElse( final int id, final Function0 other )
        {
            return Attribute.getOrElse$( this, id, other );
        }

        public Iterator<Tuple2<Id,SlotConfiguration>> iterator()
        {
            return Attribute.iterator$( this );
        }

        public int size()
        {
            return Attribute.size$( this );
        }

        public Object apply( final int id )
        {
            return Attribute.apply$( this, id );
        }

        public void copy( final int from, final int to )
        {
            Attribute.copy$( this, from, to );
        }

        public String toString()
        {
            return Attribute.toString$( this );
        }

        public ArrayBuffer<Unchangeable<SlotConfiguration>> org$neo4j$cypher$internal$v4_0$util$attribution$Attribute$$array()
        {
            return this.org$neo4j$cypher$internal$v4_0$util$attribution$Attribute$$array;
        }

        public final void org$neo4j$cypher$internal$v4_0$util$attribution$Attribute$_setter_$org$neo4j$cypher$internal$v4_0$util$attribution$Attribute$$array_$eq(
                final ArrayBuffer<Unchangeable<SlotConfiguration>> x$1 )
        {
            this.org$neo4j$cypher$internal$v4_0$util$attribution$Attribute$$array = x$1;
        }
    }
}
