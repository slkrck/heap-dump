package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.logical.plans.LogicalPlan;

public final class PipelineTreeBuilder$
{
    public static PipelineTreeBuilder$ MODULE$;

    static
    {
        new PipelineTreeBuilder$();
    }

    private final PipelineTreeBuilder.PipelineDefinitionBuild NO_PIPELINE_BUILD;

    private PipelineTreeBuilder$()
    {
        MODULE$ = this;
        this.NO_PIPELINE_BUILD = new PipelineTreeBuilder.PipelineDefinitionBuild( PipelineId$.MODULE$.NO_PIPELINE(), (LogicalPlan) null );
    }

    public PipelineTreeBuilder.PipelineDefinitionBuild NO_PIPELINE_BUILD()
    {
        return this.NO_PIPELINE_BUILD;
    }
}
