package org.neo4j.cypher.internal.physicalplanning;

import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

public final class RegularBufferVariant$ implements BufferVariant, Product, Serializable
{
    public static RegularBufferVariant$ MODULE$;

    static
    {
        new RegularBufferVariant$();
    }

    private RegularBufferVariant$()
    {
        MODULE$ = this;
        Product.$init$( this );
    }

    public String productPrefix()
    {
        return "RegularBufferVariant";
    }

    public int productArity()
    {
        return 0;
    }

    public Object productElement( final int x$1 )
    {
        throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof RegularBufferVariant$;
    }

    public int hashCode()
    {
        return -238399991;
    }

    public String toString()
    {
        return "RegularBufferVariant";
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
