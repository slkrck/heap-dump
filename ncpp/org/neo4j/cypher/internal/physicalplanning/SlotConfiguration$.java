package org.neo4j.cypher.internal.physicalplanning;

import scala.collection.immutable.Map;
import scala.collection.mutable.Map.;
import scala.runtime.BoxesRunTime;

public final class SlotConfiguration$
{
    public static SlotConfiguration$ MODULE$;

    static
    {
        new SlotConfiguration$();
    }

    private SlotConfiguration$()
    {
        MODULE$ = this;
    }

    public SlotConfiguration empty()
    {
        return new SlotConfiguration(.MODULE$.empty(), .MODULE$.empty(), .MODULE$.empty(), 0, 0);
    }

    public SlotConfiguration apply( final Map<String,Slot> slots, final int numberOfLongs, final int numberOfReferences )
    {
        scala.collection.mutable.Map stringToSlot = (scala.collection.mutable.Map).MODULE$.apply( slots.toSeq() );
        return new SlotConfiguration( stringToSlot,.MODULE$.empty(), .MODULE$.empty(), numberOfLongs, numberOfReferences);
    }

    public boolean isLongSlot( final Slot slot )
    {
        boolean var2;
        if ( slot instanceof LongSlot )
        {
            var2 = true;
        }
        else
        {
            var2 = false;
        }

        return var2;
    }

    public final boolean isRefSlotAndNotAlias( final SlotConfiguration slots, final String k )
    {
        return !slots.isAlias( k ) && slots.get( k ).forall( ( x$1 ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$isRefSlotAndNotAlias$1( x$1 ) );
        } );
    }
}
