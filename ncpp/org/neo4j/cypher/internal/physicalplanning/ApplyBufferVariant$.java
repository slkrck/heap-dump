package org.neo4j.cypher.internal.physicalplanning;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple3;
import scala.None.;
import scala.runtime.AbstractFunction3;
import scala.runtime.BoxesRunTime;

public final class ApplyBufferVariant$ extends AbstractFunction3<Object,int[],int[],ApplyBufferVariant> implements Serializable
{
    public static ApplyBufferVariant$ MODULE$;

    static
    {
        new ApplyBufferVariant$();
    }

    private ApplyBufferVariant$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "ApplyBufferVariant";
    }

    public ApplyBufferVariant apply( final int argumentSlotOffset, final ArgumentStateMapId[] reducersOnRHSReversed, final BufferId[] delegates )
    {
        return new ApplyBufferVariant( argumentSlotOffset, reducersOnRHSReversed, delegates );
    }

    public Option<Tuple3<Object,int[],int[]>> unapply( final ApplyBufferVariant x$0 )
    {
        return (Option) (x$0 == null ?.MODULE$ :
        new Some( new Tuple3( BoxesRunTime.boxToInteger( x$0.argumentSlotOffset() ), x$0.reducersOnRHSReversed(), x$0.delegates() ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }
}
