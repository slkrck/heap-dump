package org.neo4j.cypher.internal.physicalplanning;

public final class PhysicalPlanningAttributes$
{
    public static PhysicalPlanningAttributes$ MODULE$;

    static
    {
        new PhysicalPlanningAttributes$();
    }

    private PhysicalPlanningAttributes$()
    {
        MODULE$ = this;
    }
}
