package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.logical.plans.VariablePredicate;
import org.neo4j.cypher.internal.runtime.ast.ExpressionVariable;
import org.neo4j.cypher.internal.v4_0.expressions.LogicalVariable;
import org.neo4j.exceptions.InternalException;
import scala.MatchError;
import scala.Option;
import scala.Some;
import scala.None.;

public final class VariablePredicates$
{
    public static VariablePredicates$ MODULE$;

    static
    {
        new VariablePredicates$();
    }

    private final int NO_PREDICATE_OFFSET;

    private VariablePredicates$()
    {
        MODULE$ = this;
        this.NO_PREDICATE_OFFSET = -1;
    }

    public int NO_PREDICATE_OFFSET()
    {
        return this.NO_PREDICATE_OFFSET;
    }

    public int expressionSlotForPredicate( final Option<VariablePredicate> predicate )
    {
        boolean var3 = false;
        Some var4 = null;
        int var2;
        if (.MODULE$.equals( predicate )){
        var2 = this.NO_PREDICATE_OFFSET();
        return var2;
    } else{
        if ( predicate instanceof Some )
        {
            var3 = true;
            var4 = (Some) predicate;
            VariablePredicate var6 = (VariablePredicate) var4.value();
            if ( var6 != null )
            {
                LogicalVariable var7 = var6.variable();
                if ( var7 instanceof ExpressionVariable )
                {
                    ExpressionVariable var8 = (ExpressionVariable) var7;
                    int offset = var8.offset();
                    var2 = offset;
                    return var2;
                }
            }
        }

        if ( var3 )
        {
            VariablePredicate var10 = (VariablePredicate) var4.value();
            if ( var10 != null )
            {
                LogicalVariable v = var10.variable();
                throw new InternalException(
                        (new StringBuilder( 90 )).append( "Failure during physical planning: the expression slot of variable " ).append( v ).append(
                                " has not been allocated." ).toString() );
            }
        }

        throw new MatchError( predicate );
    }
    }
}
