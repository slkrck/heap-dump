package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class ReduceOutput implements OutputDefinition, Product, Serializable
{
    private final int bufferId;
    private final LogicalPlan plan;

    public ReduceOutput( final int bufferId, final LogicalPlan plan )
    {
        this.bufferId = bufferId;
        this.plan = plan;
        Product.$init$( this );
    }

    public static Option<Tuple2<BufferId,LogicalPlan>> unapply( final ReduceOutput x$0 )
    {
        return ReduceOutput$.MODULE$.unapply( var0 );
    }

    public static ReduceOutput apply( final int bufferId, final LogicalPlan plan )
    {
        return ReduceOutput$.MODULE$.apply( var0, var1 );
    }

    public static Function1<Tuple2<BufferId,LogicalPlan>,ReduceOutput> tupled()
    {
        return ReduceOutput$.MODULE$.tupled();
    }

    public static Function1<BufferId,Function1<LogicalPlan,ReduceOutput>> curried()
    {
        return ReduceOutput$.MODULE$.curried();
    }

    public int bufferId()
    {
        return this.bufferId;
    }

    public LogicalPlan plan()
    {
        return this.plan;
    }

    public ReduceOutput copy( final int bufferId, final LogicalPlan plan )
    {
        return new ReduceOutput( bufferId, plan );
    }

    public int copy$default$1()
    {
        return this.bufferId();
    }

    public LogicalPlan copy$default$2()
    {
        return this.plan();
    }

    public String productPrefix()
    {
        return "ReduceOutput";
    }

    public int productArity()
    {
        return 2;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = new BufferId( this.bufferId() );
            break;
        case 1:
            var10000 = this.plan();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof ReduceOutput;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        if ( this != x$1 )
        {
            label55:
            {
                boolean var2;
                if ( x$1 instanceof ReduceOutput )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label38:
                    {
                        ReduceOutput var4 = (ReduceOutput) x$1;
                        if ( this.bufferId() == var4.bufferId() )
                        {
                            label36:
                            {
                                LogicalPlan var10000 = this.plan();
                                LogicalPlan var5 = var4.plan();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label36;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label36;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var6 = true;
                                    break label38;
                                }
                            }
                        }

                        var6 = false;
                    }

                    if ( var6 )
                    {
                        break label55;
                    }
                }

                var6 = false;
                return var6;
            }
        }

        var6 = true;
        return var6;
    }
}
