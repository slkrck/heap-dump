package org.neo4j.cypher.internal.physicalplanning;

import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple5;
import scala.collection.IndexedSeq;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.reflect.ScalaSignature;
import scala.reflect.ClassTag.;
import scala.runtime.BoxesRunTime;

@JavaDocToJava
public class BufferDefinition implements Product, Serializable
{
    private final int id;
    private final ArgumentStateMapId[] reducers;
    private final ArgumentStateMapId[] workCancellers;
    private final ArgumentStateMapId[] downstreamStates;
    private final BufferVariant variant;
    private final SlotConfiguration bufferSlotConfiguration;

    public BufferDefinition( final int id, final ArgumentStateMapId[] reducers, final ArgumentStateMapId[] workCancellers,
            final ArgumentStateMapId[] downstreamStates, final BufferVariant variant, final SlotConfiguration bufferSlotConfiguration )
    {
        this.id = id;
        this.reducers = reducers;
        this.workCancellers = workCancellers;
        this.downstreamStates = downstreamStates;
        this.variant = variant;
        this.bufferSlotConfiguration = bufferSlotConfiguration;
        Product.$init$( this );
    }

    public static Option<Tuple5<BufferId,int[],int[],int[],BufferVariant>> unapply( final BufferDefinition x$0 )
    {
        return BufferDefinition$.MODULE$.unapply( var0 );
    }

    public static BufferDefinition apply( final int id, final ArgumentStateMapId[] reducers, final ArgumentStateMapId[] workCancellers,
            final ArgumentStateMapId[] downstreamStates, final BufferVariant variant, final SlotConfiguration bufferSlotConfiguration )
    {
        return BufferDefinition$.MODULE$.apply( var0, var1, var2, var3, var4, var5 );
    }

    public int id()
    {
        return this.id;
    }

    public ArgumentStateMapId[] reducers()
    {
        return this.reducers;
    }

    public ArgumentStateMapId[] workCancellers()
    {
        return this.workCancellers;
    }

    public ArgumentStateMapId[] downstreamStates()
    {
        return this.downstreamStates;
    }

    public BufferVariant variant()
    {
        return this.variant;
    }

    public SlotConfiguration bufferSlotConfiguration()
    {
        return this.bufferSlotConfiguration;
    }

    public BufferDefinition withReducers( final IndexedSeq<ArgumentStateMapId> reducers )
    {
        ArgumentStateMapId[] x$3 = (ArgumentStateMapId[]) reducers.toArray(.MODULE$.apply( ArgumentStateMapId.class ));
        int x$4 = this.copy$default$1();
        ArgumentStateMapId[] x$5 = this.copy$default$3();
        ArgumentStateMapId[] x$6 = this.copy$default$4();
        BufferVariant x$7 = this.copy$default$5();
        SlotConfiguration x$8 = this.bufferSlotConfiguration();
        return this.copy( x$4, x$3, x$5, x$6, x$7, x$8 );
    }

    public BufferDefinition withWorkCancellers( final IndexedSeq<ArgumentStateMapId> workCancellers )
    {
        ArgumentStateMapId[] x$9 = (ArgumentStateMapId[]) workCancellers.toArray(.MODULE$.apply( ArgumentStateMapId.class ));
        int x$10 = this.copy$default$1();
        ArgumentStateMapId[] x$11 = this.copy$default$2();
        ArgumentStateMapId[] x$12 = this.copy$default$4();
        BufferVariant x$13 = this.copy$default$5();
        SlotConfiguration x$14 = this.bufferSlotConfiguration();
        return this.copy( x$10, x$11, x$9, x$12, x$13, x$14 );
    }

    public boolean canEqual( final Object that )
    {
        return that instanceof BufferDefinition;
    }

    public boolean equals( final Object obj )
    {
        boolean var10000;
        if ( obj instanceof BufferDefinition )
        {
            BufferDefinition other = (BufferDefinition) obj;
            if ( this.asTuple().equals( other.asTuple() ) )
            {
                var10000 = true;
                return var10000;
            }
        }

        var10000 = false;
        return var10000;
    }

    public int hashCode()
    {
        return this.asTuple().hashCode();
    }

    private Tuple5<BufferId,Seq<ArgumentStateMapId>,Seq<ArgumentStateMapId>,Seq<ArgumentStateMapId>,BufferVariant> asTuple()
    {
        return new Tuple5( new BufferId( this.id() ), scala.Predef..MODULE$.genericArrayOps( this.reducers() ).toList(),scala.Predef..
        MODULE$.genericArrayOps( this.workCancellers() ).toList(), scala.Predef..MODULE$.genericArrayOps( this.downstreamStates() ).toList(), this.variant());
    }

    public String toString()
    {
        return (new StringBuilder( 16 )).append( "BufferDefinition" ).append( this.asTuple() ).toString();
    }

    public BufferDefinition copy( final int id, final ArgumentStateMapId[] reducers, final ArgumentStateMapId[] workCancellers,
            final ArgumentStateMapId[] downstreamStates, final BufferVariant variant, final SlotConfiguration bufferSlotConfiguration )
    {
        return new BufferDefinition( id, reducers, workCancellers, downstreamStates, variant, bufferSlotConfiguration );
    }

    public int copy$default$1()
    {
        return this.id();
    }

    public ArgumentStateMapId[] copy$default$2()
    {
        return this.reducers();
    }

    public ArgumentStateMapId[] copy$default$3()
    {
        return this.workCancellers();
    }

    public ArgumentStateMapId[] copy$default$4()
    {
        return this.downstreamStates();
    }

    public BufferVariant copy$default$5()
    {
        return this.variant();
    }

    public String productPrefix()
    {
        return "BufferDefinition";
    }

    public int productArity()
    {
        return 5;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = new BufferId( this.id() );
            break;
        case 1:
            var10000 = this.reducers();
            break;
        case 2:
            var10000 = this.workCancellers();
            break;
        case 3:
            var10000 = this.downstreamStates();
            break;
        case 4:
            var10000 = this.variant();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( this );
    }
}
