package org.neo4j.cypher.internal.physicalplanning;

import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.None.;
import scala.collection.Iterator;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

public final class BufferId$ extends AbstractFunction1<Object,BufferId> implements Serializable
{
    public static BufferId$ MODULE$;

    static
    {
        new BufferId$();
    }

    private BufferId$()
    {
        MODULE$ = this;
    }

    public final String toString()
    {
        return "BufferId";
    }

    public int apply( final int x )
    {
        return x;
    }

    public Option<Object> unapply( final int x$0 )
    {
        return (Option) (new BufferId( x$0 ) == null ?.MODULE$ :new Some( BoxesRunTime.boxToInteger( x$0 ) ));
    }

    private Object readResolve()
    {
        return MODULE$;
    }

    public final int copy$extension( final int $this, final int x )
    {
        return x;
    }

    public final int copy$default$1$extension( final int $this )
    {
        return $this;
    }

    public final String productPrefix$extension( final int $this )
    {
        return "BufferId";
    }

    public final int productArity$extension( final int $this )
    {
        return 1;
    }

    public final Object productElement$extension( final int $this, final int x$1 )
    {
        switch ( x$1 )
        {
        case 0:
            return BoxesRunTime.boxToInteger( $this );
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }
    }

    public final Iterator<Object> productIterator$extension( final int $this )
    {
        return scala.runtime.ScalaRunTime..MODULE$.typedProductIterator( new BufferId( $this ) );
    }

    public final boolean canEqual$extension( final int $this, final Object x$1 )
    {
        return x$1 instanceof Integer;
    }

    public final int hashCode$extension( final int $this )
    {
        return BoxesRunTime.boxToInteger( $this ).hashCode();
    }

    public final boolean equals$extension( final int $this, final Object x$1 )
    {
        boolean var3;
        if ( x$1 instanceof BufferId )
        {
            var3 = true;
        }
        else
        {
            var3 = false;
        }

        boolean var10000;
        if ( var3 )
        {
            int var5 = ((BufferId) x$1).x();
            if ( $this == var5 )
            {
                var10000 = true;
                return var10000;
            }
        }

        var10000 = false;
        return var10000;
    }

    public final String toString$extension( final int $this )
    {
        return scala.runtime.ScalaRunTime..MODULE$._toString( new BufferId( $this ) );
    }
}
