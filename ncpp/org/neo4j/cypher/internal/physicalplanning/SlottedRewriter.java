package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.ir.VarPatternLength;
import org.neo4j.cypher.internal.logical.plans.AggregatingPlan;
import org.neo4j.cypher.internal.logical.plans.ExpansionMode;
import org.neo4j.cypher.internal.logical.plans.LogicalPlan;
import org.neo4j.cypher.internal.logical.plans.NestedPlanExpression;
import org.neo4j.cypher.internal.logical.plans.Projection;
import org.neo4j.cypher.internal.logical.plans.ValueHashJoin;
import org.neo4j.cypher.internal.logical.plans.VarExpand;
import org.neo4j.cypher.internal.logical.plans.VariablePredicate;
import org.neo4j.cypher.internal.physicalplanning.ast.GetDegreePrimitive;
import org.neo4j.cypher.internal.physicalplanning.ast.HasLabelsFromSlot;
import org.neo4j.cypher.internal.physicalplanning.ast.IdFromSlot;
import org.neo4j.cypher.internal.physicalplanning.ast.IsPrimitiveNull;
import org.neo4j.cypher.internal.physicalplanning.ast.LabelsFromSlot;
import org.neo4j.cypher.internal.physicalplanning.ast.NodeFromSlot;
import org.neo4j.cypher.internal.physicalplanning.ast.NodeProperty;
import org.neo4j.cypher.internal.physicalplanning.ast.NodePropertyExists;
import org.neo4j.cypher.internal.physicalplanning.ast.NodePropertyExistsLate;
import org.neo4j.cypher.internal.physicalplanning.ast.NodePropertyLate;
import org.neo4j.cypher.internal.physicalplanning.ast.NullCheck;
import org.neo4j.cypher.internal.physicalplanning.ast.NullCheckProperty;
import org.neo4j.cypher.internal.physicalplanning.ast.NullCheckReferenceProperty;
import org.neo4j.cypher.internal.physicalplanning.ast.NullCheckVariable;
import org.neo4j.cypher.internal.physicalplanning.ast.PrimitiveEquals;
import org.neo4j.cypher.internal.physicalplanning.ast.ReferenceFromSlot;
import org.neo4j.cypher.internal.physicalplanning.ast.RelationshipFromSlot;
import org.neo4j.cypher.internal.physicalplanning.ast.RelationshipProperty;
import org.neo4j.cypher.internal.physicalplanning.ast.RelationshipPropertyExists;
import org.neo4j.cypher.internal.physicalplanning.ast.RelationshipPropertyExistsLate;
import org.neo4j.cypher.internal.physicalplanning.ast.RelationshipPropertyLate;
import org.neo4j.cypher.internal.physicalplanning.ast.RelationshipTypeFromSlot;
import org.neo4j.cypher.internal.physicalplanning.ast.SlottedCachedPropertyWithPropertyToken;
import org.neo4j.cypher.internal.physicalplanning.ast.SlottedCachedPropertyWithoutPropertyToken;
import org.neo4j.cypher.internal.planner.spi.TokenContext;
import org.neo4j.cypher.internal.runtime.ast.RuntimeProperty;
import org.neo4j.cypher.internal.v4_0.expressions.And;
import org.neo4j.cypher.internal.v4_0.expressions.CachedProperty;
import org.neo4j.cypher.internal.v4_0.expressions.EntityType;
import org.neo4j.cypher.internal.v4_0.expressions.Equals;
import org.neo4j.cypher.internal.v4_0.expressions.Expression;
import org.neo4j.cypher.internal.v4_0.expressions.False;
import org.neo4j.cypher.internal.v4_0.expressions.FunctionInvocation;
import org.neo4j.cypher.internal.v4_0.expressions.FunctionName;
import org.neo4j.cypher.internal.v4_0.expressions.GetDegree;
import org.neo4j.cypher.internal.v4_0.expressions.HasLabels;
import org.neo4j.cypher.internal.v4_0.expressions.IsNotNull;
import org.neo4j.cypher.internal.v4_0.expressions.IsNull;
import org.neo4j.cypher.internal.v4_0.expressions.LogicalProperty;
import org.neo4j.cypher.internal.v4_0.expressions.LogicalVariable;
import org.neo4j.cypher.internal.v4_0.expressions.Namespace;
import org.neo4j.cypher.internal.v4_0.expressions.Not;
import org.neo4j.cypher.internal.v4_0.expressions.Or;
import org.neo4j.cypher.internal.v4_0.expressions.Property;
import org.neo4j.cypher.internal.v4_0.expressions.PropertyKeyName;
import org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection;
import org.neo4j.cypher.internal.v4_0.expressions.True;
import org.neo4j.cypher.internal.v4_0.expressions.Variable;
import org.neo4j.cypher.internal.v4_0.expressions.functions.Function;
import org.neo4j.cypher.internal.v4_0.util.InputPosition;
import org.neo4j.cypher.internal.v4_0.util.attribution.Id;
import org.neo4j.cypher.internal.v4_0.util.attribution.SameId;
import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import org.neo4j.cypher.internal.v4_0.util.symbols.NodeType;
import org.neo4j.cypher.internal.v4_0.util.symbols.RelationshipType;
import org.neo4j.cypher.internal.v4_0.util.topDown.;
import org.neo4j.exceptions.CantCompileQueryException;
import org.neo4j.exceptions.InternalException;
import scala.Function1;
import scala.MatchError;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.collection.IndexedSeq;
import scala.collection.Seq;
import scala.collection.immutable.Map;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ObjectRef;

@JavaDocToJava
public class SlottedRewriter
{
    public final TokenContext org$neo4j$cypher$internal$physicalplanning$SlottedRewriter$$tokenContext;

    public SlottedRewriter( final TokenContext tokenContext )
    {
        this.org$neo4j$cypher$internal$physicalplanning$SlottedRewriter$$tokenContext = tokenContext;
    }

    private static final Expression makeNegativeIfNeeded$1( final Expression e, final boolean positiveCheck$1 )
    {
        return (Expression) (!positiveCheck$1 ? new Not( e, e.position() ) : e);
    }

    private static final Expression nullCheckIfNeeded$1( final Slot slot, final Expression p )
    {
        return (Expression) (slot.nullable() ? new NullCheck( slot.offset(), p ) : p);
    }

    public boolean org$neo4j$cypher$internal$physicalplanning$SlottedRewriter$$rewriteUsingIncoming( final LogicalPlan oldPlan )
    {
        boolean var2;
        if ( oldPlan instanceof AggregatingPlan )
        {
            var2 = true;
        }
        else
        {
            var2 = false;
        }

        return var2;
    }

    public LogicalPlan apply( final LogicalPlan in, final PhysicalPlanningAttributes.SlotConfigurations slotConfigurations )
    {
        Function1 rewritePlanWithSlots = .
        MODULE$.apply( org.neo4j.cypher.internal.v4_0.util.Rewriter..MODULE$.lift( new Serializable( this, slotConfigurations )
        {
            public static final long serialVersionUID = 0L;
            private final PhysicalPlanningAttributes.SlotConfigurations slotConfigurations$1;

            public
            {
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                    this.slotConfigurations$1 = slotConfigurations$1;
                }
            }

            public final <A1, B1> B1 applyOrElse( final A1 x1, final Function1<A1,B1> default )
            {
                boolean var4 = false;
                LogicalPlan var5 = null;
                Object var3;
                if ( x1 instanceof Projection )
                {
                    Projection var7 = (Projection) x1;
                    Map expressions = var7.projectExpressions();
                    SlotConfiguration slotConfiguration = (SlotConfiguration) this.slotConfigurations$1.apply( var7.id() );
                    Function1 rewriterx = this.$outer.org$neo4j$cypher$internal$physicalplanning$SlottedRewriter$$rewriteCreator( slotConfiguration, var7,
                            this.slotConfigurations$1 );
                    Map newExpressions = (Map) expressions.collect( new Serializable( null, rewriterx )
                    {
                        public static final long serialVersionUID = 0L;
                        private final Function1 rewriter$1;

                        public
                        {
                            this.rewriter$1 = rewriter$1;
                        }

                        public final <A1 extends Tuple2<String,Expression>, B1> B1 applyOrElse( final A1 x2, final Function1<A1,B1> default )
                        {
                            Object var3;
                            if ( x2 != null )
                            {
                                String column = (String) x2._1();
                                Expression expression = (Expression) x2._2();
                                var3 = scala.Predef.ArrowAssoc..MODULE$.$minus$greater$extension( scala.Predef..MODULE$.ArrowAssoc(
                                    column ), org.neo4j.cypher.internal.v4_0.util.Rewritable.RewritableAny..
                                MODULE$.endoRewrite$extension( org.neo4j.cypher.internal.v4_0.util.Rewritable..MODULE$.RewritableAny(
                                        expression ), this.rewriter$1));
                            }
                            else
                            {
                                var3 = var2.apply( x2 );
                            }

                            return var3;
                        }

                        public final boolean isDefinedAt( final Tuple2<String,Expression> x2 )
                        {
                            boolean var2;
                            if ( x2 != null )
                            {
                                var2 = true;
                            }
                            else
                            {
                                var2 = false;
                            }

                            return var2;
                        }
                    }, scala.collection.immutable.Map..MODULE$.canBuildFrom());
                    LogicalPlan x$8 = var7.copy$default$1();
                    SameId x$9 = new SameId( var7.id() );
                    Projection newPlanxx = var7.copy( x$8, newExpressions, x$9 );
                    var3 = newPlanxx;
                }
                else if ( x1 instanceof VarExpand )
                {
                    VarExpand var16 = (VarExpand) x1;
                    SlotConfiguration incomingSlotConfigurationx = (SlotConfiguration) this.slotConfigurations$1.apply( var16.source().id() );
                    Function1 rewriterxxx =
                            this.$outer.org$neo4j$cypher$internal$physicalplanning$SlottedRewriter$$rewriteCreator( incomingSlotConfigurationx, var16,
                                    this.slotConfigurations$1 );
                    Option newNodePredicate = var16.nodePredicate().map( ( x ) -> {
                        return new VariablePredicate( x.variable(),
                                (Expression) org.neo4j.cypher.internal.v4_0.util.Rewritable.RewritableAny..MODULE$.endoRewrite$extension(
                                org.neo4j.cypher.internal.v4_0.util.Rewritable..MODULE$.RewritableAny( x.predicate() ),rewriterxxx));
                    } ); Option newRelationshipPredicate = var16.relationshipPredicate().map( ( x ) -> {
                    return new VariablePredicate( x.variable(),
                            (Expression) org.neo4j.cypher.internal.v4_0.util.Rewritable.RewritableAny..MODULE$.endoRewrite$extension(
                            org.neo4j.cypher.internal.v4_0.util.Rewritable..MODULE$.RewritableAny( x.predicate() ),rewriterxxx));
                } ); LogicalPlan x$12 = var16.copy$default$1();
                    String x$13 = var16.copy$default$2();
                    SemanticDirection x$14 = var16.copy$default$3();
                    SemanticDirection x$15 = var16.copy$default$4();
                    Seq x$16 = var16.copy$default$5();
                    String x$17 = var16.copy$default$6();
                    String x$18 = var16.copy$default$7();
                    VarPatternLength x$19 = var16.copy$default$8();
                    ExpansionMode x$20 = var16.copy$default$9();
                    SameId x$21 = new SameId( var16.id() );
                    VarExpand newPlanxxx = var16.copy( x$12, x$13, x$14, x$15, x$16, x$17, x$18, x$19, x$20, newNodePredicate, newRelationshipPredicate, x$21 );
                    var3 = newPlanxxx;
                }
                else
                {
                    if ( x1 instanceof ValueHashJoin )
                    {
                        ValueHashJoin var34 = (ValueHashJoin) x1;
                        LogicalPlan lhs = var34.left();
                        LogicalPlan rhs = var34.right();
                        Equals e = var34.join();
                        if ( e != null )
                        {
                            Expression lhsExp = e.lhs();
                            Expression rhsExp = e.rhs();
                            Function1 lhsRewriter = this.$outer.org$neo4j$cypher$internal$physicalplanning$SlottedRewriter$$rewriteCreator(
                                    (SlotConfiguration) this.slotConfigurations$1.apply( lhs.id() ), var34, this.slotConfigurations$1 );
                            Function1 rhsRewriter = this.$outer.org$neo4j$cypher$internal$physicalplanning$SlottedRewriter$$rewriteCreator(
                                    (SlotConfiguration) this.slotConfigurations$1.apply( rhs.id() ), var34, this.slotConfigurations$1 );
                            Expression lhsExpAfterRewrite = (Expression) org.neo4j.cypher.internal.v4_0.util.Rewritable.RewritableAny..
                            MODULE$.endoRewrite$extension( org.neo4j.cypher.internal.v4_0.util.Rewritable..MODULE$.RewritableAny( lhsExp ), lhsRewriter);
                            Expression rhsExpAfterRewrite = (Expression) org.neo4j.cypher.internal.v4_0.util.Rewritable.RewritableAny..
                            MODULE$.endoRewrite$extension( org.neo4j.cypher.internal.v4_0.util.Rewritable..MODULE$.RewritableAny( rhsExp ), rhsRewriter);
                            Equals x$22 = new Equals( lhsExpAfterRewrite, rhsExpAfterRewrite, e.position() );
                            LogicalPlan x$23 = var34.copy$default$1();
                            LogicalPlan x$24 = var34.copy$default$2();
                            SameId x$25 = new SameId( var34.id() );
                            var3 = var34.copy( x$23, x$24, x$22, x$25 );
                            return var3;
                        }
                    }

                    if ( x1 instanceof LogicalPlan )
                    {
                        var4 = true;
                        var5 = (LogicalPlan) x1;
                        if ( this.$outer.org$neo4j$cypher$internal$physicalplanning$SlottedRewriter$$rewriteUsingIncoming( var5 ) )
                        {
                            LogicalPlan leftPlan = (LogicalPlan) var5.lhs().getOrElse( () -> {
                                throw new InternalException( "Leaf plans cannot be rewritten this way" );
                            } );
                            SlotConfiguration incomingSlotConfiguration = (SlotConfiguration) this.slotConfigurations$1.apply( leftPlan.id() );
                            Function1 rewriter =
                                    this.$outer.org$neo4j$cypher$internal$physicalplanning$SlottedRewriter$$rewriteCreator( incomingSlotConfiguration, var5,
                                            this.slotConfigurations$1 );
                            LogicalPlan newPlan = (LogicalPlan) org.neo4j.cypher.internal.v4_0.util.Rewritable.RewritableAny..
                            MODULE$.endoRewrite$extension( org.neo4j.cypher.internal.v4_0.util.Rewritable..MODULE$.RewritableAny( var5 ), rewriter);
                            var3 = newPlan;
                            return var3;
                        }
                    }

                    if ( var4 )
                    {
                        SlotConfiguration slotConfigurationx = (SlotConfiguration) this.slotConfigurations$1.apply( var5.id() );
                        Function1 rewriterxx = this.$outer.org$neo4j$cypher$internal$physicalplanning$SlottedRewriter$$rewriteCreator( slotConfigurationx, var5,
                                this.slotConfigurations$1 );
                        LogicalPlan newPlanx = (LogicalPlan) org.neo4j.cypher.internal.v4_0.util.Rewritable.RewritableAny..
                        MODULE$.endoRewrite$extension( org.neo4j.cypher.internal.v4_0.util.Rewritable..MODULE$.RewritableAny( var5 ), rewriterxx);
                        var3 = newPlanx;
                    }
                    else
                    {
                        var3 = var2.apply( x1 );
                    }
                }

                return var3;
            }

            public final boolean isDefinedAt( final Object x1 )
            {
                boolean var3 = false;
                LogicalPlan var4 = null;
                boolean var2;
                if ( x1 instanceof Projection )
                {
                    var2 = true;
                }
                else if ( x1 instanceof VarExpand )
                {
                    var2 = true;
                }
                else
                {
                    if ( x1 instanceof ValueHashJoin )
                    {
                        ValueHashJoin var6 = (ValueHashJoin) x1;
                        Equals e = var6.join();
                        if ( e != null )
                        {
                            var2 = true;
                            return var2;
                        }
                    }

                    if ( x1 instanceof LogicalPlan )
                    {
                        var3 = true;
                        var4 = (LogicalPlan) x1;
                        if ( this.$outer.org$neo4j$cypher$internal$physicalplanning$SlottedRewriter$$rewriteUsingIncoming( var4 ) )
                        {
                            var2 = true;
                            return var2;
                        }
                    }

                    if ( var3 )
                    {
                        var2 = true;
                    }
                    else
                    {
                        var2 = false;
                    }
                }

                return var2;
            }
        } ), .MODULE$.apply$default$2());
        LogicalPlan resultPlan = (LogicalPlan) org.neo4j.cypher.internal.v4_0.util.Rewritable.RewritableAny..
        MODULE$.endoRewrite$extension( org.neo4j.cypher.internal.v4_0.util.Rewritable..MODULE$.RewritableAny( in ), rewritePlanWithSlots);
        org.neo4j.cypher.internal.v4_0.util.AssertionUtils..MODULE$.ifAssertionsEnabled( () -> {
        org.neo4j.cypher.internal.v4_0.util.Foldable.FoldableAny..
        MODULE$.findByAllClass$extension( org.neo4j.cypher.internal.v4_0.util.Foldable..MODULE$.FoldableAny( resultPlan ), scala.reflect.ClassTag..
        MODULE$.apply( Variable.class )).foreach( ( v ) -> {
            throw new CantCompileQueryException(
                    (new StringBuilder( 24 )).append( "Failed to rewrite away " ).append( v ).append( "\n" ).append( resultPlan ).toString() );
        } );
    } ); return resultPlan;
    }

    public Function1<Object,Object> org$neo4j$cypher$internal$physicalplanning$SlottedRewriter$$rewriteCreator( final SlotConfiguration slotConfiguration,
            final LogicalPlan thisPlan, final PhysicalPlanningAttributes.SlotConfigurations slotConfigurations )
    {
        Function1 innerRewriter = org.neo4j.cypher.internal.v4_0.util.Rewriter..
        MODULE$.lift( new Serializable( this, slotConfiguration, thisPlan, slotConfigurations )
        {
            public static final long serialVersionUID = 0L;
            private final SlotConfiguration slotConfiguration$1;
            private final LogicalPlan thisPlan$1;
            private final PhysicalPlanningAttributes.SlotConfigurations slotConfigurations$2;

            public
            {
                if ( $outer == null )
                {
                    throw null;
                }
                else
                {
                    this.$outer = $outer;
                    this.slotConfiguration$1 = slotConfiguration$1;
                    this.thisPlan$1 = thisPlan$1;
                    this.slotConfigurations$2 = slotConfigurations$2;
                }
            }

            public final <A1, B1> B1 applyOrElse( final A1 x3, final Function1<A1,B1> default )
            {
                boolean var26 = false;
                IsNull var27 = null;
                boolean var28 = false;
                ObjectRef var29 = ObjectRef.create( (Object) null );
                Object var3;
                if ( x3 instanceof NestedPlanExpression )
                {
                    NestedPlanExpression var31 = (NestedPlanExpression) x3;
                    LogicalPlan rewrittenPlan = this.$outer.apply( var31.plan(), this.slotConfigurations$2 );
                    SlotConfiguration innerSlotConf = (SlotConfiguration) this.slotConfigurations$2.getOrElse( var31.plan().id(), () -> {
                        throw new InternalException( (new StringBuilder( 41 )).append( "Missing slot configuration for plan with " ).append(
                                new Id( var31.plan().id() ) ).toString() );
                    } );
                    Function1 rewriter = this.$outer.org$neo4j$cypher$internal$physicalplanning$SlottedRewriter$$rewriteCreator( innerSlotConf, this.thisPlan$1,
                            this.slotConfigurations$2 );
                    Expression rewrittenProjection = (Expression) org.neo4j.cypher.internal.v4_0.util.Rewritable.RewritableAny..
                    MODULE$.endoRewrite$extension( org.neo4j.cypher.internal.v4_0.util.Rewritable..MODULE$.RewritableAny( var31.projection() ), rewriter);
                    var3 = var31.copy( rewrittenPlan, rewrittenProjection, var31.position() );
                }
                else
                {
                    NodeType var10000;
                    RelationshipType var316;
                    if ( x3 instanceof Property )
                    {
                        Property var36 = (Property) x3;
                        Expression var37 = var36.map();
                        PropertyKeyName var38 = var36.propertyKey();
                        if ( var37 instanceof Variable )
                        {
                            Variable var39 = (Variable) var37;
                            String keyxxx = var39.name();
                            if ( var38 != null )
                            {
                                String propKeyx = var38.name();
                                Slot var42 = this.slotConfiguration$1.apply( keyxxx );
                                Object var24;
                                if ( !(var42 instanceof LongSlot) )
                                {
                                    if ( !(var42 instanceof RefSlot) )
                                    {
                                        throw new MatchError( var42 );
                                    }

                                    RefSlot var66 = (RefSlot) var42;
                                    int offsetxxxxxxxxxx = var66.offset();
                                    var24 = var36.copy( new ReferenceFromSlot( offsetxxxxxxxxxx, keyxxx ), var36.copy$default$2(), var36.position() );
                                }
                                else
                                {
                                    Object var25;
                                    int offsetxxxxx;
                                    boolean nullable;
                                    label984:
                                    {
                                        LongSlot var43 = (LongSlot) var42;
                                        offsetxxxxx = var43.offset();
                                        nullable = var43.nullable();
                                        CypherType typ = var43.typ();
                                        Option maybeToken =
                                                this.$outer.org$neo4j$cypher$internal$physicalplanning$SlottedRewriter$$tokenContext.getOptPropertyKeyId(
                                                        propKeyx );
                                        Tuple2 var49 = new Tuple2( typ, maybeToken );
                                        if ( var49 != null )
                                        {
                                            label902:
                                            {
                                                CypherType var50 = (CypherType) var49._1();
                                                Option var51 = (Option) var49._2();
                                                var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                                                if ( var10000 == null )
                                                {
                                                    if ( var50 != null )
                                                    {
                                                        break label902;
                                                    }
                                                }
                                                else if ( !var10000.equals( var50 ) )
                                                {
                                                    break label902;
                                                }

                                                if ( var51 instanceof Some )
                                                {
                                                    Some var53 = (Some) var51;
                                                    int token = BoxesRunTime.unboxToInt( var53.value() );
                                                    var25 = new NodeProperty( offsetxxxxx, token,
                                                            (new StringBuilder( 1 )).append( keyxxx ).append( "." ).append( propKeyx ).toString(), var36 );
                                                    break label984;
                                                }
                                            }
                                        }

                                        if ( var49 != null )
                                        {
                                            label895:
                                            {
                                                CypherType var55 = (CypherType) var49._1();
                                                Option var56 = (Option) var49._2();
                                                var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                                                if ( var10000 == null )
                                                {
                                                    if ( var55 != null )
                                                    {
                                                        break label895;
                                                    }
                                                }
                                                else if ( !var10000.equals( var55 ) )
                                                {
                                                    break label895;
                                                }

                                                if ( scala.None..MODULE$.equals( var56 )){
                                                var25 = new NodePropertyLate( offsetxxxxx, propKeyx,
                                                        (new StringBuilder( 1 )).append( keyxxx ).append( "." ).append( propKeyx ).toString(), var36 );
                                                break label984;
                                            }
                                            }
                                        }

                                        if ( var49 != null )
                                        {
                                            label888:
                                            {
                                                CypherType var58 = (CypherType) var49._1();
                                                Option var59 = (Option) var49._2();
                                                var316 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                                                if ( var316 == null )
                                                {
                                                    if ( var58 != null )
                                                    {
                                                        break label888;
                                                    }
                                                }
                                                else if ( !var316.equals( var58 ) )
                                                {
                                                    break label888;
                                                }

                                                if ( var59 instanceof Some )
                                                {
                                                    Some var61 = (Some) var59;
                                                    int tokenx = BoxesRunTime.unboxToInt( var61.value() );
                                                    var25 = new RelationshipProperty( offsetxxxxx, tokenx,
                                                            (new StringBuilder( 1 )).append( keyxxx ).append( "." ).append( propKeyx ).toString(), var36 );
                                                    break label984;
                                                }
                                            }
                                        }

                                        if ( var49 != null )
                                        {
                                            CypherType var63 = (CypherType) var49._1();
                                            Option var64 = (Option) var49._2();
                                            var316 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                                            if ( var316 == null )
                                            {
                                                if ( var63 != null )
                                                {
                                                    throw new InternalException(
                                                            "Expressions on object other then nodes and relationships are not yet supported" );
                                                }
                                            }
                                            else if ( !var316.equals( var63 ) )
                                            {
                                                throw new InternalException( "Expressions on object other then nodes and relationships are not yet supported" );
                                            }

                                            if ( scala.None..MODULE$.equals( var64 )){
                                            var25 = new RelationshipPropertyLate( offsetxxxxx, propKeyx,
                                                    (new StringBuilder( 1 )).append( keyxxx ).append( "." ).append( propKeyx ).toString(), var36 );
                                            break label984;
                                        }
                                        }

                                        throw new InternalException( "Expressions on object other then nodes and relationships are not yet supported" );
                                    }

                                    var24 = nullable ? new NullCheckProperty( offsetxxxxx, (LogicalProperty) var25 ) : var25;
                                }

                                var3 = var24;
                                return var3;
                            }
                        }
                    }

                    if ( x3 instanceof CachedProperty )
                    {
                        CachedProperty var68 = (CachedProperty) x3;
                        String originalEntityName = var68.originalEntityName();
                        LogicalVariable variable = var68.entityVariable();
                        PropertyKeyName pkn = var68.propertyKey();
                        EntityType entityType = var68.entityType();
                        if ( pkn != null )
                        {
                            Object var21;
                            label918:
                            {
                                String propKeyxx;
                                int offsetxxxxxxxxxxxxx;
                                boolean nullablex;
                                label985:
                                {
                                    propKeyxx = pkn.name();
                                    boolean var74 = false;
                                    LongSlot var75 = null;
                                    Slot var76 = this.slotConfiguration$1.apply( variable.name() );
                                    if ( var76 instanceof LongSlot )
                                    {
                                        label956:
                                        {
                                            CypherType cypherType;
                                            label855:
                                            {
                                                var74 = true;
                                                var75 = (LongSlot) var76;
                                                offsetxxxxxxxxxxxxx = var75.offset();
                                                nullablex = var75.nullable();
                                                cypherType = var75.typ();
                                                NodeType var80 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                                                if ( cypherType == null )
                                                {
                                                    if ( var80 != null )
                                                    {
                                                        break label855;
                                                    }
                                                }
                                                else if ( !cypherType.equals( var80 ) )
                                                {
                                                    break label855;
                                                }

                                                org.neo4j.cypher.internal.v4_0.expressions.NODE_TYPE.var81 =
                                                        org.neo4j.cypher.internal.v4_0.expressions.NODE_TYPE..MODULE$;
                                                if ( entityType == null )
                                                {
                                                    if ( var81 == null )
                                                    {
                                                        break label985;
                                                    }
                                                }
                                                else if ( entityType.equals( var81 ) )
                                                {
                                                    break label985;
                                                }
                                            }

                                            RelationshipType var82 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                                            if ( cypherType == null )
                                            {
                                                if ( var82 != null )
                                                {
                                                    break label956;
                                                }
                                            }
                                            else if ( !cypherType.equals( var82 ) )
                                            {
                                                break label956;
                                            }

                                            org.neo4j.cypher.internal.v4_0.expressions.RELATIONSHIP_TYPE.var83 =
                                                    org.neo4j.cypher.internal.v4_0.expressions.RELATIONSHIP_TYPE..MODULE$;
                                            if ( entityType == null )
                                            {
                                                if ( var83 == null )
                                                {
                                                    break label985;
                                                }
                                            }
                                            else if ( entityType.equals( var83 ) )
                                            {
                                                break label985;
                                            }
                                        }
                                    }

                                    if ( var74 )
                                    {
                                        throw new InternalException( (new StringBuilder( 47 )).append( "Unexpected type on slot '" ).append( var75 ).append(
                                                "' for cached property " ).append( var68 ).toString() );
                                    }

                                    if ( !(var76 instanceof RefSlot) )
                                    {
                                        throw new MatchError( var76 );
                                    }

                                    RefSlot var88 = (RefSlot) var76;
                                    int offsetxxxxxxxxxxxxxxx = var88.offset();
                                    boolean nullablexx = var88.nullable();
                                    Option var92 = this.$outer.org$neo4j$cypher$internal$physicalplanning$SlottedRewriter$$tokenContext.getOptPropertyKeyId(
                                            propKeyxx );
                                    Object var22;
                                    if ( var92 instanceof Some )
                                    {
                                        Some var93 = (Some) var92;
                                        int propIdx = BoxesRunTime.unboxToInt( var93.value() );
                                        var22 = new SlottedCachedPropertyWithPropertyToken( originalEntityName, pkn, offsetxxxxxxxxxxxxxxx, false, propIdx,
                                                this.slotConfiguration$1.getCachedPropertyOffsetFor( var68 ), entityType, nullablexx );
                                    }
                                    else
                                    {
                                        if ( !scala.None..MODULE$.equals( var92 )){
                                        throw new MatchError( var92 );
                                    }

                                        var22 = new SlottedCachedPropertyWithoutPropertyToken( originalEntityName, pkn, offsetxxxxxxxxxxxxxxx, false, propKeyxx,
                                                this.slotConfiguration$1.getCachedPropertyOffsetFor( var68 ), entityType, nullablexx );
                                    }

                                    var21 = nullablexx ? new NullCheckReferenceProperty( offsetxxxxxxxxxxxxxxx, (LogicalProperty) var22 ) : var22;
                                    break label918;
                                }

                                Option var85 =
                                        this.$outer.org$neo4j$cypher$internal$physicalplanning$SlottedRewriter$$tokenContext.getOptPropertyKeyId( propKeyxx );
                                Object var23;
                                if ( var85 instanceof Some )
                                {
                                    Some var86 = (Some) var85;
                                    int propId = BoxesRunTime.unboxToInt( var86.value() );
                                    var23 = new SlottedCachedPropertyWithPropertyToken( originalEntityName, pkn, offsetxxxxxxxxxxxxx, true, propId,
                                            this.slotConfiguration$1.getCachedPropertyOffsetFor( var68 ), entityType, nullablex );
                                }
                                else
                                {
                                    if ( !scala.None..MODULE$.equals( var85 )){
                                    throw new MatchError( var85 );
                                }

                                    var23 = new SlottedCachedPropertyWithoutPropertyToken( originalEntityName, pkn, offsetxxxxxxxxxxxxx, true, propKeyxx,
                                            this.slotConfiguration$1.getCachedPropertyOffsetFor( var68 ), entityType, nullablex );
                                }

                                var21 = var23;
                            }

                            var3 = var21;
                            return var3;
                        }
                    }

                    if ( x3 instanceof Equals )
                    {
                        Equals var95 = (Equals) x3;
                        Expression var96 = var95.lhs();
                        Expression var97 = var95.rhs();
                        if ( var96 instanceof Variable )
                        {
                            Variable var98 = (Variable) var96;
                            String k1 = var98.name();
                            if ( var97 instanceof Variable )
                            {
                                Variable var100 = (Variable) var97;
                                String k2 = var100.name();
                                var3 = this.$outer.org$neo4j$cypher$internal$physicalplanning$SlottedRewriter$$primitiveEqualityChecks(
                                        this.slotConfiguration$1, var95, k1, k2, true );
                                return var3;
                            }
                        }
                    }

                    if ( x3 instanceof Not )
                    {
                        Not var102 = (Not) x3;
                        Expression e = var102.rhs();
                        if ( e instanceof Equals )
                        {
                            Equals var104 = (Equals) e;
                            Expression var105 = var104.lhs();
                            Expression var106 = var104.rhs();
                            if ( var105 instanceof Variable )
                            {
                                Variable var107 = (Variable) var105;
                                String k1x = var107.name();
                                if ( var106 instanceof Variable )
                                {
                                    Variable var109 = (Variable) var106;
                                    String k2x = var109.name();
                                    var3 = this.$outer.org$neo4j$cypher$internal$physicalplanning$SlottedRewriter$$primitiveEqualityChecks(
                                            this.slotConfiguration$1, var104, k1x, k2x, false );
                                    return var3;
                                }
                            }
                        }
                    }

                    if ( x3 instanceof IsNull )
                    {
                        var26 = true;
                        var27 = (IsNull) x3;
                        Expression var111 = var27.lhs();
                        if ( var111 instanceof Variable )
                        {
                            Object var20;
                            label921:
                            {
                                Variable var112 = (Variable) var111;
                                String keyxxxxx = var112.name();
                                Slot slotxxxx = this.slotConfiguration$1.apply( keyxxxxx );
                                boolean var115 = false;
                                LongSlot var116 = null;
                                if ( slotxxxx instanceof LongSlot )
                                {
                                    var115 = true;
                                    var116 = (LongSlot) slotxxxx;
                                    int offsetxxxxxxxxxxxxxxxxxx = var116.offset();
                                    boolean var119 = var116.nullable();
                                    if ( var119 )
                                    {
                                        var20 = new IsPrimitiveNull( offsetxxxxxxxxxxxxxxxxxx );
                                        break label921;
                                    }
                                }

                                if ( var115 )
                                {
                                    boolean var120 = var116.nullable();
                                    if ( !var120 )
                                    {
                                        var20 = new False( var27.position() );
                                        break label921;
                                    }
                                }

                                var20 = var27;
                            }

                            var3 = var20;
                            return var3;
                        }
                    }

                    if ( x3 instanceof GetDegree )
                    {
                        GetDegree var121 = (GetDegree) x3;
                        Expression var122 = var121.node();
                        Option typx = var121.relType();
                        SemanticDirection direction = var121.dir();
                        if ( var122 instanceof Variable )
                        {
                            Object var19;
                            label807:
                            {
                                Option maybeTokenx;
                                int offsetxxxxxxxxxxxxxxxxxxx;
                                label922:
                                {
                                    Variable var125 = (Variable) var122;
                                    String n = var125.name();
                                    maybeTokenx = typx.map( ( r ) -> {
                                        return r.name();
                                    } );
                                    boolean var128 = false;
                                    LongSlot var129 = null;
                                    Slot var130 = this.slotConfiguration$1.apply( n );
                                    if ( var130 instanceof LongSlot )
                                    {
                                        var128 = true;
                                        var129 = (LongSlot) var130;
                                        offsetxxxxxxxxxxxxxxxxxxx = var129.offset();
                                        boolean var132 = var129.nullable();
                                        CypherType var133 = var129.typ();
                                        if ( !var132 )
                                        {
                                            var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                                            if ( var10000 == null )
                                            {
                                                if ( var133 == null )
                                                {
                                                    break label922;
                                                }
                                            }
                                            else if ( var10000.equals( var133 ) )
                                            {
                                                break label922;
                                            }
                                        }
                                    }

                                    int offsetxxxxxxxxxxxxxxxxxxxx;
                                    label798:
                                    {
                                        if ( var128 )
                                        {
                                            offsetxxxxxxxxxxxxxxxxxxxx = var129.offset();
                                            boolean var136 = var129.nullable();
                                            CypherType var137 = var129.typ();
                                            if ( var136 )
                                            {
                                                var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                                                if ( var10000 == null )
                                                {
                                                    if ( var137 == null )
                                                    {
                                                        break label798;
                                                    }
                                                }
                                                else if ( var10000.equals( var137 ) )
                                                {
                                                    break label798;
                                                }
                                            }
                                        }

                                        var19 = var121;
                                        break label807;
                                    }

                                    var19 = new NullCheck( offsetxxxxxxxxxxxxxxxxxxxx,
                                            new GetDegreePrimitive( offsetxxxxxxxxxxxxxxxxxxxx, maybeTokenx, direction ) );
                                    break label807;
                                }

                                var19 = new GetDegreePrimitive( offsetxxxxxxxxxxxxxxxxxxx, maybeTokenx, direction );
                            }

                            var3 = var19;
                            return var3;
                        }
                    }

                    if ( x3 instanceof Variable )
                    {
                        Variable var139 = (Variable) x3;
                        String kx = var139.name();
                        Option var141 = this.slotConfiguration$1.get( kx );
                        if ( !(var141 instanceof Some) )
                        {
                            throw new CantCompileQueryException(
                                    (new StringBuilder( 41 )).append( "Did not find `" ).append( kx ).append( "` in the slot configuration" ).toString() );
                        }

                        Object var18;
                        label986:
                        {
                            Some var142 = (Some) var141;
                            Slot slotxxxxxxx = (Slot) var142.value();
                            boolean var144 = false;
                            LongSlot var145 = null;
                            if ( slotxxxxxxx instanceof LongSlot )
                            {
                                var144 = true;
                                var145 = (LongSlot) slotxxxxxxx;
                                int offsetxxxxxxxxxxxxxxxxxxxxx = var145.offset();
                                boolean var148 = var145.nullable();
                                CypherType var149 = var145.typ();
                                if ( !var148 )
                                {
                                    label958:
                                    {
                                        var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                                        if ( var10000 == null )
                                        {
                                            if ( var149 != null )
                                            {
                                                break label958;
                                            }
                                        }
                                        else if ( !var10000.equals( var149 ) )
                                        {
                                            break label958;
                                        }

                                        var18 = new NodeFromSlot( offsetxxxxxxxxxxxxxxxxxxxxx, kx );
                                        break label986;
                                    }
                                }
                            }

                            int offsetxxxxxxxxxxxxxxxxxxxxxx;
                            label987:
                            {
                                if ( var144 )
                                {
                                    offsetxxxxxxxxxxxxxxxxxxxxxx = var145.offset();
                                    boolean var152 = var145.nullable();
                                    CypherType var153 = var145.typ();
                                    if ( var152 )
                                    {
                                        var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                                        if ( var10000 == null )
                                        {
                                            if ( var153 == null )
                                            {
                                                break label987;
                                            }
                                        }
                                        else if ( var10000.equals( var153 ) )
                                        {
                                            break label987;
                                        }
                                    }
                                }

                                if ( var144 )
                                {
                                    int offsetxxxxxxxxxxxxxxxxxxxxxxx = var145.offset();
                                    boolean var156 = var145.nullable();
                                    CypherType var157 = var145.typ();
                                    if ( !var156 )
                                    {
                                        label960:
                                        {
                                            var316 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                                            if ( var316 == null )
                                            {
                                                if ( var157 != null )
                                                {
                                                    break label960;
                                                }
                                            }
                                            else if ( !var316.equals( var157 ) )
                                            {
                                                break label960;
                                            }

                                            var18 = new RelationshipFromSlot( offsetxxxxxxxxxxxxxxxxxxxxxxx, kx );
                                            break label986;
                                        }
                                    }
                                }

                                if ( var144 )
                                {
                                    int offsetxxxxxxxxxxxxxxxxxxxxxxxx = var145.offset();
                                    boolean var160 = var145.nullable();
                                    CypherType var161 = var145.typ();
                                    if ( var160 )
                                    {
                                        label962:
                                        {
                                            var316 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                                            if ( var316 == null )
                                            {
                                                if ( var161 != null )
                                                {
                                                    break label962;
                                                }
                                            }
                                            else if ( !var316.equals( var161 ) )
                                            {
                                                break label962;
                                            }

                                            var18 = new NullCheckVariable( offsetxxxxxxxxxxxxxxxxxxxxxxxx,
                                                    new RelationshipFromSlot( offsetxxxxxxxxxxxxxxxxxxxxxxxx, kx ) );
                                            break label986;
                                        }
                                    }
                                }

                                if ( !(slotxxxxxxx instanceof RefSlot) )
                                {
                                    throw new CantCompileQueryException( (new StringBuilder( 45 )).append( "Unknown type for `" ).append( kx ).append(
                                            "` in the slot configuration" ).toString() );
                                }

                                RefSlot var163 = (RefSlot) slotxxxxxxx;
                                int offsetxxxxxxxxxxxxxxxxxxxxxxxxx = var163.offset();
                                var18 = new ReferenceFromSlot( offsetxxxxxxxxxxxxxxxxxxxxxxxxx, kx );
                                break label986;
                            }

                            var18 = new NullCheckVariable( offsetxxxxxxxxxxxxxxxxxxxxxx, new NodeFromSlot( offsetxxxxxxxxxxxxxxxxxxxxxx, kx ) );
                        }

                        var3 = var18;
                    }
                    else
                    {
                        Function var317;
                        if ( x3 instanceof FunctionInvocation )
                        {
                            label1059:
                            {
                                var28 = true;
                                var29.elem = (FunctionInvocation) x3;
                                var317 = ((FunctionInvocation) var29.elem).function();
                                org.neo4j.cypher.internal.v4_0.expressions.functions.Id.var165 = org.neo4j.cypher.internal.v4_0.expressions.functions.Id..
                                MODULE$;
                                if ( var317 == null )
                                {
                                    if ( var165 != null )
                                    {
                                        break label1059;
                                    }
                                }
                                else if ( !var317.equals( var165 ) )
                                {
                                    break label1059;
                                }

                                Expression var166 = (Expression) ((FunctionInvocation) var29.elem).args().head();
                                Object var15;
                                if ( !(var166 instanceof Variable) )
                                {
                                    var15 = (FunctionInvocation) var29.elem;
                                }
                                else
                                {
                                    Object var16;
                                    label932:
                                    {
                                        Variable var167 = (Variable) var166;
                                        String keyxxxxxxxx = var167.name();
                                        Slot slotxxxxxxxx = this.slotConfiguration$1.apply( keyxxxxxxxx );
                                        boolean var170 = false;
                                        LongSlot var171 = null;
                                        if ( slotxxxxxxxx instanceof LongSlot )
                                        {
                                            var170 = true;
                                            var171 = (LongSlot) slotxxxxxxxx;
                                            int offset = var171.offset();
                                            boolean var174 = var171.nullable();
                                            if ( var174 )
                                            {
                                                var16 = new NullCheck( offset, new IdFromSlot( offset ) );
                                                break label932;
                                            }
                                        }

                                        if ( var170 )
                                        {
                                            int offsetx = var171.offset();
                                            boolean var176 = var171.nullable();
                                            if ( !var176 )
                                            {
                                                var16 = new IdFromSlot( offsetx );
                                                break label932;
                                            }
                                        }

                                        var16 = (FunctionInvocation) var29.elem;
                                    }

                                    var15 = var16;
                                }

                                var3 = var15;
                                return var3;
                            }
                        }

                        label1060:
                        {
                            if ( var28 )
                            {
                                var317 = ((FunctionInvocation) var29.elem).function();
                                org.neo4j.cypher.internal.v4_0.expressions.functions.Exists.var177 =
                                        org.neo4j.cypher.internal.v4_0.expressions.functions.Exists..MODULE$;
                                if ( var317 == null )
                                {
                                    if ( var177 == null )
                                    {
                                        break label1060;
                                    }
                                }
                                else if ( var317.equals( var177 ) )
                                {
                                    break label1060;
                                }
                            }

                            label1061:
                            {
                                if ( var28 )
                                {
                                    var317 = ((FunctionInvocation) var29.elem).function();
                                    org.neo4j.cypher.internal.v4_0.expressions.functions.Labels.var187 =
                                            org.neo4j.cypher.internal.v4_0.expressions.functions.Labels..MODULE$;
                                    if ( var317 == null )
                                    {
                                        if ( var187 == null )
                                        {
                                            break label1061;
                                        }
                                    }
                                    else if ( var317.equals( var187 ) )
                                    {
                                        break label1061;
                                    }
                                }

                                label1062:
                                {
                                    if ( var28 )
                                    {
                                        var317 = ((FunctionInvocation) var29.elem).function();
                                        org.neo4j.cypher.internal.v4_0.expressions.functions.Type.var203 =
                                                org.neo4j.cypher.internal.v4_0.expressions.functions.Type..MODULE$;
                                        if ( var317 == null )
                                        {
                                            if ( var203 == null )
                                            {
                                                break label1062;
                                            }
                                        }
                                        else if ( var317.equals( var203 ) )
                                        {
                                            break label1062;
                                        }
                                    }

                                    Object var318;
                                    label1063:
                                    {
                                        if ( var28 )
                                        {
                                            var317 = ((FunctionInvocation) var29.elem).function();
                                            org.neo4j.cypher.internal.v4_0.expressions.functions.Count.var219 =
                                                    org.neo4j.cypher.internal.v4_0.expressions.functions.Count..MODULE$;
                                            if ( var317 == null )
                                            {
                                                if ( var219 == null )
                                                {
                                                    break label1063;
                                                }
                                            }
                                            else if ( var317.equals( var219 ) )
                                            {
                                                break label1063;
                                            }
                                        }

                                        if ( x3 instanceof HasLabels )
                                        {
                                            HasLabels var266 = (HasLabels) x3;
                                            Expression var267 = var266.expression();
                                            Seq labels = var266.labels();
                                            if ( var267 instanceof Variable )
                                            {
                                                Object var4;
                                                label937:
                                                {
                                                    int offsetxxxxxxxxxxxxxxxx;
                                                    label993:
                                                    {
                                                        Variable var269 = (Variable) var267;
                                                        String k = var269.name();
                                                        boolean var271 = false;
                                                        LongSlot var272 = null;
                                                        Slot var273 = this.slotConfiguration$1.apply( k );
                                                        if ( var273 instanceof LongSlot )
                                                        {
                                                            var271 = true;
                                                            var272 = (LongSlot) var273;
                                                            offsetxxxxxxxxxxxxxxxx = var272.offset();
                                                            boolean var275 = var272.nullable();
                                                            CypherType var276 = var272.typ();
                                                            if ( !var275 )
                                                            {
                                                                var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                                                                if ( var10000 == null )
                                                                {
                                                                    if ( var276 == null )
                                                                    {
                                                                        break label993;
                                                                    }
                                                                }
                                                                else if ( var10000.equals( var276 ) )
                                                                {
                                                                    break label993;
                                                                }
                                                            }
                                                        }

                                                        if ( var271 )
                                                        {
                                                            int offsetxxxxxxxxxxxxxxxxx = var272.offset();
                                                            boolean var285 = var272.nullable();
                                                            CypherType var286 = var272.typ();
                                                            if ( var285 )
                                                            {
                                                                label966:
                                                                {
                                                                    var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                                                                    if ( var10000 == null )
                                                                    {
                                                                        if ( var286 != null )
                                                                        {
                                                                            break label966;
                                                                        }
                                                                    }
                                                                    else if ( !var10000.equals( var286 ) )
                                                                    {
                                                                        break label966;
                                                                    }

                                                                    Tuple2 var289 = this.resolveLabelTokens$1( labels );
                                                                    if ( var289 == null )
                                                                    {
                                                                        throw new MatchError( var289 );
                                                                    }

                                                                    Seq resolvedLabelTokensxx = (Seq) var289._1();
                                                                    Seq lateLabelsxx = (Seq) var289._2();
                                                                    Tuple2 var5 = new Tuple2( resolvedLabelTokensxx, lateLabelsxx );
                                                                    Seq resolvedLabelTokensxxx = (Seq) var5._1();
                                                                    Seq lateLabelsxxx = (Seq) var5._2();
                                                                    var4 = new NullCheck( offsetxxxxxxxxxxxxxxxxx,
                                                                            new HasLabelsFromSlot( offsetxxxxxxxxxxxxxxxxx, resolvedLabelTokensxxx,
                                                                                    lateLabelsxxx ) );
                                                                    break label937;
                                                                }
                                                            }
                                                        }

                                                        var4 = var266;
                                                        break label937;
                                                    }

                                                    Tuple2 var279 = this.resolveLabelTokens$1( labels );
                                                    if ( var279 == null )
                                                    {
                                                        throw new MatchError( var279 );
                                                    }

                                                    Seq resolvedLabelTokens = (Seq) var279._1();
                                                    Seq lateLabels = (Seq) var279._2();
                                                    Tuple2 var6 = new Tuple2( resolvedLabelTokens, lateLabels );
                                                    Seq resolvedLabelTokensx = (Seq) var6._1();
                                                    Seq lateLabelsx = (Seq) var6._2();
                                                    var4 = new HasLabelsFromSlot( offsetxxxxxxxxxxxxxxxx, resolvedLabelTokensx, lateLabelsx );
                                                }

                                                var3 = var4;
                                                return var3;
                                            }
                                        }

                                        if ( var26 )
                                        {
                                            Expression prop = var27.lhs();
                                            if ( prop instanceof Property )
                                            {
                                                Property var295 = (Property) prop;
                                                Expression var296 = var295.map();
                                                PropertyKeyName var297 = var295.propertyKey();
                                                if ( var296 instanceof Variable )
                                                {
                                                    Variable var298 = (Variable) var296;
                                                    String keyxxxxxx = var298.name();
                                                    if ( var297 != null )
                                                    {
                                                        String propKeyxxx = var297.name();
                                                        Slot slotxxxxx = this.slotConfiguration$1.apply( keyxxxxxx );
                                                        Option maybeSpecializedExpressionx =
                                                                this.$outer.org$neo4j$cypher$internal$physicalplanning$SlottedRewriter$$specializeCheckIfPropertyExists(
                                                                        this.slotConfiguration$1, keyxxxxxx, propKeyxxx, var295, slotxxxxx );
                                                        if ( maybeSpecializedExpressionx.isDefined() )
                                                        {
                                                            RuntimeProperty propertyExists = (RuntimeProperty) maybeSpecializedExpressionx.get();
                                                            Not notPropertyExists = new Not( propertyExists, var27.position() );
                                                            var318 =
                                                                    slotxxxxx.nullable() ? new Or( new IsPrimitiveNull( slotxxxxx.offset() ), notPropertyExists,
                                                                            var27.position() ) : notPropertyExists;
                                                        }
                                                        else
                                                        {
                                                            var318 = var27;
                                                        }

                                                        var3 = var318;
                                                        return var3;
                                                    }
                                                }
                                            }
                                        }

                                        if ( x3 instanceof IsNotNull )
                                        {
                                            IsNotNull var305 = (IsNotNull) x3;
                                            Expression propx = var305.lhs();
                                            if ( propx instanceof Property )
                                            {
                                                Property var307 = (Property) propx;
                                                Expression var308 = var307.map();
                                                PropertyKeyName var309 = var307.propertyKey();
                                                if ( var308 instanceof Variable )
                                                {
                                                    Variable var310 = (Variable) var308;
                                                    String keyxxxxxxx = var310.name();
                                                    if ( var309 != null )
                                                    {
                                                        String propKeyxxxx = var309.name();
                                                        Slot slotxxxxxx = this.slotConfiguration$1.apply( keyxxxxxxx );
                                                        Option maybeSpecializedExpressionxx =
                                                                this.$outer.org$neo4j$cypher$internal$physicalplanning$SlottedRewriter$$specializeCheckIfPropertyExists(
                                                                        this.slotConfiguration$1, keyxxxxxxx, propKeyxxxx, var307, slotxxxxxx );
                                                        if ( maybeSpecializedExpressionxx.isDefined() )
                                                        {
                                                            RuntimeProperty propertyExistsx = (RuntimeProperty) maybeSpecializedExpressionxx.get();
                                                            var318 = slotxxxxxx.nullable() ? new And(
                                                                    new Not( new IsPrimitiveNull( slotxxxxxx.offset() ), var305.position() ), propertyExistsx,
                                                                    var305.position() ) : propertyExistsx;
                                                        }
                                                        else
                                                        {
                                                            var318 = var305;
                                                        }

                                                        var3 = var318;
                                                        return var3;
                                                    }
                                                }
                                            }
                                        }

                                        var3 = var2.apply( x3 );
                                        return var3;
                                    }

                                    Expression var220 = (Expression) ((FunctionInvocation) var29.elem).args().head();
                                    FunctionInvocation var7;
                                    if ( !(var220 instanceof Variable) )
                                    {
                                        var7 = (FunctionInvocation) var29.elem;
                                    }
                                    else
                                    {
                                        Variable var221 = (Variable) var220;
                                        String keyxxxx = var221.name();
                                        Slot slotxxx = this.slotConfiguration$1.apply( keyxxxx );
                                        if ( ((FunctionInvocation) var29.elem).distinct() )
                                        {
                                            Object var9;
                                            label623:
                                            {
                                                int offsetxxxxxxx;
                                                label994:
                                                {
                                                    boolean var225 = false;
                                                    LongSlot var226 = null;
                                                    if ( slotxxx instanceof LongSlot )
                                                    {
                                                        var225 = true;
                                                        var226 = (LongSlot) slotxxx;
                                                        offsetxxxxxxx = var226.offset();
                                                        boolean var229 = var226.nullable();
                                                        CypherType var230 = var226.typ();
                                                        if ( var229 )
                                                        {
                                                            var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                                                            if ( var10000 == null )
                                                            {
                                                                if ( var230 == null )
                                                                {
                                                                    break label994;
                                                                }
                                                            }
                                                            else if ( var10000.equals( var230 ) )
                                                            {
                                                                break label994;
                                                            }
                                                        }
                                                    }

                                                    if ( var225 )
                                                    {
                                                        int offsetxxxxxxxx = var226.offset();
                                                        boolean var233 = var226.nullable();
                                                        CypherType var234 = var226.typ();
                                                        if ( !var233 )
                                                        {
                                                            label969:
                                                            {
                                                                var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                                                                if ( var10000 == null )
                                                                {
                                                                    if ( var234 != null )
                                                                    {
                                                                        break label969;
                                                                    }
                                                                }
                                                                else if ( !var10000.equals( var234 ) )
                                                                {
                                                                    break label969;
                                                                }

                                                                var9 = new Some( new IdFromSlot( offsetxxxxxxxx ) );
                                                                break label623;
                                                            }
                                                        }
                                                    }

                                                    if ( var225 )
                                                    {
                                                        int offsetxxxxxxxxx = var226.offset();
                                                        boolean var237 = var226.nullable();
                                                        CypherType var238 = var226.typ();
                                                        if ( var237 )
                                                        {
                                                            label971:
                                                            {
                                                                var316 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                                                                if ( var316 == null )
                                                                {
                                                                    if ( var238 != null )
                                                                    {
                                                                        break label971;
                                                                    }
                                                                }
                                                                else if ( !var316.equals( var238 ) )
                                                                {
                                                                    break label971;
                                                                }

                                                                var9 = new Some( new NullCheck( offsetxxxxxxxxx, new IdFromSlot( offsetxxxxxxxxx ) ) );
                                                                break label623;
                                                            }
                                                        }
                                                    }

                                                    int offsetxxxxxxxxxxx;
                                                    label592:
                                                    {
                                                        if ( var225 )
                                                        {
                                                            offsetxxxxxxxxxxx = var226.offset();
                                                            boolean var241 = var226.nullable();
                                                            CypherType var242 = var226.typ();
                                                            if ( !var241 )
                                                            {
                                                                var316 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                                                                if ( var316 == null )
                                                                {
                                                                    if ( var242 == null )
                                                                    {
                                                                        break label592;
                                                                    }
                                                                }
                                                                else if ( var316.equals( var242 ) )
                                                                {
                                                                    break label592;
                                                                }
                                                            }
                                                        }

                                                        var9 = scala.None..MODULE$;
                                                        break label623;
                                                    }

                                                    var9 = new Some( new IdFromSlot( offsetxxxxxxxxxxx ) );
                                                    break label623;
                                                }

                                                var9 = new Some( new NullCheck( offsetxxxxxxx, new IdFromSlot( offsetxxxxxxx ) ) );
                                            }

                                            var318 = var9;
                                        }
                                        else
                                        {
                                            Object var8;
                                            label582:
                                            {
                                                int offsetxxxxxxxxxxxx;
                                                label995:
                                                {
                                                    boolean var244 = false;
                                                    LongSlot var245 = null;
                                                    if ( slotxxx instanceof LongSlot )
                                                    {
                                                        var244 = true;
                                                        var245 = (LongSlot) slotxxx;
                                                        offsetxxxxxxxxxxxx = var245.offset();
                                                        boolean var248 = var245.nullable();
                                                        CypherType var249 = var245.typ();
                                                        if ( var248 )
                                                        {
                                                            var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                                                            if ( var10000 == null )
                                                            {
                                                                if ( var249 == null )
                                                                {
                                                                    break label995;
                                                                }
                                                            }
                                                            else if ( var10000.equals( var249 ) )
                                                            {
                                                                break label995;
                                                            }
                                                        }
                                                    }

                                                    if ( var244 )
                                                    {
                                                        boolean var251 = var245.nullable();
                                                        CypherType var252 = var245.typ();
                                                        if ( !var251 )
                                                        {
                                                            label973:
                                                            {
                                                                var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                                                                if ( var10000 == null )
                                                                {
                                                                    if ( var252 != null )
                                                                    {
                                                                        break label973;
                                                                    }
                                                                }
                                                                else if ( !var10000.equals( var252 ) )
                                                                {
                                                                    break label973;
                                                                }

                                                                var8 = new Some( new True( var221.position() ) );
                                                                break label582;
                                                            }
                                                        }
                                                    }

                                                    if ( var244 )
                                                    {
                                                        int offsetxxxxxxxxxxxxxx = var245.offset();
                                                        boolean var255 = var245.nullable();
                                                        CypherType var256 = var245.typ();
                                                        if ( var255 )
                                                        {
                                                            label975:
                                                            {
                                                                var316 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                                                                if ( var316 == null )
                                                                {
                                                                    if ( var256 != null )
                                                                    {
                                                                        break label975;
                                                                    }
                                                                }
                                                                else if ( !var316.equals( var256 ) )
                                                                {
                                                                    break label975;
                                                                }

                                                                var8 = new Some( new NullCheck( offsetxxxxxxxxxxxxxx, new True( var221.position() ) ) );
                                                                break label582;
                                                            }
                                                        }
                                                    }

                                                    label551:
                                                    {
                                                        if ( var244 )
                                                        {
                                                            boolean var258 = var245.nullable();
                                                            CypherType var259 = var245.typ();
                                                            if ( !var258 )
                                                            {
                                                                var316 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                                                                if ( var316 == null )
                                                                {
                                                                    if ( var259 == null )
                                                                    {
                                                                        break label551;
                                                                    }
                                                                }
                                                                else if ( var316.equals( var259 ) )
                                                                {
                                                                    break label551;
                                                                }
                                                            }
                                                        }

                                                        var8 = scala.None..MODULE$;
                                                        break label582;
                                                    }

                                                    var8 = new Some( new True( var221.position() ) );
                                                    break label582;
                                                }

                                                var8 = new Some( new NullCheck( offsetxxxxxxxxxxxx, new True( var221.position() ) ) );
                                            }

                                            var318 = var8;
                                        }

                                        Option maybeNewInnerExpression = var318;
                                        FunctionInvocation var319;
                                        if ( ((Option) maybeNewInnerExpression).isDefined() )
                                        {
                                            IndexedSeq x$26 = (IndexedSeq) scala.package..MODULE$.IndexedSeq().apply( scala.Predef..MODULE$.wrapRefArray(
                                                (Object[]) (new Expression[]{(Expression) ((Option) maybeNewInnerExpression).get()}) ));
                                            Namespace x$27 = ((FunctionInvocation) var29.elem).copy$default$1();
                                            FunctionName x$28 = ((FunctionInvocation) var29.elem).copy$default$2();
                                            boolean x$29 = ((FunctionInvocation) var29.elem).copy$default$3();
                                            InputPosition x$30 = ((FunctionInvocation) var29.elem).position();
                                            var319 = ((FunctionInvocation) var29.elem).copy( x$27, x$28, x$29, x$26, x$30 );
                                        }
                                        else
                                        {
                                            var319 = (FunctionInvocation) var29.elem;
                                        }

                                        var7 = var319;
                                    }

                                    var3 = var7;
                                    return var3;
                                }

                                Expression var204 = (Expression) ((FunctionInvocation) var29.elem).args().head();
                                Object var10;
                                if ( !(var204 instanceof Variable) )
                                {
                                    var10 = (FunctionInvocation) var29.elem;
                                }
                                else
                                {
                                    Object var11;
                                    label996:
                                    {
                                        Variable var205 = (Variable) var204;
                                        String keyxx = var205.name();
                                        Slot slotxx = this.slotConfiguration$1.apply( keyxx );
                                        boolean var208 = false;
                                        LongSlot var209 = null;
                                        if ( slotxx instanceof LongSlot )
                                        {
                                            var208 = true;
                                            var209 = (LongSlot) slotxx;
                                            int offsetxxxx = var209.offset();
                                            boolean var212 = var209.nullable();
                                            CypherType var213 = var209.typ();
                                            if ( var212 )
                                            {
                                                label978:
                                                {
                                                    var316 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                                                    if ( var316 == null )
                                                    {
                                                        if ( var213 != null )
                                                        {
                                                            break label978;
                                                        }
                                                    }
                                                    else if ( !var316.equals( var213 ) )
                                                    {
                                                        break label978;
                                                    }

                                                    var11 = new NullCheck( offsetxxxx, new RelationshipTypeFromSlot( offsetxxxx ) );
                                                    break label996;
                                                }
                                            }
                                        }

                                        int offsetxxxxxx;
                                        label646:
                                        {
                                            if ( var208 )
                                            {
                                                offsetxxxxxx = var209.offset();
                                                boolean var216 = var209.nullable();
                                                CypherType var217 = var209.typ();
                                                if ( !var216 )
                                                {
                                                    var316 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                                                    if ( var316 == null )
                                                    {
                                                        if ( var217 == null )
                                                        {
                                                            break label646;
                                                        }
                                                    }
                                                    else if ( var316.equals( var217 ) )
                                                    {
                                                        break label646;
                                                    }
                                                }
                                            }

                                            var11 = (FunctionInvocation) var29.elem;
                                            break label996;
                                        }

                                        var11 = new RelationshipTypeFromSlot( offsetxxxxxx );
                                    }

                                    var10 = var11;
                                }

                                var3 = var10;
                                return var3;
                            }

                            Expression var188 = (Expression) ((FunctionInvocation) var29.elem).args().head();
                            Object var12;
                            if ( !(var188 instanceof Variable) )
                            {
                                var12 = (FunctionInvocation) var29.elem;
                            }
                            else
                            {
                                Object var13;
                                label997:
                                {
                                    Variable var189 = (Variable) var188;
                                    String keyx = var189.name();
                                    Slot slotx = this.slotConfiguration$1.apply( keyx );
                                    boolean var192 = false;
                                    LongSlot var193 = null;
                                    if ( slotx instanceof LongSlot )
                                    {
                                        var192 = true;
                                        var193 = (LongSlot) slotx;
                                        int offsetxx = var193.offset();
                                        boolean var196 = var193.nullable();
                                        CypherType var197 = var193.typ();
                                        if ( var196 )
                                        {
                                            label981:
                                            {
                                                var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                                                if ( var10000 == null )
                                                {
                                                    if ( var197 != null )
                                                    {
                                                        break label981;
                                                    }
                                                }
                                                else if ( !var10000.equals( var197 ) )
                                                {
                                                    break label981;
                                                }

                                                var13 = new NullCheck( offsetxx, new LabelsFromSlot( offsetxx ) );
                                                break label997;
                                            }
                                        }
                                    }

                                    int offsetxxx;
                                    label679:
                                    {
                                        if ( var192 )
                                        {
                                            offsetxxx = var193.offset();
                                            boolean var200 = var193.nullable();
                                            CypherType var201 = var193.typ();
                                            if ( !var200 )
                                            {
                                                var10000 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                                                if ( var10000 == null )
                                                {
                                                    if ( var201 == null )
                                                    {
                                                        break label679;
                                                    }
                                                }
                                                else if ( var10000.equals( var201 ) )
                                                {
                                                    break label679;
                                                }
                                            }
                                        }

                                        var13 = (FunctionInvocation) var29.elem;
                                        break label997;
                                    }

                                    var13 = new LabelsFromSlot( offsetxxx );
                                }

                                var12 = var13;
                            }

                            var3 = var12;
                            return var3;
                        }

                        Object var14;
                        label713:
                        {
                            Expression var178 = (Expression) ((FunctionInvocation) var29.elem).args().head();
                            if ( var178 instanceof Property )
                            {
                                Property var179 = (Property) var178;
                                Expression var180 = var179.map();
                                PropertyKeyName var181 = var179.propertyKey();
                                if ( var180 instanceof Variable )
                                {
                                    Variable var182 = (Variable) var180;
                                    String key = var182.name();
                                    if ( var181 != null )
                                    {
                                        String propKey = var181.name();
                                        Slot slot = this.slotConfiguration$1.apply( key );
                                        Option maybeSpecializedExpression =
                                                this.$outer.org$neo4j$cypher$internal$physicalplanning$SlottedRewriter$$specializeCheckIfPropertyExists(
                                                        this.slotConfiguration$1, key, propKey, var179, slot );
                                        var14 = slot.nullable() && maybeSpecializedExpression.isDefined() &&
                                                        maybeSpecializedExpression.get() instanceof LogicalProperty ? new NullCheckProperty( slot.offset(),
                                                (LogicalProperty) maybeSpecializedExpression.get() ) : maybeSpecializedExpression.getOrElse( () -> {
                                            return (FunctionInvocation) var29.elem;
                                        } );
                                        break label713;
                                    }
                                }
                            }

                            var14 = (FunctionInvocation) var29.elem;
                        }

                        var3 = var14;
                    }
                }

                return var3;
            }

            public final boolean isDefinedAt( final Object x3 )
            {
                boolean var3 = false;
                IsNull var4 = null;
                boolean var5 = false;
                FunctionInvocation var6 = null;
                boolean var2;
                if ( x3 instanceof NestedPlanExpression )
                {
                    var2 = true;
                }
                else
                {
                    if ( x3 instanceof Property )
                    {
                        Property var8 = (Property) x3;
                        Expression var9 = var8.map();
                        PropertyKeyName var10 = var8.propertyKey();
                        if ( var9 instanceof Variable && var10 != null )
                        {
                            var2 = true;
                            return var2;
                        }
                    }

                    if ( x3 instanceof CachedProperty )
                    {
                        CachedProperty var11 = (CachedProperty) x3;
                        PropertyKeyName pkn = var11.propertyKey();
                        if ( pkn != null )
                        {
                            var2 = true;
                            return var2;
                        }
                    }

                    if ( x3 instanceof Equals )
                    {
                        Equals var13 = (Equals) x3;
                        Expression var14 = var13.lhs();
                        Expression var15 = var13.rhs();
                        if ( var14 instanceof Variable && var15 instanceof Variable )
                        {
                            var2 = true;
                            return var2;
                        }
                    }

                    if ( x3 instanceof Not )
                    {
                        Not var16 = (Not) x3;
                        Expression e = var16.rhs();
                        if ( e instanceof Equals )
                        {
                            Equals var18 = (Equals) e;
                            Expression var19 = var18.lhs();
                            Expression var20 = var18.rhs();
                            if ( var19 instanceof Variable && var20 instanceof Variable )
                            {
                                var2 = true;
                                return var2;
                            }
                        }
                    }

                    if ( x3 instanceof IsNull )
                    {
                        var3 = true;
                        var4 = (IsNull) x3;
                        Expression var21 = var4.lhs();
                        if ( var21 instanceof Variable )
                        {
                            var2 = true;
                            return var2;
                        }
                    }

                    if ( x3 instanceof GetDegree )
                    {
                        GetDegree var22 = (GetDegree) x3;
                        Expression var23 = var22.node();
                        if ( var23 instanceof Variable )
                        {
                            var2 = true;
                            return var2;
                        }
                    }

                    if ( x3 instanceof Variable )
                    {
                        var2 = true;
                    }
                    else
                    {
                        Function var10000;
                        if ( x3 instanceof FunctionInvocation )
                        {
                            label251:
                            {
                                var5 = true;
                                var6 = (FunctionInvocation) x3;
                                var10000 = var6.function();
                                org.neo4j.cypher.internal.v4_0.expressions.functions.Id.var24 = org.neo4j.cypher.internal.v4_0.expressions.functions.Id..
                                MODULE$;
                                if ( var10000 == null )
                                {
                                    if ( var24 != null )
                                    {
                                        break label251;
                                    }
                                }
                                else if ( !var10000.equals( var24 ) )
                                {
                                    break label251;
                                }

                                var2 = true;
                                return var2;
                            }
                        }

                        if ( var5 )
                        {
                            label252:
                            {
                                var10000 = var6.function();
                                org.neo4j.cypher.internal.v4_0.expressions.functions.Exists.var25 = org.neo4j.cypher.internal.v4_0.expressions.functions.Exists..
                                MODULE$;
                                if ( var10000 == null )
                                {
                                    if ( var25 != null )
                                    {
                                        break label252;
                                    }
                                }
                                else if ( !var10000.equals( var25 ) )
                                {
                                    break label252;
                                }

                                var2 = true;
                                return var2;
                            }
                        }

                        if ( var5 )
                        {
                            label253:
                            {
                                var10000 = var6.function();
                                org.neo4j.cypher.internal.v4_0.expressions.functions.Labels.var26 = org.neo4j.cypher.internal.v4_0.expressions.functions.Labels..
                                MODULE$;
                                if ( var10000 == null )
                                {
                                    if ( var26 != null )
                                    {
                                        break label253;
                                    }
                                }
                                else if ( !var10000.equals( var26 ) )
                                {
                                    break label253;
                                }

                                var2 = true;
                                return var2;
                            }
                        }

                        if ( var5 )
                        {
                            label254:
                            {
                                var10000 = var6.function();
                                org.neo4j.cypher.internal.v4_0.expressions.functions.Type.var27 = org.neo4j.cypher.internal.v4_0.expressions.functions.Type..
                                MODULE$;
                                if ( var10000 == null )
                                {
                                    if ( var27 != null )
                                    {
                                        break label254;
                                    }
                                }
                                else if ( !var10000.equals( var27 ) )
                                {
                                    break label254;
                                }

                                var2 = true;
                                return var2;
                            }
                        }

                        if ( var5 )
                        {
                            label255:
                            {
                                var10000 = var6.function();
                                org.neo4j.cypher.internal.v4_0.expressions.functions.Count.var28 = org.neo4j.cypher.internal.v4_0.expressions.functions.Count..
                                MODULE$;
                                if ( var10000 == null )
                                {
                                    if ( var28 != null )
                                    {
                                        break label255;
                                    }
                                }
                                else if ( !var10000.equals( var28 ) )
                                {
                                    break label255;
                                }

                                var2 = true;
                                return var2;
                            }
                        }

                        if ( x3 instanceof HasLabels )
                        {
                            HasLabels var29 = (HasLabels) x3;
                            Expression var30 = var29.expression();
                            if ( var30 instanceof Variable )
                            {
                                var2 = true;
                                return var2;
                            }
                        }

                        if ( var3 )
                        {
                            Expression prop = var4.lhs();
                            if ( prop instanceof Property )
                            {
                                Property var32 = (Property) prop;
                                Expression var33 = var32.map();
                                PropertyKeyName var34 = var32.propertyKey();
                                if ( var33 instanceof Variable && var34 != null )
                                {
                                    var2 = true;
                                    return var2;
                                }
                            }
                        }

                        if ( x3 instanceof IsNotNull )
                        {
                            IsNotNull var35 = (IsNotNull) x3;
                            Expression propx = var35.lhs();
                            if ( propx instanceof Property )
                            {
                                Property var37 = (Property) propx;
                                Expression var38 = var37.map();
                                PropertyKeyName var39 = var37.propertyKey();
                                if ( var38 instanceof Variable && var39 != null )
                                {
                                    var2 = true;
                                    return var2;
                                }
                            }
                        }

                        var2 = false;
                    }
                }

                return var2;
            }

            private final Tuple2 resolveLabelTokens$1( final Seq labels )
            {
                Seq maybeTokens = (Seq) labels.map( ( l ) -> {
                    return new Tuple2( this.$outer.org$neo4j$cypher$internal$physicalplanning$SlottedRewriter$$tokenContext.getOptLabelId( l.name() ),
                            l.name() );
                }, scala.collection.Seq..MODULE$.canBuildFrom());
                Tuple2 var5 = maybeTokens.partition( ( x$1 ) -> {
                    return BoxesRunTime.boxToBoolean( $anonfun$applyOrElse$8( x$1 ) );
                } );
                if ( var5 != null )
                {
                    Seq resolvedLabelTokens = (Seq) var5._1();
                    Seq lateLabels = (Seq) var5._2();
                    Tuple2 var2 = new Tuple2( resolvedLabelTokens, lateLabels );
                    Seq resolvedLabelTokensx = (Seq) var2._1();
                    Seq lateLabelsx = (Seq) var2._2();
                    return new Tuple2( resolvedLabelTokensx.flatMap( ( x$3 ) -> {
                        return scala.Option..MODULE$.option2Iterable( (Option) x$3._1() );
                    }, scala.collection.Seq..MODULE$.canBuildFrom() ),lateLabelsx.map( ( x$4 ) -> {
                    return (String) x$4._2();
                }, scala.collection.Seq..MODULE$.canBuildFrom()));
                }
                else
                {
                    throw new MatchError( var5 );
                }
            }
        } ); return .MODULE$.apply( innerRewriter, this.stopAtOtherLogicalPlans( thisPlan ) );
    }

    public Expression org$neo4j$cypher$internal$physicalplanning$SlottedRewriter$$primitiveEqualityChecks( final SlotConfiguration slots, final Equals e,
            final String k1, final String k2, final boolean positiveCheck )
    {
        Expression shortcutWhenDifferentTypes = positiveCheck ? new False( e.position() ) : new True( e.position() );
        Slot slot1 = slots.apply( k1 );
        Slot slot2 = slots.apply( k2 );
        Tuple2 var10 = new Tuple2( slot1, slot2 );
        Object var6;
        if ( var10 != null )
        {
            Slot var11 = (Slot) var10._1();
            Slot var12 = (Slot) var10._2();
            if ( var11 instanceof LongSlot )
            {
                LongSlot var13 = (LongSlot) var11;
                boolean var14 = var13.nullable();
                CypherType typ1 = var13.typ();
                if ( !var14 && var12 instanceof LongSlot )
                {
                    LongSlot var16 = (LongSlot) var12;
                    boolean var17 = var16.nullable();
                    CypherType typ2 = var16.typ();
                    if ( !var17 )
                    {
                        label175:
                        {
                            if ( typ1 == null )
                            {
                                if ( typ2 == null )
                                {
                                    break label175;
                                }
                            }
                            else if ( typ1.equals( typ2 ) )
                            {
                                break label175;
                            }

                            var6 = shortcutWhenDifferentTypes;
                            return (Expression) var6;
                        }
                    }
                }
            }
        }

        if ( var10 != null )
        {
            Slot var20 = (Slot) var10._1();
            Slot var21 = (Slot) var10._2();
            if ( var20 instanceof LongSlot )
            {
                LongSlot var22 = (LongSlot) var20;
                boolean var23 = var22.nullable();
                CypherType typ1 = var22.typ();
                if ( !var23 && var21 instanceof LongSlot )
                {
                    LongSlot var25 = (LongSlot) var21;
                    boolean var26 = var25.nullable();
                    CypherType typ2 = var25.typ();
                    if ( !var26 )
                    {
                        label176:
                        {
                            if ( typ1 == null )
                            {
                                if ( typ2 != null )
                                {
                                    break label176;
                                }
                            }
                            else if ( !typ1.equals( typ2 ) )
                            {
                                break label176;
                            }

                            PrimitiveEquals eq = new PrimitiveEquals( new IdFromSlot( slot1.offset() ), new IdFromSlot( slot2.offset() ) );
                            var6 = makeNegativeIfNeeded$1( eq, positiveCheck );
                            return (Expression) var6;
                        }
                    }
                }
            }
        }

        if ( var10 != null )
        {
            Slot var30 = (Slot) var10._1();
            Slot var31 = (Slot) var10._2();
            if ( var30 instanceof LongSlot )
            {
                LongSlot var32 = (LongSlot) var30;
                boolean null1 = var32.nullable();
                CypherType typ1 = var32.typ();
                if ( var31 instanceof LongSlot )
                {
                    LongSlot var35 = (LongSlot) var31;
                    boolean null2 = var35.nullable();
                    CypherType typ2 = var35.typ();
                    if ( null1 || null2 )
                    {
                        label177:
                        {
                            if ( typ1 == null )
                            {
                                if ( typ2 == null )
                                {
                                    break label177;
                                }
                            }
                            else if ( typ1.equals( typ2 ) )
                            {
                                break label177;
                            }

                            var6 = this.makeNullChecksExplicit( slot1, slot2, (Expression) shortcutWhenDifferentTypes );
                            return (Expression) var6;
                        }
                    }
                }
            }
        }

        label178:
        {
            if ( var10 != null )
            {
                Slot var39 = (Slot) var10._1();
                Slot var40 = (Slot) var10._2();
                if ( var39 instanceof LongSlot )
                {
                    LongSlot var41 = (LongSlot) var39;
                    boolean null1 = var41.nullable();
                    CypherType typ1 = var41.typ();
                    if ( var40 instanceof LongSlot )
                    {
                        LongSlot var44 = (LongSlot) var40;
                        boolean null2 = var44.nullable();
                        CypherType typ2 = var44.typ();
                        if ( null1 || null2 )
                        {
                            if ( typ1 == null )
                            {
                                if ( typ2 == null )
                                {
                                    break label178;
                                }
                            }
                            else if ( typ1.equals( typ2 ) )
                            {
                                break label178;
                            }
                        }
                    }
                }
            }

            var6 = makeNegativeIfNeeded$1( e, positiveCheck );
            return (Expression) var6;
        }

        PrimitiveEquals eq = new PrimitiveEquals( new IdFromSlot( slot1.offset() ), new IdFromSlot( slot2.offset() ) );
        var6 = this.makeNullChecksExplicit( slot1, slot2, makeNegativeIfNeeded$1( eq, positiveCheck ) );
        return (Expression) var6;
    }

    private Expression makeNullChecksExplicit( final Slot slot1, final Slot slot2, final Expression predicate )
    {
        return nullCheckIfNeeded$1( slot1, nullCheckIfNeeded$1( slot2, predicate ) );
    }

    public Option<RuntimeProperty> org$neo4j$cypher$internal$physicalplanning$SlottedRewriter$$specializeCheckIfPropertyExists(
            final SlotConfiguration slotConfiguration, final String key, final String propKey, final Property prop, final Slot slot )
    {
        Option maybeToken = this.org$neo4j$cypher$internal$physicalplanning$SlottedRewriter$$tokenContext.getOptPropertyKeyId( propKey );
        Tuple2 var8 = new Tuple2( slot, maybeToken );
        Object var6;
        if ( var8 != null )
        {
            Slot var9 = (Slot) var8._1();
            Option var10 = (Option) var8._2();
            if ( var9 instanceof LongSlot )
            {
                LongSlot var11 = (LongSlot) var9;
                int offset = var11.offset();
                CypherType typ = var11.typ();
                if ( var10 instanceof Some )
                {
                    label124:
                    {
                        Some var14 = (Some) var10;
                        int token = BoxesRunTime.unboxToInt( var14.value() );
                        NodeType var16 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                        if ( typ == null )
                        {
                            if ( var16 != null )
                            {
                                break label124;
                            }
                        }
                        else if ( !typ.equals( var16 ) )
                        {
                            break label124;
                        }

                        var6 = new Some(
                                new NodePropertyExists( offset, token, (new StringBuilder( 1 )).append( key ).append( "." ).append( propKey ).toString(),
                                        prop ) );
                        return (Option) var6;
                    }
                }
            }
        }

        int offset;
        label125:
        {
            if ( var8 != null )
            {
                Slot var17 = (Slot) var8._1();
                Option var18 = (Option) var8._2();
                if ( var17 instanceof LongSlot )
                {
                    LongSlot var19 = (LongSlot) var17;
                    offset = var19.offset();
                    CypherType typ = var19.typ();
                    if ( scala.None..MODULE$.equals( var18 )){
                    NodeType var22 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTNode();
                    if ( typ == null )
                    {
                        if ( var22 == null )
                        {
                            break label125;
                        }
                    }
                    else if ( typ.equals( var22 ) )
                    {
                        break label125;
                    }
                }
                }
            }

            if ( var8 != null )
            {
                Slot var23 = (Slot) var8._1();
                Option var24 = (Option) var8._2();
                if ( var23 instanceof LongSlot )
                {
                    LongSlot var25 = (LongSlot) var23;
                    int offset = var25.offset();
                    CypherType typ = var25.typ();
                    if ( var24 instanceof Some )
                    {
                        label126:
                        {
                            Some var28 = (Some) var24;
                            int token = BoxesRunTime.unboxToInt( var28.value() );
                            RelationshipType var30 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                            if ( typ == null )
                            {
                                if ( var30 != null )
                                {
                                    break label126;
                                }
                            }
                            else if ( !typ.equals( var30 ) )
                            {
                                break label126;
                            }

                            var6 = new Some( new RelationshipPropertyExists( offset, token,
                                    (new StringBuilder( 1 )).append( key ).append( "." ).append( propKey ).toString(), prop ) );
                            return (Option) var6;
                        }
                    }
                }
            }

            int offset;
            label127:
            {
                if ( var8 != null )
                {
                    Slot var31 = (Slot) var8._1();
                    Option var32 = (Option) var8._2();
                    if ( var31 instanceof LongSlot )
                    {
                        LongSlot var33 = (LongSlot) var31;
                        offset = var33.offset();
                        CypherType typ = var33.typ();
                        if ( scala.None..MODULE$.equals( var32 )){
                        RelationshipType var36 = org.neo4j.cypher.internal.v4_0.util.symbols.package..MODULE$.CTRelationship();
                        if ( typ == null )
                        {
                            if ( var36 == null )
                            {
                                break label127;
                            }
                        }
                        else if ( typ.equals( var36 ) )
                        {
                            break label127;
                        }
                    }
                    }
                }

                var6 = scala.None..MODULE$;
                return (Option) var6;
            }

            var6 = new Some(
                    new RelationshipPropertyExistsLate( offset, propKey, (new StringBuilder( 1 )).append( key ).append( "." ).append( propKey ).toString(),
                            prop ) );
            return (Option) var6;
        }

        var6 = new Some(
                new NodePropertyExistsLate( offset, propKey, (new StringBuilder( 1 )).append( key ).append( "." ).append( propKey ).toString(), prop ) );
        return (Option) var6;
    }

    private Function1<Object,Object> stopAtOtherLogicalPlans( final LogicalPlan thisPlan )
    {
        return ( x0$1 ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$stopAtOtherLogicalPlans$1( thisPlan, x0$1 ) );
        };
    }
}
