package org.neo4j.cypher.internal.physicalplanning;

import org.neo4j.cypher.internal.v4_0.util.symbols.CypherType;
import scala.Function1;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple3;
import scala.collection.Iterator;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.Statics;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class LongSlot implements Slot, Product, Serializable
{
    private final int offset;
    private final boolean nullable;
    private final CypherType typ;

    public LongSlot( final int offset, final boolean nullable, final CypherType typ )
    {
        this.offset = offset;
        this.nullable = nullable;
        this.typ = typ;
        Product.$init$( this );
    }

    public static Option<Tuple3<Object,Object,CypherType>> unapply( final LongSlot x$0 )
    {
        return LongSlot$.MODULE$.unapply( var0 );
    }

    public static LongSlot apply( final int offset, final boolean nullable, final CypherType typ )
    {
        return LongSlot$.MODULE$.apply( var0, var1, var2 );
    }

    public static Function1<Tuple3<Object,Object,CypherType>,LongSlot> tupled()
    {
        return LongSlot$.MODULE$.tupled();
    }

    public static Function1<Object,Function1<Object,Function1<CypherType,LongSlot>>> curried()
    {
        return LongSlot$.MODULE$.curried();
    }

    public int offset()
    {
        return this.offset;
    }

    public boolean nullable()
    {
        return this.nullable;
    }

    public CypherType typ()
    {
        return this.typ;
    }

    public boolean isTypeCompatibleWith( final Slot other )
    {
        boolean var2;
        if ( other instanceof LongSlot )
        {
            LongSlot var4 = (LongSlot) other;
            CypherType otherTyp = var4.typ();
            var2 = this.typ().isAssignableFrom( otherTyp ) || otherTyp.isAssignableFrom( this.typ() );
        }
        else
        {
            var2 = false;
        }

        return var2;
    }

    public boolean isLongSlot()
    {
        return true;
    }

    public LongSlot asNullable()
    {
        return new LongSlot( this.offset(), true, this.typ() );
    }

    public LongSlot copy( final int offset, final boolean nullable, final CypherType typ )
    {
        return new LongSlot( offset, nullable, typ );
    }

    public int copy$default$1()
    {
        return this.offset();
    }

    public boolean copy$default$2()
    {
        return this.nullable();
    }

    public CypherType copy$default$3()
    {
        return this.typ();
    }

    public String productPrefix()
    {
        return "LongSlot";
    }

    public int productArity()
    {
        return 3;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = BoxesRunTime.boxToInteger( this.offset() );
            break;
        case 1:
            var10000 = BoxesRunTime.boxToBoolean( this.nullable() );
            break;
        case 2:
            var10000 = this.typ();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof LongSlot;
    }

    public int hashCode()
    {
        int var1 = -889275714;
        var1 = Statics.mix( var1, this.offset() );
        var1 = Statics.mix( var1, this.nullable() ? 1231 : 1237 );
        var1 = Statics.mix( var1, Statics.anyHash( this.typ() ) );
        return Statics.finalizeHash( var1, 3 );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var6;
        if ( this != x$1 )
        {
            label57:
            {
                boolean var2;
                if ( x$1 instanceof LongSlot )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( var2 )
                {
                    label40:
                    {
                        LongSlot var4 = (LongSlot) x$1;
                        if ( this.offset() == var4.offset() && this.nullable() == var4.nullable() )
                        {
                            label37:
                            {
                                CypherType var10000 = this.typ();
                                CypherType var5 = var4.typ();
                                if ( var10000 == null )
                                {
                                    if ( var5 != null )
                                    {
                                        break label37;
                                    }
                                }
                                else if ( !var10000.equals( var5 ) )
                                {
                                    break label37;
                                }

                                if ( var4.canEqual( this ) )
                                {
                                    var6 = true;
                                    break label40;
                                }
                            }
                        }

                        var6 = false;
                    }

                    if ( var6 )
                    {
                        break label57;
                    }
                }

                var6 = false;
                return var6;
            }
        }

        var6 = true;
        return var6;
    }
}
