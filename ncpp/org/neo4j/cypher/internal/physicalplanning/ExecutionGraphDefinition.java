package org.neo4j.cypher.internal.physicalplanning;

import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Tuple5;
import scala.collection.IndexedSeq;
import scala.collection.Iterator;
import scala.collection.immutable.Map;
import scala.reflect.ScalaSignature;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime.;

@JavaDocToJava
public class ExecutionGraphDefinition implements Product, Serializable
{
    private final PhysicalPlan physicalPlan;
    private final IndexedSeq<BufferDefinition> buffers;
    private final IndexedSeq<ArgumentStateDefinition> argumentStateMaps;
    private final IndexedSeq<PipelineDefinition> pipelines;
    private final Map<Object,Object> applyRhsPlans;

    public ExecutionGraphDefinition( final PhysicalPlan physicalPlan, final IndexedSeq<BufferDefinition> buffers,
            final IndexedSeq<ArgumentStateDefinition> argumentStateMaps, final IndexedSeq<PipelineDefinition> pipelines,
            final Map<Object,Object> applyRhsPlans )
    {
        this.physicalPlan = physicalPlan;
        this.buffers = buffers;
        this.argumentStateMaps = argumentStateMaps;
        this.pipelines = pipelines;
        this.applyRhsPlans = applyRhsPlans;
        Product.$init$( this );
    }

    public static Option<Tuple5<PhysicalPlan,IndexedSeq<BufferDefinition>,IndexedSeq<ArgumentStateDefinition>,IndexedSeq<PipelineDefinition>,Map<Object,Object>>> unapply(
            final ExecutionGraphDefinition x$0 )
    {
        return ExecutionGraphDefinition$.MODULE$.unapply( var0 );
    }

    public static ExecutionGraphDefinition apply( final PhysicalPlan physicalPlan, final IndexedSeq<BufferDefinition> buffers,
            final IndexedSeq<ArgumentStateDefinition> argumentStateMaps, final IndexedSeq<PipelineDefinition> pipelines,
            final Map<Object,Object> applyRhsPlans )
    {
        return ExecutionGraphDefinition$.MODULE$.apply( var0, var1, var2, var3, var4 );
    }

    public static BufferId[] NO_BUFFERS()
    {
        return ExecutionGraphDefinition$.MODULE$.NO_BUFFERS();
    }

    public static ArgumentStateMapId[] NO_ARGUMENT_STATE_MAPS()
    {
        return ExecutionGraphDefinition$.MODULE$.NO_ARGUMENT_STATE_MAPS();
    }

    public PhysicalPlan physicalPlan()
    {
        return this.physicalPlan;
    }

    public IndexedSeq<BufferDefinition> buffers()
    {
        return this.buffers;
    }

    public IndexedSeq<ArgumentStateDefinition> argumentStateMaps()
    {
        return this.argumentStateMaps;
    }

    public IndexedSeq<PipelineDefinition> pipelines()
    {
        return this.pipelines;
    }

    public Map<Object,Object> applyRhsPlans()
    {
        return this.applyRhsPlans;
    }

    public int findArgumentStateMapForPlan( final int planId )
    {
        return ((ArgumentStateMapId) this.argumentStateMaps().find( ( x$1 ) -> {
            return BoxesRunTime.boxToBoolean( $anonfun$findArgumentStateMapForPlan$1( planId, x$1 ) );
        } ).map( ( x$2 ) -> {
            return new ArgumentStateMapId( $anonfun$findArgumentStateMapForPlan$2( x$2 ) );
        } ).getOrElse( () -> {
            throw new IllegalStateException( "Requested an ArgumentStateMap for an operator which does not have any." );
        } )).x();
    }

    public ExecutionGraphDefinition copy( final PhysicalPlan physicalPlan, final IndexedSeq<BufferDefinition> buffers,
            final IndexedSeq<ArgumentStateDefinition> argumentStateMaps, final IndexedSeq<PipelineDefinition> pipelines,
            final Map<Object,Object> applyRhsPlans )
    {
        return new ExecutionGraphDefinition( physicalPlan, buffers, argumentStateMaps, pipelines, applyRhsPlans );
    }

    public PhysicalPlan copy$default$1()
    {
        return this.physicalPlan();
    }

    public IndexedSeq<BufferDefinition> copy$default$2()
    {
        return this.buffers();
    }

    public IndexedSeq<ArgumentStateDefinition> copy$default$3()
    {
        return this.argumentStateMaps();
    }

    public IndexedSeq<PipelineDefinition> copy$default$4()
    {
        return this.pipelines();
    }

    public Map<Object,Object> copy$default$5()
    {
        return this.applyRhsPlans();
    }

    public String productPrefix()
    {
        return "ExecutionGraphDefinition";
    }

    public int productArity()
    {
        return 5;
    }

    public Object productElement( final int x$1 )
    {
        Object var10000;
        switch ( x$1 )
        {
        case 0:
            var10000 = this.physicalPlan();
            break;
        case 1:
            var10000 = this.buffers();
            break;
        case 2:
            var10000 = this.argumentStateMaps();
            break;
        case 3:
            var10000 = this.pipelines();
            break;
        case 4:
            var10000 = this.applyRhsPlans();
            break;
        default:
            throw new IndexOutOfBoundsException( BoxesRunTime.boxToInteger( x$1 ).toString() );
        }

        return var10000;
    }

    public Iterator<Object> productIterator()
    {
        return .MODULE$.typedProductIterator( this );
    }

    public boolean canEqual( final Object x$1 )
    {
        return x$1 instanceof ExecutionGraphDefinition;
    }

    public int hashCode()
    {
        return .MODULE$._hashCode( this );
    }

    public String toString()
    {
        return .MODULE$._toString( this );
    }

    public boolean equals( final Object x$1 )
    {
        boolean var12;
        label84:
        {
            if ( this != x$1 )
            {
                boolean var2;
                if ( x$1 instanceof ExecutionGraphDefinition )
                {
                    var2 = true;
                }
                else
                {
                    var2 = false;
                }

                if ( !var2 )
                {
                    break label84;
                }

                label71:
                {
                    label79:
                    {
                        ExecutionGraphDefinition var4 = (ExecutionGraphDefinition) x$1;
                        PhysicalPlan var10000 = this.physicalPlan();
                        PhysicalPlan var5 = var4.physicalPlan();
                        if ( var10000 == null )
                        {
                            if ( var5 != null )
                            {
                                break label79;
                            }
                        }
                        else if ( !var10000.equals( var5 ) )
                        {
                            break label79;
                        }

                        IndexedSeq var10 = this.buffers();
                        IndexedSeq var6 = var4.buffers();
                        if ( var10 == null )
                        {
                            if ( var6 != null )
                            {
                                break label79;
                            }
                        }
                        else if ( !var10.equals( var6 ) )
                        {
                            break label79;
                        }

                        var10 = this.argumentStateMaps();
                        IndexedSeq var7 = var4.argumentStateMaps();
                        if ( var10 == null )
                        {
                            if ( var7 != null )
                            {
                                break label79;
                            }
                        }
                        else if ( !var10.equals( var7 ) )
                        {
                            break label79;
                        }

                        var10 = this.pipelines();
                        IndexedSeq var8 = var4.pipelines();
                        if ( var10 == null )
                        {
                            if ( var8 != null )
                            {
                                break label79;
                            }
                        }
                        else if ( !var10.equals( var8 ) )
                        {
                            break label79;
                        }

                        Map var11 = this.applyRhsPlans();
                        Map var9 = var4.applyRhsPlans();
                        if ( var11 == null )
                        {
                            if ( var9 != null )
                            {
                                break label79;
                            }
                        }
                        else if ( !var11.equals( var9 ) )
                        {
                            break label79;
                        }

                        if ( var4.canEqual( this ) )
                        {
                            var12 = true;
                            break label71;
                        }
                    }

                    var12 = false;
                }

                if ( !var12 )
                {
                    break label84;
                }
            }

            var12 = true;
            return var12;
        }

        var12 = false;
        return var12;
    }
}
